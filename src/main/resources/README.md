# The project is divided to :

1 .DAL - Data Access Layer
    1.1 Folders: enums, model, repository.
    1.2 Each db model entity in the model folder must have a matching repository file in the repository folder.
    1.3 After an update or creation of an entity occurs, the DB will be updated in the compilation time.

2. BL - business logic
    2.1 Each service in the service folder has access to its repository file.
    2.2 All of the services that represent an entity inherit from the CRUD service.
    2.3 Due to spring compilation errors, each service has a constructor that points at its repository.

3. Controllers
    3.1 Each controller inherits from the CRUD controller.
    3.2 Due to spring compilation errors, each controller has a constructor that points at its service.


4.Config folder
    4.1 Token util.
    4.2 Request filter.
    4.3 Web security config
        4.3.1 LDAP connection.
        4.3.2 Authenticate request.
    4.4 JwtAuthenticationEntryPoint -Authenticate entry point.

# application.properties
This file includes all of the external properties, for example the DB connection details, the LDAP connection details,
and the current environment type (onPrem)

#log4j2.xml
In this project we use a logger named log4j2.xml. This file is used for configuring the logger.

# Upload to server

- run the command `$ mvn clean package` in the terminal
- then connect to the server 3.125.153.95 with FileZilla
- copy the jar file to /home/ec2-user folder
- login to the server using SSH: `$ ssh -i ciso-be.pem ec2-user@3.125.153.95`
- move the jar file to the `cyberview` user folder using the command `$ mv ciso-be.jar ../cyberview`
- change directory to that folder `$ cd ../cyberview`
- change the ownership of the jar file to `cyberview` user `$ chown cyberview:cyberview ciso-be.jar`
- change permissions to allow execution of the jar file `$ chmod +x ciso-be.jar`
- restart the service `$ sudo service ciso-be restart`
- view the logs at: `$ tail -f /var/log/ciso-be.log`

