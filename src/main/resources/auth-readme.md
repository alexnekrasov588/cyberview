# Role and Permissions 
#### DataBase:
* At the CompanyUsers table we have a foreign key to the Roles table (one to one).
* Permissions table: we have a module-name field (ModuleEnum in the enums folder), 4 booleans fields (CRUD): 
isCreate,isRead,isUpdate and isDelete, and foreign key to the Roles table (one(Role) to many(Permission)).
* Role table: we have only a role name and a foreign key to a company table(many(role) to one(company)).
  
####Code:
* At CRUDService we have a "handlePermission" function - this function call to a private function "hasPermission".
The input fields to this
 function: token, OperationEnum and ModuleEnum. This function throws a security error when the permissions do not match.
* Each services that inherit from the crud service adds to its constructor a module type.
* Each route from the controllers calls "handlePermission", catches a security error and returns 403 if the current user
 from the token doesn't have the right permissions.   


