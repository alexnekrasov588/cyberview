package com.lstechs.ciso.enums;

public enum VulnResponseEnum {
	RETAIN, MODIFY, SHARE, AVOID
}
