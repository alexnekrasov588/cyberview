package com.lstechs.ciso.enums;

public enum OperationEnum {
	CREATE,
	READ,
	UPDATE,
	DELETE
}
