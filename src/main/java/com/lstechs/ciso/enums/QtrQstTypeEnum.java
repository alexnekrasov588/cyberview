package com.lstechs.ciso.enums;

public enum QtrQstTypeEnum {
    TEXT,
    FILE,
    CHECKBOX,
    DROPDOWN,
	DATE
}
