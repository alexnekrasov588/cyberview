package com.lstechs.ciso.enums;

public enum ExplanationAndArticleNumberTypeEnum {
	TEXT,
	FILE,
	CHECKBOX,
	DROPDOWN,
	DATE
}
