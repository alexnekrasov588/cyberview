package com.lstechs.ciso.enums;

public enum FavoriteActionEnum {
	CREATE,
	UPDATE,
	DELETE
}
