package com.lstechs.ciso.enums;

public enum ModuleEnum {
	CUSTOMER_STANDARD,
	NEWS,
	CVE,
	PROJECT,
	VENDOR,
	AUTH,
	TASK,
	RISK_ASSESSMENTS,
	ASSET_MANAGEMENT
}
