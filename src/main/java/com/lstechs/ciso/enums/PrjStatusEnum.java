package com.lstechs.ciso.enums;

public enum PrjStatusEnum {
	COMPLETED,
	INITIATED,
	IN_PROGRESS,
	ON_HOLD,
	PLANNED

}
