package com.lstechs.ciso.enums;

public enum NewsSrcTypeEnum {
    WEB_SITE,
    RSS,
	API
}
