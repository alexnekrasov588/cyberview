package com.lstechs.ciso.enums;

public enum RiskEnum {
	INIT_TO_ORG,
	INIT_TO_DATA,
	AFTER_TO_ORG,
	AFTER_TO_DATA
}
