package com.lstechs.ciso.enums;

public enum StandardTypeEnum {
	STANDARD,
	RISK_ASSESSMENTS,
	POLICY,
	OTHER
}
