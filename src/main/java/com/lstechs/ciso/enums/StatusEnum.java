package com.lstechs.ciso.enums;

public enum StatusEnum {
    OPENED,
    IN_PROGRESS,
	ON_HOLD,
    DONE
}
