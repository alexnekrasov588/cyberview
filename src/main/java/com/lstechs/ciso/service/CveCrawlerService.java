package com.lstechs.ciso.service;

import com.lstechs.ciso.crawlers.rss.FeedMessage;
import com.lstechs.ciso.crawlers.rss.RSSFeedParser;
import com.lstechs.ciso.model.CVEModel;
import com.lstechs.ciso.model.CVESourcesModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@EnableScheduling
public class CveCrawlerService {

    @Autowired
    private CVEService cveService;

    @Autowired
    private CVESourcesService cveSourcesService;

    private static Logger LOGGER = LogManager.getLogger(CveCrawlerService.class.getName());

    @Async
    @Scheduled(cron = "${crawler.cve.cron}")
    public void scanSites() {
        // get all news sources in the DB
        List<CVESourcesModel> sources = cveSourcesService.getAll();
        List<RSSFeedParser> rssList = new ArrayList<RSSFeedParser>();

        for (CVESourcesModel source : sources) {
            rssList.add(new RSSFeedParser(source.getCveSrcSiteUrl(), source));
        }

        LOGGER.info("Found " + rssList.size() + " CVE RSS feeds to scan");
        for (RSSFeedParser parser : rssList) {
            List<FeedMessage> messages = parser.getArticles();
            List<CVEModel> articles = new ArrayList<>();
            for (FeedMessage message : messages) {
                CVESourcesModel sourceModel = (CVESourcesModel) parser.getSourcesModel();
                articles.add(new CVEModel(message.getTitle(), message.getDescription(),message.getLink(), true, message.getDate(), sourceModel));
            }

            for (CVEModel cveArticle : articles) {
                try {
                    cveService.create(cveArticle);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
        }
    }

}

