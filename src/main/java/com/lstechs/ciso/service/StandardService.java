package com.lstechs.ciso.service;

import com.lstechs.ciso.enums.ModuleEnum;
import com.lstechs.ciso.enums.QtrQstTypeEnum;
import com.lstechs.ciso.enums.StandardTypeEnum;
import com.lstechs.ciso.model.*;
import com.lstechs.ciso.repository.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.common.usermodel.HyperlinkType;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class StandardService extends CRUDService<StandardModel, SingleStandardModel> {

	@Autowired
	public StandardRepository standardRepository;

	private static final Logger LOGGER = LogManager.getLogger(StandardService.class.getName());

	@Autowired
	public LicenseStandardService licenseStandardService;

	@Autowired
	public AuthenticationService authenticationService;

	@Autowired
	public StdQuestionaireService stdQuestionaireService;

	@Autowired
	public StandardCompanyStatusService standardCompanyStatusService;

	@Autowired
	public StdSectionService stdSectionService;

	@Autowired
	public StdQtrQuestionsService stdQtrQuestionsService;

	@Autowired
	public StdRequirementsService stdRequirementsService;

	@Autowired
	public CustomerStandardService customerStandardService;

	@Autowired
	public ModuleTaskService moduleTaskService;

	@Autowired
	public CrossReferenceService crossReferenceService;

	@Autowired
	public RequestQuestionnaireService requestQuestionnaireService;

	@Autowired
	public CstQstFileAnswerRepository cstQstFileAnswerRepository;

	@Autowired
	public SelectedStandardsRepository selectedStandardsRepository;

	@Autowired
	public SelectedQuestionnaireRepository selectedQuestionnaireRepository;

	@Autowired
	public RoleService roleService;

	@Autowired
	public StdQuestionaireRepository stdQuestionaireRepository;

	@Autowired
	public RequirementsHeaderTitlesService requirementsHeaderTitlesService;

	@Autowired
	public RequirementsHeaderTitlesRepository requirementsHeaderTitlesRepository;

	@Autowired
	public VendorStandardHeaderTitlesService vendorStandardHeaderTitlesService;

	@Autowired
	public VendorStandardHeaderTitlesRepository vendorStandardHeaderTitlesRepository;

	@Autowired
	public LicenseRepository licenseRepository;

	@Autowired
	public LicenseStandardRepository licenseStandardRepository;

	public StandardService(StandardRepository standardRepository) {
		super(standardRepository);
	}

	@Override
	public SingleStandardModel getSingleInstance(StandardModel standardModel) {
		return new SingleStandardModel(standardModel);
	}

	@Override
	public Page<StandardModel> getAll(Integer pageNo, Integer pageSize, String orderBy, boolean orderByDesc, String query) throws Exception {
		LOGGER.info("get all from crud service, Repository: " + crudRepository.getClass().getSimpleName());
		Sort sort = Sort.by(Sort.Order.desc(orderBy));
		if (!orderByDesc) {
			sort = Sort.by(Sort.Order.asc(orderBy));
		}
		Pageable paging = PageRequest.of(pageNo, pageSize, sort);
		return standardRepository.getTempalteStandard(paging);
	}

	@Override
	public StandardModel create(StandardModel standardModel) throws Exception {
		if (isStandardExistsByNameVersionAndLevel(standardModel.getStdName(), standardModel.getStdLvlName(),
			standardModel.getStdVersion(), 0))
			throw new Exception("This standard already exist!");
		if (standardModel.getStdType() == null)
			standardModel.setStdType(StandardTypeEnum.STANDARD);
		LOGGER.info("create from crud service, Repository: " + crudRepository.getClass().getSimpleName());
		return crudRepository.save(standardModel);
	}

	public StandardModel createVendorStandard(StandardModel standardModel) throws Exception {
		if (isStandardExistsByNameVersionAndLevel(standardModel.getStdName(), standardModel.getStdLvlName(),
			standardModel.getStdVersion(), 0))
			throw new Exception("This standard already exist!");
		if (standardModel.getStdType() == null)
			standardModel.setStdType(StandardTypeEnum.STANDARD);
		LOGGER.info("create from crud service, Repository: " + crudRepository.getClass().getSimpleName());

		StandardModel savedStandardModel = crudRepository.save(standardModel);

		// Creating Default Vendor Header Titles
		VendorStandardHeaderTitlesModel vendorStandardHeaderTitles = new VendorStandardHeaderTitlesModel();
		vendorStandardHeaderTitles.setStandardId(savedStandardModel);
		try {
			vendorStandardHeaderTitlesService.create(vendorStandardHeaderTitles);
		} catch (Exception ex) {
			LOGGER.info(ex.getMessage());
		}
		return savedStandardModel;
	}

	public StandardModel createCisoStandard(StandardModel standardModel, String token) throws Exception {

		if (isStandardExistsByNameVersionAndLevel(standardModel.getStdName(), standardModel.getStdLvlName(),
			standardModel.getStdVersion(), 0))
			throw new Exception("This standard already exist!");

		if (standardModel.getStdType() == null)
			standardModel.setStdType(StandardTypeEnum.STANDARD);
		LOGGER.info("create from crud service, Repository: " + crudRepository.getClass().getSimpleName());

		// Activate the Standard
		standardModel.setStdIsActive(true);
		standardModel.setStdIsCompleted(true);
		standardModel.setStandardCreatedByCompany(true);

		StandardModel savedStandardModel = crudRepository.save(standardModel);

		// Adding Standard to License Standard
		CompanyModel companyModel = authenticationService.getCompanyFromToken(token.substring(7));
		LicenseModel companyLicense = licenseRepository.findLicenseByLcnsId(companyModel.getCmpLcnsId().getLcnsId());
		LicenseStandardModel companyLicenseStandardModel = new LicenseStandardModel();
		companyLicenseStandardModel.setStdId(savedStandardModel);
		companyLicenseStandardModel.setLcnsId(companyLicense);

		try {
			licenseStandardRepository.save(companyLicenseStandardModel);
		} catch (Exception ex) {
			LOGGER.info(ex.getMessage());
		}

		// Creating Questionnaire
		StdQuestionaireModel stdQuestionaireModel = new StdQuestionaireModel();
		stdQuestionaireModel.setStdId(savedStandardModel);
		stdQuestionaireModel.setStdQtrName(savedStandardModel.getStdName());
		stdQuestionaireModel.setStandardCreatedByCompany(true);
		stdQuestionaireModel.setQuestionnaireCreatedByCompany(true);

		// Saving the Questionnaire & the default Titles
		try {
			StdQuestionaireModel savedStdQuestionaireModel = stdQuestionaireService.create(stdQuestionaireModel);
			RequirementsHeaderTitlesModel requirementsHeaderTitlesModel = new RequirementsHeaderTitlesModel();
			requirementsHeaderTitlesModel.setQuestionnaireId(savedStdQuestionaireModel);
			requirementsHeaderTitlesRepository.save(requirementsHeaderTitlesModel);
		} catch (Exception ex) {
			LOGGER.info(ex.getMessage());
		}
		return savedStandardModel;
	}

	@Override
	public StandardModel update(int id, StandardModel standardModel) throws Exception {
		if (isStandardExistsByNameVersionAndLevel(standardModel.getStdName(), standardModel.getStdLvlName(),
			standardModel.getStdVersion(), id))
			throw new Exception("This standard details already exist!");

		LOGGER.info("update Standard  , id: " + id + ".");
		StandardModel updateStandardModel = getById(id);
		updateStandardModel.setStdDescription(standardModel.getStdDescription());
		updateStandardModel.setStdIsActive(standardModel.isStdIsActive());
		updateStandardModel.setStdIsCompleted(standardModel.isStdIsCompleted());
		updateStandardModel.setStdName(standardModel.getStdName());
		updateStandardModel.setStdVersion(standardModel.getStdVersion());

		if (updateStandardModel.isStandardCreatedByCompany()) {
			List<StdQuestionaireModel> stdQuestionaireModelList = stdQuestionaireRepository.getQuestionaireListByTemplateId(standardModel.getStdId());
			if (stdQuestionaireModelList.size() > 0) {
				for (StdQuestionaireModel stdQuestionaireModel : stdQuestionaireModelList) {
					stdQuestionaireModel.setStdQtrName(standardModel.getStdName());
					stdQuestionaireRepository.save(stdQuestionaireModel);
				}
			}
		}
		return standardRepository.save(updateStandardModel);
	}

	public void markComplete(int id) throws Exception {
		LOGGER.info("Mark complete in standard - id: " + id + ".");
		getById(id);
		standardRepository.markComplete(id);
	}

	@Override
	public StandardModel delete(int id) throws Exception {
		LOGGER.info("delete standard id: " + id + ".");
		StandardModel standardModel = getById(id);

		// Deleting Titles if the request coming from Vendor user
		try {
			VendorStandardHeaderTitlesModel vendorStandardHeaderTitlesModel = vendorStandardHeaderTitlesRepository.getVendorHeaderTitlesByStandardId(id);
			if (vendorStandardHeaderTitlesModel != null) {
				vendorStandardHeaderTitlesModel.setDeletedAt(new Date(System.currentTimeMillis()));
				vendorStandardHeaderTitlesRepository.save(vendorStandardHeaderTitlesModel);
			}
		} catch (Exception ex) {
			LOGGER.error(ex.getMessage());
		}

		// Deleting Standard
		standardModel.setDeletedAt(new Date(System.currentTimeMillis()));
		standardRepository.save(standardModel);
		licenseStandardService.deleteLicenseStandardByStandardId(id);
		List<StandardCompanyStatusModel> standardCompanyStatusModels = standardCompanyStatusService
			.getAllByStandardId(id);
		for (StandardCompanyStatusModel standardCompanyStatusModel : standardCompanyStatusModels) {
			standardCompanyStatusService.delete(standardCompanyStatusModel.getStdCmpId());
		}
		List<StdQuestionaireModel> stdQuestionnaireModels = stdQuestionaireService.getQuestionaireListByTemplateId(id);
		for (StdQuestionaireModel tmpStdQuestionnaireModel : stdQuestionnaireModels) {
			stdQuestionaireService.delete(tmpStdQuestionnaireModel.getStdQtrId());
		}
		crossReferenceService.deleteByStdId(id);
		return standardModel;
	}

	public StandardModel deleteStandardFromCiso(int id) throws Exception {
		LOGGER.info("delete standard id: " + id + ".");
		StandardModel standardModel = getById(id);

		if (standardModel.isStandardCreatedByCompany()) {
			standardModel.setDeletedAt(new Date(System.currentTimeMillis()));
			standardRepository.save(standardModel);
			licenseStandardService.deleteLicenseStandardByStandardId(id);
			List<StandardCompanyStatusModel> standardCompanyStatusModels = standardCompanyStatusService
				.getAllByStandardId(id);
			for (StandardCompanyStatusModel standardCompanyStatusModel : standardCompanyStatusModels) {
				standardCompanyStatusService.delete(standardCompanyStatusModel.getStdCmpId());
			}
			List<StdQuestionaireModel> stdQuestionnaireModels = stdQuestionaireService.getQuestionaireListByTemplateId(id);
			for (StdQuestionaireModel tmpStdQuestionnaireModel : stdQuestionnaireModels) {
				stdQuestionaireService.delete(tmpStdQuestionnaireModel.getStdQtrId());
			}
			crossReferenceService.deleteByStdId(id);
			return standardModel;
		} else {
			LOGGER.error("Standard wasn't created by Ciso user");
			return null;
		}
	}

	public List<StandardModel> getAllAvailCompanyStandard(String token) throws Exception {
		LOGGER.info("Get all avail company standard by token: " + token + ".");
		List<LicenseStandardModel> licenseStandardModel = getStandardsByToken(token);
		List<StandardModel> standardModels = new ArrayList<>();
		for (LicenseStandardModel tmpLicenseStandardModel : licenseStandardModel) {
			StandardModel standardModel = getById(tmpLicenseStandardModel.getStdId().getStdId());
			if (!standardModels.contains(standardModel))
				standardModels.add(standardModel);
		}
		return standardModels;
	}

	public Page<StandardModel> getAllAvailCompanyStandardByPage(Integer pageNo, Integer pageSize, String orderBy,
																boolean orderByDesc, String query, String token) {
		LOGGER.info("get all avail company standard by token: " + token + ".");

		//Selected Standards Handling
		CompanyUserModel companyUserModel = authenticationService.getCompanyUserFromToken(token.substring(7));
		int roleId = companyUserModel.getCmpUsrRoleId().getRoleId();
		RoleModel role = companyUserModel.getCmpUsrRoleId();

		//Checking if the role is Ciso Or All Standards and questionnaires were selected
		// And getting all existing standards
		if (role.isAreAllStandardsSelected() || role.getRoleName().equals("Ciso")) {

			List<LicenseStandardModel> licenseStandardModel = getStandardsByToken(token);
			List<Integer> standardModels = new ArrayList<>();
			for (LicenseStandardModel tmpLicenseStandardModel : licenseStandardModel) {
				if (!standardModels.contains(tmpLicenseStandardModel.getStdId().getStdId()))
					standardModels.add(tmpLicenseStandardModel.getStdId().getStdId());
			}
			if (standardModels.size() > 0) {
				Sort sort = Sort.by(Sort.Order.desc(orderBy));
				if (!orderByDesc) {
					sort = Sort.by(Sort.Order.asc(orderBy));
				}
				Pageable paging = PageRequest.of(pageNo, pageSize, sort);


				Page<StandardModel> standardModelsAsPage = standardRepository.getCompanyStandard(paging, standardModels);

				// Standard Progress Handling.
				List<Map<String, Object>> listMapStandardsDetails = new ArrayList<>();
				ArrayList<Integer> standardIds = new ArrayList<>(Collections.emptyList());
				List<StandardModel> standardModelsAsList = standardModelsAsPage.getContent();

				CompanyModel companyModel = authenticationService.getCompanyFromToken(token.substring(7));
				String lcnsToken = companyModel.getCmpLcnsId().getLcnsToken();

				List<Object[]> standardProgressModels = getCompliantStdReqCountByLcnsTokenAndCmpId(lcnsToken,
					companyModel.getCmpId());

				// Populate The Ids
				for (StandardModel standardModel : standardModelsAsList) {
					standardIds.add(standardModel.getStdId());
				}

				for (Object[] tmpStandardProgress : standardProgressModels) {
					for (Integer standardId : standardIds) {
						if (standardId.equals(tmpStandardProgress[0])) {
							Map<String, Object> standardsDetails = new HashMap<>();
							standardsDetails.put("standardId", tmpStandardProgress[0]);
							standardsDetails.put("standardName", tmpStandardProgress[1]);
							standardsDetails.put("progress", tmpStandardProgress[2]);
							listMapStandardsDetails.add(standardsDetails);

						}
					}
				}

				for (StandardModel standardModel : standardModelsAsList) {
					for (Map<String, Object> listMapStandardsDetail : listMapStandardsDetails) {

						Integer standardId = (Integer) listMapStandardsDetail.get("standardId");

						if (standardModel.getStdId() == standardId) {
							BigDecimal standardProgress = (BigDecimal) listMapStandardsDetail.get("progress");
							standardModel.setStandardProgress(standardProgress);
						}
					}
				}
				return new PageImpl<>(standardModelsAsList);
			} else
				return null;
		} else {

			List<SelectedStandardModel> selectedStandardModelList = selectedStandardsRepository.getSelectedStandardModelByRoleId(roleId);
			List<Integer> standardModels = new ArrayList<>();

			for (SelectedStandardModel selectedStandardModel : selectedStandardModelList) {
				standardModels.add(selectedStandardModel.getStandardId());
			}

			if (standardModels.size() > 0) {
				Sort sort = Sort.by(Sort.Order.desc(orderBy));
				if (!orderByDesc) {
					sort = Sort.by(Sort.Order.asc(orderBy));
				}
				Pageable paging = PageRequest.of(pageNo, pageSize, sort);


				return standardRepository.getCompanyStandard(paging, standardModels);
			}
			return null;
		}
	}

	public List<StandardNameModel> getStandardsNameByToken(String token) {
		LOGGER.info("get standards name by token: " + token + ".");
		List<LicenseStandardModel> licenseStandardModel = getStandardsByToken(token);
		List<Integer> standardModels = new ArrayList<>();
		for (LicenseStandardModel tmpLicenseStandardModel : licenseStandardModel) {
			if (!standardModels.contains(tmpLicenseStandardModel.getStdId().getStdId()))
				standardModels.add(tmpLicenseStandardModel.getStdId().getStdId());
		}
		return standardRepository.getCompanyStandardName(standardModels);
	}

	public boolean isStandardExistsByNameVersionAndLevel(String stdName, String stdLvlName, String stdVersion,
														 int stdId) {
		LOGGER.info("Check if standard already exist by: Name: " + stdName + ", level: " + stdLvlName + " and version: "
			+ stdVersion + ".");
		return standardRepository.isStandardExistsByNameVersionAndLevel(stdName, stdLvlName, stdVersion, stdId);
	}

	public StandardModel getStandardByNameVersionAndLevel(String stdName, String stdLvlName, String stdVersion) {
		return standardRepository.getStandardExistsByNameVersionAndLevel(stdName, stdLvlName, stdVersion);
	}

	public boolean ifStandardExistInCompany(int standardId, String token) {
		LOGGER.info("if standard exist in company ,standardId: " + standardId + ".");
		List<LicenseStandardModel> licenseStandardModel = getStandardsByToken(token);
		for (LicenseStandardModel tmpLicenseStandardModel : licenseStandardModel) {
			if (tmpLicenseStandardModel.getStdId().getStdId() == standardId) {
				return true;
			}
		}
		return false;
	}

	private List<LicenseStandardModel> getStandardsByToken(String token) {
		LOGGER.info("Get standards by token: " + token + ".");
		CompanyModel companyModel = authenticationService.getCompanyFromToken(token.substring(7));
		LicenseModel licenseModel = companyModel.getCmpLcnsId();
		return licenseStandardService.findAllLicenseStandardByLcnsId(licenseModel.getLcnsId());
	}

	public List<Object[]> getCompliantStdReqCountByLcnsTokenAndCmpId(String lcnsToken, int cmpId) {
		LOGGER.info("Get compliant Standard Requirement count By license token: " + lcnsToken + ".");
		return standardRepository.getCompliantStdReqCountByLcnsTokenAndCmpId(lcnsToken, cmpId);
	}

	public List<StandardModel> getAllStandardWithoutReqsByToken(String lcnsToken) {
		LOGGER.info("Get all standards that have no requirements By license token: " + lcnsToken + ".");
		return standardRepository.getAllStandardWithoutReqsByToken(lcnsToken);
	}

	public List<StdSectionModel> getSectionsByQuestionnaireId(int stdQtrId, int cmpId) throws Exception {
		LOGGER.info("Get all Sections by questionnaire id: " + stdQtrId + ".");
		List<StdSectionModel> stdSectionModels = stdSectionService.getAllSectionByQuestionaireIdWithOutParent(stdQtrId);
		if (stdSectionModels != null && stdSectionModels.size() > 0) {
			for (StdSectionModel tmpStdSectionModel : stdSectionModels) {
				tmpStdSectionModel.setStdQtrQuestionsList(getStdQtrQuestionsModels(tmpStdSectionModel, cmpId));
				tmpStdSectionModel.setStdSectionModels(recursive(tmpStdSectionModel.getStdSctId(), cmpId));
				tmpStdSectionModel.setParent(null);
			}
		}
		return stdSectionModels;
	}

	public StdSectionModel getSectionBySectionId(int stdSctId, int questionnaireId) throws Exception {
		if (stdSectionService.isSectionInQuestionnaire(stdSctId, questionnaireId)) {
			LOGGER.info("Section id:" + stdSctId + " exist in questionnaire id: " + questionnaireId + ".");
			LOGGER.info("Get Section by Section id: " + stdSctId + ".");
			return stdSectionService.getById(stdSctId);
		}
		LOGGER.info("Section id:" + stdSctId + " NOT exist in questionnaire id: " + questionnaireId + ".");
		return null;
	}

	public StandardModel getStandard(int id, String token, boolean isFromDuplicate) throws Exception {
		LOGGER.info("Get standards by id: " + id + ".");

		//Selected Standards and Questionnaires Handling
		CompanyUserModel companyUserModel = authenticationService.getCompanyUserFromToken(token.substring(7));
		int roleId = companyUserModel.getCmpUsrRoleId().getRoleId();
		RoleModel role = roleService.getById(roleId);

		//Getting the Standard Model by Standard Id
		StandardModel standardModel = getById(id);

		// Getting All Questionnaires By Template Id
		ArrayList<StdQuestionaireModel> StdQuestionnaireModels = new ArrayList<>(stdQuestionaireService.getQuestionaireListByTemplateIdWithDuplicate(id, companyUserModel.getCmpUsrCmpId().getCmpId()));

		ArrayList<StdQuestionaireModel> stdQuestionnaireModelsTemporaryCopy = new ArrayList<>(StdQuestionnaireModels.size());


		for (StdQuestionaireModel originalList : StdQuestionnaireModels) {
			StdQuestionaireModel newStdQuestionaireModel = new StdQuestionaireModel(originalList);
			newStdQuestionaireModel.setStdQtrId(originalList.getStdQtrId());
			newStdQuestionaireModel.setDuplicatedFrom(originalList.getDuplicatedFrom());
			stdQuestionnaireModelsTemporaryCopy.add(newStdQuestionaireModel);
		}

		// Handling With Questionnaires Names
		if (StdQuestionnaireModels != null && StdQuestionnaireModels.size() > 0) {
			for (StdQuestionaireModel stdQuestionnaireModels : StdQuestionnaireModels) {

				// Setting the Questionnaire specific name
				stdQuestionnaireModels.setQuestionnaireSpecificName(stdQuestionnaireModels.getStdQtrName());

				if (stdQuestionnaireModels.getDuplicatedFrom() > 0) {

					if (stdQuestionnaireModels.getParentId() > 0) {
						StdQuestionaireModel parentQuestionnaire = null;

						for (StdQuestionaireModel stdQuestionnaireModelFromTemporaryCopy : stdQuestionnaireModelsTemporaryCopy) {
							if (stdQuestionnaireModels.getParentId() == stdQuestionnaireModelFromTemporaryCopy.getStdQtrId()) {
								parentQuestionnaire = stdQuestionnaireModelFromTemporaryCopy;
							}
						}
						if (parentQuestionnaire != null) {
							if (parentQuestionnaire.getDuplicatedFrom() == 0) {

								stdQuestionnaireModels.setStdQtrName(parentQuestionnaire.getStdQtrName() + " - " + stdQuestionnaireModels.getStdQtrName());

							} else {
								// In case that direct Parent Questionnaire has duplicatedFrom is bigger than 0
								StdQuestionaireModel originalStdQuestionaire = getUppermostQuestionnaire(parentQuestionnaire, StdQuestionnaireModels);
								stdQuestionnaireModels.setStdQtrName(originalStdQuestionaire.getStdQtrName() + " - " + parentQuestionnaire.getStdQtrName() + " - " + stdQuestionnaireModels.getStdQtrName());
							}
						}
					} else {
						// In case that Questionnaire has no ParentId
						StdQuestionaireModel originalStdQuestionnaire = getUppermostQuestionnaire(stdQuestionnaireModels, StdQuestionnaireModels);
						stdQuestionnaireModels.setStdQtrName(originalStdQuestionnaire.getStdQtrName() + " - " + stdQuestionnaireModels.getStdQtrName());
					}
				}
			}
		}

		// Ciso is getting all the Questionnaires
		if (role.getRoleName().equals("Ciso")) {
			if (StdQuestionnaireModels != null && StdQuestionnaireModels.size() > 0) {
				standardModel.setStdQuestionaireList(StdQuestionnaireModels);
			}
			return standardModel;
		}

		// Some Standards are selected && All Questionnaires are selected
		if (role.isAreAllStandardsSelected() && role.isAreAllQuestionnairesSelected()) {
			if (StdQuestionnaireModels != null && StdQuestionnaireModels.size() > 0) {
				standardModel.setStdQuestionaireList(StdQuestionnaireModels);
			}
		}

		// Some Standards are selected && All Questionnaires are selected
		if (!role.isAreAllStandardsSelected() && role.isAreAllQuestionnairesSelected()) {
			if (StdQuestionnaireModels != null && StdQuestionnaireModels.size() > 0) {
				standardModel.setStdQuestionaireList(StdQuestionnaireModels);
			}
		}
		if (role.isAreAllStandardsSelected() && !role.isAreAllQuestionnairesSelected()) {
			// Return null if customer doesn't has any Questionnaires assigned to his roleId
			List<SelectedQuestionnaireModel> selectedQuestionnaireModelList = selectedQuestionnaireRepository.getSelectedQuestionnairesModelByRoleIdAndStandardId(roleId, id);
			if (selectedQuestionnaireModelList.size() == 0) {
				return null;
			}

			//Creating List of Questionnaires Ids from Selected Questionnaires List
			List<Integer> questionnairesIds = new ArrayList<>(Collections.emptyList());
			for (SelectedQuestionnaireModel selectedQuestionnaireModel : selectedQuestionnaireModelList) {
				questionnairesIds.add(selectedQuestionnaireModel.getQuestionnaireId());
			}

			//Getting all questionnaires by questionnaires Ids from Selected Questionnaire List
			List<StdQuestionaireModel> stdQuestionnaireModels = stdQuestionaireService.getQuestionairesByIds(questionnairesIds);
			if (stdQuestionnaireModels != null && stdQuestionnaireModels.size() > 0) {
				standardModel.setStdQuestionaireList(stdQuestionnaireModels);
			}

		}
		//All Standards and questionnaires were selected
		if (role.isAreAllStandardsSelected() && role.isAreAllQuestionnairesSelected()) {
			if (StdQuestionnaireModels != null && StdQuestionnaireModels.size() > 0) {
				standardModel.setStdQuestionaireList(StdQuestionnaireModels);
			}
			return standardModel;
		} else {
			// Return null if customer doesn't has any Questionnaires assigned to his roleId
			List<SelectedQuestionnaireModel> selectedQuestionnaireModelList = selectedQuestionnaireRepository.getSelectedQuestionnairesModelByRoleIdAndStandardId(roleId, id);
			if (selectedQuestionnaireModelList.size() == 0) {
				return null;
			}

			//Creating List of Questionnaires Ids from Selected Questionnaires List
			List<Integer> questionnairesIds = new ArrayList<>(Collections.emptyList());
			for (SelectedQuestionnaireModel selectedQuestionnaireModel : selectedQuestionnaireModelList) {
				questionnairesIds.add(selectedQuestionnaireModel.getQuestionnaireId());
			}

			//Getting all questionnaires by questionnaires Ids from Selected Questionnaire List
			List<StdQuestionaireModel> stdQuestionnaireModels = stdQuestionaireService.getQuestionairesByIds(questionnairesIds);
			if (stdQuestionnaireModels != null && stdQuestionnaireModels.size() > 0) {
				standardModel.setStdQuestionaireList(stdQuestionnaireModels);
			}
		}
		return standardModel;
	}


	public StdQuestionaireModel getUppermostQuestionnaire(StdQuestionaireModel questionnaire, List<StdQuestionaireModel> stdQuestionaireModelList) {

		StdQuestionaireModel parentQuestionnaire = null;
		if (questionnaire.getDuplicatedFrom() > 0) {
			for (StdQuestionaireModel stdQuestionaireModel : stdQuestionaireModelList) {
				if (questionnaire.getDuplicatedFrom() == stdQuestionaireModel.getStdQtrId()) {
					parentQuestionnaire = stdQuestionaireModel;
				}
			}
		}
		if (parentQuestionnaire != null) {
			return getUppermostQuestionnaire(parentQuestionnaire, stdQuestionaireModelList);
		}
		return questionnaire;
	}

	public StdQuestionaireModel getQuestionnaireContent(int stdQtrId, int cmpId) throws Exception {
		LOGGER.info("Get Questionnaire, id: " + stdQtrId + ".");
		StdQuestionaireModel stdQuestionnaireModel = stdQuestionaireService.getById(stdQtrId);
		stdQuestionnaireModel.setStdSectionModelList(getSectionsByQuestionnaireId(stdQtrId, cmpId));

		// Function that is setting all subsections parent field to NULL
		settingAllSubsectionsParentToNull(stdQuestionnaireModel);

		// Requirement Titles Handling
		if (stdQuestionnaireModel.isStandardCreatedByCompany()) {
			RequirementsHeaderTitlesModel requirementsHeaderTitlesModel = requirementsHeaderTitlesService.getRequirementsHeaderTitlesByQuestionnaireId(stdQtrId);
			if (requirementsHeaderTitlesModel != null) {
				stdQuestionnaireModel.setRequirementsHeaderTitlesModel(requirementsHeaderTitlesModel);
			}
		} else {
			VendorStandardHeaderTitlesModel vendorStandardHeaderTitlesModel = vendorStandardHeaderTitlesService.getVendorHeaderTitlesByStandardId(stdQuestionnaireModel.getStdId().getStdId());
			if (vendorStandardHeaderTitlesModel != null) {
				stdQuestionnaireModel.setVendorStandardHeaderTitlesModel(vendorStandardHeaderTitlesModel);
			}
		}
		return stdQuestionnaireModel;
	}

	private void settingAllSubsectionsParentToNull(StdQuestionaireModel stdQuestionnaireModel) {

		List<StdSectionModel> parentSections = stdQuestionnaireModel.getStdSectionModelList();
		List<StdSectionModel> childrenSections = Collections.emptyList();

		for (StdSectionModel parentSection : parentSections) {
			List<StdSectionModel> tempSectionsList = parentSection.getStdSectionModels();

			for (StdSectionModel tempSectionList : tempSectionsList) {
				tempSectionList.setParent(null);
			}
		}
	}

	public List<StandardModel> getAllStandardsWithRequirements() {
		LOGGER.info("Get All Standards with Requirements.");
		List<StandardModel> standardModels = new ArrayList<>();
		List<StandardModel> tmpStandardModelList = getAll();
		for (StandardModel tmpStd : tmpStandardModelList) {
			LOGGER.info("Get Requirements by standard id: " + tmpStd.getStdId() + ".");
			List<StdRequirementsModel> requirementsModelList = stdRequirementsService
				.getRequirementsListByTemplateId(tmpStd.getStdId());
			if (requirementsModelList.size() > 0) {
				requirementsModelList = addQuestionnaireName(requirementsModelList);
				tmpStd.setStdRequirementsList(requirementsModelList);
				standardModels.add(tmpStd);
			}
		}
		return standardModels;
	}

	private List<StdRequirementsModel> addQuestionnaireName(List<StdRequirementsModel> requirementsModelList) {
		for (StdRequirementsModel srm : requirementsModelList) {
			srm.setQuestionnaireName(stdRequirementsService.getStdQtrNameByReqId(srm.getStdRqrId()));
		}
		return requirementsModelList;
	}

	private List<StdSectionModel> recursive(int id, int cmpId) throws Exception {
		List<StdSectionModel> recursiveSection = new ArrayList<>();
		List<StdSectionModel> stdSectionModels = stdSectionService.getAllSectionByParentId(id);
		if (stdSectionModels.size() == 0) {
			return recursiveSection;
		}
		for (StdSectionModel tmpStdSectionModel : stdSectionModels) {
			tmpStdSectionModel.setStdQtrQuestionsList(getStdQtrQuestionsModels(tmpStdSectionModel, cmpId));
			tmpStdSectionModel.setStdSectionModels(recursive(tmpStdSectionModel.getStdSctId(), cmpId));

			recursiveSection.add(tmpStdSectionModel);
		}
		return recursiveSection;
	}

	private List<StdQtrQuestionsModel> getStdQtrQuestionsModels(StdSectionModel stdSectionModel, Integer cmpId)
		throws Exception {
		List<StdQtrQuestionsModel> stdQtrQuestionsModels = stdQtrQuestionsService
			.getAllQuestionBySectionId(stdSectionModel.getStdSctId());
		if (stdQtrQuestionsModels != null && stdQtrQuestionsModels.size() > 0) {
			for (StdQtrQuestionsModel tmpStdQtrQuestionsModel : stdQtrQuestionsModels) {
				if (tmpStdQtrQuestionsModel.getStdRqrId() != null
					&& tmpStdQtrQuestionsModel.getStdRqrId().getStdRqrId() > 0) {
					StdRequirementsModel stdRequirementsModel = stdRequirementsService
						.getById(tmpStdQtrQuestionsModel.getStdRqrId().getStdRqrId());
					List<CustomerStandardModel> customerStandardModels = stdRequirementsModel
						.getCustomerStandardModel();
					if (customerStandardModels != null && customerStandardModels.size() > 0) {
						// there is customer standard to this requirement
						// get customer standard by cmpId - one per company
						if (cmpId != null) {
							CustomerStandardModel customerStandardModel = customerStandardService
								.getCustomerStandardByReqIdAndCmpId(stdRequirementsModel.getStdRqrId(), cmpId);
							if (customerStandardModel != null) {

								try {
									List<CstQstFileAnswerModel> cstQstFileAnswerModels = cstQstFileAnswerRepository.getCstQstFileAnswerByCustomerStandardId(customerStandardModel);
								/*
								  Extracting Customer Standard file names and Url's
								  And setting HashMap with the values
								 */
									HashMap<String, String> cstQstFileAnswer = new HashMap<>();
									for (CstQstFileAnswerModel cstQstFileAnswerModel : cstQstFileAnswerModels) {
										cstQstFileAnswer.put(cstQstFileAnswerModel.getFileUrl(), cstQstFileAnswerModel.getFileName());
									}
									customerStandardModel.setCstQstFileAnswer(cstQstFileAnswer);
								} catch (Exception ex) {
									LOGGER.error(ex.getMessage());
								}


								stdRequirementsModel.setCustomerStandard(customerStandardModel);
								ModuleTaskModel moduleTaskModel = moduleTaskService.getModuleTaskByModuleAndIdAndCmpId(
									ModuleEnum.CUSTOMER_STANDARD, customerStandardModel.getCstStdId(), cmpId);
								if (moduleTaskModel != null && moduleTaskModel.getTskId() != null) {
									stdRequirementsModel.getCustomerStandard().setTaskModel(moduleTaskModel.getTskId());
								}
							}
						}
					}
					tmpStdQtrQuestionsModel.setStdRequirements(stdRequirementsModel);
				}
			}
		}
		return stdQtrQuestionsModels;
	}

	public StandardModel duplicateStandard(StandardModel originalStandard, String token) throws Exception {
		LOGGER.info("Duplicate Standard , id: " + originalStandard.getStdId() + ".");
		StandardModel tmpStandardModel = new StandardModel(originalStandard);
		tmpStandardModel.setStdName("(Copy) " + originalStandard.getStdName());
		// check if standard already exist
		if (isStandardExistsByNameVersionAndLevel(tmpStandardModel.getStdName(), tmpStandardModel.getStdLvlName(),
			tmpStandardModel.getStdVersion(), 0))
			throw new Exception("This standard already duplicated and exist!");
		StandardModel newStandardModel = create(tmpStandardModel);
		LOGGER.info("Create template , id: " + newStandardModel.getStdId() + ".");

		VendorStandardHeaderTitlesModel newVendorStandardHeaderTitlesModel = new VendorStandardHeaderTitlesModel();
		VendorStandardHeaderTitlesModel originalVendorStandardHeaderTitlesModel = vendorStandardHeaderTitlesRepository.getVendorHeaderTitlesByStandardId(originalStandard.getStdId());

		if (originalVendorStandardHeaderTitlesModel != null) {
			newVendorStandardHeaderTitlesModel.setRequirementTitle(originalVendorStandardHeaderTitlesModel.getRequirementTitle());
			newVendorStandardHeaderTitlesModel.setDescriptionTitle(originalVendorStandardHeaderTitlesModel.getDescriptionTitle());
			newVendorStandardHeaderTitlesModel.setExplanationTitle(originalVendorStandardHeaderTitlesModel.getExplanationTitle());
			newVendorStandardHeaderTitlesModel.setArticleNumberTitle(originalVendorStandardHeaderTitlesModel.getArticleNumberTitle());
			newVendorStandardHeaderTitlesModel.setGenericAnswer1Title(originalVendorStandardHeaderTitlesModel.getGenericAnswer1Title());
			newVendorStandardHeaderTitlesModel.setGenericAnswer2Title(originalVendorStandardHeaderTitlesModel.getGenericAnswer2Title());
			newVendorStandardHeaderTitlesModel.setStandardId(newStandardModel);

			// Saving the Titles
			vendorStandardHeaderTitlesRepository.save(newVendorStandardHeaderTitlesModel);
		}

		List<StdQuestionaireModel> stdQuestionnaireModels = stdQuestionaireService
			.getQuestionaireListByTemplateId(originalStandard.getStdId());
		if (stdQuestionnaireModels != null && stdQuestionnaireModels.size() > 0) {
			for (StdQuestionaireModel tempQuestionnaireModel : stdQuestionnaireModels) {
				stdQuestionaireService.duplicateVendorOriginalQuestionnaire(tempQuestionnaireModel, newStandardModel, token, originalStandard, true);
			}
		}
		return newStandardModel;
	}

	public List<StandardComplianceModel> standardComplianceReport(String lcnsToken, int cmpId) throws Exception {

		List<StandardComplianceModel> standardComplianceModels = new ArrayList<>();
		List<Object[]> standardProgressModels = getCompliantStdReqCountByLcnsTokenAndCmpId(lcnsToken, cmpId);

		for (Object[] tmpStandardProgress : standardProgressModels) {
			StandardComplianceModel standardComplianceModel = new StandardComplianceModel();
			standardComplianceModel.setStandardModel(getById((int) tmpStandardProgress[0]));
			standardComplianceModel.setProgress(((BigDecimal) tmpStandardProgress[2]).doubleValue());
			int year = Calendar.getInstance().get(Calendar.YEAR);
			standardComplianceModel.setCycleEndDate("31/12/" + year);
			standardComplianceModels.add(standardComplianceModel);
		}

		List<StandardModel> standardsWithoutReqs = getAllStandardWithoutReqsByToken(lcnsToken);

		for (StandardModel tmpStd : standardsWithoutReqs) {
			StandardComplianceModel standardComplianceModel = new StandardComplianceModel();
			standardComplianceModel.setStandardModel(tmpStd);
			standardComplianceModel.setProgress(0);
			int year = Calendar.getInstance().get(Calendar.YEAR);
			standardComplianceModel.setCycleEndDate("31/12/" + year);
			standardComplianceModels.add(standardComplianceModel);
		}

		return standardComplianceModels;
	}

	public List<StandardModel> getStdListByIds(int[] ids) throws Exception {
		List<StandardModel> standardModels = new ArrayList<>();
		for (int id : ids) {
			standardModels.add(getById(id));
		}
		return standardModels;
	}

	public StdReportsFilesModel getComplianceReport(StdReportRequestModel stdReportRequestModel, int cmpId)
		throws Exception {
		List<Integer> qstList = Arrays.stream(stdReportRequestModel.getStdQtrId()).boxed().collect(Collectors.toList());
		List<StdQuestionaireModel> questionnaireList = stdQuestionaireService.getQuestionairesByIds(qstList);
		List<Integer> customerStdWithFilesIds = new ArrayList();
		if (questionnaireList != null && questionnaireList.size() > 0) {
			String[] columns = {"Requirement", "Testing procedure", "Guidance", "Answer", "Compliant", "Remark"};

			try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream()) {
				for (StdQuestionaireModel model : questionnaireList) {
					Sheet sheet = workbook
						.createSheet(model.getStdId().getStdName().replace('/', ' ').replace(':', ' '));

					Font headerFont = workbook.createFont();
					headerFont.setBold(true);
					headerFont.setColor(IndexedColors.BLACK.getIndex());

					Font linkFont = workbook.createFont();
					linkFont.setBold(true);
					linkFont.setColor(IndexedColors.BLUE.getIndex());
					linkFont.setUnderline(XSSFFont.U_SINGLE);

					CellStyle headerCellStyle = workbook.createCellStyle();
					headerCellStyle.setFont(headerFont);
					headerCellStyle.setWrapText(true);
					headerCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
					headerCellStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
					headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

					CellStyle regularCellStyle = workbook.createCellStyle();
					regularCellStyle.setWrapText(true);
					regularCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);

					CellStyle linkCellStyle = workbook.createCellStyle();
					linkCellStyle.setWrapText(true);
					linkCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
					linkCellStyle.setFont(linkFont);

					Row header = sheet.createRow(0);

					Cell standardNameCell = header.createCell(0);
					standardNameCell.setCellValue("Questionnaire name: " + model.getStdQtrName());
					standardNameCell.setCellStyle(headerCellStyle);

					int rowIndex = 2;

					// Row for Header
					Row headerRow = sheet.createRow(rowIndex);

					// Header
					for (int col = 0; col < columns.length; col++) {
						Cell cell = headerRow.createCell(col);
						cell.setCellValue(columns[col]);
						cell.setCellStyle(headerCellStyle);
					}

					List<StdSectionModel> stdSectionModels = stdSectionService
						.getAllSectionByQuestionaireId(model.getStdQtrId());
					if (stdSectionModels != null && stdSectionModels.size() > 0) {
						for (StdSectionModel tmpStdSectionModel : stdSectionModels) {
							List<StdQtrQuestionsModel> stdQtrQuestionsModels = stdQtrQuestionsService
								.getAllQuestionBySectionId(tmpStdSectionModel.getStdSctId());
							if (stdQtrQuestionsModels != null && stdQtrQuestionsModels.size() > 0) {
								for (StdQtrQuestionsModel tmpStdQtrQuestionsModel : stdQtrQuestionsModels) {
									if (tmpStdQtrQuestionsModel.getStdRqrId() != null
										&& tmpStdQtrQuestionsModel.getStdRqrId().getStdRqrId() != 0) {
										StdRequirementsModel stdRequirementsModel = stdRequirementsService
											.getById(tmpStdQtrQuestionsModel.getStdRqrId().getStdRqrId());
										if (stdRequirementsModel != null) {
											rowIndex++;
											Row row = sheet.createRow(rowIndex);
											if (stdRequirementsModel.getStdRqrName() != null) {
												Cell cell = row.createCell(0);
												cell.setCellValue(stdRequirementsModel.getStdRqrName());
												cell.setCellStyle(regularCellStyle);
											}
											if (stdRequirementsModel.getStdRqrDescription() != null) {
												Cell cell = row.createCell(1);
												cell.setCellValue(stdRequirementsModel.getStdRqrDescription());
												cell.setCellStyle(regularCellStyle);
											}
											if (stdRequirementsModel.getStdRqrExplanation() != null) {
												Cell cell = row.createCell(2);
												cell.setCellValue(stdRequirementsModel.getStdRqrExplanation());
												cell.setCellStyle(regularCellStyle);
											}
											CustomerStandardModel customerStandardModel = customerStandardService
												.getCustomerStandardByReqIdAndCmpId(
													stdRequirementsModel.getStdRqrId(), cmpId);
											if (customerStandardModel != null) {
												if (customerStandardModel.getIsFileExist()) {
													customerStdWithFilesIds.add(customerStandardModel.getCstStdId());
													Cell cell = row.createCell(3);
													cell.setCellValue(customerStandardModel.getFile().getFileName());

													File file = new File(customerStandardModel.getFile().getFileName());
													Hyperlink href = workbook.getCreationHelper()
														.createHyperlink(HyperlinkType.FILE);
													String filePath = file.toURI().toString().substring(
														file.toURI().toString().lastIndexOf('/') + 1,
														file.toURI().toString().length());
													href.setAddress(filePath);
													cell.setHyperlink(href);
													cell.setCellStyle(linkCellStyle);
//													Cell cell = row.createCell(3);
//													cell.setCellValue(customerStandardModel.getFile().getFileName());
//													cell.setCellStyle(regularCellStyle);
												} else if (customerStandardModel.getCstQstAnswer() != null) {
													Cell cell = row.createCell(3);
//													cell.setCellValue(customerStandardModel.getCstQstAnswer());
													cell.setCellStyle(regularCellStyle);
												}
												if (customerStandardModel.getCompliant() != null) {
													Cell cell = row.createCell(4);
													cell.setCellValue(customerStandardModel.getCompliant().toString());
													cell.setCellStyle(regularCellStyle);
												}

												if (customerStandardModel.getCstStdRemarks() != null) {
													Cell cell = row.createCell(5);
													cell.setCellValue(customerStandardModel.getCstStdRemarks());
													cell.setCellStyle(regularCellStyle);
												}
											}
										}
									}
								}
							}
						}

					}
					sheet.setDefaultColumnWidth(30);

				}
				workbook.write(out);

				// return new ByteArrayInputStream(out.toByteArray());
				return new StdReportsFilesModel(customerStdWithFilesIds,
					new ByteArrayInputStream(out.toByteArray()));
			}
		}
		return null;
	}

	public StandardModel createCustomStandardFromExcelFile(MultipartFile file, String token, String type, String
		name, String desc, String version)
		throws Exception {

		CompanyModel companyModel = authenticationService.getCompanyFromToken(token.substring(7));

		XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());
		XSSFSheet worksheet = workbook.getSheetAt(3);

		StandardTypeEnum standardType = StandardTypeEnum.valueOf(type);

		StandardModel standardModel = new StandardModel();
		standardModel.setStdName(name);
		standardModel.setStdVersion(version);
		standardModel.setStdDescription(desc);
		standardModel.setStdIsActive(true);
		standardModel.setStdIsCompleted(true);
		standardModel.setStdType(standardType);
		standardModel.setStdLvlName(type);
		standardModel.setStandardCreatedByCompany(true);

		StandardModel createdStandard = create(standardModel);

		LicenseModel licenseModel = companyModel.getCmpLcnsId();

		LicenseStandardModel licenseStandardModel = new LicenseStandardModel(licenseModel, createdStandard);
		licenseStandardService.create(licenseStandardModel);

		StdQuestionaireModel questionaireModel = new StdQuestionaireModel();
		questionaireModel.setStdId(createdStandard);
		questionaireModel.setStdQtrName(name);
		questionaireModel.setQuestionnaireCreatedByCompany(true);
		questionaireModel.setStandardCreatedByCompany(true);

		StdQuestionaireModel createdStdQuestionaire = stdQuestionaireService.create(questionaireModel);

		// Handling with titles
		Cell requirementTitle = null, descriptionTitle = null, explanationTitle = null, articleNumberTitle = null, genericAnswer1Title = null, genericAnswer2Title = null;

		for (int i = 9; i < 10; i++) {

			XSSFRow row = worksheet.getRow(i);
			try {

				requirementTitle = row.getCell(6);
				descriptionTitle = row.getCell(8);
				explanationTitle = row.getCell(9);
				articleNumberTitle = row.getCell(10);
				genericAnswer1Title = row.getCell(11);
				genericAnswer2Title = row.getCell(13);

			} catch (Exception ex) {
				LOGGER.info(ex.getMessage());
			}
		}

		RequirementsHeaderTitlesModel requirementsHeaderTitlesModel = new RequirementsHeaderTitlesModel();
		assert requirementTitle != null;
		if (!requirementTitle.toString().equals("")) {
			requirementsHeaderTitlesModel.setRequirementTitle(requirementTitle.toString());
		}
		assert descriptionTitle != null;
		if (!descriptionTitle.toString().equals("")) {
			requirementsHeaderTitlesModel.setDescriptionTitle(descriptionTitle.toString());
		}
		assert explanationTitle != null;
		if (!explanationTitle.toString().equals("")) {
			requirementsHeaderTitlesModel.setExplanationTitle(explanationTitle.toString());
		}
		assert articleNumberTitle != null;
		if (!articleNumberTitle.toString().equals("")) {
			requirementsHeaderTitlesModel.setArticleNumberTitle(articleNumberTitle.toString());
		}
		assert genericAnswer1Title != null;
		if (!genericAnswer1Title.toString().equals("")) {
			requirementsHeaderTitlesModel.setGenericAnswer1Title(genericAnswer1Title.toString());
		}
		assert genericAnswer2Title != null;
		if (!genericAnswer2Title.toString().equals("")) {
			requirementsHeaderTitlesModel.setGenericAnswer2Title(genericAnswer2Title.toString());
		}

		requirementsHeaderTitlesModel.setQuestionnaireId(createdStdQuestionaire);
		requirementsHeaderTitlesRepository.save(requirementsHeaderTitlesModel);

		StdSectionModel stdSection;
		StdSectionModel parent = new StdSectionModel();
		StdSectionModel stdSubSection = new StdSectionModel();

		for (int i = 10; i < worksheet.getPhysicalNumberOfRows() - 10; i++) {

			XSSFRow row = worksheet.getRow(i);

			try {
				// Section Subject SubSection SubSubject SubSection SubSubject Description ...
				// Requirements...
				// Answer Types...
				Cell section, subject, subSection, subSubject,
					title, requirement, requirementSubject, description, articleNumber, explanation,
					answerType, answerType1, answerType2;
				section = row.getCell(1);
				subject = row.getCell(2);
				subSection = row.getCell(3);
				subSubject = row.getCell(4);
				title = row.getCell(5);
				requirement = row.getCell(6);
				requirementSubject = row.getCell(7);
				description = row.getCell(8);
				explanation = row.getCell(9);
				articleNumber = row.getCell(10);
				answerType1 = row.getCell(11);
				answerType2 = row.getCell(13);
				answerType = row.getCell(15);

				String parentString = convertCellToString(workbook, section) + " "
					+ convertCellToString(workbook, subject);
				String sectionString = convertCellToString(workbook, subSection) + " "
					+ convertCellToString(workbook, subSubject);

				// Generic Answers Handling...
				// Answer Type
				QtrQstTypeEnum qtrQstType = null;
				QtrQstTypeEnum genericAnswer1 = null;
				QtrQstTypeEnum genericAnswer2 = null;

				if (!answerType.getStringCellValue().equals(""))
					qtrQstType = QtrQstTypeEnum.valueOf(answerType.getStringCellValue().toUpperCase());

				// Answer Type 1
				if (!answerType1.getStringCellValue().equals(""))
					genericAnswer1 = QtrQstTypeEnum.valueOf(answerType1.getStringCellValue().toUpperCase());

				// Answer Type 2
				if (!answerType2.getStringCellValue().equals(""))
					genericAnswer2 = QtrQstTypeEnum.valueOf(answerType2.getStringCellValue().toUpperCase());

				// Section Handling
				if (section != null && subject != null) {

					// parent
					if (parent.getStdSctId() == 0
						|| (parent.getStdSctName() != null && !parent.getStdSctName().equals(parentString))) {
						stdSection = new StdSectionModel();
						stdSection.setOrder(0);
						stdSection.setStdQtrId(createdStdQuestionaire);
						stdSection.setStdSctName(parentString);
						try {
							parent = stdSectionService.create(stdSection);
						} catch (Exception ex) {
							LOGGER.info(ex.getMessage());
						}
					}

					if (subSection != null && subSubject != null) {
						// subSection
						if (stdSubSection.getStdSctId() == 0 || (stdSubSection.getStdSctName() != null
							&& !stdSubSection.getStdSctName().equals(sectionString))) {
							stdSection = new StdSectionModel();
							stdSection.setOrder(1);
							stdSection.setStdQtrId(createdStdQuestionaire);
							stdSection.setStdSctName(sectionString);
							stdSection.setParent(parent);
							try {
								stdSubSection = stdSectionService.create(stdSection);
							} catch (Exception ex) {
								LOGGER.info(ex.getMessage());
							}

						}

						// Converting From Cells to Primitives
						if (requirement != null && requirementSubject != null && title != null && answerType != null && !requirement.getStringCellValue().equals("") && !requirementSubject.getStringCellValue().equals("")) {

							String reqName = convertCellToString(workbook, requirement) + " "
								+ convertCellToString(workbook, requirementSubject);

							String reqTitle = convertCellToString(workbook, title);
							String reqDescription = convertCellToString(workbook, description);
							String reqArticleNumber = convertCellToString(workbook, articleNumber);
							String reqExplanation = convertCellToString(workbook, explanation);

							createReq(reqTitle, reqName, reqDescription, reqArticleNumber, reqExplanation, qtrQstType, genericAnswer1, genericAnswer2, createdStandard, stdSubSection, file);
						}
					}
				}
			} catch (Exception ex) {
				LOGGER.error("Error in file: " + file.getName() + " Line: " + i + " , exception: " + ex);
			}
		}
		return createdStandard;
	}

	private void createReq(String title, String reqName, String description, String articleNumber, String explanation,
						   QtrQstTypeEnum answerType, QtrQstTypeEnum answerType1, QtrQstTypeEnum answerType2,
						   StandardModel standardModel, StdSectionModel sectionModel, MultipartFile file) throws IOException {

		XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());
		XSSFSheet dropdownWorksheet = workbook.getSheetAt(2);


		StdRequirementsModel req = new StdRequirementsModel();
		req.setStdId(standardModel);
		req.setStdRqrDescription(description);
		req.setStdRqrTitle(title);
		req.setStdRqrExplanation(explanation);
		req.setStdRqrName(reqName);
		req.setArticleNum(articleNumber);

		// Answers
		req.setQtrQstType(answerType);
		req.setAnswerDate1Type(answerType1);
		req.setAnswerDate2Type(answerType2);


		// Setting Dropdowns
		Cell dropdownTitle, type1Value, type1Label, type2Value, type2Label, type3Value, type3Label;

		// Creating Options Lists
		List<StdQstOptionModel> stdQstOptionModelAsList = new ArrayList<>(Collections.emptyList());
		List<StdAnswer1OptionModel> stdAnswer1OptionModelAsList = new ArrayList<>(Collections.emptyList());
		List<StdAnswer2OptionModel> stdAnswer2OptionModelAsList = new ArrayList<>(Collections.emptyList());

		// Option
		if (answerType != null) {
			if (answerType.equals(QtrQstTypeEnum.DROPDOWN)) {
				for (int i = 8; i < dropdownWorksheet.getPhysicalNumberOfRows() - 25; i++) {

					XSSFRow row = dropdownWorksheet.getRow(i);

					dropdownTitle = row.getCell(1);

					if (dropdownTitle.toString().equals(title)) {

						type1Value = row.getCell(2);
						type1Label = row.getCell(3);

						StdQstOptionModel stdQstOptionModel = new StdQstOptionModel();
						stdQstOptionModel.setSqoValue(type1Value.toString());
						stdQstOptionModel.setSqoLabel(type1Label.toString());
						stdQstOptionModelAsList.add(stdQstOptionModel);
					}
				}
			}
		}

		// Options 1
		if (answerType1 != null) {
			if (answerType1.equals(QtrQstTypeEnum.DROPDOWN)) {
				for (int i = 8; i < dropdownWorksheet.getPhysicalNumberOfRows() - 25; i++) {

					XSSFRow row = dropdownWorksheet.getRow(i);

					dropdownTitle = row.getCell(1);

					if (dropdownTitle.toString().equals(title)) {

						type2Value = row.getCell(4);
						type2Label = row.getCell(5);

						StdAnswer1OptionModel stdAnswer1OptionModel = new StdAnswer1OptionModel();
						stdAnswer1OptionModel.setsA1Label(type2Label.toString());
						stdAnswer1OptionModel.setsA1Value(type2Value.toString());
						stdAnswer1OptionModelAsList.add(stdAnswer1OptionModel);
					}
				}
			}
		}

		// Options 2
		if (answerType2 != null) {
			if (answerType2.equals(QtrQstTypeEnum.DROPDOWN)) {
				for (int i = 8; i < dropdownWorksheet.getPhysicalNumberOfRows() - 25; i++) {

					XSSFRow row = dropdownWorksheet.getRow(i);

					dropdownTitle = row.getCell(1);

					if (dropdownTitle.toString().equals(title)) {

						type3Value = row.getCell(6);
						type3Label = row.getCell(7);

						StdAnswer2OptionModel stdAnswer2OptionModel = new StdAnswer2OptionModel();
						stdAnswer2OptionModel.setsA2Label(type3Label.toString());
						stdAnswer2OptionModel.setsA2Value(type3Value.toString());
						stdAnswer2OptionModelAsList.add(stdAnswer2OptionModel);
					}
				}
			}
		}

		if (stdQstOptionModelAsList.size() > 0) {
			req.setStdQstOptionModels(stdQstOptionModelAsList);
		}
		if (stdAnswer1OptionModelAsList.size() > 0) {
			req.setStdAnswer1OptionModels(stdAnswer1OptionModelAsList);
		}
		if (stdAnswer2OptionModelAsList.size() > 0) {
			req.setStdAnswer2OptionModels(stdAnswer2OptionModelAsList);
		}

		req.setSectionId(sectionModel.getStdSctId());
		try {
			StdRequirementsModel newRequirementsModel = stdRequirementsService.create(req);
			newRequirementsModel.setStdQtrQuestionsModel(
				requestQuestionnaireService.createStdQtrQuestionsModel(newRequirementsModel, sectionModel, file));
		} catch (Exception ex) {
			LOGGER.info(ex.getMessage());
		}

	}

	private String convertCellToString(XSSFWorkbook workbook, Cell cell) {
		String value = "";
		FormulaEvaluator formulaEvaluator = workbook.getCreationHelper().createFormulaEvaluator();
		switch (formulaEvaluator.evaluateInCell(cell).getCellTypeEnum()) {
			case NUMERIC:
				value = String.valueOf(cell.getNumericCellValue());
				break;
			case STRING:
				value = cell.getStringCellValue();
				break;
			case BOOLEAN:
				if (cell.getBooleanCellValue()) {
					value = "true";
				} else {
					value = "false";
				}
			default:
				break;

		}
		return value;
	}

	public StandardProgrssModel getStandardProgrss(int questionnairesId, String token) {
		CompanyModel companyModel = authenticationService.getCompanyFromToken(token.substring(7));
		StandardProgrssModel model = new StandardProgrssModel();
		int sum = 0;
		List<StdSectionModel> sections = stdSectionService.getAllSectionByQuestionaireId(questionnairesId);
		if (sections != null && sections.size() > 0) {
			for (StdSectionModel section : sections) {
				List<StdQtrQuestionsModel> questions = stdQtrQuestionsService
					.getAllQuestionBySectionId(section.getStdSctId());
				if (questions != null && questions.size() > 0) {
					for (StdQtrQuestionsModel question : questions) {
						List<CustomerStandardModel> customerStandards = customerStandardService
							.getCustomerStandardsByQuestionsId(question.getQtrQstId(), companyModel.getCmpId());
						for (CustomerStandardModel customerStandard : customerStandards) {
							sum++;
							if (customerStandard.getCompliant() != null) {
								switch (customerStandard.getCompliant()) {
									case YES:
										model.setApplicable(model.getApplicable() + 1);
										break;
									case NO:
										model.setNotImplemented(model.getNotImplemented() + 1);
										break;
									case NA:
										model.setNotImplemented(model.getNotImplemented() + 1);
										break;
									case NOTAPP:
										model.setNotApplicable(model.getNotApplicable() + 1);
										break;
									default:
										break;
								}
							} else
								model.setNotImplemented(model.getNotImplemented() + 1);
						}
					}
				}
			}
		}
		return model;
	}

	public List<FilterModel> getStandardListOption(String token) {

		List<LicenseStandardModel> licenseStandardModel = getStandardsByToken(token);
		List<Integer> standardModels = new ArrayList<>();
		for (LicenseStandardModel tmpLicenseStandardModel : licenseStandardModel) {
			if (!standardModels.contains(tmpLicenseStandardModel.getStdId().getStdId()))
				standardModels.add(tmpLicenseStandardModel.getStdId().getStdId());
		}
		return standardRepository.getStandardListOption(standardModels);

	}

	public StandardModel getStdOnPrem(int id) {
		return standardRepository.getStdIdOnPrem(id);
	}

	public List<StdRequirementsModel> getReqByStdId(int id) {
		LOGGER.info("get StdQuestionnaireModels by std id: " + id + ".");
		return stdRequirementsService.getReqByStdId(id);
	}
}
