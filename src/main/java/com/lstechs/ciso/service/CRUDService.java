package com.lstechs.ciso.service;

import com.lstechs.ciso.enums.ModuleEnum;
import com.lstechs.ciso.enums.OperationEnum;
import com.lstechs.ciso.enums.UsersEnum;
import com.lstechs.ciso.model.*;
import com.lstechs.ciso.repository.CRUDRepository;
import com.lstechs.ciso.repository.StdQtrQuestionsRepository;
import javassist.NotFoundException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.*;

@Primary
public abstract class CRUDService<T extends CRUDModel, P> implements ICRUDService<T, P> {


	public CRUDRepository<T> crudRepository;

	@Autowired
	private AuthenticationService authenticationService;

	@Autowired
	private StdQtrQuestionsRepository stdQtrQuestionsRepository;

	private static final Logger LOGGER = LogManager.getLogger(CRUDService.class.getName());

	public CRUDService(CRUDRepository<T> crudRepository) {
		this.crudRepository = crudRepository;
	}

	@Override
	public Page<T> getAll(Integer pageNo, Integer pageSize, String orderBy, boolean orderByDesc, String query) throws Exception {
		LOGGER.info("get all from crud service, Repository: " + crudRepository.getClass().getSimpleName());
		Sort sort = Sort.by(Sort.Order.desc(orderBy));
		if (!orderByDesc) {
			sort = Sort.by(Sort.Order.asc(orderBy));
		}
		Pageable paging = PageRequest.of(pageNo, pageSize, sort);
		return crudRepository.findAll(paging, query.toLowerCase());
	}

	public List<T> getAll() {
		LOGGER.info("get all from crud service, Repository: " + crudRepository.getClass().getSimpleName());
		return crudRepository.findAll();
	}

	@Override
	public T getById(int id) throws Exception {
		LOGGER.info("get by id: " + id + " from crud service, Repository: " + crudRepository.getClass().getSimpleName());
		return crudRepository.findById(id).orElseThrow(() -> new NotFoundException("Id: " + id + " not found"));
	}

	@Override
	public T create(T t) throws Exception {
		LOGGER.info("create from crud service, Repository: " + crudRepository.getClass().getSimpleName());
		return crudRepository.save(t);
	}

	public StdQtrQuestionsModel createQuestionnaire(StdQtrQuestionsModel t) throws Exception {
		LOGGER.info("create from crud service, Repository: " + crudRepository.getClass().getSimpleName());
		try {
			return stdQtrQuestionsRepository.save(t);
		} catch (Exception ex) {
			LOGGER.error(ex.getMessage());
		}

		return null;
	}

	@Override
	public T update(int id, T t) throws Exception {
		LOGGER.info("update id: " + id + " from crud service, Repository: " + crudRepository.getClass().getSimpleName());
		getById(id);
		return crudRepository.save(t);
	}

	@Override
	public T delete(int id) throws Exception {
		LOGGER.info("delete id: " + id + " from crud service, Repository: " + crudRepository.getClass().getSimpleName());
		T t = getById(id);
		t.setDeletedAt(new Date(System.currentTimeMillis()));
		return crudRepository.save(t);
	}


	public List<T> getSyncList(Date updatedAt) {
		LOGGER.info("get sync list with updatedAt: " + updatedAt + " from crud service, Repository: " + crudRepository.getClass().getSimpleName());
		Sort sort = Sort.by(Sort.Order.desc("createdAt"));
		Pageable paging = PageRequest.of(0, 1000, sort);
		return crudRepository.syncList(paging, updatedAt).getContent();
	}

	public Date lastUpdateAt() {
		LOGGER.info("get last updatedAt from crud service, Repository: " + crudRepository.getClass().getSimpleName());
		return crudRepository.lastUpdateAt();
	}

	public List<T> setSyncList(T[] arr) {
		List<T> retList = new ArrayList<>();
		LOGGER.info("get sync list from crud service, Repository: " + crudRepository.getClass().getSimpleName());
		for (T t : arr) {
			try {
				retList.add(create(t));
			} catch (Exception ex) {
				LOGGER.error("error cannot create row", ex);
			}
		}
		return retList;
	}

	public List<StdQtrQuestionsModel> saveQuestionnaires(StdQtrQuestionsModel[] arr) {
		List<StdQtrQuestionsModel> retList = new ArrayList<>();
		LOGGER.info("get sync list from crud service, Repository: " + crudRepository.getClass().getSimpleName());
		for (StdQtrQuestionsModel stdQtrQuestionsModel : arr) {
			try {
				stdQtrQuestionsModel.setStdQstOptionModel(null);
				stdQtrQuestionsModel.setStdAnswer1OptionModel(null);
				stdQtrQuestionsModel.setStdAnswer2OptionModel(null);
				stdQtrQuestionsModel.setQtrQstId(0);
				retList.add(createQuestionnaire(stdQtrQuestionsModel));
			} catch (Exception ex) {
				LOGGER.error("error cannot create row", ex);
			}
		}
		return retList;
	}


	public void handlePermission(String token, OperationEnum operationEnum, ModuleEnum moduleEnum) {
		if (!hasPermission(token.substring(7), operationEnum, moduleEnum)) throw new SecurityException("Forbidden");
	}

	public boolean hasPermission(String token, OperationEnum operationEnum, ModuleEnum moduleEnum) {
		UsersEnum usersEnum = authenticationService.getUserType(token);
		if (usersEnum.equals(UsersEnum.CompanyUser)) {
			CompanyUserModel companyUserModel = authenticationService.getCompanyUserFromToken(token);
			List<PermissionModel> permissionModelList = companyUserModel.getCmpUsrRoleId().getPermissionModels();
			Optional<PermissionModel> matchingObject = permissionModelList.stream().filter(p -> p.getModule().equals(moduleEnum)).findFirst();
			PermissionModel permissionModel = matchingObject.orElse(null);
			if (permissionModel == null) return false;
			switch (operationEnum) {
				case CREATE:
					return permissionModel.getIsCreate();
				case READ:
					return permissionModel.getIsRead();
				case UPDATE:
					return permissionModel.getIsUpdate();
				case DELETE:
					return permissionModel.getIsDelete();
				default:
					return false;
			}
		} else if (usersEnum.equals(UsersEnum.VendorUser)) {
			VendorUserModel vendorUserModel = authenticationService.getVendorUserFromToken(token);
			return vendorUserModel != null;
		}
		return false;
	}

	public Map<String, Object> getAllResponse(Page<T> t) {
		Map<String, Object> map = new HashMap();

		if (t != null && t.hasContent()) {
			map.put("data", t.getContent());
			map.put("page", t.getNumber());
			map.put("totalPage", t.getTotalPages());
			if (t.getTotalPages() == 1 && t.getNumber() == 0) map.put("pagesLeft", 0);
			else map.put("pagesLeft", t.getTotalPages() - t.getNumber() - 1);
		}
		return map;
	}

	public P getSingleInstance(T t) throws Exception {
		LOGGER.info("get single instance.");
		P p = null;
		return p;
	}
}
