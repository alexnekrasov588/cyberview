package com.lstechs.ciso.service;

import com.lstechs.ciso.enums.PrjStatusEnum;
import com.lstechs.ciso.enums.StatusEnum;
import com.lstechs.ciso.model.*;
import com.lstechs.ciso.repository.ProjectRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class ProjectService extends CRUDService<ProjectModel, SingleProjectModel> {

	private static final Logger LOGGER = LogManager.getLogger(ProjectService.class.getName());

	@Autowired
	private ProjectRepository projectRepository;

	@Autowired
	private TaskService taskService;

	@Autowired
	private RecurringTaskService recurringTaskService;

	@Autowired
	private TaskCommentService taskCommentService;

	@Autowired
	private AuthenticationService authenticationService;

	@Autowired
	private CompanyUserService companyUserService;

	@Autowired
	private RoleService roleService;

	public ProjectService(ProjectRepository projectRepository) {
		super(projectRepository);
	}

	@Override
	public SingleProjectModel getSingleInstance(ProjectModel projectModel) throws Exception {
		SingleCompanyUserModel singleCompanyUserModel = companyUserService
			.getSingleCompanyUSerModelById(projectModel.getPrjManagerId().getCmpUsrId());
		return new SingleProjectModel(projectModel, singleCompanyUserModel);
	}

	public List<ProjectsNameModel> getAllProjectsName(String token) {
		LOGGER.info("get all projects name project .");
		CompanyUserModel companyUserModel = authenticationService.getCompanyUserFromToken(token.substring(7));
		RoleModel roleModel = roleService.getRoleModelByCmpUsrId(companyUserModel.getCmpUsrId());
		String userRoleName = roleModel.getRoleName();
		boolean isTeamMember = (userRoleName.equals("Team Member")) || (userRoleName.equals("Outsource"));
		int cmpId = companyUserModel.getCmpUsrCmpId().getCmpId();
		int cmpUsrId = companyUserModel.getCmpUsrId();
		return projectRepository.getAllProjectsName(cmpId, isTeamMember, cmpUsrId);
	}

	public List<String> getAllProjectsManager(String token) {
		LOGGER.info("get all projects manager project.");
		CompanyUserModel companyUserModel = authenticationService.getCompanyUserFromToken(token.substring(7));
		RoleModel roleModel = roleService.getRoleModelByCmpUsrId(companyUserModel.getCmpUsrId());
		String userRoleName = roleModel.getRoleName();
		boolean isTeamMember = (userRoleName.equals("Team Member")) || (userRoleName.equals("Outsource"));
		int cmpId = companyUserModel.getCmpUsrCmpId().getCmpId();
		int cmpUsrId = companyUserModel.getCmpUsrId();
		return projectRepository.getAllProjectsManagers(cmpId, isTeamMember, cmpUsrId);
	}

	/**
	 * Returns all company active projects
	 * This function return List of active projects
	 * For Security news, CVE and risk assessment modules
	 *
	 * @param cmpId company Id
	 * @return List of projects
	 */
	public List<ProjectModel> getNoFilterProjects(int cmpId) {
		return projectRepository.getAllActiveProjectsByCmpId(cmpId);
	}

	public Page<ProjectModel> getFilterProjects(Integer pageNo, Integer pageSize, String orderBy, boolean orderByDesc,
												int cmpId, int cmpUsrId, boolean isTeamMember, Map<String, String> allParams, String query)
		throws ParseException {

		LOGGER.info("get projects by ids.");

		Sort sort = Sort.by(Sort.Order.desc(orderBy));
		if (!orderByDesc) {
			sort = Sort.by(Sort.Order.asc(orderBy));
		}

		if (orderBy.equals("prjProgressPercent") || orderBy.equals("Schedule")) {
			sort = Sort.by(Sort.Order.desc("prjId"));
		}

		Pageable paging = PageRequest.of(pageNo, pageSize, sort);

		List<Integer> prjName = null;
		Date[] prjStartDate = null;
		Date[] prjEndDate = null;
		List<PrjStatusEnum> prjStatus = null;
		String schedule = null; // 0 or 1
		List<String> prjManager = null;
		Date prjStartDateFrom = null;
		Date prjStartDateTo = null;
		Date prjEndDateFrom = null;
		Date prjEndDateTo = null;
		boolean isStartDateExist = false;
		boolean isEndDateExist = false;

		for (Map.Entry<String, String> entry : allParams.entrySet()) {

			String k = entry.getKey();
			String v = entry.getValue();
			String[] array = v.split(",");

			if (k.equals("prjName")) {
				Integer[] tmp = convertIntsToInteger(Arrays.stream(array).mapToInt(Integer::parseInt).toArray());
				prjName = Arrays.asList(tmp);
			}
			if (k.equals("prjStatus")) {
				prjStatus = Arrays.asList(convertToPrjStatusArray(array));
			}
			if (k.equals("prjStartDate")) {
				prjStartDate = convertStringsToDates(array);
			}
			if (k.equals("prjEndDate")) {
				prjEndDate = convertStringsToDates(array);
			}
			if (k.equals("Schedule")) {
				schedule = array[0];
			}
			if (k.equals("prjManagerId.cmpUsrFullName")) {
				prjManager = Arrays.asList(array);
			}
		}

		if (prjEndDate != null) {
			prjEndDateFrom = prjEndDate[0];
			prjEndDateTo = prjEndDate[1];
			isEndDateExist = true;

		}
		if (prjStartDate != null) {
			prjStartDateFrom = prjStartDate[0];
			prjStartDateTo = prjStartDate[1];
			isStartDateExist = true;
		}

		if (orderBy.equals("prjProgressPercent")) {

			List<ProjectModel> projectList = projectRepository.getFilterProjectsList(cmpId, prjName, prjStatus,
				prjStartDateFrom, prjStartDateTo, prjEndDateFrom, prjEndDateTo, schedule, prjManager,
				isEndDateExist, isStartDateExist, isTeamMember, cmpUsrId, query);

			List<ProjectModel> sorted;

			if (!orderByDesc) {
				sorted = projectList.stream().sorted(Comparator.comparing(ProjectModel::getPrjProgressPercent))
					.collect(Collectors.toList());
			} else {
				sorted = projectList.stream()
					.sorted(Comparator.comparing(ProjectModel::getPrjProgressPercent).reversed())
					.collect(Collectors.toList());
			}


			int start = (int) paging.getOffset();
			int end = (start + paging.getPageSize()) > sorted.size() ? sorted.size() : (start + paging.getPageSize());
			Page<ProjectModel> pages = new PageImpl<>(sorted.subList(start, end), paging, sorted.size());
			return pages;

		} else if (orderBy.equals("Schedule")) {
			List<ProjectModel> projectList = projectRepository.getFilterProjectsList(cmpId, prjName, prjStatus,
				prjStartDateFrom, prjStartDateTo, prjEndDateFrom, prjEndDateTo, schedule, prjManager,
				isEndDateExist, isStartDateExist, isTeamMember, cmpUsrId, query);

			List<ProjectModel> onSchedule = new ArrayList<>();
			List<ProjectModel> offSchedule = new ArrayList<>();

			for (ProjectModel proj : projectList) {
				if (!proj.getPrjStatus().equals(PrjStatusEnum.COMPLETED) && proj.getPrjEndDate().before(new Date())) {
					offSchedule.add(proj);
				} else onSchedule.add(proj);
			}

			List<ProjectModel> sorted;

			if (!orderByDesc) {
				sorted = Stream.concat(onSchedule.stream(), offSchedule.stream())
					.collect(Collectors.toList());
			} else {
				sorted = Stream.concat(offSchedule.stream(), onSchedule.stream())
					.collect(Collectors.toList());
			}

			int start = (int) paging.getOffset();
			int end = (start + paging.getPageSize()) > sorted.size() ? sorted.size() : (start + paging.getPageSize());
			return new PageImpl<>(sorted.subList(start, end), paging, sorted.size());


		}

		return projectRepository.getFilterProjects(paging, cmpId, prjName, prjStatus,
			prjStartDateFrom, prjStartDateTo, prjEndDateFrom, prjEndDateTo, schedule, prjManager, isEndDateExist,
			isStartDateExist, isTeamMember, cmpUsrId, query);


	}

	private Integer[] convertIntsToInteger(int[] intArr) {
		Integer[] newArray = new Integer[intArr.length];
		int i = 0;
		for (int value : intArr) {
			newArray[i++] = Integer.valueOf(value);
		}
		return newArray;
	}

	private Date[] convertStringsToDates(String[] str) throws ParseException {
		Date[] dates = new Date[str.length];

		for (int i = 0; i < str.length; i++) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			dates[i] = sdf.parse(str[i]);

		}
		return dates;
	}

	private PrjStatusEnum[] convertToPrjStatusArray(String[] statusList) {
		List<PrjStatusEnum> prjStatusEnums = new ArrayList<>();
		for (String status : statusList) {

			switch (status) {
				case "COMPLETED":
					prjStatusEnums.add(PrjStatusEnum.COMPLETED);
					break;
				case "INITIATED":
					prjStatusEnums.add(PrjStatusEnum.INITIATED);
					break;
				case "IN_PROGRESS":
					prjStatusEnums.add(PrjStatusEnum.IN_PROGRESS);
					break;

				case "ON_HOLD":
					prjStatusEnums.add(PrjStatusEnum.ON_HOLD);
					break;

				case "PLANNED":
					prjStatusEnums.add(PrjStatusEnum.PLANNED);
					break;
			}

		}

		return prjStatusEnums.stream().toArray(PrjStatusEnum[]::new);
	}

	public void updateProjectStatus(int id, PrjStatusEnum prjStatusEnum) throws Exception {
		ProjectModel updateProjectModel = getById(id);
		updateProjectModel.setPrjStatus(prjStatusEnum);
		projectRepository.save(updateProjectModel);
	}

	@Override
	@CacheEvict(value = {"getListMapProjectsDetails", "projectOverview"}, allEntries = true)
	public ProjectModel create(ProjectModel projectModel) throws Exception {

		try {
			return super.create(projectModel);
		} catch (Exception ex) {
			LOGGER.error("create project faild , ex: " + ex);
			throw new Exception(ex);
		}

	}

	@Override
	@CacheEvict(value = {"getListMapProjectsDetails", "projectOverview"}, allEntries = true)
	public ProjectModel delete(int id) throws Exception {

		try {
			List<TaskModel> tasks = taskService.getAllTasksProject(id);

			if (!tasks.isEmpty()) {

			/*
               Deleting the recurring task
			 */
				for (TaskModel task : tasks) {
					RecurringTaskModel recurringTasks = recurringTaskService.findRecurringTaskByTaskId(task.getTskId());

					if (recurringTasks != null) {
						recurringTaskService.delete(recurringTasks.getRecId());
					}
				}

			/*
               Deleting the projects tasks
			 */
				for (TaskModel task : tasks) {
					taskService.delete(task.getTskId());
				}
			}
			return super.delete(id);
		} catch (Exception ex) {
			LOGGER.error("delete project faild , ex: " + ex);
			throw new Exception(ex);
		}

	}

	@Override
	@CacheEvict(value = {"getListMapProjectsDetails", "projectOverview"}, allEntries = true)
	public ProjectModel update(int id, ProjectModel projectModel) throws Exception {
		LOGGER.info("Update project , id: " + id + ".");
		ProjectModel updateProjectModel = getById(id);
		PrjStatusEnum prjStatusEnum = updateProjectModel.getPrjStatus();
		updateProjectModel.setPrjName(projectModel.getPrjName());
		updateProjectModel.setPrjDescription(projectModel.getPrjDescription());
		updateProjectModel.setPrjCmpId(projectModel.getPrjCmpId());
		updateProjectModel.setPrjManagerId(projectModel.getPrjManagerId());
		updateProjectModel.setPrjStartDate(projectModel.getPrjStartDate());
		updateProjectModel.setPrjEndDate(projectModel.getPrjEndDate());
		updateProjectModel.setPrjBudget(projectModel.getPrjBudget());
		updateProjectModel.setPrjStatus(projectModel.getPrjStatus());
		updateProjectModel.setPrjProgressPercent(getProjectProgress(projectModel.getPrjId()));
		ProjectModel retProject = projectRepository.save(updateProjectModel);
		if (!prjStatusEnum.equals(projectModel.getPrjStatus())) {
			List<TaskModel> taskModels = taskService.getAllTasksProject(projectModel.getPrjId());

			// If The Project Status Changed To Completed, All The Tasks Should Also Change To Completed.
			if (projectModel.getPrjStatus().equals(PrjStatusEnum.COMPLETED)){
				taskService.updateTasksStatusByProjectId(id, projectModel.getPrjCmpId().getCmpId(), StatusEnum.DONE);
			}

			// If The Project Status Changed To On Hold, All The Tasks Should Also Change To On Hold Except The Completed Tasks.
			else if (projectModel.getPrjStatus().equals(PrjStatusEnum.ON_HOLD)){
				taskService.updateTasksStatusToOnHoldByProjectId(id, projectModel.getPrjCmpId().getCmpId(), StatusEnum.ON_HOLD);
			}
		}
		return retProject;
	}

	@CacheEvict(value = {"getListMapProjectsDetails", "projectOverview"}, allEntries = true)
	public void updateProjectStatus(ProjectModel project, TaskModel taskModel) throws Exception {

		List<TaskModel> taskModels = taskService.getAllTasksProject(project.getPrjId());
		List<StatusEnum> statusEnums = taskModels.stream().map(t -> t.getStatus()).collect(Collectors.toList());
		Set<StatusEnum> s = new LinkedHashSet<>(statusEnums);
		if (statusEnums.size() == 1) {
			if (s.iterator().next().equals(StatusEnum.IN_PROGRESS))
				updateProjectStatus(project.getPrjId(), PrjStatusEnum.IN_PROGRESS);
			if (project.getPrjStatus().equals(PrjStatusEnum.COMPLETED) && s.iterator().next().equals(StatusEnum.DONE))
				updateProjectStatus(project.getPrjId(), PrjStatusEnum.COMPLETED);
			if (project.getPrjStatus().equals(PrjStatusEnum.ON_HOLD) && s.iterator().next().equals(StatusEnum.ON_HOLD))
				updateProjectStatus(project.getPrjId(), PrjStatusEnum.ON_HOLD);


			//In case we have 1 task and the task status changed to On Hold the project status will change to On Hold.
			if (project.getPrjStatus().equals(PrjStatusEnum.INITIATED) && s.iterator().next().equals(StatusEnum.ON_HOLD))
				updateProjectStatus(project.getPrjId(), PrjStatusEnum.ON_HOLD);
			if (project.getPrjStatus().equals(PrjStatusEnum.PLANNED) && s.iterator().next().equals(StatusEnum.ON_HOLD))
				updateProjectStatus(project.getPrjId(), PrjStatusEnum.ON_HOLD);
			if (project.getPrjStatus().equals(PrjStatusEnum.IN_PROGRESS) && s.iterator().next().equals(StatusEnum.ON_HOLD))
				updateProjectStatus(project.getPrjId(), PrjStatusEnum.ON_HOLD);
			if (project.getPrjStatus().equals(PrjStatusEnum.COMPLETED) && s.iterator().next().equals(StatusEnum.ON_HOLD))
				updateProjectStatus(project.getPrjId(), PrjStatusEnum.ON_HOLD);

			//In case we have 1 task and the task status changed to Completed the project status will change to Completed.
			if (project.getPrjStatus().equals(PrjStatusEnum.INITIATED) && s.iterator().next().equals(StatusEnum.DONE))
				updateProjectStatus(project.getPrjId(), PrjStatusEnum.COMPLETED);
			if (project.getPrjStatus().equals(PrjStatusEnum.PLANNED) && s.iterator().next().equals(StatusEnum.DONE))
				updateProjectStatus(project.getPrjId(), PrjStatusEnum.COMPLETED);
			if (project.getPrjStatus().equals(PrjStatusEnum.IN_PROGRESS) && s.iterator().next().equals(StatusEnum.DONE))
				updateProjectStatus(project.getPrjId(), PrjStatusEnum.COMPLETED);
			if (project.getPrjStatus().equals(PrjStatusEnum.ON_HOLD) && s.iterator().next().equals(StatusEnum.DONE))
				updateProjectStatus(project.getPrjId(), PrjStatusEnum.COMPLETED);

			//In case we have 1 task and the task status changed to Completed the project status will change to Completed.
			if (project.getPrjStatus().equals(PrjStatusEnum.ON_HOLD) && s.iterator().next().equals(StatusEnum.OPENED))
				updateProjectStatus(project.getPrjId(), PrjStatusEnum.PLANNED);
			if (project.getPrjStatus().equals(PrjStatusEnum.ON_HOLD) && s.iterator().next().equals(StatusEnum.IN_PROGRESS))
				updateProjectStatus(project.getPrjId(), PrjStatusEnum.IN_PROGRESS);
			if (project.getPrjStatus().equals(PrjStatusEnum.COMPLETED) && s.iterator().next().equals(StatusEnum.OPENED))
				updateProjectStatus(project.getPrjId(), PrjStatusEnum.INITIATED);
			if (project.getPrjStatus().equals(PrjStatusEnum.COMPLETED) && s.iterator().next().equals(StatusEnum.IN_PROGRESS))
				updateProjectStatus(project.getPrjId(), PrjStatusEnum.IN_PROGRESS);
			if (project.getPrjStatus().equals(PrjStatusEnum.IN_PROGRESS) && s.iterator().next().equals(StatusEnum.OPENED))
				updateProjectStatus(project.getPrjId(), PrjStatusEnum.INITIATED);

			//Previous logic
//			if (s.iterator().next().equals(StatusEnum.ON_HOLD))
//				updateProjectStatus(project.getPrjId(), PrjStatusEnum.ON_HOLD);
//			if (s.iterator().next().equals(StatusEnum.DONE))
//				updateProjectStatus(project.getPrjId(), PrjStatusEnum.COMPLETED);
//			if (s.iterator().next().equals(StatusEnum.OPENED))
//				updateProjectStatus(project.getPrjId(), PrjStatusEnum.INITIATED);
		} else {


			//In case all the tasks are have the same status
			//And the new task will
			if (statusEnums.contains(StatusEnum.OPENED) &&
				!statusEnums.contains(StatusEnum.IN_PROGRESS) &&
				!statusEnums.contains(StatusEnum.ON_HOLD) &&
				!statusEnums.contains(StatusEnum.DONE)) {
				updateProjectStatus(project.getPrjId(), project.getPrjStatus());
			} else if (statusEnums.contains(StatusEnum.IN_PROGRESS) &&
				!statusEnums.contains(StatusEnum.OPENED) &&
				!statusEnums.contains(StatusEnum.ON_HOLD) &&
				!statusEnums.contains(StatusEnum.DONE)) {
				updateProjectStatus(project.getPrjId(), PrjStatusEnum.IN_PROGRESS);
			} else if (statusEnums.contains(StatusEnum.ON_HOLD) &&
				!statusEnums.contains(StatusEnum.OPENED) &&
				!statusEnums.contains(StatusEnum.IN_PROGRESS) &&
				!statusEnums.contains(StatusEnum.DONE)) {
				updateProjectStatus(project.getPrjId(), PrjStatusEnum.ON_HOLD);
			} else if (statusEnums.contains(StatusEnum.DONE) &&
				!statusEnums.contains(StatusEnum.OPENED) &&
				!statusEnums.contains(StatusEnum.ON_HOLD) &&
				!statusEnums.contains(StatusEnum.IN_PROGRESS)) {
				updateProjectStatus(project.getPrjId(), PrjStatusEnum.COMPLETED);
			}

			/*
               In case that all the existing tasks are in Completed Status
			 */
			//And the new task status is on hold the project status will changed to On Hold
			else if (statusEnums.contains(StatusEnum.DONE) &&
				!statusEnums.contains(StatusEnum.OPENED) &&
				statusEnums.contains(StatusEnum.ON_HOLD) &&
				!statusEnums.contains(StatusEnum.IN_PROGRESS) &&
				statusEnums.get(statusEnums.size() - 1).equals(StatusEnum.ON_HOLD)
			) {
				updateProjectStatus(project.getPrjId(), PrjStatusEnum.ON_HOLD);
			}
			//And the new task status is In Progress the project status will changed to In Progress
			else if (statusEnums.contains(StatusEnum.DONE) &&
				!statusEnums.contains(StatusEnum.OPENED) &&
				!statusEnums.contains(StatusEnum.ON_HOLD) &&
				statusEnums.contains(StatusEnum.IN_PROGRESS) &&
				statusEnums.get(statusEnums.size() - 1).equals(StatusEnum.IN_PROGRESS)
			) {
				updateProjectStatus(project.getPrjId(), PrjStatusEnum.IN_PROGRESS);
			}
			//And the new task status is Opened the project status will changed to Initiated
			else if (statusEnums.contains(StatusEnum.DONE) &&
				statusEnums.contains(StatusEnum.OPENED) &&
				!statusEnums.contains(StatusEnum.ON_HOLD) &&
				!statusEnums.contains(StatusEnum.IN_PROGRESS) &&
				statusEnums.get(statusEnums.size() - 1).equals(StatusEnum.OPENED)
			) {
				updateProjectStatus(project.getPrjId(), PrjStatusEnum.INITIATED);
			}


			/*
               In case that all the existing tasks are in On Hold Status
			 */
			//And the new task status is Done the project status will changed to On Hold
			else if (statusEnums.contains(StatusEnum.ON_HOLD) &&
				!statusEnums.contains(StatusEnum.OPENED) &&
				statusEnums.contains(StatusEnum.DONE) &&
				!statusEnums.contains(StatusEnum.IN_PROGRESS) &&
				statusEnums.get(statusEnums.size() - 1).equals(StatusEnum.DONE)
			) {
				updateProjectStatus(project.getPrjId(), PrjStatusEnum.ON_HOLD);
			}
			//And the new task status is In Progress the project status will changed to In Progress
			else if (statusEnums.contains(StatusEnum.ON_HOLD) &&
				!statusEnums.contains(StatusEnum.OPENED) &&
				!statusEnums.contains(StatusEnum.DONE) &&
				statusEnums.contains(StatusEnum.IN_PROGRESS) &&
				statusEnums.get(statusEnums.size() - 1).equals(StatusEnum.IN_PROGRESS)
			) {
				updateProjectStatus(project.getPrjId(), PrjStatusEnum.IN_PROGRESS);
			}
			//And the new task status is Opened the project status will changed to On Hold
			else if (statusEnums.contains(StatusEnum.ON_HOLD) &&
				statusEnums.contains(StatusEnum.OPENED) &&
				!statusEnums.contains(StatusEnum.DONE) &&
				!statusEnums.contains(StatusEnum.IN_PROGRESS) &&
				statusEnums.get(statusEnums.size() - 1).equals(StatusEnum.OPENED)
			) {
				updateProjectStatus(project.getPrjId(), PrjStatusEnum.ON_HOLD);
			}

			/*
               In case that all the existing tasks are in Progress Status
			 */
			//And the new task status is Done the project status will changed to In Progress
			else if (statusEnums.contains(StatusEnum.IN_PROGRESS) &&
				!statusEnums.contains(StatusEnum.OPENED) &&
				statusEnums.contains(StatusEnum.DONE) &&
				!statusEnums.contains(StatusEnum.ON_HOLD) &&
				statusEnums.get(statusEnums.size() - 1).equals(StatusEnum.DONE)
			) {
				updateProjectStatus(project.getPrjId(), PrjStatusEnum.IN_PROGRESS);
			}
			//And the new task status is On Hold the project status will changed to In Progress
			else if (statusEnums.contains(StatusEnum.IN_PROGRESS) &&
				!statusEnums.contains(StatusEnum.OPENED) &&
				!statusEnums.contains(StatusEnum.DONE) &&
				statusEnums.contains(StatusEnum.ON_HOLD) &&
				statusEnums.get(statusEnums.size() - 1).equals(StatusEnum.ON_HOLD)
			) {
				updateProjectStatus(project.getPrjId(), PrjStatusEnum.IN_PROGRESS);
			}
			//And the new task status is Opened the project status will changed to In Progress
			else if (statusEnums.contains(StatusEnum.IN_PROGRESS) &&
				statusEnums.contains(StatusEnum.OPENED) &&
				!statusEnums.contains(StatusEnum.DONE) &&
				!statusEnums.contains(StatusEnum.ON_HOLD) &&
				statusEnums.get(statusEnums.size() - 1).equals(StatusEnum.OPENED)
			) {
				updateProjectStatus(project.getPrjId(), PrjStatusEnum.IN_PROGRESS);
			}

			/*
               In case that all the existing tasks are in Opened Status
			 */
			//And the new task status is Done the project status will changed to Initiated
			else if (statusEnums.contains(StatusEnum.OPENED) &&
				!statusEnums.contains(StatusEnum.IN_PROGRESS) &&
				statusEnums.contains(StatusEnum.DONE) &&
				!statusEnums.contains(StatusEnum.ON_HOLD) &&
				statusEnums.get(statusEnums.size() - 1).equals(StatusEnum.DONE)
			) {
				updateProjectStatus(project.getPrjId(), PrjStatusEnum.INITIATED);
			}
			//And the new task status is On Hold the project status will changed to Initiated
			else if (statusEnums.contains(StatusEnum.OPENED) &&
				!statusEnums.contains(StatusEnum.IN_PROGRESS) &&
				!statusEnums.contains(StatusEnum.DONE) &&
				statusEnums.contains(StatusEnum.ON_HOLD) &&
				statusEnums.get(statusEnums.size() - 1).equals(StatusEnum.ON_HOLD)
			) {
				updateProjectStatus(project.getPrjId(), PrjStatusEnum.INITIATED);
			}
			//And the new task status is In Progress the project status will changed to In Progress
			else if (statusEnums.contains(StatusEnum.OPENED) &&
				statusEnums.contains(StatusEnum.IN_PROGRESS) &&
				!statusEnums.contains(StatusEnum.DONE) &&
				!statusEnums.contains(StatusEnum.ON_HOLD) &&
				statusEnums.get(statusEnums.size() - 1).equals(StatusEnum.IN_PROGRESS)
			) {
				updateProjectStatus(project.getPrjId(), PrjStatusEnum.IN_PROGRESS);
			}

			else if (!statusEnums.contains(StatusEnum.IN_PROGRESS) &&
				statusEnums.contains(StatusEnum.OPENED) &&
				statusEnums.contains(StatusEnum.DONE) &&
				statusEnums.contains(StatusEnum.ON_HOLD) &&
				statusEnums.get(statusEnums.size() - 1).equals(StatusEnum.ON_HOLD)
			) {
				updateProjectStatus(project.getPrjId(), PrjStatusEnum.ON_HOLD);
			}

			//Previous logic
//			if ((project.getPrjStatus().equals(PrjStatusEnum.INITIATED)
//				|| project.getPrjStatus().equals(PrjStatusEnum.PLANNED)
//				|| project.getPrjStatus().equals(PrjStatusEnum.ON_HOLD)
//				|| project.getPrjStatus().equals(PrjStatusEnum.COMPLETED))
//				&& taskModel.getStatus().equals(StatusEnum.IN_PROGRESS)) {
//				updateProjectStatus(project.getPrjId(), PrjStatusEnum.IN_PROGRESS);
//			}
//			if ((project.getPrjStatus().equals(PrjStatusEnum.INITIATED)
//				|| project.getPrjStatus().equals(PrjStatusEnum.PLANNED)
//				|| project.getPrjStatus().equals(PrjStatusEnum.ON_HOLD)
//				|| project.getPrjStatus().equals(PrjStatusEnum.COMPLETED))
//				&& taskModel.getStatus().equals(StatusEnum.OPENED)) {
//				updateProjectStatus(project.getPrjId(), PrjStatusEnum.INITIATED);
//			}
//			if ((project.getPrjStatus().equals(PrjStatusEnum.INITIATED)
//				|| project.getPrjStatus().equals(PrjStatusEnum.PLANNED)
//				|| project.getPrjStatus().equals(PrjStatusEnum.IN_PROGRESS)
//				|| project.getPrjStatus().equals(PrjStatusEnum.COMPLETED))
//				&& taskModel.getStatus().equals(StatusEnum.ON_HOLD)) {
//				updateProjectStatus(project.getPrjId(), PrjStatusEnum.ON_HOLD);
//			}
		}
	}

	@Cacheable(value = "projectOverview")
	public List<ProjectOverviewModel> projectOverview(String token) {
		LOGGER.info("create project overview report.");

		List<ProjectOverviewModel> projectOverviewModels = new ArrayList<>();
		CompanyModel companyModel = authenticationService.getCompanyFromToken(token.substring(7));
		List<ProjectModel> projectModels = getAllActiveProjectsByCmpId(companyModel.getCmpId());
		for (ProjectModel tmpProjectModel : projectModels) {
			ProjectOverviewModel projectOverviewModel = new ProjectOverviewModel();
			projectOverviewModel.setProject(tmpProjectModel);
			int tasksCount = taskService.getAllTasksCountByProjId(tmpProjectModel.getPrjId());
			int completedTask = taskService.getAllDoneTasksCountByProjId(tmpProjectModel.getPrjId());
			projectOverviewModel.setCompletedTask(completedTask);
			projectOverviewModel.setTotalTask(tasksCount);
			if (tasksCount == 0)
				projectOverviewModel.setProgress(0);
			else
				projectOverviewModel.setProgress(round2D((double) completedTask / tasksCount * 100));
			projectOverviewModels.add(projectOverviewModel);
		}
		return projectOverviewModels;
	}

	public double getProjectProgress(int projId) {
		int tasksCount = taskService.getAllTasksCountByProjId(projId);
		int completedTask = taskService.getAllDoneTasksCountByProjId(projId);
		if (tasksCount == 0)
			return 0;
		return round2D((double) completedTask / tasksCount * 100);
	}

	private double round2D(double value) {
		long factor = (long) Math.pow(10, 2);
		value = value * factor;
		long tmp = Math.round(value);
		return (double) tmp / factor;
	}

	public List<ProjectDeadlinesModel> projectDeadlinesReport(String token) {
		LOGGER.info("create project deadlines report.");
		List<ProjectDeadlinesModel> projectDeadlinesModels = new ArrayList<>();
		CompanyModel companyModel = authenticationService.getCompanyFromToken(token.substring(7));
		List<ProjectModel> projectModels = getAllActiveProjectsByCmpId(companyModel.getCmpId());

		for (ProjectModel tmpProjectModel : projectModels) {
			ProjectDeadlinesModel projectDeadlinesModel = new ProjectDeadlinesModel();
			projectDeadlinesModel.setProjectModel(tmpProjectModel);
			long getDifferenceDays = getDifferenceDays(tmpProjectModel.getPrjEndDate(),
				new Date(System.currentTimeMillis()));
			projectDeadlinesModel.setDelayTime(getDifferenceDays + "d");
			projectDeadlinesModels.add(projectDeadlinesModel);
		}
		return projectDeadlinesModels;
	}

	private static long getDifferenceDays(Date d1, Date d2) {
		long diff = d2.getTime() - d1.getTime();
		return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
	}

	public Page<ProjectModel> getAllProjectsByTskAssignee(Integer pageNo, Integer pageSize, String orderBy,
														  boolean orderByDesc, int assignee, String query) {
		LOGGER.info("Get all Projects by assignee id: " + assignee);
		if (orderBy.equals("prjProgressPercent"))
			return getAllProjectsListByTskAssigneeOrderByProgress(pageNo, pageSize, orderByDesc, assignee, query);

		if (orderBy.equals("prjEndDate"))
			orderBy = "prj_end_date";
		if (orderBy.equals("prjStartDate"))
			orderBy = "prj_start_date";
		if (orderBy.equals("prjStatus"))
			orderBy = "prj_status";
		if (orderBy.equals("prjId"))
			orderBy = "prj_id";
		if (orderBy.equals("prjName"))
			orderBy = "prj_name";

		Sort sort = Sort.by(Sort.Order.desc(orderBy));
		if (!orderByDesc) {
			sort = Sort.by(Sort.Order.asc(orderBy));
		}
		Pageable paging = PageRequest.of(pageNo, pageSize, sort);
		return projectRepository.getAllProjectsByTskAssignee(paging, assignee, query);
	}

	public List<ProjectModel> getAllProjectsListByTskAssignee(int assignee) {
		LOGGER.info("Get all Projects list by assignee id: " + assignee);
		return projectRepository.getAllProjectsListByTskAssignee(assignee);
	}

	public ProjectModel getProjectById(int prjId, int cmpId) {
		return projectRepository.getProjectById(prjId, cmpId);
	}

	public Page<ProjectModel> getAllProjectsListByTskAssigneeOrderByProgress(Integer pageNo, Integer pageSize,
																			 boolean orderByDesc, int assignee, String query) {
		LOGGER.info("Get all Projects list order by progress asc, by assignee id: " + assignee);
		Pageable paging = PageRequest.of(pageNo, pageSize);

		if (orderByDesc)
			return projectRepository.getAllProjectsListByTskAssigneeOrderByProgressDesc(paging, assignee, query);
		else
			return projectRepository.getAllProjectsListByTskAssigneeOrderByProgressAsc(paging, assignee, query);
	}

	public Page<ProjectModel> getAllProjectsByCompanyId(Integer pageNo, Integer pageSize, String orderBy,
														boolean orderByDesc, int cmpId, String query) {
		LOGGER.info("Get all Projects by Company id: " + cmpId);
		Sort sort = Sort.by(Sort.Order.desc(orderBy));
		if (!orderByDesc) {
			sort = Sort.by(Sort.Order.asc(orderBy));
		}
		Pageable paging = PageRequest.of(pageNo, pageSize, sort);

		return projectRepository.getAllProjectsByCompanyId(paging, cmpId, query);
	}

	public Page<ProjectModel> getAllProjectsByCompanyIdOrderByProgress(Integer pageNo, Integer pageSize,
																	   boolean orderByDesc, int cmpId, String query) {
		LOGGER.info("Get all Projects order by progress by Company id: " + cmpId);

		Pageable paging = PageRequest.of(pageNo, pageSize);
		if (orderByDesc)
			return projectRepository.getAllProjectsByCompanyIdOrderByProgressDesc(paging, cmpId);

		return projectRepository.getAllProjectsByCompanyIdOrderByProgressAsc(paging, cmpId);
	}

	public List<ProjectModel> getAllActiveProjectsByCmpId(int cmpId) {
		return projectRepository.getAllActiveProjectsByCmpId(cmpId);
	}

	public ByteArrayInputStream getProjectsReport(int cmpId, ProjectsReportModel projectsReportModel) throws Exception {
		String[] columns = projectsReportModel.getProjectsReportTitle();
		try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream()) {
			CreationHelper createHelper = workbook.getCreationHelper();
			Sheet sheet = workbook.createSheet("Projects");

			Font headerFont = workbook.createFont();
			headerFont.setBold(true);
			headerFont.setColor(IndexedColors.BLACK.getIndex());

			Font boldFont = workbook.createFont();
			boldFont.setBold(true);
			boldFont.setColor(IndexedColors.BLACK.getIndex());
			boldFont.setFontName("Arial");

			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			headerCellStyle.setWrapText(true);
			headerCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
			headerCellStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
			headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

			int rowIndex = 0;

			// Row for Header
			Row headerRow = sheet.createRow(rowIndex);

			// Header
			for (int col = 0; col < columns.length; col++) {
				Cell cell = headerRow.createCell(col);
				cell.setCellValue(columns[col]);
				cell.setCellStyle(headerCellStyle);
			}

			boolean isEndDateExist = projectsReportModel.getEndDate() != null;
			boolean isStartDateExist = projectsReportModel.getStartDate() != null;

			List<ProjectModel> projectModels = projectRepository.getProjectReport(cmpId, isEndDateExist,
				isStartDateExist, projectsReportModel.getStartDate(), projectsReportModel.getEndDate());

			CellStyle regularCellStyle = workbook.createCellStyle();
			regularCellStyle.setWrapText(true);
			regularCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);

			CellStyle redCellStyle = workbook.createCellStyle();
			redCellStyle.setWrapText(true);
			redCellStyle.setFont(boldFont);
			redCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
			redCellStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
			redCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

			CellStyle greenCellStyle = workbook.createCellStyle();
			greenCellStyle.setWrapText(true);
			greenCellStyle.setFont(boldFont);
			greenCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
			greenCellStyle.setFillForegroundColor(IndexedColors.GREEN.getIndex());
			greenCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

			CellStyle yellowCellStyle = workbook.createCellStyle();
			yellowCellStyle.setWrapText(true);
			yellowCellStyle.setFont(boldFont);
			yellowCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
			yellowCellStyle.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
			yellowCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

			CellStyle blueCellStyle = workbook.createCellStyle();
			blueCellStyle.setWrapText(true);
			blueCellStyle.setFont(boldFont);
			blueCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
			blueCellStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
			blueCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

			CellStyle leftCellStyle = workbook.createCellStyle();
			leftCellStyle.setWrapText(true);
			leftCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
			leftCellStyle.setAlignment(HorizontalAlignment.LEFT);

			CellStyle regularDateCellStyle = workbook.createCellStyle();
			regularDateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd/MM/yyyy"));
			regularDateCellStyle.setWrapText(true);
			regularDateCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
			regularDateCellStyle.setAlignment(HorizontalAlignment.LEFT);

			if (projectModels != null && projectModels.size() > 0) {
				for (ProjectModel proj : projectModels) {
					rowIndex++;
					int cellIndex = 0;
					Row row = sheet.createRow(rowIndex);
					if (Arrays.stream(columns).anyMatch("Project Name"::equals)) {
						Cell cell = row.createCell(cellIndex);
						cellIndex++;
						cell.setCellValue(proj.getPrjName());
						cell.setCellStyle(regularCellStyle);
					}
					if (Arrays.stream(columns).anyMatch("Descrption"::equals)) {
						Cell cell = row.createCell(cellIndex);
						cellIndex++;
						cell.setCellValue(proj.getPrjDescription());
						cell.setCellStyle(regularCellStyle);
					}
					if (Arrays.stream(columns).anyMatch("Budget"::equals)) {
						Cell cell = row.createCell(cellIndex);
						cellIndex++;
						cell.setCellValue(proj.getPrjBudget());
						cell.setCellStyle(leftCellStyle);
					}
					if (Arrays.stream(columns).anyMatch("Start Date"::equals)) {
						Cell cell = row.createCell(cellIndex);
						cellIndex++;
						cell.setCellValue(proj.getPrjStartDate());
						cell.setCellStyle(regularDateCellStyle);
					}
					if (Arrays.stream(columns).anyMatch("Deadline"::equals)) {
						Cell cell = row.createCell(cellIndex);
						cellIndex++;
						cell.setCellValue(proj.getPrjEndDate());
						cell.setCellStyle(regularDateCellStyle);
					}
					if (Arrays.stream(columns).anyMatch("Status"::equals)) {
						Cell cell = row.createCell(cellIndex);
						cellIndex++;
						String status = convertPrjStatus(proj.getPrjStatus());
						cell.setCellValue(status);
						switch (status) {
							case "Completed":
								cell.setCellStyle(greenCellStyle);
								break;

							case "Inittated":
								cell.setCellStyle(blueCellStyle);
								break;

							case "In Progress":
								cell.setCellStyle(yellowCellStyle);
								break;

							case "On Hold":
								cell.setCellStyle(redCellStyle);
								break;

							case "Planned":
								cell.setCellStyle(blueCellStyle);
								break;

						}

					}
					if (Arrays.stream(columns).anyMatch("Schedule"::equals)) {
						Cell cell = row.createCell(cellIndex);
						cellIndex++;
						String schedule = getSchedule(proj);
						cell.setCellValue(schedule);
						if (schedule.equals("On Schedule"))
							cell.setCellStyle(greenCellStyle);
						if (schedule.equals("Off Schedule"))
							cell.setCellStyle(redCellStyle);
					}
					if (Arrays.stream(columns).anyMatch("Progress"::equals)) {
						Cell cell = row.createCell(cellIndex);
						cellIndex++;
						cell.setCellValue(proj.getPrjProgressPercent());
						cell.setCellStyle(leftCellStyle);
					}
					if (Arrays.stream(columns).anyMatch("Manager"::equals)) {
						Cell cell = row.createCell(cellIndex);
						cell.setCellValue(proj.getPrjManagerId().getCmpUsrFullName());
						cell.setCellStyle(regularCellStyle);
					}
				}
			}

			sheet.setDefaultColumnWidth(30);
			workbook.write(out);
			return new ByteArrayInputStream(out.toByteArray());
		}
	}

	public String convertPrjStatus(PrjStatusEnum status) {

		switch (status) {
			case COMPLETED:
				return "Completed";

			case INITIATED:
				return "Inittated";

			case IN_PROGRESS:
				return "In Progress";

			case ON_HOLD:
				return "On Hold";

			case PLANNED:
				return "Planned";

		}
		return null;

	}

	public String getSchedule(ProjectModel model) {
		String schedule = "Off Schedule";
		Date now = new Date(System.currentTimeMillis());

		if (model.getPrjStatus() == PrjStatusEnum.COMPLETED || model.getPrjEndDate().compareTo(now) > 0) {
			schedule = "On Schedule";
		}
		return schedule;
	}

}
