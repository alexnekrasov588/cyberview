package com.lstechs.ciso.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@EnableScheduling
public class DeleteService {

	private static Logger LOGGER = LogManager.getLogger(DashboardService.class.getName());

	@Autowired
	private CustomerStandardService customerStandardService;

	@Async
	@Scheduled(cron = "${cron.delete.customerStandard}")
	public void deleteCustomerStandard() {
		LOGGER.info("delete customer standard begin.");
		customerStandardService.deleteCustomerStandard();
	}

}
