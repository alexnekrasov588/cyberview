package com.lstechs.ciso.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.lstechs.ciso.enums.StatusEnum;
import com.lstechs.ciso.model.ProjectModel;
import com.lstechs.ciso.model.RecurringTaskModel;
import com.lstechs.ciso.model.TaskModel;

@Service
@EnableAsync
public class RecurringSchedulingService {

	@Autowired
	private RecurringTaskService recurringTaskService;

	@Autowired
	private TaskService taskService;

	@Autowired
	private ProjectService projectService;

	private static Logger LOGGER = LogManager.getLogger(RecurringSchedulingService.class.getName());

	private int getDayOfMonth(Date aDate) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(aDate);
		return cal.get(Calendar.DAY_OF_MONTH);
	}

	private int getDayOfWeek(Date aDate) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(aDate);
		return cal.get(Calendar.DAY_OF_WEEK);
	}

	private int getMonth(Date aDate) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(aDate);
		return cal.get(Calendar.MONTH);
	}

	private Date getDateWithZeroTime(Date date) throws ParseException {
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		Date dateWithZeroTime = formatter.parse(formatter.format(date));
		return dateWithZeroTime;
	}

	private static long getDifferenceDays(Date d1, Date d2) {
	    long diff = d2.getTime() - d1.getTime();
	    return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
	}

	private void copyTask(TaskModel task,RecurringTaskModel recurringTask) throws Exception {
		try {
			LOGGER.info("copy Task: " + task.getTskId());
			TaskModel createTask = new TaskModel(task);
			createTask.setTskStartDate(getDateWithZeroTime(new Date()));
			Calendar cal = Calendar.getInstance();
			long DifferenceDays = getDifferenceDays(task.getTskStartDate(),task.getTskEndDate());
			cal.add(Calendar.DAY_OF_MONTH, (int) DifferenceDays);
			createTask.setTskEndDate(cal.getTime());
			ProjectModel projectModel = projectService.getById(task.getTskPrjID().getPrjId());
			createTask.setTskPrjID(projectModel);
			TaskModel model = taskService.create(createTask);
			LOGGER.info("copy Task: " + task.getTskId() + " Succecss. new task id: " + model.getTskId());
			RecurringTaskModel updateRecurringTask = recurringTaskService.getById(recurringTask.getRecId());
			updateRecurringTask.setNumOfShowUpdated(recurringTask.getNumOfShowUpdated() + 1);
			recurringTaskService.update(updateRecurringTask.getRecId(), updateRecurringTask);
			LOGGER.info("update Number of show updated :" + updateRecurringTask.getRecId() + " Succecss. getNumOfShowUpdated = " +updateRecurringTask.getNumOfShowUpdated());

		}catch (Exception e) {
			LOGGER.error("error with copyTask , ex: " + e.getMessage());
		}
	}

	@Async
	@Scheduled(cron = "${cron.recurring}")
	public void recurringScheduling() throws Exception {
		Date now = getDateWithZeroTime(new Date());
		LOGGER.info("recurring scheduling start : " + new Date());
		List<RecurringTaskModel> recurringList = recurringTaskService.getAll();
		for (RecurringTaskModel tmpRecurring : recurringList) {
			if (tmpRecurring.getNumOfShow() > 0 && tmpRecurring.getNumOfShow() <= tmpRecurring.getNumOfShowUpdated())
				continue;
			if (tmpRecurring.getEndDate() != null && tmpRecurring.getEndDate().before(now))
				continue;
			if (!tmpRecurring.isMonthly() && !tmpRecurring.isWeekly() && !tmpRecurring.isYearly())
				continue;
			TaskModel taskModel = taskService.getById(tmpRecurring.getTskId().getTskId());
			if (taskModel.getTskStartDate().after(now))
				continue;
			if (now.equals(getDateWithZeroTime(taskModel.getTskStartDate())))
				continue;
			if (taskModel.getStatus().equals(StatusEnum.DONE) || taskModel.getStatus().equals(StatusEnum.ON_HOLD))
				continue;

			if (tmpRecurring.isWeekly()) {
				if (getDayOfWeek(now) == getDayOfWeek(taskModel.getTskStartDate())) {
					copyTask(taskModel,tmpRecurring);
				}
			}
			if (tmpRecurring.isMonthly()) {
				if (getDayOfMonth(now) == getDayOfMonth(taskModel.getTskStartDate())) {
					copyTask(taskModel,tmpRecurring);
				}
			}
			if (tmpRecurring.isYearly()) {
				if (getMonth(now) == getMonth(taskModel.getTskStartDate())
						&& getDayOfMonth(now) == getDayOfMonth(taskModel.getTskStartDate())) {
					copyTask(taskModel,tmpRecurring);
				}

			}

		}
		LOGGER.info("recurring scheduling end : " + new Date());

	}

}
