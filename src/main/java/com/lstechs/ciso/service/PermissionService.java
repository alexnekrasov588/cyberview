package com.lstechs.ciso.service;

import com.lstechs.ciso.model.PermissionModel;
import com.lstechs.ciso.model.SingleCompanyUserModel;
import com.lstechs.ciso.repository.PermissionRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PermissionService extends CRUDService<PermissionModel, SingleCompanyUserModel> {
	public PermissionService(PermissionRepository permissionRepository) {
		super(permissionRepository);
	}

	@Autowired
	private PermissionRepository permissionRepository;

	private static final Logger LOGGER = LogManager.getLogger(CRUDService.class.getName());

	public List<PermissionModel> getPermissionListByRoleId(int id){
		LOGGER.info("get permissions list by role id: " + id);
		return permissionRepository.getPermissionByRoleId(id);
	}
}
