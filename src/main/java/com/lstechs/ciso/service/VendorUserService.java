package com.lstechs.ciso.service;

import java.util.ArrayList;

import com.lstechs.ciso.model.SingleVendorUSerModel;
import com.lstechs.ciso.model.VendorUserModel;
import com.lstechs.ciso.repository.VendorUserRepository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class VendorUserService extends CRUDService<VendorUserModel, SingleVendorUSerModel> {
	@Autowired
	private VendorUserRepository vendorUserRepository;

	@Autowired
	private CompanyUserService companyUserService;

	@Autowired
	private PasswordEncoder bcryptEncoder;

	private static Logger LOGGER = LogManager.getLogger(VendorUserService.class.getName());

	public VendorUserService(VendorUserRepository vendorUserRepository) {
		super(vendorUserRepository);
	}


	@Override
	public SingleVendorUSerModel getSingleInstance(VendorUserModel vendorUserModel) throws Exception {
		SingleVendorUSerModel singleVendorUSerModel = new SingleVendorUSerModel(vendorUserModel);
		return singleVendorUSerModel;
	}

	@Override
	public VendorUserModel update(int id, VendorUserModel vendorUserModel) throws Exception {
		LOGGER.info("update vendor user , id: " + id + ".");
		VendorUserModel vendorUser = getVendorUserByEmail(vendorUserModel.getVndrEmail());
		if (vendorUser != null && vendorUser.getVndrId() != vendorUserModel.getVndrId()) {
			LOGGER.error("Email already exist");
			throw new Exception("Email already exist");
		}

		VendorUserModel updateVendorUser = getById(id);
		if (vendorUserModel.getVndrPassword() != null) {
			validatePassword(vendorUserModel);
			updateVendorUser.setVndrPassword(bcryptEncoder.encode(vendorUserModel.getVndrPassword()));
			vendorUserRepository.changePassword(id, updateVendorUser.getVndrPassword());
		}

		updateVendorUser.setVndrEmail(vendorUserModel.getVndrEmail().toLowerCase());
		updateVendorUser.setVndrName(vendorUserModel.getVndrName());

		return vendorUserRepository.save(updateVendorUser);
	}

	@Override
	public VendorUserModel create(VendorUserModel vendorUserModel) throws Exception {
		LOGGER.info("create new vendor user.");
		if (loadUserByEmail(vendorUserModel.getVndrEmail().toLowerCase()) != null) {
			LOGGER.error("Email already exist");
			throw new Exception("Email already exist");
		}
		validatePassword(vendorUserModel);
		VendorUserModel newVendorUserModel = new VendorUserModel();
		newVendorUserModel.setVndrPassword(bcryptEncoder.encode(vendorUserModel.getVndrPassword()));
		newVendorUserModel.setVndrEmail(vendorUserModel.getVndrEmail());
		newVendorUserModel.setVndrName(vendorUserModel.getVndrName());
		return vendorUserRepository.save(newVendorUserModel);
	}

	private void validatePassword(VendorUserModel vendorUserModel) throws Exception {
		UserDetails userDetails = companyUserService.loadUserByEmail(vendorUserModel.getVndrEmail().toLowerCase());
		if (userDetails != null) {
			if (bcryptEncoder.matches(vendorUserModel.getVndrPassword(), userDetails.getPassword())) {
				LOGGER.error("Email already exist with this password in company user");
				throw new Exception("Please choose a different password");
			}
		}
	}

	public UserDetails loadUserByEmail(String email) {
		LOGGER.info("Load vendor user by email: " + email);
		VendorUserModel vendorUserModel = vendorUserRepository.findVendorUserByEmailAndPassword(email.toLowerCase());
		if (vendorUserModel == null) {
			LOGGER.error("cannot find vendor user with email: " + email + ".");
			return null;
		}
		return new User(vendorUserModel.getVndrEmail(), vendorUserModel.getVndrPassword(), new ArrayList<>());
	}

	public VendorUserModel getVendorUserByEmail(String email) {
		LOGGER.info("get vendor user by email : " + email + ".");
		return vendorUserRepository.findVendorUserByEmailAndPassword(email.toLowerCase());
	}

	public void changePassword(int id, String password, String oldPassword) throws Exception {
		LOGGER.info("change vendor user password , id: " + id + ".");
		VendorUserModel vendorUserModel = getById(id);
		if (bcryptEncoder.matches(oldPassword, vendorUserModel.getVndrPassword())) {
			vendorUserRepository.changePassword(id, bcryptEncoder.encode(password));
		} else {
			LOGGER.error("change vendor user password, id:" + id + " failed , Current password is incorrect.");
			throw new Exception("Current password is incorrect.");
		}
	}

	public void resetPassword(int id, String password) throws Exception {
		LOGGER.info("Reset vendor user password , id: " + id + ".");
		VendorUserModel vendorUserModel = new VendorUserModel(getById(id));
		vendorUserModel.setVndrPassword(password);
		validatePassword(vendorUserModel);
		vendorUserRepository.changePassword(id, bcryptEncoder.encode(password));
	}

}
