package com.lstechs.ciso.service;

import com.lstechs.ciso.model.*;
import com.lstechs.ciso.repository.NewsArticlesTagsRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NewsArticlesTagsService extends CRUDService<NewsArticlesTagsModel, SingleNewsArticlesTagsModel> {

	@Autowired
	private NewsArticlesTagsRepository newsArticlesTagsRepository;

	private static Logger LOGGER = LogManager.getLogger(NewsArticlesTagsService.class.getName());

	public NewsArticlesTagsService(NewsArticlesTagsRepository newsArticlesTagsRepository) {
		super(newsArticlesTagsRepository);
	}

	@Override
	public SingleNewsArticlesTagsModel getSingleInstance(NewsArticlesTagsModel newsArticlesTagsModel) throws Exception {
		SingleNewsArticlesTagsModel singleNewsArticlesTagsModel = new SingleNewsArticlesTagsModel(newsArticlesTagsModel);
		return singleNewsArticlesTagsModel;
	}

	public NewsArticlesTagsModel create(NewsArticlesModel newsArticlesModel, NewsTagsModel newsTagsModel) {
		LOGGER.info("create new NewsArticlesTagsModel");
		NewsArticlesTagsModel newNewsArticlesTagsModel = new NewsArticlesTagsModel();
		newNewsArticlesTagsModel.setNewsArtcId(newsArticlesModel);
		newNewsArticlesTagsModel.setNewsTagId(newsTagsModel);
		return newsArticlesTagsRepository.save(newNewsArticlesTagsModel);
	}

	public List<NewsArticlesModel> getArticlesTagsByTagsId(int[] tagsId) {
		LOGGER.info("get articles tags by tags id");
		return newsArticlesTagsRepository.getArticlesTags(tagsId);
	}
}
