package com.lstechs.ciso.service;

import com.lstechs.ciso.model.NewsSourcesModel;
import com.lstechs.ciso.model.SingleNewsSourcesModel;
import com.lstechs.ciso.repository.NewsSourcesRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NewsSourcesService extends CRUDService<NewsSourcesModel, SingleNewsSourcesModel> {

	public NewsSourcesService(NewsSourcesRepository newsSourcesRepository) {
		super(newsSourcesRepository);
	}

	@Autowired
	private NewsSourcesRepository newsSourcesRepository;

	public List<NewsSourcesModel> getAllActive() {
		return (List<NewsSourcesModel>) this.crudRepository.findAll();
	}

	public NewsSourcesModel getOnPrem(int id) throws Exception {
		List<NewsSourcesModel> results = newsSourcesRepository.getOnPrem(id);
		if (results.size() > 0) {
			return results.get(0);
		} else {
			return null;
		}
	}
}
