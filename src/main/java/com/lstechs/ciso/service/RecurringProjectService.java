
package com.lstechs.ciso.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lstechs.ciso.model.RecurringProjectModel;
import com.lstechs.ciso.repository.RecurringProjectRepository;

@Service
public class RecurringProjectService extends CRUDService<RecurringProjectModel,RecurringProjectModel> {

	@Autowired
	private RecurringProjectRepository recurringProjectRepository;
	
	private static Logger LOGGER = LogManager.getLogger(RecurringProjectService.class.getName());
	
	public RecurringProjectService(RecurringProjectRepository recurringProjectRepository) {
		super(recurringProjectRepository);
		// TODO Auto-generated constructor stub
	}
	

	@Override
	public RecurringProjectModel update(int id, RecurringProjectModel t) throws Exception {
		LOGGER.info("update recurring project id: " + id);
		RecurringProjectModel model = getById(id);
		model.setMonthly(t.isMonthly());
		model.setWeekly(t.isWeekly());
		model.setYearly(t.isYearly());
		model.setNumOfShow(t.getNumOfShow());
		if(t.getNumOfShowUpdated() > 0)
		model.setNumOfShowUpdated(t.getNumOfShowUpdated());
		model.setPrjId(t.getPrjId());
		return recurringProjectRepository.save(model);		
	}
	
	public RecurringProjectModel findRecurringProjectByPrjId(int prjId) {
		LOGGER.info("find recurring project by project id: " + prjId);
		return recurringProjectRepository.findRecurringProjectByPrjId(prjId);
	}
	
	@Override
	public RecurringProjectModel create(RecurringProjectModel t) throws Exception {
		if(findRecurringProjectByPrjId(t.getPrjId().getPrjId()) != null)
			throw new Exception("RecurringProject exist for this project");
		return super.create(t);
	}



}
