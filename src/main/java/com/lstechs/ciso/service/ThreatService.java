package com.lstechs.ciso.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.lstechs.ciso.enums.ThreatTypeEnum;
import com.lstechs.ciso.model.ThreatModel;
import com.lstechs.ciso.repository.ThreatRepository;

@Service
public class ThreatService extends CRUDService<ThreatModel, ThreatModel> {

	public ThreatService(ThreatRepository threatRepository) {
		super(threatRepository);
	}

	@Autowired
	public ThreatRepository threatRepository;

	private static final Logger LOGGER = LogManager.getLogger(ThreatService.class.getName());

	public List<ThreatModel> getAll() {
		return threatRepository.getAll();
	}

	public List<ThreatModel> getAdversarialThreats() {
		List<ThreatModel> adversarialThreats = new ArrayList<>(Collections.emptyList());
		List<ThreatModel> threatModelList = threatRepository.getAll();

		for (ThreatModel threatModel : threatModelList)
			if (threatModel.getThreatType().equals(ThreatTypeEnum.ADVERSARIAL)) {
				adversarialThreats.add(threatModel);
			}
		return adversarialThreats;
	}

	public List<List<ThreatModel>> getNonAdversarialThreats() {

		List<ThreatModel> treats = Collections.emptyList();

		try {
			treats = threatRepository.getAll();
		} catch (Exception ex) {
			LOGGER.info(ex.getMessage());
		}

		List<List<ThreatModel>> nonAdversarialThreatsList = new ArrayList<>(Collections.emptyList());

		List<ThreatModel> accidental = new ArrayList<>(Collections.emptyList());
		List<ThreatModel> structural = new ArrayList<>(Collections.emptyList());
		List<ThreatModel> environmental = new ArrayList<>(Collections.emptyList());
		List<ThreatModel> nonAdversarial = new ArrayList<>(Collections.emptyList());

		// Sorting The Threat List.
		for (ThreatModel treat : treats) {

			if (treat.getThreatType().equals(ThreatTypeEnum.NON_ADVERSARIAL)) {
				if (treat.getThreatTitle().equals("ACCIDENTAL")) {
					accidental.add(treat);
				}
				if (treat.getThreatTitle().equals("STRUCTURAL")) {
					structural.add(treat);
				}
				if (treat.getThreatTitle().equals("ENVIRONMENTAL")) {
					environmental.add(treat);
				}
				if (treat.getThreatTitle().equals("NON ADVERSARIAL ")) {
					nonAdversarial.add(treat);
				}
			}
			nonAdversarialThreatsList.add(accidental);
			nonAdversarialThreatsList.add(structural);
			nonAdversarialThreatsList.add(environmental);
			nonAdversarialThreatsList.add(nonAdversarial);
		}
		return nonAdversarialThreatsList;
	}

	public List<ThreatModel> createThreatFromExcelFile(MultipartFile file) throws Exception {

		List<ThreatModel> threatModels = new ArrayList<>();
		XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());
		XSSFSheet worksheet = workbook.getSheetAt(0);

		for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
			ThreatModel threatModel = new ThreatModel();
			XSSFRow row = worksheet.getRow(i);

			try {


				Cell c;
				c = row.getCell(0);
				threatModel.setThreatName(c.getStringCellValue());

				c = row.getCell(1);
				threatModel.setThreatDescription(c.getStringCellValue());

				c = row.getCell(2);
				threatModel.setThreatTitle(c.getStringCellValue());

				c = row.getCell(3);
				threatModel.setThreatIsExample(c.getBooleanCellValue());

				c = row.getCell(4);
				ThreatTypeEnum threatTypeEnum = ThreatTypeEnum.valueOf(c.getStringCellValue().trim());
				threatModel.setThreatType(threatTypeEnum);

				create(threatModel);
				threatModels.add(threatModel);

			} catch (Exception ex) {
				LOGGER.error("Error in file: " + file.getName() + " Line: " + i + " , exception: " + ex);
			}

		}
		return threatModels;
	}

}
