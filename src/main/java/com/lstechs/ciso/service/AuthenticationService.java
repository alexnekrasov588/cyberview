package com.lstechs.ciso.service;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.lstechs.ciso.config.ActiveDirectoryConnectionUtils;
import com.lstechs.ciso.config.JwtTokenUtil;
import com.lstechs.ciso.enums.UsersEnum;
import com.lstechs.ciso.model.*;
import io.jsonwebtoken.Claims;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.ldap.userdetails.InetOrgPersonContextMapper;
import org.springframework.stereotype.Service;

import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.LdapContext;
import java.util.ArrayList;
import java.util.Date;

@Service
public class AuthenticationService implements UserDetailsService {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private VendorUserService vendorUserService;

	@Autowired
	private CompanyUserService companyUserService;

	@Autowired
	private RoleService roleService;

	@Autowired
	private ResetTokensService resetTokensService;

	@Autowired
	private MailService mailService;

	@Autowired
	private CompanyService companyService;

	@Autowired
	private LdapConnectionService ldapConnectionService;

	@Autowired
	private PasswordEncoder bcryptEncoder;

	@Autowired
	private ActiveDirectoryConnectionUtils adConnectionUtils;

	@Autowired
	private ActiveDirectoryLdapService adLdapService;

	private UsersEnum type;

	private Object user;

	@Value("${onPrem}")
	private boolean onPrem;

	@Value("${client.url}")
	private String clientUrl;

	private static final Logger LOGGER = LogManager.getLogger(AuthenticationService.class.getName());

	public boolean isOnPrem() {
		return onPrem;
	}

	@Bean
	public InetOrgPersonContextMapper userContextMapper() {
		return new InetOrgPersonContextMapper();
	}

	public AuthResponseModel getToken(AuthRequestModel authRequestModel, boolean isVendor) throws Exception {
		type = isVendor ? UsersEnum.VendorUser : UsersEnum.CompanyUser;
		String email = authRequestModel.getUsername();
		String tokenEmail = email;

		LOGGER.info("Get Token for : " + email + " when onPrem: " + onPrem + ".");
		UserDetails userDetails = null;

		if (onPrem) {
			if (isVendor) {
				user = vendorLdapConnection(authRequestModel.getUsername(), authRequestModel.getPassword());
				if (user != null) {
					VendorUserModel vendorUserModel = (VendorUserModel) user;
					userDetails = new User(vendorUserModel.getVndrEmail(), vendorUserModel.getVndrPassword(), new ArrayList<>());
				}
			} else {
				LdapConnectionModel ldapConnectionModel = ldapConnectionService.getLdapConnection();
				if (ldapConnectionModel != null) {
					String workingLdapURL = ldapConnectionModel.getFullUrl();
					LdapContext ctx = adConnectionUtils.createContext(workingLdapURL, ldapConnectionModel.getLdapManagerDn(), ldapConnectionModel.getLdapManagerPassword());
					if (ctx == null) {
						ctx = adConnectionUtils.createContext(ldapConnectionModel.getFullUrl2(), ldapConnectionModel.getLdapManagerDn(), ldapConnectionModel.getLdapManagerPassword());
						if (ctx == null) {
							throw new Exception("Connection failed to both urls");
						} else {
							workingLdapURL = ldapConnectionModel.getFullUrl2();
						}
					}

					SearchResult ldapUser = adLdapService.findUserBy(ctx, ldapConnectionModel.getLdapUserSearchBase(), "sAMAccountName", email);
					if (ldapUser != null) {
						String ldapUserEmail = adLdapService.extractAttributeFromResults(ldapUser, "mail");

						//check first if user exist in db and is ciso user
						CompanyUserModel companyUserModel = companyUserService.getCompanyUserByEmail(ldapUserEmail);
						if (companyUserModel != null && companyUserModel.getCmpUsrRoleId().getRoleName().toLowerCase().equals("ciso")) {
							if (bcryptEncoder.matches(authRequestModel.getPassword(), companyUserModel.getCmpUsrPassword())) {
								user = companyUserModel;
								tokenEmail = ldapUserEmail;
							} else {
								LOGGER.error("Incorrect password for CISO onPrem user");
								throw new Exception("Incorrect password");
							}
						} else {
							// find the correct user dn from the ldap and then try to login with it
							String desiredUserDN = adLdapService.getUserDN(ctx, ldapConnectionModel.getLdapUserSearchBase(), email);

							try {
								// try to login to LDAP using the desired user
								LdapContext userCtx = adConnectionUtils.createContext(workingLdapURL, desiredUserDN, authRequestModel.getPassword());

								SearchResult desiredLdapUser = adLdapService.findUserBy(userCtx, ldapConnectionModel.getLdapUserSearchBase(), "sAMAccountName", email);

								if (desiredLdapUser != null) {
									// login using the desired user credentials has succeed!
									String userEmail = adLdapService.extractAttributeFromResults(desiredLdapUser, "mail");

									CompanyUserModel foundCompanyUser = companyUserService.getCompanyUserByEmail(userEmail);
									if (foundCompanyUser != null) {
										user = foundCompanyUser;
										tokenEmail = userEmail;
									} else {
										throw new Exception("User found in Active Directory but not in the system DB");
									}
								} else {
									throw new Exception("User not found");
								}
							} catch (Exception ex) {
								if (ex.getMessage().contains("[LDAP: error code 49")) {
									throw new Exception("Incorrect password");
								} else {
									throw ex;
								}
							}
						}
					} else {
						throw new Exception("User not found");
					}
				} else {
					throw new Exception("No Ldap connection");
				}
			}
		} else {
			authenticate(email, authRequestModel.getPassword());
		}

		isValidLicense(tokenEmail);

		final String token = jwtTokenUtil.generateToken(tokenEmail, user, type);
		AuthResponseModel authResponseModel = new AuthResponseModel(token, type);
		return authResponseModel;
	}

	private VendorUserModel vendorLdapConnection(String username, String password) throws Exception {

		LOGGER.info("authenticate for vendor user : " + username + ".");
		VendorUserModel vendorUserModel = vendorUserService.getVendorUserByEmail(username);
		if (vendorUserModel != null) {
			if (bcryptEncoder.matches(password, vendorUserModel.getVndrPassword())) return vendorUserModel;
			else {
				LOGGER.error("authenticate for : " + username + "failed.");
				throw new Exception("Incorrect username or password");
			}
		} else return null;
	}

	private void isValidLicense(String username) throws Exception {
		if (type.equals(UsersEnum.VendorUser)) return;
		CompanyModel companyModel;
		if (onPrem) {
			companyModel = companyService.getLDAPComapny();
		} else {
			if (user == null) {
				companyModel = companyService.getCompanyByUserName(username);
			} else {
				CompanyUserModel companyUserModel = (CompanyUserModel) user;
				companyModel = companyUserModel.getCmpUsrCmpId();
			}

		}
		if (companyModel.getCmpLcnsId() != null) {
			Date endDate = companyModel.getCmpLcnsId().getLcnsEndDate();
			if (endDate.before(new Date())) {
				throw new Exception("The license has expired");
			}
		} else throw new Exception("The license has expired");
	}

	private void authenticate(String username, String password) throws Exception {
		try {
			LOGGER.info("authenticate for : " + username + "when onPrem: " + onPrem + ".");
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		} catch (DisabledException e) {
			LOGGER.error("authenticate for : " + username + "failed , exception: " + e);
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			LOGGER.error("authenticate for : " + username + "failed , exception: " + e);
			throw new Exception("Incorrect username or password", e);
		}
	}

	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		LOGGER.info("load user by username : " + email + "when onPrem: " + onPrem + ".");
		if (type == UsersEnum.VendorUser) {
			user = vendorUserService.getVendorUserByEmail(email);
			if (user == null) {
				LOGGER.error("Load vendor user by username : " + email + "failed , vendor user not found.");
				throw new UsernameNotFoundException("Incorrect username or password");
			}
			return new User(((VendorUserModel) user).getVndrEmail(), ((VendorUserModel) user).getVndrPassword(), new ArrayList<>());
		} else {
			user = companyUserService.getCompanyUserByEmail(email);
			if (user == null) {
				LOGGER.error("Load company user by username : " + email + "failed , vendor user not found.");
				throw new UsernameNotFoundException("Incorrect username or password");
			}
			return new User(((CompanyUserModel) user).getCmpUsrEmail(), ((CompanyUserModel) user).getCmpUsrPassword(), new ArrayList<>());
		}

	}

	public void changePassword(ObjectNode objectNode, int id) throws Exception {
		LOGGER.info("reset password id: " + id + "when onPrem: " + onPrem + ".");
		String newPassword = objectNode.get("newPassword").asText();
		String oldPassword = objectNode.get("oldPassword").asText();
		String objType = objectNode.get("type").asText();
		if (objType.equals(UsersEnum.CompanyUser.toString())) {
			companyUserService.changePassword(id, newPassword, oldPassword);
		} else {
			vendorUserService.changePassword(id, newPassword, oldPassword);
		}
	}

	public void forgotPasswordRequest(String email, boolean isVendor) throws Exception {
		LOGGER.info("forgot password request :" + email);
		type = isVendor ? UsersEnum.VendorUser : UsersEnum.CompanyUser;
		loadUserByUsername(email);
		String token = jwtTokenUtil.generateTempToken(email);
		int userId;
		Date expiresAt = new Date(System.currentTimeMillis() + jwtTokenUtil.getExpiresAt());

		if (type.equals(UsersEnum.CompanyUser)) {
			CompanyUserModel companyUserModel = (CompanyUserModel) user;
			userId = companyUserModel.getCmpUsrId();
		} else {
			VendorUserModel vendorUserModel = (VendorUserModel) user;
			userId = vendorUserModel.getVndrId();
		}
		ResetTokensModel resetTokensModel = new ResetTokensModel(userId, type.toString(), token, expiresAt, new Date(System.currentTimeMillis()));
		resetTokensService.create(resetTokensModel);
		LOGGER.info("sending email forgot password request to: " + email);
		mailService.sendEmail(email, "to reset your password click here : http://" + clientUrl + "/#/reset?token=" + token, "Ciso Reset password ");
	}

	public void resetPassword(String token, String password) throws Exception {
		LOGGER.info("reset password token: " + token);
		ResetTokensModel resetTokensModel = getResetToken(token);
		if (resetTokensModel == null) {
			throw new Exception("Token not found");
		} else {
			if (resetTokensModel.getUserType().equals(UsersEnum.CompanyUser.toString())) {
				companyUserService.resetPassword(resetTokensModel.getUserId(), password);
			} else {
				vendorUserService.resetPassword(resetTokensModel.getUserId(), password);
			}
			resetTokensService.deleteDuplicate(resetTokensModel.getUserId(), resetTokensModel.getUserType());
			LOGGER.info("Reset password token: " + token + "success.");
		}
	}

	public ResetTokensModel getResetToken(String token) {
		LOGGER.info("token: " + token + " validation");
		ResetTokensModel resetTokensModel = resetTokensService.getResetTokensByResetToken(token);
		if (resetTokensModel == null) {
			LOGGER.error("Token not found");
			return null;
		}
		return resetTokensModel;
	}

	public CompanyUserModel getCompanyUserFromToken(String token) {
		LOGGER.info("Get company user from token: " + token);
		String email = jwtTokenUtil.getUsernameFromToken(token);
		CompanyUserModel companyUserModel = companyUserService.getCompanyUserByEmail(email);
		if (companyUserModel == null) {
			LOGGER.error("Company user not exist");
			throw new Error("Company user not exist");
		}
		return companyUserModel;
	}

	public CompanyModel getCompanyFromToken(String token) {
		String email = jwtTokenUtil.getUsernameFromToken(token);
		CompanyUserModel companyUserModel = companyUserService.getCompanyUserByEmail(email);
		if (companyUserModel == null) {
			LOGGER.error("Company user not exist");
			throw new Error("Company user not exist");
		}
		return companyUserModel.getCmpUsrCmpId();
	}

	public VendorUserModel getVendorUserFromToken(String token) {
		LOGGER.info("get vendor user from token: " + token);
		String email = jwtTokenUtil.getUsernameFromToken(token);
		return vendorUserService.getVendorUserByEmail(email);
	}

	public UsersEnum getUserType(String token) {
		LOGGER.info("get user type  from token: " + token);
		Claims claims = jwtTokenUtil.getAllClaimsFromToken(token);
		if (claims.get("type").equals("VendorUser")) return UsersEnum.VendorUser;
		else return UsersEnum.CompanyUser;
	}

	private CompanyUserModel getLDAPCompanyUser(String username, String password, String email, String name) {
		LOGGER.info("get LDAP Company User");
		CompanyModel companyModel = companyService.getLDAPComapny();
		RoleModel roleModel = roleService.getLdapRolesBySpecificName("team member");
		try {
			CompanyUserModel companyUserModel = new CompanyUserModel(username + "-" + name, password, email, companyModel, roleModel, null);
			return companyUserModel;
		} catch (Exception e) {
			LOGGER.error(e);
			return null;
		}
	}

	public void setUserType(String type) {
		this.type = UsersEnum.valueOf(type);
	}

}
