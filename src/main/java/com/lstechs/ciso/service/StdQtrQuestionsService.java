package com.lstechs.ciso.service;

import com.lstechs.ciso.model.SingleCompanyUserModel;
import com.lstechs.ciso.model.StdQtrQuestionsModel;
import com.lstechs.ciso.model.StdRequirementsModel;
import com.lstechs.ciso.model.StdSectionModel;
import com.lstechs.ciso.repository.StdQtrQuestionsRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class StdQtrQuestionsService extends CRUDService<StdQtrQuestionsModel, SingleCompanyUserModel> {

	@Autowired
	public StdQtrQuestionsRepository stdQtrQuestionsRepository;

	@Autowired
	public StdQstOptionService stdQstOptionService;

	@Autowired
	public StdSectionService stdSectionService;

	@Autowired
	public StdRequirementsService stdRequirementsService;

	private static final Logger LOGGER = LogManager.getLogger(StdQtrQuestionsService.class.getName());

	public StdQtrQuestionsService(StdQtrQuestionsRepository stdQtrQuestionsRepository) {
		super(stdQtrQuestionsRepository);
	}

	@Override
	public StdQtrQuestionsModel create(StdQtrQuestionsModel stdQtrQuestionsModel) throws Exception {
		if(stdQtrQuestionsModel.getOrder() == 0){
			int maxOrder = stdQtrQuestionsRepository.getMaxOrder(stdQtrQuestionsModel.getStdSctId().getStdSctId()) + 1;
			maxOrder = Math.max(stdSectionService.getMaxOrderByParentId(stdQtrQuestionsModel.getStdSctId().getStdSctId()),maxOrder);
			stdQtrQuestionsModel.setOrder(maxOrder);
			stdRequirementsService.updateOrderByQstId(stdQtrQuestionsModel.getStdRqrId().getStdRqrId(),maxOrder);
		}
		return super.create(stdQtrQuestionsModel);
	}

	public int getMaxOrder(int sectionId){
		return stdQtrQuestionsRepository.getMaxOrder(sectionId) + 1;
	}

	public int getMaxOrderByIds(List<Integer> sectionId){
		return stdQtrQuestionsRepository.getMaxOrderByIds(sectionId) + 1;
	}

	@Override
	public StdQtrQuestionsModel update(int id, StdQtrQuestionsModel stdLevelModel) throws Exception {
		LOGGER.info("update StdQtr questions  , id: " + id + ".");
		StdQtrQuestionsModel updateStdQtrQuestionsModel = getById(id);
		updateStdQtrQuestionsModel.setQtrQstName(stdLevelModel.getQtrQstName());
		updateStdQtrQuestionsModel.setQtrQstType(stdLevelModel.getQtrQstType());
		updateStdQtrQuestionsModel.setAnswerDate1Type(stdLevelModel.getAnswerDate1Type());
		updateStdQtrQuestionsModel.setAnswerDate2Type(stdLevelModel.getAnswerDate2Type());
		updateStdQtrQuestionsModel.setStdRqrId(stdLevelModel.getStdRqrId());
		if(stdLevelModel.getOrder() > 0)
			updateStdQtrQuestionsModel.setOrder(stdLevelModel.getOrder());
		return stdQtrQuestionsRepository.save(updateStdQtrQuestionsModel);
	}

	public List<StdQtrQuestionsModel> getAllQuestionBySectionId(int id) {
		LOGGER.info("get all questions by section id: " + id + ".");
		return stdQtrQuestionsRepository.getAllQuestionBySectionId(id);
	}

	public List<StdQtrQuestionsModel> getAllQuestionByStandardIds(int[] ids) {
		LOGGER.info("get all questions by section ids");
		List<StdSectionModel> stdSectionModels = stdSectionService.getAllSectionByStasndardIds(ids);
		if(stdSectionModels != null && stdSectionModels.size()>0){
			int[]sectionIds = new int[stdSectionModels.size()];
			int i = 0;
			for(StdSectionModel tmpStdSectionModel:stdSectionModels){
				sectionIds[i] = tmpStdSectionModel.getStdSctId();
				i++;
			}
			List<Integer> list = Arrays.stream(sectionIds).boxed().collect(Collectors.toList());


			return stdQtrQuestionsRepository.getAllQuestionBySectionIds(list);
		}
		return null;
	}

	public List<StdRequirementsModel> getAllReqBySectionId(int id) {
		LOGGER.info("Get all Requirements by Section id: " + id + ".");
		return stdQtrQuestionsRepository.getAllReqBySectionId(id);
	}


	public StdQtrQuestionsModel getQuestionsByRequirementId(int id) {
		LOGGER.info("get  questions by requirement id: " + id + ".");
		return stdQtrQuestionsRepository.getQuestionByReqId(id);
	}

	public StdQtrQuestionsModel getOnPrem(int id) {
		return stdQtrQuestionsRepository.getOnPrem(id);
	}


	@Override
	public StdQtrQuestionsModel delete(int id) throws Exception {
		StdQtrQuestionsModel stdQtrQuestionsModel = getById(id);
		stdQtrQuestionsModel.setDeletedAt(new Date(System.currentTimeMillis()));
		stdQstOptionService.deleteStdQstOptionByQtsId(id);
		LOGGER.info("Delete question id: " + id + ".");
		return stdQtrQuestionsRepository.save(stdQtrQuestionsModel);
	}

	public StdQtrQuestionsModel deleteQuetionsByReqId(int id) throws Exception {
		StdQtrQuestionsModel stdQtrQuestionsModel = getQuestionsByRequirementId(id);
		LOGGER.info("Delete question id: " + stdQtrQuestionsModel.getQtrQstId() + ".");
		return delete(stdQtrQuestionsModel.getQtrQstId());
	}


}
