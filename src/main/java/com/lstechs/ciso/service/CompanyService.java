package com.lstechs.ciso.service;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.lstechs.ciso.model.*;
import com.lstechs.ciso.repository.CompanyRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

@Service
public class CompanyService extends CRUDService<CompanyModel, SingleCompanyModel> {
	@Autowired
	private CompanyRepository companyRepository;

	@Autowired
	private RoleService roleService;

	@Autowired
	private CompanyUserService companyUserService;

	@Autowired
	private LicenseService licenseService;

	@Autowired
	private AssetTypeService assetTypeService;

	private static final Logger LOGGER = LogManager.getLogger(CompanyService.class.getName());

	public CompanyService(CompanyRepository companyRepository) {
		super(companyRepository);
	}

	@Override  // override to able sorting by a specific column
	public Page<CompanyModel> getAll(Integer pageNo, Integer pageSize, String orderBy, boolean orderByDesc, String query) throws Exception {
		LOGGER.info("Get all from Company");

		if (orderBy.equals("endDate")) { // fields that had access from JPA Repository
			orderBy = "cmpLcnsId.lcnsEndDate";
		}

		Sort sort = Sort.by(Sort.Order.desc(orderBy));
		if (!orderByDesc) {
			sort = Sort.by(Sort.Order.asc(orderBy));
		}
		Pageable paging;

		// all field that need to calculate and sort by them
		if (orderBy.equals("cmpUsersCount")) {
			if (!orderByDesc) {
				paging = PageRequest.of(pageNo, pageSize); // native query without Sort
				return companyRepository.findAllOrderByCmpUsersCountASC(paging, query.toLowerCase());
			} else {
				paging = PageRequest.of(pageNo, pageSize);
				return companyRepository.findAllOrderByCmpUsersCountDESC(paging, query.toLowerCase());
			}
		} else if (orderBy.equals("standardsCount")) {
			if (!orderByDesc) {
				paging = PageRequest.of(pageNo, pageSize); // native query without Sort
				return companyRepository.findAllOrderByStandardsCountASC(paging, query.toLowerCase());
			} else {
				paging = PageRequest.of(pageNo, pageSize);
				return companyRepository.findAllOrderByStandardsCountDESC(paging, query.toLowerCase());
			}
		} else {
			paging = PageRequest.of(pageNo, pageSize, sort); // default
			return companyRepository.findAll(paging, query.toLowerCase());
		}
	}

	@Override
	public CompanyModel update(int id, CompanyModel newCompanyModel) throws Exception {
		LOGGER.info("update company , id: " + id + ".");
		CompanyModel companyModel = getById(id); // get the prev company and update him
		companyModel.setCmpDataRetentionPeriod(newCompanyModel.getCmpDataRetentionPeriod());

		// CASE 1 - There is a license for the prev company and no license for the updated company(new)
		if (companyModel.getCmpLcnsId() != null && newCompanyModel.getCmpLcnsId() == null) {
			updateCmpLicenseActivate(companyModel, false); // need to deactivate the old license
			companyModel.setCmpLcnsId(null);
		}

		// CASE 2 - There is a license for the updated company(new) and no license for the prev company
		else if (newCompanyModel.getCmpLcnsId() != null && companyModel.getCmpLcnsId() == null) {
			LicenseModel updateLicenseModel = updateCmpLicenseActivate(newCompanyModel, true); // need to activate the new license
			companyModel.setCmpLcnsId(updateLicenseModel);
		}

		// CASE 3 - There is a license for the updated company(new) and to the prev company
		else if (companyModel.getCmpLcnsId() != null && companyModel.getCmpLcnsId() != null) {
			// check if the licenses are different
			if (newCompanyModel.getCmpLcnsId().getLcnsId() != companyModel.getCmpLcnsId().getLcnsId()) {
				updateCmpLicenseActivate(companyModel, false); // need to deactivate the old license
				LicenseModel updateLicenseModel = updateCmpLicenseActivate(newCompanyModel, true); // need to activate the new license
				companyModel.setCmpLcnsId(updateLicenseModel); // update the company's license
			} // else - no updated needed
		}
		companyModel.setCmpLogoUrl(newCompanyModel.getCmpLogoUrl());
		companyModel.setCmpName(newCompanyModel.getCmpName());
		return companyRepository.save(companyModel);
	}

	@Override
	public CompanyModel delete(int id) throws Exception {
		LOGGER.info("Delete company , id: " + id + ".");
		CompanyModel companyModel = getById(id);
		List<RoleModel> roleModels = roleService.getAllRolesByCmpId(id);
		if (roleModels != null && roleModels.size() > 0) {
			for (RoleModel tmpRoleModel : roleModels) {
				roleService.delete(tmpRoleModel.getRoleId());
			}
		}
		List<CompanyUserModel> companyUserModels = companyUserService.getUsersByCmpId(companyModel.getCmpId());
		if (companyUserModels != null && companyUserModels.size() > 0) {
			for (CompanyUserModel tmpCompanyUserModel : companyUserModels) {
				companyUserService.delete(tmpCompanyUserModel.getCmpUsrId());
			}
		}
		LicenseModel licenseModel = updateCmpLicenseActivate(companyModel, false);
		companyModel.setCmpLcnsId(licenseModel);
		companyModel.setDeletedAt(new Date(System.currentTimeMillis()));
		return companyRepository.save(companyModel);
	}

	@Override
	public CompanyModel create(CompanyModel companyModel) throws Exception {
		LOGGER.info("Create new  company.");
		LicenseModel licenseModel = updateCmpLicenseActivate(companyModel, true);
		companyModel.setCmpLcnsId(licenseModel);
		companyModel.setCmpSupportEmail(" ");
		CompanyModel newCompanyModel = companyRepository.save(companyModel);
		roleService.createDefaultRolesForCompany(newCompanyModel);
		assetTypeService.createDefaultAssetTypePerCompany(newCompanyModel);
		return newCompanyModel;
	}

	private LicenseModel updateCmpLicenseActivate(CompanyModel companyModel, boolean isActive) throws Exception {
		LicenseModel cmpLicenseModel = companyModel.getCmpLcnsId();
		if (cmpLicenseModel != null) {
			LicenseModel licenseModel = licenseService.getById(cmpLicenseModel.getLcnsId());
			licenseModel.setLcnsIsActive(isActive);
			return licenseService.update(cmpLicenseModel.getLcnsId(), licenseModel);
		}
		return null; // there is no license to company
	}


	public CompanyModel getLDAPComapny() {
		LOGGER.info("get LDAP company.");
		return companyRepository.getLDAPCompany().get(0);
	}

	public CompanyModel getCompanyByUserName(String username) {
		CompanyUserModel companyUserModel = companyUserService.getCompanyUserByEmail(username);
		return companyUserModel.getCmpUsrCmpId();
	}

	public CompanyModel updateCompanyDetails(ObjectNode objectNode, int id) throws Exception {
 		CompanyModel companyModel = getById(id);
		LOGGER.info("update company details id: " + id + ".");
		try {
			String cmpName = objectNode.get("cmpName").asText();
			if (cmpName != null) companyModel.setCmpName(cmpName);
		} catch (Exception ex) {
			LOGGER.error("failed to read cmpName");
		}

		try {
			String cmpSupportEmail = objectNode.get("cmpSupportEmail").asText();

			if (cmpSupportEmail == null || cmpSupportEmail == "null"  || cmpSupportEmail == "") {
				cmpSupportEmail = "";
				companyModel.setCmpSupportEmail(cmpSupportEmail);
			}
			else {
				if (!isValid(cmpSupportEmail)) {
					throw new IllegalArgumentException();
				}
				companyModel.setCmpSupportEmail(cmpSupportEmail);
			} }catch (Exception ex) {
				LOGGER.error("failed to read cmpSupportEmail");
				System.out.println(ex);
			}
		    try {

		    	String cmpSiteName = objectNode.get("cmpSiteName").asText();
			    if (cmpSiteName != null) companyModel.setCmpSiteName(cmpSiteName);
		    } catch (Exception ex) {
			LOGGER.error("failed to read cmpSiteName");
		  }

			return companyRepository.save(companyModel);
		}


	private static boolean isValid(String email) {

		String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
			"[a-zA-Z0-9_+&*-]+)*@" +
			"(?:[a-zA-Z0-9-]+\\.)+[a-z" +
			"A-Z]{2,7}$";

		Pattern pat = Pattern.compile(emailRegex);

		// Defect Number 42 Fix - Allow to save NULL value at Email field

		if(email == null || email.equals("null")) {
			return true;
		}

		//		if (email == null)
//			return false;
		return pat.matcher(email).matches();
	}


	/**
	 * @param cmpId
	 * @return company model if not exceeding users limit
	 * @throws Exception Exceeding users limit
	 */
	public CompanyModel getValidCompany(int cmpId) throws Exception {
		CompanyModel companyModel = getById(cmpId);
		if (companyModel.getCmpLcnsId() != null) {
			LicenseModel licenseModel = licenseService.getById(companyModel.getCmpLcnsId().getLcnsId());
			List<CompanyUserModel> companyUserModels = companyUserService.getUsersByCmpId(cmpId);
			if (companyUserModels.size() >= licenseModel.getLcnsAllowedUsersCount()) {
				throw new Exception("Exceeding users limit");
			}
			return companyModel;
		} else throw new Exception("Cannot create company user: There is no assigned license to company");
	}

	public CompanyModel storeCmpLogoFile(MultipartFile file, int id) throws Exception {
		if (file == null) {
			return updateCmpLogoFile(null, id, "Delete Company's logo file, company id: ");
		}
		String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));

		try {
			if (file.getContentType() == null) {
				throw new Exception("File not found.");
			} else if (!file.getContentType().startsWith("image/")) {
				throw new Exception("The selected file type is not an image type!");
			}

			if (fileName.contains("..")) {
				throw new Exception("Sorry! Filename contains invalid path sequence " + fileName);
			}
			return updateCmpLogoFile(file.getBytes(), id, "Upload logo file to company with id: ");

		} catch (Exception ex) {
			throw new Exception("Could not store file " + fileName + ". Please try again!", ex);
		}
	}

	private CompanyModel updateCmpLogoFile(byte[] file, int id, String infoDescription) throws Exception {
		LOGGER.info("Get user company id: " + id);
		CompanyModel updateCompanyModel = getById(id);

		LOGGER.info(infoDescription + id + ".");
		updateCompanyModel.setCmpLogoFile(file);
		return companyRepository.save(updateCompanyModel);
	}


	@Override
	public SingleCompanyModel getSingleInstance(CompanyModel companyModel) throws Exception {
		SingleCompanyModel singleCompanyModel = new SingleCompanyModel(companyModel);
		return singleCompanyModel;
	}

	public SingleCompanyModel getSingalCompanyModelByCmpId(int cmpId) throws Exception {
		CompanyModel companyModel = getById(cmpId);
		return getSingleInstance(companyModel);
	}
}
