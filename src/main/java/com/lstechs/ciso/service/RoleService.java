package com.lstechs.ciso.service;

import com.lstechs.ciso.enums.ModuleEnum;
import com.lstechs.ciso.enums.UsersEnum;
import com.lstechs.ciso.model.*;
import com.lstechs.ciso.repository.RoleRepository;
import com.lstechs.ciso.repository.SelectedQuestionnaireRepository;
import com.lstechs.ciso.repository.SelectedStandardsRepository;
import com.lstechs.ciso.repository.StdQuestionaireRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class RoleService extends CRUDService<RoleModel, SingleRoleModel> {

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private PermissionService permissionService;

	@Autowired
	private SelectedStandardsRepository selectedStandardsRepository;

	@Autowired
	private SelectedQuestionnaireRepository selectedQuestionnaireRepository;

	@Autowired
	private AuthenticationService authenticationService;

	@Autowired
	private CompanyService companyService;

	@Autowired
	private StdQuestionaireRepository stdQuestionaireRepository;

	private static final Logger LOGGER = LogManager.getLogger(RoleService.class.getName());

	public RoleService(RoleRepository roleRepository) {
		super(roleRepository);
	}

	@Override
	public SingleRoleModel getSingleInstance(RoleModel roleModel) throws Exception {
		SingleCompanyModel cmpId = companyService.getSingalCompanyModelByCmpId(roleModel.getCmpId().getCmpId());
		return new SingleRoleModel(roleModel, cmpId);
	}

	public Page<RoleModel> getAllRoles(Integer pageNo, Integer pageSize, String orderBy, boolean orderByDesc, String query, String token, int cmpId) {
		LOGGER.info("get all roles");
		Sort sort = Sort.by(Sort.Order.desc(orderBy));
		if (!orderByDesc) {
			sort = Sort.by(Sort.Order.asc(orderBy));
		}
		Pageable paging = PageRequest.of(pageNo, pageSize, sort);
		UsersEnum usersEnum = authenticationService.getUserType(token);
		Page<RoleModel> pagedResult;
		if (usersEnum.equals(UsersEnum.CompanyUser)) {
			CompanyUserModel companyUserModel = authenticationService.getCompanyUserFromToken(token);
			pagedResult = roleRepository.findAllRolesByCmpId(paging, query.toLowerCase(), companyUserModel.getCmpUsrCmpId().getCmpId());
		} else {
			if (cmpId > 0) {
				pagedResult = roleRepository.findAllRolesByCmpId(paging, query.toLowerCase(), cmpId);
			} else pagedResult = roleRepository.findAll(paging, query.toLowerCase());
		}

		// Selected Standards And Questionnaires Handling
		List<RoleModel> listResults = pagedResult.getContent();
		for (RoleModel listResult : listResults) {

			ArrayList<Integer> standardIds = new ArrayList<>(Collections.emptyList());
			ArrayList<Integer> questionnaireIds = new ArrayList<>(Collections.emptyList());

			//Selected Standards
			if (!listResult.isAreAllStandardsSelected()) {
				List<SelectedStandardModel> selectedStandardModelList = selectedStandardsRepository.getSelectedStandardModelByRoleId(listResult.getRoleId());

				for (SelectedStandardModel selectedStandardModel : selectedStandardModelList) {
					standardIds.add(selectedStandardModel.getStandardId());

					//Selected Questionnaires
					if (!listResult.isAreAllQuestionnairesSelected()) {
						List<SelectedQuestionnaireModel> selectedQuestionnaireModelList = selectedQuestionnaireRepository.getSelectedQuestionnairesModelByRoleIdAndStandardId(listResult.getRoleId(), selectedStandardModel.getStandardId());
						for (SelectedQuestionnaireModel selectedQuestionnaireModel : selectedQuestionnaireModelList) {
							questionnaireIds.add(selectedQuestionnaireModel.getQuestionnaireId());
						}
						listResult.setSelectedQuestionnaires(questionnaireIds);
					}
				}
				listResult.setSelectedStandards(standardIds);
			}
		}
		return new PageImpl<>(listResults);
	}

	@Override
	public RoleModel update(int id, RoleModel roleModel) throws Exception {

		boolean areAllStandardsSelected = false;
		boolean areAllQuestionnairesSelected = false;

		//Deleting all existing SelectedStandardModels & SelectedQuestionnaireModels
		try {
			List<SelectedStandardModel> selectedStandardModelList = selectedStandardsRepository.getSelectedStandardModelByRoleId(roleModel.getRoleId());
			List<SelectedQuestionnaireModel> selectedQuestionnaireModelList = selectedQuestionnaireRepository.getSelectedQuestionnairesModelByRoleId(roleModel.getRoleId());
			if (selectedStandardModelList.size() > 0) {
				selectedStandardsRepository.deleteSelectedStandardModel(roleModel.getRoleId());
			}
			if (selectedQuestionnaireModelList.size() > 0) {
				selectedQuestionnaireRepository.deleteSelectedQuestionnaireModel(roleModel.getRoleId());
			}
		} catch (Exception ex) {
			LOGGER.error(ex.getMessage());
		}

		//Getting the standardIds list and questionnaireIds list
		ArrayList<Integer> standardIds = roleModel.getSelectedStandards();
		ArrayList<Integer> questionnaireIds = roleModel.getSelectedQuestionnaires();

		//If the customer choose all Standards and all Questionnaires
		if (standardIds == null && questionnaireIds == null) {
			areAllQuestionnairesSelected = true;
			areAllStandardsSelected = true;
		}
		//If the customer choose all Questionnaires OR didn't choose any of Questionnaires AND choose some of the Standards
		if (standardIds != null && questionnaireIds == null) {
			for (Integer standardId : standardIds) {

				areAllQuestionnairesSelected = true;
				areAllStandardsSelected = false;

				SelectedStandardModel selectedStandardModel = new SelectedStandardModel();
				selectedStandardModel.setStandardId(standardId);
				selectedStandardModel.setRoleId(roleModel);
				try {
					selectedStandardsRepository.save(selectedStandardModel);
				} catch (Exception ex) {
					LOGGER.error(ex.getMessage());
				}
			}
		}
		//If the customer choose all Standards AND choose some of the Questionnaires
		else if (standardIds == null && questionnaireIds != null) {
			for (Integer questionnaireId : questionnaireIds) {

				areAllQuestionnairesSelected = false;
				areAllStandardsSelected = true;

				StdQuestionaireModel stdQuestionaireModel = stdQuestionaireRepository.getStdQuestionnaireByQuestionnaireId(questionnaireId);
				SelectedQuestionnaireModel selectedQuestionnaireModel = new SelectedQuestionnaireModel();
				selectedQuestionnaireModel.setQuestionnaireId(questionnaireId);
				selectedQuestionnaireModel.setRoleId(roleModel);
				selectedQuestionnaireModel.setStandardId(stdQuestionaireModel.getStdId().getStdId());
				try {
					selectedQuestionnaireRepository.save(selectedQuestionnaireModel);
				} catch (Exception ex) {
					LOGGER.error(ex.getMessage());
				}
			}
		}
		//If the customer choose some of the Standards AND choose some of the Questionnaires
		else if (standardIds != null && questionnaireIds != null) {
			for (Integer standardId : standardIds) {
				areAllStandardsSelected = false;

				SelectedStandardModel selectedStandardModel = new SelectedStandardModel();
				selectedStandardModel.setStandardId(standardId);
				selectedStandardModel.setRoleId(roleModel);
				try {
					selectedStandardsRepository.save(selectedStandardModel);
				} catch (Exception ex) {
					LOGGER.error(ex.getMessage());
				}
			}
			for (Integer questionnaireId : questionnaireIds) {
				areAllQuestionnairesSelected = false;

				StdQuestionaireModel stdQuestionaireModel = stdQuestionaireRepository.getStdQuestionnaireByQuestionnaireId(questionnaireId);
				SelectedQuestionnaireModel selectedQuestionnaireModel = new SelectedQuestionnaireModel();
				selectedQuestionnaireModel.setQuestionnaireId(questionnaireId);
				selectedQuestionnaireModel.setRoleId(roleModel);
				selectedQuestionnaireModel.setStandardId(stdQuestionaireModel.getStdId().getStdId());
				try {
					selectedQuestionnaireRepository.save(selectedQuestionnaireModel);
				} catch (Exception ex) {
					LOGGER.error(ex.getMessage());
				}
			}
		}

		RoleModel updateRoleModel = getById(id);
		if (updateRoleModel.isEditable()) {
			LOGGER.info("Update role id: " + id + ".");
			updateRoleModel.setRoleName(roleModel.getRoleName());
			updateRoleModel.setEditable(roleModel.isEditable());
			updateRoleModel.setAreAllQuestionnairesSelected(areAllQuestionnairesSelected);
			updateRoleModel.setAreAllStandardsSelected(areAllStandardsSelected);
			List<PermissionModel> permissionModels = permissionService.getPermissionListByRoleId(id);
			for (PermissionModel tmpPermissionModel : permissionModels) {
				permissionService.delete(tmpPermissionModel.getPermId());
			}
			createPermission(roleModel);
			return roleRepository.save(updateRoleModel);
		} else {
			LOGGER.error("Role id: " + id + ", is a default role, and not editable!");
			throw new Exception("This role is not editable");
		}
	}

	@Override
	public RoleModel create(RoleModel roleModel) throws Exception {
		LOGGER.info("Create new role .");

		RoleModel newRoleModel = new RoleModel();
		newRoleModel.setRoleName(roleModel.getRoleName());
		newRoleModel.setCmpId(roleModel.getCmpId());
		newRoleModel.setEditable(roleModel.isEditable());
		RoleModel createRoleModel = roleRepository.save(newRoleModel);

		//Getting the standardIds list and questionnaireIds list
		ArrayList<Integer> standardIds = roleModel.getSelectedStandards();
		ArrayList<Integer> questionnaireIds = roleModel.getSelectedQuestionnaires();

		//If the customer choose all Standards and all Questionnaires
		if (standardIds == null && questionnaireIds == null) {
			roleModel.setAreAllStandardsSelected(true);
			roleModel.setAreAllQuestionnairesSelected(true);
		}
		//If the customer choose all Questionnaires OR didn't choose any of Questionnaires AND choose some of the Standards
		if (standardIds != null && questionnaireIds == null) {

			for (Integer standardId : standardIds) {
				roleModel.setAreAllStandardsSelected(false);
				roleModel.setAreAllQuestionnairesSelected(true);

				SelectedStandardModel selectedStandardModel = new SelectedStandardModel();
				selectedStandardModel.setStandardId(standardId);
				selectedStandardModel.setRoleId(createRoleModel);
				try {
					selectedStandardsRepository.save(selectedStandardModel);
				} catch (Exception ex) {
					LOGGER.error(ex.getMessage());
				}
			}
		}
		//If the customer choose all Standards AND choose some of the Questionnaires
		else if (standardIds == null && questionnaireIds != null) {
			for (Integer questionnaireId : questionnaireIds) {
				roleModel.setAreAllStandardsSelected(true);
				roleModel.setAreAllQuestionnairesSelected(false);

				StdQuestionaireModel stdQuestionaireModel = stdQuestionaireRepository.getStdQuestionnaireByQuestionnaireId(questionnaireId);
				SelectedQuestionnaireModel selectedQuestionnaireModel = new SelectedQuestionnaireModel();
				selectedQuestionnaireModel.setQuestionnaireId(questionnaireId);
				selectedQuestionnaireModel.setRoleId(createRoleModel);
				selectedQuestionnaireModel.setStandardId(stdQuestionaireModel.getStdId().getStdId());
				try {
					selectedQuestionnaireRepository.save(selectedQuestionnaireModel);
				} catch (Exception ex) {
					LOGGER.error(ex.getMessage());
				}
			}
		}
		//If the customer choose some of the Standards AND choose some of the Questionnaires
		else if (standardIds != null && questionnaireIds != null) {
			for (Integer standardId : standardIds) {
				roleModel.setAreAllStandardsSelected(false);

				SelectedStandardModel selectedStandardModel = new SelectedStandardModel();
				selectedStandardModel.setStandardId(standardId);
				selectedStandardModel.setRoleId(createRoleModel);
				try {
					selectedStandardsRepository.save(selectedStandardModel);
				} catch (Exception ex) {
					LOGGER.error(ex.getMessage());
				}
			}
			for (Integer questionnaireId : questionnaireIds) {
				roleModel.setAreAllQuestionnairesSelected(false);

				StdQuestionaireModel stdQuestionaireModel = stdQuestionaireRepository.getStdQuestionnaireByQuestionnaireId(questionnaireId);
				SelectedQuestionnaireModel selectedQuestionnaireModel = new SelectedQuestionnaireModel();
				selectedQuestionnaireModel.setQuestionnaireId(questionnaireId);
				selectedQuestionnaireModel.setRoleId(createRoleModel);
				selectedQuestionnaireModel.setStandardId(stdQuestionaireModel.getStdId().getStdId());
				try {
					selectedQuestionnaireRepository.save(selectedQuestionnaireModel);
				} catch (Exception ex) {
					LOGGER.error(ex.getMessage());
				}
			}
		}

		createRoleModel.setPermissionList(roleModel.getPermissionList());
		createPermission(createRoleModel);
		return createRoleModel;
	}

	public RoleModel duplicateRole(int duplicatedRoleId) throws Exception {
		LOGGER.info("duplicate role id: " + duplicatedRoleId + ".");
		RoleModel newRoleModel = new RoleModel();
		RoleModel duplicatedRole = getById(duplicatedRoleId);
		newRoleModel.setRoleName("(Copy) " + duplicatedRole.getRoleName());
		newRoleModel.setCmpId(duplicatedRole.getCmpId());
		newRoleModel.setEditable(true);
		RoleModel createRoleModel = roleRepository.save(newRoleModel);
		createRoleModel.setPermissionList(duplicateAndCreatePermissions(createRoleModel, duplicatedRole));
		createRoleModel.setPermissionModels(createRoleModel.getPermissionList());
		return createRoleModel;
	}

	public void createPermission(RoleModel roleModel) throws Exception {
		List<PermissionModel> permissionModels = roleModel.getPermissionList();
		if (permissionModels != null && permissionModels.size() > 0) {
			for (PermissionModel tmpPermissionModel : permissionModels) {
				tmpPermissionModel.setRoleId(roleModel);
				permissionService.create(tmpPermissionModel);
			}
		}
	}

	public List<PermissionModel> duplicateAndCreatePermissions(RoleModel newRoleModel, RoleModel duplicatedRole) throws Exception {
		List<PermissionModel> duplicatedPermissionList = permissionService.getPermissionListByRoleId(duplicatedRole.getRoleId());
		List<PermissionModel> newPermissionList = new ArrayList<>();
		if (duplicatedPermissionList != null && duplicatedPermissionList.size() > 0) {
			for (PermissionModel tmpPermissionModel : duplicatedPermissionList) {
				PermissionModel newPermission = new PermissionModel(tmpPermissionModel);
				newPermission.setRoleId(newRoleModel);
				PermissionModel newPermissionModel = permissionService.create(newPermission);
				newPermissionList.add(newPermissionModel);
			}
		}
		return newPermissionList;
	}

	public void createDefaultRolesForCompany(CompanyModel companyModel) throws Exception {
		String[] defaultRolesArr = {"Ciso", "Supervisor", "Team Member", "Outsource", "Team Leader", "Auditor"};
		ModuleEnum[] moduleArr = {ModuleEnum.NEWS, ModuleEnum.CVE, ModuleEnum.PROJECT, ModuleEnum.TASK, ModuleEnum.CUSTOMER_STANDARD, ModuleEnum.ASSET_MANAGEMENT, ModuleEnum.RISK_ASSESSMENTS, ModuleEnum.AUTH};
		for (int i = 0; i < defaultRolesArr.length; i++) {
			RoleModel roleModel = new RoleModel();
			roleModel.setCmpId(companyModel);
			roleModel.setRoleName(defaultRolesArr[i]);
			roleModel.setEditable(true);
			List<PermissionModel> permissionModels = new ArrayList<>();
			for (int j = 0; j < moduleArr.length; j++) {
				PermissionModel permissionModel;

				//Setting all permissions to True
				if (
					(i == 0 && j == 2) || //Ciso: All permissions for Projects.
						(i == 0 && j == 3) || //Ciso: All permissions for Tasks.
						(i == 0 && j == 4) || //Ciso: All permissions for Standards.
						(i == 0 && j == 5) || //Ciso: All permissions for Assets.
						(i == 0 && j == 6) || //Ciso: All permissions for Vulnerability.
						(i == 0 && j == 7) || //Ciso: All permissions for Roles.
						(i == 1 && j == 2) || //Supervisor: All permissions for Projects.
						(i == 1 && j == 3) || //Supervisor: All permissions for Tasks.
						(i == 1 && j == 4) || //Supervisor: All permissions for Standards.
						(i == 1 && j == 5) || //Supervisor: All permissions for Assets.
						(i == 1 && j == 6)  //Supervisor: All permissions for Vulnerability.
				) {
					permissionModel = setPermission(true, true, true, true, moduleArr[j]);
					permissionModels.add(permissionModel);
				}

				//Setting all permissions to False
				if (
					(i == 1 && j == 7) || //Supervisor: No permissions for Roles.
						(i == 2 && j == 4) || //Team Member: No permissions for Standards.
						(i == 2 && j == 5) || //Team Member: No permissions for Assets.
						(i == 2 && j == 6) || //Team Member: No permissions for Vulnerability.
						(i == 2 && j == 7) || //Team Member: No permissions for Roles.
						(i == 3 && j == 4) || //Outsource: No permissions for Standards.
						(i == 3 && j == 5) || //Outsource: No permissions for Assets.
						(i == 3 && j == 6) || //Outsource: No permissions for Vulnerability.
						(i == 3 && j == 7) || //Outsource: No permissions for Roles.
						(i == 4 && j == 4) || //Team Leader: No permissions for Standards.
						(i == 4 && j == 5) || //Team Leader: No permissions for Assets.
						(i == 4 && j == 6) || //Team Leader: No permissions for Vulnerability.
						(i == 4 && j == 7) || //Team Leader: No permissions for Roles.
						(i == 5 && j == 7)    //Auditor: No permissions for Roles.
				) {
					permissionModel = setPermission(false, false, false, false, moduleArr[j]);
					permissionModels.add(permissionModel);
				}

				// Setting all permissions to False except for "read only" to True
				if (
					(i == 2 && j == 2) || //Team Member: read only for Projects.
						(i == 3 && j == 2) || //Outsource: read only for Projects.
						(i == 5 && j == 2) || //Auditor: read only for Projects.
						(i == 5 && j == 3) || //Auditor: read only for Tasks.
						(i == 5 && j == 4) ||  //Auditor: read only for Standards.
						(i == 5 && j == 5) || //Auditor: read only for Assets.
						(i == 5 && j == 6)   //Auditor: read only for Vulnerability.
				) {
					permissionModel = setPermission(false, true, false, false, moduleArr[j]);
					permissionModels.add(permissionModel);
				}
				// Setting all permissions to True except for "delete" to False(Just CVE's and New's)
				if (
					(i == 0 && j == 0) || //Ciso: read, create and edit only for New's.
						(i == 0 && j == 1) || //Ciso: read, create and edit only for CVE's.
						(i == 1 && j == 0) || //Supervisor: read only for New's.
						(i == 1 && j == 1) || //Supervisor: read only for New's.
						(i == 2 && j == 0) || //Outsource: read only for New's.
						(i == 2 && j == 1) || //Outsource: read only for New's.
						(i == 3 && j == 0) || //Outsource: read only for New's.
						(i == 3 && j == 1) || //Outsource: read only for CVE's.
						(i == 4 && j == 0) || //Team Leader: read only for New's.
						(i == 4 && j == 1) || //Team Leader: read only for CVE's.
						(i == 5 && j == 0) || //Auditor: read only for New's.
						(i == 5 && j == 1)    //Auditor: read only for CVE's.
				) {
					permissionModel = setPermission(true, true, true, false, moduleArr[j]);
					permissionModels.add(permissionModel);
				}
				// Setting all permissions to True except for "create" And"delete" to False(Just CVE's and New's)
				if (
					(i == 2 && j == 3) || //Team Member: read and edit only for Tasks.
						(i == 3 && j == 3) || //Outsource: read and edit only for Tasks.
						(i == 4 && j == 2) || //Team Leader: read and edit only for Projects
						(i == 4 && j == 3)    //Team Leader: read and edit only for Tasks

				) {
					permissionModel = setPermission(false, true, true, false, moduleArr[j]);
					permissionModels.add(permissionModel);
				}
			}
			roleModel.setPermissionList(permissionModels);
			create(roleModel);
		}
	}

	public PermissionModel setPermission(boolean isCreate, boolean isRead, boolean isUpdate, boolean isDelete, ModuleEnum module) {
		PermissionModel permissionModel = new PermissionModel();
		permissionModel.setIsCreate(isCreate);
		permissionModel.setIsUpdate(isUpdate);
		permissionModel.setIsRead(isRead);
		permissionModel.setIsDelete(isDelete);
		permissionModel.setModule(module);
		return permissionModel;
	}

	public List<RoleModel> getAllRolesByCmpId(int cmpId) {
		LOGGER.info("get all roles by CmpId .");
		return roleRepository.getAllRolesByCmpId(cmpId);
	}

	public RoleModel getLdapRolesBySpecificName(String roleName) {
		LOGGER.info("get ldap role by role name: " + roleName + ".");
		return roleRepository.getRolesBySpecificName(roleName).get(0);
	}

	public RoleModel getRoleModelByCmpUsrId(int cmpUsrId) {
		LOGGER.info("get role model by CmpUsrId: " + cmpUsrId);
		return roleRepository.getRoleModelByCmpUsrId(cmpUsrId);
	}

}
