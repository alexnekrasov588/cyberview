package com.lstechs.ciso.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.lstechs.ciso.model.CompanyUserModel;
import com.lstechs.ciso.model.RoleModel;
import com.lstechs.ciso.model.SingleCompanyUserModel;
import com.lstechs.ciso.repository.CompanyUserRepository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

@Service
public class CompanyUserService extends CRUDService<CompanyUserModel, SingleCompanyUserModel> {

	@Autowired
	private CompanyUserRepository companyUserRepository;

	@Autowired
	private RoleService roleService;

	@Autowired
	private PasswordEncoder bcryptEncoder;

	@Autowired
	private VendorUserService vendorUserService;

	private static final Logger LOGGER = LogManager.getLogger(CompanyUserService.class.getName());

	public CompanyUserService(CompanyUserRepository companyUserRepository) {
		super(companyUserRepository);
	}

	@Override
	public CompanyUserModel update(int id, CompanyUserModel companyUserModel) throws Exception {
		LOGGER.info("update company user , id: " + id + ".");
		CompanyUserModel companyUser = getCompanyUserByEmail(companyUserModel.getCmpUsrEmail());
		if (companyUser != null && companyUser.getCmpUsrId() != companyUserModel.getCmpUsrId()) {
			LOGGER.error("Email already exist");
			throw new Exception("Email already exist");
		}
		CompanyUserModel updateCompanyUser = getById(id);
		updateCompanyUser.setCmpUsrCmpId(companyUserModel.getCmpUsrCmpId());
		updateCompanyUser.setCmpUsrEmail(companyUserModel.getCmpUsrEmail());
		updateCompanyUser.setCmpUsrFullName(companyUserModel.getCmpUsrFullName());
		updateCompanyUser.setCmpUsrRoleId(companyUserModel.getCmpUsrRoleId());

		if (companyUserModel.getCmpUsrPassword() != null) {
			validatePassword(companyUserModel);
			updateCompanyUser.setCmpUsrPassword(bcryptEncoder.encode(companyUserModel.getCmpUsrPassword()));
			companyUserRepository.changePassword(id, updateCompanyUser.getCmpUsrPassword());
		}
		return companyUserRepository.save(updateCompanyUser);
	}

	public CompanyUserModel updateUserDetails(int id, CompanyUserModel companyUserModel) throws Exception {
		LOGGER.info("update company user , id: " + id + ".");
		CompanyUserModel updateCompanyUser = getById(id);
		updateCompanyUser.setCmpUsrFullName(companyUserModel.getCmpUsrFullName());
		return companyUserRepository.save(updateCompanyUser);
	}

	@Override
	public CompanyUserModel create(CompanyUserModel companyUserModel) throws Exception {
		LOGGER.info("create new company user.");
		if (loadUserByEmail(companyUserModel.getCmpUsrEmail()) != null) {
			LOGGER.error("Email already exist");
			throw new Exception("Email already exist");
		}

		validatePassword(companyUserModel);
		CompanyUserModel newCompanyUserModel = new CompanyUserModel();
		newCompanyUserModel.setCmpUsrPassword(bcryptEncoder.encode(companyUserModel.getCmpUsrPassword()));
		newCompanyUserModel.setCmpUsrCmpId(companyUserModel.getCmpUsrCmpId());
		newCompanyUserModel.setCmpUsrEmail(companyUserModel.getCmpUsrEmail());
		newCompanyUserModel.setCmpUsrFullName(companyUserModel.getCmpUsrFullName());
		newCompanyUserModel.setCmpUsrRoleId(companyUserModel.getCmpUsrRoleId());
		return companyUserRepository.save(newCompanyUserModel);
	}

	private void validatePassword(CompanyUserModel companyUserModel) throws Exception {
		UserDetails userDetails = vendorUserService.loadUserByEmail(companyUserModel.getCmpUsrEmail().toLowerCase());
		if (userDetails != null) {
			if (bcryptEncoder.matches(companyUserModel.getCmpUsrPassword(), userDetails.getPassword())) {
				LOGGER.error("Email already exist with this password in vendor user");
				throw new Exception("Please choose a different password");
			}
		}
	}

	public UserDetails loadUserByEmail(String email) {
		LOGGER.info("Load company user by email: " + email);
		CompanyUserModel companyUserModel = companyUserRepository.findCompanyUserModelByCmpUsrAndCmpUsrEmail(email.toLowerCase());
		if (companyUserModel == null) {
			LOGGER.error("cannot find company user with email: " + email + ".");
			return null;
		}
		return new User(companyUserModel.getCmpUsrEmail(), companyUserModel.getCmpUsrPassword(), new ArrayList<>());
	}

	public CompanyUserModel getCompanyUserByEmail(String email) {
		LOGGER.info("Get company user by email : " + email + ".");
		return companyUserRepository.findCompanyUserModelByCmpUsrAndCmpUsrEmail(email.toLowerCase());
	}

	public void changePassword(int id, String newPassword, String oldPassword) throws Exception {
		LOGGER.info("change company user password , id: " + id + ".");
		CompanyUserModel companyUserModel = getById(id);

		if (bcryptEncoder.matches(oldPassword, companyUserModel.getCmpUsrPassword())) {
			companyUserRepository.changePassword(id, bcryptEncoder.encode(newPassword));
		} else {
			LOGGER.error("change company user password  , id:" + id + " failed , Current password is incorrect.");
			throw new Exception("Current password is incorrect.");
		}
	}

	public Page<CompanyUserModel> getUsersComapny(Integer pageNo, Integer pageSize, String orderBy, boolean orderByDesc, String query, int id) throws Exception {
		if (orderBy.equals("roleName")) {
			orderBy = "cmpUsrRoleId.roleName";
		}
		LOGGER.info("get users company id: " + id);
		Sort sort = Sort.by(Sort.Order.desc(orderBy));
		if (!orderByDesc) {
			sort = Sort.by(Sort.Order.asc(orderBy));
		}
		Pageable paging = PageRequest.of(pageNo, pageSize, sort);
		return companyUserRepository.findUsersCompany(paging, query.toLowerCase(), id);
	}

	public void resetPassword(int id, String password) throws Exception {
		LOGGER.info("Reset company user password , id: " + id + ".");
		CompanyUserModel companyUserModel = new CompanyUserModel(getById(id));
		companyUserModel.setCmpUsrPassword(password);
		validatePassword(companyUserModel);
		companyUserRepository.changePassword(id, bcryptEncoder.encode(password));
	}

	public List<CompanyUserModel> getUsersByCmpId(int cmpId) {
		LOGGER.info("get users by cmpId: " + cmpId + ".");
		return companyUserRepository.getUsersByCmpId(cmpId);
	}

	public int getActiveUsersCountByCmpId(int cmpId) {
		LOGGER.info("Get active users count by cmpId: " + cmpId + ".");
		return companyUserRepository.getUsersCountByCmpId(cmpId);
	}

	public CompanyUserModel storeCmpUsrLogoFile(MultipartFile file, int id) throws Exception {
		if (file == null) {
			return updateProfileImgFile(null, id, "Delete profile image to user with id: ");
		}
		String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));

		try {
			if (file.getContentType() == null) {
				throw new Exception("File not found.");
			} else if (!file.getContentType().startsWith("image/")) {
				throw new Exception("The selected file type is not an image type!");
			}

			if (fileName.contains("..")) {
				throw new Exception("Sorry! Filename contains invalid path sequence " + fileName);
			}
			return updateProfileImgFile(file.getBytes(), id, "Upload profile image to user with id: ");

		} catch (Exception ex) {
			throw new Exception("Could not store file " + fileName + ". Please try again!", ex);
		}
	}

	private CompanyUserModel updateProfileImgFile(byte[] file, int id, String infoDescription) throws Exception {
		LOGGER.info("Get user company id: " + id);
		CompanyUserModel updateCompanyUser = getById(id);

		LOGGER.info(infoDescription + id + ".");
		updateCompanyUser.setCmpUsrProfileImgFile(file);
		return companyUserRepository.save(updateCompanyUser);
	}

	@Override
	public SingleCompanyUserModel getSingleInstance(CompanyUserModel companyUserModel) throws Exception {
		RoleModel roleModel = roleService.getById(companyUserModel.getCmpUsrRoleId().getRoleId());
		companyUserModel.setCmpUsrRoleId(roleModel);
		return new SingleCompanyUserModel(companyUserModel);
	}

	public SingleCompanyUserModel getSingleCompanyUSerModelById(int id) throws Exception {
		CompanyUserModel companyUserModel = new CompanyUserModel(getById(id));
		return new SingleCompanyUserModel(companyUserModel);
	}
}
