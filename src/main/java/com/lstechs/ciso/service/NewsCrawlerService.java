package com.lstechs.ciso.service;

import com.lstechs.ciso.crawlers.WebsiteConfiguration;
import com.lstechs.ciso.crawlers.WebsiteCrawler;
import com.lstechs.ciso.crawlers.json.JsonReader;
import com.lstechs.ciso.crawlers.rss.FeedMessage;
import com.lstechs.ciso.crawlers.rss.RSSFeedParser;
import com.lstechs.ciso.enums.NewsSrcTypeEnum;
import com.lstechs.ciso.model.NewsArticlesModel;
import com.lstechs.ciso.model.NewsSourcesModel;
import com.lstechs.ciso.model.NewsTagsModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.util.ArrayList;
import java.util.List;

@Service
@EnableScheduling
public class NewsCrawlerService {

	@Autowired
	private NewsArticlesService newsArticlesService;

	@Autowired
	private NewsArticlesTagsService newsArticlesTagsService;

	@Autowired
	private NewsTagsService newsTagsService;

	@Autowired
	private NewsSourcesService newsSourcesService;

	private static final Logger LOGGER = LogManager.getLogger(NewsCrawlerService.class.getName());

	@Async
	@Scheduled(cron = "${crawler.news.cron}")
	public void scanSites() {
		// get all news sources in the DB
		List<NewsSourcesModel> sources = newsSourcesService.getAllActive();
		List<WebsiteConfiguration> websiteList = new ArrayList<>();
		List<RSSFeedParser> rssList = new ArrayList<>();
		List<JsonReader> apiList = new ArrayList<>();

		for (NewsSourcesModel source : sources) {
			if (source.getNewsSrcType().equals(NewsSrcTypeEnum.WEB_SITE)) {
				websiteList.add(new WebsiteConfiguration(source.getNewsSrcSiteUrl(), source.getNewsNextPageBtnSelector(), source.getNewsArticleUrlSelector(), source.getNewsArticleContentContainerSelector(), source.getNewsArticleTitleSelector(), source.getNewsImgSelector(), source, source.getNewsDateSelector()));
			} else if (source.getNewsSrcType().equals(NewsSrcTypeEnum.RSS)) {
				rssList.add(new RSSFeedParser(source.getNewsSrcSiteUrl(), source));
			} else {
				apiList.add(new JsonReader(source.getNewsSrcSiteUrl(), source.getNewsArticleTitleSelector(), source.getNewsArticleContentContainerSelector(), source));
			}
		}

		LOGGER.info("Found " + websiteList.size() + " websites to scan");
		LOGGER.info("Found " + rssList.size() + " RSS Feeds to scan");
		LOGGER.info("Found " + apiList.size() + " API's to scan");

		// SCAN REGULAR WEBSITES
		for (WebsiteConfiguration config : websiteList) {
			new Thread(() -> {
				List<String> existingHashes = newsArticlesService.getAllArticleHashes();
				List<NewsArticlesModel> articles = new WebsiteCrawler(config).scanSite(existingHashes);
				for (NewsArticlesModel article : articles) {
					try {
						newsArticlesService.create(article);
					} catch (Exception e) {
						System.out.println(e.getMessage());
					}
				}
			}).start();
		}

		// SCAN RSS FEEDS
		for (RSSFeedParser parser : rssList) {
			List<FeedMessage> messages = parser.getArticles();
			for (FeedMessage message : messages) {
				NewsArticlesModel newsArticlesModel;
				try {
					NewsSourcesModel newsSourcesModel = (NewsSourcesModel) parser.getSourcesModel();
					ArrayList<String> returnedArray = WebsiteCrawler.extractArticleContentFromRSS(message.getLink(), newsSourcesModel.getNewsArticleContentContainerSelector(), newsSourcesModel.getNewsSrcBaseUrl());
					String content = "";
					if (returnedArray.get(0).isEmpty()) {
						content = message.getDescription();
					} else {
						content = returnedArray.get(0);
					}
					newsArticlesModel = newsArticlesService.create(new NewsArticlesModel(message.getTitle(), message.getLink(), message.getImageUrl(), (NewsSourcesModel) parser.getSourcesModel(), content, returnedArray.get(1), true, message.getDate()));
					if (newsArticlesModel != null) {
						// build tags list
						for (String tagName : message.getTagsList()) {
							List<NewsTagsModel> tagModel = newsTagsService.getByTagName(tagName.toLowerCase());
							if (tagModel.size() == 0) {
								try {
									tagModel.add(newsTagsService.create(new NewsTagsModel(tagName.toLowerCase())));
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
							newsArticlesTagsService.create(newsArticlesModel, tagModel.get(0));
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		// SCAN API URLS
		for (JsonReader jsonReader : apiList) {
			List<String> existingHashes = newsArticlesService.getAllArticleHashes();
			List<NewsArticlesModel> articleList = jsonReader.scan();
			for (NewsArticlesModel newsArticlesModel : articleList) {
				String titleHash = DigestUtils.md5DigestAsHex(newsArticlesModel.getNewsArtcUrl().getBytes());
				if (!existingHashes.contains(titleHash)) {
					try {
						ArrayList<String> returnedArray = WebsiteCrawler.extractArticleContent(newsArticlesModel.getNewsArtcUrl(), jsonReader.getDescriptionKey());
						if (!returnedArray.get(0).isEmpty()) {
							newsArticlesModel.setNewsArtcContent(returnedArray.get(0));
							newsArticlesModel.setNewsArtcSummary(returnedArray.get(1));
						}
						newsArticlesService.create(newsArticlesModel);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
}

