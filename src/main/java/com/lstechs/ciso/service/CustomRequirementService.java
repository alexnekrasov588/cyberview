package com.lstechs.ciso.service;

import com.lstechs.ciso.model.*;
import com.lstechs.ciso.repository.CustomRequirementRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomRequirementService extends CRUDService<CustomRequirementModel, SingleCustomRequirementModel> {

	@Autowired
	private CustomRequirementRepository customRequirementRepository;

	@Autowired
	private CrossReferenceService crossReferenceService;

	private static Logger LOGGER = LogManager.getLogger(CustomRequirementService.class.getName());


	public CustomRequirementService(CustomRequirementRepository customRequirementRepository) {
		super(customRequirementRepository);
	}

	@Override
	public CustomRequirementModel delete(int id) throws Exception {
		crossReferenceService.deleteByCstRqrId(id);
		return super.delete(id);
	}

	@Override
	public SingleCustomRequirementModel getSingleInstance(CustomRequirementModel customRequirementModel) throws Exception {
		List<SingleCrossReferenceModel> singleCrossReferenceModels = new ArrayList<>();
		for(CrossReferenceModel crossReferenceModel : customRequirementModel.getCrossReferenceModels()){
			SingleCrossReferenceModel singleCrossReferenceModel = crossReferenceService.getSingleInstance(crossReferenceModel);
			if (singleCrossReferenceModel != null)
				singleCrossReferenceModels.add(singleCrossReferenceModel);
		}
		return new SingleCustomRequirementModel(customRequirementModel, singleCrossReferenceModels);
	}

	public CustomRequirementModel requestParser(RequestCustomRequirementModel requestCustomRequirementModel) throws Exception {
		LOGGER.info("requestParser");
		CustomRequirementModel newCstRqr;

		if (requestCustomRequirementModel.getCstRqrId() == 0) {
			LOGGER.info("Create new Custom Requirement");
			newCstRqr = customRequirementRepository.save(new CustomRequirementModel(requestCustomRequirementModel.getCstRqrDescription()));

		} else {
			LOGGER.info("Update Custom Requirement id:" + requestCustomRequirementModel.getCstRqrId() + ".");
			newCstRqr = getById(requestCustomRequirementModel.getCstRqrId());
			newCstRqr.setCstRqrDescription(requestCustomRequirementModel.getCstRqrDescription());
			crossReferenceService.deleteByCstRqrId(requestCustomRequirementModel.getCstRqrId());

		}
		List<CrossReferenceModel> newCrossReferenceModelList = crossReferenceService.createCrossReferenceOfCstRqrId(newCstRqr, requestCustomRequirementModel.getCrossReferenceModel());
		newCstRqr.setCrossReferenceModels(newCrossReferenceModelList);
		customRequirementRepository.save(newCstRqr);
		return newCstRqr;
	}

}
