package com.lstechs.ciso.service;

import com.lstechs.ciso.model.SingleUserProjectModel;
import com.lstechs.ciso.model.UserProjectModel;
import com.lstechs.ciso.repository.UserProjectRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserProjectService extends CRUDService<UserProjectModel, SingleUserProjectModel> {

	public UserProjectService(UserProjectRepository userProjectRepository) {
		super(userProjectRepository);
	}

	private static final Logger LOGGER = LogManager.getLogger(UserProjectService.class.getName());

	@Autowired
	private ProjectService projectService;

	@Autowired
	private CompanyUserService companyUserService;

	@Autowired
	UserProjectRepository userProjectRepository;

	@Override
	public SingleUserProjectModel getSingleInstance(UserProjectModel userProjectModel) throws Exception {
		return new SingleUserProjectModel(userProjectModel);
	}

	@Override
	public UserProjectModel create(UserProjectModel userProjectModel) throws Exception {
		LOGGER.info("create user project.");
		UserProjectModel newUserProjectModel = new UserProjectModel();
		newUserProjectModel.setPrjId(projectService.getById(userProjectModel.getPrjId().getPrjId()));
		newUserProjectModel.setUsrId(companyUserService.getById(userProjectModel.getUsrId().getCmpUsrId()));
		return userProjectRepository.save(newUserProjectModel);
	}

	@Override
	public UserProjectModel update(int id, UserProjectModel userProjectModel) throws Exception {
		LOGGER.info("update user project.");
		UserProjectModel updateUserProjectModel = getById(id);
		updateUserProjectModel.setPrjId(projectService.getById(userProjectModel.getPrjId().getPrjId()));
		updateUserProjectModel.setUsrId(companyUserService.getById(userProjectModel.getUsrId().getCmpUsrId()));
		return userProjectRepository.save(updateUserProjectModel);
	}

	public List<UserProjectModel> getByProjectId(int id){
		LOGGER.info("get by project id: " + id);
		return userProjectRepository.getAllUsersByProjId(id);
	}
}
