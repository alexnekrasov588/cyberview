package com.lstechs.ciso.service;

import com.lstechs.ciso.model.LicenseModel;
import com.lstechs.ciso.model.LicenseStandardModel;
import com.lstechs.ciso.model.SingleCompanyUserModel;
import com.lstechs.ciso.model.StandardModel;
import com.lstechs.ciso.repository.LicenseStandardRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class LicenseStandardService extends CRUDService<LicenseStandardModel, SingleCompanyUserModel> {

	private static final Logger LOGGER = LogManager.getLogger(LicenseStandardService.class.getName());

	@Autowired
	private LicenseStandardRepository licenseStandardRepository;

	@Autowired
	private StandardService standardService;

	public LicenseStandardService(LicenseStandardRepository licenseStandardRepository) {
		super(licenseStandardRepository);
	}

	public void deleteLicenseStandardByStandardId(int id) {
		LOGGER.info("delete licenseStandard by standardId: " + id);
		licenseStandardRepository.deleteLicenseStandardByStandard(id);
	}

	public void deleteLicenseStandardByLicenseId(int id) {
		LOGGER.info("delete licenseStandard by licenseId: " + id);
		licenseStandardRepository.deleteLicenseStandardByLicence(id);
	}

	public void createLicenseStandard(List<Integer> standardList, LicenseModel licenseModel) throws Exception {
		if (standardList != null) {

			List<LicenseStandardModel> licenseStandardsModel = findAllLicenseStandardByLcnsId(licenseModel.getLcnsId());

			if (licenseStandardsModel.size() == 0) {
				for (int standardId : standardList) {
					StandardModel standardModel = standardService.getById(standardId);
					LicenseStandardModel licenseStandardModel = new LicenseStandardModel(licenseModel, standardModel);
					create(licenseStandardModel);
					LOGGER.info("create new license standard.");
				}
			} else {

				List<Integer> licenseStandardIds = new java.util.ArrayList<>(Collections.emptyList());

				for (LicenseStandardModel licenseStandard : licenseStandardsModel) {
					licenseStandardIds.add(licenseStandard.getStdId().getStdId());
				}
				standardList.removeAll(licenseStandardIds);
				for (int standardId : standardList) {
					StandardModel standardModel = standardService.getById(standardId);
					LicenseStandardModel licenseStandardModel = new LicenseStandardModel(licenseModel, standardModel);
					create(licenseStandardModel);
					LOGGER.info("create new license standard.");
				}
			}
		}
	}

	public List<LicenseStandardModel> findAllLicenseStandardByLcnsId(int id) {
		LOGGER.info("get all license Standard by licenseId: " + id);
		return licenseStandardRepository.findAllLicenseStandardByLcnsId(id);
	}
}
