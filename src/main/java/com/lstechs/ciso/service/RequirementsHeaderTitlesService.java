package com.lstechs.ciso.service;

import com.lstechs.ciso.model.RequirementsHeaderTitlesModel;
import com.lstechs.ciso.model.StdQuestionaireModel;
import com.lstechs.ciso.repository.RequirementsHeaderTitlesRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RequirementsHeaderTitlesService extends CRUDService<RequirementsHeaderTitlesModel, RequirementsHeaderTitlesModel> {

	@Autowired
	private RequirementsHeaderTitlesRepository requirementsHeaderTitlesRepository;

	@Autowired
	private StdQuestionaireService stdQuestionaireService;

	public RequirementsHeaderTitlesService(RequirementsHeaderTitlesRepository requirementsHeaderTitlesRepository) {
		super(requirementsHeaderTitlesRepository);
	}

	private static final Logger LOGGER = LogManager.getLogger(RequirementsHeaderTitlesService.class.getName());

	@Override
	public RequirementsHeaderTitlesModel create(RequirementsHeaderTitlesModel requirementsHeaderTitlesModel) throws Exception {
		try {
			return super.create(requirementsHeaderTitlesModel);
		} catch (Exception ex) {
			LOGGER.error("create Requirements Titles failed , ex: " + ex);
			throw new Exception(ex);
		}
	}

	/**
	 * @param requirementsHeaderTitlesModel requirementsHeaderTitlesModel
	 * @param questionnaireId               questionnaireId
	 * @return savedRequirementsHeaderTitlesModel
	 */
	public RequirementsHeaderTitlesModel saveRequirementsHeaderTitles(RequirementsHeaderTitlesModel requirementsHeaderTitlesModel, int questionnaireId) throws Exception {

		StdQuestionaireModel stdQuestionaireModel = stdQuestionaireService.getById(questionnaireId);
		RequirementsHeaderTitlesModel savedRequirementsHeaderTitlesModel = new RequirementsHeaderTitlesModel();

		if (stdQuestionaireModel.isStandardCreatedByCompany()) {
			if (requirementsHeaderTitlesModel != null && questionnaireId > 0) {
				try {
					RequirementsHeaderTitlesModel requirementsHeaderTitlesModelByQuestionnaireId = requirementsHeaderTitlesRepository.getRequirementsHeaderTitlesByQuestionnaireId(questionnaireId);

					// Setting the titles
					requirementsHeaderTitlesModelByQuestionnaireId.setRequirementTitle(requirementsHeaderTitlesModel.getRequirementTitle());
					requirementsHeaderTitlesModelByQuestionnaireId.setDescriptionTitle(requirementsHeaderTitlesModel.getDescriptionTitle());
					requirementsHeaderTitlesModelByQuestionnaireId.setExplanationTitle(requirementsHeaderTitlesModel.getExplanationTitle());
					requirementsHeaderTitlesModelByQuestionnaireId.setArticleNumberTitle(requirementsHeaderTitlesModel.getArticleNumberTitle());
					requirementsHeaderTitlesModelByQuestionnaireId.setGenericAnswer1Title(requirementsHeaderTitlesModel.getGenericAnswer1Title());
					requirementsHeaderTitlesModelByQuestionnaireId.setGenericAnswer2Title(requirementsHeaderTitlesModel.getGenericAnswer2Title());

					// Saving the titles
					savedRequirementsHeaderTitlesModel = requirementsHeaderTitlesRepository.save(requirementsHeaderTitlesModelByQuestionnaireId);
				} catch (Exception ex) {
					LOGGER.info(ex.getMessage());
				}
			} else {
				LOGGER.info("RequirementsHeaderTitlesModel Or QuestionnaireId Is Null");
				throw new NullPointerException("RequirementsHeaderTitlesModel Or questionnaireId Is Null");
			}
		} else {
			LOGGER.info("Standard Created By Vendor User");
			throw new Exception("Standard Created By Vendor User");
		}
		return savedRequirementsHeaderTitlesModel;
	}

	public RequirementsHeaderTitlesModel getRequirementsHeaderTitlesByQuestionnaireId(int questionnaireId) {

		RequirementsHeaderTitlesModel savedRequirementsHeaderTitlesModel = new RequirementsHeaderTitlesModel();

		try {
			savedRequirementsHeaderTitlesModel = requirementsHeaderTitlesRepository.getRequirementsHeaderTitlesByQuestionnaireId(questionnaireId);
		} catch (Exception ex) {
			LOGGER.info(ex.getMessage());
		}

		return savedRequirementsHeaderTitlesModel;
	}
}
