package com.lstechs.ciso.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import com.lstechs.ciso.repository.FavouriteCVERepository;
import com.lstechs.ciso.repository.FavouriteNewsRepository;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import com.lstechs.ciso.enums.FolderTypeEnum;
import com.lstechs.ciso.model.CompanyUserModel;
import com.lstechs.ciso.model.FavouriteCVEModel;
import com.lstechs.ciso.model.FavouriteFoldersModel;
import com.lstechs.ciso.model.FavouriteNewsModel;
import com.lstechs.ciso.model.FilterModel;
import com.lstechs.ciso.model.FoldersChildrens;
import com.lstechs.ciso.model.FoldersStructure;
import com.lstechs.ciso.repository.FavouriteFoldersRepository;

@Service
public class FavouriteFoldersService extends CRUDService<FavouriteFoldersModel, FavouriteFoldersModel> {

	public FavouriteFoldersService(FavouriteFoldersRepository favouriteFoldersRepository) {
		super(favouriteFoldersRepository);
	}

	@Autowired
	private FavouriteFoldersRepository favouriteFoldersRepository;

	@Autowired
	private AuthenticationService authenticationService;

	@Autowired
	private FavouriteNewsRepository favouriteNewsRepository;

	@Autowired
	private FavouriteCVERepository favouriteCVERepository;

	public List<FavouriteFoldersModel> getFavoriteFolders(String token, FolderTypeEnum favType) {
		CompanyUserModel companyUserModel = authenticationService.getCompanyUserFromToken(token.substring(7));
		return favouriteFoldersRepository.getFavoriteFolders(companyUserModel.getCmpUsrId(), favType);
	}

	@Override
	public FavouriteFoldersModel create(FavouriteFoldersModel favouriteFoldersModel) throws Exception {
		if (favouriteFoldersModel.getParent() != null && favouriteFoldersModel.getParent().getFavId() != 0) {
			FavouriteFoldersModel parent = getById(favouriteFoldersModel.getParent().getFavId());
			favouriteFoldersModel.setParent(parent);
		}
		return super.create(favouriteFoldersModel);
	}

	@Override
	public FavouriteFoldersModel update(int id, FavouriteFoldersModel favouriteFoldersModel) throws Exception {
		FavouriteFoldersModel updateFavouriteFoldersModel = getById(id);
		updateFavouriteFoldersModel.setFavName(favouriteFoldersModel.getFavName());
		return favouriteFoldersRepository.save(updateFavouriteFoldersModel);
	}

	@Override
	public FavouriteFoldersModel delete(int id) throws Exception {
		List<FavouriteFoldersModel> child = favouriteFoldersRepository.getFavoriteFoldersByParent(id);
		if (child != null && child.size() > 0) {
			for (FavouriteFoldersModel tmpFolder : child) {
				deleteFavouriteFolders(tmpFolder);
			}
		}

		//Deleting the Favourite News If They Are Exist.
		List<FavouriteNewsModel> favouriteNewsModelList = favouriteNewsRepository.getNewsArticlesModelByFolderId(id);

		if (favouriteNewsModelList.size() > 0) {
			for (FavouriteNewsModel favouriteNewsModel : favouriteNewsModelList) {
				favouriteNewsRepository.delete(favouriteNewsModel);
			}
		}

		//Deleting the Favourite CVE's If They Are Exist.
		List<FavouriteCVEModel> favouriteCVEModelList = favouriteCVERepository.getNewsCVEsModelByFolderId(id);
		if (favouriteCVEModelList.size() > 0) {
			for (FavouriteCVEModel favouriteCVEModel : favouriteCVEModelList) {
				favouriteCVERepository.delete(favouriteCVEModel);
			}
		}
		return super.delete(id);
	}

	public void deleteAll(String token, FolderTypeEnum favType) throws Exception {
		CompanyUserModel companyUserModel = authenticationService.getCompanyUserFromToken(token.substring(7));
		int usrId = companyUserModel.getCmpUsrId();
		List<FavouriteFoldersModel> child = favouriteFoldersRepository.getFavoriteFolders(usrId, favType);
		if (child != null && child.size() > 0) {
			for (FavouriteFoldersModel tmpFolder : child) {
				super.delete(tmpFolder.getFavId());
			}
		}

		if (child != null && child.size() > 0) {
			for (FavouriteFoldersModel tmpFolder : child) {

				if(favType.equals(FolderTypeEnum.CVE)){
					//Deleting the Favourite CVE's If They Are Exist.
					List<FavouriteCVEModel> favouriteCVEModelList = favouriteCVERepository.getNewsCVEsModelByFolderId(tmpFolder.getFavId());
					if (favouriteCVEModelList.size() > 0) {
						for (FavouriteCVEModel favouriteCVEModel : favouriteCVEModelList) {
							favouriteCVERepository.delete(favouriteCVEModel);
						}
					}
				} else {
					//Deleting the Favourite News If They Are Exist.
					List<FavouriteNewsModel> favouriteNewsModelList = favouriteNewsRepository.getNewsArticlesModelByFolderId(tmpFolder.getFavId());
					if (favouriteNewsModelList.size() > 0) {
						for (FavouriteNewsModel favouriteNewsModel : favouriteNewsModelList) {
							favouriteNewsRepository.delete(favouriteNewsModel);
						}
					}
				}
			}
		}
	}

	private void deleteFavouriteFolders(FavouriteFoldersModel folder) throws Exception {
		List<FavouriteFoldersModel> child = favouriteFoldersRepository.getFavoriteFoldersByParent(folder.getFavId());
		if (child != null && child.size() > 0) {
			for (FavouriteFoldersModel tmpFolder : child) {
				deleteFavouriteFolders(tmpFolder);
			}
		}
		super.delete(folder.getFavId());

	}

	public List<FilterModel> getFavoriteFoldersOption(String token, FolderTypeEnum favType) throws ParseException {
		CompanyUserModel companyUserModel = authenticationService.getCompanyUserFromToken(token.substring(7));
		int usrId = companyUserModel.getCmpUsrId();
		return favouriteFoldersRepository.getFavoriteFoldersOption(usrId, favType);
	}

	public FoldersStructure getFoldersStructure(String token, FolderTypeEnum favType) {
		CompanyUserModel companyUserModel = authenticationService.getCompanyUserFromToken(token.substring(7));
		int usrId = companyUserModel.getCmpUsrId();
		List<FavouriteFoldersModel> parent = favouriteFoldersRepository.getFavoriteFoldersWithoutParents(usrId,
			favType);

		FoldersStructure foldersStructure = new FoldersStructure();
		List<FoldersChildrens> ch = new ArrayList<FoldersChildrens>();
		for (FavouriteFoldersModel tmpParent : parent) {
			if (tmpParent.getParent() == null) {
				FoldersChildrens children = new FoldersChildrens();
				children.setName(tmpParent.getFavName());
				children.setType("folder");
				children.setFolderId(tmpParent.getFavId());
				List<FoldersChildrens> childrens = new ArrayList<FoldersChildrens>();
				List<FavouriteFoldersModel> childFolders = favouriteFoldersRepository
					.getFavoriteFoldersByParent(tmpParent.getFavId());
				if (childFolders != null && childFolders.size() > 0) {
					for (FavouriteFoldersModel model : childFolders) {
						FoldersChildrens recursiveChild = new FoldersChildrens();
						recursiveChild = getChildrens(model, favType);
						childrens.add(recursiveChild);
					}
				}

				if (favType.equals(FolderTypeEnum.NEWS)) {
					if (tmpParent.getFavouriteNewsModels() != null && tmpParent.getFavouriteNewsModels().size() > 0) {
						for (FavouriteNewsModel news : tmpParent.getFavouriteNewsModels()) {
							if (news.getDeletedAt() == null) {
								FoldersChildrens childNews = new FoldersChildrens();
								childNews.setName(news.getFvrtNewsName());
								childNews.setType("file");
								childNews.setNewsId(news.getNewsArtcId().getNewsArtcId());
								childNews.setFolderId(tmpParent.getFavId());
								childrens.add(childNews);
							}

						}
					}
				} else if (favType.equals(FolderTypeEnum.CVE)) {
					if (tmpParent.getFavouriteCVEModels() != null && tmpParent.getFavouriteCVEModels().size() > 0) {
						for (FavouriteCVEModel news : tmpParent.getFavouriteCVEModels()) {
							if (news.getDeletedAt() == null) {
								FoldersChildrens childCve = new FoldersChildrens();
								childCve.setName(news.getFvrtCveName());
								childCve.setType("file");
								childCve.setNewsId(news.getCveId().getCveId());
								childCve.setFolderId(tmpParent.getFavId());
								childrens.add(childCve);
							}
						}
					}
				}
				children.setChildrens(childrens);
				ch.add(children);
			}
			foldersStructure.setChildrens(ch);

		}
		return foldersStructure;
	}

	private FoldersChildrens getChildrens(FavouriteFoldersModel favouriteFoldersModel, FolderTypeEnum favType) {
		FoldersChildrens recursiveChild = new FoldersChildrens();
		recursiveChild.setName(favouriteFoldersModel.getFavName());
		recursiveChild.setType("folder");
		recursiveChild.setFolderId(favouriteFoldersModel.getFavId());
		List<FoldersChildrens> childrens = new ArrayList<FoldersChildrens>();
		List<FavouriteFoldersModel> childFolders = favouriteFoldersRepository
			.getFavoriteFoldersByParent(favouriteFoldersModel.getFavId());
		if (childFolders != null && childFolders.size() > 0) {
			for (FavouriteFoldersModel model : childFolders) {
				childrens.add(getChildrens(model, favType));
			}
		}
		if (favType.equals(FolderTypeEnum.NEWS)) {
			if (favouriteFoldersModel.getFavouriteNewsModels() != null
				&& favouriteFoldersModel.getFavouriteNewsModels().size() > 0) {
				for (FavouriteNewsModel news : favouriteFoldersModel.getFavouriteNewsModels()) {
					if (news.getDeletedAt() == null) {
						FoldersChildrens childNews = new FoldersChildrens();
						childNews.setName(news.getFvrtNewsName());
						childNews.setType("file");
						childNews.setNewsId(news.getNewsArtcId().getNewsArtcId());
						childNews.setFolderId(favouriteFoldersModel.getFavId());
						childrens.add(childNews);
					}
				}
			}
		} else if (favType.equals(FolderTypeEnum.CVE)) {
			if (favouriteFoldersModel.getFavouriteCVEModels() != null
				&& favouriteFoldersModel.getFavouriteCVEModels().size() > 0) {
				for (FavouriteCVEModel news : favouriteFoldersModel.getFavouriteCVEModels()) {
					if (news.getDeletedAt() == null) {
						FoldersChildrens childCve = new FoldersChildrens();
						childCve.setName(news.getFvrtCveName());
						childCve.setType("file");
						childCve.setNewsId(news.getCveId().getCveId());
						childCve.setFolderId(favouriteFoldersModel.getFavId());

						childrens.add(childCve);
					}
				}
			}
		}
		recursiveChild.setChildrens(childrens);
		return recursiveChild;
	}

}
