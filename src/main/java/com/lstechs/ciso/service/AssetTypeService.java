package com.lstechs.ciso.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.lstechs.ciso.enums.AssetTypeEnum;
import com.lstechs.ciso.model.AssetTypeModel;
import com.lstechs.ciso.model.CompanyModel;
import com.lstechs.ciso.repository.AssetTypeRepository;

@Service
public class AssetTypeService extends CRUDService<AssetTypeModel, AssetTypeModel> {

	@Autowired
	public AssetTypeRepository assetRepository;

	public AssetTypeService(AssetTypeRepository assetTypeRepository) {
		super(assetTypeRepository);
	}

	private static final Logger LOGGER = LogManager.getLogger(AssetTypeService.class.getName());

	public Page<AssetTypeModel> getAllAssetTypeByCmpId(Integer pageNo, Integer pageSize, String orderBy,
													   boolean orderByDesc, String query, int cmpId) {
		LOGGER.info("Get all asset type by cmpId");

		Sort sort = Sort.by(Sort.Order.desc(orderBy));
		if (!orderByDesc) {
			sort = Sort.by(Sort.Order.asc(orderBy));
		}
		Pageable paging = PageRequest.of(pageNo, pageSize, sort);
		return assetRepository.getAllAssetTypeByCmpId(paging, cmpId, query);
	}

	public List<AssetTypeModel> getAssetTypeByCmpId(int cmpId, String type) {
		LOGGER.info("get asset type bt cmpId: " + cmpId);
		AssetTypeEnum assetTypeEnum = null;
		try {
			assetTypeEnum = AssetTypeEnum.valueOf(type);
		} catch (Exception ex) {
			LOGGER.info("get all asset");
		}

		return assetRepository.getAssetTypeByCmpId(cmpId, assetTypeEnum);
	}

	@Override
	public AssetTypeModel update(int id, AssetTypeModel assetTypeModel) throws Exception {
		AssetTypeModel updateAssetTypeModel = getById(id);
		LOGGER.info("update asset type id: " + id);
		updateAssetTypeModel.setAstTypeName(assetTypeModel.getAstTypeName());
		return assetRepository.save(updateAssetTypeModel);
	}

	public AssetTypeModel getAssetTypeByName(int cmpId, String name, AssetTypeEnum assetTypeEnum) {
		LOGGER.info("getAssetTypeByName");
		List<AssetTypeModel> assetTypeModels = assetRepository.getAssetTypeByName(cmpId, name.toLowerCase(), assetTypeEnum);
		if (assetTypeModels != null && assetTypeModels.size() > 0)
			return assetTypeModels.get(0);
		else
			return null;
	}

	public void createDefaultAssetTypePerCompany(CompanyModel companyModel) throws Exception {
		LOGGER.info("create default asset type for cmpId:" + companyModel.getCmpId());
		String[] assetNamesApp = {"Cloud/Application", "Web Application", "Local Application", "Other"};
		String[] assetNamesDevice = {"Desktop", "Laptop", "Local Storage", "Mobile Device", "Person", "Phone",
			"Removable Media", "Server", "Network Device", "Other"};
		for (String value : assetNamesApp) {
			AssetTypeModel assetTypeModel = new AssetTypeModel(value, AssetTypeEnum.APPLICATION,
				companyModel);
			create(assetTypeModel);
		}
		for (String s : assetNamesDevice) {
			AssetTypeModel assetTypeModel = new AssetTypeModel(s, AssetTypeEnum.DEVICES,
				companyModel);
			create(assetTypeModel);
		}
	}

	public List<AssetTypeModel> deleteAssetTypes(int[] assetTypeIds) throws Exception {
		List<AssetTypeModel> deletedAssetTypeModel = new ArrayList<>(Collections.emptyList());

		if (assetTypeIds.length > 0) {

			LOGGER.info("Delete assets : " + Arrays.toString(assetTypeIds));
			for (int assetTypeId : assetTypeIds) {
				LOGGER.info("Delete asset type id: " + assetTypeId);
				AssetTypeModel deletedAssetType = delete(assetTypeId);
				deletedAssetTypeModel.add(deletedAssetType);
			}
			return deletedAssetTypeModel;
		} else {
			LOGGER.info("Requested Asset Types List For Deletion Is Empty or Null");
			return null;
		}
	}
}
