package com.lstechs.ciso.service;

import com.lstechs.ciso.enums.DashboardEnum;
import com.lstechs.ciso.model.*;
import com.lstechs.ciso.repository.SelectedStandardsRepository;
import com.lstechs.ciso.repository.StandardRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class DashboardService {

	private static final Logger LOGGER = LogManager.getLogger(DashboardService.class.getName());

	@Autowired
	private StandardService standardService;

	@Autowired
	private ProjectService projectService;

	@Autowired
	private TaskService taskService;

	@Autowired
	private AuthenticationService authenticationService;

	@Autowired
	private RoleService roleService;

	@Autowired
	private NewsArticlesService articlesService;

	@Autowired
	private CVEService cveService;

	@Autowired
	private SelectedStandardsRepository selectedStandardsRepository;

	@Autowired
	private StandardRepository standardRepository;

	public List<Map<String, Object>> getNews(int cmpId, String token) {
		List<NewsArticlesModel> articlesModels = articlesService.findActiveNewsForDashboard();
		List<CVEModel> cve = cveService.findCVEModelForDashboard();
		List<CVEModel> cveAssetModels = cveService.findCVEModelForDashboardByAssets(token);
		List<NewsArticlesModel> articlesAssetModels = Collections.emptyList();
		try {
			articlesAssetModels = articlesService.findActiveAssetNewsForDashboard(cmpId);
		} catch (Exception ex) {
			LOGGER.error(ex.getMessage());
		}

		List<Map<String, Object>> list = new ArrayList<>();
		Map<String, Object> newsMap = new HashMap<>();
		newsMap.put("cve", cve);
		newsMap.put("news", articlesModels);
		newsMap.put("assetNews", articlesAssetModels);
		newsMap.put("assetCve", cveAssetModels);

		list.add(newsMap);
		return list;
	}


	public List<Map<String, Object>> getListMapStandardsDetails(String lcnsToken, int cmpId, String token) throws Exception {
		// get standards details from compliance standards
		List<Map<String, Object>> listMapStandardsDetails = new ArrayList<>();

		List<StandardModel> standardsWithoutReqs = standardService.getAllStandardWithoutReqsByToken(lcnsToken);

		//Selected Standards and Questionnaires Handling
		CompanyUserModel companyUserModel = authenticationService.getCompanyUserFromToken(token.substring(7));
		int roleId = companyUserModel.getCmpUsrRoleId().getRoleId();
		RoleModel role = roleService.getById(roleId);

		//Selected Standards
		ArrayList<Integer> standardIds = new ArrayList<>(Collections.emptyList());
		if (!role.isAreAllStandardsSelected()) {
			List<SelectedStandardModel> selectedStandardModelList = selectedStandardsRepository.getSelectedStandardModelByRoleId(role.getRoleId());

			for (SelectedStandardModel selectedStandardModel : selectedStandardModelList) {
				standardIds.add(selectedStandardModel.getStandardId());
			}

			List<StandardModel> newStandardsList = new ArrayList<>(Collections.emptyList());


			if (standardsWithoutReqs.size() == 0) {
				List<StandardModel> standardList = standardRepository.getStandardsByIds(standardIds);
//				for (StandardModel standardModel : standardList) {
//					if (standardModel.isStdIsCompleted()) {
//						Map<String, Object> standardsDetails = new HashMap<>();
//						standardsDetails.put("standardId", standardModel.getStdId());
//						standardsDetails.put("standardName", standardModel.getStdName());
//						standardsDetails.put("progress", 0);
//						listMapStandardsDetails.add(standardsDetails);
//					}
//				}
				List<Object[]> standardProgressModels = standardService.getCompliantStdReqCountByLcnsTokenAndCmpId(lcnsToken,
					cmpId);
				for (Object[] tmpStandardProgress : standardProgressModels) {
					for (Integer standardId : standardIds) {
						if (standardId == tmpStandardProgress[0]) {
							Map<String, Object> standardsDetails = new HashMap<>();
							standardsDetails.put("standardId", tmpStandardProgress[0]);
							standardsDetails.put("standardName", tmpStandardProgress[1]);
							standardsDetails.put("progress", tmpStandardProgress[2]);
							listMapStandardsDetails.add(standardsDetails);

						}
					}
				}
				return listMapStandardsDetails;

			} else {
				//Checking if the Selected Standards for this role is part of all standards by license token
				for (StandardModel standard : standardsWithoutReqs) {
					for (Integer standardId : standardIds) {
						if (standardId == standard.getStdId()) {
							newStandardsList.add(standard);
						}
					}
				}

				for (StandardModel standardModel : newStandardsList) {
					if (standardModel.isStdIsCompleted()) {
						Map<String, Object> standardsDetails = new HashMap<>();
						standardsDetails.put("standardId", standardModel.getStdId());
						standardsDetails.put("standardName", standardModel.getStdName());
						standardsDetails.put("progress", 0);
						listMapStandardsDetails.add(standardsDetails);
					}
				}
				List<Object[]> standardProgressModels = standardService.getCompliantStdReqCountByLcnsTokenAndCmpId(lcnsToken,
					cmpId);
				for (Object[] tmpStandardProgress : standardProgressModels) {
					Map<String, Object> standardsDetails = new HashMap<>();
					standardsDetails.put("standardId", tmpStandardProgress[0]);
					standardsDetails.put("standardName", tmpStandardProgress[1]);
					standardsDetails.put("progress", tmpStandardProgress[2]);
					listMapStandardsDetails.add(standardsDetails);
				}
				return listMapStandardsDetails;
			}
		} else {

			for (StandardModel standardModel : standardsWithoutReqs) {
				if (standardModel.isStdIsCompleted()) {
					Map<String, Object> standardsDetails = new HashMap<>();
					standardsDetails.put("standardId", standardModel.getStdId());
					standardsDetails.put("standardName", standardModel.getStdName());
					standardsDetails.put("progress", 0);
					listMapStandardsDetails.add(standardsDetails);
				}
			}
			List<Object[]> standardProgressModels = standardService.getCompliantStdReqCountByLcnsTokenAndCmpId(lcnsToken,
				cmpId);
			for (Object[] tmpStandardProgress : standardProgressModels) {
				Map<String, Object> standardsDetails = new HashMap<>();
				standardsDetails.put("standardId", tmpStandardProgress[0]);
				standardsDetails.put("standardName", tmpStandardProgress[1]);
				standardsDetails.put("progress", tmpStandardProgress[2]);
				listMapStandardsDetails.add(standardsDetails);
			}
			return listMapStandardsDetails;
		}
	}

	@Cacheable(value = "getListMapProjectsDetails")
	public List<Map<String, Object>> getListMapProjectsDetails(String token, DashboardEnum dashboardEnum)
		throws Exception {
		List<Map<String, Object>> listMapProjectsDetails = new ArrayList<>();
		// get all active projects by token
		CompanyUserModel companyUserModel = authenticationService.getCompanyUserFromToken(token.substring(7));
		RoleModel roleModel = roleService.getRoleModelByCmpUsrId(companyUserModel.getCmpUsrId());
		String userRoleName = roleModel.getRoleName();

		List<ProjectModel> projectModels;
		if (userRoleName.equals("Team Member")) { // A Team Member can only see the projects that have one or more tasks
			// and he assigned to them
			projectModels = projectService.getAllProjectsListByTskAssignee(companyUserModel.getCmpUsrId());
		} else {
			projectModels = projectService.getAllActiveProjectsByCmpId(companyUserModel.getCmpUsrCmpId().getCmpId());
		}

		switch (dashboardEnum) {
			case TASKS_DETAILS: {
				for (ProjectModel projectModel : projectModels) {
					List<TaskModel> taskModelList = taskService.getAllUnDoneTasksByProjId(projectModel.getPrjId());
					for (TaskModel taskModel : taskModelList) {
						Map<String, Object> TasksDetails = new HashMap<>();
						TasksDetails.put("taskId", taskModel.getTskId());
						TasksDetails.put("taskEndDate", taskModel.getTskEndDate());
						TasksDetails.put("taskStartDate", taskModel.getTskStartDate());
						TasksDetails.put("taskName", taskModel.getTskName());
						TasksDetails.put("projectName", projectModel.getPrjName());
						TasksDetails.put("projectId", projectModel.getPrjId());
						TasksDetails.put("status", taskModel.getStatus());
						TasksDetails.put("owner", taskModel.getTskAssignee().getCmpUsrFullName());
						listMapProjectsDetails.add(TasksDetails);
					}
				}
			}
			break;
			case PROJECTS_DETAILS:
				for (ProjectModel projectModel : projectModels) {
					Map<String, Object> projectDetails = new HashMap<>();
					projectDetails.put("numberOfUndoneTasks",
						taskService.getAllUnDoneTasksCountByProjId(projectModel.getPrjId()));
					projectDetails.put("projectName", projectModel.getPrjName());
					projectDetails.put("projectId", projectModel.getPrjId());
					listMapProjectsDetails.add(projectDetails);
				}
				break;
			default:
				break;
		}

		return listMapProjectsDetails;
	}
}
