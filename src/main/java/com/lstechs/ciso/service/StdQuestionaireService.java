package com.lstechs.ciso.service;

import com.lstechs.ciso.model.SingleCompanyUserModel;
import com.lstechs.ciso.model.*;
import com.lstechs.ciso.repository.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class StdQuestionaireService extends CRUDService<StdQuestionaireModel, SingleCompanyUserModel> {

	@Autowired
	private StdQuestionaireRepository stdQuestionaireRepository;

	@Autowired
	private StdQtrQuestionsRepository stdQtrQuestionsRepository;

	@Autowired
	private CustomerStandardRepository customerStandardRepository;

	@Autowired
	private StdSectionService stdSectionService;

	@Autowired
	private StdSectionRepository stdSectionRepository;

	@Autowired
	private StandardRepository standardRepository;

	@Autowired
	private StandardService standardService;

	@Autowired
	private AuthenticationService authenticationService;

	@Autowired
	private RequirementsHeaderTitlesRepository requirementsHeaderTitlesRepository;

	@Autowired
	private RequirementsHeaderTitlesService requirementsHeaderTitlesService;

	@Autowired
	private VendorStandardHeaderTitlesRepository vendorStandardHeaderTitlesRepository;

	private static final Logger LOGGER = LogManager.getLogger(StdQuestionaireService.class.getName());

	public StdQuestionaireService(StdQuestionaireRepository stdQuestionaireRepository) {
		super(stdQuestionaireRepository);
	}

	@Override
	public StdQuestionaireModel update(int id, StdQuestionaireModel stdLevelModel) throws Exception {
		LOGGER.info("update Std questionaire  , id: " + id + ".");
		StdQuestionaireModel updateStdQuestionaireModel = getById(id);
		updateStdQuestionaireModel.setStdId(stdLevelModel.getStdId());
		updateStdQuestionaireModel.setStdQtrName(stdLevelModel.getStdQtrName());
//		updateStdQuestionaireModel.setParentId(stdLevelModel.getParentId());
		return stdQuestionaireRepository.save(updateStdQuestionaireModel);
	}

	public Page<StdQuestionaireModel> getAllQuestionaireByTemplateId(int pageNo, int pageSize, String orderBy, boolean orderByDesc, String query, int templateId) {
		LOGGER.info("get all Std questionaire by  templateId: " + templateId + ".");
		Sort sort = Sort.by(Sort.Order.desc(orderBy));
		if (!orderByDesc) {
			sort = Sort.by(Sort.Order.asc(orderBy));
		}
		Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(orderBy));
		return stdQuestionaireRepository.getAllQuestionaireByTemplateId(paging, templateId);
	}

	public List<StdQuestionaireModel> getQuestionaireListByTemplateId(int id) {
		LOGGER.info("get all Std questionaire by  templateId: " + id + ".");
		return stdQuestionaireRepository.getQuestionaireListByTemplateId(id);
	}

	public List<StdQuestionaireModel> getQuestionaireListByTemplateIdWithDuplicate(int id, int companyId) {
		LOGGER.info("get all Std questionaire by  templateId: " + id + ".");
		return stdQuestionaireRepository.getQuestionaireListByTemplateIdWithDuplicated(id, companyId);
	}


	public StdQuestionaireModel getStdQuestionaireModelBySectionId(int id) {
		LOGGER.info("get all Std questionaire by  section ID: " + id + ".");
		return stdQuestionaireRepository.getStdQuestionaireBySectionId(id);
	}

	public List<StdQuestionaireModel> getQuestionaireListByStdIds(int[] id) {
		LOGGER.info("get all Std questionaire by  templateIds ");
		List<Integer> list = Arrays.stream(id).boxed().collect(Collectors.toList());
		return stdQuestionaireRepository.getQuestionaireListByStdIds(list);
	}

	// On Premise
	public List<StdQuestionaireModel> getQuestionaireListByStdIdsForOnPremise(int[] id) {
		LOGGER.info("get all Std questionaire by  templateIds ");
		List<Integer> list = Arrays.stream(id).boxed().collect(Collectors.toList());
		return stdQuestionaireRepository.getQuestionaireListByStdIdsForOnPremise(list);
	}

	public List<QuestionnaireNameModel> getQuestionairesNameByStdIds(int[] id) {
		LOGGER.info("get all Std questionaire name by  templateIds ");
		List<Integer> integers = Arrays.stream(id).boxed().collect(Collectors.toList());

		List<QuestionnaireNameModel> questionnaireNameModel = stdQuestionaireRepository.getQuestionairesNameByStdIds(integers);

		//Setting the Standard Id in QuestionnaireNameModel
		for (QuestionnaireNameModel questionnaireName : questionnaireNameModel) {
			StdQuestionaireModel stdQuestionaireModel = stdQuestionaireRepository.getStdQuestionnaireByQuestionnaireId(questionnaireName.getStdQtrId());
			questionnaireName.setStdId(stdQuestionaireModel.getStdId().getStdId());
		}
		return questionnaireNameModel;
	}

	public List<StdQuestionaireModel> getQuestionairesByIds(List<Integer> ids) {
		LOGGER.info("get all Std questionaire  by  templateIds ");
		return stdQuestionaireRepository.getQuestionairesByIds(ids);
	}

	public StdQuestionaireModel getQuestionairesOnPrem(int id) {
		return stdQuestionaireRepository.getOnPrem(id).get(0);
	}

	@Override
	public StdQuestionaireModel delete(int id) throws Exception {
		LOGGER.info("delete questionaire id: " + id + ".");
		StdQuestionaireModel stdQuestionaireModel = getById(id);
		List<StdSectionModel> stdSectionModels = stdSectionService.getAllSectionByQuestionaireId(id);
		for (StdSectionModel tmpStdSectionModel : stdSectionModels) {
			if (stdSectionService.getSectionDelete(tmpStdSectionModel.getStdSctId()).getDeletedAt() == null)
				stdSectionService.delete(tmpStdSectionModel.getStdSctId());
		}
		stdQuestionaireModel.setDeletedAt(new Date(System.currentTimeMillis()));
		return stdQuestionaireRepository.save(stdQuestionaireModel);
	}

	public StdQuestionaireModel deleteCisoDuplicatedQuestionnaire(int standardId, int id, boolean shouldDeleteAll) throws Exception {
		LOGGER.info("Delete Questionnaire id: " + id + ".");
		StdQuestionaireModel stdQuestionaireModel = getById(id);
		StdQuestionaireModel deletedQuestionnaire;

		List<StdQuestionaireModel> stdQuestionaireModelListByStandardId = stdQuestionaireRepository.getQuestionaireListByTemplateId(standardId);


		boolean hasMoreThanOneOriginalQuestionnaire = false;

		List<StdQuestionaireModel> originalQuestionnaires = new java.util.ArrayList<>(Collections.emptyList());

		for (StdQuestionaireModel stdQuestionaireModelByStandardId : stdQuestionaireModelListByStandardId) {

			if (stdQuestionaireModelByStandardId.getDuplicatedFrom() == 0) {
				originalQuestionnaires.add(stdQuestionaireModelByStandardId);
			}
		}

		if (originalQuestionnaires.size() > 1) {
			hasMoreThanOneOriginalQuestionnaire = true;
		}

		// Deleting The Questionnaire, Sections QtrQuestions And Requirements From The Delete Request
		if (stdQuestionaireModel.getDuplicatedFrom() > 0 || hasMoreThanOneOriginalQuestionnaire) {
			stdQuestionaireModel.setDeletedAt(new Date(System.currentTimeMillis()));
			deletedQuestionnaire = stdQuestionaireRepository.save(stdQuestionaireModel);

			// Deleting The Titles
			RequirementsHeaderTitlesModel requirementsHeaderTitlesModel = requirementsHeaderTitlesRepository.getRequirementsHeaderTitlesByQuestionnaireId(stdQuestionaireModel.getStdQtrId());
			if (requirementsHeaderTitlesModel != null) {
				requirementsHeaderTitlesModel.setDeletedAt(new Date(System.currentTimeMillis()));
				requirementsHeaderTitlesRepository.save(requirementsHeaderTitlesModel);
			}


			List<StdSectionModel> stdSectionModels = stdSectionService.getAllSectionByQuestionaireId(id);
			for (StdSectionModel tmpStdSectionModel : stdSectionModels) {
				if (stdSectionService.getSectionDelete(tmpStdSectionModel.getStdSctId()).getDeletedAt() == null)
					stdSectionService.delete(tmpStdSectionModel.getStdSctId());
			}
		} else {
			LOGGER.error("Cannot delete questionnaire, when it is the only original one ");
			return null;
		}

		// Checking if the User chose to delete the all sub Questionnaires Otherwise wi just reset the relevant fields
		if (shouldDeleteAll) {
			if (stdQuestionaireModelListByStandardId.size() > 0) {

				for (StdQuestionaireModel stdQuestionaire : stdQuestionaireModelListByStandardId) {
					StdQuestionaireModel upperMostQuestionnaire = standardService.getUppermostQuestionnaire(stdQuestionaireModel, stdQuestionaireModelListByStandardId);

					if (stdQuestionaire.getParentId() == stdQuestionaireModel.getStdQtrId()) {
						stdQuestionaire.setDeletedAt(new Date(System.currentTimeMillis()));
						stdQuestionaireRepository.save(stdQuestionaire);

						// Deleting The Titles
						RequirementsHeaderTitlesModel requirementsHeaderTitlesModel = requirementsHeaderTitlesRepository.getRequirementsHeaderTitlesByQuestionnaireId(stdQuestionaire.getStdQtrId());
						if (requirementsHeaderTitlesModel != null) {
							requirementsHeaderTitlesModel.setDeletedAt(new Date(System.currentTimeMillis()));
							requirementsHeaderTitlesRepository.save(requirementsHeaderTitlesModel);
						}
					} else if (stdQuestionaire.getDuplicatedFrom() == stdQuestionaireModel.getStdQtrId() && stdQuestionaire.getParentId() != stdQuestionaireModel.getStdQtrId()) {
						// Resetting the values
						stdQuestionaire.setDuplicatedFrom(0);
						stdQuestionaire.setParentId(0);
						stdQuestionaire.setStdQtrName(upperMostQuestionnaire.getStdQtrName() + " - " + stdQuestionaire.getStdQtrName());
						stdQuestionaire.setQuestionnaireSpecificName(stdQuestionaire.getStdQtrName());
						stdQuestionaireRepository.save(stdQuestionaire);
					}
				}
			}
		} else {
			if (stdQuestionaireModelListByStandardId.size() > 0) {
				for (StdQuestionaireModel stdQuestionaire : stdQuestionaireModelListByStandardId) {

					if (stdQuestionaire.getDuplicatedFrom() == stdQuestionaireModel.getStdQtrId()) {

						// Getting the Most Upper Original Questionnaire
						StdQuestionaireModel upperMostQuestionnaire = standardService.getUppermostQuestionnaire(stdQuestionaireModel, stdQuestionaireModelListByStandardId);

						// In case that the sub Questionnaire of the deleted Questionnaire is hierarchical
						if (stdQuestionaire.getParentId() == stdQuestionaireModel.getStdQtrId()) {
							stdQuestionaire.setStdQtrName(upperMostQuestionnaire.getStdQtrName() + " - " + deletedQuestionnaire.getStdQtrName() + " - " + stdQuestionaire.getStdQtrName());
						} else {
							stdQuestionaire.setStdQtrName(upperMostQuestionnaire.getStdQtrName() + " - " + stdQuestionaire.getStdQtrName());
						}

						// Resetting the values
						stdQuestionaire.setDuplicatedFrom(0);
						stdQuestionaire.setParentId(0);
						stdQuestionaire.setQuestionnaireSpecificName(stdQuestionaire.getStdQtrName());
						stdQuestionaireRepository.save(stdQuestionaire);
					}
				}
			}
		}
		return deletedQuestionnaire;
	}

	/**
	 * @param originalStdQuestionnaireModel originalStdQuestionnaireModel
	 * @param standardModel                 standardModel
	 * @param prefix                        prefix
	 * @return StdQuestionaireModel
	 * @throws Exception Exception
	 */
	public StdQuestionaireModel duplicateOriginalQuestionnaire(StdQuestionaireModel
																   originalStdQuestionnaireModel, StandardModel standardModel, String prefix, int parentId, String token) throws
		Exception {
		LOGGER.info("Duplicate questionnaire , id: " + originalStdQuestionnaireModel.getStdQtrId() + ".");

		CompanyModel company = authenticationService.getCompanyFromToken(token.substring(7));

		StdQuestionaireModel tmpStdQuestionnaireModel = new StdQuestionaireModel(originalStdQuestionnaireModel);

		tmpStdQuestionnaireModel.setStdQtrName(prefix);
		tmpStdQuestionnaireModel.setStdId(standardModel);
		tmpStdQuestionnaireModel.setQuestionnaireCreatedByCompany(true);
		tmpStdQuestionnaireModel.setDuplicatedFrom(originalStdQuestionnaireModel.getStdQtrId());
		tmpStdQuestionnaireModel.setCompanyId(company.getCmpId());

		if (originalStdQuestionnaireModel.isQuestionnaireCreatedByCompany()) {
			tmpStdQuestionnaireModel.setQuestionnaireCreatedByCompany(true);
			tmpStdQuestionnaireModel.setStandardCreatedByCompany(true);
		}

		//If this questionnaire is sub questionnaire we are setting the parenId
		if ((parentId > 0)) {
			tmpStdQuestionnaireModel.setParentId(parentId);
		}
		StdQuestionaireModel newQuestionnaireModel = create(tmpStdQuestionnaireModel);

		// If Standard Created Not from Vendor User We Are Setting The Titles From The Original Questionnaire
		if (originalStdQuestionnaireModel.isStandardCreatedByCompany()) {

			RequirementsHeaderTitlesModel newRequirementsHeaderTitlesModel = new RequirementsHeaderTitlesModel();
			RequirementsHeaderTitlesModel requirementsHeaderTitlesModel = requirementsHeaderTitlesRepository.getRequirementsHeaderTitlesByQuestionnaireId(originalStdQuestionnaireModel.getStdQtrId());

			if (requirementsHeaderTitlesModel != null) {
				newRequirementsHeaderTitlesModel.setRequirementTitle(requirementsHeaderTitlesModel.getRequirementTitle());
				newRequirementsHeaderTitlesModel.setDescriptionTitle(requirementsHeaderTitlesModel.getDescriptionTitle());
				newRequirementsHeaderTitlesModel.setExplanationTitle(requirementsHeaderTitlesModel.getExplanationTitle());
				newRequirementsHeaderTitlesModel.setArticleNumberTitle(requirementsHeaderTitlesModel.getArticleNumberTitle());
				newRequirementsHeaderTitlesModel.setGenericAnswer1Title(requirementsHeaderTitlesModel.getGenericAnswer1Title());
				newRequirementsHeaderTitlesModel.setGenericAnswer2Title(requirementsHeaderTitlesModel.getGenericAnswer2Title());
				newRequirementsHeaderTitlesModel.setQuestionnaireId(newQuestionnaireModel);

				// Saving the Titles
				requirementsHeaderTitlesService.create(newRequirementsHeaderTitlesModel);
			}
		}

		LOGGER.info("Create new Questionnaire , id: " + newQuestionnaireModel.getStdQtrId() + ".");

		List<StdSectionModel> stdSectionModelList = stdSectionService.getAllSectionByQuestionaireIdWithOutParent(originalStdQuestionnaireModel.getStdQtrId());
		for (StdSectionModel tempStdSectionModel : stdSectionModelList) {
			stdSectionService.duplicateSection(tempStdSectionModel, null, newQuestionnaireModel, token);
		}
		LOGGER.info("Duplicate questionnaire , id: " + originalStdQuestionnaireModel.getStdQtrId() + " success.");
		return newQuestionnaireModel;
	}

	/**
	 * @param standardModel StandardModel
	 * @param prefix        for special questionnaire name
	 * @return StdQuestionnaireModel
	 * @throws Exception Exception
	 */
	public StdQuestionaireModel duplicateQuestionnaire(StdQuestionaireModel
														   originalStdQuestionnaireModel, StandardModel standardModel, String prefix, int parentId, String token) throws Exception {

		LOGGER.info("Duplicate questionnaire , id: " + originalStdQuestionnaireModel.getStdQtrId() + ".");

		CompanyModel company = authenticationService.getCompanyFromToken(token.substring(7));

		StdQuestionaireModel tmpStdQuestionnaireModel = new StdQuestionaireModel(originalStdQuestionnaireModel);
		tmpStdQuestionnaireModel.setStdQtrName(prefix);
		tmpStdQuestionnaireModel.setParentId(originalStdQuestionnaireModel.getParentId());
		tmpStdQuestionnaireModel.setStdId(standardModel);
		tmpStdQuestionnaireModel.setQuestionnaireCreatedByCompany(true);
		tmpStdQuestionnaireModel.setDuplicatedFrom(originalStdQuestionnaireModel.getStdQtrId());
		tmpStdQuestionnaireModel.setCompanyId(company.getCmpId());

		if (originalStdQuestionnaireModel.isQuestionnaireCreatedByCompany()) {
			tmpStdQuestionnaireModel.setQuestionnaireCreatedByCompany(true);
			tmpStdQuestionnaireModel.setStandardCreatedByCompany(true);
		}

		//If this questionnaire is sub questionnaire we are setting the parenId
		if ((parentId > 0)) {
			tmpStdQuestionnaireModel.setParentId(parentId);
		}

		StdQuestionaireModel newQuestionnaireModel = create(tmpStdQuestionnaireModel);
		LOGGER.info("Create new Questionnaire , id: " + newQuestionnaireModel.getStdQtrId() + ".");

		// If Standard Created Not from Vendor User We Are Setting The Titles From The Original Questionnaire
		if (originalStdQuestionnaireModel.isStandardCreatedByCompany()) {

			RequirementsHeaderTitlesModel newRequirementsHeaderTitlesModel = new RequirementsHeaderTitlesModel();
			RequirementsHeaderTitlesModel requirementsHeaderTitlesModel = requirementsHeaderTitlesRepository.getRequirementsHeaderTitlesByQuestionnaireId(originalStdQuestionnaireModel.getStdQtrId());

			if (requirementsHeaderTitlesModel != null) {
				newRequirementsHeaderTitlesModel.setRequirementTitle(requirementsHeaderTitlesModel.getRequirementTitle());
				newRequirementsHeaderTitlesModel.setDescriptionTitle(requirementsHeaderTitlesModel.getDescriptionTitle());
				newRequirementsHeaderTitlesModel.setExplanationTitle(requirementsHeaderTitlesModel.getExplanationTitle());
				newRequirementsHeaderTitlesModel.setArticleNumberTitle(requirementsHeaderTitlesModel.getArticleNumberTitle());
				newRequirementsHeaderTitlesModel.setGenericAnswer1Title(requirementsHeaderTitlesModel.getGenericAnswer1Title());
				newRequirementsHeaderTitlesModel.setGenericAnswer2Title(requirementsHeaderTitlesModel.getGenericAnswer2Title());
				newRequirementsHeaderTitlesModel.setQuestionnaireId(newQuestionnaireModel);

				// Saving the Titles
				requirementsHeaderTitlesService.create(newRequirementsHeaderTitlesModel);
			}
		}

		List<StdSectionModel> stdSectionModelList = stdSectionService.getAllSectionByQuestionaireIdWithOutParent(originalStdQuestionnaireModel.getStdQtrId());
		for (StdSectionModel tempStdSectionModel : stdSectionModelList) {
			stdSectionService.duplicateSectionWithCustomerStandards(tempStdSectionModel, null, newQuestionnaireModel);
		}

		LOGGER.info("Duplicate questionnaire , id: " + originalStdQuestionnaireModel.getStdQtrId() + " success.");
		return newQuestionnaireModel;
	}

	public StdQuestionaireModel duplicateVendorOriginalQuestionnaire(StdQuestionaireModel
																		 originalStdQuestionnaireModel, StandardModel standardModel, String token, StandardModel originalStandard, boolean isDuplicatedFromStandardDuplication) throws
		Exception {
		LOGGER.info("Duplicate questionnaire , id: " + originalStdQuestionnaireModel.getStdQtrId() + ".");
		StdQuestionaireModel tmpStdQuestionnaireModel = new StdQuestionaireModel(originalStdQuestionnaireModel);

		// If Questionnaire duplicated with Standard duplication
		// Setting Standard name to the Questionnaire name
		if (isDuplicatedFromStandardDuplication) {
			tmpStdQuestionnaireModel.setStdQtrName(standardModel.getStdName().substring(7));
		} else {
			tmpStdQuestionnaireModel.setStdQtrName("(Copy) " + originalStdQuestionnaireModel.getStdQtrName());
			tmpStdQuestionnaireModel.setDuplicatedFrom(originalStdQuestionnaireModel.getStdQtrId());
		}
		tmpStdQuestionnaireModel.setStdId(standardModel);
		tmpStdQuestionnaireModel.setQuestionnaireCreatedByCompany(false);

		StdQuestionaireModel newQuestionnaireModel = create(tmpStdQuestionnaireModel);

		LOGGER.info("Create new Questionnaire , id: " + newQuestionnaireModel.getStdQtrId() + ".");

		List<StdSectionModel> stdSectionModelList = stdSectionService.getAllSectionByQuestionaireIdWithOutParent(originalStdQuestionnaireModel.getStdQtrId());
		for (StdSectionModel tempStdSectionModel : stdSectionModelList) {
			stdSectionService.duplicateSection(tempStdSectionModel, null, newQuestionnaireModel, token);
		}
		LOGGER.info("Duplicate questionnaire , id: " + originalStdQuestionnaireModel.getStdQtrId() + " success.");
		return newQuestionnaireModel;
	}

}
