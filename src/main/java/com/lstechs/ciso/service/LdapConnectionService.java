package com.lstechs.ciso.service;

import com.lstechs.ciso.model.LdapConnectionModel;
import com.lstechs.ciso.model.SingleLdapConnectionModel;
import com.lstechs.ciso.repository.LdapConnectionRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LdapConnectionService extends CRUDService<LdapConnectionModel, SingleLdapConnectionModel> {

	public LdapConnectionService(LdapConnectionRepository ldapConnectionRepository) {
		super(ldapConnectionRepository);
	}

	@Autowired
	private PasswordEncoder bcryptEncoder;

	@Autowired
	private LdapConnectionRepository ldapConnectionRepository;

	public static boolean isReadyToConnect;

	private static Logger LOGGER = LogManager.getLogger(LdapConnectionService.class.getName());

	@Override
	public SingleLdapConnectionModel getSingleInstance(LdapConnectionModel ldapConnectionModel) throws Exception {
		return new SingleLdapConnectionModel(ldapConnectionModel);
	}

	@Override
	public LdapConnectionModel create(LdapConnectionModel ldapConnectionModel) throws Exception {
		LOGGER.info("create new Ldap connection.");
		List<LdapConnectionModel> checkLdapConnectionModel = getAll();
		if (checkLdapConnectionModel != null && checkLdapConnectionModel.size() > 0) {
			throw new Exception("The table is limited to one row only.");
		}
		LdapConnectionModel newLdapConnectionModel = new LdapConnectionModel();
		newLdapConnectionModel.setLdapManagerDn(ldapConnectionModel.getLdapManagerDn());
		newLdapConnectionModel.setLdapPort(ldapConnectionModel.getLdapPort());
		newLdapConnectionModel.setLdapUrl(ldapConnectionModel.getLdapUrl());
		if (ldapConnectionModel.getLdapUrl2() != null) {
			newLdapConnectionModel.setLdapUrl2(ldapConnectionModel.getLdapUrl2());
		}
		newLdapConnectionModel.setLdapUserSearchBase(ldapConnectionModel.getLdapUserSearchBase());
		if (ldapConnectionModel.getLdapUserSearchFilter() != null) {
			newLdapConnectionModel.setLdapUserSearchFilter(ldapConnectionModel.getLdapUserSearchFilter());
		}
		newLdapConnectionModel.setLdapManagerPassword(ldapConnectionModel.getLdapManagerPassword());
		isReadyToConnect = true;
		return ldapConnectionRepository.save(newLdapConnectionModel);
	}

	@Override
	public LdapConnectionModel update(int id, LdapConnectionModel ldapConnectionModel) throws Exception {
		LOGGER.info("update Ldap connection.");
		LdapConnectionModel updateLdapConnectionModel = getById(id);
		updateLdapConnectionModel.setLdapManagerDn(ldapConnectionModel.getLdapManagerDn());
		updateLdapConnectionModel.setLdapUrl(ldapConnectionModel.getLdapUrl());
		updateLdapConnectionModel.setLdapUrl2(ldapConnectionModel.getLdapUrl2());
		updateLdapConnectionModel.setLdapUserSearchBase(ldapConnectionModel.getLdapUserSearchBase());
		updateLdapConnectionModel.setLdapUserSearchFilter(ldapConnectionModel.getLdapUserSearchFilter());
		updateLdapConnectionModel.setLdapManagerPassword(bcryptEncoder.encode(ldapConnectionModel.getLdapManagerPassword()));
		return ldapConnectionRepository.save(updateLdapConnectionModel);
	}

	public LdapConnectionModel getLdapConnection() throws Exception {
		List<LdapConnectionModel> ldapConnectionModels = getAll();
		for (LdapConnectionModel ldapConnectionModel : ldapConnectionModels) {
			if (ldapConnectionModel.getDeletedAt() == null)
				return ldapConnectionModel;
		}
		return null;
	}


}
