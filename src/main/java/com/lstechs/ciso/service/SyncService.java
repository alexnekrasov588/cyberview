
package com.lstechs.ciso.service;

import com.lstechs.ciso.enums.ModuleEnum;
import com.lstechs.ciso.model.*;
import com.lstechs.ciso.repository.CstQstFileAnswerRepository;
import com.lstechs.ciso.repository.StdSectionRepository;
import com.lstechs.ciso.repository.VendorStandardHeaderTitlesRepository;
import com.lstechs.ciso.utils.DateUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;

@Service
@EnableScheduling
public class SyncService {

	private static final Logger LOGGER = LogManager.getLogger(SyncService.class.getName());

	@Autowired
	private NewsArticlesService newsArticlesService;

	@Autowired
	private CVEService cveService;

	@Autowired
	private StandardService standardService;

	@Autowired
	private CustomerStandardService customerStandardService;

	@Autowired
	private ModuleTaskService moduleTaskService;

	@Autowired
	private StdQuestionaireService stdQuestionaireService;

	@Autowired
	private StdSectionService stdSectionService;

	@Autowired
	private StdRequirementsService stdRequirementsService;

	@Autowired
	private StdQtrQuestionsService stdQtrQuestionsService;

	@Autowired
	private StdQstOptionService stdQstOptionService;

	@Autowired
	private StdAnswer1OptionService stdAnswer1OptionService;

	@Autowired
	private StdAnswer2OptionService stdAnswer2OptionService;

	@Autowired
	private ThreatService threatService;

	@Autowired
	private NewsSourcesService newsSourceService;

	@Autowired
	private CVESourcesService cveSourcesService;

	@Autowired
	private CstQstFileAnswerRepository cstQstFileAnswerRepository;

	@Autowired
	private StdSectionRepository stdSectionRepository;

	@Autowired
	private VendorStandardHeaderTitlesRepository vendorStandardHeaderTitlesRepository;

	@Value("${onPrem.sync.hostname}")
	private String hostname;

	@Value("${onPrem.sync.port}")
	private int port;

	@Value("${onPrem.sync.timeout}")
	private int timeout;

	@Value("${onPrem}")
	private boolean onPrem;

	@Autowired
	private RestTemplate restTemplate;


	//use only in onPrem env
	private Date getLastUpdatedAtNews() throws Exception {
		LOGGER.info("get last updatedAt from newsArticles service");
		return newsArticlesService.lastUpdateAt();
	}

	//use only in onPrem env
	private Date getLastUpdatedAtCVE() throws Exception {
		LOGGER.info("get last updatedAt from cve service");
		return cveService.lastUpdateAt();
	}

	//use only in onPrem env
	private Date getLastUpdatedAtStandard() throws Exception {
		LOGGER.info("get last updatedAt from standard service");
		return standardService.lastUpdateAt();
	}

	// use in cloud
	private List<NewsArticlesModel> getSyncNewsList(Date updatedAt) throws Exception {
		LOGGER.info("get sync News articles list updatedAt : " + updatedAt);
		return newsArticlesService.getSyncList(updatedAt);
	}

	// use in cloud
	private List<CVEModel> getSyncCVEList(Date updatedAt) throws Exception {
		LOGGER.info("get sync CVE list updatedAt : " + updatedAt);
		return cveService.getSyncList(updatedAt);
	}

	// use in cloud
	private List<StandardModel> getSyncStandardList(Date updatedAt) throws Exception {
		LOGGER.info("get sync standard list updatedAt : " + updatedAt);
		return standardService.getSyncList(updatedAt);
	}

	//use in onPrem
	private boolean isHostAvailable() {
		LOGGER.info("is host available()");
		try (Socket socket = new Socket()) {
			InetSocketAddress socketAddress = new InetSocketAddress(hostname, port);
			socket.connect(socketAddress, timeout);
			LOGGER.info("connect to : " + hostname + " success.");
			return true;
		} catch (Exception ex) {
			LOGGER.error("connect to : " + hostname + " failed.");
			return false;
		}
	}

	public List<NewsArticlesModel> syncNewsArticlesModel() throws Exception {
		LOGGER.info("sync news articles model");
		if (isHostAvailable()) {
			Date updatedAt = getLastUpdatedAtNews();
			if (updatedAt == null) {
				updatedAt = new Date(0);
			}
			final String baseUrl = "https://" + hostname + "/api/news/sync/" + updatedAt.getTime();
			URI uri = new URI(baseUrl);
			NewsArticlesModel[] newsArticlesModels = restTemplate.getForObject(uri, NewsArticlesModel[].class);
			if (newsArticlesModels == null) return null;
			if (newsArticlesModels.length > 0) {
				LOGGER.info("syncing news articles from " + DateUtils.formatDate(updatedAt));
				for (NewsArticlesModel newsArticlesModel : newsArticlesModels) {
					newsArticlesModel.setNewsSrcId(newsSourceService.getOnPrem(newsArticlesModel.getNewsSrcId().getNewsSrcId()));
					newsArticlesModel.setNewsUpdateOnPrem(newsArticlesModel.getUpdatedAt());
				}
				return newsArticlesService.setSyncList(newsArticlesModels);
			}
			return null;
		} else {
			throw new Exception("Error connection to : " + hostname + ":" + port);
		}
	}

	public List<CVEModel> syncCVEModel() throws Exception {
		LOGGER.info("sync cve model");
		if (isHostAvailable()) {
			Date updatedAt = getLastUpdatedAtCVE();
			if (updatedAt == null) {
				updatedAt = new Date(0);
			}
			final String baseUrl = "https://" + hostname + "/api/cve/sync/" + updatedAt.getTime();
			URI uri = new URI(baseUrl);
			CVEModel[] cveModels = restTemplate.getForObject(uri, CVEModel[].class);
			if (cveModels != null && cveModels.length > 0) {
				LOGGER.info("syncing CVE articles from " + DateUtils.formatDate(updatedAt));
				for (CVEModel cveModel : cveModels) {
					cveModel.setCveSrcId(cveSourcesService.getOnPrem(cveModel.getCveSrcId().getCveSrcId()));
					cveModel.setCveUpdateOnPrem(cveModel.getUpdatedAt());
				}
			}
			return cveService.setSyncList(cveModels);
		} else {
			throw new Exception("Error connection to : " + hostname + ":" + port);
		}
	}

	public List<ThreatModel> syncThreat() throws Exception {
		LOGGER.info("syncing threats");
		if (isHostAvailable()) {
			Date updatedAt = new Date(0);
			final String baseUrl = "https://" + hostname + "/api/threat/sync/" + updatedAt.getTime();
			URI uri = new URI(baseUrl);
			ThreatModel[] threatModel = restTemplate.getForObject(uri, ThreatModel[].class);
			return threatService.setSyncList(threatModel);
		} else {
			throw new Exception("Error connection to : " + hostname + ":" + port);
		}
	}

	public List<NewsSourcesModel> syncNewsSource() throws Exception {
		if (isHostAvailable()) {
			LOGGER.info("syncing news sources");
			Date updatedAt = new Date(0);
			final String baseUrl = "https://" + hostname + "/api/newsSources/sync/" + updatedAt.getTime();
			URI uri = new URI(baseUrl);
			NewsSourcesModel[] newsSourcesModel = restTemplate.getForObject(uri, NewsSourcesModel[].class);
			if (newsSourcesModel != null && newsSourcesModel.length > 0) {
				for (NewsSourcesModel sourcesModel : newsSourcesModel) {
					sourcesModel.setNewsSrcOnPremId(sourcesModel.getNewsSrcId());
					sourcesModel.setNewsSrcId(0);
				}
				return newsSourceService.setSyncList(newsSourcesModel);
			}
			return null;

		} else {
			throw new Exception("Error connection to : " + hostname + ":" + port);
		}
	}

	public List<CVESourcesModel> syncCVESource() throws Exception {
		if (isHostAvailable()) {
			LOGGER.info("syncing CVE sources");
			Date updatedAt = new Date(0);
			final String baseUrl = "https://" + hostname + "/api/cveSources/sync/" + updatedAt.getTime();
			URI uri = new URI(baseUrl);
			CVESourcesModel[] cveSourcesModel = restTemplate.getForObject(uri, CVESourcesModel[].class);
			if (cveSourcesModel != null && cveSourcesModel.length > 0) {
				for (CVESourcesModel sourcesModel : cveSourcesModel) {
					sourcesModel.setCveSrcOnPremId(sourcesModel.getCveSrcId());
				}
			}
			return cveSourcesService.setSyncList(cveSourcesModel);
		} else {
			throw new Exception("Error connection to : " + hostname + ":" + port);
		}
	}

	public void syncStandards() throws Exception {
		if (isHostAvailable()) {
			LOGGER.info("syncing standards");
			List<StandardModel> standardModels = standardService.getAll();


			// Saving Titles
			for (StandardModel standardModel : standardModels) {
				VendorStandardHeaderTitlesModel vendorStandardHeaderTitlesModel = new VendorStandardHeaderTitlesModel();
				vendorStandardHeaderTitlesModel.setStandardId(standardModel);
				vendorStandardHeaderTitlesRepository.save(vendorStandardHeaderTitlesModel);
			}

			if (standardModels.size() == 0) {
				throw new Exception("There are no standards in the system");
			}
			String ids = standardModels.stream().map(stdModel -> String.valueOf(stdModel.getStdOnPremId())).collect(Collectors.joining(","));
			String baseUrl = "https://" + hostname + "/api/templates/syncQuestionaire?ids=" + ids;
			URI uri = new URI(baseUrl);
			StdQuestionaireModel[] stdQuestionaireModel = restTemplate.getForObject(uri, StdQuestionaireModel[].class);
			if (stdQuestionaireModel != null && stdQuestionaireModel.length > 0) {
				LOGGER.info("syncing questionnaires");
				for (StdQuestionaireModel questionaireModel : stdQuestionaireModel) {
					questionaireModel.setStdId(standardService.getStdOnPrem(questionaireModel.std));
					questionaireModel.setStdQtrOnPremId(questionaireModel.getStdQtrId());
					questionaireModel.setVendorStandardHeaderTitlesModel(null);
				}
				stdQuestionaireService.setSyncList(stdQuestionaireModel);
			}

			baseUrl = "https://" + hostname + "/api/templates/syncSections?ids=" + ids;
			uri = new URI(baseUrl);
			StdSectionModel[] stdSectionModels = restTemplate.getForObject(uri, StdSectionModel[].class);
			if (stdSectionModels != null && stdSectionModels.length > 0) {
				LOGGER.info("syncing sections");
				for (StdSectionModel stdSectionModel : stdSectionModels) {
					stdSectionModel.setStdQtrId(stdQuestionaireService.getQuestionairesOnPrem(stdSectionModel.qtr));
					stdSectionModel.setStdSctOnPremId(stdSectionModel.getStdSctId());

					//Unnecessary
//					 	if (stdSectionModel.getParent() != null) {
//                     	   stdSectionModel.setParent(stdSectionModel.getParent());
//                      }
					for (StdQtrQuestionsModel question : stdSectionModel.getStdQtrQuestionsModel()) {
						// remove question id
						question.setQtrQstId(0);
					}
				}

				List<StdSectionModel> tempParentList = new ArrayList<>();

				for (StdSectionModel stdSectionModel : stdSectionModels) {
					if (stdSectionModel.getParent() == null) {
						stdSectionModel.setPreviousParentId(stdSectionModel.getStdSctId());
						tempParentList.add(stdSectionModel);
					}
				}


				stdSectionService.saveSectionsFromOnPrem(tempParentList);
			}

			List<StdSectionModel> tempNotParentList = new ArrayList<>();

			for (StdSectionModel stdSectionModel : stdSectionModels) {
				if (stdSectionModel.getParent() != null) {

					StdSectionModel section = stdSectionRepository.getAllNotParentSectionsSectionFromOnPrem(stdSectionModel.getParent().getStdSctId());
					stdSectionModel.setParent(section);

					tempNotParentList.add(stdSectionModel);
				}
			}

			stdSectionService.saveSectionsFromOnPrem(tempNotParentList);
			List<StdRequirementsModel> savedStdRequirementsModel = Collections.emptyList();
			baseUrl = "https://" + hostname + "/api/templates/syncRequirements?ids=" + ids;
			uri = new URI(baseUrl);
			StdRequirementsModel[] stdRequirementsModels = restTemplate.getForObject(uri, StdRequirementsModel[].class);
			if (stdRequirementsModels != null && stdRequirementsModels.length > 0) {
				LOGGER.info("syncing requirements");
				for (StdRequirementsModel stdRequirementsModel : stdRequirementsModels) {
					stdRequirementsModel.setStdId(standardService.getStdOnPrem(stdRequirementsModel.std));
					stdRequirementsModel.setStdRqrOnPremId(stdRequirementsModel.getStdRqrId());
					stdRequirementsModel.setStdRqrId(0);
				}
				savedStdRequirementsModel = stdRequirementsService.setSyncList(stdRequirementsModels);
			}

			baseUrl = "https://" + hostname + "/api/templates/syncQuestions?ids=" + ids;
			uri = new URI(baseUrl);
			StdQtrQuestionsModel[] stdQtrQuestionsModels = restTemplate.getForObject(uri, StdQtrQuestionsModel[].class);

			baseUrl = "https://" + hostname + "/api/templates/syncOptions?ids=" + ids;
			uri = new URI(baseUrl);
			StdQstOptionModel[] stdQstOptionModelsWithoutQST = restTemplate.getForObject(uri, StdQstOptionModel[].class);

			baseUrl = "https://" + hostname + "/api/templates/syncAnswer1Options?ids=" + ids;
			uri = new URI(baseUrl);
			StdAnswer1OptionModel[] stdAnswer1OptionModels = restTemplate.getForObject(uri, StdAnswer1OptionModel[].class);

			baseUrl = "https://" + hostname + "/api/templates/syncAnswer2Options?ids=" + ids;
			uri = new URI(baseUrl);
			StdAnswer2OptionModel[] stdAnswer2OptionModels = restTemplate.getForObject(uri, StdAnswer2OptionModel[].class);

			List<StdQstOptionModel> newStdQstOptionModel = new java.util.ArrayList<>(Collections.emptyList());
			List<StdAnswer1OptionModel> newStdQst1OptionModel = new java.util.ArrayList<>(Collections.emptyList());
			List<StdAnswer2OptionModel> newStdQst2OptionModel = new java.util.ArrayList<>(Collections.emptyList());

			if (stdQtrQuestionsModels != null && stdQtrQuestionsModels.length > 0) {
				LOGGER.info("syncing questions");
				for (StdQtrQuestionsModel stdQtrQuestionsModel : stdQtrQuestionsModels) {
					stdQtrQuestionsModel.setStdRqrId(stdRequirementsService.getOnPRem(stdQtrQuestionsModel.rqr));
					stdQtrQuestionsModel.setStdSctId(stdSectionService.getOnPrem(stdQtrQuestionsModel.sct));
					stdQtrQuestionsModel.setQtrQstOnPremId(stdQtrQuestionsModel.getQtrQstId());
					stdQtrQuestionsModel.setStdQstOptionModel(null);
					stdQtrQuestionsModel.setStdAnswer1OptionModel(null);
					stdQtrQuestionsModel.setStdAnswer2OptionModel(null);

					//Option
					if (stdQstOptionModelsWithoutQST != null && stdQstOptionModelsWithoutQST.length > 0) {
						LOGGER.info("syncing questions options");
						for (StdQstOptionModel stdQstOptionModel : stdQstOptionModelsWithoutQST) {
							stdQstOptionModel.setStdOptOnPremId(stdQstOptionModel.getSqoOptId());
							if (stdQtrQuestionsModel.getQtrQstId() == stdQstOptionModel.getQst()) {
								newStdQstOptionModel.add(stdQstOptionModel);
							}
						}
					}
					//Option 1
					if (stdAnswer1OptionModels != null && stdAnswer1OptionModels.length > 0) {
						LOGGER.info("syncing answer 1 options");
						for (StdAnswer1OptionModel stdAnswer1OptionModel : stdAnswer1OptionModels) {
							stdAnswer1OptionModel.setStdOpt1OnPremId(stdAnswer1OptionModel.getsA1OptId());
							if (stdQtrQuestionsModel.getQtrQstId() == stdAnswer1OptionModel.getQst()) {
								newStdQst1OptionModel.add(stdAnswer1OptionModel);
							}
						}
					}
					//Option 2
					if (stdAnswer2OptionModels != null && stdAnswer2OptionModels.length > 0) {
						LOGGER.info("syncing answer 2 options");
						for (StdAnswer2OptionModel stdAnswer2OptionModel : stdAnswer2OptionModels) {
							stdAnswer2OptionModel.setStdOpt2OnPremId(stdAnswer2OptionModel.getsA2OptId());
							if (stdQtrQuestionsModel.getQtrQstId() == stdAnswer2OptionModel.getQst()) {
								newStdQst2OptionModel.add(stdAnswer2OptionModel);
							}
						}
					}
				}

				List<StdQtrQuestionsModel> savedStdQtrQuestionsModelList = stdQtrQuestionsService.saveQuestionnaires(stdQtrQuestionsModels);
				for (StdQtrQuestionsModel savedStdQtrQuestionsModel : savedStdQtrQuestionsModelList) {

					//Option
					for (StdQstOptionModel stdQstOptionModel : stdQstOptionModelsWithoutQST) {
						if (stdQstOptionModel.getQst() == savedStdQtrQuestionsModel.getQtrQstOnPremId()) {
							stdQstOptionModel.setSqoQstId(savedStdQtrQuestionsModel);
						}
					}
					//Option 1
					for (StdAnswer1OptionModel stdAnswer1OptionModel : stdAnswer1OptionModels) {
						if (stdAnswer1OptionModel.getQst() == savedStdQtrQuestionsModel.getQtrQstOnPremId()) {
							stdAnswer1OptionModel.setSqoQstId(savedStdQtrQuestionsModel);
						}
					}
					//Option 2
					for (StdAnswer2OptionModel stdAnswer2OptionModel : stdAnswer2OptionModels) {
						if (stdAnswer2OptionModel.getQst() == savedStdQtrQuestionsModel.getQtrQstOnPremId()) {
							stdAnswer2OptionModel.setSqoQstId(savedStdQtrQuestionsModel);
						}
					}
				}

				//Saving Options
				stdQstOptionService.setSyncList(stdQstOptionModelsWithoutQST);
				stdAnswer1OptionService.setSyncList(stdAnswer1OptionModels);
				stdAnswer2OptionService.setSyncList(stdAnswer2OptionModels);
			}
		} else {
			throw new Exception("Error connection to : " + hostname + ":" + port);
		}
	}

	public void syncAllNews() {
		boolean updateMore = false;
		List<NewsArticlesModel> newsArticlesModels = null;
		try {
			newsArticlesModels = syncNewsArticlesModel();
			if (newsArticlesModels != null && newsArticlesModels.size() > 0) updateMore = true;
			while (updateMore) {
				NewsArticlesModel[] newsArr = new NewsArticlesModel[newsArticlesModels.size()];
				newsArticlesModels.toArray(newsArr);
				newsArticlesService.setSyncList(newsArr);
				newsArticlesModels = syncNewsArticlesModel();
				updateMore = newsArticlesModels != null && newsArticlesModels.size() > 0;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void syncAllCVE() {
		boolean updateMore = false;
		List<CVEModel> cveModels = null;
		try {
			cveModels = syncCVEModel();
			if (cveModels != null && cveModels.size() > 0) updateMore = true;
			while (updateMore) {
				CVEModel[] cveArr = new CVEModel[cveModels.size()];
				cveModels.toArray(cveArr);
				cveService.setSyncList(cveArr);
				cveModels = syncCVEModel();
				updateMore = cveModels != null && cveModels.size() > 0;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void onPremUpdate() throws Exception {
		if (onPrem) {
			syncThreat();
			syncStandards();
			syncNewsSource();
			syncAllNews();
			syncCVESource();
			syncAllCVE();
		}
	}

	@Async
	@Scheduled(cron = "${cron.sync}")
	public void syncNewsAndCve() throws Exception {
		if (onPrem) {
			LOGGER.info("start to sync news and cve.");
			List<NewsArticlesModel> newsArticlesModels = syncNewsArticlesModel();
			List<CVEModel> cveModels = syncCVEModel();
			int updateList = 0;
			for (NewsArticlesModel tmpNews : newsArticlesModels) {
				try {
					newsArticlesService.create(tmpNews);
					updateList++;
				} catch (Exception ex) {
					LOGGER.error("error cannot create  news: " + tmpNews.getNewsArtcId());
					LOGGER.error(ex);
				}
			}
			LOGGER.info("News ,updates rows  : " + updateList + " from " + newsArticlesModels.size());
			updateList = 0;
			for (CVEModel tmpCVE : cveModels) {
				try {
					cveService.create(tmpCVE);
				} catch (Exception ex) {
					LOGGER.error("error cannot create  CVE: " + tmpCVE.getCveId());
					LOGGER.error(ex);
				}
			}
			LOGGER.info("CVE ,updates rows  : " + updateList + " from " + cveModels.size());
			LOGGER.info("Start sync Standards.");
			//syncStandards();
			LOGGER.info("Finish to sync Standards.");
		}
	}
}
