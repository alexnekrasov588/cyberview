package com.lstechs.ciso.service;

import com.lstechs.ciso.enums.ModuleEnum;
import com.lstechs.ciso.enums.OperationEnum;
import com.lstechs.ciso.enums.UsersEnum;
import com.lstechs.ciso.model.*;
import com.lstechs.ciso.repository.CustomerStandardRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class RequestQuestionnaireService {

	@Autowired
	AuthenticationService authenticationService;

	@Autowired
	private StdQuestionaireService stdQuestionaireService;

	@Autowired
	private StandardService standardService;

	@Autowired
	private StdSectionService stdSectionService;

	@Autowired
	private StdQtrQuestionsService stdQtrQuestionsService;

	@Autowired
	private StdRequirementsService stdRequirementsService;

	@Autowired
	private CustomerStandardService customerStandardService;

	@Autowired
	private ModuleTaskService moduleTaskService;

	@Autowired
	private StdQstOptionService stdQstOptionService;

	@Autowired
	private StdAnswer1OptionService stdAnswer1OptionService;

	@Autowired
	private StdAnswer2OptionService stdAnswer2OptionService;

	@Autowired
	private CustomerStandardRepository customerStandardRepository;

	@Autowired
	private StandardCompanyStatusService standardCompanyStatusService;

	private static final Logger LOGGER = LogManager.getLogger(RequestQuestionnaireService.class.getName());

	public StdQuestionaireModel requestParser(RequestQuestionnaireModel requestQuestionnaireModel, int templateId) throws Exception {
		LOGGER.info("requestParser.");
		StandardModel standardModel = standardService.getById(templateId);
		StdQuestionaireModel stdQuestionaireModel = getStdQuestionaireModel(requestQuestionnaireModel, standardModel);
		if (requestQuestionnaireModel.getChildren() != null && requestQuestionnaireModel.getChildren().size() > 0) {
			for (ChildrenQuestionnaireModel tmpChildrenQuestionnaireModel : requestQuestionnaireModel.getChildren()) {
				if (tmpChildrenQuestionnaireModel.getType().equals("section")) {
					if (tmpChildrenQuestionnaireModel.getStdSectionModel() == null) {
						LOGGER.error("Section cannot be null");
						throw new Exception("Section cannot be null");
					}
					StdSectionModel parent = getStdSectionModel(tmpChildrenQuestionnaireModel.getStdSectionModel(), null, stdQuestionaireModel);
					if (tmpChildrenQuestionnaireModel.getChildren() != null) {
						recursiveParser(standardModel, stdQuestionaireModel, parent, tmpChildrenQuestionnaireModel.getChildren());
					}
				} else {
					LOGGER.error("In level 0 type must be section.");
					throw new Exception("In level 0 type must be section.");
				}
			}
		}
		return stdQuestionaireModel;
	}

	private void recursiveParser(StandardModel standardModel, StdQuestionaireModel stdQuestionaireModel, StdSectionModel parent, List<ChildrenQuestionnaireModel> childrenQuestionnaireModels) throws Exception {
		LOGGER.info("recursiveParser.");
		for (ChildrenQuestionnaireModel tmpChildrenQuestionnaireModel : childrenQuestionnaireModels) {
			if (tmpChildrenQuestionnaireModel.getType().equals("section")) {
				StdSectionModel innerParent = getStdSectionModel(tmpChildrenQuestionnaireModel.getStdSectionModel(), parent, stdQuestionaireModel);
				if (tmpChildrenQuestionnaireModel.getChildren() != null)
					recursiveParser(standardModel, stdQuestionaireModel, innerParent, tmpChildrenQuestionnaireModel.getChildren());
			} else {
				getStdRequirementsModel(tmpChildrenQuestionnaireModel.getStdRequirementsModel(), parent, standardModel);
			}
		}
	}

	private StdQuestionaireModel getStdQuestionaireModel(RequestQuestionnaireModel requestQuestionnaireModel, StandardModel standardModel) throws Exception {
		if (requestQuestionnaireModel.getStdQtrId() == 0) {
			StdQuestionaireModel stdQuestionaireModel = new StdQuestionaireModel(requestQuestionnaireModel.getStdQtrName(), standardModel, 0);
			LOGGER.info("create stdQuestionaire");
			return stdQuestionaireService.create(stdQuestionaireModel);
		} else {
			StdQuestionaireModel stdQuestionaireModel = stdQuestionaireService.getById(requestQuestionnaireModel.getStdQtrId());
			stdQuestionaireModel.setStdId(standardModel);
			stdQuestionaireModel.setStdQtrName(requestQuestionnaireModel.getStdQtrName());
			LOGGER.info("update stdQuestionaire");
			return stdQuestionaireService.update(stdQuestionaireModel.getStdQtrId(), stdQuestionaireModel);
		}
	}

	private StdSectionModel getStdSectionModel(StdSectionModel stdSectionModel, StdSectionModel parent, StdQuestionaireModel stdQuestionaireModel) throws Exception {
		if (stdSectionModel == null) {
			LOGGER.error("Section cannot be null");
			throw new Exception("Section cannot be null");
		}
		if (stdSectionModel.getStdSctId() == 0) {
			StdSectionModel newStdSectionModel = new StdSectionModel(stdSectionModel);
			newStdSectionModel.setStdQtrId(stdQuestionaireModel);
			if (parent != null) newStdSectionModel.setParent(stdSectionService.getById(parent.getStdSctId()));
			LOGGER.info("create new section");
			return stdSectionService.create(newStdSectionModel);
		} else {
			StdSectionModel updateStdSectionModel = stdSectionService.getById(stdSectionModel.getStdSctId());
			updateStdSectionModel.setStdQtrId(stdQuestionaireModel);
			if (parent != null) updateStdSectionModel.setParent(parent);
			updateStdSectionModel.setStdSctName(stdSectionModel.getStdSctName());
			updateStdSectionModel.setStdSctDescription(stdSectionModel.getStdSctDescription());
			LOGGER.info("update section");
			return stdSectionService.update(updateStdSectionModel.getStdSctId(), updateStdSectionModel);
		}
	}

	private void getStdRequirementsModel(StdRequirementsModel stdRequirementsModel, StdSectionModel stdSectionModel, StandardModel standardModel) throws Exception {
		if (stdRequirementsModel == null) {
			LOGGER.error("requirements cannot be null");
			throw new Exception("requirements cannot be null");
		}
		if (stdRequirementsModel.getStdRqrId() == 0) {
			StdRequirementsModel newStdRequirementsModel = new StdRequirementsModel(stdRequirementsModel);
			newStdRequirementsModel.setStdId(standardModel);
			LOGGER.info("Create new requirement.");
			newStdRequirementsModel.setSectionId(stdSectionModel.getStdSctId());
			StdRequirementsModel newRequirementsModel = stdRequirementsService.create(newStdRequirementsModel);
			newRequirementsModel.setQtrQstType(stdRequirementsModel.getQtrQstType());
			newRequirementsModel.setAnswerDate1Type(stdRequirementsModel.getAnswerDate1Type());
			newRequirementsModel.setAnswerDate2Type(stdRequirementsModel.getAnswerDate2Type());
			newRequirementsModel.setStdQstOptionModels(stdRequirementsModel.getStdQstOptionModels());

			//Setting the generic answer dropdown type
			newRequirementsModel.setStdAnswer1OptionModels(stdRequirementsModel.getStdAnswer1OptionModels());
			newRequirementsModel.setStdAnswer2OptionModels(stdRequirementsModel.getStdAnswer2OptionModels());

			newRequirementsModel.setStdQtrQuestionsModel(createStdQtrQuestionsModel(newRequirementsModel, stdSectionModel, null));
			if (stdRequirementsModel.getOrder() == 0) {
				int maxOrder = stdRequirementsService.getMaxOrderByStdId(standardModel.getStdId()) + 1;
				newRequirementsModel.setOrder(maxOrder);
			}

			//Creating Customer Standard for existing Questionnaire with new Requirements.
			List<CustomerStandardModel> customerStandardModelList = customerStandardRepository.getCustomerStandardsByStandardId(standardModel.getStdId());
			if (customerStandardModelList.size() > 0) {
				createCustomerStandardToCompany(standardModel, customerStandardModelList.get(0).getCmpId(), newRequirementsModel);
			}

		} else {
			StdRequirementsModel updateStdRequirementsModel = stdRequirementsService.getById(stdRequirementsModel.getStdRqrId());
			updateStdRequirementsModel.setStdId(standardModel);

			// If the Vendor Changed the Answer & Generic Answer 1 & 2
			// We are setting the both Answers to Null
			// Answer
			if (updateStdRequirementsModel.getStdQtrQuestionsModel().getQtrQstType() != null && stdRequirementsModel.getQtrQstType() != null) {
				if (!updateStdRequirementsModel.getStdQtrQuestionsModel().getQtrQstType().equals(stdRequirementsModel.getQtrQstType())) {
					List<CustomerStandardModel> customerStandardModelList = customerStandardRepository.getCustomerStandardsByReqId(updateStdRequirementsModel.getStdRqrId());
					if (customerStandardModelList.size() > 0) {
						for (CustomerStandardModel customerStandardModel : customerStandardModelList) {
							customerStandardModel.setCstQstAnswer(null);
							customerStandardRepository.save(customerStandardModel);
						}
					}
				}
			}

			//Generic Answer 1
			if (updateStdRequirementsModel.getStdQtrQuestionsModel().getAnswerDate1Type() != null && stdRequirementsModel.getAnswerDate1Type() != null) {
				if (!updateStdRequirementsModel.getStdQtrQuestionsModel().getAnswerDate1Type().equals(stdRequirementsModel.getAnswerDate1Type())) {
					List<CustomerStandardModel> customerStandardModelList = customerStandardRepository.getCustomerStandardsByReqId(updateStdRequirementsModel.getStdRqrId());
					if (customerStandardModelList.size() > 0) {
						for (CustomerStandardModel customerStandardModel : customerStandardModelList) {
							customerStandardModel.setAnswerDate1(null);
							customerStandardRepository.save(customerStandardModel);
						}
					}
				}
			}

			//Generic Answer 2
			if (updateStdRequirementsModel.getStdQtrQuestionsModel().getAnswerDate2Type() != null && stdRequirementsModel.getAnswerDate2Type() != null) {
				if (!updateStdRequirementsModel.getStdQtrQuestionsModel().getAnswerDate2Type().equals(stdRequirementsModel.getAnswerDate2Type())) {
					List<CustomerStandardModel> customerStandardModelList = customerStandardRepository.getCustomerStandardsByReqId(updateStdRequirementsModel.getStdRqrId());
					if (customerStandardModelList.size() > 0) {
						for (CustomerStandardModel customerStandardModel : customerStandardModelList) {
							customerStandardModel.setAnswerDate2(null);
							customerStandardRepository.save(customerStandardModel);
						}
					}
				}
			}

			// Setting the generics Answer types
			updateStdRequirementsModel.setQtrQstType(stdRequirementsModel.getQtrQstType());
			updateStdRequirementsModel.setAnswerDate2Type(stdRequirementsModel.getAnswerDate2Type());
			updateStdRequirementsModel.setAnswerDate1Type(stdRequirementsModel.getAnswerDate1Type());

			// Setting the other fields
			updateStdRequirementsModel.setStdRqrName(stdRequirementsModel.getStdRqrName());
			updateStdRequirementsModel.setStdRqrTitle(stdRequirementsModel.getStdRqrTitle());
			updateStdRequirementsModel.setStdRqrDescription(stdRequirementsModel.getStdRqrDescription());
			updateStdRequirementsModel.setStdRqrExplanation(stdRequirementsModel.getStdRqrExplanation());
			updateStdRequirementsModel.setArticleNum(stdRequirementsModel.getArticleNum());
			updateStdRequirementsModel.setStdRqrIsMandatory(stdRequirementsModel.isStdRqrIsMandatory());
			updateStdRequirementsModel.setSectionId(stdSectionModel.getStdSctId());
			updateStdRequirementsModel.setFileFreeText(stdRequirementsModel.getFileFreeText());
			LOGGER.info("update requirement");
			StdQtrQuestionsModel stdQtrQuestionsModel = stdQtrQuestionsService.getQuestionsByRequirementId(stdRequirementsModel.getStdRqrId());

			//Deleting the exist Option that we could save the new option + the previous options
			try {
				stdQstOptionService.deleteStdQstOptionByQtsId(stdQtrQuestionsModel.getQtrQstId());
				stdAnswer1OptionService.deleteStdAnswer1OptionByQtsId(stdQtrQuestionsModel.getQtrQstId());
				stdAnswer2OptionService.deleteStdAnswer2OptionByQtsId(stdQtrQuestionsModel.getQtrQstId());
			} catch (Exception ex) {
				LOGGER.error(ex.getMessage());
			}

			//Setting the generic answer dropdown type
			updateStdRequirementsModel.setStdQstOptionModels(stdRequirementsModel.getStdQstOptionModels());
			updateStdRequirementsModel.setStdAnswer1OptionModels(stdRequirementsModel.getStdAnswer1OptionModels());
			updateStdRequirementsModel.setStdAnswer2OptionModels(stdRequirementsModel.getStdAnswer2OptionModels());

			updateStdQtrQuestionsModel(stdQtrQuestionsModel, updateStdRequirementsModel, stdSectionModel);

			try {
				stdRequirementsService.update(stdRequirementsModel.getStdRqrId(), updateStdRequirementsModel);
			} catch (Exception ex) {
				LOGGER.error(ex.getMessage());
			}
		}
	}

	private void createCustomerStandardToCompany(StandardModel standardModel, CompanyModel companyModel, StdRequirementsModel stdRequirementsModel) throws Exception {
		StdQtrQuestionsModel stdQtrQuestionsModel = stdQtrQuestionsService.getQuestionsByRequirementId(stdRequirementsModel.getStdRqrId());
		if (stdQtrQuestionsModel != null)
			customerStandardService.createCustomerStandard(stdRequirementsModel, stdQtrQuestionsModel, standardModel, companyModel);
	}

	public StdQtrQuestionsModel createStdQtrQuestionsModel(StdRequirementsModel stdRequirementsModel, StdSectionModel stdSectionModel, MultipartFile file) throws Exception {
		StdQtrQuestionsModel stdQtrQuestion = stdQtrQuestionsService.getQuestionsByRequirementId(stdRequirementsModel.getStdRqrId());
		if (stdQtrQuestion != null && stdQtrQuestion.getQtrQstId() > 0) {
			throw new Exception("This requirement already has a question");
		} else {

			StdQtrQuestionsModel newStdQtrQuestionsModel = new StdQtrQuestionsModel();
			newStdQtrQuestionsModel.setQtrQstName(stdRequirementsModel.getStdRqrName());
			newStdQtrQuestionsModel.setStdSctId(stdSectionModel);
			newStdQtrQuestionsModel.setQtrQstType(stdRequirementsModel.getQtrQstType());
			newStdQtrQuestionsModel.setAnswerDate1Type(stdRequirementsModel.getAnswerDate1Type());
			newStdQtrQuestionsModel.setAnswerDate2Type(stdRequirementsModel.getAnswerDate2Type());
			newStdQtrQuestionsModel.setStdRqrId(stdRequirementsModel);
			StdQtrQuestionsModel stdQtrQuestionsModel = stdQtrQuestionsService.create(newStdQtrQuestionsModel);

			LOGGER.info("Create new question id: " + stdQtrQuestionsModel.getQtrQstId());

			if (stdRequirementsModel.getStdQstOptionModels() != null && stdRequirementsModel.getStdQstOptionModels().size() > 0) {
				stdQtrQuestionsModel.setStdQstOptionModel(createStdQstOptionModels(stdRequirementsModel, stdQtrQuestionsModel));
			}

			if (stdRequirementsModel.getStdAnswer1OptionModels() != null && stdRequirementsModel.getStdAnswer1OptionModels().size() > 0) {
				stdQtrQuestionsModel.setStdAnswer1OptionModel(createStdAnswer1OptionModels(stdRequirementsModel, stdQtrQuestionsModel));
			}

			if (stdRequirementsModel.getStdAnswer2OptionModels() != null && stdRequirementsModel.getStdAnswer2OptionModels().size() > 0) {
				stdQtrQuestionsModel.setStdAnswer2OptionModel(createStdAnswer2OptionModels(stdRequirementsModel, stdQtrQuestionsModel));
			}
			return stdQtrQuestionsModel;
		}
	}

	private void updateStdQtrQuestionsModel(StdQtrQuestionsModel stdQtrQuestionsModel, StdRequirementsModel stdRequirementsModel, StdSectionModel stdSectionModel) throws Exception {
		if (stdQtrQuestionsModel != null) {
			stdQtrQuestionsModel.setQtrQstName(stdRequirementsModel.getStdRqrName());
			stdQtrQuestionsModel.setStdSctId(stdSectionModel);
			stdQtrQuestionsModel.setQtrQstType(stdRequirementsModel.getQtrQstType());
			stdQtrQuestionsModel.setAnswerDate1Type(stdRequirementsModel.getAnswerDate1Type());
			stdQtrQuestionsModel.setAnswerDate2Type(stdRequirementsModel.getAnswerDate2Type());
			stdQtrQuestionsModel.setStdRqrId(stdRequirementsModel);
			LOGGER.info("update question.");
			StdQtrQuestionsModel questionsModel = stdQtrQuestionsService.update(stdQtrQuestionsModel.getQtrQstId(), stdQtrQuestionsModel);

			if (stdRequirementsModel.getStdQstOptionModels() != null && stdRequirementsModel.getStdQstOptionModels().size() > 0) {
				createStdQstOptionModels(stdRequirementsModel, questionsModel);
			}
			if (stdRequirementsModel.getStdAnswer1OptionModels() != null && stdRequirementsModel.getStdAnswer1OptionModels().size() > 0) {
				createStdAnswer1OptionModels(stdRequirementsModel, questionsModel);
			}
			if (stdRequirementsModel.getStdAnswer2OptionModels() != null && stdRequirementsModel.getStdAnswer2OptionModels().size() > 0) {
				createStdAnswer2OptionModels(stdRequirementsModel, questionsModel);
			}

		}
	}

	private List<StdQstOptionModel> createStdQstOptionModels(StdRequirementsModel stdRequirementsModel, StdQtrQuestionsModel stdQtrQuestionsModel) throws Exception {
		if (stdRequirementsModel != null && stdQtrQuestionsModel != null) {
			List<StdQstOptionModel> newStdQstOptionModels = new ArrayList<>();
			List<StdQstOptionModel> stdQstOptionModels = stdRequirementsModel.getStdQstOptionModels();
			if (stdQstOptionModels != null && stdQstOptionModels.size() > 0) {
				for (StdQstOptionModel tmpStdQstOptionModel : stdQstOptionModels) {
					StdQstOptionModel stdQstOptionModel = new StdQstOptionModel();
					stdQstOptionModel.setSqoLabel(tmpStdQstOptionModel.getSqoLabel());
					stdQstOptionModel.setSqoValue(tmpStdQstOptionModel.getSqoValue());
					stdQstOptionModel.setSqoQstId(stdQtrQuestionsModel);
					LOGGER.info("create  stdQstOptionModel.");
					newStdQstOptionModels.add(stdQstOptionService.create(stdQstOptionModel));
				}
			}
			return newStdQstOptionModels;
		}
		return null;
	}

	private List<StdAnswer1OptionModel> createStdAnswer1OptionModels(StdRequirementsModel stdRequirementsModel, StdQtrQuestionsModel stdQtrQuestionsModel) throws Exception {
		if (stdRequirementsModel != null && stdQtrQuestionsModel != null) {
			List<StdAnswer1OptionModel> newStdAnswer1OptionModels = new ArrayList<>();
			List<StdAnswer1OptionModel> stdAnswer1OptionModels = stdRequirementsModel.getStdAnswer1OptionModels();
			if (stdAnswer1OptionModels != null && stdAnswer1OptionModels.size() > 0) {
				for (StdAnswer1OptionModel tmpStdAnswer1OptionModel : stdAnswer1OptionModels) {
					StdAnswer1OptionModel stdAnswer1OptionModel = new StdAnswer1OptionModel();
					stdAnswer1OptionModel.setsA1Label(tmpStdAnswer1OptionModel.getsA1Label());
					stdAnswer1OptionModel.setsA1Value(tmpStdAnswer1OptionModel.getsA1Value());
					stdAnswer1OptionModel.setSqoQstId(stdQtrQuestionsModel);
					LOGGER.info("create  stdAnswer1OptionModel.");
					newStdAnswer1OptionModels.add(stdAnswer1OptionService.create(stdAnswer1OptionModel));
				}
			}
			return newStdAnswer1OptionModels;
		}
		return null;
	}

	private List<StdAnswer2OptionModel> createStdAnswer2OptionModels(StdRequirementsModel stdRequirementsModel, StdQtrQuestionsModel stdQtrQuestionsModel) throws Exception {
		if (stdRequirementsModel != null && stdQtrQuestionsModel != null) {
			List<StdAnswer2OptionModel> newStdAnswer2OptionModels = new ArrayList<>();
			List<StdAnswer2OptionModel> stdAnswer2OptionModels = stdRequirementsModel.getStdAnswer2OptionModels();
			if (stdAnswer2OptionModels != null && stdAnswer2OptionModels.size() > 0) {
				for (StdAnswer2OptionModel tmpStdAnswer2OptionModel : stdAnswer2OptionModels) {
					StdAnswer2OptionModel stdAnswer2OptionModel = new StdAnswer2OptionModel();
					stdAnswer2OptionModel.setsA2Label(tmpStdAnswer2OptionModel.getsA2Label());
					stdAnswer2OptionModel.setsA2Value(tmpStdAnswer2OptionModel.getsA2Value());
					stdAnswer2OptionModel.setSqoQstId(stdQtrQuestionsModel);
					LOGGER.info("create  stdAnswer2OptionModel.");
					newStdAnswer2OptionModels.add(stdAnswer2OptionService.create(stdAnswer2OptionModel));
				}
			}
			return newStdAnswer2OptionModels;
		}
		return null;
	}

	public RequestQuestionnaireModel getRequestQuestionnaire(int id, String token) throws Exception {
		LOGGER.info("Get Request Questionnaire id: " + id);
		RequestQuestionnaireModel requestQuestionnaireModel = new RequestQuestionnaireModel();
		StdQuestionaireModel stdQuestionaireModel = stdQuestionaireService.getById(id);
		requestQuestionnaireModel.setStdQtrId(id);
		requestQuestionnaireModel.setStdQtrName(stdQuestionaireModel.getStdQtrName());
		List<StdSectionModel> stdSectionModels = stdSectionService.getAllSectionByQuestionaireIdWithOutParent(id);
		if (stdSectionModels != null && stdSectionModels.size() > 0) {
			List<ChildrenQuestionnaireModel> childrenQuestionnaireModels = new ArrayList<>();
			for (StdSectionModel tmpStdSectionModel : stdSectionModels) {
				ChildrenQuestionnaireModel childrenQuestionnaireModel = new ChildrenQuestionnaireModel();
				childrenQuestionnaireModel.setType("section");
				childrenQuestionnaireModel.setStdSectionModel(tmpStdSectionModel);
				List<ChildrenQuestionnaireModel> recursiveChildren = getChildren(tmpStdSectionModel.getStdSctId(), token);
				if (recursiveChildren != null && recursiveChildren.size() > 0) {
					childrenQuestionnaireModel.setChildren(recursiveChildren);
				}
				if (childrenQuestionnaireModel != null) {
					childrenQuestionnaireModels.add(childrenQuestionnaireModel);
				}
			}
			if (childrenQuestionnaireModels != null)
				requestQuestionnaireModel.setChildren(childrenQuestionnaireModels);
		}
		return requestQuestionnaireModel;
	}

	private List<ChildrenQuestionnaireModel> getChildren(int id, String token) throws Exception {

		// get relevant properties to company user
		UsersEnum usersEnum = authenticationService.getUserType(token);
		CompanyModel companyModel = null;
		if (usersEnum.equals(UsersEnum.CompanyUser)) {
			// get user's company for module tasks
			companyModel = authenticationService.getCompanyFromToken(token);
		}

		List<ChildrenQuestionnaireModel> childrenQuestionnaireModels = new ArrayList<>();
		List<StdQtrQuestionsModel> questionChild = stdQtrQuestionsService.getAllQuestionBySectionId(id);
		List<StdSectionModel> sectionChild = stdSectionService.getAllSectionByParentId(id);
		if (questionChild != null && questionChild.size() > 0) {
			for (StdQtrQuestionsModel tmpStdQtrQuestionsModel : questionChild) {
				StdRequirementsModel stdRequirementsModel = stdRequirementsService.getById(tmpStdQtrQuestionsModel.getStdRqrId().getStdRqrId());
				stdRequirementsModel.setQtrQstType(tmpStdQtrQuestionsModel.getQtrQstType());
				stdRequirementsModel.setAnswerDate1Type(tmpStdQtrQuestionsModel.getAnswerDate1Type());
				stdRequirementsModel.setAnswerDate2Type(tmpStdQtrQuestionsModel.getAnswerDate2Type());
				stdRequirementsModel.setStdQstOptionModels(tmpStdQtrQuestionsModel.getStdQstOptionModel());

				//Setting the generic answer dropdown type
				stdRequirementsModel.setStdAnswer1OptionModels(tmpStdQtrQuestionsModel.getStdAnswer1OptionModel());
				stdRequirementsModel.setStdAnswer2OptionModels(tmpStdQtrQuestionsModel.getStdAnswer2OptionModel());


				if (companyModel != null && moduleTaskService.hasPermission(token, OperationEnum.READ, ModuleEnum.PROJECT)) {
					if (stdRequirementsModel.getCustomerStandard() != null && stdRequirementsModel.getCustomerStandard().getCstStdId() > 0) {
						ModuleTaskModel moduleTaskModel = moduleTaskService.getModuleTaskByModuleAndIdAndCmpId(ModuleEnum.CUSTOMER_STANDARD, stdRequirementsModel.getCustomerStandard().getCstStdId(), companyModel.getCmpId());
						stdRequirementsModel.setModuleTaskModel(moduleTaskModel);
					}
				}
				ChildrenQuestionnaireModel childrenQuestionnaireModel = new ChildrenQuestionnaireModel();
				childrenQuestionnaireModel.setType("requirement");
				childrenQuestionnaireModel.setStdRequirementsModel(stdRequirementsModel);
				childrenQuestionnaireModels.add(childrenQuestionnaireModel);
			}
		}
		if (sectionChild != null && sectionChild.size() > 0) {
			for (StdSectionModel tmpStdSectionModel : sectionChild) {
				ChildrenQuestionnaireModel childrenQuestionnaireModel = new ChildrenQuestionnaireModel();
				childrenQuestionnaireModel.setType("section");
				childrenQuestionnaireModel.setStdSectionModel(tmpStdSectionModel);
				childrenQuestionnaireModel.setChildren(getChildren(tmpStdSectionModel.getStdSctId(), token));
				childrenQuestionnaireModels.add(childrenQuestionnaireModel);
			}
		}
		return childrenQuestionnaireModels;
	}

	public ByteArrayInputStream questionnaireToExcel(int standardId, int questionnaireId, int cmpId) throws Exception {
		String[] columns = {"Requirement", "Testing procedure", "Guidance", "Answer", "Compliant", "Remark"};
		try (
			Workbook workbook = new XSSFWorkbook();
			ByteArrayOutputStream out = new ByteArrayOutputStream()
		) {
			CreationHelper createHelper = workbook.getCreationHelper();
			Sheet sheet = workbook.createSheet("Questionnaire");
			Font headerFont = workbook.createFont();
			headerFont.setBold(true);
			headerFont.setColor(IndexedColors.BLUE.getIndex());

			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			headerCellStyle.setWrapText(true);
			headerCellStyle.setVerticalAlignment(VerticalAlignment.TOP);

			CellStyle regularCellStyle = workbook.createCellStyle();
			regularCellStyle.setWrapText(true);
			regularCellStyle.setVerticalAlignment(VerticalAlignment.TOP);

			StandardModel standardModel = standardService.getById(standardId);
			StdQuestionaireModel stdQuestionaireModel = stdQuestionaireService.getById(questionnaireId);

			Row header = sheet.createRow(0);

			Cell standardNameCell = header.createCell(0);
			standardNameCell.setCellValue("Standard name: " + standardModel.getStdName());
			standardNameCell.setCellStyle(headerCellStyle);

			Cell questionnaireCell = header.createCell(2);
			questionnaireCell.setCellValue("Questionnaire name : " + stdQuestionaireModel.getStdQtrName());
			questionnaireCell.setCellStyle(headerCellStyle);

			CellStyle dateCellStyle = workbook.createCellStyle();
			dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));

			Cell dateCell = header.createCell(4);
			dateCell.setCellValue("Date : " + new Date());
			dateCell.setCellStyle(dateCellStyle);

			int rowIndex = 2;

			// Row for Header
			Row headerRow = sheet.createRow(rowIndex);

			// Header
			for (int col = 0; col < columns.length; col++) {
				Cell cell = headerRow.createCell(col);
				cell.setCellValue(columns[col]);
				cell.setCellStyle(headerCellStyle);
			}

			List<StdSectionModel> stdSectionModels = stdSectionService.getAllSectionByQuestionaireId(questionnaireId);
			if (stdSectionModels != null && stdSectionModels.size() > 0) {
				for (StdSectionModel tmpStdSectionModel : stdSectionModels) {
					List<StdQtrQuestionsModel> stdQtrQuestionsModels = stdQtrQuestionsService.getAllQuestionBySectionId(tmpStdSectionModel.getStdSctId());
					if (stdQtrQuestionsModels != null && stdQtrQuestionsModels.size() > 0) {
						for (StdQtrQuestionsModel tmpStdQtrQuestionsModel : stdQtrQuestionsModels) {
							if (tmpStdQtrQuestionsModel.getStdRqrId() != null && tmpStdQtrQuestionsModel.getStdRqrId().getStdRqrId() != 0) {
								StdRequirementsModel stdRequirementsModel = stdRequirementsService.getById(tmpStdQtrQuestionsModel.getStdRqrId().getStdRqrId());
								if (stdRequirementsModel != null) {
									rowIndex++;
									Row row = sheet.createRow(rowIndex);
									if (stdRequirementsModel.getStdRqrName() != null) {
										Cell cell = row.createCell(0);
										cell.setCellValue(stdRequirementsModel.getStdRqrName());
										cell.setCellStyle(regularCellStyle);
									}
									if (stdRequirementsModel.getStdRqrDescription() != null) {
										Cell cell = row.createCell(1);
										cell.setCellValue(stdRequirementsModel.getStdRqrDescription());
										cell.setCellStyle(regularCellStyle);
									}
									if (stdRequirementsModel.getStdRqrExplanation() != null) {
										Cell cell = row.createCell(2);
										cell.setCellValue(stdRequirementsModel.getStdRqrExplanation());
										cell.setCellStyle(regularCellStyle);
									}
									CustomerStandardModel customerStandardModel = customerStandardService.getCustomerStandardByReqIdAndCmpId(stdRequirementsModel.getStdRqrId(), cmpId);
									if (customerStandardModel != null) {
										if (customerStandardModel.getIsFileExist()) {
											Cell cell = row.createCell(3);
											cell.setCellValue(customerStandardModel.getFile().getFileName());
											cell.setCellStyle(regularCellStyle);
										} else if (customerStandardModel.getCstQstAnswer() != null) {
											Cell cell = row.createCell(3);
//											cell.setCellValue(customerStandardModel.getCstQstAnswer());
											cell.setCellStyle(regularCellStyle);
										}
										if (customerStandardModel.getCompliant() != null) {
											Cell cell = row.createCell(4);
											cell.setCellValue(customerStandardModel.getCompliant().toString());
											cell.setCellStyle(regularCellStyle);
										}

										if (customerStandardModel.getCstStdRemarks() != null) {
											Cell cell = row.createCell(5);
											cell.setCellValue(customerStandardModel.getCstStdRemarks());
											cell.setCellStyle(regularCellStyle);
										}
									}
								}
							}
						}
					}
				}
			}
			sheet.setDefaultColumnWidth(30);
			workbook.write(out);
			return new ByteArrayInputStream(out.toByteArray());
		}
	}

}
