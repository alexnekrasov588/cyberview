package com.lstechs.ciso.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.ldap.filter.AndFilter;
import org.springframework.ldap.filter.EqualsFilter;
import org.springframework.stereotype.Service;

import javax.naming.NameNotFoundException;
import javax.naming.NamingEnumeration;
import javax.naming.directory.*;
import javax.naming.ldap.LdapContext;

@Service
public class ActiveDirectoryLdapService {

	private static Logger LOGGER = LogManager.getLogger(ActiveDirectoryLdapService.class.getName());

	public String getUserDN(LdapContext ctx, String searchBase, String userName) {
		LOGGER.info("trying to get DN of userName \"" + userName + "\" using baseDN " + searchBase);
		try {
			SearchResult searchResults = findUserBy(ctx, searchBase, "sAMAccountName", userName);
			if (searchResults != null) {
				return searchResults.getNameInNamespace();
			}
			return null;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 *
	 * @param ctx - LDAP Context object
	 * @param searchBase - LDAP searchBase, ie: DC=domain,DC=local
	 * @param searchCriteria - LDAP filter attribute, ie: sAMAccountName
	 * @param searchValue - LDAP filter attribute value, ie: ciso
	 * @return SearchResult
	 * @throws Exception
	 */
	public SearchResult findUserBy(LdapContext ctx, String searchBase, String searchCriteria, String searchValue)
		throws Exception {

		// build the filter query
		final AndFilter andFilter = new AndFilter();
		andFilter.and(new EqualsFilter("objectClass", "user"));
		andFilter.and(new EqualsFilter(searchCriteria, searchValue));

		// String searchFilter = "(&(objectClass=user)(sAMAccountName=" + username + "))";

		// encode filter query to string
		String searchFilter = andFilter.encode();

		// define search controls
		SearchControls searchControls = new SearchControls();
		searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);

		SearchResult searchResult = null;
		try {
			NamingEnumeration<SearchResult> results = ctx.search(searchBase, searchFilter, searchControls);
			if (results.hasMoreElements()) {
				searchResult = results.nextElement();

				//make sure there is not another item available, there should be only 1 match
				if (results.hasMoreElements()) {
					LOGGER.error("Matched multiple users for the " + searchCriteria + ": " + searchValue);
					return null;
				}
			}
			return searchResult;
		} catch (NameNotFoundException ex) {
			LOGGER.error("User not found: " + searchFilter, ex);
			return null;
		} catch (Exception e) {
			LOGGER.error("Error searching Active directory for " + searchFilter);
			throw e;
		}
	}

	public String extractAttributeFromResults(SearchResult userData, String attributeName) throws Exception {
		try {
			String attributeValue = null;
			// getting only the first result if we have more than one
			if (userData != null) {
				Attributes attributes = userData.getAttributes();
				Attribute attribute = attributes.get(attributeName);
				if (attribute != null) {
					attributeValue = attribute.get().toString();
					LOGGER.info("found attribute " + attributeName + " with value: " + attributeValue);
				} else {
					LOGGER.info("attribute " + attributeName + " not found");
				}
			}

			return attributeValue;
		} catch (Exception e) {
			LOGGER.error("Error fetching " + attributeName + " attribute from object");
			throw e;
		}
	}
}
