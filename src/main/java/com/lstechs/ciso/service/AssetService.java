package com.lstechs.ciso.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import com.lstechs.ciso.repository.CompanyRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.lstechs.ciso.enums.AssetTypeEnum;
import com.lstechs.ciso.model.AssetModel;
import com.lstechs.ciso.model.AssetTypeModel;
import com.lstechs.ciso.model.CompanyModel;
import com.lstechs.ciso.model.FilterModel;
import com.lstechs.ciso.model.SingleAssetModel;
import com.lstechs.ciso.repository.AssetRepository;

@Service
public class AssetService extends CRUDService<AssetModel, SingleAssetModel> {

	@Autowired
	private AssetRepository assetRepository;

	@Autowired
	private CompanyRepository companyRepository;

	@Autowired
	private AssetTypeService assetTypeService;

	public AssetService(AssetRepository assetRepository) {
		super(assetRepository);
	}

	private static final Logger LOGGER = LogManager.getLogger(AssetService.class.getName());

	public Page<AssetModel> getAllAssetByCmpId(Integer pageNo, Integer pageSize, String orderBy, boolean orderByDesc,
											   String query, int cmpId, Map<String, String> allParams) throws ParseException {

		Sort sort = Sort.by(Sort.Order.desc(orderBy));
		if (!orderByDesc) {
			sort = Sort.by(Sort.Order.asc(orderBy));
		}
		Pageable paging = PageRequest.of(pageNo, pageSize, sort);


		List<String> astName = null;
		List<String> astType = null;
		List<String> astOperator = null;
		List<String> astOwner = null;

		List<String> astModel = null;
		List<String> astVersion = null;
		List<String> astManufactor = null;
		List<String> astProduct = null;
		AssetTypeEnum type = null;

		Date[] astStartDate = null;
		Date[] astEndDate = null;

		Date astStartDateFrom = null;
		Date astStartDateTo = null;
		Date astEndDateFrom = null;
		Date astEndDateTo = null;

		boolean isStartDateExist = false;
		boolean isEndDateExist = false;

		for (Map.Entry<String, String> entry : allParams.entrySet()) {

			String k = entry.getKey();
			String v = entry.getValue();
			String[] array = v.split(",");


			if (k.equals("type")) {

				try {
					type = AssetTypeEnum.valueOf(v);
				} catch (Exception ex) {
					LOGGER.error(ex.getMessage());
				}

			}
			if (k.equals("astName")) {
				astName = Arrays.asList(array);
			}
			if (k.equals("astType")) {
				astType = Arrays.asList(array);
			}
			if (k.equals("astModel")) {
				astModel = Arrays.asList(array);
			}
			if (k.equals("astVersion")) {
				astVersion = Arrays.asList(array);
			}
			if (k.equals("astOperator")) {
				astOperator = Arrays.asList(array);
			}
			if (k.equals("astOwner")) {
				astOwner = Arrays.asList(array);
			}
			if (k.equals("astManufactor")) {
				astManufactor = Arrays.asList(array);
			}

			if (k.equals("astProduct")) {
				astProduct = Arrays.asList(array);
			}

			if (k.equals("astStartDate")) {
				astStartDate = convertStringsToDates(array);
			}
			if (k.equals("astEndDate")) {
				astEndDate = convertStringsToDates(array);
			}

		}
		if (!query.equals("") && query.length() > 0)
			return assetRepository.getAllAssetByCmpId(paging, cmpId, query, type);
		if (astName != null || astType != null || astModel != null || astOperator != null || astOwner != null
			|| astVersion != null || astManufactor != null || astProduct != null
			|| astEndDate != null || astStartDate != null) {

			if (astEndDate != null) {
				astEndDateFrom = astEndDate[0];
				astEndDateTo = astEndDate[1];
				isEndDateExist = true;

			}
			if (astStartDate != null) {
				astStartDateFrom = astStartDate[0];
				astStartDateTo = astStartDate[1];
				isStartDateExist = true;
			}

			return assetRepository.getAssetsFilter(paging, cmpId, astName, astType, astOperator, astOwner, astVersion, astManufactor, astProduct, astModel, type,
				isStartDateExist, astStartDateFrom, astStartDateTo,
				isEndDateExist, astEndDateFrom, astEndDateTo);
		}
		return assetRepository.getAllAssetByCmpId(paging, cmpId, query, type);
	}

	private Date[] convertStringsToDates(String[] str) throws ParseException {
		Date[] dates = new Date[str.length];

		for (int i = 0; i < str.length; i++) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			dates[i] = sdf.parse(str[i]);

		}
		return dates;
	}


	public List<FilterModel> getAllAssetForNews(int cmpId) throws ParseException {
		return assetRepository.getAllAssetForNews(cmpId);
	}

	@Override
	public SingleAssetModel getSingleInstance(AssetModel assetModel) throws Exception {
		SingleAssetModel singleAssetModel = new SingleAssetModel();
		singleAssetModel.setAstName(assetModel.getAstName());
		singleAssetModel.setAstDescrption(assetModel.getAstDescrption());
		singleAssetModel.setAstLocation(assetModel.getAstLocation());
		singleAssetModel.setAstOperator(assetModel.getAstOperator());
		singleAssetModel.setAstOwner(assetModel.getAstOwner());
		singleAssetModel.setAstAmount(assetModel.getAstAmount());
		singleAssetModel.setAstType(assetModel.getAstType());
		return singleAssetModel;

	}

	@Override
	@CacheEvict(value = {"filterByTags", "findActiveNewsForDashboard", "findActiveAssetNewsForDashboard"}, allEntries = true)
	public AssetModel create(AssetModel assetModel) throws Exception {
		AssetTypeModel assetTypeModel = assetTypeService.getById(assetModel.getAstType().getAstTypeId());
		assetModel.setAstType(assetTypeModel);
		try {
			return super.create(assetModel);
		} catch (Exception ex) {
			LOGGER.error("create asset faild , ex: " + ex);
			throw new Exception(ex);
		}
	}

	@Override
	@CacheEvict(value = {"filterByTags", "findActiveNewsForDashboard", "findActiveAssetNewsForDashboard"}, allEntries = true)
	public AssetModel update(int id, AssetModel assetModel) throws Exception {
		AssetModel updateAssetModel = getById(id);
		LOGGER.info("update asset id: " + id);
		AssetTypeModel assetTypeModel = assetTypeService.getById(assetModel.getAstType().getAstTypeId());
		assetModel.setAstType(assetTypeModel);
		updateAssetModel.setAstAmount(assetModel.getAstAmount());
		updateAssetModel.setAstDescrption(assetModel.getAstDescrption());
		updateAssetModel.setAstLocation(assetModel.getAstLocation());
		updateAssetModel.setAstName(assetModel.getAstName());
		updateAssetModel.setAstOperator(assetModel.getAstOperator());
		updateAssetModel.setAstOwner(assetModel.getAstOwner());
		updateAssetModel.setAstType(assetModel.getAstType());
		updateAssetModel.setAstManufactor(assetModel.getAstManufactor());
		updateAssetModel.setAstModel(assetModel.getAstModel());
		updateAssetModel.setAstProduct(assetModel.getAstProduct());
		updateAssetModel.setAstVersion(assetModel.getAstVersion());
		updateAssetModel.setAstStartDate(assetModel.getAstStartDate());
		updateAssetModel.setAstEndDate(assetModel.getAstEndDate());
		return assetRepository.save(updateAssetModel);
	}

	public List<String> getAllAssetName(int cmpId, String type) {
		AssetTypeEnum assetTypeEnum = null;
		try {
			assetTypeEnum = AssetTypeEnum.valueOf(type);
		} catch (Exception ex) {
			LOGGER.error(ex.getMessage());
		}

		return assetRepository.getAllAssetName(cmpId, assetTypeEnum);
	}

	public List<String> getAllAssetManufactor(int cmpId, String type) {
		AssetTypeEnum assetTypeEnum = null;
		try {
			assetTypeEnum = AssetTypeEnum.valueOf(type);
		} catch (Exception ex) {
			LOGGER.error(ex.getMessage());
		}
		return assetRepository.getAllAssetManufactor(cmpId, assetTypeEnum);
	}

	public List<String> getAllAssetProduct(int cmpId, String type) {
		AssetTypeEnum assetTypeEnum = null;
		try {
			assetTypeEnum = AssetTypeEnum.valueOf(type);
		} catch (Exception ex) {
			LOGGER.error(ex.getMessage());
		}
		return assetRepository.getAllAssetProduct(cmpId, assetTypeEnum);
	}

	public List<String> getAllAssetModel(int cmpId, String type) {
		AssetTypeEnum assetTypeEnum = null;
		try {
			assetTypeEnum = AssetTypeEnum.valueOf(type);
		} catch (Exception ex) {
			LOGGER.error(ex.getMessage());
		}
		return assetRepository.getAllAssetModel(cmpId, assetTypeEnum);
	}

	public List<String> getAllAssetVersion(int cmpId, String type) {
		AssetTypeEnum assetTypeEnum = null;
		try {
			assetTypeEnum = AssetTypeEnum.valueOf(type);
		} catch (Exception ex) {
			LOGGER.error(ex.getMessage());
		}
		return assetRepository.getAllAssetVersion(cmpId, assetTypeEnum);
	}

	public List<String> getAllAssetOperator(int cmpId, String type) {
		AssetTypeEnum assetTypeEnum = null;
		try {
			assetTypeEnum = AssetTypeEnum.valueOf(type);
		} catch (Exception ex) {
			LOGGER.error(ex.getMessage());
		}
		return assetRepository.getAllAssetOperator(cmpId, assetTypeEnum);
	}

	public List<String> getAllAssetOwner(int cmpId, String type) {
		AssetTypeEnum assetTypeEnum = null;
		try {
			assetTypeEnum = AssetTypeEnum.valueOf(type);
		} catch (Exception ex) {
			LOGGER.error(ex.getMessage());
		}
		return assetRepository.getAllAssetOwner(cmpId, assetTypeEnum);
	}

	public List<String> getAllAssetType(int cmpId, String type) {
		AssetTypeEnum assetTypeEnum = null;
		try {
			assetTypeEnum = AssetTypeEnum.valueOf(type);
		} catch (Exception ex) {
			LOGGER.error(ex.getMessage());
		}
		return assetRepository.getAllAssetType(cmpId, assetTypeEnum);
	}


	public List<AssetModel> createAssetsFromExcelFile(MultipartFile file, CompanyModel cmpId, String type) throws Exception {
		List<AssetModel> assetModels = new ArrayList<AssetModel>();
		XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());
		XSSFSheet worksheet = workbook.getSheetAt(0);
		AssetTypeEnum assetTypeEnum = AssetTypeEnum.valueOf(type);

		for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
			AssetModel assetModel = new AssetModel();
			XSSFRow row = worksheet.getRow(i);
			//0-Name	1-Type	2-Descrption	3-Location	4-Operator	5-Owner	6-Manufactor	7-Product	8-Version 9-Model	10-Start Date	11-End Date	12-Amount
			try {
				assetModel.setAstName(convertCellToString(workbook, row.getCell(0)));

				String typeName = convertCellToString(workbook, row.getCell(1));
				AssetTypeModel assetTypeModel = assetTypeService.getAssetTypeByName(cmpId.getCmpId(), typeName, assetTypeEnum);
				if (assetTypeModel != null)
					assetModel.setAstType(assetTypeModel);
				else {
					AssetTypeModel created = new AssetTypeModel();
					created.setAstType(assetTypeEnum);
					created.setAstTypeName(typeName);
					created.setCmpId(cmpId);
					assetModel.setAstType(assetTypeService.create(created));
				}

				Cell c;
				c = row.getCell(2);
				if (c != null)
					assetModel.setAstDescrption(convertCellToString(workbook, row.getCell(2)));

				c = row.getCell(3);
				if (c != null)
					assetModel.setAstLocation(convertCellToString(workbook, row.getCell(3)));

				c = row.getCell(4);
				if (c != null)
					assetModel.setAstOperator(convertCellToString(workbook, row.getCell(4)));

				c = row.getCell(5);
				if (c != null)
					assetModel.setAstOwner(convertCellToString(workbook, row.getCell(5)));

				c = row.getCell(6);
				if (c != null)
					assetModel.setAstManufactor(convertCellToString(workbook, row.getCell(6)));

				c = row.getCell(7);
				if (c != null)
					assetModel.setAstProduct(convertCellToString(workbook, row.getCell(7)));

				c = row.getCell(8);
				if (c != null)
					assetModel.setAstVersion(convertCellToString(workbook, row.getCell(8)));

				c = row.getCell(9);
				if (c != null)
					assetModel.setAstModel(convertCellToString(workbook, row.getCell(9)));

				c = row.getCell(10);
				if (c != null)
					try {
						assetModel.setAstStartDate(row.getCell(10).getDateCellValue());
					} catch (Exception ex) {
						LOGGER.error("Error in file: " + file.getName() + " Line: " + i + " , exception: ", ex);
					}


				c = row.getCell(11);
				if (c != null)
					try {
						assetModel.setAstEndDate(row.getCell(11).getDateCellValue());
					} catch (Exception ex) {
						LOGGER.error("Error in file: " + file.getName() + " Line: " + i + " , exception: ", ex);
					}

				c = row.getCell(12);
				if (c != null)
					try {
						assetModel.setAstAmount((int) row.getCell(12).getNumericCellValue());
					} catch (Exception ex) {
						LOGGER.error("Error in file: " + file.getName() + " Line: " + i + " , exception: ", ex);
					}


				assetModel.setCmpId(cmpId);
				create(assetModel);
				assetModels.add(assetModel);

			} catch (Exception ex) {
				LOGGER.error("Error in file: " + file.getName() + " Line: " + i + " , exception: ", ex);
			}

		}
		return assetModels;
	}

	private String convertCellToString(XSSFWorkbook workbook, Cell cell) {
		String value = "";
		FormulaEvaluator formulaEvaluator = workbook.getCreationHelper().createFormulaEvaluator();
		switch (formulaEvaluator.evaluateInCell(cell).getCellTypeEnum()) {
			case NUMERIC:
				value = String.valueOf(cell.getNumericCellValue());
				break;
			case STRING:
				value = cell.getStringCellValue();
				break;
			case BOOLEAN:
				if (cell.getBooleanCellValue()) {
					value = "true";
				} else {
					value = "false";
				}
			default:
				break;

		}
		return value.trim();
	}

	public Boolean checkForCompanyAssetsStatus(int companyId) {
		boolean hasAssets = false;
		List<AssetModel> assetModel = assetRepository.checkForCompanyAssets(companyId);
		if (assetModel.isEmpty()) {
			return hasAssets;
		} else {
			hasAssets = true;
			return hasAssets;
		}

	}

	public List<AssetModel> deleteAssets(int assetIds[]) throws Exception {
		List<AssetModel> deletedAssets = new ArrayList<>(Collections.emptyList());

		if (assetIds.length > 0 && assetIds != null) {

			LOGGER.info("Delete assets : " + Arrays.toString(assetIds));
			for (int assetId : assetIds) {
				LOGGER.info("Delete asset id: " + assetId);
				AssetModel deletedAsset = delete(assetId);
				deletedAssets.add(deletedAsset);
			}
			return deletedAssets;
		} else {
			LOGGER.info("Requested Assets List For Deletion Is Empty or Null");
			return null;
		}
	}
}
