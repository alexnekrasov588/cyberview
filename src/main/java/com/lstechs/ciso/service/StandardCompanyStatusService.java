package com.lstechs.ciso.service;

import com.lstechs.ciso.model.*;
import com.lstechs.ciso.repository.StandardCompanyStatusRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StandardCompanyStatusService extends CRUDService<StandardCompanyStatusModel, SingleStandardCompanyStatusModel> {

	@Autowired
	private StandardCompanyStatusRepository standardCompanyStatusRepository;

	@Autowired
	private StandardService standardService;

	@Autowired
	private CustomerStandardService customerStandardService;

	@Autowired
	private StdRequirementsService stdRequirementsService;

	@Autowired
	private StdQtrQuestionsService stdQtrQuestionsService;

	@Autowired
	private AuthenticationService authenticationService;

	private static Logger LOGGER = LogManager.getLogger(StandardService.class.getName());

	public StandardCompanyStatusService(StandardCompanyStatusRepository standardCompanyStatusRepository) {
		super(standardCompanyStatusRepository);
	}

	@Override
	public SingleStandardCompanyStatusModel getSingleInstance(StandardCompanyStatusModel standardCompanyStatusModel) throws Exception {
		return new SingleStandardCompanyStatusModel(standardCompanyStatusModel);
	}

	public StandardCompanyStatusModel createStandardCompanyStatusModel(int stdId, CompanyModel companyModel) throws Exception {
		LOGGER.info("create Standard Company Status Model stdId " + stdId + " cmpId: " + companyModel.getCmpId() + ".");
		if (!isStandardCompanyStatusExist(stdId, companyModel.getCmpId())) {
			StandardCompanyStatusModel standardCompanyStatusModel = new StandardCompanyStatusModel();
			StandardModel standardModel = standardService.getById(stdId);
			standardCompanyStatusModel.setStdId(standardModel);
			standardCompanyStatusModel.setCmpId(companyModel);

			// create customer standard to current company
			createCustomerStandardToCompany(standardModel, companyModel);

			return super.create(standardCompanyStatusModel);
		} else {
			throw new Exception("StandardCompanyStatus already exist with stdId: " + stdId + " and cmpId: " + companyModel.getCmpId());
		}
	}

	private void createCustomerStandardToCompany(StandardModel standardModel, CompanyModel companyModel) throws Exception {
		List<StdRequirementsModel> requirementsModelList = stdRequirementsService.getRequirementsListByTemplateId(standardModel.getStdId());
		for (StdRequirementsModel stdRequirementsModel : requirementsModelList) {
			StdQtrQuestionsModel stdQtrQuestionsModel = stdQtrQuestionsService.getQuestionsByRequirementId(stdRequirementsModel.getStdRqrId());
			if(stdQtrQuestionsModel != null)
			customerStandardService.createCustomerStandard(stdRequirementsModel, stdQtrQuestionsModel, standardModel, companyModel);
		}
	}

	public List<SingleStandardCompanyStatusModel> getStandardCompanyStatus(String token) throws Exception {
		LOGGER.info("get Standard Company Status Model by token:  " + token);
		CompanyUserModel companyUserModel = authenticationService.getCompanyUserFromToken(token.substring(7));
		CompanyModel companyModel = companyUserModel.getCmpUsrCmpId();
		List<StandardCompanyStatusModel> standardCompanyStatusModels = standardCompanyStatusRepository.getStandardCompanyStatusByCmpId(companyModel.getCmpId());
		List<SingleStandardCompanyStatusModel> singleStandardCompanyStatusModels = new ArrayList<>();
		if (standardCompanyStatusModels != null && standardCompanyStatusModels.size() > 0) {
			for (StandardCompanyStatusModel tmpStandardCompanyStatusModel : standardCompanyStatusModels) {
				singleStandardCompanyStatusModels.add(getSingleInstance(tmpStandardCompanyStatusModel));
			}
		}
		return singleStandardCompanyStatusModels;
	}

	private boolean isStandardCompanyStatusExist(int stdId, int cmpId) {
		return standardCompanyStatusRepository.isStandardCompanyStatusExist(stdId, cmpId) != null;
	}

	public List<StandardCompanyStatusModel> getAllByStandardId(int stdId) {
		return standardCompanyStatusRepository.getStandardCompanyStatusByStdId(stdId);
	}
}
