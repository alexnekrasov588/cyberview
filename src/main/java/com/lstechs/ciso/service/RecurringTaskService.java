package com.lstechs.ciso.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lstechs.ciso.model.RecurringTaskModel;
import com.lstechs.ciso.repository.RecurringTaskRepository;

@Service
public class RecurringTaskService extends CRUDService<RecurringTaskModel,RecurringTaskModel> {

	@Autowired
	private RecurringTaskRepository recurringTaskRepository;
	
	private static Logger LOGGER = LogManager.getLogger(RecurringTaskService.class.getName());
	
	public RecurringTaskService(RecurringTaskRepository recurringTaskRepository) {
		super(recurringTaskRepository);
		// TODO Auto-generated constructor stub
	}
	

	@Override
	public RecurringTaskModel update(int id, RecurringTaskModel t) throws Exception {
		LOGGER.info("update recurring task id: " + id);
		RecurringTaskModel model = getById(id);
		model.setMonthly(t.isMonthly());
		model.setWeekly(t.isWeekly());
		model.setYearly(t.isYearly());
		model.setNumOfShow(t.getNumOfShow());
		if(t.getNumOfShowUpdated() > 0)
		model.setNumOfShowUpdated(t.getNumOfShowUpdated());
		model.setTskId(t.getTskId());
		return recurringTaskRepository.save(model);		
	}
	
	public RecurringTaskModel findRecurringTaskByTaskId(int tskId) {
		LOGGER.info("find recurring task by task id: " + tskId);
		return recurringTaskRepository.findRecurringTaskByTaskId(tskId);
	}
	
	@Override
	public RecurringTaskModel create(RecurringTaskModel t) throws Exception {
		if(findRecurringTaskByTaskId(t.getTskId().getTskId()) != null)
			throw new Exception("RecurringTask exist for this task");
		return super.create(t);
	}



}
