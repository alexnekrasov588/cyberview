package com.lstechs.ciso.service;

import com.lstechs.ciso.enums.AssetTypeEnum;
import com.lstechs.ciso.enums.CompliantEnum;
import com.lstechs.ciso.model.*;
import com.lstechs.ciso.repository.CustomerStandardRepository;
import com.lstechs.ciso.repository.StdSectionRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class StdSectionService extends CRUDService<StdSectionModel, SingleCompanyUserModel> {

	@Autowired
	public StdSectionRepository stdSectionRepository;

	@Autowired
	public StdQtrQuestionsService stdQtrQuestionsService;

	@Autowired
	public StdRequirementsService stdRequirementsService;

	@Autowired
	public StdQuestionaireService stdQuestionaireService;

	@Autowired
	public CustomerStandardService customerStandardService;

	@Autowired
	public CustomerStandardRepository customerStandardRepository;

	private static final Logger LOGGER = LogManager.getLogger(StdSectionService.class.getName());

	public StdSectionService(StdSectionRepository stdSectionRepository) {
		super(stdSectionRepository);
	}

	@Override
	@Transactional
	public StdSectionModel create(StdSectionModel stdSectionModel) throws Exception {
		if (stdSectionModel.getOrder() == 0) {
			int maxOrder = stdSectionRepository.getMaxOrder(stdSectionModel.getStdQtrId().getStdQtrId()) + 1;
			if (stdSectionModel.getParent() != null)
				maxOrder = Math.max(stdQtrQuestionsService.getMaxOrder(stdSectionModel.getParent().getStdSctId()), maxOrder);
			stdSectionModel.setOrder(maxOrder);
		}
		return super.create(stdSectionModel);
	}

	public int getMaxOrderByParentId(int id) {
		LOGGER.info("get max order by parentId  , id: " + id + ".");
		return stdSectionRepository.getMaxOrderByParentId(id) + 1;
	}


	@Override
	public StdSectionModel update(int id, StdSectionModel stdLevelModel) throws Exception {
		LOGGER.info("update Std Section  , id: " + id + ".");
		StdSectionModel updateStdSectionModel = getById(id);
		updateStdSectionModel.setParent(stdLevelModel.getParent());
		updateStdSectionModel.setStdSctName(stdLevelModel.getStdSctName());
		updateStdSectionModel.setStdSctDescription(stdLevelModel.getStdSctDescription());
		updateStdSectionModel.setStdQtrId(stdLevelModel.getStdQtrId());
		if (stdLevelModel.getOrder() > 0)
			updateStdSectionModel.setOrder(stdLevelModel.getOrder());

		return stdSectionRepository.save(updateStdSectionModel);
	}

	public void updateAllAnswersInSection(int id, String answer, CompanyModel company) throws Exception {
		LOGGER.info("update Std Section  , id: " + id + ".");

		try {
			StdSectionModel updateStdSectionModel = getById(id);

			CompliantEnum compliantEnum = CompliantEnum.valueOf(answer);
			List<StdQtrQuestionsModel> stdQtrQuestionsModelAsList = stdQtrQuestionsService.getAllQuestionBySectionId(updateStdSectionModel.getStdSctId());
			List<CustomerStandardModel> customerStandardModelAsList = new ArrayList<>(Collections.emptyList());

			for (StdQtrQuestionsModel stdQtrQuestionsModel : stdQtrQuestionsModelAsList) {
				List<CustomerStandardModel> customerStandardModelAsListByQuestionId = customerStandardService.getCustomerStandardsByQuestionsId(stdQtrQuestionsModel.getQtrQstId(), company.getCmpId());
				customerStandardModelAsList.addAll(customerStandardModelAsListByQuestionId);
			}

			for (CustomerStandardModel customerStandardModel : customerStandardModelAsList) {
				customerStandardModel.setCompliant(compliantEnum);
				customerStandardRepository.save(customerStandardModel);
			}
		} catch (Exception ex) {
			LOGGER.info(ex.getMessage());
		}
	}

	@Override
	public StdSectionModel delete(int id) throws Exception {
		LOGGER.info("delete Std Section  , id: " + id + ".");
		StdSectionModel stdSectionModel = getById(id);
		stdSectionModel.setDeletedAt(new Date(System.currentTimeMillis()));
		List<StdSectionModel> stdSectionModels = getAllSectionByParentId(id);
		for (StdSectionModel tmpStdSectionModel : stdSectionModels) {
			delete(tmpStdSectionModel.getStdSctId());
		}
		List<StdQtrQuestionsModel> stdQtrQuestionsModels = stdQtrQuestionsService.getAllQuestionBySectionId(id);
		for (StdQtrQuestionsModel tmpStdQtrQuestionsModel : stdQtrQuestionsModels) {
			if (tmpStdQtrQuestionsModel.getStdRqrId() != null && tmpStdQtrQuestionsModel.getStdRqrId().getStdRqrId() > 0) {
				stdRequirementsService.delete(tmpStdQtrQuestionsModel.getStdRqrId().getStdRqrId());
			}
			stdQtrQuestionsService.delete(tmpStdQtrQuestionsModel.getQtrQstId());
		}
		return stdSectionRepository.save(stdSectionModel);
	}

	public List<StdSectionModel> getAllSectionByQuestionaireId(int id) {
		LOGGER.info("get all section by QuestionaireId: " + id + ".");
		return stdSectionRepository.getAllSectionByQuestionaireId(id);
	}


	public StdSectionModel getOnPrem(int id) {
		List<StdSectionModel> results = stdSectionRepository.getOnPrem(id);
		if (results.size() > 0) {
			return results.get(0);
		} else {
			return null;
		}
	}

	public List<StdSectionModel> getAllSectionByStasndardIds(int[] id) {
		LOGGER.info("get all section by standardIds");
		List<StdQuestionaireModel> stdQuestionaireModels = stdQuestionaireService.getQuestionaireListByStdIdsForOnPremise(id);
		if (stdQuestionaireModels != null && stdQuestionaireModels.size() > 0) {
			int i = 0;
			int[] qArr = new int[stdQuestionaireModels.size()];
			for (StdQuestionaireModel tmpStdQuestionaireModel : stdQuestionaireModels) {
				qArr[i] = tmpStdQuestionaireModel.getStdQtrId();
				i++;
			}

			List<Integer> list = Arrays.stream(qArr).boxed().collect(Collectors.toList());
			return stdSectionRepository.getAllSectionByQuestionaireIds(list);
		}
		return null;
	}

	public List<StdSectionModel> getAllSectionByQuestionaireIdWithOutParent(int id) {
		LOGGER.info("get all section by QuestionaireId: " + id + ".");
		return stdSectionRepository.getAllSectionByQuestionaireIdWithOutParent(id);
	}


	public List<StdSectionModel> getAllSectionByParentId(int id) {
		LOGGER.info("get all section by parent id: " + id + ".");
		return stdSectionRepository.getAllSectionByParentId(id);
	}

	public StdSectionModel getSectionDelete(int id) {
		LOGGER.info("get section delete id: " + id + ".");
		return stdSectionRepository.getSectionForDelete(id);
	}

	public boolean isSectionInQuestionnaire(int stdSctId, int stdQtrId) {
		LOGGER.info("return if Section id: " + stdSctId + " exist in Questionnaire id: " + stdQtrId + ".");
		return stdSectionRepository.isSectionInQuestionnaire(stdSctId, stdQtrId);
	}

	/**
	 * This Function duplicates the original questionnaire as was created by Vendor user
	 *
	 * @param originalStdSectionModel originalStdSectionModel
	 * @param duplicatedParentSection duplicatedParentSection
	 * @param newQuestionnaireModel   newQuestionnaireModel
	 * @throws Exception Exception
	 */
	public void duplicateSection(StdSectionModel originalStdSectionModel, StdSectionModel duplicatedParentSection, StdQuestionaireModel newQuestionnaireModel, String token) throws Exception {
		LOGGER.info("Duplicate Section , id: " + originalStdSectionModel.getStdSctId() + ".");
		StdSectionModel tmpSectionModel = new StdSectionModel(originalStdSectionModel);
		tmpSectionModel.setParent(duplicatedParentSection);
		tmpSectionModel.setStdQtrId(newQuestionnaireModel);
		tmpSectionModel.setOrder(originalStdSectionModel.getOrder());
		StdSectionModel newSectionModel = create(tmpSectionModel);
		LOGGER.info("Create new Section , id: " + newSectionModel.getStdSctId() + ".");

		List<StdSectionModel> sectionChildren = getAllSectionByParentId(originalStdSectionModel.getStdSctId());
		if (sectionChildren != null && sectionChildren.size() > 0) {
			for (StdSectionModel tmpStdSectionModel : sectionChildren) {
				duplicateSection(tmpStdSectionModel, newSectionModel, newSectionModel.getStdQtrId(), token);
			}
		}

		List<StdRequirementsModel> requirementsModels = stdQtrQuestionsService.getAllReqBySectionId(originalStdSectionModel.getStdSctId());
		for (StdRequirementsModel tmpRequirementsModel : requirementsModels) {
			stdRequirementsService.duplicateRequirement(tmpRequirementsModel, newSectionModel, newQuestionnaireModel.getStdId(), token);
		}
		LOGGER.info("Duplicate Section , id: " + originalStdSectionModel.getStdSctId() + " success.");
	}

	/**
	 * This Function duplicates the questionnaire with Customer Standard changes.
	 *
	 * @param originalStdSectionModel originalStdSectionModel
	 * @param duplicatedParentSection duplicatedParentSection
	 * @param newQuestionnaireModel   newQuestionnaireModel
	 * @throws Exception Exception
	 */
	public void duplicateSectionWithCustomerStandards(StdSectionModel originalStdSectionModel, StdSectionModel duplicatedParentSection, StdQuestionaireModel newQuestionnaireModel) throws Exception {
		LOGGER.info("Duplicate Section , id: " + originalStdSectionModel.getStdSctId() + ".");
		StdSectionModel tmpSectionModel = new StdSectionModel(originalStdSectionModel);
		tmpSectionModel.setParent(duplicatedParentSection);
		tmpSectionModel.setStdQtrId(newQuestionnaireModel);
		tmpSectionModel.setOrder(originalStdSectionModel.getOrder());
		StdSectionModel newSectionModel = create(tmpSectionModel);
		LOGGER.info("Create new Section , id: " + newSectionModel.getStdSctId() + ".");

		List<StdSectionModel> sectionChildren = getAllSectionByParentId(originalStdSectionModel.getStdSctId());
		if (sectionChildren != null && sectionChildren.size() > 0) {
			for (StdSectionModel tmpStdSectionModel : sectionChildren) {
				duplicateSectionWithCustomerStandards(tmpStdSectionModel, newSectionModel, newSectionModel.getStdQtrId());
			}
		}
		List<StdRequirementsModel> requirementsModels = stdQtrQuestionsService.getAllReqBySectionId(originalStdSectionModel.getStdSctId());
		for (StdRequirementsModel tmpRequirementsModel : requirementsModels) {
			stdRequirementsService.duplicateRequirementWithCustomerStandard(tmpRequirementsModel, newSectionModel, newQuestionnaireModel.getStdId());
		}
		LOGGER.info("Duplicate Section , id: " + originalStdSectionModel.getStdSctId() + " success.");
	}

	public StdSectionModel getStdSectionModelByStdQtrQuestionsId(int stdQtrQstId) {
		LOGGER.info("get section by  QtrQuestions id: " + stdQtrQstId + ".");
		return stdSectionRepository.getStdSectionModelByStdQtrQuestionsId(stdQtrQstId);
	}

	public List<StdSectionModel> saveSectionsFromOnPrem(List<StdSectionModel> arr) {
		List<StdSectionModel> retList = new ArrayList<>();
		LOGGER.info("get sync list from crud service, Repository: " + crudRepository.getClass().getSimpleName());

		for (StdSectionModel t : arr) {
			try {
				t.setStdSctId(0);
				stdSectionRepository.save(t);
				retList.add(t);

			} catch (Exception ex) {
				LOGGER.error("error cannot create row", ex);
			}
		}

		return retList;
	}


}
