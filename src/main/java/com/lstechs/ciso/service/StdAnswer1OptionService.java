package com.lstechs.ciso.service;

import com.lstechs.ciso.model.SingleCompanyUserModel;
import com.lstechs.ciso.model.StdAnswer1OptionModel;
import com.lstechs.ciso.model.StdQtrQuestionsModel;
import com.lstechs.ciso.repository.StdAnswer1OptionRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class StdAnswer1OptionService extends CRUDService<StdAnswer1OptionModel, SingleCompanyUserModel> {

	public StdAnswer1OptionService(StdAnswer1OptionRepository stdAnswer1OptionRepository) {
		super(stdAnswer1OptionRepository);
	}

	private static final Logger LOGGER = LogManager.getLogger(StdAnswer1OptionService.class.getName());

	@Autowired
	private StdAnswer1OptionRepository stdAnswer1OptionRepository;

	@Autowired
	private StdQtrQuestionsService stdQtrQuestionsService;

	public void deleteStdAnswer1OptionByQtsId(int id) {
		LOGGER.info("delete StdAnswer1Option by Qts id: " + id);
		stdAnswer1OptionRepository.deleteStdAnswer1OptionModel(id);
	}

	public List<StdAnswer1OptionModel> getStdAnswer1OptionModelByStandardIds(int[] ids) {
		List<StdQtrQuestionsModel> stdQtrQuestionsModels = stdQtrQuestionsService.getAllQuestionByStandardIds(ids);
		if (stdQtrQuestionsModels != null && stdQtrQuestionsModels.size() > 0) {
			int i = 0;
			int[] qIds = new int[stdQtrQuestionsModels.size()];
			for (StdQtrQuestionsModel tmpStdQtrQuestionsModel : stdQtrQuestionsModels) {
				qIds[i] = tmpStdQtrQuestionsModel.getQtrQstId();
				i++;
			}

			List<Integer> list = Arrays.stream(qIds).boxed().collect(Collectors.toList());
			  return stdAnswer1OptionRepository.getOptionsListByQuestionsIds(list);
		}
		return null;
	}
}
