package com.lstechs.ciso.service;

import com.lstechs.ciso.model.CompanyUserModel;
import com.lstechs.ciso.model.SingleCompanyUserModel;
import com.lstechs.ciso.model.TaskCommentModel;
import com.lstechs.ciso.model.TaskModel;
import com.lstechs.ciso.repository.TaskCommentRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class TaskCommentService extends CRUDService<TaskCommentModel, SingleCompanyUserModel> {

	@Autowired
	private TaskCommentRepository taskCommentRepository;

	@Autowired
	private TaskService taskService;

	@Autowired
	private CompanyUserService companyUserService;

	private static Logger LOGGER = LogManager.getLogger(TaskCommentService.class.getName());

	public TaskCommentService(TaskCommentRepository taskCommentRepository) {
		super(taskCommentRepository);
	}

	@Override
	public TaskCommentModel update(int id, TaskCommentModel taskCommentModel) throws Exception {
		LOGGER.info("update task comment id: " + id);
		TaskCommentModel commentModel = getById(id);
		commentModel.setCmntText(taskCommentModel.getCmntText());
		TaskModel taskModel = taskService.getById(commentModel.getCmntTskId().getTskId());
		CompanyUserModel companyUserModel = companyUserService.getById(commentModel.getUsrId().getCmpUsrId());
		commentModel.setCmntTskId(taskModel);
		commentModel.setUsrId(companyUserModel);
		return taskCommentRepository.save(commentModel);
	}

	public Page<TaskCommentModel> getAllTasksComment(Integer pageNo, Integer pageSize, String orderBy, boolean orderByDesc, String query, int taskId) throws Exception {
		LOGGER.info("get all Tasks comment with task id: " + taskId);
		Sort sort = Sort.by(Sort.Order.desc(orderBy));
		if(!orderByDesc){
			sort = Sort.by(Sort.Order.asc(orderBy));
		}
		Pageable paging = PageRequest.of(pageNo, pageSize, sort);
		Page<TaskCommentModel> pagedResult = taskCommentRepository.findTasksComment(paging, taskId, query.toLowerCase());
		return pagedResult;
	}
}
