package com.lstechs.ciso.service;

import com.lstechs.ciso.enums.FavoriteActionEnum;
import com.lstechs.ciso.model.CVEModel;
import com.lstechs.ciso.model.CompanyUserModel;
import com.lstechs.ciso.model.FavouriteCVEModel;
import com.lstechs.ciso.model.FavouriteFoldersModel;
import com.lstechs.ciso.model.FavouriteNewsModel;
import com.lstechs.ciso.model.NewsArticlesModel;
import com.lstechs.ciso.model.SingleCompanyUserModel;
import com.lstechs.ciso.repository.FavouriteNewsRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FavouriteNewsService extends CRUDService<FavouriteNewsModel, SingleCompanyUserModel> {

    @Autowired
    private FavouriteNewsRepository favouriteNewsRepository;

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private NewsArticlesService newsArticlesService;
    
	@Autowired
	private FavouriteFoldersService favouriteFoldersService;

    private static Logger LOGGER = LogManager.getLogger(FavouriteNewsService.class.getName());

    public FavouriteNewsService(FavouriteNewsRepository favouriteNewsRepository) {
        super(favouriteNewsRepository);
    }

    
	public FavouriteNewsModel favorite(int newsArtcId, String token, String fvrtNewsName, int folderId, String action)	throws Exception {
		LOGGER.info("sign favorite news id: " + newsArtcId);
		CompanyUserModel companyUserModel = authenticationService.getCompanyUserFromToken(token.substring(7));
		NewsArticlesModel newsArticlesModel = newsArticlesService.getById(newsArtcId);

		FavoriteActionEnum actionEnum = FavoriteActionEnum.valueOf(action);
	
		FavouriteFoldersModel folder = null;
		if (folderId > 0)
			folder = favouriteFoldersService.getById(folderId);

		switch (actionEnum) {
		case CREATE:
			FavouriteNewsModel newFavouriteNewsModel = new FavouriteNewsModel(newsArticlesModel, companyUserModel, fvrtNewsName,folder);
			return create(newFavouriteNewsModel);
		case UPDATE:
			FavouriteNewsModel favouriteNewsModel = favouriteNewsRepository.getFavouriteNews(newsArtcId,companyUserModel.getCmpUsrId());
			if(favouriteNewsModel != null) {
				favouriteNewsModel.setFvrtNewsName(fvrtNewsName);
				favouriteNewsModel.setFolder(folder);
				return update(favouriteNewsModel.getFvrtNewsId(),favouriteNewsModel);
			}		
			break;
		case DELETE:
			FavouriteNewsModel deletedFavouriteNews = favouriteNewsRepository.getFavouriteNews(newsArtcId,companyUserModel.getCmpUsrId());
			if (deletedFavouriteNews != null)
				return delete(deletedFavouriteNews.getFvrtNewsId());
			break;
		}
		
		return null;
	}
    
    
    
    

    public List<FavouriteNewsModel> getFavoriteByUserId(String token) {
        LOGGER.info("get favorite by userId");
        CompanyUserModel companyUserModel = authenticationService.getCompanyUserFromToken(token.substring(7));
        return favouriteNewsRepository.getNewsArticlesModelByCmpUsrId(companyUserModel.getCmpUsrId());
    }

	public Page<NewsArticlesModel> getFavoriteByUserIdWithPaging(Integer pageNo, Integer pageSize, String orderBy,boolean orderByDesc,String token) {
		LOGGER.info("get favorite by userId with paging");
		CompanyUserModel companyUserModel = authenticationService.getCompanyUserFromToken(token.substring(7));
		Sort sort = Sort.by(Sort.Order.desc(orderBy));
		if(!orderByDesc){
			sort = Sort.by(Sort.Order.asc(orderBy));
		}
		Pageable paging = PageRequest.of(pageNo, pageSize, sort);
		Page<NewsArticlesModel> pagedResult = favouriteNewsRepository.getNewsArticlesModelByCmpUsrIdWithPaging(paging,companyUserModel.getCmpUsrId());
		return pagedResult;
	}
}
