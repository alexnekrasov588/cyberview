package com.lstechs.ciso.service;

import com.lstechs.ciso.model.ResetTokensModel;
import com.lstechs.ciso.repository.ResetTokensRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@EnableScheduling
public class ResetTokensService {

    @Autowired
    private ResetTokensRepository resetTokensRepository;

    private static final Logger LOGGER = LogManager.getLogger(ResetTokensService.class.getName());


    public ResetTokensModel create(ResetTokensModel resetTokensModel) {
        LOGGER.info("create reset tokens.");
        deleteDuplicate(resetTokensModel.getUserId(), resetTokensModel.getUserType());
        return resetTokensRepository.save(resetTokensModel);
    }

    public void delete(ResetTokensModel resetTokensModel) {
        LOGGER.info("delete reset tokens id: " + resetTokensModel.getId());
        resetTokensRepository.delete(resetTokensModel);
    }

    public void deleteDuplicate(int userId, String userType) {
        LOGGER.info("delete duplicate " + userType + ", user id: " + userId);
        resetTokensRepository.deleteDuplicate(userId, userType);
    }

    public ResetTokensModel getResetTokensByResetToken(String resetToken) {
        LOGGER.info("get reset tokens by reset token: " + resetToken);
        return resetTokensRepository.findByResetToken(resetToken);
    }


    @Async
    @Scheduled(cron = "${cron}")
    public void deleteExpiresTokens() {
        LOGGER.info("delete expires tokens.");
        resetTokensRepository.deleteExpires(new Date(System.currentTimeMillis()));
    }

}
