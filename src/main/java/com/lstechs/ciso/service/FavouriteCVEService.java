package com.lstechs.ciso.service;

import com.lstechs.ciso.enums.FavoriteActionEnum;
import com.lstechs.ciso.model.CVEModel;
import com.lstechs.ciso.model.CompanyUserModel;
import com.lstechs.ciso.model.FavouriteCVEModel;
import com.lstechs.ciso.model.FavouriteFoldersModel;
import com.lstechs.ciso.model.SingleCompanyUserModel;
import com.lstechs.ciso.repository.FavouriteCVERepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FavouriteCVEService extends CRUDService<FavouriteCVEModel, SingleCompanyUserModel> {

	@Autowired
	private FavouriteCVERepository favouriteCVERepository;

	@Autowired
	AuthenticationService authenticationService;

	@Autowired
	CVEService cveService;

	@Autowired
	private FavouriteFoldersService favouriteFoldersService;

	private static Logger LOGGER = LogManager.getLogger(FavouriteCVEService.class.getName());

	public FavouriteCVEService(FavouriteCVERepository favouriteCVERepository) {
		super(favouriteCVERepository);
	}

	public FavouriteCVEModel favorite(int cveId, String token, String fvrtCveName, int folderId, String action)	throws Exception {
		LOGGER.info("sign favorite cve id: " + cveId);
		CompanyUserModel companyUserModel = authenticationService.getCompanyUserFromToken(token.substring(7));
		CVEModel cveModel = cveService.getById(cveId);

		FavoriteActionEnum actionEnum = FavoriteActionEnum.valueOf(action);
	
		FavouriteFoldersModel folder = null;
		if (folderId > 0)
			folder = favouriteFoldersService.getById(folderId);

		switch (actionEnum) {
		case CREATE:
			FavouriteCVEModel newFavouriteCVEModel = new FavouriteCVEModel(cveModel, companyUserModel, fvrtCveName,	folder);
			return create(newFavouriteCVEModel);
		case UPDATE:
			FavouriteCVEModel favouriteCVEModel = favouriteCVERepository.getFavouriteCVES(cveId,companyUserModel.getCmpUsrId());
			if(favouriteCVEModel != null) {
				favouriteCVEModel.setFvrtCveName(fvrtCveName);
				favouriteCVEModel.setFolder(folder);
				return update(favouriteCVEModel.getFvrtCveId(),favouriteCVEModel);
			}		
			break;
		case DELETE:
			FavouriteCVEModel deletedFavouriteCVE = favouriteCVERepository.getFavouriteCVES(cveId,companyUserModel.getCmpUsrId());
			if (deletedFavouriteCVE != null)
				return delete(deletedFavouriteCVE.getFvrtCveId());
			break;
		}
		
		return null;
	}

	public List<FavouriteCVEModel> getFavoriteByUserId(String token) {
		LOGGER.info("get favorite by userId");
		CompanyUserModel companyUserModel = authenticationService.getCompanyUserFromToken(token.substring(7));
		return favouriteCVERepository.getFavouriteCVEByCmpUserId(companyUserModel.getCmpUsrId());
	}
}
