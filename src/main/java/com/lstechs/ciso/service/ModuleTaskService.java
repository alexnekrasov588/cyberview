package com.lstechs.ciso.service;

import com.lstechs.ciso.enums.ModuleEnum;
import com.lstechs.ciso.model.ModuleTaskModel;
import com.lstechs.ciso.model.SingleModuleTaskModel;
import com.lstechs.ciso.model.TaskModel;
import com.lstechs.ciso.repository.ModuleTaskRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ModuleTaskService extends CRUDService<ModuleTaskModel, SingleModuleTaskModel> {

	public ModuleTaskService(ModuleTaskRepository moduleTaskRepository) {
		super(moduleTaskRepository);
	}

	private static Logger LOGGER = LogManager.getLogger(ModuleTaskService.class.getName());

	@Autowired
	private ModuleTaskRepository moduleTaskRepository;

	@Autowired
	private TaskService taskService;

	public ModuleTaskModel getModuleTaskByModuleAndIdAndCmpId(ModuleEnum moduleEnum, int id, int cmpId) {
		LOGGER.info("Get module task by module: " + moduleEnum.toString() + " and id: " + id + ", of company id:" + cmpId);
		return moduleTaskRepository.findModuleTaskByModuleAndIdAndCmpId(moduleEnum, id, cmpId);
	}


	public ModuleTaskModel getModuleTaskByTaskId(int id) {
		LOGGER.info("Get module task by task id: " + id);
		return moduleTaskRepository.findModuleTaskByTaskId(id);
	}

	@Override
	public ModuleTaskModel delete(int id) throws Exception {
		try {
			LOGGER.info("Delete module task by module id: " + id);
			moduleTaskRepository.deleteModuleTaskModelByMdlTskId(id);
		} catch (Exception ex) {
			LOGGER.error("Delete module task by module id: " + id + " field. " + ex.getMessage());
			throw new Exception("Delete module task by module id:" + id + " field. ");
		}
		return null;
	}

	@Override
	public SingleModuleTaskModel getSingleInstance(ModuleTaskModel moduleTaskModel) throws Exception {
		TaskModel taskModel = taskService.getById(moduleTaskModel.getTskId().getTskId());
		SingleModuleTaskModel singleModuleTaskModel = new SingleModuleTaskModel(moduleTaskModel, taskModel);
		return singleModuleTaskModel;
	}
}
