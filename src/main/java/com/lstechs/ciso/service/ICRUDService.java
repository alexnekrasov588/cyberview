package com.lstechs.ciso.service;

import com.lstechs.ciso.enums.ModuleEnum;
import com.lstechs.ciso.enums.OperationEnum;
import org.springframework.data.domain.Page;

import java.util.Date;
import java.util.List;

public interface ICRUDService<T,P> {

	List<T> getAll();

	Page<T> getAll(Integer pageNo, Integer pageSize, String orderBy,boolean orderByDesc, String query) throws Exception;

	T getById(int id) throws Exception;

	T create(T t) throws Exception;

	T update(int id, T t) throws Exception;

	T delete(int id) throws Exception;

	List<T> getSyncList(Date updatedAt);

	Date lastUpdateAt();

	List<T> setSyncList(T[] arr);

	void handlePermission(String token, OperationEnum operationEnum, ModuleEnum moduleEnum);

	P getSingleInstance(T t) throws Exception;
}
