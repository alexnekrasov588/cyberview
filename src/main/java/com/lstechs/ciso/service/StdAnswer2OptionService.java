package com.lstechs.ciso.service;

import com.lstechs.ciso.model.SingleCompanyUserModel;
import com.lstechs.ciso.model.StdAnswer2OptionModel;
import com.lstechs.ciso.model.StdQtrQuestionsModel;
import com.lstechs.ciso.repository.StdAnswer2OptionRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class StdAnswer2OptionService extends CRUDService<StdAnswer2OptionModel, SingleCompanyUserModel>{

	public StdAnswer2OptionService(StdAnswer2OptionRepository stdAnswer2OptionRepository) {
		super(stdAnswer2OptionRepository);
	}

	private static final Logger LOGGER = LogManager.getLogger(StdAnswer2OptionService.class.getName());

	@Autowired
	private StdAnswer2OptionRepository stdAnswer2OptionRepository;

	@Autowired
	private StdQtrQuestionsService stdQtrQuestionsService;

	public void deleteStdAnswer2OptionByQtsId(int id) {
		LOGGER.info("delete StdAnswer2Option by Qts id: " + id);
		stdAnswer2OptionRepository.deleteStdAnswer2OptionModel(id);
	}

	public List<StdAnswer2OptionModel> getStdAnswer2OptionModelByStandardIds(int[] ids) {
		List<StdQtrQuestionsModel> stdQtrQuestionsModels = stdQtrQuestionsService.getAllQuestionByStandardIds(ids);
		if (stdQtrQuestionsModels != null && stdQtrQuestionsModels.size() > 0) {
			int i = 0;
			int[] qIds = new int[stdQtrQuestionsModels.size()];
			for (StdQtrQuestionsModel tmpStdQtrQuestionsModel : stdQtrQuestionsModels) {
				qIds[i] = tmpStdQtrQuestionsModel.getQtrQstId();
				i++;
			}

			List<Integer> list = Arrays.stream(qIds).boxed().collect(Collectors.toList());
			return stdAnswer2OptionRepository.getOptionsListByQuestionsIds(list);
		}
		return null;
	}
}
