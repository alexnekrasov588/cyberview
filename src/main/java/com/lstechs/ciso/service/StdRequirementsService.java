package com.lstechs.ciso.service;

import com.lstechs.ciso.enums.UsersEnum;
import com.lstechs.ciso.model.*;
import com.lstechs.ciso.repository.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class StdRequirementsService extends CRUDService<StdRequirementsModel, SingleStdRequirementsModel> {

	@Autowired
	private StdRequirementsRepository stdRequirementsRepository;

	@Autowired
	private StdQtrQuestionsService stdQtrQuestionsService;

	@Autowired
	private StdSectionService stdSectionService;

	@Autowired
	private StdQuestionaireService stdQuestionaireService;

	@Autowired
	private CustomerStandardService customerStandardService;

	@Autowired
	private CrossReferenceService crossReferenceService;

	@Autowired
	CstQstFileAnswerRepository cstQstFileAnswerRepository;

	@Autowired
	StdQstOptionRepository stdQstOptionRepository;

	@Autowired
	StdAnswer1OptionRepository stdAnswer1OptionRepository;

	@Autowired
	AuthenticationService authenticationService;

	@Autowired
	StdAnswer2OptionRepository stdAnswer2OptionRepository;

	@Autowired
	StdSectionRepository stdSectionRepository;

	private static final Logger LOGGER = LogManager.getLogger(StdRequirementsService.class.getName());

	public StdRequirementsService(StdRequirementsRepository stdRequirementsRepository) {
		super(stdRequirementsRepository);
	}

	@Override
	public SingleStdRequirementsModel getSingleInstance(StdRequirementsModel stdRequirementsModel) throws Exception {
		return new SingleStdRequirementsModel(stdRequirementsModel, getStdQtrNameByReqId(stdRequirementsModel.getStdRqrId()));
	}

	@Override
	public StdRequirementsModel delete(int id) throws Exception {
		crossReferenceService.deleteByRqrId(id);
		return super.delete(id);
	}

	@Override
	public StdRequirementsModel create(StdRequirementsModel stdRequirementsModel) throws Exception {
		return super.create(stdRequirementsModel);
	}

	public void updateOrderByQstId(int id, int order) throws Exception {
		StdRequirementsModel stdRequirementsModel = getById(id);
		stdRequirementsModel.setOrder(order);
		stdRequirementsRepository.save(stdRequirementsModel);
	}

	@Override
	public StdRequirementsModel update(int id, StdRequirementsModel stdLevelModel) throws Exception {
		LOGGER.info("update Std Requirements  , id: " + id + ".");
		StdRequirementsModel updateStdRequirementsModel = getById(id);
		updateStdRequirementsModel.setArticleNum(stdLevelModel.getArticleNum());
		updateStdRequirementsModel.setStdId(stdLevelModel.getStdId());
		updateStdRequirementsModel.setStdRqrDescription(stdLevelModel.getStdRqrDescription());
		updateStdRequirementsModel.setStdRqrExplanation(stdLevelModel.getStdRqrExplanation());
		updateStdRequirementsModel.setStdRqrIsMandatory(stdLevelModel.isStdRqrIsMandatory());
		updateStdRequirementsModel.setStdRqrName(stdLevelModel.getStdRqrName());
		updateStdRequirementsModel.setStdRqrTitle(stdLevelModel.getStdRqrTitle());
		updateStdRequirementsModel.setFileFreeText(stdLevelModel.getFileFreeText());
		if (updateStdRequirementsModel.getOrder() > 0)
			updateStdRequirementsModel.setOrder(stdLevelModel.getOrder());
		return stdRequirementsRepository.save(updateStdRequirementsModel);
	}


	public List<StdRequirementsModel> getRequirementsListByTemplateId(int id) {
		LOGGER.info("get requirements list by template id: " + id + ".");
		return stdRequirementsRepository.getReqListByTemplateId(id);
	}


	public StdRequirementsModel getOnPRem(int id) {
		List<StdRequirementsModel> results = stdRequirementsRepository.getOnPrem(id);
		if (results.size() > 0) {
			return results.get(0);
		} else {
			return null;
		}
	}

	public List<StdRequirementsModel> getRequirementsListByTemplateIds(int[] ids) {
		LOGGER.info("get requirements list by template ids");

		List<StdSectionModel> sections = Collections.emptyList();
		List<StdRequirementsModel> stdRequirementsModelList;
		List<StdRequirementsModel> newStdRequirementsModelList = new java.util.ArrayList<>(Collections.emptyList());

		List<StdQuestionaireModel> stdQuestionaireModels = stdQuestionaireService.getQuestionaireListByStdIdsForOnPremise(ids);
		if (stdQuestionaireModels != null && stdQuestionaireModels.size() > 0) {
			int i = 0;
			int[] qArr = new int[stdQuestionaireModels.size()];
			for (StdQuestionaireModel tmpStdQuestionaireModel : stdQuestionaireModels) {
				qArr[i] = tmpStdQuestionaireModel.getStdQtrId();
				i++;
			}

			List<Integer> list = Arrays.stream(qArr).boxed().collect(Collectors.toList());

			sections = stdSectionRepository.getAllSectionByQuestionaireIds(list);
		}

		for (StdSectionModel section : sections) {
			stdRequirementsModelList = stdQtrQuestionsService.getAllReqBySectionId(section.getStdSctId());

			newStdRequirementsModelList.addAll(stdRequirementsModelList);
		}
		return newStdRequirementsModelList;
	}

	public int getReqListCountByTemplateId(int id) {
		LOGGER.info("get requirements list by template id: " + id + ".");
		return stdRequirementsRepository.getReqListCountByTemplateId(id);
	}

	public int getMaxOrderByStdId(int stdId) {
		return stdRequirementsRepository.getMaxOrderByStdId(stdId);
	}


	public StdRequirementsModel duplicateRequirement(StdRequirementsModel originalStdRequirementsModel, StdSectionModel stdSectionModel, StandardModel standardModel, String token) throws Exception {
		LOGGER.info("Duplicate Requirement , id: " + originalStdRequirementsModel.getStdRqrId() + ".");
		StdRequirementsModel stdRequirementsModel = getById(originalStdRequirementsModel.getStdRqrId());
		stdRequirementsModel.setStdId(standardModel);
		stdRequirementsModel.setOrder(originalStdRequirementsModel.getOrder());

//		stdRequirementsModel.setStdQstOptionModels(originalStdRequirementsModel.getStdQstOptionModels());
//		stdRequirementsModel.setStdAnswer1OptionModels(originalStdRequirementsModel.getStdAnswer1OptionModels());
//		stdRequirementsModel.setStdAnswer2OptionModels(originalStdRequirementsModel.getStdAnswer2OptionModels());


		StdRequirementsModel newRequirementsModel = create(new StdRequirementsModel(stdRequirementsModel));
		LOGGER.info("Create new Requirement , id: " + newRequirementsModel.getStdRqrId() + ".");

		StdQtrQuestionsModel originalStdQtrQuestionsModel = stdQtrQuestionsService.getQuestionsByRequirementId(originalStdRequirementsModel.getStdRqrId());
		StdQtrQuestionsModel newStdQtrQuestionsModel = new StdQtrQuestionsModel(originalStdQtrQuestionsModel);
		newStdQtrQuestionsModel.setStdRqrId(newRequirementsModel);
		newStdQtrQuestionsModel.setStdRequirements(newRequirementsModel);
		if (stdSectionModel == null) { // duplicate only requirement
			newStdQtrQuestionsModel.setStdSctId(originalStdQtrQuestionsModel.getStdSctId());
		} else { // come from duplicate section
			newStdQtrQuestionsModel.setStdSctId(stdSectionModel);
		}

		newStdQtrQuestionsModel.setOrder(originalStdQtrQuestionsModel.getOrder());
		newStdQtrQuestionsModel.setAnswerDate1Type(originalStdQtrQuestionsModel.getAnswerDate1Type());
		newStdQtrQuestionsModel.setAnswerDate2Type(originalStdQtrQuestionsModel.getAnswerDate2Type());

		StdQtrQuestionsModel questionsModel = stdQtrQuestionsService.create(newStdQtrQuestionsModel);

		//Duplicate Options Handling.
		//Main Options Duplicate handling
		List<StdQstOptionModel> stdQstOptionModelList = originalStdQtrQuestionsModel.getStdQstOptionModel();
		List<StdQstOptionModel> newStdQstOptionModelList = new java.util.ArrayList<>(Collections.emptyList());

		for (StdQstOptionModel stdQstOptionModel : stdQstOptionModelList) {

			StdQstOptionModel newStdQstOptionModel = new StdQstOptionModel();
			newStdQstOptionModel.setSqoQstId(questionsModel);
			newStdQstOptionModel.setStdOptOnPremId(stdQstOptionModel.getStdOptOnPremId());
			newStdQstOptionModel.setSqoLabel(stdQstOptionModel.getSqoLabel());
			newStdQstOptionModel.setSqoValue(stdQstOptionModel.getSqoValue());

			try {
				stdQstOptionRepository.save(newStdQstOptionModel);
				newStdQstOptionModelList.add(newStdQstOptionModel);
			} catch (Exception ex) {
				LOGGER.error(ex.getMessage());
			}
		}

		// Options 1 Duplicate Handling
		List<StdAnswer1OptionModel> stdAnswer1OptionModelList = originalStdQtrQuestionsModel.getStdAnswer1OptionModel();
		List<StdAnswer1OptionModel> newStdAnswer1OptionModelList = new java.util.ArrayList<>(Collections.emptyList());

		for (StdAnswer1OptionModel stdAnswer1OptionModel : stdAnswer1OptionModelList) {

			StdAnswer1OptionModel newStdAnswer1OptionModel = new StdAnswer1OptionModel();
			newStdAnswer1OptionModel.setSqoQstId(questionsModel);
			newStdAnswer1OptionModel.setStdOpt1OnPremId(stdAnswer1OptionModel.getStdOpt1OnPremId());
			newStdAnswer1OptionModel.setsA1Value(stdAnswer1OptionModel.getsA1Value());
			newStdAnswer1OptionModel.setsA1Label(stdAnswer1OptionModel.getsA1Label());

			try {
				stdAnswer1OptionRepository.save(newStdAnswer1OptionModel);
				newStdAnswer1OptionModelList.add(newStdAnswer1OptionModel);
			} catch (Exception ex) {
				LOGGER.error(ex.getMessage());
			}
		}

		// Options 2 Duplicate Handling
		List<StdAnswer2OptionModel> stdAnswer2OptionModelList = originalStdQtrQuestionsModel.getStdAnswer2OptionModel();
		List<StdAnswer2OptionModel> newStdAnswer2OptionModelList = new java.util.ArrayList<>(Collections.emptyList());

		for (StdAnswer2OptionModel stdAnswer2OptionModel : stdAnswer2OptionModelList) {

			StdAnswer2OptionModel newStdAnswer2OptionModel = new StdAnswer2OptionModel();
			newStdAnswer2OptionModel.setSqoQstId(questionsModel);
			newStdAnswer2OptionModel.setStdOpt2OnPremId(stdAnswer2OptionModel.getStdOpt2OnPremId());
			newStdAnswer2OptionModel.setsA2Value(stdAnswer2OptionModel.getsA2Value());
			newStdAnswer2OptionModel.setsA2Label(stdAnswer2OptionModel.getsA2Label());

			try {
				stdAnswer2OptionRepository.save(newStdAnswer2OptionModel);
				newStdAnswer2OptionModelList.add(newStdAnswer2OptionModel);
			} catch (Exception ex) {
				LOGGER.error(ex.getMessage());
			}
		}

		//Setting StdQtrQuestions Options
		newStdQtrQuestionsModel.setStdQstOptionModel(newStdQstOptionModelList);
		newStdQtrQuestionsModel.setStdAnswer1OptionModel(newStdAnswer1OptionModelList);
		newStdQtrQuestionsModel.setStdAnswer2OptionModel(newStdAnswer2OptionModelList);

		UsersEnum usersEnum = authenticationService.getUserType(token.substring(7));

		if (usersEnum.equals(UsersEnum.CompanyUser)) {
			CompanyUserModel companyUserModel = authenticationService.getCompanyUserFromToken(token.substring(7));
			CompanyModel companyModel = companyUserModel.getCmpUsrCmpId();
			customerStandardService.createCustomerStandard(newRequirementsModel, questionsModel, standardModel, companyModel);
		}

		LOGGER.info("Create new Question , id: " + questionsModel.getQtrQstId() + ".");
		LOGGER.info("Duplicate Requirement , id: " + originalStdRequirementsModel.getStdRqrId() + " success.");

		return newRequirementsModel;
	}

	public StdRequirementsModel duplicateRequirementWithCustomerStandard(StdRequirementsModel originalStdRequirementsModel, StdSectionModel stdSectionModel, StandardModel standardModel) throws Exception {
		LOGGER.info("Duplicate Requirement , id: " + originalStdRequirementsModel.getStdRqrId() + ".");
		StdRequirementsModel stdRequirementsModel = getById(originalStdRequirementsModel.getStdRqrId());
		stdRequirementsModel.setStdId(standardModel);
		stdRequirementsModel.setOrder(originalStdRequirementsModel.getOrder());

		/*
           Creates New Requirement
		 */
		StdRequirementsModel newRequirementsModel = create(new StdRequirementsModel(stdRequirementsModel));
		LOGGER.info("Create new Requirement , id: " + newRequirementsModel.getStdRqrId() + ".");

		/*
           Duplicates Customer Standard
		 */
		List<CustomerStandardModel> cs = originalStdRequirementsModel.getCustomerStandardModel();
		for (CustomerStandardModel c : cs) {
			CustomerStandardModel newCustomerStandardModel = new CustomerStandardModel(c);
			newCustomerStandardModel.setStdRqrId(newRequirementsModel);
			newCustomerStandardModel.setQtrQstId(c.getQtrQstId());
			newCustomerStandardModel.setStdId(c.getStdId());
			newCustomerStandardModel.setAnswerDate1(c.getAnswerDate1());
			newCustomerStandardModel.setAnswerDate2(c.getAnswerDate2());

			CustomerStandardModel newCustomerStandard = customerStandardService.create(newCustomerStandardModel);

			/*
			Extracting Customer Standard file names and URL's
			And setting HashMap with the values
			*/
			List<CstQstFileAnswerModel> cstQstFileAnswerModels = cstQstFileAnswerRepository.getCstQstFileAnswerByCustomerStandardId(c);

			HashMap<String, String> cstQstFileAnswer = new HashMap<>();
			for (CstQstFileAnswerModel cstQstFileAnswerModel : cstQstFileAnswerModels) {

				CstQstFileAnswerModel cstQstFile = new CstQstFileAnswerModel();
				cstQstFile.setFileName(cstQstFileAnswerModel.getFileName());
				cstQstFile.setFileUrl(cstQstFileAnswerModel.getFileUrl());

				cstQstFileAnswer.put(cstQstFile.getFileUrl(), cstQstFile.getFileName());
				cstQstFile.setCustomerStandardId(newCustomerStandard);
				cstQstFileAnswerRepository.save(cstQstFile);
			}
			newCustomerStandardModel.setCstQstFileAnswer(cstQstFileAnswer);
			LOGGER.info("Create new Customer Standard , id: " + newCustomerStandard.getCstStdId() + ".");
		}

		/*
           Duplicates QtrQuestions
		 */
		StdQtrQuestionsModel originalStdQtrQuestionsModel = stdQtrQuestionsService.getQuestionsByRequirementId(originalStdRequirementsModel.getStdRqrId());
		StdQtrQuestionsModel newStdQtrQuestionsModel = new StdQtrQuestionsModel(originalStdQtrQuestionsModel);
		newStdQtrQuestionsModel.setStdRqrId(newRequirementsModel);
		newStdQtrQuestionsModel.setStdRequirements(newRequirementsModel);
		if (stdSectionModel == null) { // duplicate only requirement
			newStdQtrQuestionsModel.setStdSctId(originalStdQtrQuestionsModel.getStdSctId());
		} else { // come from duplicate section
			newStdQtrQuestionsModel.setStdSctId(stdSectionModel);
		}
		newStdQtrQuestionsModel.setAnswerDate1Type(originalStdQtrQuestionsModel.getAnswerDate1Type());
		newStdQtrQuestionsModel.setAnswerDate2Type(originalStdQtrQuestionsModel.getAnswerDate2Type());
		newStdQtrQuestionsModel.setOrder(originalStdQtrQuestionsModel.getOrder());
		StdQtrQuestionsModel questionsModel = stdQtrQuestionsService.create(newStdQtrQuestionsModel);

		//Duplicate Options Handling.
		//Main Options Duplicate handling
		List<StdQstOptionModel> stdQstOptionModelList = originalStdQtrQuestionsModel.getStdQstOptionModel();
		List<StdQstOptionModel> newStdQstOptionModelList = new java.util.ArrayList<>(Collections.emptyList());

		for (StdQstOptionModel stdQstOptionModel : stdQstOptionModelList) {

			StdQstOptionModel newStdQstOptionModel = new StdQstOptionModel();
			newStdQstOptionModel.setSqoQstId(questionsModel);
			newStdQstOptionModel.setStdOptOnPremId(stdQstOptionModel.getStdOptOnPremId());
			newStdQstOptionModel.setSqoLabel(stdQstOptionModel.getSqoLabel());
			newStdQstOptionModel.setSqoValue(stdQstOptionModel.getSqoValue());

			try {
				stdQstOptionRepository.save(newStdQstOptionModel);
				newStdQstOptionModelList.add(newStdQstOptionModel);
			} catch (Exception ex) {
				LOGGER.error(ex.getMessage());
			}
		}

		// Options 1 Duplicate Handling
		List<StdAnswer1OptionModel> stdAnswer1OptionModelList = originalStdQtrQuestionsModel.getStdAnswer1OptionModel();
		List<StdAnswer1OptionModel> newStdAnswer1OptionModelList = new java.util.ArrayList<>(Collections.emptyList());

		for (StdAnswer1OptionModel stdAnswer1OptionModel : stdAnswer1OptionModelList) {

			StdAnswer1OptionModel newStdAnswer1OptionModel = new StdAnswer1OptionModel();
			newStdAnswer1OptionModel.setSqoQstId(questionsModel);
			newStdAnswer1OptionModel.setStdOpt1OnPremId(stdAnswer1OptionModel.getStdOpt1OnPremId());
			newStdAnswer1OptionModel.setsA1Value(stdAnswer1OptionModel.getsA1Value());
			newStdAnswer1OptionModel.setsA1Label(stdAnswer1OptionModel.getsA1Label());

			try {
				stdAnswer1OptionRepository.save(newStdAnswer1OptionModel);
				newStdAnswer1OptionModelList.add(newStdAnswer1OptionModel);
			} catch (Exception ex) {
				LOGGER.error(ex.getMessage());
			}
		}

		// Options 2 Duplicate Handling
		List<StdAnswer2OptionModel> stdAnswer2OptionModelList = originalStdQtrQuestionsModel.getStdAnswer2OptionModel();
		List<StdAnswer2OptionModel> newStdAnswer2OptionModelList = new java.util.ArrayList<>(Collections.emptyList());

		for (StdAnswer2OptionModel stdAnswer2OptionModel : stdAnswer2OptionModelList) {

			StdAnswer2OptionModel newStdAnswer2OptionModel = new StdAnswer2OptionModel();
			newStdAnswer2OptionModel.setSqoQstId(questionsModel);
			newStdAnswer2OptionModel.setStdOpt2OnPremId(stdAnswer2OptionModel.getStdOpt2OnPremId());
			newStdAnswer2OptionModel.setsA2Value(stdAnswer2OptionModel.getsA2Value());
			newStdAnswer2OptionModel.setsA2Label(stdAnswer2OptionModel.getsA2Label());

			try {
				stdAnswer2OptionRepository.save(newStdAnswer2OptionModel);
				newStdAnswer2OptionModelList.add(newStdAnswer2OptionModel);
			} catch (Exception ex) {
				LOGGER.error(ex.getMessage());
			}
		}

		//Setting StdQtrQuestions Options
		newStdQtrQuestionsModel.setStdQstOptionModel(newStdQstOptionModelList);
		newStdQtrQuestionsModel.setStdAnswer1OptionModel(newStdAnswer1OptionModelList);
		newStdQtrQuestionsModel.setStdAnswer2OptionModel(newStdAnswer2OptionModelList);

		LOGGER.info("Create new Question , id: " + questionsModel.getQtrQstId() + ".");
		LOGGER.info("Duplicate Requirement , id: " + originalStdRequirementsModel.getStdRqrId() + " success.");

		return newRequirementsModel;
	}

	public String getStdQtrNameByReqId(int id) {
		LOGGER.info("get StdQuestionnaireModel by req id: " + id + ".");
		StdQtrQuestionsModel stdQtrQuestionsModel = stdQtrQuestionsService.getQuestionsByRequirementId(id);
		if (stdQtrQuestionsModel == null) return "";
		StdSectionModel stdSectionModel = stdSectionService.getStdSectionModelByStdQtrQuestionsId(stdQtrQuestionsModel.getQtrQstId());
		if (stdSectionModel == null) return "";
		return stdQuestionaireService.getStdQuestionaireModelBySectionId(stdSectionModel.getStdSctId()).getStdQtrName();
	}

	public List<StdRequirementsModel> getReqByStdId(int id) {
		LOGGER.info("get StdQuestionnaireModels by std id: " + id + ".");
		return stdRequirementsRepository.getReqByStdId(id);
	}

	public List<StdRequirementsModel> getReqByQuestionnaireId(int id) {
		LOGGER.info("get StdRequirementsModel by std id: " + id + ".");

		List<StdSectionModel> stdSectionModelList = stdSectionService.getAllSectionByQuestionaireId(id);
		List<StdQtrQuestionsModel> questionList = new ArrayList<>(Collections.emptyList());
		List<StdRequirementsModel> requirementsModelListList = new ArrayList<>(Collections.emptyList());

		for (StdSectionModel stdSectionModel : stdSectionModelList) {

			List<StdQtrQuestionsModel> question = stdQtrQuestionsService.getAllQuestionBySectionId(stdSectionModel.getStdSctId());
			questionList.addAll(question);
		}

		for (StdQtrQuestionsModel question : questionList) {
			StdRequirementsModel stdRequirementsModel = stdRequirementsRepository.getReqByReqId(question.getRqr());
			requirementsModelListList.add(stdRequirementsModel);
		}
		return requirementsModelListList;
	}
}
