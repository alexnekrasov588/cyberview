package com.lstechs.ciso.service;

import com.lstechs.ciso.enums.ModuleEnum;
import com.lstechs.ciso.enums.UsersEnum;
import com.lstechs.ciso.model.*;
import com.lstechs.ciso.repository.CVERepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CVEService extends CRUDService<CVEModel, SingleCVEModel> {

	@Autowired
	private AuthenticationService authenticationService;

	@Autowired
	private CVERepository cveRepository;

	@Autowired
	private CVESourcesService cveSourcesService;

	@Autowired
	private ModuleTaskService moduleTaskService;

	private static final Logger LOGGER = LogManager.getLogger(CVEService.class.getName());

	public CVEService(CVERepository cveRepository) {
		super(cveRepository);
	}

	@Override
	public SingleCVEModel getSingleInstance(CVEModel cveModel) throws Exception {
		return new SingleCVEModel(cveModel);
	}

	public Page<CVEModel> getAllCVEs(Boolean isAssetMode, Integer pageNo, Integer pageSize, String orderBy, boolean orderByDesc, String query, String token) throws Exception {
		LOGGER.info("Get all from cve.");

		UsersEnum usersEnum = authenticationService.getUserType(token);
		CompanyModel companyModel = null;
		if (usersEnum.equals(UsersEnum.CompanyUser)) {
			// get user's company for module tasks
			companyModel = authenticationService.getCompanyFromToken(token);
		}

		orderBy = "publishedDate";
		Sort sort = Sort.by(Sort.Order.desc(orderBy));
		if (!orderByDesc) {
			sort = Sort.by(Sort.Order.asc(orderBy));
		}
		Pageable paging = PageRequest.of(pageNo, pageSize, sort);
		Page<CVEModel> pagedResult;

		/*
  		Querying by asset mode or not asset mode
		 */
		if (isAssetMode) {
			pagedResult = cveRepository.findAllCVEsByAssetFilter(paging, companyModel.getCmpId());
		} else {
			pagedResult = cveRepository.findAll(paging, query.toLowerCase());

		}

		List<CVEModel> cveModels = pagedResult.getContent();
		if (companyModel != null && cveModels != null && cveModels.size() > 0) {
			for (CVEModel tmpCVEModel : cveModels) {
				ModuleTaskModel moduleTaskModel = moduleTaskService.getModuleTaskByModuleAndIdAndCmpId(ModuleEnum.CVE, tmpCVEModel.getCveId(), companyModel.getCmpId());
				if (moduleTaskModel != null && moduleTaskModel.getTskId() != null)
					tmpCVEModel.setTaskModel(moduleTaskModel.getTskId());
			}
		}
		return pagedResult;
	}

	@Override
	public CVEModel update(int id, CVEModel cveModel) throws Exception {
		LOGGER.info("update CVE id: " + id);
		CVEModel updateCveModel = getById(id);
		updateCveModel.setCveDescription(cveModel.getCveDescription());
		updateCveModel.setCveIsAvilable(cveModel.isCveIsAvilable());
		updateCveModel.setCveTitle(cveModel.getCveTitle());
		CVESourcesModel cveSourcesModel = cveSourcesService.getById(cveModel.getCveSrcId().getCveSrcId());
		updateCveModel.setCveSrcId(cveSourcesModel);
		return cveRepository.save(updateCveModel);

	}

	public List<CVEModel> findCVEModelForDashboard() {
		return cveRepository.getLatestCVEForDashboard();
	}

	public List<CVEModel> findCVEModelForDashboardByAssets(String token) {

		UsersEnum usersEnum = authenticationService.getUserType(token.substring(7));
		CompanyModel companyModel = null;
		if (usersEnum.equals(UsersEnum.CompanyUser)) {
			// get user's company for module tasks
			companyModel = authenticationService.getCompanyFromToken(token.substring(7));
		}

		boolean orderByDesc = true;
		String orderBy = "publishedDate";
		Sort sort = Sort.by(Sort.Order.desc(orderBy));
		if (!orderByDesc) {
			sort = Sort.by(Sort.Order.asc(orderBy));
		}
		Pageable paging = PageRequest.of(0, 3, sort);
		return cveRepository.findAllCVEsByAssetFilter(paging, companyModel.getCmpId()).getContent();
	}


}
