package com.lstechs.ciso.service;

import com.lstechs.ciso.enums.QtrQstTypeEnum;
import com.lstechs.ciso.model.*;
import com.lstechs.ciso.repository.CustomerStandardRepository;
import com.lstechs.ciso.repository.StdQtrQuestionsRepository;
import javassist.NotFoundException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerStandardService extends CRUDService<CustomerStandardModel, SingleCustomerStandardModel> {

	@Autowired
	private CustomerStandardRepository customerStandardRepository;

	@Autowired
	private StdQtrQuestionsRepository stdQtrQuestionsRepository;

	@Autowired
	private StandardService standardService;

	@Autowired
	private StdRequirementsService stdRequirementsService;

	@Autowired
	private StdQtrQuestionsService stdQtrQuestionsService;


	private static final Logger LOGGER = LogManager.getLogger(CustomerStandardService.class.getName());

	public CustomerStandardService(CustomerStandardRepository customerStandardRepository) {
		super(customerStandardRepository);
	}

	@Override
	public SingleCustomerStandardModel getSingleInstance(CustomerStandardModel customerStandardModel) throws Exception {
		return new SingleCustomerStandardModel(customerStandardModel);
	}

	@Override
	@Transactional
	public Page<CustomerStandardModel> getAll(Integer pageNo, Integer pageSize, String orderBy, boolean orderByDesc, String query) throws Exception {
		LOGGER.info("get all customer standard model");
		Sort sort = Sort.by(Sort.Order.desc(orderBy));
		if (!orderByDesc) {
			sort = Sort.by(Sort.Order.asc(orderBy));
		}
		Pageable paging = PageRequest.of(pageNo, pageSize, sort);
		return customerStandardRepository.findAll(paging, query.toLowerCase());
	}

	@Override
	@Transactional
	public CustomerStandardModel getById(int id) throws Exception {
		LOGGER.info("get Customer Standard Model by id: " + id);
		return customerStandardRepository.findById(id).orElseThrow(() -> new NotFoundException("Id: " + id + " not found"));
	}

	@Override
	public CustomerStandardModel update(int id, CustomerStandardModel customerStandardModel) throws Exception {
		LOGGER.info("Update customer standard  , id: " + id + ".");
		CustomerStandardModel updateCustomerStandardModel = getById(id);
		updateCustomerStandardModel.setCompliant(customerStandardModel.getCompliant());
		updateCustomerStandardModel.setCstStdRemarks(customerStandardModel.getCstStdRemarks());
		updateCustomerStandardModel.setAnswerDate1(customerStandardModel.getAnswerDate1());
		updateCustomerStandardModel.setAnswerDate2(customerStandardModel.getAnswerDate2());

			StdQtrQuestionsModel stdQtrQuestionsModel = stdQtrQuestionsRepository.getStdQtrQuestionsModelById(updateCustomerStandardModel.getQtrQstId().getQtrQstId());
	       /*
           Checking if question type is File
           If not so we setting "CstQstAnswer" field.
		   */
			if (!stdQtrQuestionsModel.getQtrQstType().equals(QtrQstTypeEnum.FILE)) {
				updateCustomerStandardModel.setCstQstAnswer(customerStandardModel.getCstQstAnswer());
			}

			if (customerStandardModel.getQtrQstId() != null)
				updateCustomerStandardModel.setQtrQstId(stdQtrQuestionsService.getById(customerStandardModel.getQtrQstId().getQtrQstId()));
			if (customerStandardModel.getStdRqrId() != null)
				updateCustomerStandardModel.setStdRqrId(stdRequirementsService.getById(customerStandardModel.getStdRqrId().getStdRqrId()));
			if (customerStandardModel.getStdId() != null)
				updateCustomerStandardModel.setStdId(standardService.getById(customerStandardModel.getStdId().getStdId()));


		return customerStandardRepository.save(updateCustomerStandardModel);
	}

	@Override
	public CustomerStandardModel create(CustomerStandardModel customerStandardModel) throws Exception {
		LOGGER.info("Create new Customer Standard");
		customerStandardModel.setQtrQstId(stdQtrQuestionsService.getById(customerStandardModel.getQtrQstId().getQtrQstId()));
		customerStandardModel.setStdRqrId(stdRequirementsService.getById(customerStandardModel.getStdRqrId().getStdRqrId()));
		customerStandardModel.setStdId(standardService.getById(customerStandardModel.getStdId().getStdId()));
		return customerStandardRepository.save(customerStandardModel);
	}

	public void createCustomerStandard(StdRequirementsModel stdRequirementsModel, StdQtrQuestionsModel stdQtrQuestionsModel, StandardModel standardModel, CompanyModel companyModel) throws Exception {
		CustomerStandardModel customerStandardModel = new CustomerStandardModel();
		customerStandardModel.setStdRqrId(stdRequirementsModel);
		customerStandardModel.setQtrQstId(stdQtrQuestionsModel);
		customerStandardModel.setStdId(standardModel);
		CompanyModel cmpId = new CompanyModel();
		cmpId.setCmpId(companyModel.getCmpId());
		cmpId.setCmpName(companyModel.getCmpName());
		customerStandardModel.setCmpId(cmpId);
		if (!isCustomerStandardExist(cmpId.getCmpId(), stdRequirementsModel.getStdRqrId()))
			create(customerStandardModel);
	}

	public List<CustomerStandardModel> getCustomerStandardsByReqId(int id) {
		//each one to company
		LOGGER.info("Get all customer standards  by requirement , id: " + id + ".");
		return customerStandardRepository.getCustomerStandardsByReqId(id);
	}

	public List<CustomerStandardModel> getCustomerStandardsByQuestionsId(int id, int cmpId) {
		//each one to company
		LOGGER.info("Get all customer standards  by Questions , id: " + id + ".");
		return customerStandardRepository.getCustomerStandardsByQuestionsId(id, cmpId);
	}


	public CustomerStandardModel getCustomerStandardByReqIdAndCmpId(int rqrId, int cmpId) {
		LOGGER.info("Get customer standard by requirement , id: " + rqrId + ", and Company id: " + cmpId + ".");
		return customerStandardRepository.getCustomerStandardByReqIdAndCmpId(rqrId, cmpId);
	}

	public int getCompliantCount(int stdId) {
		LOGGER.info("Get standard compliance count by standard id: " + stdId + ".");
		return customerStandardRepository.getCompliantCount(stdId);
	}

	public void deleteCustomerStandard() {
		LOGGER.info("Delete customer standard  ");
		customerStandardRepository.deleteCustomerStandard();
	}

	private boolean isCustomerStandardExist(int cmpId, int stdRqrId) {
		return customerStandardRepository.isCustomerStandardExist(cmpId, stdRqrId) != null;
	}
}
