package com.lstechs.ciso.service;

import com.lstechs.ciso.model.CVESourcesModel;
import com.lstechs.ciso.model.SingleCVESourcesModel;
import com.lstechs.ciso.repository.CVESourcesRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CVESourcesService extends CRUDService<CVESourcesModel, SingleCVESourcesModel> {

    public CVESourcesService(CVESourcesRepository cveSourcesRepository) {
        super(cveSourcesRepository);
    }

    private static Logger LOGGER = LogManager.getLogger(CVESourcesService.class.getName());

    @Autowired
    private CVESourcesRepository cveSourcesRepository;

	@Override
	public SingleCVESourcesModel getSingleInstance(CVESourcesModel cveSourcesModel) throws Exception {
		SingleCVESourcesModel singleCVESourcesModel = new SingleCVESourcesModel(cveSourcesModel);
		return singleCVESourcesModel;
	}

	@Override
    public CVESourcesModel update(int id, CVESourcesModel cveSourcesModel) throws Exception {
        LOGGER.info( "update CVE source id: " + id);
        CVESourcesModel updateCveSourcesModel = getById(id);
        updateCveSourcesModel.setCveSrcIsActive(cveSourcesModel.isCveSrcIsActive());
        updateCveSourcesModel.setCveSrcSiteName(cveSourcesModel.getCveSrcSiteName());
        updateCveSourcesModel.setCveSrcSiteUrl(cveSourcesModel.getCveSrcSiteUrl());
        return cveSourcesRepository.save(updateCveSourcesModel);
    }
	

    public CVESourcesModel getOnPrem(int id) throws Exception {
		return cveSourcesRepository.getOnPrem(id);

    }
}
