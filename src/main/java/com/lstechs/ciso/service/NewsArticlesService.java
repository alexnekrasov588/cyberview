package com.lstechs.ciso.service;

import com.lstechs.ciso.enums.ModuleEnum;
import com.lstechs.ciso.enums.OperationEnum;
import com.lstechs.ciso.enums.UsersEnum;
import com.lstechs.ciso.model.*;
import com.lstechs.ciso.repository.NewsArticlesRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class NewsArticlesService extends CRUDService<NewsArticlesModel, SingleNewsArticlesModel> {

	public NewsArticlesService(NewsArticlesRepository newsArticlesRepository) {
		super(newsArticlesRepository);
	}

	@Autowired
	private NewsArticlesRepository newsArticlesRepository;

	@Autowired
	private ModuleTaskService moduleTaskService;

	@Autowired
	public NewsTagsService newsTagsService;

	@Autowired
	private AuthenticationService authenticationService;

	private static final Logger LOGGER = LogManager.getLogger(NewsArticlesService.class.getName());

	@Override
	public SingleNewsArticlesModel getSingleInstance(NewsArticlesModel newsArticlesModel) throws Exception {
		return new SingleNewsArticlesModel(newsArticlesModel);
	}

	@Override
	@CacheEvict(value = {"filterByTags", "findActiveNewsForDashboard", "findActiveAssetNewsForDashboard"}, allEntries = true)
	public NewsArticlesModel create(NewsArticlesModel newsArticlesModel) throws Exception {

		try {
			return super.create(newsArticlesModel);
		} catch (Exception ex) {
			LOGGER.error("create news faild , ex: " + ex);
			throw new Exception(ex);
		}

	}

	public Page<NewsArticlesModel> getAllNews(boolean isAssetMode, Integer pageNo, Integer pageSize, String orderBy,
											  boolean orderByDesc, String query, int[] tagsId, String token) throws Exception {
		LOGGER.info("Get all from News.");
		UsersEnum usersEnum = authenticationService.getUserType(token);

		CompanyModel companyModel = null;
		if (usersEnum.equals(UsersEnum.CompanyUser)) {
			// get user's company for module tasks
			companyModel = authenticationService.getCompanyFromToken(token);
		}
		Pageable paging;
		Page<NewsArticlesModel> pagedResult;
		Sort sort = Sort.by(Sort.Order.desc(orderBy));
		if (!orderByDesc) {
			sort = Sort.by(Sort.Order.asc(orderBy));
		}

		if (isAssetMode) {
			paging = PageRequest.of(pageNo, pageSize);
			if (tagsId != null && tagsId.length > 0)
				pagedResult = newsArticlesRepository.findAllActiveNewsByAssetFilter(paging, companyModel.getCmpId(), query.toLowerCase(), tagsId);
			else
				pagedResult = newsArticlesRepository.findAllActiveNewsByAsset(paging, companyModel.getCmpId(), query.toLowerCase());
		} else {
			if (tagsId != null && tagsId.length > 0) {
				paging = PageRequest.of(pageNo, pageSize);
				if (usersEnum.equals(UsersEnum.VendorUser)) {
					pagedResult = newsArticlesRepository.findAllNewsByTags(paging, query.toLowerCase(), tagsId);
				} else {
					pagedResult = newsArticlesRepository.findAllActiveNewsByTags(paging, query.toLowerCase(), tagsId);
				}
			} else {
				paging = PageRequest.of(pageNo, pageSize, sort);

				if (usersEnum.equals(UsersEnum.VendorUser)) {
					pagedResult = newsArticlesRepository.findAll(paging, query.toLowerCase());
				} else {
					pagedResult = newsArticlesRepository.findAllActiveNews(paging, query.toLowerCase());
				}

			}
		}

		List<NewsArticlesModel> newsArticlesModels = pagedResult.getContent();
		if (newsArticlesModels.size() > 0) {
			for (NewsArticlesModel tmpNewsArticlesModel : newsArticlesModels) {
				tmpNewsArticlesModel.setNewsArtcContent("");
				List<NewsTagsModel> tagsModels = newsTagsService
					.getTagsByArticleId(tmpNewsArticlesModel.getNewsArtcId());
				if (tagsModels != null && tagsModels.size() > 0) { // return news's tags in format "tagId"= ,
					// "newsTagName"=
					List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
					for (NewsTagsModel newsTagsModel : tagsModels) {
						Map<String, Object> tagsMap = new HashMap<String, Object>();
						tagsMap.put("newsTagId", newsTagsModel.getNewsTagId());
						tagsMap.put("newsTagName", newsTagsModel.getNewsTagName());
						mapList.add(tagsMap);
					}
					tmpNewsArticlesModel.setNewsArtcTagsLst(mapList);
				}
				if (companyModel != null && hasPermission(token, OperationEnum.READ, ModuleEnum.NEWS)) {
					ModuleTaskModel moduleTaskModel = moduleTaskService.getModuleTaskByModuleAndIdAndCmpId(ModuleEnum.NEWS, tmpNewsArticlesModel.getNewsArtcId(), companyModel.getCmpId());
					if (moduleTaskModel != null && moduleTaskModel.getTskId() != null)
						tmpNewsArticlesModel.setTaskModel(moduleTaskModel.getTskId());
				}
			}
		}
		return pagedResult;
	}

	@Cacheable(value = "findActiveNewsForDashboard")
	public List<NewsArticlesModel> findActiveNewsForDashboard() {
		Pageable paging;
		paging = PageRequest.of(0, 3);
		return newsArticlesRepository.findActiveNewsForDashboard(paging).getContent();
	}

	@Cacheable(value = "findActiveAssetNewsForDashboard")
	public List<NewsArticlesModel> findActiveAssetNewsForDashboard(int cmpId) {
		Pageable paging;
		paging = PageRequest.of(0, 3);
		 return  newsArticlesRepository.findAllActiveNewsByAsset(paging, cmpId, "").getContent();
	}

	public List<String> getAllArticleHashes() {
		return newsArticlesRepository.findAllArticleHashes();
	}

	public NewsArticlesModel getNewsArticle(int id) {
		NewsArticlesModel newsArticleModel = null;
		try {
			newsArticleModel = newsArticlesRepository.getNewsArticleById(id);
		} catch (Exception ex) {
			LOGGER.error(ex);
		}
		return newsArticleModel;
	}


}
