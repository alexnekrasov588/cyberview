package com.lstechs.ciso.service;

import com.lstechs.ciso.config.JwtTokenUtil;
import com.lstechs.ciso.model.*;
import com.lstechs.ciso.repository.LicenseRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.*;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Service
public class LicenseService extends CRUDService<LicenseModel, SingleLicenseModel> {

	public LicenseService(LicenseRepository licenseRepository) {
		super(licenseRepository);
	}

	@Autowired
	private LicenseRepository licenseRepository;

	@Autowired
	private LicenseStandardService licenseStandardService;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Value("${license.folder}")
	private String filePath;

	private static final Logger LOGGER = LogManager.getLogger(LicenseService.class.getName());

	@Override
	public SingleLicenseModel getSingleInstance(LicenseModel licenseModel) throws Exception {
		return new SingleLicenseModel((licenseModel));
	}

	@Override
	public LicenseModel create(LicenseModel licenseModel) throws Exception {
		LOGGER.info("create new license.");

		if (licenseRepository.findLicenseByToken(licenseModel.getLcnsToken()) != null) {
			LOGGER.error("Token already exist");
			throw new Exception("Token already exist");
		}
		LicenseModel createLicenseModel = licenseRepository.save(licenseModel);
		List<Integer> standardList = licenseModel.getLcnsStandardList();
		if (standardList != null) licenseStandardService.createLicenseStandard(standardList, createLicenseModel);
		return createLicenseModel;
	}

	@Override
	public LicenseModel delete(int id) throws Exception {
		LOGGER.info("delete license id: " + id + ".");
		LicenseModel licenseModel = getById(id);
		if (isAssignedToCmp(licenseModel.getCompanyModel())) {
			LOGGER.error("Cannot delete license that is being currently in use by company");
			throw new Exception("Cannot delete license that is being currently in use by company");
		}
		licenseModel.setDeletedAt(new Date(System.currentTimeMillis()));
		licenseStandardService.deleteLicenseStandardByLicenseId(id);
		return licenseRepository.save(licenseModel);
	}

	@Override
	public LicenseModel update(int id, LicenseModel licenseModel) throws Exception {
		LOGGER.info("update license id: " + id + ".");

		LicenseModel license = licenseRepository.findLicenseByToken(licenseModel.getLcnsToken());
		if (license != null && license.getLcnsId() != id) {
			LOGGER.error("Token already exist");
			throw new Exception("Token already exist");
		}

		LicenseModel updateLicenseModel = getById(id);
		updateLicenseModel.setLcnsToken(licenseModel.getLcnsToken());
		updateLicenseModel.setLcnsFeaturesLst(licenseModel.getLcnsFeaturesLst());
		updateLicenseModel.setLcnsAllowedUsersCount(licenseModel.getLcnsAllowedUsersCount());
		updateLicenseModel.setLcnsDescription(licenseModel.getLcnsDescription());
		updateLicenseModel.setLcnsStartDate(licenseModel.getLcnsStartDate());
		updateLicenseModel.setLcnsEndDate(licenseModel.getLcnsEndDate());
		updateLicenseModel.setLcnsIsActive(licenseModel.isLcnsIsActive());
		updateLicenseModel.setLcnsActivationIP(licenseModel.getLcnsActivationIP());
		updateLicenseModel.setLcnsActivationDate(licenseModel.getLcnsActivationDate());

		//Delete Standard from License
		if (updateLicenseModel.getLcnsStandardList() != licenseModel.getLcnsStandardList()) {
			licenseStandardService.deleteLicenseStandardByLicenseId(id);
		}
		licenseStandardService.createLicenseStandard(licenseModel.getLcnsStandardList(), updateLicenseModel);
		return licenseRepository.save(updateLicenseModel);
	}

	public LicenseModel createOrUpdate(LicenseModel licenseModel) throws Exception {
		LicenseModel existLicense = licenseRepository.findLicenseByToken(licenseModel.getLcnsToken());
		if (existLicense != null) {
			LOGGER.error("Token already exist");
			return this.update(existLicense.getLcnsId(), licenseModel);
		} else {
			return this.create(licenseModel);
		}
	}

	public boolean isLicenseExist() {
		boolean isLicenseExist;
		LOGGER.info("Is license exist.");
		List<LicenseModel> licenseModels = licenseRepository.getAllLicense();
		isLicenseExist = licenseModels != null && licenseModels.size() > 0;
		return isLicenseExist;
	}

	public String getLicenseToken(int licenseId) throws Exception {
		LOGGER.info("get license token id: " + licenseId + ".");
		LicenseModel licenseModel = getById(licenseId);

		try {
			List<LicenseStandardModel> licenseStandardModel = licenseModel.getLicenseStandardModels();
			List<Integer> stdId = new ArrayList<>(Collections.emptyList());

			for (LicenseStandardModel i : licenseStandardModel) {
				stdId.add(i.getStdId().getStdId());
			}
			licenseModel.setLcnsStandardList(stdId);
		} catch (Exception ex) {
			LOGGER.error(ex.getMessage());
		}

		if (!licenseModel.isLcnsIsActive())
			throw new Exception("The license must be assigned to company");
		List<CompanyModel> companyModels = licenseModel.getCompanyModel();
		CompanyModel companyModel = new CompanyModel();
		for (CompanyModel tmpCompanyModel : companyModels) {
			if (tmpCompanyModel.getDeletedAt() == null) companyModel = tmpCompanyModel;
		}
		companyModel.setCmpLcnsId(null);
		return jwtTokenUtil.generateLicenseToken(licenseModel, companyModel);
	}

	public Page<LicenseModel> getAllLicense(Integer pageNo, Integer pageSize, String orderBy, boolean orderByDesc, String query, boolean unassigned) {
		LOGGER.info("Get all Unassigned License ");
		Sort sort = Sort.by(Sort.Order.desc(orderBy));
		if (!orderByDesc) {
			sort = Sort.by(Sort.Order.asc(orderBy));
		}
		Pageable paging = PageRequest.of(pageNo, pageSize, sort);
		Page<LicenseModel> pagedResult = licenseRepository.findAll(paging, query.toLowerCase());

		if (unassigned) {
			List<LicenseModel> licenseModels = new ArrayList<>();
			for (LicenseModel tmpLicenseModel : pagedResult.getContent()) {
				if (!isAssignedToCmp(tmpLicenseModel.getCompanyModel()))
					licenseModels.add(tmpLicenseModel);
			}
			pagedResult = new PageImpl<>(licenseModels, paging, licenseModels.size());
		}

		return pagedResult;
	}

	private boolean isAssignedToCmp(List<CompanyModel> companyModelList) {
		if (companyModelList != null) {
			for (CompanyModel tmpCmpModel : companyModelList)
				if (tmpCmpModel.getDeletedAt() == null)
					return true;
		}
		return false;
	}

	public ResponseEntity<InputStreamResource> getLicenseFile(Integer licenseId, String token) throws Exception {
		File directory = new File(filePath);
		if (!directory.exists()) {
			directory.mkdir();
		}

		File file = new File(filePath + "/license.txt");
		FileWriter writer = new FileWriter(file);
		writer.write(token);
		writer.close();

		LOGGER.info("Generate license token for id: " + licenseId + " success.");
		InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
		return ResponseEntity.ok()
			.header(HttpHeaders.CONTENT_DISPOSITION,
				"attachment;filename=" + file.getName())
			.contentType(MediaType.APPLICATION_OCTET_STREAM).contentLength(file.length())
			.body(resource);
	}
}
