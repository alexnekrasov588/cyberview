package com.lstechs.ciso.service;

import com.lstechs.ciso.model.SingleCompanyUserModel;
import com.lstechs.ciso.model.StdQstOptionModel;
import com.lstechs.ciso.model.StdQtrQuestionsModel;
import com.lstechs.ciso.repository.StdQstOptionRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class StdQstOptionService extends CRUDService<StdQstOptionModel, SingleCompanyUserModel> {
	public StdQstOptionService(StdQstOptionRepository stdQstOptionRepository) {
		super(stdQstOptionRepository);
	}

	private static final Logger LOGGER = LogManager.getLogger(StdQstOptionService.class.getName());

	@Autowired
	public StdQstOptionRepository stdQstOptionRepository;

	@Autowired
	public StdQtrQuestionsService stdQtrQuestionsService;

	public void deleteStdQstOptionByQtsId(int id) {
		LOGGER.info("delete StdQstOption by Qts id: " + id);
		stdQstOptionRepository.deleteStdQstOptionModel(id);
	}

	public List<StdQstOptionModel> getStdQstOptionModelByStandardIds(int[] ids) {
		List<StdQtrQuestionsModel> stdQtrQuestionsModels = stdQtrQuestionsService.getAllQuestionByStandardIds(ids);
		if (stdQtrQuestionsModels != null && stdQtrQuestionsModels.size() > 0) {
			int i = 0;
			int[] qIds = new int[stdQtrQuestionsModels.size()];
			for (StdQtrQuestionsModel tmpStdQtrQuestionsModel : stdQtrQuestionsModels) {
				qIds[i] = tmpStdQtrQuestionsModel.getQtrQstId();
				i++;
			}

			List<Integer> list = Arrays.stream(qIds).boxed().collect(Collectors.toList());
			return stdQstOptionRepository.getOptionsListByQuestionsIds(list);
		}
		return null;
	}
}
