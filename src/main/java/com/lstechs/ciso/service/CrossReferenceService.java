package com.lstechs.ciso.service;

import com.lstechs.ciso.model.*;
import com.lstechs.ciso.repository.CrossReferenceRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class CrossReferenceService extends CRUDService<CrossReferenceModel, SingleCrossReferenceModel> {

	public CrossReferenceService(CrossReferenceRepository crossReferenceRepository) {
		super(crossReferenceRepository);
	}

	@Autowired
	private CrossReferenceRepository crossReferenceRepository;

	@Autowired
	private StdRequirementsService stdRequirementsService;


	private static Logger LOGGER = LogManager.getLogger(CompanyUserService.class.getName());

	@Override
	public SingleCrossReferenceModel getSingleInstance(CrossReferenceModel crossReferenceModel) throws Exception {
		if (crossReferenceModel.getStdRqrId().getDeletedAt() == null) {
			StdRequirementsModel requirementsModel = stdRequirementsService.getById(crossReferenceModel.getStdRqrId().getStdRqrId());
			SingleStdRequirementsModel stdRequirementsModel = new SingleStdRequirementsModel(requirementsModel,stdRequirementsService.getStdQtrNameByReqId(requirementsModel.getStdRqrId()));
			SingleCrossReferenceModel singleCrossReferenceModel = new SingleCrossReferenceModel(crossReferenceModel, stdRequirementsModel);
			return singleCrossReferenceModel;
		}
		return null;
	}

	@Override
	public CrossReferenceModel update(int id, CrossReferenceModel crossReferenceModel) throws Exception {
		LOGGER.info("update cross reference , id: " + id + ".");
		CrossReferenceModel updateCrossReferenceModel = getById(id);
		updateCrossReferenceModel.setStdRqrId(crossReferenceModel.getStdRqrId());
		updateCrossReferenceModel.setCstRqrId(crossReferenceModel.getCstRqrID());
		updateCrossReferenceModel.setStdId(crossReferenceModel.getStdId());
		return crossReferenceRepository.save(updateCrossReferenceModel);
	}

	public void deleteByCstRqrId(int cstRqrId) {
		LOGGER.info("Delete cross reference by custom requirement id: " + cstRqrId + ".");
		crossReferenceRepository.deleteByCstRqrId(cstRqrId);
	}

	public void deleteByRqrId(int rqrId) {
		LOGGER.info("Delete cross reference by req id: " + rqrId + ".");
		crossReferenceRepository.deleteByRqrId(rqrId);
	}

	public void deleteByStdId(int stdId) {
		LOGGER.info("Delete cross reference by std id: " + stdId + ".");
		crossReferenceRepository.deleteByStdId(stdId);
	}

	public List<CrossReferenceModel> getAllByCstRqrId(int cstRqrId) {
		LOGGER.info("Find all cross references by custom requirement id: " + cstRqrId + ".");
		return crossReferenceRepository.findAllByCstRqrId(cstRqrId);
	}

	public List<CrossReferenceModel> createCrossReferenceOfCstRqrId(CustomRequirementModel customRequirementModel,
																	List<CrossReferenceModel> crossReferenceModelList) throws Exception {
		LOGGER.info("Create cross references with custom requirement id: " + customRequirementModel.getCstRqrId() + ".");
		List<CrossReferenceModel> newCrossReferenceModels = new ArrayList<>();
		customRequirementModel.setCrossReferenceModels(null);

		for (CrossReferenceModel tmpCrossReferenceModel : crossReferenceModelList) {
			tmpCrossReferenceModel.setCstRqrId(customRequirementModel);
			CrossReferenceModel crossReferenceModel = crossReferenceRepository.save(tmpCrossReferenceModel);
			newCrossReferenceModels.add(crossReferenceModel);
		}
		return newCrossReferenceModels;
	}

	public Collection<CrossReferenceReportResultModel> crossReferenceReport() {
		LOGGER.info("run cross reference report.");
		return crossReferenceRepository.crossReference();
	}

}
