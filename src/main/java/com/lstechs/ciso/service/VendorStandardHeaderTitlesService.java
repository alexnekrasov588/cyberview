package com.lstechs.ciso.service;

import com.lstechs.ciso.model.StandardModel;
import com.lstechs.ciso.model.StdQuestionaireModel;
import com.lstechs.ciso.model.VendorStandardHeaderTitlesModel;
import com.lstechs.ciso.repository.VendorStandardHeaderTitlesRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VendorStandardHeaderTitlesService extends CRUDService<VendorStandardHeaderTitlesModel, VendorStandardHeaderTitlesModel> {

	@Autowired
	private VendorStandardHeaderTitlesRepository vendorStandardHeaderTitlesRepository;

	@Autowired
	private StdQuestionaireService stdQuestionaireService;

	@Autowired
	private StandardService standardService;

	public VendorStandardHeaderTitlesService(VendorStandardHeaderTitlesRepository vendorStandardHeaderTitlesRepository) {
		super(vendorStandardHeaderTitlesRepository);
	}

	private static final Logger LOGGER = LogManager.getLogger(VendorStandardHeaderTitlesService.class.getName());


	@Override
	public VendorStandardHeaderTitlesModel create(VendorStandardHeaderTitlesModel vendorStandardHeaderTitlesModel) throws Exception {
		try {
			return super.create(vendorStandardHeaderTitlesModel);
		} catch (Exception ex) {
			LOGGER.error("Create Vendor Titles failed , ex: " + ex);
			throw new Exception(ex);
		}
	}

	/**
	 * @param vendorStandardHeaderTitlesModel vendorStandardHeaderTitlesModel
	 * @param questionnaireId                 questionnaireId
	 * @return VendorStandardHeaderTitlesModel
	 * @throws Exception Exception
	 */
	public VendorStandardHeaderTitlesModel saveVendorRequirementsHeaderTitles(VendorStandardHeaderTitlesModel vendorStandardHeaderTitlesModel, int questionnaireId) throws Exception {

		StdQuestionaireModel stdQuestionaireModel = stdQuestionaireService.getById(questionnaireId);
		VendorStandardHeaderTitlesModel savedVendorRequirementsHeaderTitlesModel = new VendorStandardHeaderTitlesModel();

		if (!stdQuestionaireModel.isStandardCreatedByCompany()) {
			if (vendorStandardHeaderTitlesModel != null && questionnaireId > 0) {
				try {

					VendorStandardHeaderTitlesModel vendorStandardHeaderTitlesModelByStandardId = vendorStandardHeaderTitlesRepository.getVendorHeaderTitlesByStandardId(stdQuestionaireModel.getStdId().getStdId());

					//Setting The Titles
					vendorStandardHeaderTitlesModelByStandardId.setRequirementTitle(vendorStandardHeaderTitlesModel.getRequirementTitle());
					vendorStandardHeaderTitlesModelByStandardId.setDescriptionTitle(vendorStandardHeaderTitlesModel.getDescriptionTitle());
					vendorStandardHeaderTitlesModelByStandardId.setArticleNumberTitle(vendorStandardHeaderTitlesModel.getArticleNumberTitle());
					vendorStandardHeaderTitlesModelByStandardId.setExplanationTitle(vendorStandardHeaderTitlesModel.getExplanationTitle());
					vendorStandardHeaderTitlesModelByStandardId.setGenericAnswer1Title(vendorStandardHeaderTitlesModel.getGenericAnswer1Title());
					vendorStandardHeaderTitlesModelByStandardId.setGenericAnswer2Title(vendorStandardHeaderTitlesModel.getGenericAnswer2Title());

					savedVendorRequirementsHeaderTitlesModel = vendorStandardHeaderTitlesRepository.save(vendorStandardHeaderTitlesModelByStandardId);
				} catch (Exception ex) {
					LOGGER.info(ex.getMessage());
				}
			} else {
				LOGGER.info("VendorHeaderTitlesModel Or QuestionnaireId Is Null");
				throw new NullPointerException("VendorHeaderTitlesModel Or questionnaireId Is Null");
			}
		} else {
			LOGGER.info("Standard Created By Ciso User");
			throw new Exception("Standard Created By Ciso User");
		}
		return savedVendorRequirementsHeaderTitlesModel;
	}

	public VendorStandardHeaderTitlesModel getVendorHeaderTitlesByStandardId(int standardId) {

		VendorStandardHeaderTitlesModel savedVendorHeaderTitlesModel = new VendorStandardHeaderTitlesModel();
		try {
			savedVendorHeaderTitlesModel = vendorStandardHeaderTitlesRepository.getVendorHeaderTitlesByStandardId(standardId);
		} catch (Exception ex) {
			LOGGER.info(ex.getMessage());
		}
		return savedVendorHeaderTitlesModel;
	}


	public VendorStandardHeaderTitlesModel createVendorTitlesManually(int standardId) throws Exception {
		try {

			StandardModel standardModel =  standardService.getById(standardId);

			VendorStandardHeaderTitlesModel vendorStandardHeaderTitlesModel = new VendorStandardHeaderTitlesModel();
			vendorStandardHeaderTitlesModel.setStandardId(standardModel);

			return super.create(vendorStandardHeaderTitlesModel);
		} catch (Exception ex) {
			LOGGER.error("Create Vendor Titles failed , ex: " + ex);
			throw new Exception(ex);
		}
	}

}
