package com.lstechs.ciso.service;

import com.lstechs.ciso.model.CstQstFileAnswerModel;
import com.lstechs.ciso.model.CustomerStandardModel;
import com.lstechs.ciso.model.FilesModel;
import com.lstechs.ciso.model.StdQuestionaireModel;
import com.lstechs.ciso.repository.CstQstFileAnswerRepository;
import com.lstechs.ciso.repository.CustomerStandardRepository;
import com.lstechs.ciso.repository.FilesRepository;
import javassist.NotFoundException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Service
public class FilesService {

	@Autowired
	private FilesRepository filesRepository;

	@Autowired
	private CstQstFileAnswerRepository cstQstFileAnswerRepository;

	@Autowired
	private CustomerStandardRepository customerStandardRepository;

	@Value("${server.url}")
	private String url;

	private static final Logger LOGGER = LogManager.getLogger(FilesModel.class.getName());

	public FilesModel storeFile(MultipartFile file, CustomerStandardModel customerStandardModel, StdQuestionaireModel questionaireModel) throws Exception {

		String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));

		try {

			if (fileName.contains("..")) {
				throw new Exception("Sorry! Filename contains invalid path sequence " + fileName);
			}

			FilesModel dbFile = new FilesModel(fileName, file.getContentType(), file.getBytes(), customerStandardModel, questionaireModel);
			// validate wrong saving from client

			List<FilesModel> filesModelsList;
			if (customerStandardModel != null)
				filesModelsList = filesRepository.getAllFilesModelByCustomerStandardId(customerStandardModel.getCstStdId());
			else
				filesModelsList = filesRepository.getAllFilesModelByQuestionaireId(questionaireModel.getStdQtrId());


			//Not Necessary: Previous Code
//			if (filesModelsList != null && filesModelsList.size() > 0) {
//				for (FilesModel tmpFile : filesModelsList) {
//					delete(tmpFile);
//				}
//			}

			/*
			  Extracting the file URL
			 */
			FilesModel filesModel = filesRepository.save(dbFile); // save the recent uploaded file
			String fileDownloadUri = "//" + url + "/api/customerStandard/downloadFile/" + filesModel.getId();


			CstQstFileAnswerModel cstQstFileAnswerModel = new CstQstFileAnswerModel(fileName, fileDownloadUri, customerStandardModel);
			if (!(customerStandardModel == null)) {
				cstQstFileAnswerRepository.save(cstQstFileAnswerModel);
			}

			return filesModel;
		} catch (Exception ex) {
			throw new Exception("Could not store file " + fileName + ". Please try again!", ex);
		}
	}

	public List<FilesModel> getAllFilesModelByCompanyId(int cmpId) throws Exception {
		LOGGER.info("Get all files of customer standards of company id: " + cmpId + ".");
		try {
			return filesRepository.getAllFilesModelByCompanyId(cmpId);
		} catch (Exception ex) {
			throw new Exception("Could not get all files of customer standards of company id " + cmpId + ". Please try again!", ex);
		}
	}

	public List<FilesModel> getAllFilesModelByCustomerStandardIds(List<Integer> customerIds) throws Exception {
		LOGGER.info("Get all files of customer standards of customerIds.");
		if (customerIds.size() == 0) {
			return new ArrayList<>();
		}
		try {
			return filesRepository.getAllFilesModelByCustomerStandardIds(customerIds);
		} catch (Exception ex) {
			throw new Exception("Could not get all files of customer standards , Please try again!", ex);
		}
	}

	public List<FilesModel> getAllFilesModelByStdQtrIds(List<Integer> stdQstId) throws Exception {
		LOGGER.info("Get all files .");
		try {
			return filesRepository.getAllFilesModelByStdQtrIds(stdQstId);
		} catch (Exception ex) {
			throw new Exception("Could not get all files , Please try again!", ex);
		}
	}

	public void updateFileUrl(String fileId, String url) throws NotFoundException {
		FilesModel filesModel = getFile(fileId);
		filesModel.setFileUrl(url);
		filesRepository.save(filesModel);

	}

	public FilesModel getFile(String fileId) throws NotFoundException {
		FilesModel fileModel = filesRepository.findById(fileId)
			.orElseThrow(() -> new NotFoundException("File not found with id " + fileId));

		if (fileModel.getDeletedAt() != null) {
			throw new NotFoundException("File not found with id " + fileId);
		}
		return fileModel;
	}

	public FilesModel delete(String fileId) throws Exception {

		FilesModel filesModel = filesRepository.getById(fileId);
		CustomerStandardModel customerStandardModel = customerStandardRepository.getCustomerStandardsByCustomerStandardId(filesModel.getCstStdId().getCstStdId());
		LOGGER.info("Delete file of customer standard id: " + filesModel.getCstStdId().getCstStdId() + ".");
		filesModel.setDeletedAt(new Date(System.currentTimeMillis()));
		filesRepository.save(filesModel);

		/*
		  Deleting Customer Standard Files
		 */
		cstQstFileAnswerRepository.deleteCustomerStandardFiles(customerStandardModel, filesModel.getFileUrl());
		return filesModel;
	}

	public FilesModel delete(FilesModel fileModel) throws Exception {
		LOGGER.info("Delete file id: " + fileModel.getId() + ".");
		FilesModel filesModel = filesRepository.getById(fileModel.getId());
		filesModel.setDeletedAt(new Date(System.currentTimeMillis()));
		filesRepository.save(filesModel);
		return filesModel;
	}

	public InputStream zipFiles(Map<String, InputStream> fileList) {
		try {
			ByteArrayOutputStream fos = new ByteArrayOutputStream();
			ZipOutputStream zipOut = new ZipOutputStream(fos);
			for (Map.Entry<String, InputStream> entry : fileList.entrySet()) {
				String fileName = entry.getKey();
				InputStream file = entry.getValue();
				ZipEntry zipEntry = new ZipEntry(fileName);
				zipOut.putNextEntry(zipEntry);

				byte[] bytes = new byte[1024];
				int length;
				while ((length = file.read(bytes)) >= 0) {
					zipOut.write(bytes, 0, length);
				}
			}
			zipOut.close();
			fos.close();
			return new ByteArrayInputStream(fos.toByteArray());
		} catch (IOException e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}

}
