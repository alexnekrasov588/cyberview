package com.lstechs.ciso.service;

import com.lstechs.ciso.model.NewsTagsModel;
import com.lstechs.ciso.model.SingleNewsTagsModel;
import com.lstechs.ciso.repository.NewsTagsRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NewsTagsService extends CRUDService<NewsTagsModel, SingleNewsTagsModel> {

	private static Logger LOGGER = LogManager.getLogger(NewsTagsService.class.getName());

	@Autowired
	private NewsTagsRepository newsTagsRepository;

	@Override
	public SingleNewsTagsModel getSingleInstance(NewsTagsModel newsTagsModel) throws Exception {
		SingleNewsTagsModel singleNewsTagsModel = new SingleNewsTagsModel(newsTagsModel);
		return singleNewsTagsModel;
	}

	public NewsTagsService(NewsTagsRepository newsTagsRepository) {
		super(newsTagsRepository);
	}

	public Page<NewsTagsModel> autoComplete(Integer pageNo, Integer pageSize, String orderBy, String autocomplete) {
		LOGGER.info("Autocomplete string: " + autocomplete);
		Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(orderBy));
		Page<NewsTagsModel> pagedResult = newsTagsRepository.AutoComplete(paging, autocomplete);
		return pagedResult;
	}

	public List<NewsTagsModel> getByTagName(String tagName) {
		return newsTagsRepository.getByName(tagName);
	}

	public List<NewsTagsModel> getTagsByArticleId(int newsArtcId) {
		return newsTagsRepository.getNewsTagsByNewsArticleId(newsArtcId);
	}
}
