package com.lstechs.ciso.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;

@Service
public class MailService {

    @Autowired
    private JavaMailSender sender;

	@Value("${spring.mail.username}")
	private String from;

    private static Logger LOGGER = LogManager.getLogger(JavaMailSender.class.getName());

    public void sendEmail(String email, String text, String subject) throws Exception {
        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);
        helper.setFrom(from);
        helper.setTo(email);
        helper.setText(text);
        helper.setSubject(subject);
        LOGGER.info("send email to : " + email);
        sender.send(message);
    }


}
