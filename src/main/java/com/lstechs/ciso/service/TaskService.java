package com.lstechs.ciso.service;

import com.lstechs.ciso.enums.StatusEnum;
import com.lstechs.ciso.model.*;
import com.lstechs.ciso.repository.TaskRepository;
import javassist.NotFoundException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TaskService extends CRUDService<TaskModel, SingleTaskModel> {

	private static final Logger LOGGER = LogManager.getLogger(TaskService.class.getName());

	@Autowired
	private TaskRepository taskRepository;

	@Autowired
	private CompanyUserService companyUserService;

	@Autowired
	private ModuleTaskService moduleTaskService;

	@Autowired
	private NewsArticlesService newsArticlesService;

	@Autowired
	private CVEService cveService;

	@Autowired
	private ProjectService projectService;

	@Autowired
	private CustomerStandardService customerStandardService;

	@Autowired
	private StdQtrQuestionsService qtrQuestionsService;

	@Autowired
	private StdSectionService sectionService;

	@Autowired
	private VulnerabilityAssessmentsService vulnerabilityAssessmentsService;

	public TaskService(TaskRepository taskRepository) {
		super(taskRepository);
	}

	@Override
	public SingleTaskModel getSingleInstance(TaskModel taskModel) throws Exception {
		CompanyUserModel companyUserModel = companyUserService.getById(taskModel.getTskAssignee().getCmpUsrId());
		SingleCompanyUserModel singleCompanyUserModel = new SingleCompanyUserModel(companyUserModel);
		return new SingleTaskModel(taskModel, singleCompanyUserModel);
	}

	public void deleteTasks(int[] taskIds) throws Exception {
		if (taskIds.length > 0) {
			LOGGER.info("Delete tasks : " + Arrays.toString(taskIds));
			for (int taskId : taskIds) {
				LOGGER.info("Delete task id: " + taskId);
				delete(taskId);
			}
		}
	}

	@Override
	@CacheEvict(value = {"getListMapProjectsDetails", "projectOverview"}, allEntries = true)
	public TaskModel delete(int tskId) throws Exception {
		LOGGER.info("Delete task id: " + tskId);
		TaskModel taskModel = getById(tskId);
		if (taskModel.getModuleTaskModel() != null) {
			moduleTaskService.delete(taskModel.getModuleTaskModel().getMdlTskId());
		}
		/*
           In case we have related risk to current task
           We are setting it to null(Could not apply it with Hibernate)
		 */
		deleteTaskFromVulnerabilityAssessmentsModel(tskId);

		taskModel.setDeletedAt(new Date(System.currentTimeMillis()));
		return crudRepository.save(taskModel);
	}

	private void deleteTaskFromVulnerabilityAssessmentsModel(int tskId) throws Exception {
		TaskModel taskModel = getById(tskId);
		try {
			List<VulnerabilityAssessmentsModel> risks = taskModel.getVulnerabilityAssessmentsModel();

			for (VulnerabilityAssessmentsModel risk : risks) {
				risk.setTskId(null);
			}

		} catch (Exception ex) {
			LOGGER.info(ex);
		}

	}

	@Override
	@CacheEvict(value = {"getListMapProjectsDetails", "projectOverview"}, allEntries = true)
	public TaskModel create(TaskModel taskModel) throws Exception {
		TaskModel retTask = super.create(taskModel);
		projectService.updateProjectStatus(taskModel.getTskPrjID(), retTask);

		/*
          Updating project endDate and startDate if the task start/end dates exceed project timeline
		 */
		updateProjectEndDate(taskModel.getTskPrjID(), retTask);
		updateProjectStartDate(taskModel.getTskPrjID(), retTask);

		return retTask;
	}

	@CacheEvict(value = {"getListMapProjectsDetails", "projectOverview"}, allEntries = true)
	public TaskModel updateTask(int id, ProjectModel projectModel, TaskModel taskModel) throws Exception {

		LOGGER.info("Update task , id: " + id + ".");
		TaskModel updateTaskModel = getById(id);
		StatusEnum statusEnum = updateTaskModel.getStatus();
		updateTaskModel.setTskName(taskModel.getTskName());
		updateTaskModel.setTskDescription(taskModel.getTskDescription());
		updateTaskModel.setTskAssignee(taskModel.getTskAssignee());
		updateTaskModel.setTskPrjID(projectModel);
		updateTaskModel.setTskStartDate(taskModel.getTskStartDate());
		updateTaskModel.setTskEndDate(taskModel.getTskEndDate());
		updateTaskModel.setTskPriority(taskModel.getTskPriority());
		updateTaskModel.setTskCreatedBy(taskModel.getTskCreatedBy());
		updateTaskModel.setStatus(taskModel.getStatus());
		TaskModel retTask = taskRepository.save(updateTaskModel);

		/*
          Updating project endDate and startDate if the task start/end dates exceed project timeline
		 */
		updateProjectStartDate(projectModel, retTask);
		updateProjectEndDate(projectModel, retTask);

		if (!statusEnum.equals(taskModel.getStatus())) {
			projectService.updateProjectStatus(projectModel, retTask);
		}
		return retTask;
	}

	private void updateProjectStartDate(ProjectModel projectModel, TaskModel retTask) throws Exception {
		if (retTask.getTskStartDate().before(projectModel.getPrjStartDate())) {
			projectModel.setPrjStartDate(retTask.getTskStartDate());
			projectService.update(projectModel.getPrjId(), projectModel);
		}
	}

	@CacheEvict(value = {"getListMapProjectsDetails", "projectOverview"}, allEntries = true)
	public void updateProjectEndDate(ProjectModel projectModel, TaskModel taskModel) throws Exception {
		List<TaskModel> taskModels = getAllTasksProject(projectModel.getPrjId());
		Date maxDate = taskModels.stream().map(TaskModel::getTskEndDate).max(Date::compareTo).get();
		if (maxDate.after(projectModel.getPrjEndDate())) {
			projectModel.setPrjEndDate(maxDate);
			projectService.update(projectModel.getPrjId(), projectModel);
		}
	}

	public Page<TaskModel> getAllTasks(Integer pageNo, Integer pageSize, String orderBy, boolean orderByDesc,
									   String name, int assignee, int projId) throws Exception {
		LOGGER.info("get all Tasks with project id: " + projId);
		Sort sort = Sort.by(Sort.Order.desc(orderBy));
		if (!orderByDesc) {
			sort = Sort.by(Sort.Order.asc(orderBy));
		}
		Pageable paging = PageRequest.of(pageNo, pageSize, sort);
		return taskRepository.findTasks(paging, projId, name.toLowerCase(), assignee);
	}

	public void updateTasksStatusByProjectId(int projId, int cmpId, StatusEnum statusEnum) throws Exception {
		LOGGER.info("update tasks status by project id: " + projId);
		List<TaskModel> taskModels = taskRepository.getAllTaskByProjId(projId);
		for (TaskModel task : taskModels) {
			task.setStatus(statusEnum);
			update(task.getTskId(), task);
		}
	}

	public void updateTasksStatusToOnHoldByProjectId(int projId, int cmpId, StatusEnum statusEnum) throws Exception {
		LOGGER.info("update tasks status by project id: " + projId);
		List<TaskModel> taskModels = taskRepository.getAllTaskByProjId(projId);
		for (TaskModel task : taskModels) {
			if(!task.getStatus().equals(StatusEnum.DONE)){
				task.setStatus(statusEnum);
				update(task.getTskId(), task);
			}
		}
	}

	public List<TaskModel> getAllTasksProject(int projId) throws Exception {
		LOGGER.info("Get all Tasks by project id: " + projId);
		return taskRepository.getAllTaskByProjId(projId);
	}

	public List<TaskModel> getAllTaskByProjId(int projId) throws Exception {
		LOGGER.info("Get all Tasks by project id: " + projId);
		List<TaskModel> taskModels = taskRepository.getAllTaskByProjId(projId);
		for (TaskModel task : taskModels) {
			ModuleTaskModel moduleTaskModel = moduleTaskService.getModuleTaskByTaskId(task.getTskId());
			if (moduleTaskModel != null) {

				switch (moduleTaskModel.getModule()) {
					case NEWS:
						try {
							task.setUrl(
								newsArticlesService.getById(moduleTaskModel.getMdlTskIdInternal()).getNewsArtcUrl());
							task.setArtcTitle(
								newsArticlesService.getById(moduleTaskModel.getMdlTskIdInternal()).getNewsArtcTitle());
							task.setArtcContent(newsArticlesService.getById(moduleTaskModel.getMdlTskIdInternal())
								.getNewsArtcContent());
							task.setRtl(newsArticlesService.getById(moduleTaskModel.getMdlTskIdInternal()).getNewsSrcId()
								.getIsRtl());
						} catch (NotFoundException e) {
							task.setUrl("");
						}
						break;
					case CVE:
						try {
							task.setUrl(cveService.getById(moduleTaskModel.getMdlTskIdInternal()).getCveUrl());
							task.setArtcTitle(cveService.getById(moduleTaskModel.getMdlTskIdInternal()).getCveTitle());
							task.setArtcContent(
								cveService.getById(moduleTaskModel.getMdlTskIdInternal()).getCveDescription());
							task.setRtl(false);
						} catch (NotFoundException e) {
							task.setUrl("");
						}
						break;
					case CUSTOMER_STANDARD:
						try {
							CustomerStandardModel customerStandardModel = customerStandardService
								.getById(moduleTaskModel.getMdlTskIdInternal());
							StdQtrQuestionsModel stdQtrQuestionsModel = qtrQuestionsService
								.getById(customerStandardModel.getQtrQstId().getQtrQstId());
							StdSectionModel sectionModel = sectionService
								.getById(stdQtrQuestionsModel.getStdSctId().getStdSctId());

							task.setSecId(sectionModel.getStdSctId());
							task.setQstId(stdQtrQuestionsModel.getQtrQstId());
							task.setQuestionnaireId(sectionModel.getStdQtrId().getStdQtrId());
							task.setStdId(customerStandardModel.getStdId().getStdId());
						} catch (Exception ex) {
							LOGGER.error(ex);
						}
						break;
					case RISK_ASSESSMENTS:
						try {
							VulnerabilityAssessmentsModel vulnerabilityAssessmentsModel = vulnerabilityAssessmentsService.getById(moduleTaskModel.getMdlTskIdInternal());
							task.setVaId(vulnerabilityAssessmentsModel.getId());
						} catch (NotFoundException e) {
							task.setVaId(0);
						}

						break;
					default:
						break;
				}
			}
		}
		return taskModels;
	}

	public int getAllTasksCountByProjId(int projId) {
		LOGGER.info("Count all Tasks by project id: " + projId);
		return taskRepository.getAllTasksCountByProjId(projId);
	}

	public int getTasksCountByCompId(int cmpId) {
		LOGGER.info("Count all Tasks by company id: " + cmpId);
		return taskRepository.getTasksCountByCompId(cmpId);
	}

	public int getAllUnDoneTasksCountByProjId(int prjId) {
		LOGGER.info("Count all tasks that haven't yet been completed company id: " + prjId);
		return taskRepository.getAllUnDoneTasksCountByProjId(prjId);
	}

	public List<TaskModel> getAllUnDoneTasksByProjId(int projId) {
		LOGGER.info("Get all tasks that haven't yet been completed, by project id: " + projId);
		return taskRepository.getAllUnDoneTasksByProjId(projId);
	}

	public int getAllDoneTasksCountByProjId(int prjId) {
		LOGGER.info("Count all completed Tasks by company id: " + prjId);
		return taskRepository.getAllDoneTasksCountByProjId(prjId);
	}

	public List<ProjectModel> getAllProgressOverdueProjects() {
		LOGGER.info("get in progress projects");
		return taskRepository.getAllProgressOverdueProjects();
	}

	public boolean isTaskAssignToCmpUsrID(int cmpUsrId, int tskId) {
		LOGGER.info("Get is company user id: " + cmpUsrId + ", assigned to task id: " + tskId + ".");
		return taskRepository.isTaskAssignToCmpUsrId(cmpUsrId, tskId);
	}

	public ByteArrayInputStream getTasksReport(int cmpId, TasksReportModel tasksReportModel) throws Exception {

		String[] columns = tasksReportModel.getTasksReportTitle();
		try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();) {
			CreationHelper createHelper = workbook.getCreationHelper();
			Sheet sheet = workbook.createSheet("Tasks");

			Font headerFont = workbook.createFont();
			headerFont.setBold(true);
			headerFont.setColor(IndexedColors.BLACK.getIndex());

			Font boldFont = workbook.createFont();
			boldFont.setBold(true);
			boldFont.setColor(IndexedColors.BLACK.getIndex());
			boldFont.setFontName("Arial");

			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			headerCellStyle.setWrapText(true);
			headerCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
			headerCellStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
			headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

			int rowIndex = 0;

			// Row for Header
			Row headerRow = sheet.createRow(rowIndex);

			// Header
			for (int col = 0; col < columns.length; col++) {
				Cell cell = headerRow.createCell(col);
				cell.setCellValue(columns[col]);
				cell.setCellStyle(headerCellStyle);
			}

			boolean isEndDateExist = tasksReportModel.getEndDate() != null;
			boolean isStartDateExist = tasksReportModel.getStartDate() != null;

			List<Integer> ids = null;
			if (tasksReportModel.getPrjIds().length > 0)
				ids = Arrays.stream(tasksReportModel.getPrjIds()).boxed().collect(Collectors.toList());

			List<TaskModel> taskModels = taskRepository.getTasksReport(cmpId, isEndDateExist, isStartDateExist,
				tasksReportModel.getStartDate(), tasksReportModel.getEndDate(), ids);

			CellStyle regularCellStyle = workbook.createCellStyle();
			regularCellStyle.setWrapText(true);
			regularCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);

			CellStyle redCellStyle = workbook.createCellStyle();
			redCellStyle.setWrapText(true);
			redCellStyle.setFont(boldFont);
			redCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
			redCellStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
			redCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

			CellStyle greenCellStyle = workbook.createCellStyle();
			greenCellStyle.setWrapText(true);
			greenCellStyle.setFont(boldFont);
			greenCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
			greenCellStyle.setFillForegroundColor(IndexedColors.GREEN.getIndex());
			greenCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

			CellStyle yellowCellStyle = workbook.createCellStyle();
			yellowCellStyle.setWrapText(true);
			yellowCellStyle.setFont(boldFont);
			yellowCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
			yellowCellStyle.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
			yellowCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

			CellStyle blueCellStyle = workbook.createCellStyle();
			blueCellStyle.setWrapText(true);
			blueCellStyle.setFont(boldFont);
			blueCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
			blueCellStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
			blueCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

			CellStyle leftCellStyle = workbook.createCellStyle();
			leftCellStyle.setWrapText(true);
			leftCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
			leftCellStyle.setAlignment(HorizontalAlignment.LEFT);

			CellStyle regularDateCellStyle = workbook.createCellStyle();
			regularDateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd/MM/yyyy"));
			regularDateCellStyle.setWrapText(true);
			regularDateCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
			regularDateCellStyle.setAlignment(HorizontalAlignment.LEFT);

			List<ProjectModel> projectModels = new ArrayList<>();

			if (taskModels != null && taskModels.size() > 0) {
				for (TaskModel tsk : taskModels) {
					if (!projectModels.contains(tsk.getTskPrjID()))
						projectModels.add(tsk.getTskPrjID());
					rowIndex++;
					int cellIndex = 0;
					Row row = sheet.createRow(rowIndex);
					if (Arrays.stream(columns).anyMatch("Task Name"::equals)) {
						Cell cell = row.createCell(cellIndex);
						cellIndex++;
						cell.setCellValue(tsk.getTskName());
						cell.setCellStyle(regularCellStyle);
					}
					if (Arrays.stream(columns).anyMatch("Descrption"::equals)) {
						Cell cell = row.createCell(cellIndex);
						cellIndex++;
						cell.setCellValue(tsk.getTskDescription());
						cell.setCellStyle(regularCellStyle);
					}

					if (Arrays.stream(columns).anyMatch("Start Date"::equals)) {
						Cell cell = row.createCell(cellIndex);
						cellIndex++;
						cell.setCellValue(tsk.getTskStartDate());
						cell.setCellStyle(regularDateCellStyle);
					}
					if (Arrays.stream(columns).anyMatch("End Date"::equals)) {
						Cell cell = row.createCell(cellIndex);
						cellIndex++;
						cell.setCellValue(tsk.getTskEndDate());
						cell.setCellStyle(regularDateCellStyle);
					}
					if (Arrays.stream(columns).anyMatch("Status"::equals)) {
						Cell cell = row.createCell(cellIndex);
						cellIndex++;
						String status = convertTskStatus(tsk.getStatus());
						cell.setCellValue(status);
						switch (status) {
							case "Done":
								cell.setCellStyle(greenCellStyle);
								break;

							case "Opend":
								cell.setCellStyle(blueCellStyle);
								break;

							case "In Progress":
								cell.setCellStyle(yellowCellStyle);
								break;

							case "On Hold":
								cell.setCellStyle(redCellStyle);
								break;

						}

					}
					if (Arrays.stream(columns).anyMatch("Assignee"::equals)) {
						Cell cell = row.createCell(cellIndex);
						cellIndex++;
						cell.setCellValue(tsk.getTskAssignee().getCmpUsrFullName());
						cell.setCellStyle(regularCellStyle);
					}
					if (Arrays.stream(columns).anyMatch("Schedule"::equals)) {
						Cell cell = row.createCell(cellIndex);
						cellIndex++;
						String schedule = getSchedule(tsk);
						cell.setCellValue(schedule);
						if (schedule.equals("On Schedule"))
							cell.setCellStyle(greenCellStyle);
						if (schedule.equals("Off Schedule"))
							cell.setCellStyle(redCellStyle);
					}

					Cell cell = row.createCell(cellIndex);
					cell.setCellValue(tsk.getTskPrjID().getPrjName());
					cell.setCellStyle(regularCellStyle);

				}

				if (tasksReportModel.isTskPrj()) {
					Sheet prjSheet = workbook.createSheet("Projects Details");
					int rowPrjIndex = 0;
					Row headerPrjRow = prjSheet.createRow(rowPrjIndex);
					String[] prjCol = {"Project Name", "Descrption", "Budget", "Start Date", "Deadline", "Status",
						"Schedule", "Progress", "Manage"};
					for (int col = 0; col < prjCol.length; col++) {
						Cell cell = headerPrjRow.createCell(col);
						cell.setCellValue(prjCol[col]);
						cell.setCellStyle(headerCellStyle);
					}

					for (ProjectModel proj : projectModels) {
						rowPrjIndex++;
						Row row = prjSheet.createRow(rowPrjIndex);

						Cell cell = row.createCell(0);
						cell.setCellValue(proj.getPrjName());
						cell.setCellStyle(regularCellStyle);

						cell = row.createCell(1);
						cell.setCellValue(proj.getPrjDescription());
						cell.setCellStyle(regularCellStyle);

						cell = row.createCell(2);
						cell.setCellValue(proj.getPrjBudget());
						cell.setCellStyle(leftCellStyle);

						cell = row.createCell(3);
						cell.setCellValue(proj.getPrjStartDate());
						cell.setCellStyle(regularDateCellStyle);

						cell = row.createCell(4);
						cell.setCellValue(proj.getPrjEndDate());
						cell.setCellStyle(regularDateCellStyle);

						cell = row.createCell(5);
						String status = projectService.convertPrjStatus(proj.getPrjStatus());
						cell.setCellValue(status);
						switch (status) {
							case "Completed":
								cell.setCellStyle(greenCellStyle);
								break;

							case "Inittated":
								cell.setCellStyle(blueCellStyle);
								break;

							case "In Progress":
								cell.setCellStyle(yellowCellStyle);
								break;

							case "On Hold":
								cell.setCellStyle(redCellStyle);
								break;

							case "Planned":
								cell.setCellStyle(blueCellStyle);
								break;

						}

						cell = row.createCell(6);
						String schedule = projectService.getSchedule(proj);
						cell.setCellValue(schedule);
						if (schedule.equals("On Schedule"))
							cell.setCellStyle(greenCellStyle);
						if (schedule.equals("Off Schedule"))
							cell.setCellStyle(redCellStyle);

						cell = row.createCell(7);
						cell.setCellValue(proj.getPrjProgressPercent());
						cell.setCellStyle(leftCellStyle);

						cell = row.createCell(8);
						cell.setCellValue(proj.getPrjManagerId().getCmpUsrFullName());
						cell.setCellStyle(regularCellStyle);
					}

					prjSheet.setDefaultColumnWidth(30);
				}

			}

			sheet.setDefaultColumnWidth(30);
			workbook.write(out);
			return new ByteArrayInputStream(out.toByteArray());
		}

	}

	private String convertTskStatus(StatusEnum status) {

		switch (status) {
			case OPENED:
				return "Opend";

			case IN_PROGRESS:
				return "In Progress";

			case ON_HOLD:
				return "On Hold";

			case DONE:
				return "Done";

		}

		return null;

	}

	private String getSchedule(TaskModel model) throws ParseException {
		String schedule = "Off Schedule";
		Date now = new Date(System.currentTimeMillis());
		if (model.getStatus() == StatusEnum.DONE || model.getTskEndDate().compareTo(now) > 0) {
			schedule = "On Schedule";
		}
		return schedule;
	}

}
