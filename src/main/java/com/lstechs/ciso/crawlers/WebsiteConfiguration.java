package com.lstechs.ciso.crawlers;

import com.lstechs.ciso.model.NewsSourcesModel;

public class WebsiteConfiguration {
  String siteUrl;
  String nextPageBtnSelector;
  String articleUrlSelector;
  String articleImageSelector;
  String articleContentContainerSelector;
  String articleTitleSelector;
  String articlePubDateSelector;
  NewsSourcesModel sourcesModel;

  public WebsiteConfiguration(String siteUrl, String nextPageBtnSelector, String articleUrlSelector, String articleContentContainerSelector, String articleTitleSelector, String articleImageSelector, NewsSourcesModel sourcesModel,String articlePubDateSelector) {
    this.siteUrl = siteUrl;
    this.nextPageBtnSelector = nextPageBtnSelector;
    this.articleUrlSelector = articleUrlSelector;
    this.articleImageSelector = articleImageSelector;
    this.articleContentContainerSelector = articleContentContainerSelector;
    this.articleTitleSelector = articleTitleSelector;
    this.sourcesModel = sourcesModel;
    this.articlePubDateSelector = articlePubDateSelector;
  }
}
