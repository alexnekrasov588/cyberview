package com.lstechs.ciso.crawlers;

import com.lstechs.ciso.model.NewsArticlesModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.util.DigestUtils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.Month;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class WebsiteCrawler extends Thread {
	private final WebsiteConfiguration configuration;
	private final HashSet<String> pagesUrls;
	private final HashSet<String> articlesUrls;
	private final List<NewsArticlesModel> articles;
	private List<String> existingHashes;
	private boolean stopCrawler;
	private boolean isArticleFromToday = false;
	private static final Logger LOGGER = LogManager.getLogger(WebsiteCrawler.class.getName());


	public WebsiteCrawler(WebsiteConfiguration configuration) {
		this.pagesUrls = new HashSet<>();
		this.articlesUrls = new HashSet<>();
		this.articles = new ArrayList<>();
		this.configuration = configuration;
		this.existingHashes = null;
		this.stopCrawler = false;
	}

	private void collectPagesUrls(String URL) {
		try {
			Document document = Jsoup.connect(URL).get();
			Elements nextPageElements = document.select(configuration.nextPageBtnSelector);
			// collect all articles in this URL
			collectArticlesUrls(URL);
			if (!this.stopCrawler) {
				for (Element nextPageButton : nextPageElements) {
					String nextPageUrl = nextPageButton.attr("abs:href");
					if (!nextPageUrl.isEmpty()) {
						if (pagesUrls.add(nextPageUrl)) {
							System.out.println(nextPageUrl);
						}
						// go to next page
						collectPagesUrls(nextPageUrl);
					}
				}
			}

		} catch (IOException ex) {
			LOGGER.error(ex.getMessage());
		}
	}

	private void collectArticlesUrls(String pageUrl) {
		try {
			Document document = Jsoup.connect(pageUrl).get();
			Elements articleElements = document.select(configuration.articleUrlSelector);

			for (Element articleElement : articleElements) {
				String articleUrl = articleElement.attr("abs:href");
				if (!articleUrl.isEmpty()) {
					if (articlesUrls.add(articleUrl)) {
						System.out.println(articleUrl);
					}
				}
			}
			getAllArticles();
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}


	private void getAllArticles() {
		for (String articleUrl : articlesUrls) {
			this.extractArticleData(articleUrl);
		}
		this.stopCrawler = true;
	}

	private void extractArticleData(String articlePageUrl) {
		try {
			Document document = Jsoup.connect(articlePageUrl).get();
			Elements elements = document.select(configuration.articleContentContainerSelector);
			Element articlePubDateSelector = document.select(configuration.articlePubDateSelector).first();

			Date date = new Date();
			Date now = new Date(System.currentTimeMillis());

			try {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				date = sdf.parse(articlePubDateSelector.attr("datetime").replace('T', ' '));
			} catch (Exception e) {
				try {
					String tmp = articlePubDateSelector.text();
					String[] dateArr = tmp.split(" ");
					int year = Integer.parseInt(dateArr[2]);
					int month = Month.valueOf(dateArr[0].toUpperCase()).getValue();
					int day = Integer.parseInt(dateArr[1].replace(',', ' ').replaceAll("\\s+", ""));
					Calendar calendar1 = Calendar.getInstance();
					calendar1.set(year, month - 1, day, 0, 0);
					date = calendar1.getTime();
				} catch (Exception ex) {
					LOGGER.error(ex.getMessage());
				}

			}

			if (checkIfArticleFromThePast24Hours(date)) {
				Elements articleTitleElements = document.select(configuration.articleTitleSelector);
				String articleTitle = articleTitleElements.text();

				if (articleTitle != null) {
					String titleHash = DigestUtils.md5DigestAsHex(articlePageUrl.getBytes());
					if (!this.existingHashes.contains(titleHash)) {
						String articleImageUrl = null;
						if (configuration.articleImageSelector != null) {
							Elements articleImageElements = document.select(configuration.articleImageSelector);
							if (articleImageElements.size() > 0) {
								articleImageUrl = articleImageElements.attr("src");
							}
						}

						/*
						 Remove all style attributes
			 			The frontend is handling the styling
			            */
						Elements articleContentElements = new Elements();
						for (Element elm : elements) {
							Element e = (Element) elm
								.removeAttr("style")
								.removeAttr("height")
								.removeAttr("width")
								.removeAttr("class")
								.removeAttr("sizes")
								.removeAttr("srcset")
								.removeAttr("border")
								.removeAttr("loading");
							articleContentElements.add(e);
						}

						List<String> tempParagraphList = new ArrayList<>(Collections.emptyList());

						for (Element element : articleContentElements) {
							String elementAsString = element.toString();

							if (!elementAsString.contains("<iframe")) {
								tempParagraphList.add(elementAsString);

							}
						}

					/*
                       Special Handling for:
                       https://thehackernews.com/
					 */
						if (articlePageUrl.contains("https://thehackernews.com/")) {
							tempParagraphList.remove(0);
						}

					/*
                       Special Handling for:
                       https://www.bleepingcomputer.com/
					 */
						if (articlePageUrl.contains("https://www.bleepingcomputer.com/")) {
							tempParagraphList.remove(0);
							tempParagraphList.remove(tempParagraphList.size() - 1);
							tempParagraphList.remove(tempParagraphList.size() - 1);
							tempParagraphList.remove(tempParagraphList.size() - 1);
							tempParagraphList.remove(tempParagraphList.size() - 1);
							tempParagraphList.remove(tempParagraphList.size() - 1);
							tempParagraphList.remove(tempParagraphList.size() - 1);
						}

						StringBuilder stringBuilder = new StringBuilder();
						for (String s : tempParagraphList) {
							stringBuilder.append(s);
						}

						String articleContent = stringBuilder.toString();

						articles.add(new NewsArticlesModel(articleTitleElements.text(), articlePageUrl, articleImageUrl,
							this.configuration.sourcesModel, articleContent,
							elements.text().substring(0, 200), true, date));
					}
				}
			}
		} catch (IOException ex) {
			LOGGER.error(ex.getMessage());
		}
	}

	/**
	 * This Function is checking if the article date is from the last 24 hours
	 * @param articleDate articleDate
	 * @return Boolean
	 */
	private Boolean checkIfArticleFromThePast24Hours(Date articleDate) {
		Date now = new Date(System.currentTimeMillis());
		Instant nowForYesterday = Instant.now();
		Instant yesterday = nowForYesterday.minus(1, ChronoUnit.DAYS);
		return articleDate.before(now) && articleDate.after(Date.from(yesterday));
	}

	/**
	 * This function extracting the article content from RSS sources
	 *
	 * @param articlePageUrl                  articlePageUrl
	 * @param articleContentContainerSelector articleContentContainerSelector
	 * @param baseURL                         baseURL to handle specially each source
	 * @return Article
	 */
	public static ArrayList<String> extractArticleContentFromRSS(String articlePageUrl, String
		articleContentContainerSelector, String baseURL) {
		String content = "";
		ArrayList<String> returnedArray = new ArrayList<>();
		if (articleContentContainerSelector == null || articleContentContainerSelector.isEmpty()) {
			returnedArray.add(content);
			return returnedArray;
		}
		try {
			Document document = Jsoup.connect(articlePageUrl).get();

			Elements elements = document.select(articleContentContainerSelector);
			Elements articleContentElements = new Elements();

			/*
			 Remove all style attributes
			 The frontend is handling the styling
			 */
			for (Element elm : elements) {
				Element e = (Element) elm
					.removeAttr("style")
					.removeAttr("height")
					.removeAttr("width")
					.removeAttr("class")
					.removeAttr("sizes")
					.removeAttr("srcset")
					.removeAttr("border")
					.removeAttr("loading");
				articleContentElements.add(e);
			}

			/*
			  Special Handling for "https://www.darkreading.com/rss_simple.asp"
			 */
			if (baseURL.equals("http://feeds.feedburner.com/gbhackers")) {
				List<String> tempParagraphList = new ArrayList<>(Collections.emptyList());

				for (Element element : articleContentElements) {

					String elementAsString = element.toString();
					if (!elementAsString.contains("<iframe")) {
						tempParagraphList.add(elementAsString);

					}
				}
				tempParagraphList.remove(tempParagraphList.size() - 1);

				StringBuilder stringBuilder = new StringBuilder();
				for (String s : tempParagraphList) {
					stringBuilder.append(s).append("<br>");
				}
				content = stringBuilder.toString();
			}

			/*
			  Special Handling for "https://www.darkreading.com/rss_simple.asp"
			 */
			if (baseURL.equals("https://www.darkreading.com/rss_simple.asp")) {

				List<String> tempParagraphList = new ArrayList<>(Collections.emptyList());

				for (Element element : articleContentElements) {
					String elementAsString = element.toString();
					if (!elementAsString.contains("<iframe")) {
						tempParagraphList.add(elementAsString);

					}
				}
				tempParagraphList.remove(tempParagraphList.size() - 1);
				tempParagraphList.remove(tempParagraphList.size() - 1);
				StringBuilder stringBuilder = new StringBuilder();
				for (String s : tempParagraphList) {
					stringBuilder.append(s).append("<br>");
				}
				content = stringBuilder.toString();
			}

			  /*
			  Special Handling for "https://krebsonsecurity.com/"
			 */
			if (baseURL.equals("https://krebsonsecurity.com/feed/")) {
				List<String> tempParagraphList = new ArrayList<>(Collections.emptyList());

				for (Element element : articleContentElements) {
					String elementAsString = element.toString();
					if (!elementAsString.contains("<iframe")) {
						tempParagraphList.add(elementAsString);
					}
				}
				StringBuilder stringBuilder = new StringBuilder();
				for (String s : tempParagraphList) {
					stringBuilder.append(s);
				}

				content = stringBuilder.toString();

			}
			returnedArray.add(content);
			returnedArray.add(elements.text().substring(0, 200) + "...");
		} catch (IOException ex) {
			LOGGER.error(ex.getMessage());
		}
		return returnedArray;
	}

	/**
	 * This function extract the article content from API sources
	 * @param articlePageUrl                  Site URL
	 * @param articleContentContainerSelector Content selector
	 * @return Article content
	 */
	public static ArrayList<String> extractArticleContent(String articlePageUrl, String articleContentContainerSelector) {
		String content = "";
		ArrayList<String> returnedArray = new ArrayList<>();
		if (articleContentContainerSelector == null || articleContentContainerSelector.isEmpty()) {
			returnedArray.add(content);
			return returnedArray;
		}
		try {
			Document document = Jsoup.connect(articlePageUrl).get();
			Elements articleContentElements = document.select(articleContentContainerSelector);

			List<String> tempParagraphList = new ArrayList<>(Collections.emptyList());

			for (Element element : articleContentElements) {
				String elementAsString = element.toString();
				tempParagraphList.add(elementAsString);
			}

			StringBuilder stringBuilder = new StringBuilder();

			for (String s : tempParagraphList) {
				stringBuilder.append(s);
			}
			content = stringBuilder.toString();
			returnedArray.add(content);
			returnedArray.add(articleContentElements.text().substring(0, 200) + "...");
		} catch (IOException e) {
			LOGGER.error(e.getMessage());
		}
		return returnedArray;
	}

	public List<NewsArticlesModel> scanSite(List<String> existingHashes) {
		this.existingHashes = existingHashes;
		this.collectPagesUrls(configuration.siteUrl);
		return articles;
	}

	@Override
	public void run() {
		super.run();
	}
}
