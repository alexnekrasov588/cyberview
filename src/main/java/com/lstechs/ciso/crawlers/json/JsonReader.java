package com.lstechs.ciso.crawlers.json;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lstechs.ciso.model.NewsArticlesModel;
import com.lstechs.ciso.model.NewsSourcesModel;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class JsonReader {
	private String url;
	private String titleKey;
	private String descriptionKey;
	NewsSourcesModel sourcesModel;
	private List<NewsArticlesModel> articles;

	public JsonReader(String sUrl, String titleKey, String descriptionKey, NewsSourcesModel source) {
		this.url = sUrl;
		this.titleKey = titleKey;
		this.descriptionKey = descriptionKey;
		this.sourcesModel = source;
		this.articles = new ArrayList<>();
	}

	public String getDescriptionKey() {
		return descriptionKey;
	}

	public void setDescriptionKey(String descriptionKey) {
		this.descriptionKey = descriptionKey;
	}

	public List<NewsArticlesModel> scan() {
		String jsonString = JsonReader.jsonGetRequest(this.url);
		ObjectMapper mapper = new ObjectMapper();
		try {
			JsonNode actualObj = mapper.readTree(jsonString);
			JsonNode results = actualObj.get("results");
			results.forEach(jsonNode -> {
				String url = this.sourcesModel.getNewsSrcBaseUrl() + jsonNode.get("BaseUrl").textValue() + jsonNode.get("UrlName").textValue();
				String title = jsonNode.get(this.titleKey).asText();
				String description = jsonNode.get("Description").asText();
				String publishDate = jsonNode.get(sourcesModel.getNewsDateSelector()).asText().substring(0,19).replace('T',' ');
				Date date = new Date();
				try {
					SimpleDateFormat  sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					date =sdf.parse(publishDate);
				} catch (ParseException e) {
					e.printStackTrace();
				}

				this.articles.add(new NewsArticlesModel(title, url, null, this.sourcesModel, description, description, true,date));
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
		return this.articles;
	}

	private static String streamToString(InputStream inputStream) {
		return new Scanner(inputStream, "UTF-8").useDelimiter("\\Z").next();
	}

	public static String jsonGetRequest(String urlQueryString) {
		String json = null;
		try {
			URL url = new URL(urlQueryString);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoOutput(true);
			connection.setInstanceFollowRedirects(false);
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setRequestProperty("charset", "utf-8");
			connection.connect();
			InputStream inStream = connection.getInputStream();
			json = streamToString(inStream); // input stream to string
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return json;
	}
}
