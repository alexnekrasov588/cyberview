package com.lstechs.ciso.crawlers.rss;

import com.lstechs.ciso.model.CRUDModel;
import com.rometools.rome.feed.synd.SyndCategory;
import com.rometools.rome.feed.synd.SyndEnclosure;
import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.io.FeedException;
import com.rometools.rome.io.SyndFeedInput;
import com.rometools.rome.io.SyndFeedOutput;
import com.rometools.rome.io.XmlReader;
import org.jdom2.Element;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.xml.sax.InputSource;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class RSSFeedParser {
	private CRUDModel sourcesModel;
	private SyndFeed feed;

	public RSSFeedParser(String feedUrl, CRUDModel source) {
		try {
			this.sourcesModel = source;
			URL url = new URL(feedUrl);
			XmlReader reader = new XmlReader(url);
			this.feed = new SyndFeedInput().build(reader);
			/*SyndFeedOutput output = new SyndFeedOutput();
			output.output(this.feed,new PrintWriter(System.out));*/
		} catch (MalformedURLException e) {
			throw new RuntimeException(e);
		} catch (FeedException | IOException e) {
			e.printStackTrace();
		}
	}

	public CRUDModel getSourcesModel() {
		return this.sourcesModel;
	}

	private ArrayList<String> getCategoriesList(List<SyndCategory> categories) {
		ArrayList<String> tags = new ArrayList<>();
		for (SyndCategory category : categories) {
			tags.add(category.getName());
		}
		return tags;
	}

	public List<FeedMessage> getArticles() {
		if (feed == null) return new ArrayList<>();
		List<SyndEntry> entries = feed.getEntries();
		List<FeedMessage> feedArticles = new ArrayList<>();
		for (SyndEntry item : entries) {
			String imgURL = null;
			List<Element> foreignMarkup = item.getForeignMarkup();
			if (!foreignMarkup.isEmpty()) {
				for (Element e : foreignMarkup) {
					if (e.getName().equals("thumbnail")) {
						imgURL = e.getAttribute("url").getValue();
					}
				}
			}
			String rawItemDescription = item.getDescription().getValue();
			Document document = Jsoup.parse(rawItemDescription);
			String itemDescription = document.text();
			imgURL = document.select("img").attr("src");
			String breakChars = "[…]";
			int bracketsIdx = itemDescription.indexOf(breakChars);
			if (bracketsIdx > -1) {
				String rawLinkText = itemDescription.substring(bracketsIdx + breakChars.length());
				itemDescription = itemDescription.substring(0, bracketsIdx);
				org.jsoup.nodes.Element linkElement = document.createElement("a");
				linkElement.attr("href", item.getUri());
				linkElement.attr("target", "_blank");
				linkElement.html(rawLinkText);
				itemDescription += linkElement.toString();
			}
			feedArticles.add(new FeedMessage(item.getTitle(), itemDescription, item.getUri(), item.getAuthor(), null, imgURL, item.getPublishedDate(), this.getCategoriesList(item.getCategories())));
		}
		return feedArticles;
	}
}
