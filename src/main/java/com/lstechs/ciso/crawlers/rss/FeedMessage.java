package com.lstechs.ciso.crawlers.rss;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/*
 * Represents one RSS message
 */
public class FeedMessage {

	String title;
	String description;
	String link;
	String author;
	String guid;
	String imageUrl;
	Date date;
	ArrayList<String> tagsList;

	public FeedMessage() {

	}

	public FeedMessage(String title, String description, String link, String author, String guid, String imageUrl, Date date, List<String> tagsList) {
		this.title = title;
		this.description = description;
		this.link = link;
		this.author = author;
		this.guid = guid;
		this.imageUrl = imageUrl;
		this.date = date;
		this.tagsList = (ArrayList<String>) tagsList;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public ArrayList<String> getTagsList() {
		return this.tagsList;
	}

	public void setTagsList(ArrayList<String> tagsList) {
		this.tagsList = tagsList;
	}

	@Override
	public String toString() {
		return "FeedMessage [title=" + title + ", description=" + description
			+ ", link=" + link + ", author=" + author + ", guid=" + guid
			+ "]";
	}

}
