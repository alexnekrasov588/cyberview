package com.lstechs.ciso.controller;

import com.lstechs.ciso.enums.ModuleEnum;
import com.lstechs.ciso.model.NewsSourcesModel;
import com.lstechs.ciso.model.SingleNewsSourcesModel;
import com.lstechs.ciso.service.NewsSourcesService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/newsSources")
public class NewsSourcesController extends CRUDController<NewsSourcesModel, SingleNewsSourcesModel> {
    public NewsSourcesController(NewsSourcesService newsSourcesService) {
        super(newsSourcesService, ModuleEnum.NEWS);
    }
}
