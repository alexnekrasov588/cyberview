package com.lstechs.ciso.controller;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.lstechs.ciso.enums.ModuleEnum;
import com.lstechs.ciso.enums.OperationEnum;
import com.lstechs.ciso.enums.UsersEnum;
import com.lstechs.ciso.model.*;
import com.lstechs.ciso.service.AuthenticationService;
import com.lstechs.ciso.service.CompanyService;
import com.lstechs.ciso.service.CompanyUserService;
import com.lstechs.ciso.service.RoleService;
import javassist.NotFoundException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.nio.charset.StandardCharsets;
import java.util.Map;

@RestController
@RequestMapping("/company")
public class CompanyController extends CRUDController<CompanyModel, SingleCompanyModel> {

	@Autowired
	private AuthenticationService authenticationService;

	@Autowired
	private CompanyUserService companyUserService;

	@Autowired
	private CompanyService companyService;

	@Autowired
	private RoleService roleService;

	private static final Logger LOGGER = LogManager.getLogger(CompanyController.class.getName());

	public CompanyController(CompanyService companyService) {
		super(companyService, ModuleEnum.VENDOR);
	}

	@PostMapping("/createCompany")
	public ResponseEntity<ResponseModel> createCompany
		(@Validated(CRUDModel.CRUDCreation.class) @RequestBody CompanyModel companyModel,
		 @RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.CREATE, ModuleEnum.AUTH);
			CompanyModel savedCompanyModel = companyService.create(companyModel);
			LOGGER.info("Create new Company user ,id:" + savedCompanyModel.getCmpId() + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, companyService.getSingleInstance(savedCompanyModel), null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Create new  Company user failed ,exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@PostMapping("/{companyId}/user")
	public ResponseEntity<ResponseModel> createCompanyUser
		(@PathVariable(value = "companyId") int companyId, @Validated(CRUDModel.CRUDCreation.class) @RequestBody CompanyUserModel companyUserModel,
		 @RequestHeader(name = "Authorization") String token) throws Exception {
		try {
			handlePermission(token, OperationEnum.CREATE, ModuleEnum.AUTH);
			CompanyModel companyModel = companyService.getValidCompany(companyId);
			companyUserModel.setCmpUsrCmpId(companyModel);
			RoleModel roleModel = roleService.getById(companyUserModel.getCmpUsrRoleId().getRoleId());
			companyUserModel.setCmpUsrRoleId(roleModel);
			CompanyUserModel userModel = companyUserService.create(companyUserModel);
			LOGGER.info("Create new Company user ,id:" + userModel.getCmpUsrId() + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, companyUserService.getSingleInstance(userModel), null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Create new  Company user failed ,exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@PutMapping("/{companyId}/user/{userId}")
	public ResponseEntity<ResponseModel> updateUser
		(@PathVariable(value = "companyId") int companyId,
		 @PathVariable(value = "userId") int userId,
		 @Validated(CRUDModel.CRUDUpdate.class) @RequestBody CompanyUserModel companyUserModel,
		 @RequestHeader(name = "Authorization") String token) throws Exception {
		try {
			handlePermission(token, OperationEnum.UPDATE, ModuleEnum.AUTH);
			CompanyModel companyModel = companyService.getById(companyId);
			if (companyModel.getCmpId() != companyUserModel.getCmpUsrCmpId().getCmpId()) {
				LOGGER.error("User id: " + userId + "not exist in this company.");
				throw new Exception("User id: " + userId + "not exist in this company");
			}
			CompanyUserModel updateCompanyUserModule = companyUserService.update(userId, companyUserModel);
			LOGGER.info("Update user ,id: " + userId + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, companyUserService.getSingleInstance(updateCompanyUserModule), null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Update user id: " + userId + " failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@DeleteMapping("/{companyId}/user/{userId}")
	public ResponseEntity<ResponseModel> deleteUser
		(@PathVariable(value = "companyId") int companyId, @PathVariable(value = "userId") int userId, @RequestHeader(name = "Authorization") String token) throws Exception {
		try {
			handlePermission(token, OperationEnum.DELETE, ModuleEnum.AUTH);
			UsersEnum usersEnum = authenticationService.getUserType(token.substring(7));

			CompanyModel companyModel = companyService.getById(companyId);
			CompanyUserModel companyUserModel = companyUserService.getById(userId);
			if (companyModel.getCmpId() != companyUserModel.getCmpUsrCmpId().getCmpId()) {
				LOGGER.error("User id: " + userId + " not exist in this company.");
				throw new Exception("User id: " + userId + " not exist in this company");
			}
			if (usersEnum.equals(UsersEnum.CompanyUser)) {
				CompanyUserModel companyUserModelFromToken = authenticationService.getCompanyUserFromToken(token.substring(7));
				if (userId == companyUserModelFromToken.getCmpUsrId()) {
					throw new Exception("Ciso user cannot delete himself!");
				}
			}
			companyUserService.delete(userId);
			LOGGER.info("Delete user ,id:" + userId + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, null, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Delete user , id: " + userId + " failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("/{companyId}/user")
	public ResponseEntity<ResponseModel> getUsersCompany(
		@PathVariable(value = "companyId") int companyId,
		@RequestParam(defaultValue = "0") Integer pageNo,
		@RequestParam(defaultValue = "10") Integer pageSize,
		@RequestParam(defaultValue = "cmpUsrFullName") String orderBy,
		@RequestParam(defaultValue = "false") boolean orderByDesc,
		@RequestParam(defaultValue = "") String query,
		@RequestHeader(name = "Authorization") String token) {
		try {
			Page<CompanyUserModel> companyUsers = companyUserService.getUsersComapny(pageNo, pageSize, orderBy, orderByDesc, query, companyId);
			Map<String, Object> map = companyUserService.getAllResponse(companyUsers);
			LOGGER.info("get all users company success.");
			return ResponseEntity.ok().body(new ResponseModel(1, map, null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get all users company failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("/{companyId}/user/{userId}")
	public ResponseEntity<ResponseModel> getUserCompany(@PathVariable(value = "companyId") int companyId, @PathVariable(value = "userId") int userId, @RequestHeader(name = "Authorization") String token) {
		try {
			// open this route to be a public without handle permission
			//handlePermission(token, OperationEnum.READ, ModuleEnum.VENDOR);
			CompanyUserModel companyUser = companyUserService.getById(userId);
			LOGGER.info("get user company success.");
			return ResponseEntity.ok().body(new ResponseModel(1, companyUserService.getSingleInstance(companyUser), null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get user company failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@PutMapping("details/{companyId}")
	public ResponseEntity<ResponseModel> updateCompanyDetails(@PathVariable(value = "companyId") int companyId, @RequestHeader(name = "Authorization") String token, @RequestBody ObjectNode objectNode) throws Exception {
		try {
			handlePermission(token, OperationEnum.UPDATE, ModuleEnum.AUTH);
			CompanyModel companyModel = companyService.updateCompanyDetails(objectNode, companyId);
			LOGGER.info("update company success.");
			return ResponseEntity.ok().body(new ResponseModel(1, companyService.getSingleInstance(companyModel), null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get user company failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@PutMapping("/{companyId}/userDetails/{userId}")
	public ResponseEntity<ResponseModel> updateUserDetails
		(@PathVariable(value = "companyId") int companyId,
		 @PathVariable(value = "userId") int userId,
		 @RequestBody CompanyUserModel companyUserModel,
		 @RequestHeader(name = "Authorization") String token) throws Exception {
		try {
			CompanyUserModel updateCompanyUserModel = companyUserService.updateUserDetails(userId, companyUserModel);
			LOGGER.info("Update user ,id: " + userId + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, companyUserService.getSingleInstance(updateCompanyUserModel), null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Update user id: " + userId + " failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@Override // override this function because we need to open this route and disable the handler permission
	@GetMapping("/{id}")
	public ResponseEntity<ResponseModel> getRecordById(@PathVariable(value = "id") int id, @RequestHeader(name = "Authorization") String token) throws Exception {
		try {
			CompanyModel companyModel = companyService.getById(id);
			LOGGER.info("get company by id: " + id + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, companyModel, null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (NotFoundException ex) {
			LOGGER.error("get by id: " + id + " failed , exception: " + ex);
			return ResponseEntity.badRequest().body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get by id: " + id + " failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@PutMapping("/uploadCmpLogoFile/{cmpId}")
	public ResponseEntity<ResponseModel> uploadCmpLogoFile(@PathVariable(value = "cmpId") int cmpId,
														   @RequestParam(value = "file", required = false) MultipartFile file,
														   @RequestHeader(name = "Authorization") String token) throws Exception {
		try {
			handlePermission(token, OperationEnum.UPDATE, ModuleEnum.AUTH);

			CompanyModel updatedCompanyModel = companyService.storeCmpLogoFile(file, cmpId);
			LOGGER.info("Update Logo file to company id: " + cmpId + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, companyService.getSingleInstance(updatedCompanyModel), null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (NotFoundException ex) {
			LOGGER.error("Update Logo file to company id: " + cmpId + " failed , exception: " + ex);
			return ResponseEntity.badRequest().body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Update Logo file to company id: " + cmpId + " failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("/downloadCmpLogoFile/{cmpId}")
	public ResponseEntity<ResponseModel> downloadFile(@PathVariable(value = "cmpId") int cmpId) throws Exception {
		try {
			CompanyModel companyModel = companyService.getById(cmpId);
			if (companyModel.getCmpLogoFile() == null) {
				LOGGER.info("Not found Logo file to company id:" + cmpId);
				return ResponseEntity.ok().body(new ResponseModel(1, "", null));
			}
			LOGGER.info("Get logo file to company id: " + cmpId + " success.");
			// convert logo file to base-64
			byte[] encode = java.util.Base64.getEncoder().encode(companyModel.getCmpLogoFile());
			String logoFileBase64 = new String(encode, StandardCharsets.UTF_8);
			return ResponseEntity.ok().body(new ResponseModel(1, logoFileBase64, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Get logo file to company id: " + cmpId + " failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@PutMapping("/{cmpId}/user/uploadCmpUserImgFile/{cmpUserId}")
	public ResponseEntity<ResponseModel> uploadCmpLogoFile(@PathVariable(value = "cmpId") int cmpId,
														   @PathVariable(value = "cmpUserId") int cmpUserId,
														   @RequestParam(value = "file", required = false) MultipartFile file,
														   @RequestHeader(name = "Authorization") String token) throws Exception {
		try {
			CompanyUserModel companyUserModelFromToken = authenticationService.getCompanyUserFromToken(token.substring(7));
			if (cmpUserId != companyUserModelFromToken.getCmpUsrId()) {
				throw new Exception("You cannot edit other's users profile pictures");
			}

			CompanyUserModel updatedCompanyUserModel = companyUserService.storeCmpUsrLogoFile(file, cmpUserId);
			LOGGER.info("Update Logo file to company id: " + cmpId + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, companyUserService.getSingleInstance(updatedCompanyUserModel), null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (NotFoundException ex) {
			LOGGER.error("Update profile image file to company user id: " + cmpUserId + " failed , exception: " + ex);
			return ResponseEntity.badRequest().body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Update profile image file to company user id: " + cmpUserId + " failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("/{cmpId}/user/downloadCmpUserImgFile/{cmpUserId}")
	public ResponseEntity<ResponseModel> downloadProfileImgFile(@PathVariable(value = "cmpUserId") int cmpUserId) throws Exception {
		try {
			CompanyUserModel companyUserModel = companyUserService.getById(cmpUserId);
			if (companyUserModel.getCmpUsrProfileImgFile() == null) {
				LOGGER.info("Not found profile img file to company user id:" + cmpUserId);
				return ResponseEntity.ok().body(new ResponseModel(1, "", null));
			}
			LOGGER.info("Get profile image file to company user id: " + cmpUserId + " success.");
			// convert logo file to base-64
			byte[] encode = java.util.Base64.getEncoder().encode(companyUserModel.getCmpUsrProfileImgFile());
			String imgFileBase64 = new String(encode, StandardCharsets.UTF_8);
			return ResponseEntity.ok().body(new ResponseModel(1, imgFileBase64, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Get image file to company user id: " + cmpUserId + " failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}
}
