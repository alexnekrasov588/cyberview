package com.lstechs.ciso.controller;

import com.lstechs.ciso.enums.ModuleEnum;
import com.lstechs.ciso.model.NewsTagsModel;
import com.lstechs.ciso.model.SingleNewsTagsModel;
import com.lstechs.ciso.service.NewsTagsService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/newsTags")
public class NewsTagsController extends CRUDController<NewsTagsModel, SingleNewsTagsModel> {
    public NewsTagsController(NewsTagsService newsTagsService) {
        super(newsTagsService, ModuleEnum.NEWS);
    }
}
