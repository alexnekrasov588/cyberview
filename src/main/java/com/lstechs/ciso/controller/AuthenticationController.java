package com.lstechs.ciso.controller;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.lstechs.ciso.enums.ModuleEnum;
import com.lstechs.ciso.enums.OperationEnum;
import com.lstechs.ciso.model.*;
import com.lstechs.ciso.service.AuthenticationService;
import com.lstechs.ciso.service.LicenseService;
import com.lstechs.ciso.service.NewsArticlesService;
import com.lstechs.ciso.service.ResetTokensService;
import com.lstechs.ciso.service.RoleService;
import javassist.NotFoundException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.naming.AuthenticationException;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/auth")
@CrossOrigin
public class AuthenticationController {

	@Autowired
	private AuthenticationService authenticationService;

	@Autowired
	private RoleService roleService;

	@Autowired
	private LicenseService licenseService;

	@Autowired
	private NewsArticlesService newsArticlesService;

	private static final Logger LOGGER = LogManager.getLogger(AuthenticationController.class.getName());

	@PostMapping("/login")
	public ResponseEntity<ResponseModel> login(@RequestBody AuthRequestModel authenticationRequest) throws Exception {
		try {

			AuthResponseModel authResponseModel = authenticationService.getToken(authenticationRequest, false);
			LOGGER.info("Login user :" + authenticationRequest.getUsername() + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, authResponseModel, null));

		} catch (
			NotFoundException ex) {
			LOGGER.error("login user :" + authenticationRequest.getUsername() + " failed ,exception: " + ex);
			return ResponseEntity.badRequest().body(new ResponseModel(0, null, ex.getMessage()));
		} catch (AuthenticationException ex) {
			LOGGER.error("login user :" + authenticationRequest.getUsername() + " failed ,exception: " + ex);
			throw new Exception("Incorrect username or password", ex);
		} catch (Exception ex) {
			LOGGER.error("login user :" + authenticationRequest.getUsername() + " failed ,exception: " + ex);
			if (ex.getMessage().contains("javax.naming.AuthenticationException: [LDAP: error code 49 - 80090308: LdapErr: DSID-0C0903D3, comment: AcceptSecurityContext error, data 52e, v3839\u0000]"))
				throw new Exception("Incorrect username or password", ex);
			else
				return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@PostMapping("/login/vendor")
	public ResponseEntity<ResponseModel> vendorLogin(@RequestBody AuthRequestModel authenticationRequest) throws Exception {
		try {
			AuthResponseModel authResponseModel = authenticationService.getToken(authenticationRequest, true);
			LOGGER.info("Login to vendor user :" + authenticationRequest.getUsername() + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, authResponseModel, null));
		} catch (
			NotFoundException ex) {
			LOGGER.error("Login to vendor user :" + authenticationRequest.getUsername() + " failed ,exception: " + ex);
			return ResponseEntity.badRequest().body(new ResponseModel(0, null, ex.getMessage()));
		} catch (AuthenticationException ex) {
			LOGGER.error("Login to vendor user :" + authenticationRequest.getUsername() + " failed ,exception: " + ex);
			throw new Exception("Incorrect Vendor username or password", ex);
		} catch (Exception ex) {
			LOGGER.error("Login to vendor user :" + authenticationRequest.getUsername() + " failed ,exception: " + ex);
			if (ex.getMessage().contains("javax.naming.AuthenticationException: [LDAP: error code 49 - 80090308: LdapErr: DSID-0C0903D3, comment: AcceptSecurityContext error, data 52e, v3839\u0000]"))
				throw new Exception("Incorrect Vendor username or password", ex);
			else
				return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@PostMapping("/isResetTokenValid")
	public ResponseEntity<ResponseModel> resetPasswordIfTokenIsValid(@RequestBody ObjectNode objectNode) throws Exception {
		String token = objectNode.get("token").asText();
		try {
			ResetTokensModel resetTokensModel = authenticationService.getResetToken(token);

			if (resetTokensModel == null) {
				return ResponseEntity.ok().body(new ResponseModel(0, false, null));
			}
			LOGGER.info("The reset token" + token + " valid.");
			return ResponseEntity.ok().body(new ResponseModel(1, true, null));
		} catch (Exception ex) {
			LOGGER.error("Get is reset token valid to token =" + token + ", failed");
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@PostMapping("/changePassword/{id}")
	public ResponseEntity<ResponseModel> changePassword(@PathVariable(value = "id") int id, @RequestBody ObjectNode objectNode) throws Exception {
		try {
			authenticationService.changePassword(objectNode, id);
			LOGGER.info("reset password user id :" + id + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, null, null));

		} catch (Exception ex) {
			LOGGER.error("reset password user id :" + id + " failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@PostMapping("/forgotPasswordRequest")
	public ResponseEntity<ResponseModel> forgotPasswordRequest(@RequestBody ObjectNode objectNode) throws Exception {
		String email = objectNode.get("email").asText();
		try {
			authenticationService.forgotPasswordRequest(email, false);
			LOGGER.info("Forgot password request mail:" + email + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, null, null));
		} catch (Exception ex) {
			LOGGER.error("Forgot password request mail :" + email + " failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@PostMapping("/forgotPasswordRequest/vendor")
	public ResponseEntity<ResponseModel> forgotVendorPasswordRequest(@RequestBody ObjectNode objectNode) throws Exception {
		String email = objectNode.get("email").asText();
		try {
			authenticationService.forgotPasswordRequest(email, true);
			LOGGER.info("Forgot vendor password request mail:" + email + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, null, null));
		} catch (Exception ex) {
			LOGGER.error("Forgot vendor password request mail :" + email + " failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@PostMapping("/resetPassword")
	public ResponseEntity<ResponseModel> resetPassword(@RequestBody ObjectNode objectNode) throws Exception {
		String resetToken = objectNode.get("resetToken").asText();
		String newPassword = objectNode.get("newPassword").asText();
		try {
			authenticationService.resetPassword(resetToken, newPassword);
			LOGGER.info("reset password token:" + resetToken + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, null, null));
		} catch (Exception ex) {
			LOGGER.error("reset password token: " + resetToken + " failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@PostMapping("/role")
	public ResponseEntity<ResponseModel> createRole(@RequestBody RoleModel roleModel, @RequestHeader(name = "Authorization") String token) throws Exception {
		try {
			roleService.handlePermission(token, OperationEnum.CREATE, ModuleEnum.AUTH);
			RoleModel newRoleModel = roleService.create(roleModel);
			LOGGER.info("create new role id:" + newRoleModel.getRoleId() + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, roleService.getSingleInstance(newRoleModel), null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("create new role failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@PutMapping("/role/{roleId}")
	public ResponseEntity<ResponseModel> updateRole(@PathVariable int roleId, @RequestBody RoleModel roleModel, @RequestHeader(name = "Authorization") String token) throws Exception {
		try {
			roleService.handlePermission(token, OperationEnum.UPDATE, ModuleEnum.AUTH);
			RoleModel updateRoleModel = roleService.update(roleId, roleModel);
			LOGGER.info("update role id:" + updateRoleModel.getRoleId() + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, roleService.getSingleInstance(updateRoleModel), null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("create new role failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@DeleteMapping("/role/{roleId}")
	public ResponseEntity<ResponseModel> deleteRole(@PathVariable int roleId, @RequestHeader(name = "Authorization") String token) throws Exception {
		try {
			roleService.handlePermission(token, OperationEnum.DELETE, ModuleEnum.AUTH);
			RoleModel roleModel = roleService.getById(roleId);
			if (roleModel.isEditable()) {
				if (roleModel.getCompanyUserModel() != null && roleModel.getCompanyUserModel().size() > 0) {
					LOGGER.error("Cannot delete role id:" + roleModel.getRoleId() + " - you cannot delete role that is being currently in use.");
					return ResponseEntity.status(500).body(new ResponseModel(0, null, "Cannot delete role that is being currently in use."));
				}
				roleModel = roleService.delete(roleId);
				LOGGER.info("Delete role id:" + roleModel.getRoleId() + " success.");
				return ResponseEntity.ok().body(new ResponseModel(1, roleService.getSingleInstance(roleModel), null));
			} else {
				LOGGER.error("Cannot delete role id:" + roleModel.getRoleId() + " - not editable role.");
				return ResponseEntity.ok().body(new ResponseModel(0, null, "Cannot delete default role"));
			}
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Delete role id:" + roleId + " failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("/roles")
	public ResponseEntity<ResponseModel> getRoles(
		@RequestHeader(name = "Authorization") String token,
		@RequestParam(defaultValue = "0") Integer pageNo,
		@RequestParam(defaultValue = "10") Integer pageSize,
		@RequestParam(defaultValue = "roleName") String orderBy,
		@RequestParam(defaultValue = "false") boolean orderByDesc,
		@RequestParam(defaultValue = "") String query,
		@RequestParam(defaultValue = "0") Integer cmpId) {
		try {
			roleService.handlePermission(token, OperationEnum.READ, ModuleEnum.AUTH);
			Page<RoleModel> roleModels = roleService.getAllRoles(pageNo, pageSize, orderBy, orderByDesc, query, token.substring(7), cmpId);
			Map<String, Object> map = roleService.getAllResponse(roleModels);
			LOGGER.info("get all roles success.");
			return ResponseEntity.ok().body(new ResponseModel(1, map, null));

		} catch (SecurityException ex) {
			LOGGER.error("get all roles failed , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get all roles failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@PostMapping("/role/{roleId}/duplicate")
	public ResponseEntity<ResponseModel> duplicateRole(@PathVariable int roleId, @RequestHeader(name = "Authorization") String token) throws Exception {
		try {
			roleService.handlePermission(token, OperationEnum.CREATE, ModuleEnum.AUTH);
			RoleModel roleModel = roleService.duplicateRole(roleId);
			LOGGER.info("duplicate role  id:" + roleId + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, roleService.getSingleInstance(roleModel), null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("duplicate role id: " + roleId + " failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("/isOnPrem")
	public ResponseEntity<ResponseModel> isOnPrem(@RequestHeader(name = "Authorization") String token) {
		try {

			Map<String, Object> map = new HashMap<String, Object>();
			map.put("isOnPrem", authenticationService.isOnPrem());
			map.put("isLicenseExist", licenseService.isLicenseExist());
			LOGGER.info("is on prem");
			return ResponseEntity.ok().body(new ResponseModel(1, map, null));

		} catch (SecurityException ex) {
			LOGGER.error("is onPrem failed , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("is onPrem failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

}
