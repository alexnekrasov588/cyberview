package com.lstechs.ciso.controller;

import com.lstechs.ciso.enums.DashboardEnum;
import com.lstechs.ciso.model.*;
import com.lstechs.ciso.service.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/dashboard")
public class DashboardController {

	@Autowired
	private DashboardService dashboardService;

	@Autowired
	private AuthenticationService authenticationService;

	private static final Logger LOGGER = LogManager.getLogger(ProjectController.class.getName());

	@GetMapping
	public ResponseEntity<ResponseModel> getDashboardContent(@RequestHeader(name = "Authorization") String token) {
		try {
			Map<String, List<Map<String, Object>>> map = new HashMap<>();

			// ---------STANDARDS----------
			// get list of standards details
			CompanyModel companyModel = authenticationService.getCompanyFromToken(token.substring(7));
			String lcnsToken = companyModel.getCmpLcnsId().getLcnsToken();
			List<Map<String, Object>> listMapStandardsDetails = dashboardService.getListMapStandardsDetails(lcnsToken, companyModel.getCmpId(), token);
			map.put("standards", listMapStandardsDetails);

			// ---------PROJECTS----------COMPLETED
			// get project info by company user
			List<Map<String, Object>> listMapProjectsDetails = dashboardService.getListMapProjectsDetails(token, DashboardEnum.PROJECTS_DETAILS);
			map.put("projects", listMapProjectsDetails);

			// ---------PROJECTS----------COMPLETED
			// get project info by company user
			List<Map<String, Object>> listMapTasksDetails = dashboardService.getListMapProjectsDetails(token, DashboardEnum.TASKS_DETAILS);
			map.put("tasks", listMapTasksDetails);

			List<Map<String, Object>> newsMap = dashboardService.getNews(companyModel.getCmpId(), token);
			map.put("news", newsMap);

			LOGGER.info("Get all dashboard content success");
			return ResponseEntity.ok().body(new ResponseModel(1, map, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , Exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Get all avail active company users count and all avail tasks count by this user failed, Exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

}


