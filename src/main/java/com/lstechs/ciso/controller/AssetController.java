package com.lstechs.ciso.controller;

import java.util.List;
import java.util.Map;

import com.lstechs.ciso.model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import com.lstechs.ciso.enums.ModuleEnum;
import com.lstechs.ciso.enums.OperationEnum;

import com.lstechs.ciso.service.AssetService;
import com.lstechs.ciso.service.AuthenticationService;

@RestController
@RequestMapping("/asset")
public class AssetController extends CRUDController<AssetModel, SingleAssetModel> {

	@Autowired
	public AuthenticationService authenticationService;

	@Autowired
	public AssetService assetService;

	private static final Logger LOGGER = LogManager.getLogger(AssetController.class.getName());

	public AssetController(AssetService assetService) {
		super(assetService, ModuleEnum.PROJECT);
	}

	@PutMapping("/upload")
	public ResponseEntity<ResponseModel> uploadAsset(@RequestParam("file") MultipartFile file,
													 @RequestParam(defaultValue = "") String type,
													 @RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.UPDATE, ModuleEnum.PROJECT);
			CompanyModel companyModel = authenticationService.getCompanyFromToken(token.substring(7));
			if (file == null) {
				return ResponseEntity.status(500).body(new ResponseModel(0, null, "File not found."));
			}
			List<AssetModel> assetModels = assetService.createAssetsFromExcelFile(file, companyModel, type);
			if (assetModels == null || assetModels.size() == 0) {
				return ResponseEntity.status(500).body(new ResponseModel(0, null, "Cannot read file."));
			}
			LOGGER.info("upload asset file  success.");
			return ResponseEntity.ok().body(new ResponseModel(1, assetModels, null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("upload asset file failed  , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@Override
	@GetMapping
	public ResponseEntity<ResponseModel> getAllRecords(@RequestParam(defaultValue = "0") Integer pageNo,
													   @RequestParam(defaultValue = "10") Integer pageSize,
													   @RequestParam(defaultValue = "astId") String orderBy,
													   @RequestParam(defaultValue = "true") boolean orderByDesc,
													   @RequestParam(defaultValue = "") String query,
													   @RequestParam Map<String, String> allParams,
													   @RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.PROJECT);
			CompanyModel companyModel = authenticationService.getCompanyFromToken(token.substring(7));
			Page<AssetModel> assetList = assetService.getAllAssetByCmpId(pageNo, pageSize, orderBy, orderByDesc, query, companyModel.getCmpId(), allParams);
			Map<String, Object> map = assetService.getAllResponse(assetList);
			LOGGER.info("Get all asset success.");
			return ResponseEntity.ok().body(new ResponseModel(1, map, null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Get all from asset failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("/getAllAssetForNews")
	public ResponseEntity<ResponseModel> getAllAssetForNews(@RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.PROJECT);
			CompanyModel companyModel = authenticationService.getCompanyFromToken(token.substring(7));
			List<FilterModel> filterModel = assetService.getAllAssetForNews(companyModel.getCmpId());
			LOGGER.info("get  filter asset  success.");
			return ResponseEntity.ok().body(new ResponseModel(1, filterModel, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get  filter asset  failed , exception" + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("/getAllAssetName")
	public ResponseEntity<ResponseModel> getAllAssetName(@RequestHeader(name = "Authorization") String token
		, @RequestParam(defaultValue = "") String type) {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.PROJECT);
			CompanyModel companyModel = authenticationService.getCompanyFromToken(token.substring(7));
			List<String> filterModel = assetService.getAllAssetName(companyModel.getCmpId(), type);
			LOGGER.info("get  filter asset  success.");
			return ResponseEntity.ok().body(new ResponseModel(1, filterModel, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get  filter asset  failed , exception" + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("/getAllAssetVersion")
	public ResponseEntity<ResponseModel> getAllAssetVersion(@RequestHeader(name = "Authorization") String token,
															@RequestParam(defaultValue = "") String type) {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.PROJECT);
			CompanyModel companyModel = authenticationService.getCompanyFromToken(token.substring(7));
			List<String> filterModel = assetService.getAllAssetVersion(companyModel.getCmpId(), type);
			LOGGER.info("get  filter asset  success.");
			return ResponseEntity.ok().body(new ResponseModel(1, filterModel, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get  filter asset  failed , exception" + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("/getAllAssetModel")
	public ResponseEntity<ResponseModel> getAllAssetModel(@RequestHeader(name = "Authorization") String token,
														  @RequestParam(defaultValue = "") String type) {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.PROJECT);
			CompanyModel companyModel = authenticationService.getCompanyFromToken(token.substring(7));
			List<String> filterModel = assetService.getAllAssetModel(companyModel.getCmpId(), type);
			LOGGER.info("get  filter asset  success.");
			return ResponseEntity.ok().body(new ResponseModel(1, filterModel, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get  filter asset  failed , exception" + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("/getAllAssetProduct")
	public ResponseEntity<ResponseModel> getAllAssetProduct(@RequestHeader(name = "Authorization") String token,
															@RequestParam(defaultValue = "") String type) {

		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.PROJECT);
			CompanyModel companyModel = authenticationService.getCompanyFromToken(token.substring(7));
			List<String> filterModel = assetService.getAllAssetProduct(companyModel.getCmpId(), type);
			LOGGER.info("get  filter asset  success.");
			return ResponseEntity.ok().body(new ResponseModel(1, filterModel, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get  filter asset  failed , exception" + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("/getAllAssetManufactor")
	public ResponseEntity<ResponseModel> getAllAssetManufactor(@RequestHeader(name = "Authorization") String token,
															   @RequestParam(defaultValue = "") String type) {

		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.PROJECT);
			CompanyModel companyModel = authenticationService.getCompanyFromToken(token.substring(7));
			List<String> filterModel = assetService.getAllAssetManufactor(companyModel.getCmpId(), type);
			LOGGER.info("get  filter asset  success.");
			return ResponseEntity.ok().body(new ResponseModel(1, filterModel, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get  filter asset  failed , exception" + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("/getAllAssetOperator")
	public ResponseEntity<ResponseModel> getAllAssetOperator(@RequestHeader(name = "Authorization") String token,
															 @RequestParam(defaultValue = "") String type) {

		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.PROJECT);
			CompanyModel companyModel = authenticationService.getCompanyFromToken(token.substring(7));
			List<String> filterModel = assetService.getAllAssetOperator(companyModel.getCmpId(), type);
			LOGGER.info("get  filter asset  success.");
			return ResponseEntity.ok().body(new ResponseModel(1, filterModel, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get  filter asset  failed , exception" + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("/getAllAssetOwner")
	public ResponseEntity<ResponseModel> getAllAssetOwner(@RequestHeader(name = "Authorization") String token,
														  @RequestParam(defaultValue = "") String type) {

		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.PROJECT);
			CompanyModel companyModel = authenticationService.getCompanyFromToken(token.substring(7));
			List<String> filterModel = assetService.getAllAssetOwner(companyModel.getCmpId(), type);
			LOGGER.info("get  filter asset  success.");
			return ResponseEntity.ok().body(new ResponseModel(1, filterModel, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get  filter asset  failed , exception" + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("/getAllAssetType")
	public ResponseEntity<ResponseModel> getAllAssetType(@RequestHeader(name = "Authorization") String token,
														 @RequestParam(defaultValue = "") String type) {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.PROJECT);
			CompanyModel companyModel = authenticationService.getCompanyFromToken(token.substring(7));
			List<String> filterModel = assetService.getAllAssetType(companyModel.getCmpId(), type);
			LOGGER.info("get  filter asset  success.");
			return ResponseEntity.ok().body(new ResponseModel(1, filterModel, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get  filter asset  failed , exception" + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@DeleteMapping("/deleteAssets")
	public ResponseEntity<ResponseModel> deleteAssets(@RequestParam(name = "assetIds") int[] assetIds,
													@RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.DELETE, ModuleEnum.ASSET_MANAGEMENT);
			List<AssetModel> deletedAssets = assetService.deleteAssets(assetIds);
			LOGGER.info("Assets Deleted Successfully.");
			return ResponseEntity.ok().body(new ResponseModel(1, deletedAssets, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Delete assets  failed , exception" + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}
}
