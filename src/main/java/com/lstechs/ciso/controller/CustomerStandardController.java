package com.lstechs.ciso.controller;

import com.lstechs.ciso.enums.ModuleEnum;
import com.lstechs.ciso.enums.OperationEnum;
import com.lstechs.ciso.model.*;
import com.lstechs.ciso.service.CustomerStandardService;
import com.lstechs.ciso.service.FilesService;
import javassist.NotFoundException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/customerStandard")
public class CustomerStandardController extends CRUDController<CustomerStandardModel, SingleCustomerStandardModel> {
	public CustomerStandardController(CustomerStandardService customerStandardService) {
		super(customerStandardService, ModuleEnum.CUSTOMER_STANDARD);
	}

	private static final Logger LOGGER = LogManager.getLogger(CustomerStandardController.class.getName());

	@Value("${server.url}")
	private String url;

	@Autowired
	private CustomerStandardService customerStandardService;

	@Autowired
	private FilesService filesService;

	@PutMapping("/upload/{id}")
	public ResponseEntity<ResponseModel> updateRecord(@PathVariable(value = "id") int id,
													  @RequestParam("file") MultipartFile file,
													  @RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.UPDATE, ModuleEnum.CUSTOMER_STANDARD);
			if (file == null) {
				return ResponseEntity.status(500).body(new ResponseModel(0, null, "File not found."));
			}
			CustomerStandardModel customerStandardModel = customerStandardService.getById(id);
			FilesModel filesModel = filesService.storeFile(file, customerStandardModel, null);
			String fileDownloadUri = "//" + url + "/api/customerStandard/downloadFile/" + filesModel.getId();
			filesService.updateFileUrl(filesModel.getId(), fileDownloadUri);

			LOGGER.info("update  customer standard id: " + id + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, new UploadFileResponseModel(filesModel.getFileName(), fileDownloadUri, file.getContentType(), file.getSize()), null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (NotFoundException ex) {
			LOGGER.error("update  customer standard id: " + id + " failed , exception: " + ex);
			return ResponseEntity.badRequest().body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("update  customer standard id: " + id + " failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("/downloadFile/{fileId}")
	public ResponseEntity<Resource> downloadFile(@PathVariable String fileId) throws NotFoundException {
		// Load file from database
		FilesModel dbFile = filesService.getFile(fileId);

		return ResponseEntity.ok()
			.contentType(MediaType.parseMediaType(dbFile.getFileType()))
			.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + dbFile.getFileName() + "\"")
			.body(new ByteArrayResource(dbFile.getData()));
	}

	@DeleteMapping("{fileId}/file")
	public ResponseEntity<ResponseModel> deleteFile(@PathVariable(value = "fileId") String fileId,
													@RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.DELETE, ModuleEnum.CUSTOMER_STANDARD);
			FilesModel filesModel = filesService.delete(fileId);
			LOGGER.info("Delete file ,id:" + filesModel.getId() + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, filesModel, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Delete file failed, exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}
}
