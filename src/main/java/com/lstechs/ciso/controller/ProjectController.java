package com.lstechs.ciso.controller;

import com.lstechs.ciso.enums.ModuleEnum;
import com.lstechs.ciso.enums.OperationEnum;
import com.lstechs.ciso.enums.PrjStatusEnum;
import com.lstechs.ciso.enums.StatusEnum;
import com.lstechs.ciso.model.*;
import com.lstechs.ciso.service.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.ByteArrayInputStream;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/projects")
public class ProjectController extends CRUDController<ProjectModel, SingleProjectModel> {

	@Autowired
	public TaskService taskService;

	@Autowired
	public ProjectService projectService;

	@Autowired
	public CompanyUserService companyUserService;

	@Autowired
	public TaskCommentService taskCommentService;

	@Autowired
	public ModuleTaskService moduleTaskService;

	@Autowired
	public UserProjectService userProjectService;

	@Autowired
	public AuthenticationService authenticationService;

	@Autowired
	public RoleService roleService;

	@Autowired
	public RecurringTaskService recurringTaskService;

	@Autowired
	public RecurringProjectService recurringProjectService;

	@Autowired
	public VulnerabilityAssessmentsService vulnerabilityAssessmentsService;

	private static final Logger LOGGER = LogManager.getLogger(ProjectController.class.getName());

	public ProjectController(ProjectService projectService) {
		super(projectService, ModuleEnum.PROJECT);
	}

	@GetMapping("/getProjectsName")
	public ResponseEntity<ResponseModel> getProjectsName(@RequestHeader(name = "Authorization") String token) {

		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.PROJECT);
			List<ProjectsNameModel> projectsName = projectService.getAllProjectsName(token);
			LOGGER.info("get  all projects name  success.");
			return ResponseEntity.ok().body(new ResponseModel(1, projectsName, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get  all projects name  failed , exception" + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("/getProjectsManager")
	public ResponseEntity<ResponseModel> getProjectsManager(@RequestHeader(name = "Authorization") String token) {

		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.PROJECT);
			List<String> projectsManager = projectService.getAllProjectsManager(token);
			LOGGER.info("get  all projects manager  success.");
			return ResponseEntity.ok().body(new ResponseModel(1, projectsManager, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get  all projects manager  failed , exception" + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@Override
	@GetMapping
	public ResponseEntity<ResponseModel> getAllRecords(@RequestParam(defaultValue = "0") Integer pageNo,
													   @RequestParam(defaultValue = "10") Integer pageSize,
													   @RequestParam(defaultValue = "prjId") String orderBy,
													   @RequestParam(defaultValue = "true") boolean orderByDesc,
													   @RequestParam(defaultValue = "") String query,
													   @RequestParam Map<String, String> allParams,
													   @RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.PROJECT);
			Page<ProjectModel> projectModelPage;

			CompanyUserModel companyUserModel = authenticationService.getCompanyUserFromToken(token.substring(7));
			RoleModel roleModel = roleService.getRoleModelByCmpUsrId(companyUserModel.getCmpUsrId());
			String userRoleName = roleModel.getRoleName();

			boolean isTeamMember = (userRoleName.equals("Team Member")) || (userRoleName.equals("Outsource"));
			int cmpId = companyUserModel.getCmpUsrCmpId().getCmpId();
			int cmpUsrId = companyUserModel.getCmpUsrId();

			if (!query.isEmpty()) {
				query = query.toLowerCase();
			}

			projectModelPage = projectService.getFilterProjects(pageNo, pageSize, orderBy, orderByDesc, cmpId, cmpUsrId, isTeamMember, allParams, query);
			Map<String, Object> map = projectService.getAllResponse(projectModelPage);
			LOGGER.info("Get all projects success.");
			return ResponseEntity.ok().body(new ResponseModel(1, map, null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Get all from project failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@PostMapping("/{projectId}/task")
	public ResponseEntity<ResponseModel> createTask
		(@PathVariable(value = "projectId") int projectId, @Validated(CRUDModel.CRUDCreation.class) @RequestBody TaskModel taskModel, @RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.CREATE, ModuleEnum.TASK);
			ProjectModel projectModel = projectService.getById(projectId);
			taskModel.setTskPrjID(projectModel);
			CompanyUserModel companyUserModel = companyUserService.getById(taskModel.getTskAssignee().getCmpUsrId());
			taskModel.setTskAssignee(companyUserModel);
			TaskModel newTaskModel = taskService.create(taskModel);
			LOGGER.info("Create new Task ,id:" + newTaskModel.getTskId() + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, taskService.getSingleInstance(newTaskModel), null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Create new  Task failed ,exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@DeleteMapping("/{projectId}/task")
	public ResponseEntity<ResponseModel> deleteTask(@PathVariable(value = "projectId") int projectId,
													@RequestParam(defaultValue = "taskIds") int[] taskIds,
													@RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.DELETE, ModuleEnum.TASK);

			CompanyUserModel companyUserModel = authenticationService.getCompanyUserFromToken(token.substring(7));
			RoleModel roleModel = roleService.getRoleModelByCmpUsrId(companyUserModel.getCmpUsrId());
			String userRoleName = roleModel.getRoleName();
			boolean checkDeleted = false;

			if (userRoleName.equals("Team Member")) {
				// A Team Member can only delete tasks which he assigned with
				for (int taskId : taskIds) {
					if (!taskService.isTaskAssignToCmpUsrID(companyUserModel.getCmpUsrId(), taskId))
						checkDeleted = true;
					else
						taskService.delete(taskId);
				}
			} else {
				taskService.deleteTasks(taskIds);
			}

			if (taskIds.length > 0) {
				LOGGER.info("Delete Tasks from project id:" + projectId + " success.");
			} else {
				LOGGER.info("Delete Tasks from project id:" + projectId + " task ids empty.");
			}
			return ResponseEntity.ok().body(new ResponseModel(checkDeleted ? 0 : 1, null, checkDeleted ? "Some tasks were not deleted because they didn't assigned to the current user" : null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Delete Tasks from project id: " + projectId + " failed, exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@PutMapping("/{projectId}/task/{taskId}")
	public ResponseEntity<ResponseModel> updateTask(@PathVariable(value = "projectId") int projectId,
													@PathVariable(value = "taskId") int taskId,
													@Validated(CRUDModel.CRUDUpdate.class) @RequestBody TaskModel taskModel,
													@RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.UPDATE, ModuleEnum.TASK);
			ProjectModel project = projectService.getById(projectId);
			taskService.getById(taskId);
			TaskModel updateTaskModle = taskService.updateTask(taskId, project, taskModel);
			LOGGER.info("Update task ,id: " + taskId + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, taskService.getSingleInstance(updateTaskModle), null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Update task id: " + taskId + " failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("/{projectId}/task")
	public ResponseEntity<ResponseModel> getAllTasks(@RequestParam(defaultValue = "0") Integer pageNo,
													 @RequestParam(defaultValue = "10") Integer pageSize,
													 @RequestParam(defaultValue = "id") String orderBy,
													 @RequestParam(defaultValue = "true") boolean orderByDesc,
													 @RequestParam(defaultValue = "") String name,
													 @RequestParam(defaultValue = "0") Integer assignee,
													 @PathVariable Integer projectId,
													 @RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.TASK);
			Page<TaskModel> taskModels = taskService.getAllTasks(pageNo, pageSize, orderBy, orderByDesc, name, assignee, projectId);
			Map<String, Object> map = taskService.getAllResponse(taskModels);
			LOGGER.info("get all task with project id: " + projectId + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, map, null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get all task with project id: " + projectId + " failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@PostMapping("/{projectId}/task/{taskId}/comment")
	public ResponseEntity<ResponseModel> createTaskComment
		(@PathVariable(value = "projectId") int projectId, @PathVariable(value = "taskId") int taskId, @Validated(CRUDModel.CRUDCreation.class) @RequestBody TaskCommentModel taskCommentModel,
		 @RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.CREATE, ModuleEnum.TASK);
			TaskModel taskModel = taskService.getById(taskId);
			taskCommentModel.setCmntTskId(taskModel);
			CompanyUserModel companyUserModel = authenticationService.getCompanyUserFromToken(token.substring(7));
			taskCommentModel.setUsrId(companyUserModel);
			TaskCommentModel newTaskModel = taskCommentService.create(taskCommentModel);
			LOGGER.info("Create new Task comment ,id:" + newTaskModel.getCmntId() + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, newTaskModel, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Create new  Task  failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@DeleteMapping("/{projectId}/task/{taskId}/comment/{cmmtId}")
	public ResponseEntity<ResponseModel> deleteTaskComment
		(@PathVariable(value = "projectId") int projectId, @PathVariable(value = "taskId") int taskId, @PathVariable(value = "cmmtId") int cmmtId,
		 @RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.DELETE, ModuleEnum.TASK);
			taskCommentService.getById(cmmtId);
			taskCommentService.delete(cmmtId);
			LOGGER.info("Delete task comment ,id: " + cmmtId + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, null, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Delete  task comment ,id: " + cmmtId + " failed, exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@PutMapping("/{projectId}/task/{taskId}/comment/{cmmtId}")
	public ResponseEntity<ResponseModel> updateTaskComment
		(@PathVariable(value = "projectId") int projectId,
		 @PathVariable(value = "taskId") int taskId,
		 @PathVariable(value = "cmmtId") int cmmtId,
		 @Validated(CRUDModel.CRUDUpdate.class) @RequestBody TaskCommentModel taskCommentModel,
		 @RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.UPDATE, ModuleEnum.TASK);
			TaskCommentModel updateTaskCommentModel = taskCommentService.update(cmmtId, taskCommentModel);

			LOGGER.info("Update task ,id: " + taskId + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, updateTaskCommentModel, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Update task id: " + taskId + " failed, exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("/{projectId}/task/{taskId}/comment")
	public ResponseEntity<ResponseModel> getAllTasksComment(@RequestParam(defaultValue = "0") Integer pageNo,
															@RequestParam(defaultValue = "10") Integer pageSize,
															@RequestParam(defaultValue = "id") String orderBy,
															@RequestParam(defaultValue = "true") boolean orderByDesc,
															@RequestParam(defaultValue = "") String query,
															@PathVariable Integer projectId, @PathVariable Integer taskId,
															@RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.TASK);
			Page<TaskCommentModel> taskCommentModels = taskCommentService.getAllTasksComment(pageNo, pageSize, orderBy, orderByDesc, query, taskId);
			Map<String, Object> map = taskCommentService.getAllResponse(taskCommentModels);
			LOGGER.info("get all task comment with task id: " + taskId + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, map, null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get all task comment with task id: " + taskId + " failed, exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}


	@PostMapping("/{projectId}/taskModule")
	@CacheEvict(value = {"filterByTags", "findActiveNewsForDashboard", "findActiveAssetNewsForDashboard"}, allEntries = true)
	public ResponseEntity<ResponseModel> createTaskModule
		(@PathVariable(value = "projectId") int projectId,
		 @RequestParam(defaultValue = "") String module,
		 @RequestParam(defaultValue = "0") Integer mdlTskIdInternal,
		 @Validated(CRUDModel.CRUDCreation.class) @RequestBody TaskModel taskModel,
		 @RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.CREATE, ModuleEnum.TASK);
			CompanyModel companyModel = authenticationService.getCompanyFromToken(token.substring(7));
			if (module.equals("") || mdlTskIdInternal == 0) {
				LOGGER.error("module and mdlTskIdInternal can't be empty.");
				return ResponseEntity.status(500).body(new ResponseModel(0, null, "module and mdlTskIdInternal can't be empty."));
			}
			ProjectModel projectModel = projectService.getById(projectId);
			if (projectModel.getPrjStatus() == PrjStatusEnum.COMPLETED) {
				taskModel.setStatus(StatusEnum.DONE);
				taskModel.setTskStartDate(projectModel.getPrjEndDate());
				taskModel.setTskEndDate(projectModel.getPrjEndDate());
			}

			taskModel.setTskPrjID(projectModel);
			if (moduleTaskService.getModuleTaskByModuleAndIdAndCmpId(ModuleEnum.valueOf(module), mdlTskIdInternal, companyModel.getCmpId()) != null) {
				return ResponseEntity.status(400).body(new ResponseModel(0, null, "Task module already exist."));
			}

			TaskModel newTaskModel = taskService.create(taskModel);
			if (module.equals(ModuleEnum.RISK_ASSESSMENTS.toString())) {
				vulnerabilityAssessmentsService.updateTask(mdlTskIdInternal, newTaskModel);
			}
			LOGGER.info("Create new Task ,id:" + newTaskModel.getTskId() + " success.");

			ModuleTaskModel moduleTaskModel = moduleTaskService.create(new ModuleTaskModel(newTaskModel, ModuleEnum.valueOf(module), mdlTskIdInternal));
			LOGGER.info("Create new module task ,id:" + moduleTaskModel.getTskId() + " success.");

			return ResponseEntity.ok().body(new ResponseModel(1, moduleTaskService.getSingleInstance(moduleTaskModel), null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Create new  Task failed, exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("/{projectId}/gantt")
	public ResponseEntity<ResponseModel> getProjectGantt(@PathVariable Integer projectId, @RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.PROJECT);
			List<TaskModel> taskModels = taskService.getAllTaskByProjId(projectId);
			if (taskModels == null)
				return ResponseEntity.badRequest().body(new ResponseModel(0, null, "project id: " + projectId + " not exists!"));

			LOGGER.info("get all task  with project id: " + projectId + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, taskModels, null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get all task with project id:" + projectId + " failed, exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("/projectOverview")
	public ResponseEntity<ResponseModel> getProjectOverview(@RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.PROJECT);
			List<ProjectOverviewModel> projectOverviewModelList = projectService.projectOverview(token);
			LOGGER.info("get project overview success.");
			return ResponseEntity.ok().body(new ResponseModel(1, projectOverviewModelList, null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get project overview failed, exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("/projectDeadline")
	public ResponseEntity<ResponseModel> getProjectDeadline(@RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.PROJECT);
			List<ProjectDeadlinesModel> projectDeadlinesModels = projectService.projectDeadlinesReport(token);
			LOGGER.info("get project deadline report success.");
			return ResponseEntity.ok().body(new ResponseModel(1, projectDeadlinesModels, null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get project deadline report failed, exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("/{projId}/members")
	public ResponseEntity<ResponseModel> getProjectsMember(@PathVariable int projId, @RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.PROJECT);

			List<UserProjectModel> userProjectModels = userProjectService.getByProjectId(projId);
			if (userProjectModels == null)
				return ResponseEntity.badRequest().body(new ResponseModel(0, null, "project id: " + projId + " not exists!"));

			LOGGER.info("get projects member success.");
			return ResponseEntity.ok().body(new ResponseModel(1, userProjectModels, null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get projects member failed, exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}


	@PostMapping("/{projectId}/task/{taskId}/recurringTask")
	public ResponseEntity<ResponseModel> createRecurringTask
		(@PathVariable(value = "projectId") int projectId, @PathVariable(value = "taskId") int taskId, @Validated(CRUDModel.CRUDCreation.class) @RequestBody RecurringTaskModel recurringTaskModel,
		 @RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.CREATE, ModuleEnum.TASK);
			TaskModel taskModel = taskService.getById(taskId);
			recurringTaskModel.setTskId(taskModel);
			RecurringTaskModel newRecurringTaskModel = recurringTaskService.create(recurringTaskModel);
			LOGGER.info("Create new recurringTask ,id:" + newRecurringTaskModel.getRecId() + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, newRecurringTaskModel, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Create new  recurringTask  failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("/{projectId}/task/{taskId}/recurringTask")
	public ResponseEntity<ResponseModel> getRecurringTask
		(@PathVariable(value = "projectId") int projectId, @PathVariable(value = "taskId") int taskId,
		 @RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.TASK);
			RecurringTaskModel updateRecurringTaskModel = recurringTaskService.findRecurringTaskByTaskId(taskId);
			LOGGER.info("Get recurringTask ,by task id: " + taskId + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, updateRecurringTaskModel, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Get  recurringTask ,by task id: " + taskId + " failed, exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@PutMapping("/{projectId}/task/{taskId}/recurringTask/{id}")
	public ResponseEntity<ResponseModel> updateRecurringTask
		(@PathVariable(value = "projectId") int projectId,
		 @PathVariable(value = "taskId") int taskId,
		 @PathVariable(value = "id") int id,
		 @Validated(CRUDModel.CRUDUpdate.class) @RequestBody RecurringTaskModel recurringTaskModel,
		 @RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.UPDATE, ModuleEnum.TASK);
			RecurringTaskModel updateRecurringTaskModel = recurringTaskService.update(id, recurringTaskModel);

			LOGGER.info("Update RecurringTask ,id: " + taskId + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, updateRecurringTaskModel, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Update RecurringTask id: " + taskId + " failed, exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}


	@PostMapping("/{projectId}/recurringProject")
	public ResponseEntity<ResponseModel> createRecurringProject
		(@PathVariable(value = "projectId") int projectId,
		 @Validated(CRUDModel.CRUDCreation.class) @RequestBody RecurringProjectModel recurringProjectModel,
		 @RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.CREATE, ModuleEnum.PROJECT);
			ProjectModel projectModel = projectService.getById(projectId);
			recurringProjectModel.setPrjId(projectModel);
			RecurringProjectModel newRecurringProjectModel = recurringProjectService.create(recurringProjectModel);
			LOGGER.info("Create new recurringProject ,id:" + newRecurringProjectModel.getRecId() + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, newRecurringProjectModel, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Create new  recurringProject  failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("/{projectId}/recurringProject")
	public ResponseEntity<ResponseModel> getRecurringProject
		(@PathVariable(value = "projectId") int projectId,
		 @RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.PROJECT);
			RecurringProjectModel recurringProjectModel = recurringProjectService.findRecurringProjectByPrjId(projectId);
			LOGGER.info("Get recurringProject ,by project id: " + projectId + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, recurringProjectModel, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Get  recurringProject ,by project id: " + projectId + " failed, exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}


	@PutMapping("/{projectId}/recurringProject/{id}")
	public ResponseEntity<ResponseModel> updateRecurringProject
		(@PathVariable(value = "projectId") int projectId,
		 @PathVariable(value = "id") int id,
		 @Validated(CRUDModel.CRUDUpdate.class) @RequestBody RecurringProjectModel recurringProjectModel,
		 @RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.UPDATE, ModuleEnum.PROJECT);
			RecurringProjectModel updateRecurringProjectModel = recurringProjectService.update(id, recurringProjectModel);

			LOGGER.info("Update RecurringProject ,id: " + projectId + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, updateRecurringProjectModel, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Update RecurringProject id: " + projectId + " failed, exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}


	@PostMapping("/projectsReport")
	public ResponseEntity<InputStreamResource> projectsReport
		(@RequestBody ProjectsReportModel projectsReportModel,
		 @RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.PROJECT);
			CompanyModel companyModel = authenticationService.getCompanyFromToken(token.substring(7));
			int cmpId = companyModel.getCmpId();
			ByteArrayInputStream in = projectService.getProjectsReport(cmpId, projectsReportModel);
			HttpHeaders headers = new HttpHeaders();
			headers.add("Content-Disposition", "attachment; filename=projectsReport.zip");

			return ResponseEntity
				.ok()
				.headers(headers)
				.body(new InputStreamResource(in));

		} catch (Exception e) {
			LOGGER.error("exception: " + e.getMessage());
			return null;
		}
	}

	@PostMapping("/tasksReport")
	public ResponseEntity<InputStreamResource> tasksReport
		(@RequestBody TasksReportModel tasksReportModel,
		 @RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.PROJECT);
			CompanyModel companyModel = authenticationService.getCompanyFromToken(token.substring(7));
			int cmpId = companyModel.getCmpId();
			ByteArrayInputStream in = taskService.getTasksReport(cmpId, tasksReportModel);
			HttpHeaders headers = new HttpHeaders();
			headers.add("Content-Disposition", "attachment; filename=tasksReport.zip");

			return ResponseEntity
				.ok()
				.headers(headers)
				.body(new InputStreamResource(in));

		} catch (Exception e) {
			LOGGER.error("exception: " + e.getMessage());
			return null;
		}
	}

	/**
	 * Returns all company active projects
	 * This function return List of active projects
	 * For Security news, CVE and risk assessment modules
	 *
	 * @param token for auth
	 * @return List<ProjectModel>
	 */
	@GetMapping("/allProjectsWithoutFilters")
	public ResponseEntity<ResponseModel> getAllRecords(@RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.PROJECT);

			CompanyUserModel companyUserModel = authenticationService.getCompanyUserFromToken(token.substring(7));
			int cmpId = companyUserModel.getCmpUsrCmpId().getCmpId();

			List<ProjectModel> projectModelPage = projectService.getNoFilterProjects(cmpId);
			return ResponseEntity.ok().body(new ResponseModel(1, projectModelPage, null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Get all from project failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}
}
