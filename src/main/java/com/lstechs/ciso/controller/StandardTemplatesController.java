package com.lstechs.ciso.controller;

import com.lstechs.ciso.enums.ModuleEnum;
import com.lstechs.ciso.enums.OperationEnum;
import com.lstechs.ciso.model.*;
import com.lstechs.ciso.repository.StandardRepository;
import com.lstechs.ciso.service.*;
import javassist.NotFoundException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/templates")
public class StandardTemplatesController extends CRUDController<StandardModel, SingleStandardModel> {

	@Value("${server.url}")
	private String url;

	@Autowired
	private AuthenticationService authenticationService;

	@Autowired
	private StandardService standardService;

	@Autowired
	private StdQuestionaireService stdQuestionaireService;

	@Autowired
	private StdQtrQuestionsService stdQtrQuestionsService;

	@Autowired
	private StdSectionService stdSectionService;

	@Autowired
	private RequestQuestionnaireService requestQuestionnaireService;

	@Autowired
	private StdRequirementsService stdRequirementsService;

	@Autowired
	private StdQstOptionService stdQstOptionService;

	@Autowired
	private StdAnswer1OptionService stdAnswer1OptionService;

	@Autowired
	private StdAnswer2OptionService stdAnswer2OptionService;

	@Autowired
	private FilesService filesService;

	@Autowired
	private StandardRepository standardRepository;

	@Autowired
	private VendorStandardHeaderTitlesService vendorStandardHeaderTitlesService;

	private static final Logger LOGGER = LogManager.getLogger(StandardTemplatesController.class.getName());

	public StandardTemplatesController(StandardService standardService) {
		super(standardService, ModuleEnum.VENDOR);
	}

	@PostMapping("/createStandard")
	public ResponseEntity<ResponseModel> createStandard(@Validated(CRUDModel.CRUDUpdate.class) @RequestBody StandardModel standardModel,
														@RequestHeader(name = "Authorization") String token) {
		try {
			standardService.handlePermission(token, OperationEnum.CREATE, ModuleEnum.VENDOR);
			StandardModel model = standardService.createVendorStandard(standardModel);
			LOGGER.info("update custom Standard  id: " + standardModel.getStdId() + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, model, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (NotFoundException ex) {
			LOGGER.error("update custom Standard  failed , exception: " + ex);
			return ResponseEntity.badRequest().body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("update custom Standard  failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@PostMapping("/{templateId}/questionnaire")
	public ResponseEntity<ResponseModel> createOrUpdateQuestionnaire(@PathVariable(value = "templateId") int templateId,
																	 @Validated(CRUDModel.CRUDCreation.class) @RequestBody RequestQuestionnaireModel requestQuestionnaireModel,
																	 @RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.CREATE, ModuleEnum.VENDOR);
			StdQuestionaireModel newQuestionnaireModel = requestQuestionnaireService
				.requestParser(requestQuestionnaireModel, templateId);

			if (requestQuestionnaireModel.getStdQtrId() == 0) { // create new questionnaire
				LOGGER.info("Create new Questionnaire id:" + newQuestionnaireModel.getStdQtrId() + ", success.");
				return ResponseEntity.ok().body(new ResponseModel(1, newQuestionnaireModel, null));
			} else { // update questionnaire content
				LOGGER.info("Update Questionnaire id:" + requestQuestionnaireModel.getStdQtrId() + ", success.");
				token = token.replace("Bearer ", "");
				RequestQuestionnaireModel newRequestQuestionnaireModel = requestQuestionnaireService
					.getRequestQuestionnaire(requestQuestionnaireModel.getStdQtrId(), token);
				return ResponseEntity.ok().body(new ResponseModel(1, newRequestQuestionnaireModel, null));
			}
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Create new Questionnaire failed ,exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("/{templateId}/questionnaire")
	public ResponseEntity<ResponseModel> getAllQuestionnaire(@PathVariable(value = "templateId") int templateId,
															 @RequestParam(defaultValue = "0") Integer pageNo, @RequestParam(defaultValue = "10") Integer pageSize,
															 @RequestParam(defaultValue = "id") String orderBy, @RequestParam(defaultValue = "true") boolean orderByDesc,
															 @RequestParam(defaultValue = "") String query, @RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.VENDOR);
			Page<StdQuestionaireModel> stdQuestionnaireModels = stdQuestionaireService
				.getAllQuestionaireByTemplateId(pageNo, pageSize, orderBy, orderByDesc, query, templateId);
			Map<String, Object> map = stdQuestionaireService.getAllResponse(stdQuestionnaireModels);
			LOGGER.info("get all questionnaire success.");
			return ResponseEntity.ok().body(new ResponseModel(1, map, null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get all users questionnaire failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("{templateId}/questionnaire/{id}")
	public ResponseEntity<ResponseModel> getQuestionnaireById(@PathVariable(value = "templateId") int templateId,
															  @PathVariable(value = "id") int id, @RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.VENDOR);
			StdQuestionaireModel stdQuestionaireModel = stdQuestionaireService.getById(id);

			//Title Handling
			if (!stdQuestionaireModel.isStandardCreatedByCompany()) {
				VendorStandardHeaderTitlesModel vendorStandardHeaderTitlesModel = vendorStandardHeaderTitlesService.getVendorHeaderTitlesByStandardId(stdQuestionaireModel.getStdId().getStdId());
				if (vendorStandardHeaderTitlesModel != null) {
					stdQuestionaireModel.setVendorStandardHeaderTitlesModel(vendorStandardHeaderTitlesModel);
				}
			}
			LOGGER.info("get questionnaire by  id: " + id + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, stdQuestionaireModel, null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (NotFoundException ex) {
			LOGGER.error("get questionnare by id: " + id + " failed, exception: " + ex);
			return ResponseEntity.badRequest().body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get questionnare by id: " + id + " failed, exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("{templateId}/questionnaire/{id}/questions")
	public ResponseEntity<ResponseModel> getRequestQuestionnaire(@PathVariable(value = "id") int id,
																 @RequestHeader(name = "Authorization") String token) {

		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.VENDOR);
			RequestQuestionnaireModel requestQuestionnaireModel = requestQuestionnaireService.getRequestQuestionnaire(id, token.substring(7));
			LOGGER.info("Get request questionnaire id: " + id);
			return ResponseEntity.ok().body(new ResponseModel(1, requestQuestionnaireModel, null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get RequestQuestionaire by id: " + id + " failed, exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@PutMapping("/{templateId}/questionnaire/{questionnaireID}")
	public ResponseEntity<ResponseModel> updateQuestionnaire(@PathVariable(value = "templateId") int templateId,
															 @PathVariable(value = "questionnaireID") int questionnaireID,
															 @Validated(CRUDModel.CRUDUpdate.class) @RequestBody StdQuestionaireModel stdQuestionaireModel,
															 @RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.UPDATE, ModuleEnum.VENDOR);
			stdQuestionaireModel.setStdId(standardService.getById(stdQuestionaireModel.getStdId().getStdId()));
			StdQuestionaireModel updateStdQuestionaireModel = stdQuestionaireService.update(questionnaireID,
				stdQuestionaireModel);
			LOGGER.info("Update Questionnare ,id:" + updateStdQuestionaireModel.getStdQtrId() + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, updateStdQuestionaireModel, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Update Questionnare failed, exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@PostMapping("/{templateId}/duplicate")
	public ResponseEntity<ResponseModel> duplicateTemplate(@PathVariable(value = "templateId") int templateId,
														   @RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.CREATE, ModuleEnum.VENDOR);
			StandardModel standardModel = standardService.getById(templateId);
			StandardModel newStandardModel = standardService.duplicateStandard(standardModel, token);
			LOGGER.info("Duplicate Standard , id: " + templateId + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, newStandardModel, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Duplicate template , id: " + templateId + "failed, exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@PutMapping("/{templateId}/complete")
	public ResponseEntity<ResponseModel> markComplete(@PathVariable(value = "templateId") int templateId,
													  @RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.UPDATE, ModuleEnum.VENDOR);
			standardService.markComplete(templateId);
			LOGGER.info("mark complete in standard ,id: " + templateId + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, null, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("mark complete in standard ,id: " + templateId + " failed, exception : " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@DeleteMapping("{templateId}/requirement/{reqId}")
	public ResponseEntity<ResponseModel> deleteRequirement(@PathVariable(value = "templateId") int templateId,
														   @PathVariable(value = "reqId") int reqId, @RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.DELETE, ModuleEnum.VENDOR);
			stdQtrQuestionsService.deleteQuetionsByReqId(reqId);
			StdRequirementsModel stdRequirementsModel = stdRequirementsService.delete(reqId);
			LOGGER.info("Delete requirement ,id:" + reqId + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, stdRequirementsModel, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Delete requirement failed, exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@DeleteMapping("{templateId}/section/{id}")
	public ResponseEntity<ResponseModel> deleteSection(@PathVariable(value = "templateId") int templateId,
													   @PathVariable(value = "id") int id, @RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.DELETE, ModuleEnum.VENDOR);
			StdSectionModel stdSectionModel = stdSectionService.delete(id);
			LOGGER.info("Delete section ,id:" + id + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, stdSectionModel.getStdSctId(), null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Delete section failed, exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@DeleteMapping("{templateId}/questionnaire/{id}")
	public ResponseEntity<ResponseModel> deleteQuestionaire(@PathVariable(value = "templateId") int templateId,
															@PathVariable(value = "id") int id, @RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.DELETE, ModuleEnum.VENDOR);
			StdQuestionaireModel stdQuestionaireModel = stdQuestionaireService.delete(id);
			LOGGER.info("Delete section ,id:" + id + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, stdQuestionaireService.getSingleInstance(stdQuestionaireModel), null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Delete section failed, exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@PostMapping("/{templateId}/questionnaire/{id}/duplicate")
	public ResponseEntity<ResponseModel> duplicateQuestionnaire(@PathVariable(value = "templateId") int templateId,
																@PathVariable(value = "id") int id, @RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.CREATE, ModuleEnum.VENDOR);
			StandardModel standardModel = standardRepository.getStandardById(templateId);
			StdQuestionaireModel stdQuestionaireModel = stdQuestionaireService.getById(id);
			StdQuestionaireModel newStdQuestionnaireModel = stdQuestionaireService
				.duplicateVendorOriginalQuestionnaire(stdQuestionaireModel, standardModel, token, standardModel, false);

			return ResponseEntity.ok().body(new ResponseModel(1, newStdQuestionnaireModel, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Duplicate template , id: " + templateId + "failed, exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("/withRequirements")
	public ResponseEntity<ResponseModel> getStandardsWithRequirements(
		@RequestHeader(name = "Authorization") String token) {
		try {
			standardService.handlePermission(token, OperationEnum.READ, ModuleEnum.VENDOR);
			List<StandardModel> standardModels = standardService.getAllStandardsWithRequirements();
			LOGGER.info("Get all standards with Requirements success.");
			return ResponseEntity.ok().body(new ResponseModel(1, standardModels, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Get all avail sections by questionnaireId failed ,exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("/syncStd")
	public @ResponseBody
	List<StandardModel> getStdListByIds(@RequestParam(defaultValue = "ids") int[] ids) {
		try {
			return standardService.getStdListByIds(ids);
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return null;
		} catch (Exception ex) {
			LOGGER.error("sync std, exception: " + ex);
			return null;
		}
	}

	@GetMapping("/syncQuestionaire")
	public @ResponseBody
	List<StdQuestionaireModel> getQuestionaire(@RequestParam(defaultValue = "ids") int[] ids) {
		try {
			List<StdQuestionaireModel> questionnaire = stdQuestionaireService.getQuestionaireListByStdIdsForOnPremise(ids);
			return questionnaire;
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return null;
		} catch (Exception ex) {
			LOGGER.error("sync questionaire, exception: " + ex);
			return null;
		}
	}

	@GetMapping("/syncSections")
	public @ResponseBody
	List<StdSectionModel> getSections(@RequestParam(defaultValue = "ids") int[] ids) {
		try {
			return stdSectionService.getAllSectionByStasndardIds(ids);
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return null;
		} catch (Exception ex) {
			LOGGER.error("sync sections, exception: " + ex);
			return null;
		}
	}


	@GetMapping("/syncRequirements")
	public @ResponseBody
	List<StdRequirementsModel> getRequirements(@RequestParam(defaultValue = "ids") int[] ids) {
		try {
			return stdRequirementsService.getRequirementsListByTemplateIds(ids);
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return null;
		} catch (Exception ex) {
			LOGGER.error("sync Requirements, exception: " + ex);
			return null;
		}
	}

	@GetMapping("/syncQuestions")
	public @ResponseBody
	List<StdQtrQuestionsModel> getQuestions(@RequestParam(defaultValue = "ids") int[] ids) {
		try {
			return stdQtrQuestionsService.getAllQuestionByStandardIds(ids);
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return null;
		} catch (Exception ex) {
			LOGGER.error("sync Questions, exception: " + ex);
			return null;
		}
	}

	@GetMapping("/syncOptions")
	public @ResponseBody
	List<StdQstOptionModel> getOptions(@RequestParam(defaultValue = "ids") int[] ids) {
		try {
			return stdQstOptionService.getStdQstOptionModelByStandardIds(ids);
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return null;
		} catch (Exception ex) {
			LOGGER.error("sync Options, exception: " + ex);
			return null;
		}
	}

	@GetMapping("/syncAnswer1Options")
	public @ResponseBody
	List<StdAnswer1OptionModel> getAnswer1Options(@RequestParam(defaultValue = "ids") int[] ids) {
		try {
			return stdAnswer1OptionService.getStdAnswer1OptionModelByStandardIds(ids);
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return null;
		} catch (Exception ex) {
			LOGGER.error("sync Answer 1 Options, exception: " + ex);
			return null;
		}
	}

	@GetMapping("/syncAnswer2Options")
	public @ResponseBody
	List<StdAnswer2OptionModel> getAnswer2Options(@RequestParam(defaultValue = "ids") int[] ids) {
		try {
			return stdAnswer2OptionService.getStdAnswer2OptionModelByStandardIds(ids);
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return null;
		} catch (Exception ex) {
			LOGGER.error("sync Answer 2Options, exception: " + ex);
			return null;
		}
	}

	@PutMapping("/{templateId}/questionnaire/{id}/upload")
	public ResponseEntity<ResponseModel> updateRecord(@PathVariable(value = "templateId") int templateId,
													  @PathVariable(value = "id") int id,
													  @RequestParam("file") MultipartFile file,
													  @RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.UPDATE, ModuleEnum.CUSTOMER_STANDARD);
			if (file == null) {
				return ResponseEntity.status(500).body(new ResponseModel(0, null, "File not found."));
			}
			StdQuestionaireModel questionaireModel = stdQuestionaireService.getById(id);
			FilesModel filesModel = filesService.storeFile(file, null, questionaireModel);
			String fileDownloadUri = "//" + url + "/api/customerStandard/downloadFile/" + filesModel.getId();
			filesService.updateFileUrl(filesModel.getId(), fileDownloadUri);

			LOGGER.info("update  customer standard id: " + id + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, new UploadFileResponseModel(filesModel.getFileName(), fileDownloadUri, file.getContentType(), file.getSize()), null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (NotFoundException ex) {
			LOGGER.error("update  customer standard id: " + id + " failed , exception: " + ex);
			return ResponseEntity.badRequest().body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("update  customer standard id: " + id + " failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@PutMapping("{standardId}/questionnaire/{questionnaireId}/reqHeaderTitles")
	public ResponseEntity<ResponseModel> updateRequirementsHeaderTitles(@PathVariable(value = "questionnaireId") int questionnaireId,
																		@PathVariable(value = "standardId") int standardId,
																		@Validated(CRUDModel.CRUDCreation.class) @RequestBody VendorStandardHeaderTitlesModel vendorStandardHeaderTitlesModel,
																		@RequestHeader(name = "Authorization") String token) {
		try {
			standardService.handlePermission(token, OperationEnum.UPDATE, ModuleEnum.CUSTOMER_STANDARD);
			VendorStandardHeaderTitlesModel savedVendorStandardHeaderTitlesModel = vendorStandardHeaderTitlesService.saveVendorRequirementsHeaderTitles(vendorStandardHeaderTitlesModel, questionnaireId);
			return ResponseEntity.ok().body(new ResponseModel(1, savedVendorStandardHeaderTitlesModel, null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Create new Questionnaire failed ,exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("/addVendorTitlesManually/{standardId}")
	public ResponseEntity<ResponseModel> addVendorTitlesManually(@PathVariable(value = "standardId") int standardId,
																 @RequestHeader(name = "Authorization") String token) {
		try {
//			standardService.handlePermission(token, OperationEnum.UPDATE, ModuleEnum.CUSTOMER_STANDARD);
			VendorStandardHeaderTitlesModel savedVendorStandardHeaderTitlesModel = vendorStandardHeaderTitlesService.createVendorTitlesManually(standardId);
			return ResponseEntity.ok().body(new ResponseModel(1, savedVendorStandardHeaderTitlesModel, null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Create new Questionnaire failed ,exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}
}
