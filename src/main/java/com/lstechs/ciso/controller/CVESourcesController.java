package com.lstechs.ciso.controller;

import com.lstechs.ciso.enums.ModuleEnum;
import com.lstechs.ciso.model.CVESourcesModel;
import com.lstechs.ciso.model.SingleCVESourcesModel;
import com.lstechs.ciso.service.CVESourcesService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cveSources")
public class CVESourcesController extends CRUDController<CVESourcesModel, SingleCVESourcesModel> {
    public CVESourcesController(CVESourcesService cveSourcesService) {
        super(cveSourcesService, ModuleEnum.CVE);
    }
}
