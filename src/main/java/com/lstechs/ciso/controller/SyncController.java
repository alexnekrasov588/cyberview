package com.lstechs.ciso.controller;

import com.itextpdf.text.log.SysoCounter;
import com.lstechs.ciso.model.CVEModel;
import com.lstechs.ciso.model.NewsArticlesModel;
import com.lstechs.ciso.model.ResponseModel;
import com.lstechs.ciso.model.StandardModel;
import com.lstechs.ciso.service.SyncService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/sync")
public class SyncController {

	private static Logger LOGGER = LogManager.getLogger(StandardTemplatesController.class.getName());

	@Value("${onPrem}")
	private boolean onPrem;

	@Autowired
	private SyncService syncService;

	@PostMapping("/news")
	public ResponseEntity<ResponseModel> syncNews() throws Throwable {
		if (onPrem) {
			try {
				List<NewsArticlesModel> newsArticlesModels = syncService.syncNewsArticlesModel();
				LOGGER.info("sync news success.");
				return ResponseEntity.ok().body(new ResponseModel(1, newsArticlesModels, null));

			} catch (Exception ex) {
				LOGGER.info("sync news failed , exception: " + ex);
				return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
			}
		} else {
			LOGGER.info("Method not allowed.");
			return ResponseEntity.status(405).body(new ResponseModel(0, null, "Method not allowed."));
		}
	}


	@PostMapping("/cve")
	public ResponseEntity<ResponseModel> syncCVE() throws Throwable {
		if (onPrem) {
			try {
				List<CVEModel> cveModels = syncService.syncCVEModel();
				LOGGER.info("sync cve success.");
				return ResponseEntity.ok().body(new ResponseModel(1, cveModels, null));

			} catch (Exception ex) {
				LOGGER.info("sync cve failed , exception: " + ex);
				return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
			}
		} else {
			LOGGER.info("Method not allowed.");
			return ResponseEntity.status(405).body(new ResponseModel(0, null, "Method not allowed."));
		}
	}

	@PostMapping("/syncAll")
	public ResponseEntity<ResponseModel> syncStandard() throws Throwable {
		if (onPrem) {
			try {
				syncService.syncNewsAndCve();
				LOGGER.info("sync standard success.");
				return ResponseEntity.ok().body(new ResponseModel(1, "sync was success", null));

			} catch (Exception ex) {
				LOGGER.info("sync cve failed , exception: " + ex);
				return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
			}
		} else {
			LOGGER.info("Method not allowed.");
			return ResponseEntity.status(405).body(new ResponseModel(0, null, "Method not allowed."));
		}
	}

	@PostMapping("/test")
	public ResponseEntity<ResponseModel> test() throws Throwable {
		if (onPrem) {
			try {
				syncService.onPremUpdate();
				LOGGER.info("sync onPremUpdate success.");
				return ResponseEntity.ok().body(new ResponseModel(1, "sync was success", null));

			} catch (Exception ex) {
				LOGGER.error("sync failed, exception", ex);
				return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
			}
		} else {
			LOGGER.info("Method not allowed.");
			return ResponseEntity.status(405).body(new ResponseModel(0, null, "Method not allowed."));
		}
	}


}
