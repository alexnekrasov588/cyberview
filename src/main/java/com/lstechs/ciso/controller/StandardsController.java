package com.lstechs.ciso.controller;

import com.lstechs.ciso.enums.ModuleEnum;
import com.lstechs.ciso.enums.OperationEnum;
import com.lstechs.ciso.model.*;
import com.lstechs.ciso.repository.StandardRepository;
import com.lstechs.ciso.service.*;

import javassist.NotFoundException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/standards")
public class StandardsController {

	@Autowired
	public StandardService standardService;

	@Autowired
	public StdQuestionaireService stdQuestionaireService;

	@Autowired
	public FilesService filesService;

	@Autowired
	public StandardCompanyStatusService standardCompanyStatusService;

	@Autowired
	public AuthenticationService authenticationService;

	@Autowired
	public RequestQuestionnaireService requestQuestionnaireService;

	@Autowired
	public StdQuestionaireService questionaireService;

	@Autowired
	public StandardRepository standardRepository;

	@Autowired
	public RequirementsHeaderTitlesService requirementsHeaderTitlesService;

	@Autowired
	public StdQtrQuestionsService stdQtrQuestionsService;

	@Autowired
	public StdRequirementsService stdRequirementsService;

	@Autowired
	public StdSectionService stdSectionService;

	private static final Logger LOGGER = LogManager.getLogger(StandardsController.class.getName());

	@PostMapping("/createStandard")
	public ResponseEntity<ResponseModel> createStandard(@Validated(CRUDModel.CRUDUpdate.class) @RequestBody StandardModel standardModel,
														@RequestHeader(name = "Authorization") String token) {
		try {
			standardService.handlePermission(token, OperationEnum.CREATE, ModuleEnum.CUSTOMER_STANDARD);
			StandardModel model = standardService.createCisoStandard(standardModel, token);
			LOGGER.info("update custom Standard  id: " + standardModel.getStdId() + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, model, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (NotFoundException ex) {
			LOGGER.error("update custom Standard  failed , exception: " + ex);
			return ResponseEntity.badRequest().body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("update custom Standard  failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	/**
	 * @param standardModel standardModel
	 * @param token         token
	 * @return StandardModel
	 */
	@PutMapping("/updateStandard")
	public ResponseEntity<ResponseModel> updateStandard(@Validated(CRUDModel.CRUDUpdate.class) @RequestBody StandardModel standardModel,
														@RequestHeader(name = "Authorization") String token) {
		try {
			standardService.handlePermission(token, OperationEnum.UPDATE, ModuleEnum.CUSTOMER_STANDARD);
			StandardModel model = standardService.update(standardModel.getStdId(), standardModel);
			LOGGER.info("update custom Standard  id: " + standardModel.getStdId() + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, model, null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (NotFoundException ex) {
			LOGGER.error("update custom Standard  failed , exception: " + ex);
			return ResponseEntity.badRequest().body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("update custom Standard  failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	/**
	 * @param id    Standard Id
	 * @param token token
	 * @return StandardModel
	 */
	@DeleteMapping("/deleteStandard/{id}")
	public ResponseEntity<ResponseModel> deleteRecord(@PathVariable(value = "id") int id, @RequestHeader(name = "Authorization") String token) {
		try {
			standardService.handlePermission(token, OperationEnum.UPDATE, ModuleEnum.CUSTOMER_STANDARD);
			StandardModel model = standardService.deleteStandardFromCiso(id);
			LOGGER.info("delete  id: " + id + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, model, null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (NotFoundException ex) {
			LOGGER.error("delete  id: " + id + " failed , exception: " + ex);
			return ResponseEntity.badRequest().body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("delete   id: " + id + " failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	/**
	 * @param standardId      standardId
	 * @param questionnaireId questionnaireId
	 * @param token           token
	 * @return InputStreamResource
	 * @throws Exception Exception
	 */
	@GetMapping(value = "/{standardId}/questionnaire/{questionnaireId}/export")
	public ResponseEntity<InputStreamResource> excelCustomersReport(@PathVariable(value = "standardId") int standardId,
																	@PathVariable(value = "questionnaireId") int questionnaireId,
																	@RequestHeader(name = "Authorization") String token) throws Exception {
		CompanyModel companyModel = authenticationService.getCompanyFromToken(token.substring(7));
		standardService.handlePermission(token, OperationEnum.READ, ModuleEnum.CUSTOMER_STANDARD);
		ByteArrayInputStream in = requestQuestionnaireService.questionnaireToExcel(standardId, questionnaireId, companyModel.getCmpId());

		try {
			List<FilesModel> filesModels = filesService.getAllFilesModelByCompanyId(companyModel.getCmpId());

			Map<String, InputStream> files = new HashMap<>();
			for (FilesModel f : filesModels) {
				// add all customer standard files
				files.put(f.getFileName(), f.toInputStream());
			}
			// add standard xlsx file
			files.put("stdAnswers.xlsx", in);

			InputStream zipInputStream = filesService.zipFiles(files);

			HttpHeaders headers = new HttpHeaders();
			headers.add("Content-Disposition", "attachment; filename=exportZip.zip");

			return ResponseEntity
				.ok()
				.headers(headers)
				.body(new InputStreamResource(zipInputStream));
		} catch (Exception e) {
			LOGGER.error("exception: " + e.getMessage());
			return null;
		}
	}

	/**
	 * @param pageNo      pageNo
	 * @param pageSize    pageSize
	 * @param orderBy     orderBy
	 * @param orderByDesc orderByDesc
	 * @param query       query
	 * @param token       token
	 * @return StandardModel
	 */
	@GetMapping
	public ResponseEntity<ResponseModel> getCompanyStandards(
		@RequestParam(defaultValue = "0") Integer pageNo,
		@RequestParam(defaultValue = "10") Integer pageSize,
		@RequestParam(defaultValue = "id") String orderBy,
		@RequestParam(defaultValue = "true") boolean orderByDesc,
		@RequestParam(defaultValue = "") String query,
		@RequestHeader(name = "Authorization") String token) {
		try {
			standardService.handlePermission(token, OperationEnum.READ, ModuleEnum.CUSTOMER_STANDARD);
			Page<StandardModel> standardModels = standardService.getAllAvailCompanyStandardByPage(pageNo, pageSize, orderBy, orderByDesc, query, token);
			Map<String, Object> map = standardService.getAllResponse(standardModels);
			LOGGER.info("get all avail company standard");
			return ResponseEntity.ok().body(new ResponseModel(1, map, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get all avail company standard failed ,exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	/**
	 * @param standardId standardId
	 * @param token      token
	 * @return StandardModel
	 */
	@GetMapping("/{standardId}")
	public ResponseEntity<ResponseModel> getStandard(@PathVariable(value = "standardId") int standardId, @RequestHeader(name = "Authorization") String token) {
		try {
			standardService.handlePermission(token, OperationEnum.READ, ModuleEnum.CUSTOMER_STANDARD);
			if (standardService.ifStandardExistInCompany(standardId, token)) {
				StandardModel standardModel = standardService.getStandard(standardId, token, false);
				return ResponseEntity.ok().body(new ResponseModel(1, standardModel, null));
			} else {
				LOGGER.error("Standard id: " + standardId + " not found");
				return ResponseEntity.status(404).body(new ResponseModel(0, null, "Standard id: " + standardId + " not found"));
			}
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get all avail company standard failed ,exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	/**
	 * @param token token
	 * @return String Standard Name
	 */
	@GetMapping("/getStandardsName")
	public ResponseEntity<ResponseModel> getStandardsNameByToken(@RequestHeader(name = "Authorization") String token) {
		try {
			standardService.handlePermission(token, OperationEnum.READ, ModuleEnum.CUSTOMER_STANDARD);
			return ResponseEntity.ok().body(new ResponseModel(1, standardService.getStandardsNameByToken(token), null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get all avail company standard failed ,exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	/**
	 * @param stdQtrId stdId
	 * @param token    token
	 * @return StdRequirementsModel
	 */
	@GetMapping("/getReqByStdQtrId/{stdQtrId}")
	public ResponseEntity<ResponseModel> getReqByStdQtrId(@PathVariable(value = "stdQtrId") int stdQtrId, @RequestHeader(name = "Authorization") String token) {
		try {
			standardService.handlePermission(token, OperationEnum.READ, ModuleEnum.CUSTOMER_STANDARD);
			return ResponseEntity.ok().body(new ResponseModel(1, stdRequirementsService.getReqByQuestionnaireId(stdQtrId), null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: ", ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get all avail company standard failed ,exception: ", ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	/**
	 * @param token token
	 * @return FilterModel
	 */
	@GetMapping("/getStandardListOption")
	public ResponseEntity<ResponseModel> getStandardListOption(@RequestHeader(name = "Authorization") String token) {
		try {
			standardService.handlePermission(token, OperationEnum.READ, ModuleEnum.CUSTOMER_STANDARD);
			return ResponseEntity.ok().body(new ResponseModel(1, standardService.getStandardListOption(token), null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get all avail company standard failed ,exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	/**
	 * @param ids   ids
	 * @param token token
	 * @return QuestionnaireNameModel
	 */
	@GetMapping("/getQuestionairesName")
	public ResponseEntity<ResponseModel> getQuestionairesNameByStd(@RequestParam(defaultValue = "") int[] ids, @RequestHeader(name = "Authorization") String token) {
		try {
			standardService.handlePermission(token, OperationEnum.READ, ModuleEnum.CUSTOMER_STANDARD);
			return ResponseEntity.ok().body(new ResponseModel(1, questionaireService.getQuestionairesNameByStdIds(ids), null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get all avail company standard failed ,exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	/**
	 * @param stdReportRequestModel stdReportRequestModel
	 * @param token                 token
	 * @return StdReportsFilesModel
	 */
	@PostMapping("/getComplianceReport")
	public ResponseEntity<InputStreamResource> getComplianceReport(@RequestBody StdReportRequestModel stdReportRequestModel,
																   @RequestHeader(name = "Authorization") String token) {
		try {
			standardService.handlePermission(token, OperationEnum.READ, ModuleEnum.CUSTOMER_STANDARD);
			CompanyModel companyModel = authenticationService.getCompanyFromToken(token.substring(7));
			int cmpId = companyModel.getCmpId();
			StdReportsFilesModel reportsFilesModel = standardService.getComplianceReport(stdReportRequestModel, cmpId);

			List<FilesModel> filesModels = filesService.getAllFilesModelByCustomerStandardIds(reportsFilesModel.getCustomerStdWithFilesIds());

			Map<String, InputStream> files = new HashMap<>();
			for (FilesModel f : filesModels) {
				// add all customer standard files
				files.put(f.getFileName(), f.toInputStream());
			}
			// add standard xlsx file
			files.put("complianceReport.xlsx", reportsFilesModel.getExcelFile());

			InputStream zipInputStream = filesService.zipFiles(files);

			HttpHeaders headers = new HttpHeaders();
			headers.add("Content-Disposition", "attachment; filename=complianceZip.zip");

			return ResponseEntity
				.ok()
				.headers(headers)
				.body(new InputStreamResource(zipInputStream));

		} catch (Exception e) {
			LOGGER.error("exception: " + e.getMessage());
			return null;
		}
	}

	/**
	 * @param stdReportRequestModel stdReportRequestModel
	 * @param token                 token
	 * @return InputStreamResource
	 */
	@PostMapping("/getStdReport")
	public ResponseEntity<InputStreamResource> getStdReport
	(@RequestBody StdReportRequestModel stdReportRequestModel,
	 @RequestHeader(name = "Authorization") String token) {
		try {
			standardService.handlePermission(token, OperationEnum.READ, ModuleEnum.CUSTOMER_STANDARD);
			List<Integer> list = Arrays.stream(stdReportRequestModel.getStdQtrId()).boxed().collect(Collectors.toList());
			List<FilesModel> filesModels = filesService.getAllFilesModelByStdQtrIds(list);

			Map<String, InputStream> files = new HashMap<>();
			for (FilesModel f : filesModels) {

				files.put(f.getFileName(), f.toInputStream());
			}

			InputStream zipInputStream = filesService.zipFiles(files);

			HttpHeaders headers = new HttpHeaders();
			headers.add("Content-Disposition", "attachment; filename=stdZip.zip");

			return ResponseEntity
				.ok()
				.headers(headers)
				.body(new InputStreamResource(zipInputStream));

		} catch (Exception e) {
			LOGGER.error("exception: " + e.getMessage());
			return null;
		}
	}

//	@GetMapping("/{standardId}/questionnaires/{questionnaireId}/sections/{sectionId}")
//	public ResponseEntity<ResponseModel> getSectionById(@PathVariable(value = "standardId") int standardId,
//														@RequestHeader(name = "Authorization") String token,
//														@PathVariable(value = "questionnaireId") int questionnaireId,
//														@PathVariable(value = "sectionId") int sectionId) {
//		try {
//			standardService.handlePermission(token, OperationEnum.READ, ModuleEnum.CUSTOMER_STANDARD);
//			if (standardService.ifStandardExistInCompany(standardId, token)) {
//				StdSectionModel stdSectionModel = standardService
//				StdQuestionaireModel stdQuestionnaireModel = standardService.getQuestionnaireContent(questionnaireId);
//				return ResponseEntity.ok().body(new ResponseModel(1, stdQuestionnaireModel, null));
//			} else {
//				LOGGER.error("Questionnaires id: " + questionnaireId + " not found");
//				return ResponseEntity.status(404).body(new ResponseModel(0, null, "Questionnaire id: " + questionnaireId + " not found"));
//			}
//		} catch (SecurityException ex) {
//			LOGGER.error("FORBIDDEN , exception: " + ex);
//			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
//		} catch (Exception ex) {
//			LOGGER.error("Get all avail sections by questionnaireId failed ,exception: " + ex);
//			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
//		}
//	}

	/**
	 * @param standardId      standardId
	 * @param token           token
	 * @param questionnaireId questionnaireId
	 * @return StdQuestionnaireModel
	 */
	@GetMapping("/{standardId}/questionnaires/{questionnaireId}")
	public ResponseEntity<ResponseModel> getAllSectionsByQuestionnaireId(@PathVariable(value = "standardId") int standardId,
																		 @RequestHeader(name = "Authorization") String token,
																		 @PathVariable(value = "questionnaireId") int questionnaireId) {
		try {
			standardService.handlePermission(token, OperationEnum.READ, ModuleEnum.CUSTOMER_STANDARD);
			if (standardService.ifStandardExistInCompany(standardId, token)) {
				// get user's company for module tasks
				CompanyModel companyModel = authenticationService.getCompanyFromToken(token.substring(7));
				StdQuestionaireModel stdQuestionnaireModel = standardService.getQuestionnaireContent(questionnaireId, companyModel.getCmpId());
				return ResponseEntity.ok().body(new ResponseModel(1, stdQuestionnaireModel, null));
			} else {
				LOGGER.error("Standard id: " + standardId + " not found");
				return ResponseEntity.status(404).body(new ResponseModel(0, null, "Standard id: " + standardId + " not found"));
			}
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Get all avail sections by questionnaireId failed ,exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	/**
	 * @param standardId      standardId
	 * @param questionnaireId questionnaireId
	 * @param stdSctId        stdSctId
	 * @param token           token
	 * @return StdSectionModel
	 */
	@GetMapping("/{standardId}/questionnaires/{questionnaireId}/sections/{sectionId}")
	public ResponseEntity<ResponseModel> getSectionBySectionId(@PathVariable(value = "standardId") int standardId,
															   @PathVariable(value = "questionnaireId") int questionnaireId,
															   @PathVariable(value = "sectionId") int stdSctId,
															   @RequestHeader(name = "Authorization") String token) {
		try {
			standardService.handlePermission(token, OperationEnum.READ, ModuleEnum.CUSTOMER_STANDARD);
			if (standardService.ifStandardExistInCompany(standardId, token)) {
				StdSectionModel sectionModel = standardService.getSectionBySectionId(stdSctId, questionnaireId);
				if (sectionModel != null) {
					return ResponseEntity.ok().body(new ResponseModel(1, sectionModel, null));
				} else {
					LOGGER.error("Section id: " + stdSctId + " not found in questionnaire id: " + questionnaireId + " or not found at all");
					return ResponseEntity.status(404).body(new ResponseModel(0, null, "Section id: " + stdSctId + " not found in questionnaire id: " + questionnaireId + " or not found at all"));
				}
			} else {
				LOGGER.error("Standard id: " + standardId + " not found");
				return ResponseEntity.status(404).body(new ResponseModel(0, null, "Standard id: " + standardId + " not found"));
			}
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Get Section by id: " + stdSctId + " failed ,exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	/**
	 * @param token token
	 * @return StandardComplianceModel
	 */
	@GetMapping("/standardComplianceReport")
	public ResponseEntity<ResponseModel> standardComplianceReport(@RequestHeader(name = "Authorization") String token) {
		try {
			standardService.handlePermission(token, OperationEnum.READ, ModuleEnum.CUSTOMER_STANDARD);
			CompanyModel companyModel = authenticationService.getCompanyFromToken(token.substring(7));
			String lcnsToken = companyModel.getCmpLcnsId().getLcnsToken();
			List<StandardComplianceModel> standardComplianceModels = standardService.standardComplianceReport(lcnsToken, companyModel.getCmpId());
			LOGGER.info("get standard compliance report success.");
			return ResponseEntity.ok().body(new ResponseModel(1, standardComplianceModels, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get standard compliance report failed ,exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	/**
	 * @param standardId standardId
	 * @param qtrId      qtrId
	 * @param token      token
	 * @return RequestQuestionnaireModel
	 */
	@GetMapping("{standardId}/questionnaire/{questionnaireId}/questions")
	public ResponseEntity<ResponseModel> getRequestQuestionnaire(@PathVariable String standardId,
																 @PathVariable(value = "questionnaireId") int qtrId,
																 @RequestHeader(name = "Authorization") String token) {

		try {
			standardService.handlePermission(token, OperationEnum.READ, ModuleEnum.CUSTOMER_STANDARD);
			RequestQuestionnaireModel requestQuestionnaireModel = requestQuestionnaireService.getRequestQuestionnaire(qtrId, token.substring(7));
			LOGGER.info("get request questionnaire id: " + qtrId);
			return ResponseEntity.ok().body(new ResponseModel(1, requestQuestionnaireModel, null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get RequestQuestionnaire by id: " + qtrId + " failed, exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	/**
	 * @param standardId standardId
	 * @param token      token
	 * @return StandardCompanyStatusModel
	 */
	@PostMapping("/{standardId}/standardCompanyStatus")
	public ResponseEntity<ResponseModel> createStandardCompanyStatus(@PathVariable(value = "standardId") int standardId,
																	 @RequestHeader(name = "Authorization") String token) {
		try {
			standardService.handlePermission(token, OperationEnum.CREATE, ModuleEnum.CUSTOMER_STANDARD);
			CompanyModel companyModel = authenticationService.getCompanyFromToken(token.substring(7));
			StandardCompanyStatusModel standardCompanyStatusModel = standardCompanyStatusService.createStandardCompanyStatusModel(standardId, companyModel);
			LOGGER.info("Start implementing standard : " + standardId + " in company: " + companyModel.getCmpId() + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, standardCompanyStatusService.getSingleInstance(standardCompanyStatusModel), null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Start implementing standard : " + standardId + " failed, exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	/**
	 * @param token token
	 * @return SingleStandardCompanyStatusModel
	 */
	@GetMapping("/standardCompanyStatus")
	public ResponseEntity<ResponseModel> getStandardCompanyStatus(@RequestHeader(name = "Authorization") String token) {
		try {
			standardService.handlePermission(token, OperationEnum.READ, ModuleEnum.CUSTOMER_STANDARD);
			List<SingleStandardCompanyStatusModel> singleStandardCompanyStatusModels = standardCompanyStatusService.getStandardCompanyStatus(token);
			LOGGER.info("get standard company status by token success");
			return ResponseEntity.ok().body(new ResponseModel(1, singleStandardCompanyStatusModels, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get standard company status by token failed, exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	/**
	 * @param stdDescription stdDescription
	 * @param token          token
	 * @param file           file
	 * @param stdName        stdName
	 * @param stdType        stdType
	 * @param stdVersion     stdVersion
	 * @return Standard
	 */
	@PostMapping("/createStandardByFile")
	public ResponseEntity<ResponseModel> createStandardByFile(@RequestParam("file") MultipartFile file,
															  @RequestParam String stdName,
															  @RequestParam String stdType,
															  @RequestParam String stdVersion,
															  @RequestParam String stdDescription,
															  @RequestHeader(name = "Authorization") String token) {
		try {
			standardService.handlePermission(token, OperationEnum.CREATE, ModuleEnum.CUSTOMER_STANDARD);
			if (file == null) {
				return ResponseEntity.status(500).body(new ResponseModel(0, null, "File not found."));
			}
			if (stdName == null || stdName.isEmpty() || stdType == null || stdType.isEmpty()) {
				return ResponseEntity.status(500).body(new ResponseModel(0, null, "At least one of the mandatory fields is null or empty."));
			}
			if (!Objects.requireNonNull(file.getOriginalFilename()).endsWith(".xlsx") && !file.getOriginalFilename().endsWith(".csv") && !file.getOriginalFilename().endsWith(".xls")) {
				return ResponseEntity.status(500).body(new ResponseModel(0, null, "File is not an Excel file type."));
			}
			StandardModel std = standardService.createCustomStandardFromExcelFile(file, token, stdType, stdName, stdDescription, stdVersion);
			if (std == null) {
				return ResponseEntity.status(500).body(new ResponseModel(0, null, "Cannot read file."));
			}
			LOGGER.info("upload asset file  success.");
			return ResponseEntity.ok().body(new ResponseModel(1, std, null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("upload asset file failed  , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	/**
	 * @param standardId       standardId
	 * @param questionnairesId questionnairesId
	 * @param token            token
	 * @return standardProgrss
	 */
	@GetMapping("/{standardId}/questionnaire/{questionnairesId}/getStandardProgrss")
	public ResponseEntity<ResponseModel> getStandardProgress(@PathVariable(value = "standardId") int standardId,
															 @PathVariable(value = "questionnairesId") int questionnairesId,
															 @RequestHeader(name = "Authorization") String token) {
		try {
			standardService.handlePermission(token, OperationEnum.READ, ModuleEnum.CUSTOMER_STANDARD);
			if (standardService.ifStandardExistInCompany(standardId, token)) {
				StandardProgrssModel standardProgrss = standardService.getStandardProgrss(questionnairesId, token);
				return ResponseEntity.ok().body(new ResponseModel(1, standardProgrss, null));
			} else {
				LOGGER.error("Standard id: " + standardId + " not found");
				return ResponseEntity.status(404).body(new ResponseModel(0, null, "Standard id: " + standardId + " not found"));
			}
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get all avail company standard failed ,exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	/**
	 * @param request duplication request
	 * @param token   Token
	 * @return Questionnaire
	 */
	@PostMapping("/duplicateOriginal")
	public ResponseEntity<ResponseModel> duplicateOriginalQuestionnaire(@RequestBody DuplicateQuestionnaireModel request,
																		@RequestHeader(name = "Authorization") String token) {
		try {
			standardService.handlePermission(token, OperationEnum.CREATE, ModuleEnum.CUSTOMER_STANDARD);

			// Getting the values
			int stdId = request.getStdId();
			int qtrId = request.getQtrId();
			String name = request.getName();
			int parentId = request.getParentId();

			StandardModel standardModel = standardRepository.getStandardById(stdId);
			StdQuestionaireModel stdQuestionaireModel = stdQuestionaireService.getById(qtrId);
			StdQuestionaireModel newStdQuestionnaireModel = stdQuestionaireService.duplicateOriginalQuestionnaire(stdQuestionaireModel, standardModel, name, parentId, token);

			return ResponseEntity.ok().body(new ResponseModel(1, newStdQuestionnaireModel, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Duplicate Standard failed, exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	/**
	 * @param request duplication request
	 * @param token   Token
	 * @return Questionnaire
	 */
	@PostMapping("/duplicate")
	public ResponseEntity<ResponseModel> duplicateQuestionnaire(@RequestBody DuplicateQuestionnaireModel request,
																@RequestHeader(name = "Authorization") String token) {
		try {
			standardService.handlePermission(token, OperationEnum.CREATE, ModuleEnum.CUSTOMER_STANDARD);

			// Getting the values
			int stdId = request.getStdId();
			int qtrId = request.getQtrId();
			String name = request.getName();
			int parentId = request.getParentId();

			StandardModel standardModel = standardRepository.getStandardById(stdId);
			StdQuestionaireModel stdQuestionaireModel = stdQuestionaireService.getById(qtrId);
			StdQuestionaireModel newStdQuestionnaireModel = stdQuestionaireService.duplicateQuestionnaire(stdQuestionaireModel, standardModel, name, parentId, token);

			return ResponseEntity.ok().body(new ResponseModel(1, newStdQuestionnaireModel, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Duplicate Standard failed, exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@DeleteMapping("{standardId}/questionnaire/{id}/shouldDeleteAll/{shouldDeleteAll}")
	public ResponseEntity<ResponseModel> deleteCisoDuplicatedQuestionnaire(@PathVariable(value = "standardId") int standardId,
																		   @PathVariable(value = "shouldDeleteAll") boolean shouldDeleteAll,
																		   @PathVariable(value = "id") int id, @RequestHeader(name = "Authorization") String token) {
		try {
			standardService.handlePermission(token, OperationEnum.DELETE, ModuleEnum.CUSTOMER_STANDARD);
			StdQuestionaireModel stdQuestionaireModel = stdQuestionaireService.deleteCisoDuplicatedQuestionnaire(standardId, id, shouldDeleteAll);
			LOGGER.info("Delete Questionnaire ,id:" + id + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, stdQuestionaireService.getSingleInstance(stdQuestionaireModel), null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Delete section failed, exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@PutMapping("{standardId}/questionnaire/{questionnaireId}")
	public ResponseEntity<ResponseModel> updateQuestionnaire(@PathVariable(value = "questionnaireId") int questionnaireId,
															 @PathVariable(value = "standardId") int standardId,
															 @Validated(CRUDModel.CRUDCreation.class) @RequestBody StdQuestionaireModel stdQuestionaireModel,
															 @RequestHeader(name = "Authorization") String token) {
		try {
			standardService.handlePermission(token, OperationEnum.UPDATE, ModuleEnum.CUSTOMER_STANDARD);
			StdQuestionaireModel updatedStdQuestionnaireModel = stdQuestionaireService.update(questionnaireId, stdQuestionaireModel);
			return ResponseEntity.ok().body(new ResponseModel(1, updatedStdQuestionnaireModel, null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Create new Questionnaire failed ,exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@PutMapping("{standardId}/questionnaire/{questionnaireId}/reqHeaderTitles")
	public ResponseEntity<ResponseModel> updateRequirementsHeaderTitles(@PathVariable(value = "questionnaireId") int questionnaireId,
																		@PathVariable(value = "standardId") int standardId,
																		@Validated(CRUDModel.CRUDCreation.class) @RequestBody RequirementsHeaderTitlesModel requirementsHeaderTitlesModel,
																		@RequestHeader(name = "Authorization") String token) {
		try {
			standardService.handlePermission(token, OperationEnum.UPDATE, ModuleEnum.CUSTOMER_STANDARD);
			RequirementsHeaderTitlesModel savedRequirementsHeaderTitlesModel = requirementsHeaderTitlesService.saveRequirementsHeaderTitles(requirementsHeaderTitlesModel, questionnaireId);
			return ResponseEntity.ok().body(new ResponseModel(1, savedRequirementsHeaderTitlesModel, null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Create new Questionnaire failed ,exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@PutMapping("{standardId}/questionnaire/{questionnaireId}/section/{sectionId}/updateAllAnswersInSection")
	public ResponseEntity<ResponseModel> updateAllAnswersInSection(@PathVariable(value = "questionnaireId") int questionnaireId,
																   @PathVariable(value = "standardId") int standardId,
																   @PathVariable(value = "sectionId") int sectionId,
																   @RequestBody Answer answer,
																   @RequestHeader(name = "Authorization") String token) {
		try {
			standardService.handlePermission(token, OperationEnum.UPDATE, ModuleEnum.CUSTOMER_STANDARD);
			CompanyModel company = authenticationService.getCompanyFromToken(token.substring(7));
			ComplianceAnswer complianceAnswer = answer.getAnswer();
			stdSectionService.updateAllAnswersInSection(sectionId, complianceAnswer.getAnswer(), company);
			return ResponseEntity.ok().body(new ResponseModel(1, null, null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Create new Questionnaire failed ,exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}


}
