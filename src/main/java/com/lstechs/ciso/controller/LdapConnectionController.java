package com.lstechs.ciso.controller;

import com.lstechs.ciso.config.ActiveDirectoryConnectionUtils;
import com.lstechs.ciso.enums.ModuleEnum;
import com.lstechs.ciso.model.CRUDModel;
import com.lstechs.ciso.model.LdapConnectionModel;
import com.lstechs.ciso.model.ResponseModel;
import com.lstechs.ciso.model.SingleLdapConnectionModel;
import com.lstechs.ciso.service.LdapConnectionService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/ldapConnection")
public class LdapConnectionController extends CRUDController<LdapConnectionModel, SingleLdapConnectionModel> {

	@Autowired
	LdapConnectionService ldapConnectionService;

	@Autowired
	private ActiveDirectoryConnectionUtils adConnectionUtils;

	private static Logger LOGGER = LogManager.getLogger(CompanyController.class.getName());


	public LdapConnectionController(LdapConnectionService ldapConnectionService) {
		super(ldapConnectionService, ModuleEnum.VENDOR);
	}

	@Override
	@PostMapping
	public ResponseEntity<ResponseModel> createRecord(@Validated(CRUDModel.CRUDUpdate.class) @RequestBody LdapConnectionModel ldapConnectionModel,
													  @RequestHeader(name = "Authorization") String token) throws Exception {
		try {
			if (!adConnectionUtils.checkLdapConnection(ldapConnectionModel.getFullUrl(), ldapConnectionModel.getLdapManagerDn(), ldapConnectionModel.getLdapManagerPassword())) {
				// connect with the first url failed
				if (!adConnectionUtils.checkLdapConnection(ldapConnectionModel.getFullUrl2(), ldapConnectionModel.getLdapManagerDn(), ldapConnectionModel.getLdapManagerPassword())) {
					// connect with the second url failed
					LOGGER.error("Connect to ldap failed");
					return ResponseEntity.status(500).body(new ResponseModel(0, null, "ldap connection not created"));
				} else {
					//connect with the second url success
					LdapConnectionModel newLdapConnectionModel = ldapConnectionService.create(ldapConnectionModel);
					LOGGER.info("Creat ldap connection success with getFullUrl2.");
					return ResponseEntity.ok().body(new ResponseModel(1, newLdapConnectionModel, null));
				}
			} else {
				//connect with the first url success
				LdapConnectionModel newLdapConnectionModel = ldapConnectionService.create(ldapConnectionModel);
				LOGGER.info("Creat ldap connection success with getFullUrl.");
				return ResponseEntity.ok().body(new ResponseModel(1, newLdapConnectionModel, null));
			}
		} catch (Exception ex) {
			LOGGER.error("Create or connect to ldap failed", ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}
}
