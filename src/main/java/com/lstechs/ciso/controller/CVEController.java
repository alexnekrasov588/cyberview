package com.lstechs.ciso.controller;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.lstechs.ciso.enums.ModuleEnum;
import com.lstechs.ciso.enums.OperationEnum;
import com.lstechs.ciso.model.*;
import com.lstechs.ciso.service.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/cve")
public class CVEController extends CRUDController<CVEModel, SingleCVEModel> {

	@Autowired
	private FavouriteCVEService favouriteCVEService;

	@Autowired
	private AssetService assetService;

	@Autowired
	private CVEService cveService;

	@Autowired
	private AuthenticationService authenticationService;

	private static final Logger LOGGER = LogManager.getLogger(CVEController.class.getName());

	public CVEController(CVEService cveService) {
		super(cveService, ModuleEnum.CVE);
	}

	@GetMapping("/all")
	public ResponseEntity<ResponseModel> getAllCVERecords(@RequestParam(defaultValue = "0") Integer pageNo,
													   @RequestParam(defaultValue = "10") Integer pageSize,
													   @RequestParam(defaultValue = "id") String orderBy,
													   @RequestParam(defaultValue = "true") boolean orderByDesc,
													   @RequestParam(defaultValue = "") String query,
													   @RequestParam(defaultValue = "false") boolean isAssetMode,
													   @RequestParam Map<String, String> allParams,
													   @RequestHeader(name = "Authorization") String token) throws Exception {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.CVE);
			CompanyModel companyModel = authenticationService.getCompanyFromToken(token.substring(7));
			Boolean hasAssets = assetService.checkForCompanyAssetsStatus(companyModel.getCmpId());
			Map<String, Object> map;

			if (!hasAssets) {
				Page<CVEModel> cveModels = cveService.getAllCVEs(false,pageNo, pageSize, orderBy, orderByDesc, query, token.substring(7));
				map = cveService.getAllResponse(cveModels);
				map.put("hasAssets", false);
			} else {
				Page<CVEModel> cveModels = cveService.getAllCVEs(isAssetMode, pageNo, pageSize, orderBy, orderByDesc, query, token.substring(7));
				map = cveService.getAllResponse(cveModels);
				map.put("hasAssets", true);
			}
			LOGGER.info("get all CVEs success.");
			return ResponseEntity.ok().body(new ResponseModel(1, map, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get all CVEs failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@PostMapping("/favorite/{cveId}")
	public ResponseEntity<ResponseModel> favorite(@PathVariable(value = "cveId") int cveId,
												  @RequestBody ObjectNode objectNode,
												  @RequestParam(defaultValue = "CREATE") String action,
												  @RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.UPDATE, ModuleEnum.CVE);
			String name = objectNode.get("name").asText();
			int folderId = 0;
			try {
				folderId = objectNode.get("folderId").asInt();
			} catch (Exception ex) {
				folderId = 0;
			}

			FavouriteCVEModel favouriteCVEModel = favouriteCVEService.favorite(cveId, token, name, folderId, action);
			LOGGER.info("sign favorite cve success cve id: " + cveId);
			return ResponseEntity.ok().body(new ResponseModel(1, favouriteCVEModel, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("sign favorite cve failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("/favoriteNews")
	public ResponseEntity<ResponseModel> favoriteNews(@RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.CVE);
			List<FavouriteCVEModel> cveModels = favouriteCVEService.getFavoriteByUserId(token);
			LOGGER.info("get all favourite cve success");
			return ResponseEntity.ok().body(new ResponseModel(1, cveModels, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get all favourite cve  failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

}
