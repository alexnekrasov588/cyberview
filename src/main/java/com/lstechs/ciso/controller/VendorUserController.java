package com.lstechs.ciso.controller;

import com.lstechs.ciso.enums.ModuleEnum;
import com.lstechs.ciso.enums.OperationEnum;
import com.lstechs.ciso.model.CRUDModel;
import com.lstechs.ciso.model.ResponseModel;
import com.lstechs.ciso.model.SingleVendorUSerModel;
import com.lstechs.ciso.model.VendorUserModel;
import com.lstechs.ciso.service.AuthenticationService;
import com.lstechs.ciso.service.VendorUserService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/vendorUser")
public class VendorUserController extends CRUDController<VendorUserModel, SingleVendorUSerModel> {

    @Autowired
    AuthenticationService authenticationService;

    @Autowired
    VendorUserService vendorUserService;

    private static Logger LOGGER = LogManager.getLogger(VendorUserController.class.getName());

    public VendorUserController(final VendorUserService vendorUserService) {
        super(vendorUserService, ModuleEnum.VENDOR);
    }

    @Override
    @PutMapping("/{id}")
    public ResponseEntity<ResponseModel> updateRecord(@PathVariable(value = "id") final int id,
            @Validated(CRUDModel.CRUDUpdate.class) @RequestBody final VendorUserModel updatedVndrUserModel,
            @RequestHeader(name = "Authorization") final String token) throws Exception {
        try {
            handlePermission(token, OperationEnum.UPDATE, ModuleEnum.VENDOR);
            VendorUserModel vendorUserModel = authenticationService.getVendorUserFromToken(token.substring(7));
            if (vendorUserModel.getVndrId() == updatedVndrUserModel.getVndrId()
                    && !vendorUserModel.getVndrEmail().equals(updatedVndrUserModel.getVndrEmail())) {
                throw new Exception("Vendor User cannot change his own email adress");
            }
            VendorUserModel newVendorUserModel = vendorUserService.update(id, updatedVndrUserModel);
            LOGGER.info("Update Vendor User id: " + id + ", success.");
            return ResponseEntity.ok().body(new ResponseModel(1, newVendorUserModel, null));

        } catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Update Vendor User id: " + id + " failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
    }

}
