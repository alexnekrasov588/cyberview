package com.lstechs.ciso.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.lstechs.ciso.enums.ModuleEnum;
import com.lstechs.ciso.enums.OperationEnum;
import com.lstechs.ciso.model.ResponseModel;
import com.lstechs.ciso.model.ThreatModel;
import com.lstechs.ciso.service.ThreatService;

@RestController
@RequestMapping("/threat")
public class ThreatController extends CRUDController<ThreatModel, ThreatModel> {

	@Autowired
	public ThreatService threatService;

	private static final Logger LOGGER = LogManager.getLogger(ThreatController.class.getName());

	public ThreatController(ThreatService threatService) {
		super(threatService, ModuleEnum.PROJECT);
	}

	@GetMapping("/getAll")
	public ResponseEntity<ResponseModel> getAll(@RequestHeader(name = "Authorization") String token) throws Exception {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.PROJECT);
			List<ThreatModel> threatModels = threatService.getAll();
			LOGGER.info("get all threat success.");
			return ResponseEntity.ok().body(new ResponseModel(1, threatModels, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get all threat failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("/getAdversarialThreats")
	public ResponseEntity<ResponseModel> getAdversarialThreats(@RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.PROJECT);
			List<ThreatModel> threatModels = threatService.getAdversarialThreats();
			LOGGER.info("get all threat success.");
			return ResponseEntity.ok().body(new ResponseModel(1, threatModels, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get all threat failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("/getNonAdversarialThreats")
	public ResponseEntity<ResponseModel> getNonAdversarialThreats(@RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.PROJECT);
			List<List<ThreatModel>> threatModels = threatService.getNonAdversarialThreats();
			LOGGER.info("get all threat success.");
			return ResponseEntity.ok().body(new ResponseModel(1, threatModels, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get all threat failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}


	@PutMapping("/upload")
	public ResponseEntity<ResponseModel> uploadThreat(@RequestParam("file") MultipartFile file,
													  @RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.UPDATE, ModuleEnum.PROJECT);
			if (file == null) {
				return ResponseEntity.status(500).body(new ResponseModel(0, null, "File not found."));
			}
			List<ThreatModel> threatModel = threatService.createThreatFromExcelFile(file);
			if (threatModel == null || threatModel.size() == 0) {
				return ResponseEntity.status(500).body(new ResponseModel(0, null, "Cannot read file."));
			}
			LOGGER.info("upload threat file  success.");
			return ResponseEntity.ok().body(new ResponseModel(1, threatModel, null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("upload threat file failed  , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

}
