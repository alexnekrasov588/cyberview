package com.lstechs.ciso.controller;

import com.lstechs.ciso.enums.ModuleEnum;
import com.lstechs.ciso.model.NewsArticlesTagsModel;
import com.lstechs.ciso.model.SingleNewsArticlesTagsModel;
import com.lstechs.ciso.service.NewsArticlesTagsService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/artTags")
public class ArticlesTagsController extends CRUDController<NewsArticlesTagsModel, SingleNewsArticlesTagsModel> {

    public ArticlesTagsController(NewsArticlesTagsService newsArticlesTagsService) {
        super(newsArticlesTagsService,ModuleEnum.NEWS);
    }
}
