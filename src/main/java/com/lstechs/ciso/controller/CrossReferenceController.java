package com.lstechs.ciso.controller;

import com.lstechs.ciso.enums.ModuleEnum;
import com.lstechs.ciso.enums.OperationEnum;
import com.lstechs.ciso.model.CrossReferenceModel;
import com.lstechs.ciso.model.CrossReferenceReportResultModel;
import com.lstechs.ciso.model.ResponseModel;
import com.lstechs.ciso.model.SingleCrossReferenceModel;
import com.lstechs.ciso.service.CrossReferenceService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/crossReference")
public class CrossReferenceController extends CRUDController<CrossReferenceModel, SingleCrossReferenceModel> {
	public CrossReferenceController(CrossReferenceService crossReferenceService) {
		super(crossReferenceService, ModuleEnum.VENDOR);
	}

	@Autowired
	CrossReferenceService crossReferenceService;

	private static Logger LOGGER = LogManager.getLogger(CrossReferenceController.class.getName());

	@GetMapping("/report")
	public ResponseEntity<ResponseModel> getCross(@RequestHeader(name = "Authorization") String token) throws Exception {

		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.CUSTOMER_STANDARD);
			Collection<CrossReferenceReportResultModel> crossReferenceReport = crossReferenceService.crossReferenceReport();
			LOGGER.info("cross reference report success.");
			return ResponseEntity.ok().body(new ResponseModel(1, crossReferenceReport, null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN, exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("cross reference report failed, exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}
}
