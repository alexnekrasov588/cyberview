package com.lstechs.ciso.controller;

import com.lstechs.ciso.enums.ModuleEnum;
import com.lstechs.ciso.enums.OperationEnum;
import com.lstechs.ciso.model.*;
import com.lstechs.ciso.service.CrossReferenceService;
import com.lstechs.ciso.service.CustomRequirementService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/customRequirement")
public class CustomRequirementController {

	@Autowired
	CustomRequirementService customRequirementService;

	@Autowired
	CrossReferenceService crossReferenceService;

	private static Logger LOGGER = LogManager.getLogger(CustomRequirementController.class.getName());

	public void handlePermission(String token, OperationEnum operationEnum, ModuleEnum moduleEnum) {
		customRequirementService.handlePermission(token, operationEnum, moduleEnum);
	}


	@GetMapping
	public ResponseEntity<ResponseModel> getAllRecords(@RequestHeader(name = "Authorization") String token) throws Exception {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.VENDOR);
			List<CustomRequirementModel> customRequirementModels = customRequirementService.getAll();
			List<SingleCustomRequirementModel> singleCustomRequirementModels = new ArrayList<>();
			for (CustomRequirementModel c: customRequirementModels )
				singleCustomRequirementModels.add(customRequirementService.getSingleInstance(c));

			LOGGER.info("Get all Custom Requirements.");
			return ResponseEntity.ok().body(new ResponseModel(1, singleCustomRequirementModels, null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Create or Update Custom Requirement failed, exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@PostMapping
	public ResponseEntity<ResponseModel> createOrUpdateCustomReq(@Validated( CRUDModel.CRUDCreation.class) @RequestBody RequestCustomRequirementModel requestCustomRequirementModel,
																 @RequestHeader(name = "Authorization") String token) throws Exception {
		try {
			handlePermission(token, OperationEnum.CREATE, ModuleEnum.VENDOR);
			CustomRequirementModel newCustomRequirementModel = customRequirementService.requestParser(requestCustomRequirementModel);

			LOGGER.info(requestCustomRequirementModel.getCstRqrId() == 0 ? "Create Custom Requirement" :  "Update Custom Requirement" + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, customRequirementService.getSingleInstance(newCustomRequirementModel), null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Create or Update Custom Requirement failed, exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@DeleteMapping("/{cstReqId}")
	public ResponseEntity<ResponseModel> deleteRecord(@PathVariable(value = "cstReqId") int cstReqId,
													@RequestHeader(name = "Authorization") String token) throws Exception {
		try {
			handlePermission(token, OperationEnum.DELETE, ModuleEnum.VENDOR);
			customRequirementService.delete(cstReqId);
			LOGGER.info("Delete Custom Requirement id: " + cstReqId + " success.");

			return ResponseEntity.ok().body(new ResponseModel(1, cstReqId, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Delete Custom Requirement failed, exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}


}
