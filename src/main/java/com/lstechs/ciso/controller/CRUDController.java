package com.lstechs.ciso.controller;

import com.lstechs.ciso.enums.ModuleEnum;
import com.lstechs.ciso.enums.OperationEnum;
import com.lstechs.ciso.model.CRUDModel;
import com.lstechs.ciso.model.ResponseModel;
import com.lstechs.ciso.service.CRUDService;
import javassist.NotFoundException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;


public abstract class CRUDController<T extends CRUDModel,P> implements ICRUDController<T,P> {

	private CRUDService<T,P> crudService;
	private ModuleEnum moduleEnum;


	private static final Logger LOGGER = LogManager.getLogger(CRUDController.class.getName());

	public CRUDController(CRUDService<T,P> crudService, ModuleEnum moduleEnum) {
		this.moduleEnum = moduleEnum;
		this.crudService = crudService;
	}

	public void handlePermission(String token, OperationEnum operationEnum, ModuleEnum moduleEnum) {
		crudService.handlePermission(token, operationEnum, moduleEnum);
	}

	@Override
	@GetMapping
	public ResponseEntity<ResponseModel> getAllRecords(@RequestParam(defaultValue = "0") Integer pageNo,
													   @RequestParam(defaultValue = "10") Integer pageSize,
													   @RequestParam(defaultValue = "id") String orderBy,
													   @RequestParam(defaultValue = "true") boolean orderByDesc,
													   @RequestParam(defaultValue = "") String query,
													   @RequestParam Map<String,String> allParams,
													   @RequestHeader(name = "Authorization") String token) throws Exception {
		try {
			handlePermission(token, OperationEnum.READ, moduleEnum);
			Page<T> t = crudService.getAll(pageNo, pageSize, orderBy,orderByDesc, query);
			Map<String, Object> map = crudService.getAllResponse(t);
			LOGGER.info("get all from " + crudService.getClass().getSimpleName() + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, map, null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get all from" + crudService.getClass().getSimpleName() + " failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@Override
	@GetMapping("/{id}")
	public ResponseEntity<ResponseModel> getRecordById(@PathVariable(value = "id") int id, @RequestHeader(name = "Authorization") String token) throws Exception {

		try {
			handlePermission(token, OperationEnum.READ, moduleEnum);
			T t = crudService.getById(id);
			LOGGER.info("get " + t.getClass().getSimpleName() + " by id: " + id + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, crudService.getSingleInstance(t), null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (NotFoundException ex) {
			LOGGER.error("get by id: " + id + " from " + crudService.getClass().getSimpleName() + " failed , exception: " + ex);
			return ResponseEntity.badRequest().body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get by id: " + id + " from " + crudService.getClass().getSimpleName() + " failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@Override
	@PostMapping
	public ResponseEntity<ResponseModel> createRecord(@Validated(CRUDModel.CRUDCreation.class) @RequestBody T t, @RequestHeader(name = "Authorization") String token) throws Exception {
		try {
			handlePermission(token, OperationEnum.CREATE, moduleEnum);
			T tNew = crudService.create(t);
			LOGGER.info("create new " + t.getClass().getSimpleName() + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, crudService.getSingleInstance(tNew), null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("create new " + crudService.getClass().getSimpleName() + " failed, exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@Override
	@PutMapping("/{id}")
	public ResponseEntity<ResponseModel> updateRecord(@PathVariable(value = "id") int id,
													  @Validated(CRUDModel.CRUDUpdate.class) @RequestBody T t,
													  @RequestHeader(name = "Authorization") String token) throws Exception {
		try {
			handlePermission(token, OperationEnum.UPDATE, moduleEnum);
			T tNew = crudService.update(id, t);
			LOGGER.info("update  " + t.getClass().getSimpleName() + " id: " + id + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, crudService.getSingleInstance(tNew), null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (NotFoundException ex) {
			LOGGER.error("update  " + t.getClass().getSimpleName() + " id: " + id + " failed , exception: " + ex);
			return ResponseEntity.badRequest().body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("update  " + t.getClass().getSimpleName() + " id: " + id + " failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@Override
	@DeleteMapping("/{id}")
	public ResponseEntity<ResponseModel> deleteRecord(@PathVariable(value = "id") int id, @RequestHeader(name = "Authorization") String token) throws Exception {
		try {
			handlePermission(token, OperationEnum.DELETE, moduleEnum);
			T t = crudService.delete(id);
			LOGGER.info("delete  " + t.getClass().getSimpleName() + " id: " + id + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, crudService.getSingleInstance(t), null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (NotFoundException ex) {
			LOGGER.error("delete  " + crudService.getClass().getSimpleName() + " id: " + id + " failed , exception: " + ex);
			return ResponseEntity.badRequest().body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("delete  " + crudService.getClass().getSimpleName() + " id: " + id + " failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("/sync/{updatedAt}")
	public @ResponseBody List<T> getSyncRecords(@PathVariable("updatedAt") String updatedAt) throws Exception {
		Date update;
		try {

			try {
				update = new Date(Long.parseLong(updatedAt));
			}catch (Exception ex) {
				 update = new Date(0);
			}

			List<T> t = crudService.getSyncList(update);
			LOGGER.info("sync from " + crudService.getClass().getSimpleName() + " success.");
			return t;
//			return ResponseEntity.ok().body(t);

		} catch (Exception ex) {
			LOGGER.error("sync from" + crudService.getClass().getSimpleName() + " failed , exception: " + ex);
//			return ResponseEntity.status(500).body(null);
			return null;
		}
	}


}
