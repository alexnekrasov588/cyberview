package com.lstechs.ciso.controller;


import com.fasterxml.jackson.databind.node.ObjectNode;
import com.lstechs.ciso.config.JwtTokenUtil;
import com.lstechs.ciso.enums.ModuleEnum;
import com.lstechs.ciso.enums.OperationEnum;
import com.lstechs.ciso.model.*;
import com.lstechs.ciso.service.*;
import javassist.NotFoundException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/news")
public class NewsArticlesController extends CRUDController<NewsArticlesModel, SingleNewsArticlesModel> {

	public NewsArticlesController(NewsArticlesService newsArticlesService) {
		super(newsArticlesService, ModuleEnum.NEWS);
	}

	@Autowired
	private FavouriteNewsService favouriteNewsService;

	@Autowired
	private NewsTagsService newsTagsService;

	@Autowired
	private NewsArticlesService newsArticlesService;

	@Autowired
	private FavouriteFoldersService favouriteFoldersService;

	@Autowired
	private AssetService assetService;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private AuthenticationService authenticationService;

	private static final Logger LOGGER = LogManager.getLogger(NewsArticlesController.class.getName());

	@Override
	@GetMapping("/{newsArticleId}")
	public ResponseEntity<ResponseModel> getRecordById(@PathVariable(value = "newsArticleId") int newsArticleId, @RequestHeader(name = "Authorization") String token) throws Exception {

		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.NEWS);
			NewsArticlesModel newsArticleModel = newsArticlesService.getById(newsArticleId);
			LOGGER.info("get news article by id: " + newsArticleId + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, newsArticleModel, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (NotFoundException ex) {
			LOGGER.error("get news article by id: " + newsArticleId + " failed , exception: " + ex);
			return ResponseEntity.badRequest().body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get news article by id: " + newsArticleId + " failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@PostMapping("/favorite/{newsArticleId}")
	public ResponseEntity<ResponseModel> favorite(@PathVariable(value = "newsArticleId") int newsArticleId,
												  @RequestBody ObjectNode objectNode,
												  @RequestParam(defaultValue = "CREATE") String action,
												  @RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.UPDATE, ModuleEnum.NEWS);
			String name = objectNode.get("name").asText();
			int folderId = 0;
			try {
				folderId = objectNode.get("folderId").asInt();
			} catch (Exception ex) {
				folderId = 0;
			}
			FavouriteNewsModel favouriteNewsModel = favouriteNewsService.favorite(newsArticleId, token, name, folderId, action);
			LOGGER.info("sign favorite news success favouriteNews id: " + newsArticleId);
			return ResponseEntity.ok().body(new ResponseModel(1, favouriteNewsModel, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN", ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("sign favorite news failed", ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}


	@GetMapping("/favoriteNews")
	public ResponseEntity<ResponseModel> favoriteNews(@RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.NEWS);
			List<FavouriteNewsModel> favouriteNewsModels = favouriteNewsService.getFavoriteByUserId(token);
			LOGGER.info("get all favourite News success");
			return ResponseEntity.ok().body(new ResponseModel(1, favouriteNewsModels, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get all favourite news failed ,exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("/favoriteNewsWithPaging")
	public ResponseEntity<ResponseModel> favoriteNewsWithPaging(@RequestParam(defaultValue = "0") Integer pageNo,
																@RequestParam(defaultValue = "10") Integer pageSize,
																@RequestParam(defaultValue = "id") String orderBy,
																@RequestParam(defaultValue = "true") boolean orderByDesc,
																@RequestHeader(name = "Authorization") String token) throws Exception {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.NEWS);
			Page<NewsArticlesModel> favouriteNewsModels = favouriteNewsService.getFavoriteByUserIdWithPaging(pageNo, pageSize, orderBy, orderByDesc, token);
			Map<String, Object> map = newsArticlesService.getAllResponse(favouriteNewsModels);
			LOGGER.info("get all favourite News success");
			return ResponseEntity.ok().body(new ResponseModel(1, map, null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get all favourite news failed ,exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("/autocomplete")
	public ResponseEntity<ResponseModel> autocomplete(@RequestParam(defaultValue = "0") Integer pageNo,
													  @RequestParam(defaultValue = "10") Integer pageSize,
													  @RequestParam(defaultValue = "newsTagName") String orderBy,
													  @RequestParam(defaultValue = "") String query,
													  @RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.NEWS);
			Page<NewsTagsModel> newsTagsModels = newsTagsService.autoComplete(pageNo, pageSize, orderBy, query);
			Map<String, Object> map = newsTagsService.getAllResponse(newsTagsModels);
			LOGGER.info("autocomplete query: " + query + " success");
			return ResponseEntity.ok().body(new ResponseModel(1, map, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("autocomplete query: " + query + " ,exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

//	@GetMapping("/newsTags")
//	public ResponseEntity<ResponseModel> getNewsArticleByTagsId(@RequestParam("id") int[] tagsId, @RequestHeader(name = "Authorization") String token) {
//		try {
//			handlePermission(token, OperationEnum.READ, ModuleEnum.NEWS);
//			List<NewsArticlesModel> newsArticlesModels = newsArticlesTagsService.getArticlesTagsByTagsId(tagsId);
//			LOGGER.info("get news articles by tags id success");
//			return ResponseEntity.ok().body(new ResponseModel(1, newsArticlesModels, null));
//		} catch (SecurityException ex) {
//			LOGGER.error("FORBIDDEN , exception: " + ex);
//			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
//		} catch (Exception ex) {
//			LOGGER.error("get news articles by tags id failed ,exception: " + ex);
//			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
//		}
//	}

	@PutMapping("/{id}/isActive")
	public ResponseEntity<ResponseModel> isActiveNews(@RequestHeader(name = "Authorization") String token,
													  @PathVariable(value = "id") int id) {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.VENDOR);
			NewsArticlesModel newsArticlesModel = newsArticlesService.getById(id);
			newsArticlesModel.setNewsIsActive(!newsArticlesModel.getNewsIsActive());
			NewsArticlesModel newsArticles = newsArticlesService.update(id, newsArticlesModel);

			LOGGER.info("update is active for id: " + id + " success");
			return ResponseEntity.ok().body(new ResponseModel(1, newsArticlesService.getSingleInstance(newsArticles), null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get all favourite news failed ,exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("/filterByTags")
//	@Cacheable(value="filterByTags")
	public ResponseEntity<ResponseModel> getAllRecords(@RequestParam(defaultValue = "0") Integer pageNo,
													   @RequestParam(defaultValue = "10") Integer pageSize,
													   @RequestParam(defaultValue = "newsArtcId") String orderBy,
													   @RequestParam(defaultValue = "true") boolean orderByDesc,
													   @RequestParam(defaultValue = "") String query,
													   @RequestParam(defaultValue = "") int[] tagsId,
													   @RequestParam(defaultValue = "false") boolean isAssetMode,
													   @RequestHeader(name = "Authorization") String token) throws Exception {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.NEWS);

			Page<NewsArticlesModel> newsArticlesModels = newsArticlesService.getAllNews(isAssetMode, pageNo, pageSize, orderBy, orderByDesc, query, tagsId, token.substring(7));
			Map<String, Object> map = newsArticlesService.getAllResponse(newsArticlesModels);
			LOGGER.info("get all news success.");
			return ResponseEntity.ok().body(new ResponseModel(1, map, null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get all news failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@Override
	@GetMapping
	public ResponseEntity<ResponseModel> getAllRecords(@RequestParam(defaultValue = "0") Integer pageNo,
													   @RequestParam(defaultValue = "10") Integer pageSize,
													   @RequestParam(defaultValue = "newsArtcId") String orderBy,
													   @RequestParam(defaultValue = "true") boolean orderByDesc,
													   @RequestParam(defaultValue = "") String query,
													   @RequestParam Map<String, String> allParams,
													   @RequestHeader(name = "Authorization") String token) throws Exception {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.NEWS);
			Page<NewsArticlesModel> newsArticlesModels = newsArticlesService.getAllNews(false, pageNo, pageSize, orderBy, orderByDesc, query, null, token.substring(7));
			Map<String, Object> map = newsArticlesService.getAllResponse(newsArticlesModels);
			LOGGER.info("get all news success.");
			return ResponseEntity.ok().body(new ResponseModel(1, map, null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get all news failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	/**
	 * @param pageNo      pageNo
	 * @param pageSize    pageSize
	 * @param orderBy     orderBy
	 * @param orderByDesc orderByDesc
	 * @param query       query
	 * @param tagsId      tagsId
	 * @param isAssetMode isAssetMode
	 * @param token       token
	 * @return News[]
	 * @throws Exception Exception
	 */
	@GetMapping("/all")
	public ResponseEntity<ResponseModel> getAll(@RequestParam(defaultValue = "0") Integer pageNo,
												@RequestParam(defaultValue = "10") Integer pageSize,
												@RequestParam(defaultValue = "newsArtcId") String orderBy,
												@RequestParam(defaultValue = "true") boolean orderByDesc,
												@RequestParam(name = "query", defaultValue = "") String query,
												@RequestParam(defaultValue = "") int[] tagsId,
												@RequestParam(defaultValue = "false") boolean isAssetMode,
												@RequestHeader(name = "Authorization") String token) throws Exception {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.NEWS);
			CompanyModel companyModel = authenticationService.getCompanyFromToken(token.substring(7));
			Boolean hasAssets = assetService.checkForCompanyAssetsStatus(companyModel.getCmpId());

			if (!hasAssets) {
				Page<NewsArticlesModel> newsArticlesModels = newsArticlesService.getAllNews(false, pageNo, pageSize, orderBy, orderByDesc, query, tagsId, token.substring(7));
				Map<String, Object> map = newsArticlesService.getAllResponse(newsArticlesModels);
				LOGGER.info("get all news success.");
				map.put("hasAssets", false);
				return ResponseEntity.ok().body(new ResponseModel(1, map, null));
			} else {
				Page<NewsArticlesModel> newsArticlesModels = newsArticlesService.getAllNews(isAssetMode, pageNo, pageSize, orderBy, orderByDesc, query, tagsId, token.substring(7));
				Map<String, Object> map = newsArticlesService.getAllResponse(newsArticlesModels);
				LOGGER.info("get all news success.");
				map.put("hasAssets", true);
				return ResponseEntity.ok().body(new ResponseModel(1, map, null));
			}

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get all news failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("/getNewsArticle/{id}")
	public ResponseEntity<ResponseModel> getSingleArticle(@PathVariable(value = "id") int id,
														  @RequestHeader(name = "Authorization") String token) throws Exception {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.NEWS);
			NewsArticlesModel newsArticlesModel = newsArticlesService.getNewsArticle(id);
			LOGGER.info("Getting single news article succeeded.");
			return ResponseEntity.ok().body(new ResponseModel(1, newsArticlesModel, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get all news failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}
}
