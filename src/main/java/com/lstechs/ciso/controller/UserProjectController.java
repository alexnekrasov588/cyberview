package com.lstechs.ciso.controller;

import com.lstechs.ciso.enums.ModuleEnum;
import com.lstechs.ciso.model.SingleUserProjectModel;
import com.lstechs.ciso.model.UserProjectModel;
import com.lstechs.ciso.service.UserProjectService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/userProject")
public class UserProjectController extends CRUDController<UserProjectModel, SingleUserProjectModel> {
	public UserProjectController(UserProjectService userProjectService) {
		super(userProjectService, ModuleEnum.PROJECT);
	}

}
