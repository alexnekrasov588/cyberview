package com.lstechs.ciso.controller;

import com.lstechs.ciso.config.JwtTokenUtil;
import com.lstechs.ciso.enums.ModuleEnum;
import com.lstechs.ciso.enums.OperationEnum;
import com.lstechs.ciso.model.*;
import com.lstechs.ciso.service.CompanyService;
import com.lstechs.ciso.service.LicenseService;
import com.lstechs.ciso.service.RoleService;
import com.lstechs.ciso.service.StandardService;
import com.lstechs.ciso.service.SyncService;

import io.jsonwebtoken.Claims;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.*;

@RestController
@RequestMapping("/license")
public class LicenseController extends CRUDController<LicenseModel, SingleLicenseModel> {

	public LicenseController(LicenseService licenseService) {
		super(licenseService, ModuleEnum.VENDOR);
	}

	@Autowired
	private LicenseService licenseService;

	@Autowired
	private StandardService standardService;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private CompanyService companyService;

	@Autowired
	private RoleService roleService;

	@Autowired
	private SyncService syncService;

	@Value("${license.folder}")
	private String filePath;

	private static final Logger LOGGER = LogManager.getLogger(ProjectController.class.getName());

	@GetMapping("/getAll")
	public ResponseEntity<ResponseModel> getAllLicense(@RequestParam(defaultValue = "0") Integer pageNo,
													   @RequestParam(defaultValue = "10") Integer pageSize,
													   @RequestParam(defaultValue = "id") String orderBy,
													   @RequestParam(defaultValue = "true") boolean orderByDesc,
													   @RequestParam(defaultValue = "") String query,
													   @RequestParam(defaultValue = "false") Boolean unassigned,
													   @RequestHeader(name = "Authorization") String token) throws Exception {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.VENDOR);
			Page<LicenseModel> licenseModels = licenseService.getAllLicense(pageNo, pageSize, orderBy, orderByDesc, query, unassigned);

			Map<String, Object> map = licenseService.getAllResponse(licenseModels);

			LOGGER.info("get all licenses success.");
			return ResponseEntity.ok().body(new ResponseModel(1, map, null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get all licenses failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("/{licenseId}/file")
	public HttpEntity<?> getLicenseFile(@PathVariable Integer licenseId, HttpServletResponse response, @RequestHeader(name = "Authorization") String token) throws Exception {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.VENDOR);
			String getToken = licenseService.getLicenseToken(licenseId);
			return licenseService.getLicenseFile(licenseId, getToken);
		} catch (Exception ex) {
			LOGGER.error("Download license file failed, Exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}

	}

	@PostMapping("/onPrem")
	public ResponseEntity<ResponseModel> createLicense(@RequestParam("file") MultipartFile file) {
		try {
//			handlePermission(token, OperationEnum.CREATE, ModuleEnum.VENDOR);
			String getToken = "";
			try {
				InputStream is = file.getInputStream();
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));
				getToken = bufferedReader.readLine();
				bufferedReader.close();
			} catch (FileNotFoundException ex) {
				LOGGER.error(ex.getMessage());
				return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
			} catch (IOException ex) {
				LOGGER.error(ex.getMessage());
				return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
			}
			if (jwtTokenUtil.isTokenExpired(getToken)) {
				LOGGER.error("Token is expired");
				return ResponseEntity.status(500).body(new ResponseModel(0, null, "Token is expired."));
			}
			if (getToken != "") {
				LOGGER.info("Read license token from file success");
				Claims claims = jwtTokenUtil.getAllClaimsFromToken(getToken);
				LinkedHashMap linkedHashMap = claims.get("License", LinkedHashMap.class);
				LicenseModel newLicenseModel = new LicenseModel();
				if (linkedHashMap.get("lcnsToken") != null)
					newLicenseModel.setLcnsToken((String) linkedHashMap.get("lcnsToken"));
				if (linkedHashMap.get("lcnsFeaturesLst") != null)
					newLicenseModel.setLcnsFeaturesLst((String) linkedHashMap.get("lcnsFeaturesLst"));
				if (linkedHashMap.get("lcnsAllowedUsersCount") != null)
					newLicenseModel.setLcnsAllowedUsersCount((int) linkedHashMap.get("lcnsAllowedUsersCount"));
				if (linkedHashMap.get("lcnsDescription") != null)
					newLicenseModel.setLcnsDescription((String) linkedHashMap.get("lcnsDescription"));
				if (linkedHashMap.get("lcnsStartDate") != null)
					newLicenseModel.setLcnsStartDate(new Date((long) linkedHashMap.get("lcnsStartDate")));
				if (linkedHashMap.get("lcnsEndDate") != null)
					newLicenseModel.setLcnsEndDate(new Date((long) linkedHashMap.get("lcnsEndDate")));
				if (linkedHashMap.get("lcnsIsActive") != null)
					newLicenseModel.setLcnsIsActive((boolean) linkedHashMap.get("lcnsIsActive"));
				if (linkedHashMap.get("lcnsActivationIP") != null)
					newLicenseModel.setLcnsActivationIP((String) linkedHashMap.get("lcnsActivationIP"));
				if (linkedHashMap.get("lcnsActivationDate") != null)
					newLicenseModel.setLcnsActivationDate(new Date((long) linkedHashMap.get("lcnsActivationDate")));
				if (linkedHashMap.get("lcnsCmpName") != null)
					newLicenseModel.setLcnsCmpName((String) linkedHashMap.get("lcnsCmpName"));

				List<Integer> standards = new ArrayList<>();

				if (linkedHashMap.get("licenseStandardModels") != null) {
					List<LicenseStandardModel> licenseStandardModels = (List<LicenseStandardModel>) linkedHashMap.get("licenseStandardModels");
					for (Object o : licenseStandardModels) {
						LinkedHashMap hashMap = (LinkedHashMap) o;
						if (hashMap.get("stdId") != null) {
							LinkedHashMap stdId = (LinkedHashMap) hashMap.get("stdId");
							StandardModel standardModel = new StandardModel();
							if (stdId.get("stdId") != null)
								standardModel.setStdOnPremId((int)stdId.get("stdId"));
							if (stdId.get("stdName") != null)
								standardModel.setStdName((String) stdId.get("stdName"));
							if (stdId.get("stdVersion") != null)
								standardModel.setStdVersion((String) stdId.get("stdVersion"));
							if (stdId.get("stdDescription") != null)
								standardModel.setStdDescription((String) stdId.get("stdDescription"));
							if (stdId.get("stdIsActive") != null)
								standardModel.setStdIsActive((boolean) stdId.get("stdIsActive"));
							if (stdId.get("stdIsCompleted") != null)
								standardModel.setStdIsCompleted((boolean) stdId.get("stdIsCompleted"));
							if (stdId.get("stdLvlName") != null)
								standardModel.setStdLvlName((String) stdId.get("stdLvlName"));

							StandardModel newStandardModel;
							try {
								newStandardModel = standardService.create(standardModel);
							} catch (Exception ex) {
								newStandardModel = standardService.getStandardByNameVersionAndLevel(standardModel.getStdName(), standardModel.getStdLvlName(), standardModel.getStdVersion());
							}
							standards.add(newStandardModel.getStdId());
						}
					}
				}

				newLicenseModel.setLcnsStandardList(standards);
				LicenseModel licenseModel = null;
				try {
					licenseModel = licenseService.createOrUpdate(newLicenseModel);
				} catch (Exception ex) {
					LOGGER.error("error setting license", ex);
				}

				linkedHashMap = claims.get("Company", LinkedHashMap.class);
				CompanyModel companyModel = new CompanyModel();
				if (linkedHashMap.get("cmpName") != null)
					companyModel.setCmpName((String) linkedHashMap.get("cmpName"));
				if (linkedHashMap.get("cmpLogoUrl") != null)
					companyModel.setCmpLogoUrl((String) linkedHashMap.get("cmpLogoUrl"));
				if (linkedHashMap.get("cmpSiteName") != null)
					companyModel.setCmpSiteName((String) linkedHashMap.get("cmpSiteName"));
				if (linkedHashMap.get("cmpSupportEmail") != null)
					companyModel.setCmpSupportEmail((String) linkedHashMap.get("cmpSupportEmail"));
				if (linkedHashMap.get("cmpDataRetentionPeriod") != null)
					companyModel.setCmpDataRetentionPeriod((int) linkedHashMap.get("cmpDataRetentionPeriod"));

				CompanyModel newCompanyModel = companyService.create(companyModel);
				List<RoleModel> roleModels = roleService.getAllRolesByCmpId(newCompanyModel.getCmpId());
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("licenseModel", licenseService.getSingleInstance(licenseModel));
				map.put("companyModel", new CompanyModel(newCompanyModel));
				map.put("roleModels", roleModels);

				if (licenseModel != null) {
					companyModel.setCmpLcnsId(licenseModel);
				}
//				newCompanyModel.setCmpLcnsId(null);
				LOGGER.info("Create new license success");
				syncService.onPremUpdate();
				return ResponseEntity.ok().body(new ResponseModel(1, map, null));
			} else {
				LOGGER.error("File is empty");
				return ResponseEntity.status(500).body(new ResponseModel(0, null, "file is empty"));
			}
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN", ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Sign favorite news failed", ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@PutMapping("/{lcsId}")
	public ResponseEntity<ResponseModel> updateRecord(@PathVariable(value = "lcsId") int lcsId,
													  @Validated(CRUDModel.CRUDUpdate.class) @RequestBody LicenseModel updatedLicenseModel,
													  @RequestHeader(name = "Authorization") String token) throws Exception {
		try {
			handlePermission(token, OperationEnum.UPDATE, ModuleEnum.VENDOR);
			if (updatedLicenseModel.getLcnsStartDate().after(updatedLicenseModel.getLcnsEndDate())) {
				LOGGER.error("Start Date have to be before End Date!!");
				return ResponseEntity.status(403).body(new ResponseModel(0, null, "Start Date have to be before End Date!!"));
			}
			LicenseModel licenseModel = licenseService.update(lcsId, updatedLicenseModel);
			LOGGER.info("Update  license, id: " + lcsId + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, licenseService.getSingleInstance(licenseModel), null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Update license id: " + lcsId + " failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}


}
