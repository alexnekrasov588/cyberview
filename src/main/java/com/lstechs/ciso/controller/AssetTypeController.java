package com.lstechs.ciso.controller;

import java.util.List;
import java.util.Map;

import com.lstechs.ciso.model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.lstechs.ciso.enums.ModuleEnum;
import com.lstechs.ciso.enums.OperationEnum;
import com.lstechs.ciso.service.AssetTypeService;
import com.lstechs.ciso.service.AuthenticationService;

@RestController
@RequestMapping("/assetType")
public class AssetTypeController extends CRUDController<AssetTypeModel, AssetTypeModel> {

	@Autowired
	public AuthenticationService authenticationService;

	@Autowired
	public AssetTypeService assetTypeService;

	private static final Logger LOGGER = LogManager.getLogger(AssetController.class.getName());

	public AssetTypeController(AssetTypeService assetTypeService) {
		super(assetTypeService, ModuleEnum.PROJECT);
	}

	@Override
	@GetMapping
	public ResponseEntity<ResponseModel> getAllRecords(@RequestParam(defaultValue = "0") Integer pageNo,
													   @RequestParam(defaultValue = "10") Integer pageSize,
													   @RequestParam(defaultValue = "astId") String orderBy,
													   @RequestParam(defaultValue = "true") boolean orderByDesc,
													   @RequestParam(defaultValue = "") String query,
													   @RequestParam Map<String, String> allParams,
													   @RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.NEWS);
			CompanyModel companyModel = authenticationService.getCompanyFromToken(token.substring(7));
			Page<AssetTypeModel> assetTypeList = assetTypeService.getAllAssetTypeByCmpId(pageNo, pageSize, orderBy, orderByDesc, query, companyModel.getCmpId());
			Map<String, Object> map = assetTypeService.getAllResponse(assetTypeList);
			LOGGER.info("Get all asset type success.");
			return ResponseEntity.ok().body(new ResponseModel(1, map, null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("Get all from asset type failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("/getAssetType")
	public ResponseEntity<ResponseModel> getAssetType(@RequestHeader(name = "Authorization") String token,
													  @RequestParam(defaultValue = "") String type) {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.NEWS);
			CompanyModel companyModel = authenticationService.getCompanyFromToken(token.substring(7));
			List<AssetTypeModel> assetTypeList = assetTypeService.getAssetTypeByCmpId(companyModel.getCmpId(), type);
			LOGGER.info("get asset type success.");
			return ResponseEntity.ok().body(new ResponseModel(1, assetTypeList, null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get asset type failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@PutMapping
	public ResponseEntity<ResponseModel> updateAssetType(@RequestHeader(name = "Authorization") String token,
														 @RequestBody AssetTypeModel assetTypeModel) {
		try {
			handlePermission(token, OperationEnum.UPDATE, ModuleEnum.ASSET_MANAGEMENT);
			AssetTypeModel updatedAssetTypeModel = assetTypeService.update(assetTypeModel.getAstTypeId(), assetTypeModel);
			LOGGER.info("update asset type success.");
			return ResponseEntity.ok().body(new ResponseModel(1, updatedAssetTypeModel, null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("update asset type failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@DeleteMapping("/deleteAssetTypes")
	public ResponseEntity<ResponseModel> deleteAssetTypes(@RequestHeader(name = "Authorization") String token,
														  @RequestParam(name = "assetTypesIds") int[] assetIds) {
		try {
			handlePermission(token, OperationEnum.DELETE, ModuleEnum.ASSET_MANAGEMENT);
			List<AssetTypeModel> deletedAssetTypeModelList = assetTypeService.deleteAssetTypes(assetIds);
			LOGGER.info("delete asset type success.");
			return ResponseEntity.ok().body(new ResponseModel(1, deletedAssetTypeModelList, null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("delete asset type failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}
}
