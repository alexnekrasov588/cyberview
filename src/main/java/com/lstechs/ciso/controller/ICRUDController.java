package com.lstechs.ciso.controller;


import com.lstechs.ciso.model.ResponseModel;
import org.springframework.http.ResponseEntity;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface ICRUDController<T,P> {
	ResponseEntity<ResponseModel> getAllRecords(Integer pageNo, Integer pageSize, String orderBy,boolean orderByDesc, String query,Map<String,String> allParams,String token) throws Exception;

	ResponseEntity<ResponseModel> getRecordById(int id,String token) throws Exception;

	ResponseEntity<ResponseModel> createRecord(T t,String token) throws Exception;

	ResponseEntity<ResponseModel> updateRecord(int id, T t,String token) throws Exception;

	ResponseEntity<ResponseModel> deleteRecord(int id,String token) throws Exception;

	List<T> getSyncRecords(String updatedAt) throws Exception;

}
