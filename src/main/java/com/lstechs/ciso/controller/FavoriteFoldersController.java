package com.lstechs.ciso.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lstechs.ciso.enums.FolderTypeEnum;
import com.lstechs.ciso.enums.ModuleEnum;
import com.lstechs.ciso.enums.OperationEnum;
import com.lstechs.ciso.model.CRUDModel;
import com.lstechs.ciso.model.CompanyUserModel;
import com.lstechs.ciso.model.FavouriteFoldersModel;
import com.lstechs.ciso.model.FilterModel;
import com.lstechs.ciso.model.FoldersStructure;
import com.lstechs.ciso.model.ResponseModel;
import com.lstechs.ciso.service.AuthenticationService;
import com.lstechs.ciso.service.FavouriteFoldersService;

import javassist.NotFoundException;

@RestController
@RequestMapping("/favoriteFolders")
public class FavoriteFoldersController extends CRUDController<FavouriteFoldersModel, FavouriteFoldersModel> {

	private static final Logger LOGGER = LogManager.getLogger(FavoriteFoldersController.class.getName());

	public FavoriteFoldersController(FavouriteFoldersService favouriteFoldersService) {
		super(favouriteFoldersService, ModuleEnum.NEWS);
	}

	@Autowired
	private FavouriteFoldersService favouriteFoldersService;

	@Autowired
	private AuthenticationService authenticationService;


	/// Override all CRUD because of the handlePermission

	@Override
	@PostMapping
	public ResponseEntity<ResponseModel> createRecord(@Validated(CRUDModel.CRUDCreation.class) @RequestBody FavouriteFoldersModel t, @RequestHeader(name = "Authorization") String token) throws Exception {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.NEWS);
			CompanyUserModel companyUserModel = authenticationService.getCompanyUserFromToken(token.substring(7));
			t.setCmpUserId(companyUserModel);
			FavouriteFoldersModel tNew = favouriteFoldersService.create(t);
			LOGGER.info("create new " + t.getClass().getSimpleName() + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, "create success.", null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("create new " + favouriteFoldersService.getClass().getSimpleName() + " failed, exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@Override
	@PutMapping("/{id}")
	public ResponseEntity<ResponseModel> updateRecord(@PathVariable(value = "id") int id,
													  @Validated(CRUDModel.CRUDUpdate.class) @RequestBody FavouriteFoldersModel t,
													  @RequestHeader(name = "Authorization") String token) throws Exception {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.NEWS);
			FavouriteFoldersModel tNew = favouriteFoldersService.update(id, t);
			LOGGER.info("update  " + t.getClass().getSimpleName() + " id: " + id + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, "update success.", null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (NotFoundException ex) {
			LOGGER.error("update  " + t.getClass().getSimpleName() + " id: " + id + " failed , exception: " + ex);
			return ResponseEntity.badRequest().body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("update  " + t.getClass().getSimpleName() + " id: " + id + " failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@Override
	@DeleteMapping("/{id}")
	public ResponseEntity<ResponseModel> deleteRecord(@PathVariable(value = "id") int id, @RequestHeader(name = "Authorization") String token) throws Exception {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.NEWS);
			FavouriteFoldersModel t = favouriteFoldersService.delete(id);
			LOGGER.info("delete  " + t.getClass().getSimpleName() + " id: " + id + " success.");
			return ResponseEntity.ok().body(new ResponseModel(1, "deleted success.", null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (NotFoundException ex) {
			LOGGER.error("delete  " + favouriteFoldersService.getClass().getSimpleName() + " id: " + id + " failed , exception: " + ex);
			return ResponseEntity.badRequest().body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("delete  " + favouriteFoldersService.getClass().getSimpleName() + " id: " + id + " failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@DeleteMapping("/{favType}/deleteAll")
	public ResponseEntity<ResponseModel> deleteAll(@PathVariable(value = "favType") String favType, @RequestHeader(name = "Authorization") String token) throws Exception {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.NEWS);

			//Checking the FavouriteType
			FolderTypeEnum type;
			if (favType.equals("CVE")) {
				type = FolderTypeEnum.CVE;
			} else {
				type = FolderTypeEnum.NEWS;
			}

			favouriteFoldersService.deleteAll(token, type);
			LOGGER.info("delete  all success.");
			return ResponseEntity.ok().body(new ResponseModel(1, "deleted success.", null));

		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("delete all failed , exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("/news")
	public ResponseEntity<ResponseModel> getFavoriteFoldersNews(@RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.NEWS);
			FoldersStructure favouriteFoldersModels = favouriteFoldersService.getFoldersStructure(token, FolderTypeEnum.NEWS);
			LOGGER.info("get all favourite folders news success");
			return ResponseEntity.ok().body(new ResponseModel(1, favouriteFoldersModels, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get all favourite folders news failed ,exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("/cve")
	public ResponseEntity<ResponseModel> getFavoriteFoldersCves(@RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.CVE);
			FoldersStructure favouriteFoldersModels = favouriteFoldersService.getFoldersStructure(token, FolderTypeEnum.CVE);
			LOGGER.info("get all favourite folders cves success");
			return ResponseEntity.ok().body(new ResponseModel(1, favouriteFoldersModels, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get all favourite folders cves failed ,exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("/news/option")
	public ResponseEntity<ResponseModel> getFavoriteFoldersOptionNews(@RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.NEWS);
			List<FilterModel> folderOption = favouriteFoldersService.getFavoriteFoldersOption(token, FolderTypeEnum.NEWS);
			LOGGER.info("get all favourite folders news success");
			return ResponseEntity.ok().body(new ResponseModel(1, folderOption, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get all favourite folders news failed ,exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}

	@GetMapping("/cve/option")
	public ResponseEntity<ResponseModel> getFavoriteFoldersOptionCves(@RequestHeader(name = "Authorization") String token) {
		try {
			handlePermission(token, OperationEnum.READ, ModuleEnum.CVE);
			List<FilterModel> folderOption = favouriteFoldersService.getFavoriteFoldersOption(token, FolderTypeEnum.CVE);
			LOGGER.info("get all favourite folders news success");
			return ResponseEntity.ok().body(new ResponseModel(1, folderOption, null));
		} catch (SecurityException ex) {
			LOGGER.error("FORBIDDEN , exception: " + ex);
			return ResponseEntity.status(403).body(new ResponseModel(0, null, ex.getMessage()));
		} catch (Exception ex) {
			LOGGER.error("get all favourite folders news failed ,exception: " + ex);
			return ResponseEntity.status(500).body(new ResponseModel(0, null, ex.getMessage()));
		}
	}
}
