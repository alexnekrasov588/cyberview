package com.lstechs.ciso.model;

public class SingleVendorUSerModel {
	private int vndrId;
	private String vndrName;
	private String vndrEmail;

	public SingleVendorUSerModel(int vndrId, String vndrName, String vndrEmail) {
		this.vndrId = vndrId;
		this.vndrName = vndrName;
		this.vndrEmail = vndrEmail;
	}

	public SingleVendorUSerModel(VendorUserModel vendorUserModel) {
		this.vndrId = vendorUserModel.getVndrId();
		this.vndrName = vendorUserModel.getVndrName();
		this.vndrEmail = vendorUserModel.getVndrEmail();
	}

	public int getVndrId() {
		return vndrId;
	}

	public void setVndrId(int vndrId) {
		this.vndrId = vndrId;
	}

	public String getVndrName() {
		return vndrName;
	}

	public void setVndrName(String vndrName) {
		this.vndrName = vndrName;
	}

	public String getVndrEmail() {
		return vndrEmail;
	}

	public void setVndrEmail(String vndrEmail) {
		this.vndrEmail = vndrEmail;
	}
}
