package com.lstechs.ciso.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Company")
@EntityListeners(AuditingEntityListener.class)
public class CompanyModel extends CRUDModel {

	@Id
	@Column(name = "cmpId", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int cmpId;

	@NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "cmpName", nullable = false)
	private String cmpName;

	@Column(name = "cmpLogoUrl", nullable = true)
	private String cmpLogoUrl;

	@JsonIgnore
	@Lob
	@Column(name = "cmpLogoFile", nullable = true)
	private byte[] cmpLogoFile;

	@Column(name = "cmpSiteName", nullable = true)
	private String cmpSiteName;

	@Column(name = "cmpSupportEmail")
	private String cmpSupportEmail;

	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@ManyToOne
	@JoinColumn(name = "cmpLcnsId")
	private LicenseModel cmpLcnsId;

	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "cmpDataRetentionPeriod", nullable = false)
	private int cmpDataRetentionPeriod;

	@JsonIgnore
	@OneToMany(mappedBy = "prjCmpId")
	private List<ProjectModel> projectModel;

	@JsonIgnore
	@OneToMany(mappedBy = "cmpUsrCmpId")
	private List<CompanyUserModel> companyUserModel;

	@JsonIgnore
	@OneToMany(mappedBy = "cmpId")
	private List<CustomerStandardModel> customerStandardModel;

	@JsonIgnore
	@OneToMany(mappedBy = "cmpId")
	private List<RoleModel> roleModels;

	@JsonIgnore
	@OneToMany(mappedBy = "cmpId")
	private List<StandardCompanyStatusModel> StandardCompanyStatusModel;

	@JsonIgnore
	@OneToMany(mappedBy = "cmpId")
	private List<AssetModel> assetModel;

	@JsonIgnore
	@OneToMany(mappedBy = "cmpId")
	private List<AssetTypeModel> assetTypeModel;

	@JsonIgnore
	@OneToMany(mappedBy = "cmpId")
	private List<VulnerabilityAssessmentsModel> vulnerabilityAssessmentsModel;

	@Transient
	private int cmpUsersCount;

	public CompanyModel(String cmpName, String cmpLogoUrl, byte[] cmpLogoFile, String cmpSiteName, String cmpSupportEmail, LicenseModel cmpLcnsId, int cmpDataRetentionPeriod) {
		super();
		this.cmpName = cmpName;
		this.cmpLogoUrl = cmpLogoUrl;
		this.cmpLogoFile = cmpLogoFile;
		this.cmpSiteName = cmpSiteName;
		this.cmpSupportEmail = cmpSupportEmail.toLowerCase();
		this.cmpLcnsId = cmpLcnsId;
		this.cmpDataRetentionPeriod = cmpDataRetentionPeriod;
	}

	public CompanyModel() {
		super();
	}

	public CompanyModel(CompanyModel companyModel) {
		super();
		this.cmpId = companyModel.getCmpId();
		this.cmpName = companyModel.getCmpName();
		this.cmpLogoUrl = companyModel.getCmpLogoUrl();
		this.cmpLogoFile = companyModel.getCmpLogoFile();
		this.cmpSiteName = companyModel.getCmpSiteName();
		this.cmpSupportEmail = companyModel.getCmpSupportEmail();
		this.cmpLcnsId = companyModel.getCmpLcnsId();
		this.cmpDataRetentionPeriod = companyModel.getCmpDataRetentionPeriod();
	}

	public int getCmpId() {
		return cmpId;
	}

	public void setCmpId(int cmpId) {
		this.cmpId = cmpId;
	}

	public String getCmpName() {
		return cmpName;
	}

	public void setCmpName(String cmpName) {
		this.cmpName = cmpName;
	}

	public String getCmpLogoUrl() {
		return cmpLogoUrl;
	}

	public void setCmpLogoUrl(String cmpLogoUrl) {
		this.cmpLogoUrl = cmpLogoUrl;
	}

	public byte[] getCmpLogoFile() {
		return cmpLogoFile;
	}

	public void setCmpLogoFile(byte[] cmpImgFile) {
		this.cmpLogoFile = cmpImgFile;
	}

	public String getCmpSiteName() {
		return cmpSiteName;
	}

	public void setCmpSiteName(String cmpSiteName) {
		this.cmpSiteName = cmpSiteName;
	}

	public String getCmpSupportEmail() {
		return cmpSupportEmail.toLowerCase();
	}

	public void setCmpSupportEmail(String cmpSupportEmail) {
		this.cmpSupportEmail = cmpSupportEmail.toLowerCase();
	}

	public LicenseModel getCmpLcnsId() {
		return cmpLcnsId;
	}

	public void setCmpLcnsId(LicenseModel cmpLcnsId) {
		this.cmpLcnsId = cmpLcnsId;
	}

	public int getCmpDataRetentionPeriod() {
		return cmpDataRetentionPeriod;
	}

	public void setCmpDataRetentionPeriod(int cmpDataRetentionPeriod) {
		this.cmpDataRetentionPeriod = cmpDataRetentionPeriod;
	}

	public List<ProjectModel> getProjectModel() {
		return projectModel;
	}

	public void setProjectModel(List<ProjectModel> projectModel) {
		this.projectModel = projectModel;
	}

	@JsonIgnore
	public List<CompanyUserModel> getCompanyUserModel() {
		if (companyUserModel != null) {
			List<CompanyUserModel> newCompanyUserModels = new ArrayList<>();
			for (CompanyUserModel tmpCompanyUserModel : companyUserModel) {
				if (tmpCompanyUserModel.getDeletedAt() == null) newCompanyUserModels.add(tmpCompanyUserModel);
			}
			return newCompanyUserModels;
		}
		return null;
	}

	public void setCompanyUserModel(List<CompanyUserModel> companyUserModel) {
		this.companyUserModel = companyUserModel;
	}

	@JsonIgnore
	public List<CustomerStandardModel> getCustomerStandardModel() {
		if (companyUserModel != null) {
			List<CustomerStandardModel> newCustomerStandardModel = new ArrayList<>();
			for (CustomerStandardModel tmpCustomerStandardModel : customerStandardModel) {
				if (tmpCustomerStandardModel.getDeletedAt() == null)
					newCustomerStandardModel.add(tmpCustomerStandardModel);
			}
			return newCustomerStandardModel;
		}
		return null;
	}

	public void setCustomerStandardModel(List<CustomerStandardModel> customerStandardModel) {
		this.customerStandardModel = customerStandardModel;
	}

	public List<RoleModel> getRoleModels() {
		return roleModels;
	}

	public void setRoleModels(List<RoleModel> roleModels) {
		this.roleModels = roleModels;
	}

	public int getCmpUsersCount() {
		if (getCompanyUserModel() != null && getCompanyUserModel().size() > 0) {
			return getCompanyUserModel().size();
		}
		return 0;
	}


}
