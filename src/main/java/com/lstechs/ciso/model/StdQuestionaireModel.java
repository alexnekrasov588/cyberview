package com.lstechs.ciso.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Type;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "StdQuestionaire")
@EntityListeners(AuditingEntityListener.class)
public class StdQuestionaireModel extends CRUDModel {

	@Id
	@Column(name = "stdQtrId", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int stdQtrId;

	@NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "stdQtrName", nullable = false)
	@Type(type = "text")
	private String stdQtrName;

	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumn(name = "stdId", nullable = false)
	private StandardModel stdId;

	@JsonIgnore
	@OneToMany(mappedBy = "stdQtrId")
	private List<FilesModel> filesModel;

	@Column(name = "stdQtrOnPremId", nullable = true, columnDefinition = "Integer default '0'")
	private int stdQtrOnPremId;

	@Column(name = "parentId", nullable = true, columnDefinition = "Integer default '0'")
	private int parentId;

	@Column(name = "duplicatedFrom", nullable = false, columnDefinition = "Integer default '0'")
	private int duplicatedFrom;

	@Column(name = "isQuestionnaireCreatedByCompany", columnDefinition = "BOOLEAN DEFAULT false")
	private boolean isQuestionnaireCreatedByCompany;

	@Column(name = "isStandardCreatedByCompany", columnDefinition = "BOOLEAN DEFAULT false")
	private boolean isStandardCreatedByCompany;

	@JsonIgnore
	@Column(name = "companyId", columnDefinition = "Integer default '0'")
	private int companyId;

	@Transient
	public int std;

	@Transient
	public String questionnaireSpecificName;

	@Transient
	private List<StdSectionModel> stdSectionModelList;

	@OneToOne(mappedBy = "questionnaireId")
	private RequirementsHeaderTitlesModel requirementsHeaderTitlesModel;

	@OneToOne(mappedBy = "standardId")
	private VendorStandardHeaderTitlesModel vendorStandardHeaderTitlesModel;

	public StdQuestionaireModel(String stdQtrName, StandardModel stdId, int parentId) {
		super();
		this.stdQtrName = stdQtrName;
		this.stdId = stdId;
		this.parentId = parentId;
	}

	public StdQuestionaireModel(StdQuestionaireModel stdQuestionaireModel) {
		super();
		this.stdQtrName = stdQuestionaireModel.getStdQtrName();
		this.stdId = stdQuestionaireModel.getStdId();
		this.parentId = stdQuestionaireModel.getParentId();
	}

	public StdQuestionaireModel() {
		super();
	}

	public int getStdQtrId() {
		return stdQtrId;
	}

	public void setStdQtrId(int stdQtrId) {
		this.stdQtrId = stdQtrId;
	}

	public String getStdQtrName() {
		return stdQtrName;
	}

	public void setStdQtrName(String stdQtrName) {
		this.stdQtrName = stdQtrName;
	}

	@JsonIgnore
	public StandardModel getStdId() {
		return stdId;
	}

	@JsonProperty
	public void setStdId(StandardModel stdId) {
		this.stdId = stdId;
	}

	public List<StdSectionModel> getStdSectionModelList() {
		return stdSectionModelList;
	}

	public void setStdSectionModelList(List<StdSectionModel> stdSectionModelList) {
		this.stdSectionModelList = stdSectionModelList;
	}

	public int getStd() {
		if (this.stdId != null)
			return this.stdId.getStdId();
		else return 0;
	}

	public int getStdQtrOnPremId() {
		return stdQtrOnPremId;
	}

	public void setStdQtrOnPremId(int stdQtrOnPremId) {
		this.stdQtrOnPremId = stdQtrOnPremId;
	}

	public int getParentId() {
		return parentId;
	}

	public void setParentId(int parentId) {
		this.parentId = parentId;
	}

	public boolean isQuestionnaireCreatedByCompany() { return isQuestionnaireCreatedByCompany; }

	public void setQuestionnaireCreatedByCompany(boolean questionnaireCreatedByCompany) { isQuestionnaireCreatedByCompany = questionnaireCreatedByCompany; }

	public int getDuplicatedFrom() { return duplicatedFrom; }

	public void setDuplicatedFrom(int duplicatedFrom) { this.duplicatedFrom = duplicatedFrom; }

	public String getQuestionnaireSpecificName() { return questionnaireSpecificName; }

	public void setQuestionnaireSpecificName(String questionnaireSpecificName) { this.questionnaireSpecificName = questionnaireSpecificName; }

	public RequirementsHeaderTitlesModel getRequirementsHeaderTitlesModel() { return requirementsHeaderTitlesModel; }

	public void setRequirementsHeaderTitlesModel(RequirementsHeaderTitlesModel requirementsHeaderTitlesModel) { this.requirementsHeaderTitlesModel = requirementsHeaderTitlesModel; }

	public boolean isStandardCreatedByCompany() { return isStandardCreatedByCompany; }

	public void setStandardCreatedByCompany(boolean standardCreatedByCompany) { isStandardCreatedByCompany = standardCreatedByCompany; }

	public VendorStandardHeaderTitlesModel getVendorStandardHeaderTitlesModel() { return vendorStandardHeaderTitlesModel; }

	public void setVendorStandardHeaderTitlesModel(VendorStandardHeaderTitlesModel vendorStandardHeaderTitlesModel) { this.vendorStandardHeaderTitlesModel = vendorStandardHeaderTitlesModel; }

	public int getCompanyId() { return companyId; }

	public void setCompanyId(int companyId) { this.companyId = companyId; }
}
