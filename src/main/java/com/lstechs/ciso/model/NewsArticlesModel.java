package com.lstechs.ciso.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Type;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.util.DigestUtils;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "NewsArticles")
@EntityListeners(AuditingEntityListener.class)
public class NewsArticlesModel extends CRUDModel {

	@Id
	@Column(name = "newsArtcId", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int newsArtcId;

	@NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "newsArtcTitle", nullable = false)
	@Type(type = "text")
	private String newsArtcTitle;

	@Column(name = "newsArtcUrl", nullable = true)
	@Type(type = "text")
	private String newsArtcUrl;

	@Column(name = "newsImgUrl", nullable = true)
	@Type(type = "text")
	private String newsImgUrl;

	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@ManyToOne()
	@JoinColumn(name = "newsSrcId", nullable = false)
	private NewsSourcesModel newsSrcId;

	@NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "newsArtcContent", nullable = false)
	@Type(type = "text")
	private String newsArtcContent;

	@Column(name = "newsArtcSummary", nullable = true)
	@Type(type = "text")
	private String newsArtcSummary;

	@NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "newsArtcHash", nullable = false, unique = true)
	private String newsArtcHash;

	//	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "newsIsActive", nullable = true)
	private boolean newsIsActive;

	@Column(name="newsPubDate")
	private Date newsPubDate;
	
	@Column(name="newsUpdateOnPrem")
	private Date newsUpdateOnPrem;

	@JsonIgnore
	@OneToMany(mappedBy = "newsArtcId")
	private List<FavouriteNewsModel> favouriteNewsModel;


	@JsonIgnore
	@OneToMany(mappedBy = "newsArtcId")
	private List<NewsArticlesTagsModel> newsArticlesTagsModel;

	@Transient
	private List<Map<String, Object>> newsArtcTagsLst;

	@Transient
	private TaskModel taskModel;


	public NewsArticlesModel(String newsArtcTitle, String newsArtcUrl, String newsImgUrl, NewsSourcesModel newsSrcId, String newsArtcContent, String newsArtcSummary, boolean newsIsActive, Date newsPubDate) {
		super();
		this.newsArtcTitle = newsArtcTitle;
		this.newsSrcId = newsSrcId;
		this.newsArtcUrl = newsArtcUrl;
		this.newsImgUrl = newsImgUrl;
		this.newsArtcContent = newsArtcContent;
		this.newsArtcHash = DigestUtils.md5DigestAsHex(newsArtcUrl.getBytes());
		this.newsArtcSummary = newsArtcSummary;
		this.newsIsActive = newsIsActive;
		this.newsPubDate = newsPubDate;
	}

	public NewsArticlesModel() {
		super();
	}

	public int getNewsArtcId() {
		return newsArtcId;
	}

	public void setNewsArtcId(int newsArtcId) {
		this.newsArtcId = newsArtcId;
	}

	public String getNewsArtcTitle() {
		return newsArtcTitle;
	}

	public void setNewsArtcTitle(String newsArtcTitle) {
		this.newsArtcTitle = newsArtcTitle;
	}

	public NewsSourcesModel getNewsSrcId() {
		return newsSrcId;
	}

	public void setNewsSrcId(NewsSourcesModel newsSrcId) {
		this.newsSrcId = newsSrcId;
	}

	public String getNewsImgUrl() {
		return newsImgUrl;
	}

	public void setNewsImgUrl(String newsImgUrl) {
		this.newsImgUrl = newsImgUrl;
	}

	public String getNewsArtcContent() {
		return newsArtcContent;
	}

	public void setNewsArtcContent(String newsArtcContent) {
		this.newsArtcContent = newsArtcContent;
	}

	public List<FavouriteNewsModel> getFavouriteNewsModel() {
		return favouriteNewsModel;
	}

	public void setFavouriteNewsModel(List<FavouriteNewsModel> favouriteNewsModel) {
		this.favouriteNewsModel = favouriteNewsModel;
	}

	public String getNewsArtcHash() {
		return newsArtcHash;
	}

	public void setNewsArtcHash(String newsArtcHash) {
		this.newsArtcHash = newsArtcHash;
	}

	public String getNewsArtcSummary() { return newsArtcSummary; }

	public void setNewsArtcSummary(String newsArtcSummary) { this.newsArtcSummary = newsArtcSummary; }

	public List<NewsArticlesTagsModel> getNewsArticlesTagsModel() {
		return newsArticlesTagsModel;
	}

	public void setNewsArticlesTagsModel(List<NewsArticlesTagsModel> newsArticlesTagsModel) {
		this.newsArticlesTagsModel = newsArticlesTagsModel;
	}

	public String getNewsArtcUrl() {
		return newsArtcUrl;
	}

	public void setNewsArtcUrl(String newsArtcUrl) {
		this.newsArtcUrl = newsArtcUrl;
	}

	public SingleTaskModel getTaskModel() {
		if (taskModel != null) {
			return new SingleTaskModel(taskModel, new SingleCompanyUserModel(taskModel.getTskAssignee()));
		} else return null;
	}

	public void setTaskModel(TaskModel taskModel) {
		this.taskModel = taskModel;
	}

	public boolean getNewsIsActive() {
		return newsIsActive;
	}

	public void setNewsIsActive(boolean newsIsActive) {
		this.newsIsActive = newsIsActive;
	}

	public List<Map<String, Object>> getNewsArtcTagsLst() {
		return newsArtcTagsLst;
	}

	public void setNewsArtcTagsLst(List<Map<String, Object>> newsArtcTagsLst) {
		this.newsArtcTagsLst = newsArtcTagsLst;
	}

	public Date getNewsPubDate() {
		return newsPubDate;
	}

	public void setNewsPubDate(Date newsPubDate) {
		this.newsPubDate = newsPubDate;
	}

	public Date getNewsUpdateOnPrem() {
		return newsUpdateOnPrem;
	}

	public void setNewsUpdateOnPrem(Date newsUpdateOnPrem) {
		this.newsUpdateOnPrem = newsUpdateOnPrem;
	}
	
	
}
