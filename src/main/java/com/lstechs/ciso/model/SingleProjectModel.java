package com.lstechs.ciso.model;

import com.lstechs.ciso.enums.PrjStatusEnum;

import java.util.Date;

public class SingleProjectModel {
	private int prjId;
	private String prjName;
	private String prjDescription;
	private int prjCmpId;
	private SingleCompanyUserModel prjManagerId;
	private Date prjStartDate;
	private Date prjEndDate;
	private double prjBudget;
	private PrjStatusEnum prjStatus;
	private double prjProgressPercent;

	public SingleProjectModel(int prjId, String prjName, String prjDescription, int prjCmpId, SingleCompanyUserModel prjManagerId, Date prjStartDate, Date prjEndDate, double prjBudget, PrjStatusEnum prjStatus, double prjProgressPercent) {
		this.prjId = prjId;
		this.prjName = prjName;
		this.prjDescription = prjDescription;
		this.prjCmpId = prjCmpId;
		this.prjManagerId = prjManagerId;
		this.prjStartDate = prjStartDate;
		this.prjEndDate = prjEndDate;
		this.prjBudget = prjBudget;
		this.prjStatus = prjStatus;
		this.prjProgressPercent = prjProgressPercent;
	}

	public SingleProjectModel(ProjectModel projectModel,SingleCompanyUserModel singleCompanyUserModel) {
		this.prjId = projectModel.getPrjId();
		this.prjName = projectModel.getPrjName();
		this.prjDescription = projectModel.getPrjDescription();
		this.prjCmpId = projectModel.getPrjCmpId().getCmpId();
		this.prjManagerId = singleCompanyUserModel;
		this.prjStartDate = projectModel.getPrjStartDate();
		this.prjEndDate = projectModel.getPrjEndDate();
		this.prjBudget = projectModel.getPrjBudget();
		this.prjStatus = projectModel.getPrjStatus();
		this.prjProgressPercent = projectModel.getPrjProgressPercent();
	}

	public int getPrjId() {
		return prjId;
	}

	public void setPrjId(int prjId) {
		this.prjId = prjId;
	}

	public String getPrjName() {
		return prjName;
	}

	public void setPrjName(String prjName) {
		this.prjName = prjName;
	}

	public String getPrjDescription() {
		return prjDescription;
	}

	public void setPrjDescription(String prjDescription) {
		this.prjDescription = prjDescription;
	}

	public int getPrjCmpId() {
		return prjCmpId;
	}

	public void setPrjCmpId(int prjCmpId) {
		this.prjCmpId = prjCmpId;
	}

	public SingleCompanyUserModel getPrjManagerId() {
		return prjManagerId;
	}

	public void setPrjManagerId(SingleCompanyUserModel prjManagerId) {
		this.prjManagerId = prjManagerId;
	}

	public Date getPrjStartDate() {
		return prjStartDate;
	}

	public void setPrjStartDate(Date prjStartDate) {
		this.prjStartDate = prjStartDate;
	}

	public Date getPrjEndDate() {
		return prjEndDate;
	}

	public void setPrjEndDate(Date prjEndDate) {
		this.prjEndDate = prjEndDate;
	}

	public double getPrjBudget() {
		return prjBudget;
	}

	public void setPrjBudget(double prjBudget) {
		this.prjBudget = prjBudget;
	}

	public PrjStatusEnum getPrjStatus() {
		return prjStatus;
	}

	public void setPrjStatus(PrjStatusEnum prjStatus) {
		this.prjStatus = prjStatus;
	}

	public double getPrjProgressPercent() {
		return prjProgressPercent;
	}

	public void setPrjProgressPercent(double prjProgressPercent) {
		this.prjProgressPercent = prjProgressPercent;
	}
}
