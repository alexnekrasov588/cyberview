package com.lstechs.ciso.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Role")
@EntityListeners(AuditingEntityListener.class)
public class RoleModel extends CRUDModel {

	@Id
	@Column(name = "roleId", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int roleId;

	@NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "roleName", nullable = false)
	private String roleName;

	@NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "isEditable", nullable = false, columnDefinition = "BOOLEAN DEFAULT true")
	private boolean isEditable = true;

	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cmpId", nullable = false)
	private CompanyModel cmpId;

	@JsonIgnore
	@OneToMany(mappedBy = "cmpUsrRoleId")
	private List<CompanyUserModel> companyUserModel;

	@OneToMany(mappedBy = "roleId")
	private List<PermissionModel> permissionModels;

	@Transient
	private List<PermissionModel> permissionList;

	@NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "areAllStandardsSelected", nullable = false, columnDefinition="BOOLEAN DEFAULT true")
	private boolean areAllStandardsSelected = true;

	@NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "areAllQuestionnairesSelected", nullable = false, columnDefinition="BOOLEAN DEFAULT true")
	private boolean areAllQuestionnairesSelected = true;

	@Transient
	private ArrayList<Integer> selectedQuestionnaires;

	@Transient
	private ArrayList<Integer> selectedStandards;

	@JsonIgnore
	@OneToMany(mappedBy = "roleId")
	private List<SelectedStandardModel> selectedStandardsModel;

	@JsonIgnore
	@OneToMany(mappedBy = "roleId")
	private List<SelectedQuestionnaireModel> selectedQuestionnaireModel;


	public RoleModel(String roleName, CompanyModel cmpId, Boolean isEditable, Boolean areAllStandardsSelected, Boolean areAllQuestionnairesSelected) {
		super();
		this.roleName = roleName;
		this.cmpId = cmpId;
		this.isEditable = isEditable;
		this.areAllStandardsSelected = areAllStandardsSelected;
		this.areAllQuestionnairesSelected = areAllQuestionnairesSelected;
	}

	public RoleModel() {
		super();
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	@JsonIgnore
	public CompanyModel getCmpId() {
		return cmpId;
	}

	@JsonProperty
	public void setCmpId(CompanyModel cmpId) {
		this.cmpId = cmpId;
	}

	public List<CompanyUserModel> getCompanyUserModel() {
		return companyUserModel;
	}

	public void setCompanyUserModel(List<CompanyUserModel> companyUserModel) {
		this.companyUserModel = companyUserModel;
	}

	public List<PermissionModel> getPermissionModels() {
		if (permissionModels != null) {
			List<PermissionModel> permissions = new ArrayList<>();
			for (PermissionModel tmpPermissionModel : permissionModels) {
				if (tmpPermissionModel.getDeletedAt() == null) permissions.add(tmpPermissionModel);
			}
			return permissions;
		} else return null;
	}

	public void setPermissionModels(List<PermissionModel> permissionModels) {
		this.permissionModels = permissionModels;
	}

	@JsonIgnore
	public List<PermissionModel> getPermissionList() {
		return permissionList;
	}

	@JsonProperty
	public void setPermissionList(List<PermissionModel> permissionList) {
		this.permissionList = permissionList;
	}

	public boolean isEditable() {
		return isEditable;
	}

	public void setEditable(boolean editable) {
		this.isEditable = editable;
	}

	public boolean isAreAllStandardsSelected() {
		return areAllStandardsSelected;
	}

	public void setAreAllStandardsSelected(boolean areAllStandardsSelected) {
		this.areAllStandardsSelected = areAllStandardsSelected;
	}

	public boolean isAreAllQuestionnairesSelected() {
		return areAllQuestionnairesSelected;
	}

	public void setAreAllQuestionnairesSelected(boolean areAllQuestionnairesSelected) {
		this.areAllQuestionnairesSelected = areAllQuestionnairesSelected;
	}

	public ArrayList<Integer> getSelectedQuestionnaires() {
		return selectedQuestionnaires;
	}

	public void setSelectedQuestionnaires(ArrayList<Integer> selectedQuestionnaires) {
		this.selectedQuestionnaires = selectedQuestionnaires;
	}

	public ArrayList<Integer> getSelectedStandards() {
		return selectedStandards;
	}

	public void setSelectedStandards(ArrayList<Integer> selectedStandards) {
		this.selectedStandards = selectedStandards;
	}

	public List<SelectedStandardModel> getSelectedStandardsModel() {
		return selectedStandardsModel;
	}

	public void setSelectedStandardsModel(List<SelectedStandardModel> selectedStandardsModel) {
		this.selectedStandardsModel = selectedStandardsModel;
	}

	public List<SelectedQuestionnaireModel> getSelectedQuestionnaireModel() {
		return selectedQuestionnaireModel;
	}

	public void setSelectedQuestionnaireModel(List<SelectedQuestionnaireModel> selectedQuestionnaireModel) {
		this.selectedQuestionnaireModel = selectedQuestionnaireModel;
	}
}
