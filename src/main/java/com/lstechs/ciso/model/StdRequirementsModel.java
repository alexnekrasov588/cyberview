package com.lstechs.ciso.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.lstechs.ciso.enums.ExplanationAndArticleNumberTypeEnum;
import com.lstechs.ciso.enums.QtrQstTypeEnum;
import org.hibernate.annotations.Type;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "StdRequirements")
@EntityListeners(AuditingEntityListener.class)
public class StdRequirementsModel extends CRUDModel {

	@Id
	@Column(name = "stdRqrId", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int stdRqrId;

	@Column(name = "stdRqrOnPremId", columnDefinition = "Integer default '0'")
	private int stdRqrOnPremId;

	@NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "stdRqrName", nullable = false)
	@Type(type = "text")
	private String stdRqrName;

	@Column(name = "stdRqrTitle", nullable = true)
	@Type(type = "text")
	private String stdRqrTitle;

	@Column(name = "stdRqrDescription", nullable = true)
	@Type(type = "text")
	private String stdRqrDescription;

	@Column(name = "stdRqrExplanation", nullable = true)
	@Type(type = "text")
	private String stdRqrExplanation;

	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "stdId", nullable = false)
	private StandardModel stdId;

	@Column(name = "articleNum")
	private String articleNum;

	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "stdRqrIsMandatory", nullable = false)
	private boolean stdRqrIsMandatory;

	@Column(name = "stdRqrOrder")
	private int order;

	@JsonIgnore
	@OneToOne(mappedBy = "stdRqrId")
	private StdQtrQuestionsModel stdQtrQuestionsModel;

	@OneToMany(mappedBy = "stdRqrId", fetch = FetchType.LAZY)
	private List<CustomerStandardModel> customerStandardModel;

	@JsonIgnore
	@OneToMany(mappedBy = "stdRqrId")
	private List<CrossReferenceModel> crossReferenceModel;

	@Transient
	private QtrQstTypeEnum qtrQstType;

	@Transient
	private QtrQstTypeEnum answerDate1Type;

	@Transient
	private QtrQstTypeEnum answerDate2Type;

	@Column(name = "fileFreeText")
	@Type(type = "text")
	private String fileFreeText;

	@Transient
	private List<StdQstOptionModel> stdQstOptionModels;

	@Transient
	private List<StdAnswer1OptionModel> stdAnswer1OptionModels;

	@Transient
	private List<StdAnswer2OptionModel> stdAnswer2OptionModels;

	@Transient
	private CustomerStandardModel customerStandard;

	@Transient
	private ModuleTaskModel moduleTaskModel;

	@Transient
	private int sectionId;

	@Transient
	private String questionnaireName;

	@Transient
	public int std;

	public StdRequirementsModel(String stdRqrName, String stdRqrTitle, String stdRqrDescription, String stdRqrExplanation, StandardModel stdId,
								String articleNum, boolean stdRqrIsMandatory, int order, String fileFreeText) {
		super();
		this.stdRqrName = stdRqrName;
		this.stdRqrTitle = stdRqrTitle;
		this.stdRqrDescription = stdRqrDescription;
		this.stdRqrExplanation = stdRqrExplanation;
		this.stdId = stdId;
		this.articleNum = articleNum;
		this.stdRqrIsMandatory = stdRqrIsMandatory;
		this.order = order;
		this.fileFreeText = fileFreeText;
	}

	public StdRequirementsModel(StdRequirementsModel stdRequirementsModel) {
		super();
		this.stdRqrName = stdRequirementsModel.getStdRqrName();
		this.stdRqrTitle = stdRequirementsModel.getStdRqrTitle();
		this.stdRqrDescription = stdRequirementsModel.getStdRqrDescription();
		this.stdRqrExplanation = stdRequirementsModel.getStdRqrExplanation();
		this.stdId = stdRequirementsModel.getStdId();
		this.articleNum = stdRequirementsModel.getArticleNum();
		this.stdRqrIsMandatory = stdRequirementsModel.isStdRqrIsMandatory();
		this.order = stdRequirementsModel.getOrder();
		this.fileFreeText = stdRequirementsModel.getFileFreeText();
	}

	public StdRequirementsModel() {
		super();
	}

	public int getStdRqrId() {
		return stdRqrId;
	}

	public void setStdRqrId(int stdRqrId) {
		this.stdRqrId = stdRqrId;
	}

	public String getStdRqrName() {
		return stdRqrName;
	}

	public void setStdRqrName(String stdRqrName) {
		this.stdRqrName = stdRqrName;
	}

	public String getStdRqrTitle() {
		return stdRqrTitle;
	}

	public void setStdRqrTitle(String stdRqrTitle) {
		this.stdRqrTitle = stdRqrTitle;
	}

	public String getStdRqrDescription() {
		return stdRqrDescription;
	}

	public void setStdRqrDescription(String stdRqrDescription) {
		this.stdRqrDescription = stdRqrDescription;
	}

	public String getStdRqrExplanation() {
		return stdRqrExplanation;
	}

	public void setCustomerStandardModel(List<CustomerStandardModel> customerStandardModel) {
		this.customerStandardModel = customerStandardModel;
	}

	public void setStdRqrExplanation(String stdRqrExplanation) {
		this.stdRqrExplanation = stdRqrExplanation;
	}

	@JsonIgnore
	public StandardModel getStdId() {
		return stdId;
	}

	public void setStdId(StandardModel stdId) {
		this.stdId = stdId;
	}

	public String getArticleNum() {
		return articleNum;
	}

	public void setArticleNum(String articleNum) {
		this.articleNum = articleNum;
	}

	public boolean isStdRqrIsMandatory() {
		return stdRqrIsMandatory;
	}

	public void setStdRqrIsMandatory(boolean stdRqrIsMandatory) {
		this.stdRqrIsMandatory = stdRqrIsMandatory;
	}

	public StdQtrQuestionsModel getStdQtrQuestionsModel() {
		return stdQtrQuestionsModel;
	}

	public void setStdQtrQuestionsModel(StdQtrQuestionsModel stdQtrQuestionsModel) {
		this.stdQtrQuestionsModel = stdQtrQuestionsModel;
	}

	@JsonIgnore
	public List<CustomerStandardModel> getCustomerStandardModel() {
		return customerStandardModel;
	}

	public List<CrossReferenceModel> getCrossReferenceModel() {
		return crossReferenceModel;
	}

	public void setCrossReferenceModel(List<CrossReferenceModel> crossReferenceModel) {
		this.crossReferenceModel = crossReferenceModel;
	}

	public QtrQstTypeEnum getQtrQstType() {
		return qtrQstType;
	}

	public void setQtrQstType(QtrQstTypeEnum qtrQstType) {
		this.qtrQstType = qtrQstType;
	}

	public CustomerStandardModel getCustomerStandard() {
		return customerStandard;
	}

	public void setCustomerStandard(CustomerStandardModel customerStandard) {
		this.customerStandard = customerStandard;
	}

	public ModuleTaskModel getModuleTaskModel() {
		if (moduleTaskModel != null && moduleTaskModel.getTskId() != null && moduleTaskModel.getTskId().getTskAssignee() != null)
			moduleTaskModel.getTskId().setTskAssignee(null);
		return moduleTaskModel;
	}

	public void setModuleTaskModel(ModuleTaskModel moduleTaskModel) {
		this.moduleTaskModel = moduleTaskModel;
	}

	public List<StdQstOptionModel> getStdQstOptionModels() {
		if (stdQstOptionModels != null && stdQstOptionModels.size() > 0) {
			List<StdQstOptionModel> stdQstOption = new ArrayList<>();
			for (StdQstOptionModel tmpStdQstOptionModel : stdQstOptionModels) {
				if (tmpStdQstOptionModel.getDeletedAt() == null) stdQstOption.add(tmpStdQstOptionModel);
			}
			return stdQstOption;
		}
		return stdQstOptionModels;
	}

	public void setStdQstOptionModels(List<StdQstOptionModel> stdQstOptionModels) {
		this.stdQstOptionModels = stdQstOptionModels;
	}

	public List<StdAnswer1OptionModel> getStdAnswer1OptionModels() {
		if (stdAnswer1OptionModels != null && stdAnswer1OptionModels.size() > 0) {
			List<StdAnswer1OptionModel> stdAnswer1Option = new ArrayList<>();
			for (StdAnswer1OptionModel tmpStdAnswer1OptionModels : stdAnswer1OptionModels) {
				if (tmpStdAnswer1OptionModels.getDeletedAt() == null) stdAnswer1Option.add(tmpStdAnswer1OptionModels);
			}
			return stdAnswer1Option;
		}
		return stdAnswer1OptionModels;
	}

	public void setStdAnswer1OptionModels(List<StdAnswer1OptionModel> stdAnswer1OptionModels) {
		this.stdAnswer1OptionModels = stdAnswer1OptionModels;
	}

	public List<StdAnswer2OptionModel> getStdAnswer2OptionModels() {
		if (stdAnswer2OptionModels != null && stdAnswer2OptionModels.size() > 0) {
			List<StdAnswer2OptionModel> stdAnswer2Option = new ArrayList<>();
			for (StdAnswer2OptionModel tmpStdAnswer2OptionModels : stdAnswer2OptionModels) {
				if (tmpStdAnswer2OptionModels.getDeletedAt() == null) stdAnswer2Option.add(tmpStdAnswer2OptionModels);
			}
			return stdAnswer2Option;
		}
		return stdAnswer2OptionModels;
	}

	public void setStdAnswer2OptionModels(List<StdAnswer2OptionModel> stdAnswer2OptionModels) {
		this.stdAnswer2OptionModels = stdAnswer2OptionModels;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public String getFileFreeText() {
		return fileFreeText;
	}

	public void setFileFreeText(String fileFreeText) {
		this.fileFreeText = fileFreeText;
	}

	public int getSectionId() {
		return sectionId;
	}

	public void setSectionId(int sectionId) {
		this.sectionId = sectionId;
	}

	public String getQuestionnaireName() {
		return questionnaireName;
	}

	public void setQuestionnaireName(String questionnaireName) {
		this.questionnaireName = questionnaireName;
	}

	public int getStd() {
		if (this.stdId != null)
			return this.stdId.getStdId();
		else return 0;
	}

	public int getStdRqrOnPremId() {
		return stdRqrOnPremId;
	}

	public void setStdRqrOnPremId(int stdRqrOnPremId) {
		this.stdRqrOnPremId = stdRqrOnPremId;
	}

	public QtrQstTypeEnum getAnswerDate1Type() {
		return answerDate1Type;
	}

	public void setAnswerDate1Type(QtrQstTypeEnum answerDate1Type) {
		this.answerDate1Type = answerDate1Type;
	}

	public QtrQstTypeEnum getAnswerDate2Type() {
		return answerDate2Type;
	}

	public void setAnswerDate2Type(QtrQstTypeEnum answerDate2Type) {
		this.answerDate2Type = answerDate2Type;
	}

}
