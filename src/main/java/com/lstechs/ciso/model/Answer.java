package com.lstechs.ciso.model;

public class Answer {

	private ComplianceAnswer answer;

	public Answer() {
	}

	public Answer(ComplianceAnswer answer) {
		this.answer = answer;
	}

	public ComplianceAnswer getAnswer() {
		return answer;
	}

	public void setAnswer(ComplianceAnswer answer) {
		this.answer = answer;
	}
}
