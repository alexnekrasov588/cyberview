package com.lstechs.ciso.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.lstechs.ciso.enums.StatusEnum;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.ScriptAssert;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "Task")
@EntityListeners(AuditingEntityListener.class)
@ScriptAssert(lang = "javascript", script = "_this.tskStartDate.getTime() <= (_this.tskEndDate.getTime())", message = "End date must be greater than start date")
public class TaskModel extends CRUDModel {

	@Id
	@Column(name = "tskId", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int tskId;

	@NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "tskName", nullable = false)
	@Type(type = "text")
	private String tskName;

	@Column(name = "tskDescription", nullable = true)
	@Type(type = "text")
	private String tskDescription;

	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@JsonIgnoreProperties(value={"hibernateLazyInitializer", "handler"}, allowSetters = true)
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "tskAssignee", nullable = false)
	private CompanyUserModel tskAssignee;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tskPrjID", nullable = false)
	private ProjectModel tskPrjID;

	@Column(name = "tskStartDate", nullable = true)
	private Date tskStartDate;

	@Column(name = "tskEndDate", nullable = true)
	private Date tskEndDate;

	@Column(name = "tskPriority", nullable = true)
	private int tskPriority;

	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "tskCreatedBy", nullable = false)
	private int tskCreatedBy;

	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "status", nullable = false)
	private StatusEnum status;

	@JsonIgnore
	@OneToOne(mappedBy = "tskId")
	private ModuleTaskModel moduleTaskModel;

	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "cmntTskId")
	private List<TaskCommentModel> taskCommentModel;

	@Transient
	private String url;

	@Transient
	private String artcTitle;

	@Transient
	private String artcContent;

	@Transient
	private boolean isRtl;

	@Transient
	private int stdId;

	@Transient
	private int questionnaireId;

	@Transient
	private int vaId; // vulnerabilityAssessments

	@Transient
	private int secId;

	@Transient
	private int qstId;

	@JsonIgnore
	@OneToMany(mappedBy = "tskId")
	private List<VulnerabilityAssessmentsModel> vulnerabilityAssessmentsModel;

	public TaskModel(String tskName, String tskDescription, CompanyUserModel tskAssignee, ProjectModel tskPrjID, Date tskStartDate,
					 Date tskEndDate, int tskPriority, int tskCreatedBy, StatusEnum status) {
		super();
		this.tskName = tskName;
		this.tskDescription = tskDescription;
		this.tskAssignee = tskAssignee;
		this.tskPrjID = tskPrjID;
		this.tskStartDate = tskStartDate;
		this.tskEndDate = tskEndDate;
		this.tskPriority = tskPriority;
		this.tskCreatedBy = tskCreatedBy;
		this.status = status;
	}

	public TaskModel(TaskModel taskModel) {
		super();
		this.tskName = taskModel.getTskName();
		this.tskDescription = taskModel.getTskDescription();
		this.tskAssignee = taskModel.getTskAssignee();
		this.tskPrjID = taskModel.getTskPrjID();
		this.tskStartDate = taskModel.getTskStartDate();
		this.tskEndDate = taskModel.getTskEndDate();
		this.tskPriority = taskModel.getTskPriority();
		this.tskCreatedBy = taskModel.getTskCreatedBy();
		this.status = taskModel.getStatus();
	}

	public TaskModel() {
		super();
	}

	public int getTskId() {
		return tskId;
	}

	public void setTskId(int tskId) {
		this.tskId = tskId;
	}

	public String getTskName() {
		return tskName;
	}

	public void setTskName(String tskName) {
		this.tskName = tskName;
	}

	public String getTskDescription() {
		return tskDescription;
	}

	public void setTskDescription(String tskDescription) {
		this.tskDescription = tskDescription;
	}

	public CompanyUserModel getTskAssignee() {
		return tskAssignee;
	}

	public void setTskAssignee(CompanyUserModel tskAssignee) {
		this.tskAssignee = tskAssignee;
	}

	public ProjectModel getTskPrjID() {
		return tskPrjID;
	}

	public void setTskPrjID(ProjectModel tskPrjID) {
		this.tskPrjID = tskPrjID;
	}

	public Date getTskStartDate() {
		return tskStartDate;
	}

	public void setTskStartDate(Date tskStartDate) {
		this.tskStartDate = tskStartDate;
	}

	public Date getTskEndDate() {
		return tskEndDate;
	}

	public void setTskEndDate(Date tskEndDate) {
		this.tskEndDate = tskEndDate;
	}

	public int getTskPriority() {
		return tskPriority;
	}

	public void setTskPriority(int tskPriority) {
		this.tskPriority = tskPriority;
	}

	public int getTskCreatedBy() {
		return tskCreatedBy;
	}

	public void setTskCreatedBy(int tskCreatedBy) {
		this.tskCreatedBy = tskCreatedBy;
	}

	public StatusEnum getStatus() {
		return status;
	}

	public void setStatus(StatusEnum status) {
		this.status = status;
	}

	public ModuleTaskModel getModuleTaskModel() {
		return moduleTaskModel;
	}

	public void setModuleTaskModel(ModuleTaskModel moduleTaskModel) {
		this.moduleTaskModel = moduleTaskModel;
	}

	public List<TaskCommentModel> getTaskCommentModel() {
		return taskCommentModel;
	}

	public void setTaskCommentModel(List<TaskCommentModel> taskCommentModel) {
		this.taskCommentModel = taskCommentModel;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getArtcTitle() {
		return artcTitle;
	}

	public void setArtcTitle(String artcTitle) {
		this.artcTitle = artcTitle;
	}

	public String getArtcContent() {
		return artcContent;
	}

	public void setArtcContent(String artcContent) {
		this.artcContent = artcContent;
	}

	public boolean isRtl() {
		return isRtl;
	}

	public void setRtl(boolean rtl) {
		isRtl = rtl;
	}

	public int getStdId() {
		return stdId;
	}

	public void setStdId(int stdId) {
		this.stdId = stdId;
	}

	public int getQuestionnaireId() {
		return questionnaireId;
	}

	public void setQuestionnaireId(int questionnaireId) {
		this.questionnaireId = questionnaireId;
	}

	public List<VulnerabilityAssessmentsModel> getVulnerabilityAssessmentsModel() {
		return vulnerabilityAssessmentsModel;
	}

	public void setVulnerabilityAssessmentsModel(List<VulnerabilityAssessmentsModel> vulnerabilityAssessmentsModel) {
		this.vulnerabilityAssessmentsModel = vulnerabilityAssessmentsModel;
	}

	public int getVaId() {
		return vaId;
	}

	public void setVaId(int vaId) {
		this.vaId = vaId;
	}


	public int getQstId() {
		return qstId;
	}

	public void setQstId(int qstId) {
		this.qstId = qstId;
	}

	public int getSecId() {
		return secId;
	}

	public void setSecId(int secId) {
		this.secId = secId;
	}


}
