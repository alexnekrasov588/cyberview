package com.lstechs.ciso.model;

import java.io.ByteArrayInputStream;
import java.util.List;

public class StdReportsFilesModel {
	private List<Integer> customerStdWithFilesIds;
	private ByteArrayInputStream excelFile;
	public List<Integer> getCustomerStdWithFilesIds() {
		return customerStdWithFilesIds;
	}
	public void setCustomerStdWithFilesIds(List<Integer> customerStdWithFilesIds) {
		this.customerStdWithFilesIds = customerStdWithFilesIds;
	}
	public ByteArrayInputStream getExcelFile() {
		return excelFile;
	}
	public void setExcelFile(ByteArrayInputStream excelFile) {
		this.excelFile = excelFile;
	}
	public StdReportsFilesModel(List<Integer> customerStdWithFilesIds, ByteArrayInputStream excelFile) {
		super();
		this.customerStdWithFilesIds = customerStdWithFilesIds;
		this.excelFile = excelFile;
	}
	
	
}
