package com.lstechs.ciso.model;

public class SingleNewsArticlesModel {
	private int newsArtcId;
	private String newsArtcTitle;
	private String newsArtcUrl;
	private String newsImgUrl;
	private int newsSrcId;
	private String newsArtcContent;
	private String newsArtcSummary;
	private String newsArtcHash;
	private boolean newsIsActive;

	public SingleNewsArticlesModel(int newsArtcId, String newsArtcTitle, String newsArtcUrl, String newsImgUrl, int newsSrcId, String newsArtcContent, String newsArtcSummary, String newsArtcHash, boolean newsIsActive) {
		this.newsArtcId = newsArtcId;
		this.newsArtcTitle = newsArtcTitle;
		this.newsArtcUrl = newsArtcUrl;
		this.newsImgUrl = newsImgUrl;
		this.newsSrcId = newsSrcId;
		this.newsArtcContent = newsArtcContent;
		this.newsArtcSummary = newsArtcSummary;
		this.newsArtcHash = newsArtcHash;
		this.newsIsActive = newsIsActive;
	}

	public SingleNewsArticlesModel(NewsArticlesModel newsArticlesModel) {
		this.newsArtcId = newsArticlesModel.getNewsArtcId();
		this.newsArtcTitle = newsArticlesModel.getNewsArtcTitle();
		this.newsArtcUrl = newsArticlesModel.getNewsArtcUrl();
		this.newsImgUrl = newsArticlesModel.getNewsImgUrl();
		this.newsSrcId = newsArticlesModel.getNewsSrcId().getNewsSrcId();
		this.newsArtcContent = newsArticlesModel.getNewsArtcContent();
		this.newsArtcSummary = newsArticlesModel.getNewsArtcSummary();
		this.newsArtcHash = newsArticlesModel.getNewsArtcHash();
		this.newsIsActive = newsArticlesModel.getNewsIsActive();
	}

	public int getNewsArtcId() {
		return newsArtcId;
	}

	public void setNewsArtcId(int newsArtcId) {
		this.newsArtcId = newsArtcId;
	}

	public String getNewsArtcTitle() {
		return newsArtcTitle;
	}

	public void setNewsArtcTitle(String newsArtcTitle) {
		this.newsArtcTitle = newsArtcTitle;
	}

	public String getNewsArtcUrl() {
		return newsArtcUrl;
	}

	public void setNewsArtcUrl(String newsArtcUrl) {
		this.newsArtcUrl = newsArtcUrl;
	}

	public String getNewsImgUrl() {
		return newsImgUrl;
	}

	public void setNewsImgUrl(String newsImgUrl) {
		this.newsImgUrl = newsImgUrl;
	}

	public int getNewsSrcId() {
		return newsSrcId;
	}

	public void setNewsSrcId(int newsSrcId) {
		this.newsSrcId = newsSrcId;
	}

	public String getNewsArtcContent() {
		return newsArtcContent;
	}

	public void setNewsArtcContent(String newsArtcContent) {
		this.newsArtcContent = newsArtcContent;
	}

	public String getNewsArtcSummary() {
		return newsArtcSummary;
	}

	public void setNewsArtcSummary(String newsArtcSummary) {
		this.newsArtcSummary = newsArtcSummary;
	}

	public String getNewsArtcHash() {
		return newsArtcHash;
	}

	public void setNewsArtcHash(String newsArtcHash) {
		this.newsArtcHash = newsArtcHash;
	}

	public boolean isNewsIsActive() {
		return newsIsActive;
	}

	public void setNewsIsActive(boolean newsIsActive) {
		this.newsIsActive = newsIsActive;
	}
}
