package com.lstechs.ciso.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ProjectsReportModel {
	private boolean prjName;
	private boolean prjStartDate;
	private boolean prjEndDate;
	private boolean prjStatus;
	private boolean schedule;
	private boolean prjProgress;
	private boolean prjManager;
	private boolean prjBudget;
	private boolean prjDescrption;
	private Date startDate;
	private Date endDate;
	
	public String[] getProjectsReportTitle() {
		List<String> titles = new ArrayList<String>();
		if(this.prjName) titles.add("Project Name");
		if(this.prjDescrption) titles.add("Descrption");
		if(this.prjBudget) titles.add("Budget");
		if(this.prjStartDate) titles.add("Start Date");
		if(this.prjEndDate) titles.add("Deadline");
		if(this.prjStatus) titles.add("Status");
		if(this.schedule) titles.add("Schedule");
		if(this.prjProgress) titles.add("Progress");
		if(this.prjManager) titles.add("Manager");
        String[] itemsArray = new String[titles.size()];
        itemsArray = titles.toArray(itemsArray);
		return itemsArray;		
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public boolean isPrjName() {
		return prjName;
	}

	public void setPrjName(boolean prjName) {
		this.prjName = prjName;
	}

	public boolean isPrjStartDate() {
		return prjStartDate;
	}

	public void setPrjStartDate(boolean prjStartDate) {
		this.prjStartDate = prjStartDate;
	}

	public boolean isPrjEndDate() {
		return prjEndDate;
	}

	public void setPrjEndDate(boolean prjEndDate) {
		this.prjEndDate = prjEndDate;
	}

	public boolean isPrjStatus() {
		return prjStatus;
	}

	public void setPrjStatus(boolean prjStatus) {
		this.prjStatus = prjStatus;
	}

	public boolean isSchedule() {
		return schedule;
	}

	public void setSchedule(boolean schedule) {
		this.schedule = schedule;
	}

	public boolean isPrjProgress() {
		return prjProgress;
	}

	public void setPrjProgress(boolean prjProgress) {
		this.prjProgress = prjProgress;
	}

	public boolean isPrjManager() {
		return prjManager;
	}

	public void setPrjManager(boolean prjManager) {
		this.prjManager = prjManager;
	}


	public boolean isPrjBudget() {
		return prjBudget;
	}

	public void setPrjBudget(boolean prjBudget) {
		this.prjBudget = prjBudget;
	}

	public boolean isPrjDescrption() {
		return prjDescrption;
	}

	public void setPrjDescrption(boolean prjDescrption) {
		this.prjDescrption = prjDescrption;
	}
	
	
	
}
