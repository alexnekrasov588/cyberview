package com.lstechs.ciso.model;

import java.util.List;

public class RequestQuestionnaireModel {
    private int stdQtrId;
    private String stdQtrName;
    private List<ChildrenQuestionnaireModel> children;

    public int getStdQtrId() {
        return stdQtrId;
    }

    public void setStdQtrId(int stdQtrId) {
        this.stdQtrId = stdQtrId;
    }

    public String getStdQtrName() {
        return stdQtrName;
    }

    public void setStdQtrName(String stdQtrName) {
        this.stdQtrName = stdQtrName;
    }

    public List<ChildrenQuestionnaireModel> getChildren() {
        return children;
    }

    public void setChildren(List<ChildrenQuestionnaireModel> children) {
        this.children = children;
    }
}


