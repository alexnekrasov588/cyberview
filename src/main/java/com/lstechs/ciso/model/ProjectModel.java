package com.lstechs.ciso.model;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.lstechs.ciso.enums.StatusEnum;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.ScriptAssert;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.lstechs.ciso.enums.PrjStatusEnum;

@Entity
@Table(name = "Project")
@EntityListeners(AuditingEntityListener.class)
@ScriptAssert(lang = "javascript", script = "_this.prjStartDate.before(_this.prjEndDate)", message = "End date must be greater than start date")
public class ProjectModel extends CRUDModel {

	@Id
	@Column(name = "prjId", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int prjId;

	@NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "prjName", nullable = false)
	private String prjName;

	@Column(name = "prjDescription", nullable = true)
	@Type(type = "text")
	private String prjDescription;

	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "prjCmpId", nullable = false)
	private CompanyModel prjCmpId;

	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "prjManagerId", nullable = false)
	private CompanyUserModel prjManagerId;

	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "prjStartDate", nullable = false)
	private Date prjStartDate;

	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "prjEndDate", nullable = false)
	private Date prjEndDate;

	@Column(name = "prjBudget", nullable = true)
	private double prjBudget;

	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "prjStatus", nullable = false)
	private PrjStatusEnum prjStatus;

	@JsonIgnore
	@OneToMany(mappedBy = "tskPrjID")
	private List<TaskModel> taskModel;

	@JsonIgnore
	@OneToMany(mappedBy = "prjId")
	private List<UserProjectModel> userProjectModel;

	@Transient
	private double prjProgressPercent;


	public ProjectModel(String prjName, String prjDescription, CompanyModel prjCmpId, CompanyUserModel prjManagerId, Date prjStartDate, Date prjEndDate,
						double prjBudget, PrjStatusEnum prjStatus, double prjProgressPercent) {
		super();
		this.prjName = prjName;
		this.prjDescription = prjDescription;
		this.prjCmpId = prjCmpId;
		this.prjManagerId = prjManagerId;
		this.prjStartDate = prjStartDate;
		this.prjEndDate = prjEndDate;
		this.prjBudget = prjBudget;
		this.prjStatus = prjStatus;
		this.prjProgressPercent = prjProgressPercent;

	}

//	@JsonIgnore
//	public double getProgress() {
//		int totalTasks = 0;
//		int doneTasks = 0;
//
//		if (taskModel != null && taskModel.size() > 0) {
//			for (TaskModel tmpTaskModel : taskModel) {
//				if (tmpTaskModel.getDeletedAt() == null) {
//					totalTasks++;
//					if (tmpTaskModel.getStatus() == StatusEnum.DONE)
//						doneTasks++;
//				}
//			}
//		}
//		if (totalTasks > 0) {
//			return round2D((double) doneTasks / totalTasks * 100);
//		}
//		return 0;
//	}

	@JsonIgnore
	public double getProgress() {
		long totalDay = 0;
		int done = 0;
		if (taskModel == null || taskModel.size() == 0) {
			if(this.prjStatus.equals(PrjStatusEnum.COMPLETED)) return 100;
			else return 0;
		} else if(taskModel != null && taskModel.size() > 0) {
			for (TaskModel tmpTaskModel : taskModel) {
				if(tmpTaskModel.getDeletedAt() == null) {
					long diffDays = getDifferenceDays(tmpTaskModel.getTskStartDate(),tmpTaskModel.getTskEndDate()) + 1;
					totalDay+= diffDays;
					if (tmpTaskModel.getStatus() == StatusEnum.DONE)
						done+= diffDays;
				}

			}
			if (totalDay > 0)
			return round2D((double) done / totalDay * 100);
		}
		return 0;
	}

	@JsonIgnore
	private static long getDifferenceDays(Date d1, Date d2) {
	    long diff = d2.getTime() - d1.getTime();
	    return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
	}

	@JsonIgnore
	private double round2D(double value) {
		long factor = (long) Math.pow(10, 2);
		value = value * factor;
		long tmp = Math.round(value);
		return (double) tmp / factor;
	}

	public ProjectModel() {
		super();
	}

	public int getPrjId() {
		return prjId;
	}

	public void setPrjId(int prjId) {
		this.prjId = prjId;
	}

	public String getPrjName() {
		return prjName;
	}

	public void setPrjName(String prjName) {
		this.prjName = prjName;
	}

	public String getPrjDescription() {
		return prjDescription;
	}

	public void setPrjDescription(String prjDescription) {
		this.prjDescription = prjDescription;
	}

	public CompanyModel getPrjCmpId() {
		return prjCmpId;
	}

	public void setPrjCmpId(CompanyModel prjCmpId) {
		this.prjCmpId = prjCmpId;
	}

	public CompanyUserModel getPrjManagerId() {
		return prjManagerId;
	}

	public void setPrjManagerId(CompanyUserModel prjManagerId) {
		this.prjManagerId = prjManagerId;
	}

	public Date getPrjStartDate() {
		return prjStartDate;
	}

	public void setPrjStartDate(Date prjStartDate) {
		this.prjStartDate = prjStartDate;
	}

	public Date getPrjEndDate() {
		return prjEndDate;
	}

	public void setPrjEndDate(Date prjEndDate) {
		this.prjEndDate = prjEndDate;
	}

	public double getPrjBudget() {
		return prjBudget;
	}

	public void setPrjBudget(double prjBudget) {
		this.prjBudget = prjBudget;
	}

	public PrjStatusEnum getPrjStatus() {
		return prjStatus;
	}

	public void setPrjStatus(PrjStatusEnum prjStatus) {
		this.prjStatus = prjStatus;
	}

	public double getPrjProgressPercent() {
		return getProgress();
	}

	public void setPrjProgressPercent(double prjProgressPercent) {
		this.prjProgressPercent = prjProgressPercent;
	}

	public List<TaskModel> getTaskModel() {
		return taskModel;
	}

	public void setTaskModel(List<TaskModel> taskModel) {
		this.taskModel = taskModel;
	}

	public List<UserProjectModel> getUserProjectModel() {
		return userProjectModel;
	}

	public void setUserProjectModel(List<UserProjectModel> userProjectModel) {
		this.userProjectModel = userProjectModel;
	}
}
