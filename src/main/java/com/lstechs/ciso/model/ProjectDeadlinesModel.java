package com.lstechs.ciso.model;

public class ProjectDeadlinesModel {
	private ProjectModel projectModel;
	private String delayTime;

	public ProjectDeadlinesModel(ProjectModel projectModel, String delayTime) {
		this.projectModel = projectModel;
		this.delayTime = delayTime;
	}

	public ProjectDeadlinesModel() {
	}

	public ProjectModel getProjectModel() {
		return projectModel;
	}

	public void setProjectModel(ProjectModel projectModel) {
		this.projectModel = projectModel;
	}

	public String getDelayTime() {
		return delayTime;
	}

	public void setDelayTime(String delayTime) {
		this.delayTime = delayTime;
	}
}
