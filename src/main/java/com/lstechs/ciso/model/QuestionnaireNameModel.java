package com.lstechs.ciso.model;

public class QuestionnaireNameModel {
	private int stdQtrId;
	private String stdQtrName;
	private int stdId;
	public int getStdQtrId() {
		return stdQtrId;
	}
	public void setStdQtrId(int stdQtrId) {
		this.stdQtrId = stdQtrId;
	}
	public String getStdQtrName() {
		return stdQtrName;
	}
	public void setStdQtrName(String stdQtrName) {
		this.stdQtrName = stdQtrName;
	}
	public int getStdId() { return stdId; }
	public void setStdId(int stdId) { this.stdId = stdId; }
	public QuestionnaireNameModel(int stdQtrId, String stdQtrName) {
		super();
		this.stdQtrId = stdQtrId;
		this.stdQtrName = stdQtrName;
	}


}
