package com.lstechs.ciso.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

public class ChildrenQuestionnaireModel {
	private String type;
	private StdSectionModel stdSectionModel;
	private StdRequirementsModel stdRequirementsModel;
	private List<ChildrenQuestionnaireModel> children;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	public StdSectionModel getStdSectionModel() {
		return stdSectionModel;
	}

	public void setStdSectionModel(StdSectionModel stdSectionModel) {
		this.stdSectionModel = stdSectionModel;
	}

	public StdRequirementsModel getStdRequirementsModel() {
		return stdRequirementsModel;
	}

	public void setStdRequirementsModel(StdRequirementsModel stdRequirementsModel) {
		this.stdRequirementsModel = stdRequirementsModel;
	}

	public List<ChildrenQuestionnaireModel> getChildren() {
		return children;
	}

	public void setChildren(List<ChildrenQuestionnaireModel> children) {
		this.children = children;
	}
}
