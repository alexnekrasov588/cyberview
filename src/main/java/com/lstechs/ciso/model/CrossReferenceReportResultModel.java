package com.lstechs.ciso.model;
//
//import lombok.Value;
//
//@Value
//public class CrossReferenceReportResultModel {
//
//	double percent;
//	String std_name;
//
//}

public interface CrossReferenceReportResultModel {

	double getPercent();
	String getStandardName();
}
