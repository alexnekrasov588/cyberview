package com.lstechs.ciso.model;

import java.util.Date;

public class SingleLicenseModel {
	private int lcnsId;
	private String lcnsToken;
	private String lcnsFeaturesLst;
	private int lcnsAllowedUsersCount;
	private String lcnsDescription;
	private Date lcnsStartDate;
	private Date lcnsEndDate;
	private boolean lcnsIsActive;
	private String lcnsActivationIP;
	private Date lcnsActivationDate;

	public SingleLicenseModel(int lcnsId, String lcnsToken, String lcnsFeaturesLst, int lcnsAllowedUsersCount, String lcnsDescription, Date lcnsStartDate, Date lcnsEndDate, boolean lcnsIsActive, String lcnsActivationIP, Date lcnsActivationDate) {
		this.lcnsId = lcnsId;
		this.lcnsToken = lcnsToken;
		this.lcnsFeaturesLst = lcnsFeaturesLst;
		this.lcnsAllowedUsersCount = lcnsAllowedUsersCount;
		this.lcnsDescription = lcnsDescription;
		this.lcnsStartDate = lcnsStartDate;
		this.lcnsEndDate = lcnsEndDate;
		this.lcnsIsActive = lcnsIsActive;
		this.lcnsActivationIP = lcnsActivationIP;
		this.lcnsActivationDate = lcnsActivationDate;
	}

	public SingleLicenseModel(LicenseModel licenseModel) {
		this.lcnsId = licenseModel.getLcnsId();
		this.lcnsToken = licenseModel.getLcnsToken();
		this.lcnsFeaturesLst = licenseModel.getLcnsFeaturesLst();
		this.lcnsAllowedUsersCount = licenseModel.getLcnsAllowedUsersCount();
		this.lcnsDescription = licenseModel.getLcnsDescription();
		this.lcnsStartDate = licenseModel.getLcnsStartDate();
		this.lcnsEndDate = licenseModel.getLcnsEndDate();
		this.lcnsIsActive = licenseModel.isLcnsIsActive();
		this.lcnsActivationIP = licenseModel.getLcnsActivationIP();
		this.lcnsActivationDate = licenseModel.getLcnsActivationDate();
	}

	public int getLcnsId() {
		return lcnsId;
	}

	public void setLcnsId(int lcnsId) {
		this.lcnsId = lcnsId;
	}

	public String getLcnsToken() {
		return lcnsToken;
	}

	public void setLcnsToken(String lcnsToken) {
		this.lcnsToken = lcnsToken;
	}

	public String getLcnsFeaturesLst() {
		return lcnsFeaturesLst;
	}

	public void setLcnsFeaturesLst(String lcnsFeaturesLst) {
		this.lcnsFeaturesLst = lcnsFeaturesLst;
	}

	public int getLcnsAllowedUsersCount() {
		return lcnsAllowedUsersCount;
	}

	public void setLcnsAllowedUsersCount(int lcnsAllowedUsersCount) {
		this.lcnsAllowedUsersCount = lcnsAllowedUsersCount;
	}

	public String getLcnsDescription() {
		return lcnsDescription;
	}

	public void setLcnsDescription(String lcnsDescription) {
		this.lcnsDescription = lcnsDescription;
	}

	public Date getLcnsStartDate() {
		return lcnsStartDate;
	}

	public void setLcnsStartDate(Date lcnsStartDate) {
		this.lcnsStartDate = lcnsStartDate;
	}

	public Date getLcnsEndDate() {
		return lcnsEndDate;
	}

	public void setLcnsEndDate(Date lcnsEndDate) {
		this.lcnsEndDate = lcnsEndDate;
	}

	public boolean isLcnsIsActive() {
		return lcnsIsActive;
	}

	public void setLcnsIsActive(boolean lcnsIsActive) {
		this.lcnsIsActive = lcnsIsActive;
	}

	public String getLcnsActivationIP() {
		return lcnsActivationIP;
	}

	public void setLcnsActivationIP(String lcnsActivationIP) {
		this.lcnsActivationIP = lcnsActivationIP;
	}

	public Date getLcnsActivationDate() {
		return lcnsActivationDate;
	}

	public void setLcnsActivationDate(Date lcnsActivationDate) {
		this.lcnsActivationDate = lcnsActivationDate;
	}
}
