package com.lstechs.ciso.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Date;

@MappedSuperclass
public abstract class CRUDModel implements Serializable {


	/**
	 *
	 */
	private static final long serialVersionUID = -4294327892275426561L;

	/* Marker interface for grouping validations to be applied at the time of creating a (new) CRUD. */
	public interface CRUDCreation {
	}

	/* Marker interface for grouping validations to be applied at the time of updating a (existing) CRUD. */
	public interface CRUDUpdate {
	}

	@CreationTimestamp
	@Column(name = "createdAt", nullable = false)
	private Date createdAt;

	@UpdateTimestamp
	@Column(name = "updatedAt", nullable = false)
	private Date updatedAt;

	@JsonIgnore
	@Column(name = "deletedAt")
	private Date deletedAt;

	public CRUDModel() {
	}


	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Date getDeletedAt() {
		return deletedAt;
	}

	public void setDeletedAt(Date deletedAt) {
		this.deletedAt = deletedAt;
	}

}
