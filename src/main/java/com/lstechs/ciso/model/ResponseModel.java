package com.lstechs.ciso.model;


public class ResponseModel {
    private int status;
    private Object results;
    private String description;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Object getResults() {
        return results;
    }

    public void setResults(Object results) {
        this.results = results;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public ResponseModel(int status, Object results, String description) {
        this.status = status;
        this.results = results;
        this.description = description;
    }

}
