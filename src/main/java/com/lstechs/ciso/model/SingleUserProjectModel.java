package com.lstechs.ciso.model;

public class SingleUserProjectModel {
	private int usrPrjId;
	private int usrId;
	private int prjId;

	public SingleUserProjectModel(int usrPrjId, int usrId, int prjId) {
		this.usrPrjId = usrPrjId;
		this.usrId = usrId;
		this.prjId = prjId;
	}

	public SingleUserProjectModel(UserProjectModel userProjectModel) {
		this.usrPrjId = userProjectModel.getUsrPrjId();
		this.usrId = userProjectModel.getUsrId().getCmpUsrId();
		this.prjId = userProjectModel.getPrjId().getPrjId();
	}

	public int getUsrPrjId() {
		return usrPrjId;
	}

	public void setUsrPrjId(int usrPrjId) {
		this.usrPrjId = usrPrjId;
	}

	public int getUsrId() {
		return usrId;
	}

	public void setUsrId(int usrId) {
		this.usrId = usrId;
	}

	public int getPrjId() {
		return prjId;
	}

	public void setPrjId(int prjId) {
		this.prjId = prjId;
	}
}
