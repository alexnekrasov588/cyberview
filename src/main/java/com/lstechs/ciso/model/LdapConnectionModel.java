package com.lstechs.ciso.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "LdapConnection")
@EntityListeners(AuditingEntityListener.class)
public class LdapConnectionModel extends CRUDModel {
	@Id
	@Column(name = "ldapId", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int ldapId;

	@NotEmpty
	@Column(name = "ldapUrl", nullable = false)
	private String ldapUrl; //url("ldap://3.19.30.29:389/dc=ciso,dc=com")

	@Column(name = "ldapUrl2")
	private String ldapUrl2;

	@NotEmpty
	@Column(name = "ldapManagerDn", nullable = false)
	private String ldapManagerDn; //managerDn("cn=Manager,dc=ciso,dc=com")

	@NotEmpty
	@Column(name = "ldapPort", nullable = false)
	private String ldapPort;

	@NotEmpty
	@JsonIgnore
	@Column(name = "ldapManagerPassword", nullable = false)
	private String ldapManagerPassword; //managerPassword("123456")

	@NotEmpty
	@Column(name = "ldapUserSearchBase", nullable = false)
	private String ldapUserSearchBase; //userSearchBase("ou=users")

	@Column(name = "ldapUserSearchFilter")
	private String ldapUserSearchFilter;  //userSearchFilter("(cn={0})")

	public LdapConnectionModel(String ldapUrl, String ldapUrl2, String ldapPort, String ldapManagerDn, String ldapManagerPassword, String ldapUserSearchBase, String ldapUserSearchFilter) {
		if (!ldapUrl.startsWith("ldap://")) {
			this.ldapUrl = "ldap://" + ldapUrl;
		} else {
			this.ldapUrl = ldapUrl;
		}
		if (ldapUrl2 != null) {
			if (!ldapUrl2.startsWith("ldap://")) {
				this.ldapUrl2 = "ldap://" + ldapUrl2;
			} else {
				this.ldapUrl2 = ldapUrl2;
			}
		}
		this.ldapPort = ldapPort;
		this.ldapManagerDn = ldapManagerDn;
		this.ldapManagerPassword = ldapManagerPassword;
		this.ldapUserSearchBase = ldapUserSearchBase;
		this.ldapUserSearchFilter = ldapUserSearchFilter;
	}

	public LdapConnectionModel() {
	}

	public int getLdapId() {
		return ldapId;
	}

	public void setLdapId(int ldapId) {
		this.ldapId = ldapId;
	}

	public String getLdapUrl() {
		if (ldapUrl != null) {
			if (!ldapUrl.startsWith("ldap://")) {
				this.ldapUrl = "ldap://" + ldapUrl;
			}
			return ldapUrl;
		}
		return null;
	}

	public void setLdapUrl(String ldapUrl) {
		if (!ldapUrl.startsWith("ldap://")) {
			this.ldapUrl = "ldap://" + ldapUrl;
		} else {
			this.ldapUrl = ldapUrl;
		}
	}

	public String getLdapUrl2() {
		if (ldapUrl2 != null) {
			if (!ldapUrl2.startsWith("ldap://")) {
				this.ldapUrl2 = "ldap://" + ldapUrl2;
			}
			return ldapUrl2;
		}
		return null;
	}

	public void setLdapUrl2(String ldapUrl2) {
		if (!ldapUrl2.startsWith("ldap://")) {
			this.ldapUrl2 = "ldap://" + ldapUrl2;
		} else {
			this.ldapUrl2 = ldapUrl2;
		}

	}

	public String getLdapPort() {
		return ldapPort;
	}

	public void setLdapPort(String ldapPort) {
		this.ldapPort = ldapPort;
	}

	public String getLdapManagerDn() {
		return ldapManagerDn;
	}

	public void setLdapManagerDn(String ldapManagerDn) {
		this.ldapManagerDn = ldapManagerDn;
	}

	@JsonIgnore
	public String getLdapManagerPassword() {
		return ldapManagerPassword;
	}

	@JsonProperty
	public void setLdapManagerPassword(String ldapManagerPassword) {
		this.ldapManagerPassword = ldapManagerPassword;
	}

	public String getLdapUserSearchBase() {
		return ldapUserSearchBase;
	}

	public void setLdapUserSearchBase(String ldapUserSearchBase) {
		this.ldapUserSearchBase = ldapUserSearchBase;
	}

	public String getLdapUserSearchFilter() {
		return ldapUserSearchFilter;
	}

	public void setLdapUserSearchFilter(String ldapUserSearchFilter) {
		this.ldapUserSearchFilter = ldapUserSearchFilter;
	}

	public String getFullUrl() {
		if (this.getLdapUrl() != null) {
			return this.getLdapUrl() + ":" + this.getLdapPort();
		}
		return null;
	}

	public String getFullUrl2() {
		if (this.getLdapUrl2() != null) {
			return this.getLdapUrl2() + ":" + this.getLdapPort();
		}
		return null;
	}
}
