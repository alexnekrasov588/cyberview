package com.lstechs.ciso.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.lstechs.ciso.enums.StandardTypeEnum;

import org.hibernate.annotations.Type;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Table(name = "Standard")
@EntityListeners(AuditingEntityListener.class)
public class StandardModel extends CRUDModel {

	@Id
	@Column(name = "stdId", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int stdId;

	@NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "stdName", nullable = false)
	@Type(type = "text")
	private String stdName;

	@Column(name = "stdVersion")
	private String stdVersion;

	@Column(name = "stdDescription", nullable = true)
	@Type(type = "text")
	private String stdDescription;

	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "stdIsActive", nullable = false)
	private boolean stdIsActive;

	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "stdIsCompleted", nullable = false)
	private boolean stdIsCompleted = false;

	@Column(name = "stdLvlName")
	private String stdLvlName;

	@Column(name = "isStandardCreatedByCompany", columnDefinition = "boolean default false")
	private boolean isStandardCreatedByCompany;

	@Column(name = "stdType", nullable = true)
	private StandardTypeEnum stdType;

	@Column(name = "stdOnPremId", nullable = true, columnDefinition = "Integer default '0'")
	private int stdOnPremId;

	@JsonIgnore
	@OneToMany(mappedBy = "stdId")
	private List<StdRequirementsModel> stdRequirementsModel;

	@JsonIgnore
	@OneToMany(mappedBy = "stdId")
	private List<StdQuestionaireModel> stdQuestionaireModel;

	@JsonIgnore
	@OneToMany(mappedBy = "stdId")
	private List<CustomerStandardModel> customerStandardModel;

	@JsonIgnore
	@OneToMany(mappedBy = "stdId")
	private List<CrossReferenceModel> crossReferenceModel;

	@JsonIgnore
	@OneToMany(mappedBy = "stdId")
	private List<LicenseStandardModel> licenseStandardModels;

	@JsonIgnore
	@OneToMany(mappedBy = "stdId")
	private List<StandardCompanyStatusModel> standardCompanyStatusModel;

	@Transient
	private List<StdQuestionaireModel> stdQuestionaireList;

	@Transient
	private List<StdRequirementsModel> stdRequirementsList;

	@JsonIgnore
	@OneToMany(mappedBy = "stdId")
	private List<VulnerabilityAssessmentsModel> vulnerabilityAssessmentsModel;

	@OneToOne(mappedBy = "standardId")
	private VendorStandardHeaderTitlesModel vendorStandardHeaderTitlesModel;

	@Transient
	private BigDecimal standardProgress;

	public StandardModel(String stdName, String stdVersion, String stdDescription, String stdLvlName,
						 boolean stdIsActive, boolean stdIsCompleted, StandardTypeEnum stdType) {
		super();
		this.stdName = stdName;
		this.stdVersion = stdVersion;
		this.stdDescription = stdDescription;
		this.stdLvlName = stdLvlName;
		this.stdIsActive = stdIsActive;
		this.stdIsCompleted = stdIsCompleted;
		this.stdType = stdType;
	}

	public StandardModel(StandardModel standardModel) {
		super();
		this.stdName = standardModel.getStdName();
		this.stdVersion = standardModel.getStdVersion();
		this.stdDescription = standardModel.getStdDescription();
		this.stdLvlName = standardModel.getStdLvlName();
		this.stdIsActive = standardModel.isStdIsActive();
		this.stdType = standardModel.getStdType();
	}

	public StandardModel() {
		super();
	}

	public int getStdId() {
		return stdId;
	}

	public void setStdId(int stdId) {
		this.stdId = stdId;
	}

	public String getStdName() {
		return stdName;
	}

	public void setStdName(String stdName) {
		this.stdName = stdName;
	}

	public String getStdVersion() {
		return stdVersion;
	}

	public void setStdVersion(String stdVersion) {
		this.stdVersion = stdVersion;
	}

	public String getStdDescription() {
		return stdDescription;
	}

	public void setStdDescription(String stdDescription) {
		this.stdDescription = stdDescription;
	}

	public boolean isStdIsActive() {
		return stdIsActive;
	}

	public void setStdIsActive(boolean stdIsActive) {
		this.stdIsActive = stdIsActive;
	}

	public boolean isStdIsCompleted() {
		return stdIsCompleted;
	}

	public void setStdIsCompleted(boolean stdIsCompleted) {
		this.stdIsCompleted = stdIsCompleted;
	}

	public List<StdRequirementsModel> getStdRequirementsModel() {
		return stdRequirementsModel;
	}

	public void setStdRequirementsModel(List<StdRequirementsModel> stdRequirementsModel) {
		this.stdRequirementsModel = stdRequirementsModel;
	}

	public List<StdQuestionaireModel> getStdQuestionaireModel() {
		return stdQuestionaireModel;
	}

	public void setStdQuestionaireModel(List<StdQuestionaireModel> stdQuestionaireModel) {
		this.stdQuestionaireModel = stdQuestionaireModel;
	}

	public List<CustomerStandardModel> getCustomerStandardModel() {
		return customerStandardModel;
	}

	public void setCustomerStandardModel(List<CustomerStandardModel> customerStandardModel) {
		this.customerStandardModel = customerStandardModel;
	}

	public String getStdLvlName() {
		return stdLvlName;
	}

	public void setStdLvlName(String stdLvlName) {
		this.stdLvlName = stdLvlName;
	}

	public List<CrossReferenceModel> getCrossReferenceModel() {
		return crossReferenceModel;
	}

	public void setCrossReferenceModel(List<CrossReferenceModel> crossReferenceModel) {
		this.crossReferenceModel = crossReferenceModel;
	}

	public List<LicenseStandardModel> getLicenseStandardModels() {
		return licenseStandardModels;
	}

	public void setLicenseStandardModels(List<LicenseStandardModel> licenseStandardModels) {
		this.licenseStandardModels = licenseStandardModels;
	}

	public List<StdQuestionaireModel> getStdQuestionaireList() {
		return stdQuestionaireList;
	}

	public void setStdQuestionaireList(List<StdQuestionaireModel> stdQuestionaireList) {
		this.stdQuestionaireList = stdQuestionaireList;
	}

	public List<StdRequirementsModel> getStdRequirementsList() {
		return stdRequirementsList;
	}

	public void setStdRequirementsList(List<StdRequirementsModel> stdRequirementsList) {
		this.stdRequirementsList = stdRequirementsList;
	}

	public StandardTypeEnum getStdType() {
		return stdType;
	}

	public void setStdType(StandardTypeEnum stdType) {
		this.stdType = stdType;
	}

	public List<VulnerabilityAssessmentsModel> getVulnerabilityAssessmentsModel() {
		return vulnerabilityAssessmentsModel;
	}

	public void setVulnerabilityAssessmentsModel(List<VulnerabilityAssessmentsModel> vulnerabilityAssessmentsModel) {
		this.vulnerabilityAssessmentsModel = vulnerabilityAssessmentsModel;
	}

	public int getStdOnPremId() {
		return stdOnPremId;
	}

	public void setStdOnPremId(int stdOnPremId) {
		this.stdOnPremId = stdOnPremId;
	}

	public boolean isStandardCreatedByCompany() {
		return isStandardCreatedByCompany;
	}

	public void setStandardCreatedByCompany(boolean standardCreatedByCompany) { isStandardCreatedByCompany = standardCreatedByCompany; }

	public BigDecimal getStandardProgress() { return standardProgress; }

	public void setStandardProgress(BigDecimal standardProgress) { this.standardProgress = standardProgress; }
}
