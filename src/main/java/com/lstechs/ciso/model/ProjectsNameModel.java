package com.lstechs.ciso.model;

public class ProjectsNameModel {

	private int prjId;
	private String prjName;
	public int getPrjId() {
		return prjId;
	}
	public void setPrjId(int prjId) {
		this.prjId = prjId;
	}
	public String getPrjName() {
		return prjName;
	}
	public void setPrjName(String prjName) {
		this.prjName = prjName;
	}
	public ProjectsNameModel(int prjId, String prjName) {
		super();
		this.prjId = prjId;
		this.prjName = prjName;
	}
	
	
	
}
