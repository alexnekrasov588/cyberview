package com.lstechs.ciso.model;

public class FilterModel {
	private int id;
	private String value;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public FilterModel(int id, String value) {
		super();
		this.id = id;
		this.value = value;
	}
	
	
}
