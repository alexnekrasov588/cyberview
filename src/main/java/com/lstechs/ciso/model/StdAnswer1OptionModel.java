package com.lstechs.ciso.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Type;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "StdAnswer1Option")
@EntityListeners(AuditingEntityListener.class)
public class StdAnswer1OptionModel extends CRUDModel {

	@Id
	@Column(name = "sA1OptId", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int sA1OptId;

	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sqoQstId", nullable = false)
	private StdQtrQuestionsModel sqoQstId;

	@NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "sA1Value", nullable = false)
	@Type(type = "text")
	private String sA1Value;

	@NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "sA1Label", nullable = false)
	@Type(type = "text")
	private String sA1Label;

	@Column(name = "stdRqrOnPremId", columnDefinition = "Integer default '0'")
	private int stdOpt1OnPremId;

	@Transient
	public int qst;

	public StdAnswer1OptionModel() {
	}

	public StdAnswer1OptionModel(int sA1OptId, String sA1Value, String sA1Label) {
		this.sA1OptId = sA1OptId;
		this.sA1Value = sA1Value;
		this.sA1Label = sA1Label;
	}

	public StdAnswer1OptionModel(String sA1Value, String sA1Label) {
		this.sA1Value = sA1Value;
		this.sA1Label = sA1Label;
	}

	public int getsA1OptId() {
		return sA1OptId;
	}

	public void setsA1OptId(int sA1OptId) {
		this.sA1OptId = sA1OptId;
	}

	@JsonIgnore
	public StdQtrQuestionsModel getSqoQstId() {
		return sqoQstId;
	}

	@JsonProperty
	public void setSqoQstId(StdQtrQuestionsModel sqoQstId) {
		this.sqoQstId = sqoQstId;
	}

	public String getsA1Value() {
		return sA1Value;
	}

	public void setsA1Value(String sA1Value) {
		this.sA1Value = sA1Value;
	}

	public String getsA1Label() {
		return sA1Label;
	}

	public void setsA1Label(String sA1Label) {
		this.sA1Label = sA1Label;
	}

	public int getStdOpt1OnPremId() {
		return stdOpt1OnPremId;
	}

	public void setStdOpt1OnPremId(int stdOpt1OnPremId) {
		this.stdOpt1OnPremId = stdOpt1OnPremId;
	}

	public int getQst() {
//		if (this.sqoQstId != null)
//			return this.sqoQstId.getQtrQstId();
//		else return 0;
		return this.qst;
	}
}
