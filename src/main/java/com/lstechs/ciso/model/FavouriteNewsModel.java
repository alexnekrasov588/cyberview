package com.lstechs.ciso.model;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import javax.validation.constraints.NotNull;

@Entity
@Table(name = "FavouriteNews")
@EntityListeners(AuditingEntityListener.class)
public class FavouriteNewsModel extends CRUDModel {

	@Id
    @Column(name = "fvrtNewsId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int fvrtNewsId;

	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @NotNull(groups = { CRUDCreation.class, CRUDUpdate.class })
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "newsArtcId",nullable = false)
    private NewsArticlesModel newsArtcId;

	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@NotNull(groups = { CRUDCreation.class, CRUDUpdate.class })
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cmpUsrId",nullable = false)
    private CompanyUserModel cmpUsrId;

	@Column(name = "fvrtNewsName", nullable = true)
	private String fvrtNewsName;

	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "folder", nullable = true)
	private FavouriteFoldersModel folder;

    public FavouriteNewsModel(NewsArticlesModel newsArtcId, CompanyUserModel cmpUsrId,String fvrtNewsName,FavouriteFoldersModel favouriteFoldersModel) {
        super();
        this.newsArtcId = newsArtcId;
        this.cmpUsrId = cmpUsrId;
        this.folder = favouriteFoldersModel;
        this.fvrtNewsName = fvrtNewsName;
    }

    public FavouriteNewsModel() {
        super();
    }

    public int getFvrtNewsId() {
        return fvrtNewsId;
    }

    public void setFvrtNewsId(int fvrtNewsId) {
        this.fvrtNewsId = fvrtNewsId;
    }

    public NewsArticlesModel getNewsArtcId() {
        return newsArtcId;
    }

    public void setNewsArtcId(NewsArticlesModel newsArtcId) {
        this.newsArtcId = newsArtcId;
    }

    public CompanyUserModel getCmpUsrId() {
        return cmpUsrId;
    }

    public void setCmpUsrId(CompanyUserModel cmpUsrId) {
        this.cmpUsrId = cmpUsrId;
    }

	public FavouriteFoldersModel getFolder() {
		return folder;
	}

	public void setFolder(FavouriteFoldersModel folder) {
		this.folder = folder;
	}

	public String getFvrtNewsName() {
		return fvrtNewsName;
	}

	public void setFvrtNewsName(String fvrtNewsName) {
		this.fvrtNewsName = fvrtNewsName;
	}


}
