package com.lstechs.ciso.model;

public class SingleCrossReferenceModel {
	private int crsRefId;
	private SingleStdRequirementsModel stdRqrId;
	private int stdId;

	public SingleCrossReferenceModel(int crsRefId, SingleStdRequirementsModel stdRqrId, int stdId) {
		this.crsRefId = crsRefId;
		this.stdRqrId =stdRqrId;
		this.stdId = stdId;
	}

	public SingleCrossReferenceModel(CrossReferenceModel crossReferenceModel, SingleStdRequirementsModel stdRequirementsModel) {
		this.crsRefId = crossReferenceModel.getCrsRefId();
		this.stdRqrId = stdRequirementsModel;
		this.stdId = crossReferenceModel.getStdId().getStdId();
	}

	public int getCrsRefId() {
		return crsRefId;
	}

	public void setCrsRefId(int crsRefId) {
		this.crsRefId = crsRefId;
	}

	public SingleStdRequirementsModel getStdRqrId() {
		return stdRqrId;
	}

	public void setStdRqrId(SingleStdRequirementsModel stdRqrId) {
		this.stdRqrId = stdRqrId;
	}

	public int getStdId() {
		return stdId;
	}

	public void setStdId(int stdId) {
		this.stdId = stdId;
	}
}
