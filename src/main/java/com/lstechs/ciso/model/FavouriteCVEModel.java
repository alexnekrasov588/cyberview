package com.lstechs.ciso.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.lstechs.ciso.model.CRUDModel.CRUDCreation;
import com.lstechs.ciso.model.CRUDModel.CRUDUpdate;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "FavouriteCve")
@EntityListeners(AuditingEntityListener.class)
public class FavouriteCVEModel extends CRUDModel {

	@Id
    @Column(name = "fvrtCveId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int fvrtCveId;

    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @NotNull(groups = { CRUDCreation.class, CRUDUpdate.class })
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cveId",nullable = false)
    private CVEModel cveId;

    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @NotNull(groups = { CRUDCreation.class, CRUDUpdate.class })
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cmpUserId",nullable = false)
    private CompanyUserModel cmpUserId;

	@Column(name = "fvrtCveName", nullable = true)
	private String fvrtCveName;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "folder", nullable = true)
	private FavouriteFoldersModel folder;

    public FavouriteCVEModel(CVEModel cveId, CompanyUserModel cmpUserId,String fvrtCveName,FavouriteFoldersModel favouriteFoldersModel) {
        super();
        this.cveId = cveId;
        this.cmpUserId = cmpUserId;
        this.fvrtCveName = fvrtCveName;
        this.folder = favouriteFoldersModel;
    }

    public FavouriteCVEModel() {
        super();
    }

    public int getFvrtCveId() {
        return fvrtCveId;
    }

    public void setFvrtCveId(int fvrtCveId) {
        this.fvrtCveId = fvrtCveId;
    }

    public CVEModel getCveId() {
        return cveId;
    }

    public void setCveId(CVEModel cveId) {
        this.cveId = cveId;
    }

    public CompanyUserModel getCmpUserId() {
        return cmpUserId;
    }

    public void setCmpUserId(CompanyUserModel cmpUserId) {
        this.cmpUserId = cmpUserId;
    }

	public String getFvrtCveName() {
		return fvrtCveName;
	}

	public void setFvrtCveName(String fvrtCveName) {
		this.fvrtCveName = fvrtCveName;
	}

	public FavouriteFoldersModel getFolder() {
		return folder;
	}

	public void setFolder(FavouriteFoldersModel folder) {
		this.folder = folder;
	}
	
	
}
