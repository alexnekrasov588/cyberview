package com.lstechs.ciso.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Type;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "StdAnswer2Option")
@EntityListeners(AuditingEntityListener.class)
public class StdAnswer2OptionModel extends CRUDModel {

	@Id
	@Column(name = "sA1OptId", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int sA2OptId;

	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sqoQstId", nullable = false)
	private StdQtrQuestionsModel sqoQstId;

	@NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "sA1Value", nullable = false)
	@Type(type = "text")
	private String sA2Value;

	@NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "sA1Label", nullable = false)
	@Type(type = "text")
	private String sA2Label;

	@Column(name = "stdRqrOnPremId", columnDefinition = "Integer default '0'")
	private int stdOpt2OnPremId;

	@Transient
	public int qst;

	public StdAnswer2OptionModel() {
	}

	public StdAnswer2OptionModel(int sA2OptId, String sA2Value, String sA2Label) {
		this.sA2OptId = sA2OptId;
		this.sA2Value = sA2Value;
		this.sA2Label = sA2Label;
	}

	public StdAnswer2OptionModel(String sA2Value, String sA2Label) {
		this.sA2Value = sA2Value;
		this.sA2Label = sA2Label;
	}

	public int getsA2OptId() {
		return sA2OptId;
	}

	public void setsA1OptId(int sA2OptId) {
		this.sA2OptId = sA2OptId;
	}

	@JsonIgnore
	public StdQtrQuestionsModel getSqoQstId() {
		return sqoQstId;
	}

	@JsonProperty
	public void setSqoQstId(StdQtrQuestionsModel sqoQstId) {
		this.sqoQstId = sqoQstId;
	}

	public String getsA2Value() {
		return sA2Value;
	}

	public void setsA2Value(String sA2Value) {
		this.sA2Value = sA2Value;
	}

	public String getsA2Label() {
		return sA2Label;
	}

	public void setsA2Label(String sA2Label) {
		this.sA2Label = sA2Label;
	}

	public int getStdOpt2OnPremId() {
		return stdOpt2OnPremId;
	}

	public void setStdOpt2OnPremId(int stdOpt2OnPremId) {
		this.stdOpt2OnPremId = stdOpt2OnPremId;
	}

	public int getQst() {
//		if (this.sqoQstId != null)
//			return this.sqoQstId.getQtrQstId();
//		else return 0;
		return this.qst;
	}
}
