package com.lstechs.ciso.model;

public class StandardComplianceModel {
	private StandardModel standardModel;
	private double progress;
	private String cycleEndDate;

	public StandardComplianceModel(StandardModel standardModel, double progress, String cycleEndDate) {
		this.standardModel = standardModel;
		this.progress = progress;
		this.cycleEndDate = cycleEndDate;
	}

	public StandardComplianceModel() {
	}

	public StandardModel getStandardModel() {
		return standardModel;
	}

	public void setStandardModel(StandardModel standardModel) {
		this.standardModel = standardModel;
	}

	public double getProgress() {
		return progress;
	}

	public void setProgress(double progress) {
		this.progress = progress;
	}

	public String getCycleEndDate() {
		return cycleEndDate;
	}

	public void setCycleEndDate(String cycleEndDate) {
		this.cycleEndDate = cycleEndDate;
	}
}
