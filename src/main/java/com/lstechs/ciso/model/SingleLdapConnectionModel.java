package com.lstechs.ciso.model;

public class SingleLdapConnectionModel {
	private int ldapId;
	private String ldapUrl;
	private String ldapManagerDn;
	private String ldapManagerPassword;
	private String ldapUserSearchBase;
	private String ldapUserSearchFilter;

	public SingleLdapConnectionModel(int ldapId, String ldapUrl, String ldapManagerDn, String ldapManagerPassword, String ldapUserSearchBase, String ldapUserSearchFilter) {
		this.ldapId = ldapId;
		this.ldapUrl = ldapUrl;
		this.ldapManagerDn = ldapManagerDn;
		this.ldapManagerPassword = ldapManagerPassword;
		this.ldapUserSearchBase = ldapUserSearchBase;
		this.ldapUserSearchFilter = ldapUserSearchFilter;
	}

	public SingleLdapConnectionModel(LdapConnectionModel ldapConnectionModel) {
		this.ldapId = ldapConnectionModel.getLdapId();
		this.ldapUrl = ldapConnectionModel.getLdapUrl();
		this.ldapManagerDn = ldapConnectionModel.getLdapManagerDn();
		this.ldapManagerPassword = ldapConnectionModel.getLdapManagerPassword();
		this.ldapUserSearchBase = ldapConnectionModel.getLdapUserSearchBase();
		this.ldapUserSearchFilter = ldapConnectionModel.getLdapUserSearchFilter();
	}

	public int getLdapId() {
		return ldapId;
	}

	public void setLdapId(int ldapId) {
		this.ldapId = ldapId;
	}

	public String getLdapUrl() {
		return ldapUrl;
	}

	public void setLdapUrl(String ldapUrl) {
		this.ldapUrl = ldapUrl;
	}

	public String getLdapManagerDn() {
		return ldapManagerDn;
	}

	public void setLdapManagerDn(String ldapManagerDn) {
		this.ldapManagerDn = ldapManagerDn;
	}

	public String getLdapManagerPassword() {
		return ldapManagerPassword;
	}

	public void setLdapManagerPassword(String ldapManagerPassword) {
		this.ldapManagerPassword = ldapManagerPassword;
	}

	public String getLdapUserSearchBase() {
		return ldapUserSearchBase;
	}

	public void setLdapUserSearchBase(String ldapUserSearchBase) {
		this.ldapUserSearchBase = ldapUserSearchBase;
	}

	public String getLdapUserSearchFilter() {
		return ldapUserSearchFilter;
	}

	public void setLdapUserSearchFilter(String ldapUserSearchFilter) {
		this.ldapUserSearchFilter = ldapUserSearchFilter;
	}
}
