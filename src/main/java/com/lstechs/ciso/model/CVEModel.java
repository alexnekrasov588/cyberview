package com.lstechs.ciso.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Type;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.util.DigestUtils;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "CVE")
@EntityListeners(AuditingEntityListener.class)
public class CVEModel extends CRUDModel {

	@Id
	@Column(name = "cveId", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int cveId;

	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cveSrcId", nullable = false)
	private CVESourcesModel cveSrcId;

	@NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "cveTitle", nullable = false)
	private String cveTitle;

	@Column(name = "cveDescription", nullable = true)
	@Type(type = "text")
	private String cveDescription;

	@Column(name = "cveUrl", nullable = true)
	private String cveUrl;

	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "cveHash", nullable = false, unique = true)
	private String cveHash;

	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "cveIsAvilable", nullable = false)
	private boolean cveIsAvilable;

	@Column(name = "publishedDate")
	private Date publishedDate;

	@Column(name = "cveUpdateOnPrem")
	private Date cveUpdateOnPrem;

	@JsonIgnore
	@OneToMany(mappedBy = "cveId")
	private List<FavouriteCVEModel> favouriteCVEModel;

	@JsonIgnore
	@OneToMany(mappedBy = "cveId")
	private List<VulnerabilityAssessmentsModel> vulnerabilityAssessmentsModel;

	@Transient
	private TaskModel taskModel;

	public CVEModel(String cveTitle, String cveDescription, String cveUrl, boolean cveIsAvailable, Date publishedDate, CVESourcesModel cveSrcId) {
		super();
		this.cveTitle = cveTitle;
		this.cveDescription = cveDescription;
		this.cveUrl = cveUrl;
		this.cveIsAvilable = cveIsAvailable;
		this.cveSrcId = cveSrcId;
		this.cveHash = DigestUtils.md5DigestAsHex(cveUrl.getBytes());
		this.publishedDate = publishedDate;
	}

	public CVEModel(CVEModel cveModel) {
		super();
		this.cveTitle = cveModel.getCveTitle();
		this.cveDescription = cveModel.getCveDescription();
		this.cveUrl = cveModel.getCveUrl();
		this.cveIsAvilable = cveModel.isCveIsAvilable();
		this.cveSrcId = cveModel.getCveSrcId();
		this.cveHash = cveModel.getCveHash();
		this.publishedDate = cveModel.getPublishedDate();
	}

	public CVEModel() {
		super();
	}

	public int getCveId() {
		return cveId;
	}

	public void setCveId(int cveId) {
		this.cveId = cveId;
	}

	public String getCveTitle() {
		return cveTitle;
	}

	public void setCveTitle(String cveTitle) {
		this.cveTitle = cveTitle;
	}

	public String getCveDescription() {
		return cveDescription;
	}

	public void setCveDescription(String cveDescription) {
		this.cveDescription = cveDescription;
	}

	public boolean isCveIsAvilable() {
		return cveIsAvilable;
	}

	public void setCveIsAvilable(boolean cveIsAvilable) {
		this.cveIsAvilable = cveIsAvilable;
	}

	public Date getPublishedDate() { return publishedDate;	}

	public void setPublishedDate(Date publishedDate) {	this.publishedDate = publishedDate;	}

	public List<FavouriteCVEModel> getFavouriteCVEModel() {
		return favouriteCVEModel;
	}

	public void setFavouriteCVEModel(List<FavouriteCVEModel> favouriteCVEModel) {
		this.favouriteCVEModel = favouriteCVEModel;
	}

	public CVESourcesModel getCveSrcId() {
		return cveSrcId;
	}

	public void setCveSrcId(CVESourcesModel cveSrcId) {
		this.cveSrcId = cveSrcId;
	}

	public String getCveHash() {
		return cveHash;
	}

	public void setCveHash(String cveHash) {
		this.cveHash = cveHash;
	}

	public String getCveUrl() {
		return cveUrl;
	}

	public void setCveUrl(String cveUrl) {
		this.cveUrl = cveUrl;
	}

	public SingleTaskModel getTaskModel() {
		if (taskModel != null) {
			return new SingleTaskModel(taskModel, new SingleCompanyUserModel(taskModel.getTskAssignee()));
		} else return null;
	}

	public void setTaskModel(TaskModel taskModel) {
		this.taskModel = taskModel;
	}

	public List<VulnerabilityAssessmentsModel> getVulnerabilityAssessmentsModel() {
		return vulnerabilityAssessmentsModel;
	}

	public void setVulnerabilityAssessmentsModel(List<VulnerabilityAssessmentsModel> vulnerabilityAssessmentsModel) {
		this.vulnerabilityAssessmentsModel = vulnerabilityAssessmentsModel;
	}

	public Date getCveUpdateOnPrem() {
		return cveUpdateOnPrem;
	}

	public void setCveUpdateOnPrem(Date cveUpdateOnPrem) {
		this.cveUpdateOnPrem = cveUpdateOnPrem;
	}


}
