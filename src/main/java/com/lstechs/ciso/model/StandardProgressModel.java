package com.lstechs.ciso.model;

public class StandardProgressModel {
	private int stdId;
	private String stdName;
	private float stdProgress;

	public StandardProgressModel(int stdId, String stdName, float stdProgress) {
		this.stdId = stdId;
		this.stdName = stdName;
		this.stdProgress = stdProgress;
	}
	public StandardProgressModel(StandardProgressModel standardProgressModel) {
		this.stdId = standardProgressModel.getStdId();
		this.stdName = standardProgressModel.getStdName();
		this.stdProgress = standardProgressModel.getStdProgress();
	}

	public StandardProgressModel() {
	}

	public int getStdId() {
		return stdId;
	}

	public void setStdId(int stdId) {
		this.stdId = stdId;
	}

	public String getStdName() {
		return stdName;
	}

	public void setStdName(String stdName) {
		this.stdName = stdName;
	}

	public float getStdProgress() {
		return stdProgress;
	}

	public void setStdProgress(float stdProgress) {
		this.stdProgress = stdProgress;
	}
}
