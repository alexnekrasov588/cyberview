package com.lstechs.ciso.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "companyUser" ,indexes = {@Index(name = "emailIndex",  columnList="cmpUsrEmail")})
@EntityListeners(AuditingEntityListener.class)
public class CompanyUserModel extends CRUDModel {

	@Id
	@Column(name = "cmpUsrId", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JoinColumn(name = "tskAssignee", nullable = false)
    @JsonIgnoreProperties(value= "tskAssignee", allowSetters=true)
	private int cmpUsrId;

	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "cmpUsrCmpId", nullable = false)
	private CompanyModel cmpUsrCmpId;

	@Column(name = "cmpUsrFullName", nullable = true)
	private String cmpUsrFullName;

	@NotEmpty(groups = {CRUDCreation.class})
	@JsonIgnore
	@Column(name = "cmpUsrPassword", nullable = false)
	private String cmpUsrPassword;

	@NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Email(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "cmpUsrEmail", nullable = false)
	private String cmpUsrEmail;

	//	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "cmpUsrRoleId", nullable = false)
	private RoleModel cmpUsrRoleId;

	@JsonIgnore
	@OneToMany(mappedBy = "tskAssignee")
	private List<TaskModel> taskModel;

	@JsonIgnore
	@OneToMany(mappedBy = "prjManagerId")
	private List<ProjectModel> projectModel;

	@JsonIgnore
	@OneToMany(mappedBy = "usrId")
	private List<UserProjectModel> userProjectModel;

	@JsonIgnore
	@OneToMany(mappedBy = "cmpUsrId")
	private List<FavouriteNewsModel> favouriteNewsModel;

	@JsonIgnore
	@OneToMany(mappedBy = "cmpUserId")
	private List<FavouriteCVEModel> favouriteCVEModel;

	@JsonIgnore
	@OneToMany(mappedBy = "cmpUserId")
	private List<FavouriteFoldersModel> favouriteFoldersModels;

	@JsonIgnore
	@OneToMany(mappedBy = "usrId")
	private List<TaskCommentModel> taskCommentModel;

	@JsonIgnore
	@Lob
	@Column(name = "cmpUsrProfileImgFile")
	private byte[] cmpUsrProfileImgFile;


	public CompanyUserModel(String cmpUsrFullName, String cmpUsrPassword, String cmpUsrEmail, CompanyModel cmpUsrCmpId, RoleModel cmpUsrRoleId, byte[] cmpUsrProfileImgFile) {
		super();
		this.cmpUsrFullName = cmpUsrFullName;
		this.cmpUsrPassword = cmpUsrPassword;
		this.cmpUsrEmail = cmpUsrEmail.toLowerCase();
		this.cmpUsrCmpId = cmpUsrCmpId;
		this.cmpUsrRoleId = cmpUsrRoleId;
		this.cmpUsrProfileImgFile = cmpUsrProfileImgFile;
	}

	public CompanyUserModel(CompanyUserModel companyUserModel) {
		super();
		this.cmpUsrFullName = companyUserModel.getCmpUsrFullName();
		this.cmpUsrPassword = companyUserModel.getCmpUsrPassword();
		this.cmpUsrEmail = companyUserModel.getCmpUsrEmail().toLowerCase();
		this.cmpUsrCmpId = companyUserModel.getCmpUsrCmpId();
		this.cmpUsrRoleId = companyUserModel.getCmpUsrRoleId();
		this.cmpUsrProfileImgFile = companyUserModel.getCmpUsrProfileImgFile();
	}

	public CompanyUserModel() {
		super();
	}

	public int getCmpUsrId() {
		return cmpUsrId;
	}

	public void setCmpUsrId(int cmpUsrId) {
		this.cmpUsrId = cmpUsrId;
	}


	public String getCmpUsrFullName() {
		return cmpUsrFullName;
	}

	public void setCmpUsrFullName(String cmpUsrFullName) {
		this.cmpUsrFullName = cmpUsrFullName;
	}

	@JsonIgnore
	public String getCmpUsrPassword() {
		return cmpUsrPassword;
	}

	@JsonProperty
	public void setCmpUsrPassword(String cmpUsrPassword) {
		this.cmpUsrPassword = cmpUsrPassword;
	}


	public String getCmpUsrEmail() {
		return cmpUsrEmail.toLowerCase();
	}

	public void setCmpUsrEmail(String cmpUsrEmail) {
		this.cmpUsrEmail = cmpUsrEmail.toLowerCase();
	}

	public CompanyModel getCmpUsrCmpId() {
		return cmpUsrCmpId;
	}

	public void setCmpUsrCmpId(CompanyModel cmpUsrCmpId) {
		if (cmpUsrCmpId != null)
			this.cmpUsrCmpId = cmpUsrCmpId;
	}

	public RoleModel getCmpUsrRoleId() {
		return cmpUsrRoleId;
	}

	public void setCmpUsrRoleId(RoleModel cmpUsrRoleId) {
		this.cmpUsrRoleId = cmpUsrRoleId;
	}

	public List<TaskModel> getTaskModel() {
		return taskModel;
	}

	public void setTaskModel(List<TaskModel> taskModel) {
		this.taskModel = taskModel;
	}

	public List<ProjectModel> getProjectModel() {
		return projectModel;
	}

	public void setProjectModel(List<ProjectModel> projectModel) {
		this.projectModel = projectModel;
	}

	public List<UserProjectModel> getUserProjectModel() {
		return userProjectModel;
	}

	public void setUserProjectModel(List<UserProjectModel> userProjectModel) {
		this.userProjectModel = userProjectModel;
	}

	public List<FavouriteNewsModel> getFavouriteNewsModel() {
		return favouriteNewsModel;
	}

	public void setFavouriteNewsModel(List<FavouriteNewsModel> favouriteNewsModel) {
		this.favouriteNewsModel = favouriteNewsModel;
	}

	public List<FavouriteCVEModel> getFavouriteCVEModel() {
		return favouriteCVEModel;
	}

	public void setFavouriteCVEModel(List<FavouriteCVEModel> favouriteCVEModel) {
		this.favouriteCVEModel = favouriteCVEModel;
	}

	public List<TaskCommentModel> getTaskCommentModel() {
		return taskCommentModel;
	}

	public void setTaskCommentModel(List<TaskCommentModel> taskCommentModel) {
		this.taskCommentModel = taskCommentModel;
	}

	public byte[] getCmpUsrProfileImgFile() {
		return cmpUsrProfileImgFile;
	}

	public void setCmpUsrProfileImgFile(byte[] cmpUsrLogoFile) {
		this.cmpUsrProfileImgFile = cmpUsrLogoFile;
	}

	public List<FavouriteFoldersModel> getFavouriteFoldersModels() {
		return favouriteFoldersModels;
	}

	public void setFavouriteFoldersModels(List<FavouriteFoldersModel> favouriteFoldersModels) {
		this.favouriteFoldersModels = favouriteFoldersModels;
	}


}

