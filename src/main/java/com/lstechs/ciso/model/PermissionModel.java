package com.lstechs.ciso.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.lstechs.ciso.enums.ModuleEnum;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "Permission")
@EntityListeners(AuditingEntityListener.class)
public class PermissionModel extends CRUDModel {

	@Id
	@Column(name = "permId", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int permId;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "roleId", nullable = false)
	private RoleModel roleId;

	@NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "module", nullable = false)
	private ModuleEnum module;

	@NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "isCreate", nullable = false)
	private boolean isCreate;

	@NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "isRead", nullable = false)
	private boolean isRead;

	@NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "isUpdate", nullable = false)
	private boolean isUpdate;

	@NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "isDelete", nullable = false)
	private boolean isDelete;

	public PermissionModel(RoleModel roleId, ModuleEnum module, boolean isCreate, boolean isRead, boolean isUpdate, boolean isDelete) {
		this.roleId = roleId;
		this.module = module;
		this.isCreate = isCreate;
		this.isRead = isRead;
		this.isUpdate = isUpdate;
		this.isDelete = isDelete;
	}

	public PermissionModel(PermissionModel permissionModel) {
		this.module = permissionModel.module;
		this.isCreate = permissionModel.isCreate;
		this.isRead = permissionModel.isRead;
		this.isUpdate = permissionModel.isUpdate;
		this.isDelete = permissionModel.isDelete;
	}

	public PermissionModel() {
		super();
	}

	public int getPermId() {
		return permId;
	}

	public void setPermId(int permId) {
		this.permId = permId;
	}

	public RoleModel getRoleId() {
		return roleId;
	}

	public void setRoleId(RoleModel roleId) {
		this.roleId = roleId;
	}

	public ModuleEnum getModule() {
		return module;
	}

	public void setModule(ModuleEnum module) {
		this.module = module;
	}

	public boolean getIsCreate() {
		return isCreate;
	}

	public void setIsCreate(boolean create) {
		isCreate = create;
	}

	public boolean getIsRead() {
		return isRead;
	}

	public void setIsRead(boolean read) {
		isRead = read;
	}

	public boolean getIsUpdate() {
		return isUpdate;
	}

	public void setIsUpdate(boolean update) {
		isUpdate = update;
	}

	public boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(boolean delete) {
		isDelete = delete;
	}
}
