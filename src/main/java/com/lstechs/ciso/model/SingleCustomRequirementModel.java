package com.lstechs.ciso.model;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class SingleCustomRequirementModel {
	private int cstRqrId;
	private String cstRqrDescription;
	private List<SingleCrossReferenceModel> crossReferenceModels;

	public SingleCustomRequirementModel(int cstRqrId, String cstRqrDescription, List<SingleCrossReferenceModel> crossReferenceModels) {
		this.cstRqrId = cstRqrId;
		this.cstRqrDescription = cstRqrDescription;
		this.crossReferenceModels = crossReferenceModels;
	}

	public SingleCustomRequirementModel( CustomRequirementModel customRequirementModel, List<SingleCrossReferenceModel> singleCrossReferenceModel) {
		this.cstRqrId = customRequirementModel.getCstRqrId();
		this.cstRqrDescription = customRequirementModel.getCstRqrDescription();
		this.crossReferenceModels = singleCrossReferenceModel;
	}

	public int getCstRqrId() {
		return cstRqrId;
	}

	public void setCstRqrId(int cstRqrId) {
		this.cstRqrId = cstRqrId;
	}

	public String getCstRqrDescription() {
		return cstRqrDescription;
	}

	public void setCstRqrDescription(String cstRqrDescription) {
		this.cstRqrDescription = cstRqrDescription;
	}

	@JsonProperty("crossReferenceModels")
	public List<SingleCrossReferenceModel> getSingleCrossReferenceModels() {
		return crossReferenceModels;
	}

	public void setSingleCrossReferenceModels(List<SingleCrossReferenceModel> crossReferenceModel) {
		this.crossReferenceModels = crossReferenceModel;
	}

}
