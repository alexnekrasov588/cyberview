package com.lstechs.ciso.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TasksReportModel {

	private int[] prjIds;
	private boolean tskAssignee;
	private boolean tskDescrption;
	private boolean tskEndDate;
	private boolean tskName;
	private boolean tskPrj;
	private boolean tskStartDate;
	private boolean tskStatus;
	private boolean schedule;
	private Date startDate;
	private Date endDate;
	
	public String[] getTasksReportTitle() {
		List<String> titles = new ArrayList<String>();
		if(this.tskName) titles.add("Task Name");
		if(this.tskDescrption) titles.add("Descrption");
		if(this.tskStartDate) titles.add("Start Date");
		if(this.tskEndDate) titles.add("End Date");
		if(this.tskStatus) titles.add("Status");
		if(this.tskAssignee) titles.add("Assignee");
		if(this.schedule) titles.add("Schedule");
		titles.add("Project Name");
        String[] itemsArray = new String[titles.size()];
        itemsArray = titles.toArray(itemsArray);
		return itemsArray;		
	}
	
	public int[] getPrjIds() {
		return prjIds;
	}
	public void setPrjIds(int[] prjIds) {
		this.prjIds = prjIds;
	}
	public boolean isTskAssignee() {
		return tskAssignee;
	}
	public void setTskAssignee(boolean tskAssignee) {
		this.tskAssignee = tskAssignee;
	}
	public boolean isTskDescrption() {
		return tskDescrption;
	}
	public void setTskDescrption(boolean tskDescrption) {
		this.tskDescrption = tskDescrption;
	}
	public boolean isTskEndDate() {
		return tskEndDate;
	}
	public void setTskEndDate(boolean tskEndDate) {
		this.tskEndDate = tskEndDate;
	}
	public boolean isTskName() {
		return tskName;
	}
	public void setTskName(boolean tskName) {
		this.tskName = tskName;
	}
	public boolean isTskPrj() {
		return tskPrj;
	}
	public void setTskPrj(boolean tskPrj) {
		this.tskPrj = tskPrj;
	}
	public boolean isTskStartDate() {
		return tskStartDate;
	}
	public void setTskStartDate(boolean tskStartDate) {
		this.tskStartDate = tskStartDate;
	}
	public boolean isTskStatus() {
		return tskStatus;
	}
	public void setTskStatus(boolean tskStatus) {
		this.tskStatus = tskStatus;
	}


	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public boolean isSchedule() {
		return schedule;
	}

	public void setSchedule(boolean schedule) {
		this.schedule = schedule;
	}
	
	
}
