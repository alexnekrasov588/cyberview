package com.lstechs.ciso.model;

public class SingleAssetModel {
	private String astName;
	private String astDescrption;
	
	private String astLocation;
	private String astOperator ;
	private String astOwner;
	private int astAmount;
	private AssetTypeModel astType;
	
	
	
	
	public SingleAssetModel() {
		super();
	}
	public SingleAssetModel(String astName, String astDescrption, String astLocation, String astOperator,
			String astOwner, int astAmount, AssetTypeModel astType) {
		super();
		this.astName = astName;
		this.astDescrption = astDescrption;
		this.astLocation = astLocation;
		this.astOperator = astOperator;
		this.astOwner = astOwner;
		this.astAmount = astAmount;
		this.astType = astType;
	}
	public String getAstName() {
		return astName;
	}
	public void setAstName(String astName) {
		this.astName = astName;
	}
	public String getAstDescrption() {
		return astDescrption;
	}
	public void setAstDescrption(String astDescrption) {
		this.astDescrption = astDescrption;
	}
	public String getAstLocation() {
		return astLocation;
	}
	public void setAstLocation(String astLocation) {
		this.astLocation = astLocation;
	}
	public String getAstOperator() {
		return astOperator;
	}
	public void setAstOperator(String astOperator) {
		this.astOperator = astOperator;
	}
	public String getAstOwner() {
		return astOwner;
	}
	public void setAstOwner(String astOwner) {
		this.astOwner = astOwner;
	}
	public int getAstAmount() {
		return astAmount;
	}
	public void setAstAmount(int astAmount) {
		this.astAmount = astAmount;
	}
	public AssetTypeModel getAstType() {
		return astType;
	}
	public void setAstType(AssetTypeModel astType) {
		this.astType = astType;
	}
	
	
	
}


