package com.lstechs.ciso.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Entity
@Table(name = "NewsTags")
@EntityListeners(AuditingEntityListener.class)
public class NewsTagsModel extends CRUDModel {

    @Id
    @Column(name = "newsTagId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int newsTagId;

    @NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
    @Column(name = "newsTagName", nullable = false)
    private String newsTagName;

    @JsonIgnore
    @OneToMany(mappedBy = "newsTagId")
    private List<NewsArticlesTagsModel> newsArticlesTagsModel;


    public NewsTagsModel(String newsTagName) {
        super();
        this.newsTagName = newsTagName;
    }

    public NewsTagsModel() {
        super();
    }

    public int getNewsTagId() {
        return newsTagId;
    }

    public void setNewsTagId(int newsTagId) {
        this.newsTagId = newsTagId;
    }

    public String getNewsTagName() {
        return newsTagName;
    }

    public void setNewsTagName(String newsTagName) {
        this.newsTagName = newsTagName;
    }

    public List<NewsArticlesTagsModel> getNewsArticlesTagsModel() {
        return newsArticlesTagsModel;
    }

    public void setNewsArticlesTagsModel(List<NewsArticlesTagsModel> newsArticlesTagsModel) {
        this.newsArticlesTagsModel = newsArticlesTagsModel;
    }
}

