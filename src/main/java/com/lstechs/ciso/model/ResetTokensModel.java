package com.lstechs.ciso.model;

import org.hibernate.annotations.Type;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;


@Entity
@Table(name = "ResetTokens")
@EntityListeners(AuditingEntityListener.class)
public class ResetTokensModel {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(groups = { CRUDModel.CRUDCreation.class, CRUDModel.CRUDUpdate.class })
    @Column(name = "userId", nullable = false)
    private int userId;

    @NotEmpty(groups = { CRUDModel.CRUDCreation.class, CRUDModel.CRUDUpdate.class })
    @Column(name = "userType", nullable = false)
    private String userType;

    @NotEmpty(groups = { CRUDModel.CRUDCreation.class, CRUDModel.CRUDUpdate.class })
    @Column(name = "resetToken", nullable = false)
	@Type(type = "text")
	private String resetToken;

    @NotNull(groups = { CRUDModel.CRUDCreation.class, CRUDModel.CRUDUpdate.class })
    @Column(name = "expiresAt", nullable = false)
    private Date expiresAt;

    @NotNull(groups = { CRUDModel.CRUDCreation.class, CRUDModel.CRUDUpdate.class })
    @Column(name = "createdAt", nullable = false)
    private Date createdAt;

    public ResetTokensModel(int userId, String userType, String resetToken, Date expiresAt, Date createdAt) {
        this.userId = userId;
        this.userType = userType;
        this.resetToken = resetToken;
        this.expiresAt = expiresAt;
        this.createdAt = createdAt;
    }

    public ResetTokensModel() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getResetToken() {
        return resetToken;
    }

    public void setResetToken(String resetToken) {
        this.resetToken = resetToken;
    }

    public Date getExpiresAt() {
        return expiresAt;
    }

    public void setExpiresAt(Date expiresAt) {
        this.expiresAt = expiresAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
