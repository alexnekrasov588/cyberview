package com.lstechs.ciso.model;

import java.util.Date;

public class SingleStandardCompanyStatusModel {
	private int cmpId;
	private int stdId;
	private Date createdAt;

	public SingleStandardCompanyStatusModel(int cmpId, int stdId, Date createdAt) {
		this.cmpId = cmpId;
		this.stdId = stdId;
		this.createdAt = createdAt;
	}

	public SingleStandardCompanyStatusModel(StandardCompanyStatusModel standardCompanyStatusModel) {
		this.cmpId = standardCompanyStatusModel.getCmpId().getCmpId();
		this.stdId = standardCompanyStatusModel.getStdId().getStdId();
		this.createdAt = standardCompanyStatusModel.getCreatedAt();
	}

	public int getCmpId() {
		return cmpId;
	}

	public void setCmpId(int cmpId) {
		this.cmpId = cmpId;
	}

	public int getStdId() {
		return stdId;
	}

	public void setStdId(int stdId) {
		this.stdId = stdId;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
}
