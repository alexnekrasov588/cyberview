package com.lstechs.ciso.model;

public class SingleRoleModel {
	private int roleId;
	private String roleName;
	private boolean isEditable;
	private SingleCompanyModel cmpId;

	public SingleRoleModel(int roleId, String roleName, boolean isEditable, SingleCompanyModel cmpId) {
		this.roleId = roleId;
		this.roleName = roleName;
		this.isEditable = isEditable;
		this.cmpId = cmpId;
	}

	public SingleRoleModel(RoleModel roleModel, SingleCompanyModel cmpId) {
		this.roleId = roleModel.getRoleId();
		this.roleName = roleModel.getRoleName();
		this.isEditable = roleModel.isEditable();
		this.cmpId = cmpId;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public boolean isEditable() {
		return isEditable;
	}

	public void setEditable(boolean editable) {
		isEditable = editable;
	}

	public SingleCompanyModel getCmpId() {
		return cmpId;
	}

	public void setCmpId(SingleCompanyModel cmpId) {
		this.cmpId = cmpId;
	}
}
