package com.lstechs.ciso.model;

public class StdReportRequestModel {
	private int[] stdIds;
    private int[] stdQtrId;
	public int[] getStdIds() {
		return stdIds;
	}
	public void setStdIds(int[] stdIds) {
		this.stdIds = stdIds;
	}
	public int[] getStdQtrId() {
		return stdQtrId;
	}
	public void setStdQtrId(int[] stdQtrId) {
		this.stdQtrId = stdQtrId;
	}
	public StdReportRequestModel(int[] stdIds, int[] stdQtrId) {
		super();
		this.stdIds = stdIds;
		this.stdQtrId = stdQtrId;
	}
    
    
}
