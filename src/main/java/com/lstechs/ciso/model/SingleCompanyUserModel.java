package com.lstechs.ciso.model;

public class SingleCompanyUserModel {
	private int cmpUsrId;
	private int cmpUsrCmpId;
	private String cmpUsrFullName;
	private String cmpUsrEmail;
	private RoleModel cmpUsrRoleId;

	public SingleCompanyUserModel(int cmpUsrId, int cmpUsrCmpId, String cmpUsrFullName, String cmpUsrEmail, RoleModel cmpUsrRoleId) {
		this.cmpUsrId = cmpUsrId;
		this.cmpUsrCmpId = cmpUsrCmpId;
		this.cmpUsrFullName = cmpUsrFullName;
		this.cmpUsrEmail = cmpUsrEmail;
		this.cmpUsrRoleId = cmpUsrRoleId;
	}

	public SingleCompanyUserModel(CompanyUserModel companyUserModel) {
		this.cmpUsrId = companyUserModel.getCmpUsrId();
		this.cmpUsrCmpId = companyUserModel.getCmpUsrCmpId().getCmpId();
		this.cmpUsrFullName = companyUserModel.getCmpUsrFullName();
		this.cmpUsrEmail = companyUserModel.getCmpUsrEmail();
		this.cmpUsrRoleId = companyUserModel.getCmpUsrRoleId();
	}

	public int getCmpUsrId() {
		return cmpUsrId;
	}

	public void setCmpUsrId(int cmpUsrId) {
		this.cmpUsrId = cmpUsrId;
	}

	public int getCmpUsrCmpId() {
		return cmpUsrCmpId;
	}

	public void setCmpUsrCmpId(int cmpUsrCmpId) {
		this.cmpUsrCmpId = cmpUsrCmpId;
	}

	public String getCmpUsrFullName() {
		return cmpUsrFullName;
	}

	public void setCmpUsrFullName(String cmpUsrFullName) {
		this.cmpUsrFullName = cmpUsrFullName;
	}

	public String getCmpUsrEmail() {
		return cmpUsrEmail;
	}

	public void setCmpUsrEmail(String cmpUsrEmail) {
		this.cmpUsrEmail = cmpUsrEmail;
	}

	public RoleModel getCmpUsrRoleId() {
		return cmpUsrRoleId;
	}

	public void setCmpUsrRoleId(RoleModel cmpUsrRoleId) {
		this.cmpUsrRoleId = cmpUsrRoleId;
	}
}
