package com.lstechs.ciso.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "vendorUser")
@EntityListeners(AuditingEntityListener.class)
public class VendorUserModel extends CRUDModel {

	/**
	 *
	 */
	private static final long serialVersionUID = 5003452142582486166L;

	@Id
	@Column(name = "vndrId", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int vndrId;

	@Column(name = "vndrName", nullable = true)
	private String vndrName;

	@Email(groups = {CRUDCreation.class, CRUDUpdate.class})
	@NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "vndrEmail", nullable = false)
	private String vndrEmail;

	@NotEmpty(groups = {CRUDCreation.class})
	@JsonIgnore
	@Column(name = "vndrPassword", nullable = false)
	private String vndrPassword;


	public VendorUserModel(String vndrName, String vndrPassword, String vndrEmail) {
		super();
		this.vndrName = vndrName;
		this.vndrPassword = vndrPassword;
		this.vndrEmail = vndrEmail.toLowerCase();
	}

	public VendorUserModel() {
		super();
	}

	public VendorUserModel(VendorUserModel vendorUserModel) {
		this.vndrName = vendorUserModel.getVndrName();
		this.vndrPassword = vendorUserModel.getVndrPassword();
		this.vndrEmail = vendorUserModel.getVndrEmail();
	}

	public int getVndrId() {
		return vndrId;
	}

	public void setVndrId(int vndrId) {
		this.vndrId = vndrId;
	}

	public String getVndrName() {
		return vndrName;
	}

	public void setVndrName(String vndrName) {
		this.vndrName = vndrName;
	}

	@JsonIgnore
	public String getVndrPassword() {
		return vndrPassword;
	}

	@JsonProperty
	public void setVndrPassword(String vndrPassword) {
		this.vndrPassword = vndrPassword;
	}

	public String getVndrEmail() {
		return vndrEmail.toLowerCase();
	}

	public void setVndrEmail(String vndrEmail) {
		this.vndrEmail = vndrEmail.toLowerCase();
	}
}
