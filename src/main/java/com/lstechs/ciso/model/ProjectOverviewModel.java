package com.lstechs.ciso.model;

public class ProjectOverviewModel {
	private ProjectModel project;
	private int totalTask;
	private int completedTask;
	private double progress;

	public ProjectOverviewModel(ProjectModel project, int totalTask, int completedTask, double progress) {
		this.project = project;
		this.totalTask = totalTask;
		this.completedTask = completedTask;
		this.progress = progress;
	}

	public ProjectOverviewModel() {
	}

	public ProjectModel getProject() {
		return project;
	}

	public void setProject(ProjectModel project) {
		this.project = project;
	}

	public int getTotalTask() {
		return totalTask;
	}

	public void setTotalTask(int totalTask) {
		this.totalTask = totalTask;
	}

	public int getCompletedTask() {
		return completedTask;
	}

	public void setCompletedTask(int completedTask) {
		this.completedTask = completedTask;
	}

	public double getProgress() {
		return progress;
	}

	public void setProgress(double progress) {
		this.progress = progress;
	}
}
