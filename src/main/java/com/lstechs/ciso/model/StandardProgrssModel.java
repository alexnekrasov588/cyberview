package com.lstechs.ciso.model;

public class StandardProgrssModel {
	private int notImplemented;
	private int notApplicable;
	private int applicable;
	
	
	
	public StandardProgrssModel() {
		super();
	}

	public StandardProgrssModel(int notImplemented, int notApplicable, int applicable) {
		super();
		this.notImplemented = notImplemented;
		this.notApplicable = notApplicable;
		this.applicable = applicable;
	}
	
	public int getNotImplemented() {
		return notImplemented;
	}
	public void setNotImplemented(int notImplemented) {
		this.notImplemented = notImplemented;
	}
	public int getNotApplicable() {
		return notApplicable;
	}
	public void setNotApplicable(int notApplicable) {
		this.notApplicable = notApplicable;
	}
	public int getApplicable() {
		return applicable;
	}
	public void setApplicable(int applicable) {
		this.applicable = applicable;
	}
	
	

}
