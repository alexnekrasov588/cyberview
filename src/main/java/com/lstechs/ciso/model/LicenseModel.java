package com.lstechs.ciso.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.ScriptAssert;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "License")
@EntityListeners(AuditingEntityListener.class)
@ScriptAssert(lang = "javascript", script = "_this.lcnsStartDate.before(_this.lcnsEndDate)", message = "End date must be greater than start date")
public class LicenseModel extends CRUDModel {

	@Id
	@Column(name = "lcnsId", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int lcnsId;

	@NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "lcnsToken", nullable = false)
	@Type(type = "text")
	private String lcnsToken;

	@NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "lcnsFeaturesLst", nullable = false)
	private String lcnsFeaturesLst;

	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "lcnsAllowedUsersCount", nullable = false)
	private int lcnsAllowedUsersCount;

	@Column(name = "lcnsDescription", nullable = true)
	private String lcnsDescription;

	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "lcnsStartDate", nullable = false)
	private Date lcnsStartDate;

	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "lcnsEndDate", nullable = false)
	private Date lcnsEndDate;

	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "lcnsIsActive", nullable = false)
	private boolean lcnsIsActive;

	@Column(name = "lcnsActivationIp", nullable = true)
	private String lcnsActivationIP;

	@Column(name = "lcnsActivationDate", nullable = true)
	private Date lcnsActivationDate;

	@JsonIgnore
	@OneToMany(mappedBy = "cmpLcnsId")
	private List<CompanyModel> companyModel;

	@OneToMany(mappedBy = "lcnsId")
	private List<LicenseStandardModel> licenseStandardModels;

	@Transient
	private List<Integer> lcnsStandardList;

	@Transient
	private String lcnsCmpName;


	public LicenseModel(String lcnsToken, String lcnsFeaturesLst, int lcnsAllowedUsersCount, String lcnsDescription, Date lcnsStartDate,
						Date lcnsEndDate, boolean lcnsIsActive, String lcnsActivationIP, Date lcnsActivationDate) throws Exception {
		super();
		this.lcnsToken = lcnsToken;
		this.lcnsFeaturesLst = lcnsFeaturesLst;
		this.lcnsAllowedUsersCount = lcnsAllowedUsersCount;
		this.lcnsDescription = lcnsDescription;

		if (lcnsStartDate.after(lcnsEndDate)) {
			throw new Exception("The Start Date have to be before the End Date!!");

		} else {
			this.lcnsStartDate = lcnsStartDate;
			this.lcnsEndDate = lcnsEndDate;
		}
		this.lcnsIsActive = lcnsIsActive;
		this.lcnsActivationIP = lcnsActivationIP;
		this.lcnsActivationDate = lcnsActivationDate;
	}

	public LicenseModel() {
		super();
	}

	public int getLcnsId() {
		return lcnsId;
	}

	public void setLcnsId(int lcnsId) {
		this.lcnsId = lcnsId;
	}

	public String getLcnsToken() {
		return lcnsToken;
	}

	public void setLcnsToken(String lcnsToken) {
		this.lcnsToken = lcnsToken;
	}

	public String getLcnsFeaturesLst() {
		return lcnsFeaturesLst;
	}

	public void setLcnsFeaturesLst(String lcnsFeaturesLst) {
		this.lcnsFeaturesLst = lcnsFeaturesLst;
	}

	public int getLcnsAllowedUsersCount() {
		return lcnsAllowedUsersCount;
	}

	public void setLcnsAllowedUsersCount(int lcnsAllowedUsersCount) {
		this.lcnsAllowedUsersCount = lcnsAllowedUsersCount;
	}

	public String getLcnsDescription() {
		return lcnsDescription;
	}

	public void setLcnsDescription(String lcnsDescription) {
		this.lcnsDescription = lcnsDescription;
	}

	public Date getLcnsStartDate() {
		return lcnsStartDate;
	}

	public void setLcnsStartDate(Date lcnsStartDate) {
		this.lcnsStartDate = lcnsStartDate;
	}

	public Date getLcnsEndDate() {
		return lcnsEndDate;
	}

	public void setLcnsEndDate(Date lcnsEndDate) {
		this.lcnsEndDate = lcnsEndDate;
	}

	public boolean isLcnsIsActive() {
		return lcnsIsActive;
	}

	public void setLcnsIsActive(boolean lcnsIsActive) {
		this.lcnsIsActive = lcnsIsActive;
	}

	public String getLcnsActivationIP() {
		return lcnsActivationIP;
	}

	public void setLcnsActivationIP(String lcnsActivationIP) {
		this.lcnsActivationIP = lcnsActivationIP;
	}

	public Date getLcnsActivationDate() {
		return lcnsActivationDate;
	}

	public void setLcnsActivationDate(Date lcnsActivationDate) {
		this.lcnsActivationDate = lcnsActivationDate;
	}

	public List<CompanyModel> getCompanyModel() {
		return companyModel;
	}

	public void setCompanyModel(List<CompanyModel> companyModel) {
		this.companyModel = companyModel;
	}

	public List<LicenseStandardModel> getLicenseStandardModels() {
		if (licenseStandardModels != null && licenseStandardModels.size() > 0) {
			List<LicenseStandardModel> retLicenseStandardModels = new ArrayList<>();
			for (LicenseStandardModel tmpLicenseStandardModel : licenseStandardModels) {
				if (tmpLicenseStandardModel.getDeletedAt() == null)
					retLicenseStandardModels.add(tmpLicenseStandardModel);
			}
			return retLicenseStandardModels;
		} else return licenseStandardModels;
	}

	public void setLicenseStandardModels(List<LicenseStandardModel> licenseStandardModels) {
		this.licenseStandardModels = licenseStandardModels;
	}

	public List<Integer> getLcnsStandardList() {
		return lcnsStandardList;
	}

	public void setLcnsStandardList(List<Integer> lcnsStandardList) {
		this.lcnsStandardList = lcnsStandardList;
	}

	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	public String getLcnsCmpName() {
		for(CompanyModel cmp : this.companyModel)
			if (cmp.getDeletedAt() == null)
				setLcnsCmpName(cmp.getCmpName());

		return lcnsCmpName;
	}

	public void setLcnsCmpName(String lcnsCmpName) {
		this.lcnsCmpName = lcnsCmpName;
	}
}
