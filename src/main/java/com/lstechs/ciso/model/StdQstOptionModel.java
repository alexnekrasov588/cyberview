package com.lstechs.ciso.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Type;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "StdQstOption")
@EntityListeners(AuditingEntityListener.class)
public class StdQstOptionModel extends CRUDModel {
	@Id
	@Column(name = "sqoOptId", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int sqoOptId;

	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sqoQstId", nullable = false)
	private StdQtrQuestionsModel sqoQstId;

	@NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "sqoValue", nullable = false)
	@Type(type = "text")
	private String sqoValue;

	@NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "sqoLabel", nullable = false)
	@Type(type = "text")
	private String sqoLabel;

	@Column(name = "stdRqrOnPremId", columnDefinition = "Integer default '0'")
	private int stdOptOnPremId;

	@Transient
	public int qst;

	public StdQstOptionModel(StdQtrQuestionsModel sqoQstId, String sqoValue, String sqoLabel) {
		this.sqoQstId = sqoQstId;
		this.sqoValue = sqoValue;
		this.sqoLabel = sqoLabel;
	}

	public StdQstOptionModel(String sqoValue, String sqoLabel) {
		this.sqoValue = sqoValue;
		this.sqoLabel = sqoLabel;
	}

	public StdQstOptionModel() {
	}

	public int getSqoOptId() {
		return sqoOptId;
	}

	public void setSqoOptId(int sqoOptId) {
		this.sqoOptId = sqoOptId;
	}

	@JsonIgnore
	public StdQtrQuestionsModel getSqoQstId() {
		return sqoQstId;
	}

	@JsonProperty
	public void setSqoQstId(StdQtrQuestionsModel sqoQstId) {
		this.sqoQstId = sqoQstId;
	}

	public String getSqoValue() {
		return sqoValue;
	}

	public void setSqoValue(String sqoValue) {
		this.sqoValue = sqoValue;
	}

	public String getSqoLabel() {
		return sqoLabel;
	}

	public void setSqoLabel(String sqoLabel) {
		this.sqoLabel = sqoLabel;
	}

	public int getQst() {
//		if(this.sqoQstId != null)
//		return this.sqoQstId.getQtrQstId();
//		else return 0;
        return this.qst;
	}

	public int getStdOptOnPremId() {
		return stdOptOnPremId;
	}

	public void setStdOptOnPremId(int stdOptOnPremId) {
		this.stdOptOnPremId = stdOptOnPremId;
	}


}
