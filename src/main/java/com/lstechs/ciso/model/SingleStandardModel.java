package com.lstechs.ciso.model;

public class SingleStandardModel {
	private int stdId;
	private String stdName;
	private String stdVersion;
	private String stdDescription;
	private boolean stdIsActive;
	private boolean stdIsCompleted;
	private String stdLvlName;

	public SingleStandardModel(int stdId, String stdName, String stdVersion, String stdDescription, boolean stdIsActive, boolean stdIsCompleted, String stdLvlName) {
		this.stdId = stdId;
		this.stdName = stdName;
		this.stdVersion = stdVersion;
		this.stdDescription = stdDescription;
		this.stdIsActive = stdIsActive;
		this.stdIsCompleted = stdIsCompleted;
		this.stdLvlName = stdLvlName;
	}

	public SingleStandardModel(StandardModel standardModel) {
		this.stdId = standardModel.getStdId();
		this.stdName = standardModel.getStdName();
		this.stdVersion = standardModel.getStdVersion();
		this.stdDescription = standardModel.getStdDescription();
		this.stdIsActive = standardModel.isStdIsActive();
		this.stdIsCompleted = standardModel.isStdIsCompleted();
		this.stdLvlName = standardModel.getStdLvlName();
	}

	public int getStdId() {
		return stdId;
	}

	public void setStdId(int stdId) {
		this.stdId = stdId;
	}

	public String getStdName() {
		return stdName;
	}

	public void setStdName(String stdName) {
		this.stdName = stdName;
	}

	public String getStdVersion() {
		return stdVersion;
	}

	public void setStdVersion(String stdVersion) {
		this.stdVersion = stdVersion;
	}

	public String getStdDescription() {
		return stdDescription;
	}

	public void setStdDescription(String stdDescription) {
		this.stdDescription = stdDescription;
	}

	public boolean isStdIsActive() {
		return stdIsActive;
	}

	public void setStdIsActive(boolean stdIsActive) {
		this.stdIsActive = stdIsActive;
	}

	public boolean isStdIsCompleted() {
		return stdIsCompleted;
	}

	public void setStdIsCompleted(boolean stdIsCompleted) {
		this.stdIsCompleted = stdIsCompleted;
	}

	public String getStdLvlName() {
		return stdLvlName;
	}

	public void setStdLvlName(String stdLvlName) {
		this.stdLvlName = stdLvlName;
	}
}
