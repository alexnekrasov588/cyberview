package com.lstechs.ciso.model;

import com.lstechs.ciso.enums.NewsSrcTypeEnum;

public class SingleNewsSourcesModel {
	private int newsSrcId;
	private String newsSrcSiteName;
	private String newsSrcSiteUrl;
	private String newsSrcSiteBaseUrl;
	private NewsSrcTypeEnum newsSrcType;
	private boolean newsSrcIsActive;
	private boolean isRtl;
	private String newsNextPageBtnSelector;
	private String newsImgSelector;
	private String newsArticleUrlSelector;
	private String newsArticleContentContainerSelector;
	private String newsArticleTitleSelector;

	public SingleNewsSourcesModel(int newsSrcId, String newsSrcSiteName, String newsSrcSiteUrl, String newsSrcSiteBaseUrl, NewsSrcTypeEnum newsSrcType, boolean newsSrcIsActive,  boolean rtl,
		String newsNextPageBtnSelector, String newsImgSelector, String newsArticleUrlSelector, String newsArticleContentContainerSelector, String newsArticleTitleSelector) {
		this.newsSrcId = newsSrcId;
		this.newsSrcSiteName = newsSrcSiteName;
		this.newsSrcSiteUrl = newsSrcSiteUrl;
		this.newsSrcSiteBaseUrl = newsSrcSiteBaseUrl;
		this.newsSrcType = newsSrcType;
		this.newsSrcIsActive = newsSrcIsActive;
		this.isRtl = rtl;
		this.newsNextPageBtnSelector = newsNextPageBtnSelector;
		this.newsImgSelector = newsImgSelector;
		this.newsArticleUrlSelector = newsArticleUrlSelector;
		this.newsArticleContentContainerSelector = newsArticleContentContainerSelector;
		this.newsArticleTitleSelector = newsArticleTitleSelector;
	}

	public SingleNewsSourcesModel(NewsSourcesModel newsSourcesModel) {
		this.newsSrcId = newsSourcesModel.getNewsSrcId();
		this.newsSrcSiteName = newsSourcesModel.getNewsSrcSiteName();
		this.newsSrcSiteUrl = newsSourcesModel.getNewsSrcSiteUrl();
		this.newsSrcSiteBaseUrl = newsSourcesModel.getNewsSrcBaseUrl();
		this.newsSrcType = newsSourcesModel.getNewsSrcType();
		this.newsSrcIsActive = newsSourcesModel.isNewsSrcIsActive();
		this.isRtl = newsSourcesModel.getIsRtl();
		this.newsNextPageBtnSelector = newsSourcesModel.getNewsNextPageBtnSelector();
		this.newsImgSelector = newsSourcesModel.getNewsImgSelector();
		this.newsArticleUrlSelector = newsSourcesModel.getNewsArticleUrlSelector();
		this.newsArticleContentContainerSelector = newsSourcesModel.getNewsArticleContentContainerSelector();
		this.newsArticleTitleSelector = newsSourcesModel.getNewsArticleTitleSelector();
	}

	public int getNewsSrcId() {
		return newsSrcId;
	}

	public void setNewsSrcId(int newsSrcId) {
		this.newsSrcId = newsSrcId;
	}

	public String getNewsSrcSiteName() {
		return newsSrcSiteName;
	}

	public void setNewsSrcSiteName(String newsSrcSiteName) {
		this.newsSrcSiteName = newsSrcSiteName;
	}

	public String getNewsSrcSiteUrl() {
		return newsSrcSiteUrl;
	}

	public void setNewsSrcSiteUrl(String newsSrcSiteUrl) {
		this.newsSrcSiteUrl = newsSrcSiteUrl;
	}

	public NewsSrcTypeEnum getNewsSrcType() {
		return newsSrcType;
	}

	public void setNewsSrcType(NewsSrcTypeEnum newsSrcType) {
		this.newsSrcType = newsSrcType;
	}

	public boolean isNewsSrcIsActive() {
		return newsSrcIsActive;
	}

	public void setNewsSrcIsActive(boolean newsSrcIsActive) {
		this.newsSrcIsActive = newsSrcIsActive;
	}

	public String getNewsNextPageBtnSelector() {
		return newsNextPageBtnSelector;
	}

	public void setNewsNextPageBtnSelector(String newsNextPageBtnSelector) {
		this.newsNextPageBtnSelector = newsNextPageBtnSelector;
	}

	public String getNewsImgSelector() {
		return newsImgSelector;
	}

	public void setNewsImgSelector(String newsImgSelector) {
		this.newsImgSelector = newsImgSelector;
	}

	public String getNewsArticleUrlSelector() {
		return newsArticleUrlSelector;
	}

	public void setNewsArticleUrlSelector(String newsArticleUrlSelector) {
		this.newsArticleUrlSelector = newsArticleUrlSelector;
	}

	public String getNewsArticleContentContainerSelector() {
		return newsArticleContentContainerSelector;
	}

	public void setNewsArticleContentContainerSelector(String newsArticleContentContainerSelector) {
		this.newsArticleContentContainerSelector = newsArticleContentContainerSelector;
	}

	public String getNewsArticleTitleSelector() {
		return newsArticleTitleSelector;
	}

	public void setNewsArticleTitleSelector(String newsArticleTitleSelector) {
		this.newsArticleTitleSelector = newsArticleTitleSelector;
	}

	public String getNewsSrcSiteBaseUrl() {
		return newsSrcSiteBaseUrl;
	}

	public void setNewsSrcSiteBaseUrl(String newsSrcSiteBaseUrl) {
		this.newsSrcSiteBaseUrl = newsSrcSiteBaseUrl;
	}

	public boolean getIsRtl() {
		return isRtl;
	}

	public void setRtl(boolean rtl) {
		isRtl = rtl;
	}
}
