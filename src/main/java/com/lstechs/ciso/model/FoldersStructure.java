package com.lstechs.ciso.model;

import java.util.List;

public class FoldersStructure {
	private List<FoldersChildrens> childrens;
	
	public FoldersStructure() {
		super();
	}

	public FoldersStructure(List<FoldersChildrens> childrens) {
		super();
		this.childrens = childrens;
	}

	public List<FoldersChildrens> getChildrens() {
		return childrens;
	}

	public void setChildrens(List<FoldersChildrens> childrens) {
		this.childrens = childrens;
	}
	
	
}


// const structure = [
//     {
//       type: "folder",
//       name: "src",
//       childrens: [
//         {
//           type: "folder",
//           name: "Components",
//           childrens: [
//             { type: "file", name: "Modal.js" },
//             { type: "file", name: "Modal.css" }
//           ]
//         },
//         { type: "file", name: "index.js" },
//         { type: "file", name: "index.html" }
//       ]
//     },
//     { type: "file", name: "package.json" }
//   ];