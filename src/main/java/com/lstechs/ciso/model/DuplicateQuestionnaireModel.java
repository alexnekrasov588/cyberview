package com.lstechs.ciso.model;

public class DuplicateQuestionnaireModel {
	int stdId;
	int qtrId;
	String name;
	int parentId;

	public DuplicateQuestionnaireModel(int stdId, int qtrId, String name, int parentId) {
		this.stdId = stdId;
		this.qtrId = qtrId;
		this.name = name;
		this.parentId = parentId;
	}

	public int getStdId() {
		return stdId;
	}

	public void setStdId(int stdId) {
		this.stdId = stdId;
	}

	public int getQtrId() {
		return qtrId;
	}

	public void setQtrId(int qtrId) {
		this.qtrId = qtrId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getParentId() { return parentId; }

	public void setParentId(int parentId) { this.parentId = parentId; }
}
