package com.lstechs.ciso.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "UserProject")
@EntityListeners(AuditingEntityListener.class)
public class UserProjectModel extends CRUDModel {

	@Id
	@Column(name = "usrPrjId", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int usrPrjId;

	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "usrId", nullable = false)
	private CompanyUserModel usrId;

	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "prjId", nullable = false)
	private ProjectModel prjId;

	public UserProjectModel(CompanyUserModel usrId, ProjectModel prjId) {
		super();
		this.usrId = usrId;
		this.prjId = prjId;
	}

	public UserProjectModel() {
		super();
	}

	public int getUsrPrjId() {
		return usrPrjId;
	}

	public void setUsrPrjId(int usrPrjId) {
		this.usrPrjId = usrPrjId;
	}

	public CompanyUserModel getUsrId() {
		return usrId;
	}

	public void setUsrId(CompanyUserModel usrId) {
		this.usrId = usrId;
	}

	public ProjectModel getPrjId() {
		return prjId;
	}

	public void setPrjId(ProjectModel prjId) {
		this.prjId = prjId;
	}
}
