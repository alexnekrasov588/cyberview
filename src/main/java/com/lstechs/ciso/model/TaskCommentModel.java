package com.lstechs.ciso.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Type;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "TaskComment")
@EntityListeners(AuditingEntityListener.class)
public class TaskCommentModel extends CRUDModel {

	@Id
	@Column(name = "cmntId", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int cmntId;

	@NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "cmntText", nullable = false)
	@Type(type = "text")
	private String cmntText;

	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cmntTskId", nullable = false)
	private TaskModel cmntTskId;


	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "usrId", nullable = false)
	private CompanyUserModel usrId;

	public TaskCommentModel(String cmntText, TaskModel cmntTskId, CompanyUserModel usrId) {
		super();
		this.cmntText = cmntText;
		this.cmntTskId = cmntTskId;
		this.usrId = usrId;
	}

	public TaskCommentModel() {
		super();
	}

	public int getCmntId() {
		return cmntId;
	}

	public void setCmntId(int cmntId) {
		this.cmntId = cmntId;
	}

	public String getCmntText() {
		return cmntText;
	}

	public void setCmntText(String cmntText) {
		this.cmntText = cmntText;
	}

	public TaskModel getCmntTskId() {
		return cmntTskId;
	}

	public void setCmntTskId(TaskModel cmntTskId) {
		this.cmntTskId = cmntTskId;
	}

	public CompanyUserModel getUsrId() {
		return usrId;
	}

	public void setUsrId(CompanyUserModel usrId) {
		this.usrId = usrId;
	}

}

