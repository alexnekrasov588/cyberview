package com.lstechs.ciso.model;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "CustomRequirement")
@EntityListeners(AuditingEntityListener.class)
public class CustomRequirementModel extends CRUDModel {

	@Id
    @Column(name = "cstRqrId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int cstRqrId;

    @NotEmpty(groups = { CRUDCreation.class, CRUDUpdate.class })
    @Column(name = "cstRqrDescription", nullable = false)
    private String cstRqrDescription;

    @OneToMany(mappedBy = "cstRqrId")
    private List<CrossReferenceModel> crossReferenceModels;

    public CustomRequirementModel(String cstRqrDescription) {
        super();
        this.cstRqrDescription = cstRqrDescription;
    }

	public CustomRequirementModel() {
        super();
    }

    public int getCstRqrId() {
        return cstRqrId;
    }

    public void setCstRqrId(int cstRqrId) {
        this.cstRqrId = cstRqrId;
    }

    public String getCstRqrDescription() {
        return cstRqrDescription;
    }

    public void setCstRqrDescription(String cstRqrDescription) {
        this.cstRqrDescription = cstRqrDescription;
    }

    public List<CrossReferenceModel> getCrossReferenceModels() {
		List<CrossReferenceModel> retCrossReferenceModels = new ArrayList<>();
    	if(crossReferenceModels != null && crossReferenceModels.size() > 0){
    		for(CrossReferenceModel tmpCrossReferenceModel:crossReferenceModels){
    		if(tmpCrossReferenceModel.getDeletedAt() == null)
    			retCrossReferenceModels.add(tmpCrossReferenceModel);
			}
		}
        return retCrossReferenceModels;
    }

    public void setCrossReferenceModels(List<CrossReferenceModel> crossReferenceModel) {
        this.crossReferenceModels = crossReferenceModel;
    }
}
