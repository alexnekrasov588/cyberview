package com.lstechs.ciso.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "RecurringTask")
@EntityListeners(AuditingEntityListener.class)
public class RecurringTaskModel extends CRUDModel {
	

	@Id
	@Column(name = "recId", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int recId;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "tskId", nullable = false)
	private TaskModel tskId;
	
	@Column(name = "endDate")
	private Date endDate;
	
	@Column(name = "numOfShow")
	private int numOfShow;
	
	@Column(name = "numOfShowUpdated")
	private int numOfShowUpdated;
	
	@Column(name = "isWeekly")
	private boolean isWeekly;
	
	@Column(name = "isMonthly")
	private boolean isMonthly;
	
	@Column(name = "isYearly")
	private boolean isYearly;

	public int getRecId() {
		return recId;
	}

	public void setRecId(int recId) {
		this.recId = recId;
	}

	public TaskModel getTskId() {
		return tskId;
	}

	public void setTskId(TaskModel tskId) {
		this.tskId = tskId;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public int getNumOfShow() {
		return numOfShow;
	}

	public void setNumOfShow(int numOfShow) {
		this.numOfShow = numOfShow;
	}

	public boolean isWeekly() {
		return isWeekly;
	}

	public void setWeekly(boolean isWeekly) {
		this.isWeekly = isWeekly;
	}

	public boolean isMonthly() {
		return isMonthly;
	}

	public void setMonthly(boolean isMonthly) {
		this.isMonthly = isMonthly;
	}

	public boolean isYearly() {
		return isYearly;
	}

	public void setYearly(boolean isYearly) {
		this.isYearly = isYearly;
	}

	public int getNumOfShowUpdated() {
		return numOfShowUpdated;
	}

	public void setNumOfShowUpdated(int numOfShowUpdated) {
		this.numOfShowUpdated = numOfShowUpdated;
	}
	
	
	
	
}
