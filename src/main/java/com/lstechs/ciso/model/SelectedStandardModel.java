package com.lstechs.ciso.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "SelectedStandards")
@EntityListeners(AuditingEntityListener.class)
public class SelectedStandardModel extends CRUDModel {

	@Id
	@Column(name = "selectedStandardsId", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int selectedStandardsId;

	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@NotNull(groups = {CRUDModel.CRUDCreation.class})
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "roleId", nullable = false)
	private RoleModel roleId;

	@NotEmpty(groups = {CRUDModel.CRUDCreation.class, CRUDModel.CRUDUpdate.class})
	@Column(name = "standardId", nullable = false)
	private int standardId;

	public SelectedStandardModel() {
	}

	public SelectedStandardModel(int standardId, RoleModel roleId) {
		this.standardId = standardId;
		this.roleId = roleId;
	}

	public int getSelectedStandardsId() {
		return selectedStandardsId;
	}

	public void setSelectedStandardsId(int selectedStandardsId) {
		this.selectedStandardsId = selectedStandardsId;
	}

	public int getStandardId() {
		return standardId;
	}

	public void setStandardId(int standardId) {
		this.standardId = standardId;
	}

	public RoleModel getRoleId() {
		return roleId;
	}

	public void setRoleId(RoleModel roleId) {
		this.roleId = roleId;
	}
}
