package com.lstechs.ciso.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.lstechs.ciso.enums.AssetTypeEnum;
import com.lstechs.ciso.model.CRUDModel.CRUDCreation;
import com.lstechs.ciso.model.CRUDModel.CRUDUpdate;

@Entity
@Table(name = "AssetType")
@EntityListeners(AuditingEntityListener.class)
public class AssetTypeModel extends CRUDModel {
	@Id
	@Column(name = "astTypeId", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int astTypeId;
	
	@NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "astTypeName", nullable = false)
	private String astTypeName;
	
	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "astType", nullable = false)
	private AssetTypeEnum astType;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cmpId", nullable = false)
	private CompanyModel cmpId;
	
	@JsonIgnore
	@OneToMany(mappedBy = "astType")
	private List<AssetModel> assetModels;

	public AssetTypeModel(String astTypeName,AssetTypeEnum astType,CompanyModel cmpId) {
		super();
		this.astTypeName = astTypeName;
		this.astType = astType;
		this.cmpId = cmpId;
	}
	

	public AssetTypeModel() {
		super();
	}


	public int getAstTypeId() {
		return astTypeId;
	}

	public void setAstTypeId(int astTypeId) {
		this.astTypeId = astTypeId;
	}

	public String getAstTypeName() {
		return astTypeName;
	}

	public void setAstTypeName(String astName) {
		this.astTypeName = astName;
	}

	@JsonIgnore
	public CompanyModel getCmpId() {
		return cmpId;
	}

	@JsonProperty
	public void setCmpId(CompanyModel cmpId) {
		this.cmpId = cmpId;
	}


	public AssetTypeEnum getAstType() {
		return astType;
	}


	public void setAstType(AssetTypeEnum astType) {
		this.astType = astType;
	}
	
	


}
