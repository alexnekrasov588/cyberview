package com.lstechs.ciso.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.lstechs.ciso.enums.NewsSrcTypeEnum;
import org.hibernate.annotations.Type;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "NewsSources")
@EntityListeners(AuditingEntityListener.class)
public class NewsSourcesModel extends CRUDModel {

	@Id
    @Column(name = "newsSrcId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int newsSrcId;
	
	@Column(name = "newsSrcOnPremId",  nullable = true ,columnDefinition = "Integer default '0'")
	private int newsSrcOnPremId ;

    @NotEmpty(groups = { CRUDCreation.class, CRUDUpdate.class })
    @Column(name = "newsSrcSiteName", nullable = false)
    private String newsSrcSiteName;

    @NotEmpty(groups = { CRUDCreation.class, CRUDUpdate.class })
    @Column(name = "newsSrcSiteUrl", nullable = false)
    private String newsSrcSiteUrl;

	@NotEmpty(groups = { CRUDCreation.class, CRUDUpdate.class })
    @Column(name = "newsSrcBaseUrl", nullable = true)
    private String newsSrcBaseUrl;

    @NotNull(groups = { CRUDCreation.class, CRUDUpdate.class })
    @Enumerated(EnumType.STRING)
    @Column(name = "newsSrcType", nullable = false)
    private NewsSrcTypeEnum newsSrcType;

    @NotNull(groups = { CRUDCreation.class, CRUDUpdate.class })
    @Column(name = "newsSrcIsActive", nullable = false)
    private boolean newsSrcIsActive;

	@NotNull(groups = { CRUDCreation.class, CRUDUpdate.class })
	@Column(name = "isRtl", nullable = false, columnDefinition = "boolean default false")
	private boolean isRtl;

	@Column(name = "newsNextPageBtnSelector", nullable = true)
    @Type(type="text")
    private String newsNextPageBtnSelector;

	@Column(name = "newsImgSelector", nullable = true)
	@Type(type = "text")
	private String newsImgSelector;

    @Column(name = "newsArticleUrlSelector", nullable = true)
    @Type(type="text")
    private String newsArticleUrlSelector;

    @Column(name = "newsArticleContentContainerSelector", nullable = true)
    @Type(type="text")
    private String newsArticleContentContainerSelector;

    @Column(name = "newsArticleTitleSelector", nullable = true)
    @Type(type="text")
    private String newsArticleTitleSelector;

	@Column(name = "newsDateSelector", nullable = true)
	@Type(type = "text")
	private String newsDateSelector;


    @JsonIgnore
    @OneToMany(mappedBy = "newsSrcId")
    private List<NewsArticlesModel> newsArticlesModel;

    public NewsSourcesModel(String newsSrcSiteName, String newsSrcSiteUrl, String newsSrcBaseUrl, String newsImgSelector, NewsSrcTypeEnum newsSrcType, boolean newsSrcIsActive, List<NewsArticlesModel> newsArticlesModel,String newsDateSelector) {
        super();
        this.newsSrcSiteName = newsSrcSiteName;
        this.newsSrcSiteUrl = newsSrcSiteUrl;
        this.newsSrcBaseUrl = newsSrcBaseUrl;
        this.newsImgSelector = newsImgSelector;
        this.newsSrcType = newsSrcType;
        this.newsSrcIsActive = newsSrcIsActive;
        this.newsArticlesModel = newsArticlesModel;
        this.newsDateSelector = newsDateSelector;
    }

    public NewsSourcesModel() {
        super();
    }

    public int getNewsSrcId() {
        return newsSrcId;
    }

    public void setNewsSrcId(int newsSrcId) {
        this.newsSrcId = newsSrcId;
    }

    public String getNewsSrcSiteName() {
        return newsSrcSiteName;
    }

    public void setNewsSrcSiteName(String newsSrcSiteName) {
        this.newsSrcSiteName = newsSrcSiteName;
    }

    public String getNewsSrcSiteUrl() {
        return newsSrcSiteUrl;
    }

    public void setNewsSrcSiteUrl(String newsSrcSiteUrl) {
        this.newsSrcSiteUrl = newsSrcSiteUrl;
    }

    public String getNewsImgSelector() { return  newsImgSelector; }

    public void setNewsImgSelector(String newsImgSelector) { this.newsImgSelector = newsImgSelector; }

    public NewsSrcTypeEnum getNewsSrcType() {
        return newsSrcType;
    }

    public void setNewsSrcType(NewsSrcTypeEnum newsSrcType) {
        this.newsSrcType = newsSrcType;
    }

    public boolean isNewsSrcIsActive() {
        return newsSrcIsActive;
    }

    public void setNewsSrcIsActive(boolean newsSrcIsActive) {
        this.newsSrcIsActive = newsSrcIsActive;
    }

	public boolean getIsRtl() { return isRtl;	}

	public void setRtl(boolean rtl) { this.isRtl = rtl;	}

	public List<NewsArticlesModel> getNewsArticlesModel() {
        return newsArticlesModel;
    }

    public void setNewsArticlesModel(List<NewsArticlesModel> newsArticlesModel) {
        this.newsArticlesModel = newsArticlesModel;
    }

    public String getNewsNextPageBtnSelector() {
        return newsNextPageBtnSelector;
    }

    public void setNewsNextPageBtnSelector(String newsNextPageBtnSelector) {
        this.newsNextPageBtnSelector = newsNextPageBtnSelector;
    }

    public String getNewsArticleUrlSelector() {
        return newsArticleUrlSelector;
    }

    public void setNewsArticleUrlSelector(String newsArticleUrlSelector) {
        this.newsArticleUrlSelector = newsArticleUrlSelector;
    }

    public String getNewsArticleContentContainerSelector() {
        return newsArticleContentContainerSelector;
    }

    public void setNewsArticleContentContainerSelector(String newsArticleContentContainerSelector) {
        this.newsArticleContentContainerSelector = newsArticleContentContainerSelector;
    }

    public String getNewsArticleTitleSelector() {
        return newsArticleTitleSelector;
    }

    public void setNewsArticleTitleSelector(String newsArticleTitleSelector) {
        this.newsArticleTitleSelector = newsArticleTitleSelector;
    }

	public String getNewsSrcBaseUrl() {
		return newsSrcBaseUrl;
	}

	public void setNewsSrcBaseUrl(String newsSrcBaseUrl) {
		this.newsSrcBaseUrl = newsSrcBaseUrl;
	}

	public String getNewsDateSelector() {
		return newsDateSelector;
	}

	public void setNewsDateSelector(String newsDateSelector) {
		this.newsDateSelector = newsDateSelector;
	}

	public int getNewsSrcOnPremId() {
		return newsSrcOnPremId;
	}

	public void setNewsSrcOnPremId(int newsSrcOnPremId) {
		this.newsSrcOnPremId = newsSrcOnPremId;
	}
	
	
}

