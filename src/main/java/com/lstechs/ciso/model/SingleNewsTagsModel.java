package com.lstechs.ciso.model;

public class SingleNewsTagsModel {
	private int newsTagId;
	private String newsTagName;

	public SingleNewsTagsModel(int newsTagId, String newsTagName) {
		this.newsTagId = newsTagId;
		this.newsTagName = newsTagName;
	}

	public SingleNewsTagsModel(NewsTagsModel newsTagsModel) {
		this.newsTagId = newsTagsModel.getNewsTagId();
		this.newsTagName = newsTagsModel.getNewsTagName();
	}

	public int getNewsTagId() {
		return newsTagId;
	}

	public void setNewsTagId(int newsTagId) {
		this.newsTagId = newsTagId;
	}

	public String getNewsTagName() {
		return newsTagName;
	}

	public void setNewsTagName(String newsTagName) {
		this.newsTagName = newsTagName;
	}
}
