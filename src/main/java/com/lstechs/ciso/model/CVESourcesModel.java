package com.lstechs.ciso.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Type;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "CveSources")
@EntityListeners(AuditingEntityListener.class)
public class CVESourcesModel extends CRUDModel {

    @Id
    @Column(name = "cveSrcId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int cveSrcId;
    
	@Column(name = "cveSrcOnPremId",  nullable = true ,columnDefinition = "Integer default '0'")
	private int cveSrcOnPremId ;

    @NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
    @Column(name = "cveSrcSiteName", nullable = false)
    private String cveSrcSiteName;

    @NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
    @Column(name = "cveSrcSiteUrl", nullable = false)
	@Type(type = "text")
	private String cveSrcSiteUrl;

    @NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
    @Column(name = "cveSrcIsActive", nullable = false)
    private boolean cveSrcIsActive;

    @JsonIgnore
    @OneToMany(mappedBy = "cveSrcId")
    private List<CVEModel> cveModels;

    public CVESourcesModel(String cveSrcSiteName, String cveSrcSiteUrl, boolean cveSrcIsActive) {
        super();
        this.cveSrcSiteName = cveSrcSiteName;
        this.cveSrcSiteUrl = cveSrcSiteUrl;
        this.cveSrcIsActive = cveSrcIsActive;
    }

    public CVESourcesModel(CVESourcesModel cveSourcesModel) {
        super();
        this.cveSrcSiteName = cveSourcesModel.getCveSrcSiteName();
        this.cveSrcSiteUrl = cveSourcesModel.getCveSrcSiteUrl();
        this.cveSrcIsActive = cveSourcesModel.isCveSrcIsActive();
    }

    public CVESourcesModel() {
        super();
    }

    public int getCveSrcId() {
        return cveSrcId;
    }

    public void setCveSrcId(int cveSrcId) {
        this.cveSrcId = cveSrcId;
    }

    public String getCveSrcSiteName() {
        return cveSrcSiteName;
    }

    public void setCveSrcSiteName(String cveSrcSiteName) {
        this.cveSrcSiteName = cveSrcSiteName;
    }

    public String getCveSrcSiteUrl() {
        return cveSrcSiteUrl;
    }

    public void setCveSrcSiteUrl(String cveSrcSiteUrl) {
        this.cveSrcSiteUrl = cveSrcSiteUrl;
    }

    public boolean isCveSrcIsActive() {
        return cveSrcIsActive;
    }

    public void setCveSrcIsActive(boolean cveSrcIsActive) {
        this.cveSrcIsActive = cveSrcIsActive;
    }

    public List<CVEModel> getCveModels() {
        return cveModels;
    }

    public void setCveModels(List<CVEModel> cveModels) {
        this.cveModels = cveModels;
    }

	public int getCveSrcOnPremId() {
		return cveSrcOnPremId;
	}

	public void setCveSrcOnPremId(int cveSrcOnPremId) {
		this.cveSrcOnPremId = cveSrcOnPremId;
	}
    
}
