package com.lstechs.ciso.model;

import com.lstechs.ciso.enums.ModuleEnum;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "ModuleTask", uniqueConstraints = @UniqueConstraint(name = "uniqueModuleTaskModule", columnNames = {"module", "mdlTskIdInternal", "tskId"}))
@EntityListeners(AuditingEntityListener.class)
public class ModuleTaskModel extends CRUDModel {

	@Id
	@Column(name = "mdlTskId", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int mdlTskId;

	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@OneToOne
	@JoinColumn(name = "tskId", nullable = false)
	private TaskModel tskId;

	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "module", nullable = false)
	private ModuleEnum module;

	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "mdlTskIdInternal", nullable = false)
	private int mdlTskIdInternal;

	public ModuleTaskModel(TaskModel tskId, ModuleEnum module, int mdlTskIdInternal) {
		super();
		this.tskId = tskId;
		this.module = module;
		this.mdlTskIdInternal = mdlTskIdInternal;
	}

	public ModuleTaskModel() {
		super();
	}

	public int getMdlTskId() {
		return mdlTskId;
	}

	public void setMdlTskId(int mdlTskId) {
		this.mdlTskId = mdlTskId;
	}

	public TaskModel getTskId() {
		return tskId;
	}

	public void setTskId(TaskModel tskId) {
		this.tskId = tskId;
	}

	public ModuleEnum getModule() {
		return module;
	}

	public void setModule(ModuleEnum module) {
		this.module = module;
	}

	public int getMdlTskIdInternal() {
		return mdlTskIdInternal;
	}

	public void setMdlTskIdInternal(int mdlTskIdInternal) {
		this.mdlTskIdInternal = mdlTskIdInternal;
	}
}
