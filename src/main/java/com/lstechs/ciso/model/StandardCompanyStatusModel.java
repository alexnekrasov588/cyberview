package com.lstechs.ciso.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "StandardCompanyStatus")
@EntityListeners(AuditingEntityListener.class)
public class StandardCompanyStatusModel extends CRUDModel {
	@Id
	@Column(name = "stdCmpId", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int stdCmpId;

	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "stdId", nullable = false)
	private StandardModel stdId;

	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "cmpId", nullable = false)
	private CompanyModel cmpId;

	public StandardCompanyStatusModel() {
	}

	public StandardCompanyStatusModel(StandardModel standardModel,CompanyModel companyModel) {
		this.stdId = standardModel;
		this.cmpId = companyModel;
	}

	public int getStdCmpId() {
		return stdCmpId;
	}

	public void setStdCmpId(int stdCmpId) {
		this.stdCmpId = stdCmpId;
	}

	public StandardModel getStdId() {
		return stdId;
	}

	public void setStdId(StandardModel stdId) {
		this.stdId = stdId;
	}

	public CompanyModel getCmpId() {
		return cmpId;
	}

	public void setCmpId(CompanyModel cmpId) {
		this.cmpId = cmpId;
	}
}
