package com.lstechs.ciso.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.lstechs.ciso.enums.ThreatTypeEnum;

@Entity
@Table(name = "threat")
@EntityListeners(AuditingEntityListener.class)
public class ThreatModel extends CRUDModel {
	

	@Id
	@Column(name = "threatId", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int threatId;
	
	@Column(name = "threatName", nullable = true)
	private String threatName;
	
	@Column(name = "threatDescription", nullable = true)
	@Type(type = "text")
	private String threatDescription;
	
	@Column(name = "threatTitle", nullable = true)
	private String threatTitle;
	
	@Column(name = "threatIsExample", nullable = true)
	private boolean threatIsExample;
	
	@Column(name = "threatType", nullable = true)
	private ThreatTypeEnum threatType;
	
	@JsonIgnore
	@OneToMany(mappedBy = "threatId")
	private List<VulnerabilityAssessmentsModel> vulnerabilityAssessmentsModel;

	public ThreatModel() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ThreatModel(String threatName, String threatDescription, String threatTitle,
			boolean threatIsExample, ThreatTypeEnum threatType) {
		super();
		this.threatName = threatName;
		this.threatDescription = threatDescription;
		this.threatTitle = threatTitle;
		this.threatIsExample = threatIsExample;
		this.threatType = threatType;
	}

	public int getThreatId() {
		return threatId;
	}

	public void setThreatId(int threatId) {
		this.threatId = threatId;
	}

	public String getThreatName() {
		return threatName;
	}

	public void setThreatName(String threatName) {
		this.threatName = threatName;
	}

	public String getThreatDescription() {
		return threatDescription;
	}

	public void setThreatDescription(String threatDescription) {
		this.threatDescription = threatDescription;
	}

	public String getThreatTitle() {
		return threatTitle;
	}

	public void setThreatTitle(String threatTitle) {
		this.threatTitle = threatTitle;
	}

	public boolean isThreatIsExample() {
		return threatIsExample;
	}

	public void setThreatIsExample(boolean threatIsExample) {
		this.threatIsExample = threatIsExample;
	}

	public ThreatTypeEnum getThreatType() {
		return threatType;
	}

	public void setThreatType(ThreatTypeEnum threatType) {
		this.threatType = threatType;
	}

	public List<VulnerabilityAssessmentsModel> getVulnerabilityAssessmentsModel() {
		return vulnerabilityAssessmentsModel;
	}

	public void setVulnerabilityAssessmentsModel(List<VulnerabilityAssessmentsModel> vulnerabilityAssessmentsModel) {
		this.vulnerabilityAssessmentsModel = vulnerabilityAssessmentsModel;
	}
	
	
	
}
