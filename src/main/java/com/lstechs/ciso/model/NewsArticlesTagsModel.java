package com.lstechs.ciso.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "NewsArticlesTags")
@EntityListeners(AuditingEntityListener.class)
public class NewsArticlesTagsModel extends CRUDModel {

	@Id
	@Column(name = "newsArtcTagsId", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int newsArtcTagsId;

	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@ManyToOne(fetch = FetchType.EAGER) // when load the table get the NewsArticlesModel object instate of id only
	@JoinColumn(name = "newsArtcId", nullable = false)
	private NewsArticlesModel newsArtcId;

	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "newsTagId", nullable = false)
	private NewsTagsModel newsTagId;

	public NewsArticlesTagsModel(NewsArticlesModel newsArtcId, NewsTagsModel newsTagId) {
		this.newsArtcId = newsArtcId;
		this.newsTagId = newsTagId;
	}

	public NewsArticlesTagsModel() {
	}

	public int getNewsArtcTagsId() {
		return newsArtcTagsId;
	}

	public void setNewsArtcTagsId(int newsArtcTagsId) {
		this.newsArtcTagsId = newsArtcTagsId;
	}

	public NewsArticlesModel getNewsArtcId() {
		return newsArtcId;
	}

	public void setNewsArtcId(NewsArticlesModel newsArtcId) {
		this.newsArtcId = newsArtcId;
	}

	public NewsTagsModel getNewsTagId() {
		return newsTagId;
	}

	public void setNewsTagId(NewsTagsModel newsTagId) {
		this.newsTagId = newsTagId;
	}
}
