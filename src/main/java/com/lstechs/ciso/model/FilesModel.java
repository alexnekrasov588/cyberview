package com.lstechs.ciso.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.*;

@Entity
@Table(name = "files")
@EntityListeners(AuditingEntityListener.class)
public class FilesModel extends CRUDModel {
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;

	@Column(name = "fileName", nullable = false)
	private String fileName;

	@Column(name = "fileType", nullable = false)
	private String fileType;

	@Column(name = "fileUrl")
	private String fileUrl;

	@Lob
	private byte[] data;

	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "cstStdId")
	private CustomerStandardModel cstStdId;

	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "stdQtrId")
	private StdQuestionaireModel stdQtrId;


	public FilesModel(String fileName, String fileType, byte[] data, CustomerStandardModel cstStdId,StdQuestionaireModel stdQtrId ) {
		this.fileName = fileName;
		this.fileType = fileType;
		this.data = data;
		this.cstStdId = cstStdId;
		this.stdQtrId = stdQtrId;
	}

	public FilesModel() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	@JsonIgnore
	public CustomerStandardModel getCstStdId() {
		return cstStdId;
	}

	public void setCstStdId(CustomerStandardModel cstStdId) {
		this.cstStdId = cstStdId;
	}

	public String getFileUrl() {
		return fileUrl;
	}

	public void setFileUrl(String fileUrl) {
		this.fileUrl = fileUrl;
	}

	public InputStream toInputStream() {
		return new ByteArrayInputStream(this.data);
	}

	public StdQuestionaireModel getStdQtrId() {
		return stdQtrId;
	}

	public void setStdQtrId(StdQuestionaireModel stdQtrId) {
		this.stdQtrId = stdQtrId;
	}
}
