package com.lstechs.ciso.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.lstechs.ciso.enums.CompliantEnum;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.*;

@Entity
@Table(name = "CustomerStandard", uniqueConstraints = @UniqueConstraint(name = "uniqueCustomerStandardModule", columnNames = {"stdRqrId", "cmpId"}))
@EntityListeners(AuditingEntityListener.class)
public class CustomerStandardModel extends CRUDModel {

	@Id
	@Column(name = "cstStdId", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int cstStdId;

	@NotNull(groups = {CRUDCreation.class})
	@ManyToOne
	@JoinColumn(name = "qtrQstId", nullable = false)
	private StdQtrQuestionsModel qtrQstId;

	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@NotNull(groups = {CRUDCreation.class})
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "stdId", nullable = false)
	private StandardModel stdId;

	@NotNull(groups = {CRUDCreation.class})
	@ManyToOne
	@JoinColumn(name = "stdRqrId", nullable = false)
	private StdRequirementsModel stdRqrId;

	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "cmpId")
	private CompanyModel cmpId;

	@Column(name = "compliant")
	private CompliantEnum compliant;

	@Column(name = "cstStdRemarks")
	private String cstStdRemarks;

	@Column(name = "cstQstAnswer")
	private String cstQstAnswer;

	public CustomerStandardModel(CustomerStandardModel customerStandardModel) {
		this.qtrQstId = customerStandardModel.qtrQstId;
		this.stdId = customerStandardModel.stdId;
		this.stdRqrId = customerStandardModel.stdRqrId;
		this.cmpId = customerStandardModel.cmpId;
		this.compliant = customerStandardModel.compliant;
		this.cstStdRemarks = customerStandardModel.cstStdRemarks;
		this.answerDate1 = customerStandardModel.answerDate1;
		this.answerDate2 = customerStandardModel.answerDate2;
		this.cstQstAnswer = customerStandardModel.cstQstAnswer;
		this.cstQstFileAnswer = customerStandardModel.cstQstFileAnswer;

	}

	public Map<String, String> getCstQstFileAnswer() {
		return cstQstFileAnswer;
	}

	public void setCstQstFileAnswer(Map<String, String> cstQstFileAnswer) {
		this.cstQstFileAnswer = cstQstFileAnswer;
	}

	@Transient
	private Map<String, String> cstQstFileAnswer;

	@JsonIgnore
	@OneToMany(mappedBy = "customerStandardId")
	private List<CstQstFileAnswerModel> cstQstFileAnswerModel;

	@Column(name = "answerDate1")
	private String answerDate1;

	@Column(name = "answerDate2")
	private String answerDate2;

	@JsonIgnore
	@OneToMany(mappedBy = "cstStdId")
	private List<FilesModel> filesModel;

//	@Transient
//	private boolean isFileExist;
//
//	@Transient
//	private String fileNames;
//
//	@Transient
//	private String fileUrls;

//	public String getFileNames() {
//		return fileNames;
//	}
//
//	public void setFileNames(String fileNames) {
//		this.fileNames = fileNames;
//	}
//
//	public String getFileUrls() {
//		return fileUrls;
//	}
//
//	public void setFileUrls(String fileUrls) {
//		this.fileUrls = fileUrls;
//	}

	@Transient
	private Date fileCreatedAt;

	@Transient
	private TaskModel taskModel;


	public String getCstQstAnswer() {
		return cstQstAnswer;
	}

	public void setCstQstAnswer(String cstQstAnswer) {
		this.cstQstAnswer = cstQstAnswer;
	}

	public CustomerStandardModel(StdQtrQuestionsModel qtrQstId, StandardModel stdId, StdRequirementsModel stdRqrId, CompanyModel cmpId, CompliantEnum compliant, String cstStdRemarks,
								 String cstQstAnswer, String answerDate1, String answerDate2, Map<String, String> cstQstFileAnswer) {
		super();
		this.qtrQstId = qtrQstId;
		this.stdId = stdId;
		this.stdRqrId = stdRqrId;
		this.cmpId = cmpId;
		this.compliant = compliant;
		this.cstStdRemarks = cstStdRemarks;
		this.answerDate1 = answerDate1;
		this.answerDate2 = answerDate2;
		this.cstQstAnswer = cstQstAnswer;
		this.cstQstFileAnswer = cstQstFileAnswer;
	}

	public CustomerStandardModel() {
		super();
	}

	public int getCstStdId() {
		return cstStdId;
	}

	public void setCstStdId(int cstStdId) {
		this.cstStdId = cstStdId;
	}

	@JsonIgnore
	public StdQtrQuestionsModel getQtrQstId() {
		return qtrQstId;
	}

	@JsonProperty
	public void setQtrQstId(StdQtrQuestionsModel qtrQstId) {
		this.qtrQstId = qtrQstId;
	}

	@JsonIgnore
	public StandardModel getStdId() {
		return stdId;
	}

	@JsonProperty
	public void setStdId(StandardModel stdId) {
		this.stdId = stdId;
	}

	@JsonIgnore
	public StdRequirementsModel getStdRqrId() {
		return stdRqrId;
	}

	@JsonProperty
	public void setStdRqrId(StdRequirementsModel stdRqrId) {
		this.stdRqrId = stdRqrId;
	}

	public CompanyModel getCmpId() {
		return cmpId;
	}

	public void setCmpId(CompanyModel cmpId) {
		this.cmpId = cmpId;
	}

	public CompliantEnum getCompliant() {
		return compliant;
	}

	public void setCompliant(CompliantEnum compliant) {
		this.compliant = compliant;
	}

	public String getCstStdRemarks() {
		return cstStdRemarks;
	}

	public void setCstStdRemarks(String cstStdRemarks) {
		this.cstStdRemarks = cstStdRemarks;
	}

	public SingleTaskModel getTaskModel() {
		if (taskModel != null) {
			return new SingleTaskModel(taskModel, new SingleCompanyUserModel(taskModel.getTskAssignee()));
		} else return null;
	}

	public void setTaskModel(TaskModel taskModel) {
		this.taskModel = taskModel;
	}

	@JsonIgnore
	public List<FilesModel> getFilesModel() {
		return filesModel;
	}

	@JsonIgnore
	public FilesModel getFile() {
		if (filesModel != null && filesModel.size() > 0) {
			for (FilesModel tmpFilesModel : filesModel) {
				if (tmpFilesModel.getDeletedAt() == null) return tmpFilesModel;
			}
		}
		return null;
	}

	public void setFilesModels(List<FilesModel> filesModel) {
		this.filesModel = filesModel;
	}

	public boolean getIsFileExist() {
		return getFile() != null;
	}

//	public void setIsFileExist(boolean fileExist) {
//		isFileExist = fileExist;
//	}

	public Date getFileCreatedAt() {
		if (getFile() != null) {
			fileCreatedAt = getFile().getCreatedAt();
		}
		return fileCreatedAt;
	}

	public void setFileCreatedAt(Date fileCreatedAt) {
		this.fileCreatedAt = fileCreatedAt;
	}

	public String getAnswerDate1() {
		return answerDate1;
	}

	public void setAnswerDate1(String answerDate1) {
		this.answerDate1 = answerDate1;
	}

	public String getAnswerDate2() {
		return answerDate2;
	}

	public void setAnswerDate2(String answerDate2) {
		this.answerDate2 = answerDate2;
	}
}
