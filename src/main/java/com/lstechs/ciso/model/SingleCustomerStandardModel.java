package com.lstechs.ciso.model;

import com.lstechs.ciso.enums.CompliantEnum;

import java.util.List;

public class SingleCustomerStandardModel {
	private int cstStdId;
	private int qtrQstId;
	private int stdId;
	private int stdRqrId;
	private CompliantEnum compliant;
	private String cstStdRemarks;
	private String cstQstAnswer;

	public SingleCustomerStandardModel(int cstStdId, int qtrQstId, int stdId, int stdRqrId, CompliantEnum compliant, String cstStdRemarks, String cstQstAnswer) {
		this.cstStdId = cstStdId;
		this.qtrQstId = qtrQstId;
		this.stdId = stdId;
		this.stdRqrId = stdRqrId;
		this.compliant = compliant;
		this.cstStdRemarks = cstStdRemarks;
		this.cstQstAnswer = cstQstAnswer;
	}

	public SingleCustomerStandardModel(CustomerStandardModel customerStandardModel) {
		this.cstStdId = customerStandardModel.getCstStdId();
		this.qtrQstId = customerStandardModel.getQtrQstId().getQtrQstId();
		this.stdId = customerStandardModel.getStdId().getStdId();
		this.stdRqrId = customerStandardModel.getStdRqrId().getStdRqrId();
		this.compliant = customerStandardModel.getCompliant();
		this.cstStdRemarks = customerStandardModel.getCstStdRemarks();
		this.cstQstAnswer = customerStandardModel.getCstQstAnswer();
	}

	public String getCstQstAnswer() {
		return cstQstAnswer;
	}

	public void setCstQstAnswer(String cstQstAnswer) {
		this.cstQstAnswer = cstQstAnswer;
	}

	public int getCstStdId() {
		return cstStdId;
	}

	public void setCstStdId(int cstStdId) {
		this.cstStdId = cstStdId;
	}

	public int getQtrQstId() {
		return qtrQstId;
	}

	public void setQtrQstId(int qtrQstId) {
		this.qtrQstId = qtrQstId;
	}

	public int getStdId() {
		return stdId;
	}

	public void setStdId(int stdId) {
		this.stdId = stdId;
	}

	public int getStdRqrId() {
		return stdRqrId;
	}

	public void setStdRqrId(int stdRqrId) {
		this.stdRqrId = stdRqrId;
	}

	public CompliantEnum getCompliant() {
		return compliant;
	}

	public void setCompliant(CompliantEnum compliant) {
		this.compliant = compliant;
	}

	public String getCstStdRemarks() {
		return cstStdRemarks;
	}

	public void setCstStdRemarks(String cstStdRemarks) {
		this.cstStdRemarks = cstStdRemarks;
	}


}
