package com.lstechs.ciso.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.*;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Entity
@Table(name = "StdSection")
@EntityListeners(AuditingEntityListener.class)
public class StdSectionModel extends CRUDModel {

	@Id
	@Column(name = "stdSctId", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int stdSctId;

	@Column(name = "stdSctOnPremId", nullable = true, columnDefinition = "Integer default '0'")
	private int stdSctOnPremId;

	@ManyToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "parent", nullable = true)
	private StdSectionModel parent;

	@Column(name = "previousParentId")
	private int previousParentId;

	@NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "stdSctName", nullable = false)
	@Type(type = "text")
	private String stdSctName;

	@NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "stdSctDescription", nullable = true)
	@Type(type = "text")
	private String stdSctDescription;

	@Column(name = "stdSctOrder", nullable = true)
	private int order;

	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "stdQtrId", nullable = true)
	private StdQuestionaireModel stdQtrId;

	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@OneToMany(mappedBy = "stdSctId")
	private List<StdQtrQuestionsModel> stdQtrQuestionsModel;

	@Transient
	private List<StdQtrQuestionsModel> stdQtrQuestionsList;

	@Transient
	private List<StdSectionModel> stdSectionModels;

	@Transient
	public int qtr;

	public StdSectionModel(StdSectionModel parent, String stdSctName, String stdSctDescription, StdQuestionaireModel stdQtrId, int order) {
		super();
		this.parent = parent;
		this.stdSctName = stdSctName;
		this.stdSctDescription = stdSctDescription;
		this.stdQtrId = stdQtrId;
		this.order = order;
	}

	public StdSectionModel(StdSectionModel stdSectionModel) {
		super();
		this.parent = stdSectionModel.getParent();
		this.stdSctName = stdSectionModel.getStdSctName();
		this.stdSctDescription = stdSectionModel.getStdSctDescription();
		this.stdQtrId = stdSectionModel.getStdQtrId();
		this.order = stdSectionModel.order;
	}

	public StdSectionModel() {
		super();
	}

	public int getStdSctId() {
		return stdSctId;
	}

	public void setStdSctId(int stdSctId) {
		this.stdSctId = stdSctId;
	}

	public StdSectionModel getParent() {
		return parent;
	}

	public void setParent(StdSectionModel parent) {
		this.parent = parent;
	}

	public String getStdSctName() {
		return stdSctName;
	}

	public void setStdSctName(String stdSctName) {
		this.stdSctName = stdSctName;
	}

	public String getStdSctDescription() {
		return stdSctDescription;
	}

	public void setStdSctDescription(String stdSctDescription) {
		this.stdSctDescription = stdSctDescription;
	}

	@JsonIgnore
	public StdQuestionaireModel getStdQtrId() {
		return stdQtrId;
	}

	public void setStdQtrId(StdQuestionaireModel stdQtrId) {
		this.stdQtrId = stdQtrId;
	}

	public List<StdQtrQuestionsModel> getStdQtrQuestionsModel() {
		return stdQtrQuestionsModel;
	}

	public void setStdQtrQuestionsModel(List<StdQtrQuestionsModel> stdQtrQuestionsModel) {
		this.stdQtrQuestionsModel = stdQtrQuestionsModel;
	}

	public List<StdQtrQuestionsModel> getStdQtrQuestionsList() {
		return stdQtrQuestionsList;
	}

	public void setStdQtrQuestionsList(List<StdQtrQuestionsModel> stdQtrQuestionsList) {
		this.stdQtrQuestionsList = stdQtrQuestionsList;
	}

	public List<StdSectionModel> getStdSectionModels() {
		return stdSectionModels;
	}

	public void setStdSectionModels(List<StdSectionModel> stdSectionModels) {
		this.stdSectionModels = stdSectionModels;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public int getQtr() {
		if (this.stdQtrId != null)
			return this.stdQtrId.getStdQtrId();
		else return 0;

	}

	public int getPreviousParentId() {
		return previousParentId;
	}

	public void setPreviousParentId(int previousParentId) {
		this.previousParentId = previousParentId;
	}

	public int getStdSctOnPremId() {
		return stdSctOnPremId;
	}

	public void setStdSctOnPremId(int stdSctOnPremId) {
		this.stdSctOnPremId = stdSctOnPremId;
	}
}
