package com.lstechs.ciso.model;

import com.lstechs.ciso.enums.UsersEnum;

import java.io.Serializable;

public class AuthResponseModel implements Serializable {
    
    private String token;
    private UsersEnum type;

    public AuthResponseModel(String token, UsersEnum type) {
        this.token = token;
        this.type = type;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public UsersEnum getType() {
        return type;
    }

    public void setType(UsersEnum type) {
        this.type = type;
    }
}
