package com.lstechs.ciso.model;

public class SingleCVESourcesModel {
	private int cveSrcId;
	private String cveSrcSiteName;
	private String cveSrcSiteUrl;
	private boolean cveSrcIsActive;

	public SingleCVESourcesModel(int cveSrcId, String cveSrcSiteName, String cveSrcSiteUrl, boolean cveSrcIsActive) {
		this.cveSrcId = cveSrcId;
		this.cveSrcSiteName = cveSrcSiteName;
		this.cveSrcSiteUrl = cveSrcSiteUrl;
		this.cveSrcIsActive = cveSrcIsActive;
	}

	public SingleCVESourcesModel(CVESourcesModel cveSourcesModel) {
		this.cveSrcId = cveSourcesModel.getCveSrcId();
		this.cveSrcSiteName = cveSourcesModel.getCveSrcSiteName();
		this.cveSrcSiteUrl = cveSourcesModel.getCveSrcSiteUrl();
		this.cveSrcIsActive = cveSourcesModel.isCveSrcIsActive();
	}

	public int getCveSrcId() {
		return cveSrcId;
	}

	public void setCveSrcId(int cveSrcId) {
		this.cveSrcId = cveSrcId;
	}

	public String getCveSrcSiteName() {
		return cveSrcSiteName;
	}

	public void setCveSrcSiteName(String cveSrcSiteName) {
		this.cveSrcSiteName = cveSrcSiteName;
	}

	public String getCveSrcSiteUrl() {
		return cveSrcSiteUrl;
	}

	public void setCveSrcSiteUrl(String cveSrcSiteUrl) {
		this.cveSrcSiteUrl = cveSrcSiteUrl;
	}

	public boolean isCveSrcIsActive() {
		return cveSrcIsActive;
	}

	public void setCveSrcIsActive(boolean cveSrcIsActive) {
		this.cveSrcIsActive = cveSrcIsActive;
	}
}
