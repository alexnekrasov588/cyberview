package com.lstechs.ciso.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.lstechs.ciso.enums.ExplanationAndArticleNumberTypeEnum;
import com.lstechs.ciso.enums.QtrQstTypeEnum;
import org.hibernate.annotations.Type;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "stdQtrQuestions")
@EntityListeners(AuditingEntityListener.class)
public class StdQtrQuestionsModel extends CRUDModel {

	@Id
	@Column(name = "qtrQstId", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int qtrQstId;

	@Column(name = "qtrQstOnPremId", columnDefinition = "Integer default '0'")
	private int qtrQstOnPremId;

	@NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "qtrQstName", nullable = false)
	@Type(type = "text")
	private String qtrQstName;

	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "qtrQstType", nullable = false)
	private QtrQstTypeEnum qtrQstType;

	@Column(name = "answerDate1Type")
	private QtrQstTypeEnum answerDate1Type;

	@Column(name = "answerDate2Type")
	private QtrQstTypeEnum answerDate2Type;

	@Column(name = "stdRqrOrder")
	private int order;

	@JsonIgnore
	@OneToOne
	@JoinColumn(name = "stdRqrId", nullable = true)
	private StdRequirementsModel stdRqrId;

	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "stdSctId", nullable = true)
	private StdSectionModel stdSctId;

	@JsonIgnore
	@OneToMany(mappedBy = "qtrQstId")
	private List<CustomerStandardModel> customerStandardModel;

	@OneToMany(mappedBy = "sqoQstId", cascade = CascadeType.ALL)
	private List<StdQstOptionModel> stdQstOptionModel;

	@OneToMany(mappedBy = "sqoQstId")
	private List<StdAnswer1OptionModel> stdAnswer1OptionModel;

	@OneToMany(mappedBy = "sqoQstId")
	private List<StdAnswer2OptionModel> stdAnswer2OptionModel;

	@Transient
	private StdRequirementsModel stdRequirements;

	@Transient
	public int rqr;

	@Transient
	public int sct;

	public StdQtrQuestionsModel(String qtrQstName, QtrQstTypeEnum qtrQstType, StdRequirementsModel stdRqrId,
								StdSectionModel stdSctId, int order) {
		super();
		this.qtrQstName = qtrQstName;
		this.qtrQstType = qtrQstType;
		this.stdRqrId = stdRqrId;
		this.stdSctId = stdSctId;
		this.order = order;
	}

	public StdQtrQuestionsModel(StdQtrQuestionsModel stdQtrQuestionsModel) {
		super();
		this.qtrQstName = stdQtrQuestionsModel.getQtrQstName();
		this.qtrQstType = stdQtrQuestionsModel.getQtrQstType();
		this.stdRqrId = stdQtrQuestionsModel.getStdRqrId();
		this.stdSctId = stdQtrQuestionsModel.getStdSctId();
		this.order = stdQtrQuestionsModel.getOrder();
	}

	public StdQtrQuestionsModel() {
		super();
	}

	public int getQtrQstId() {
		return qtrQstId;
	}

	public void setQtrQstId(int qtrQstId) {
		this.qtrQstId = qtrQstId;
	}

	public String getQtrQstName() {
		return qtrQstName;
	}

	public void setQtrQstName(String qtrQstName) {
		this.qtrQstName = qtrQstName;
	}

	public QtrQstTypeEnum getQtrQstType() {
		return qtrQstType;
	}

	public void setQtrQstType(QtrQstTypeEnum qtrQstType) {
		this.qtrQstType = qtrQstType;
	}

	public StdRequirementsModel getStdRqrId() {
		return stdRqrId;
	}

	public void setStdRqrId(StdRequirementsModel stdRqrId) {
		this.stdRqrId = stdRqrId;
	}

	public List<CustomerStandardModel> getCustomerStandardModel() {
		return customerStandardModel;
	}

	public void setCustomerStandardModel(List<CustomerStandardModel> customerStandardModel) {
		this.customerStandardModel = customerStandardModel;
	}

	@JsonIgnore
	public StdSectionModel getStdSctId() {
		return stdSctId;
	}

	public void setStdSctId(StdSectionModel stdSctId) {
		this.stdSctId = stdSctId;
	}

	public StdRequirementsModel getStdRequirements() {
		return stdRequirements;
	}

	public void setStdRequirements(StdRequirementsModel stdRequirements) {
		this.stdRequirements = stdRequirements;
	}

	public List<StdQstOptionModel> getStdQstOptionModel() {
		List<StdQstOptionModel> stdQstOptionModels = new ArrayList<>();
		if (this.stdQstOptionModel != null && this.stdQstOptionModel.size() > 0) {
			for (StdQstOptionModel tmpStdQstOptionModel : this.stdQstOptionModel) {
				if (tmpStdQstOptionModel.getDeletedAt() == null)
					stdQstOptionModels.add(tmpStdQstOptionModel);
			}
		}
		return stdQstOptionModels;
	}

	public void setStdQstOptionModel(List<StdQstOptionModel> stdQstOptionModel) {
		this.stdQstOptionModel = stdQstOptionModel;
	}

	public List<StdAnswer1OptionModel> getStdAnswer1OptionModel() {
		List<StdAnswer1OptionModel> stdAnswer1OptionModels = new ArrayList<>();
		if (this.stdAnswer1OptionModel != null && this.stdAnswer1OptionModel.size() > 0) {
			for (StdAnswer1OptionModel tmpStdAnswer1OptionModel : this.stdAnswer1OptionModel) {
				if (tmpStdAnswer1OptionModel.getDeletedAt() == null)
					stdAnswer1OptionModels.add(tmpStdAnswer1OptionModel);
			}
		}
		return stdAnswer1OptionModels;
	}

	public void setStdAnswer1OptionModel(List<StdAnswer1OptionModel> stdAnswer1OptionModel) {
		this.stdAnswer1OptionModel = stdAnswer1OptionModel;
	}

	public List<StdAnswer2OptionModel> getStdAnswer2OptionModel() {
		List<StdAnswer2OptionModel> stdAnswer2OptionModels = new ArrayList<>();
		if (this.stdAnswer2OptionModel != null && this.stdAnswer2OptionModel.size() > 0) {
			for (StdAnswer2OptionModel tmpStdAnswer2OptionModel : this.stdAnswer2OptionModel) {
				if (tmpStdAnswer2OptionModel.getDeletedAt() == null)
					stdAnswer2OptionModels.add(tmpStdAnswer2OptionModel);
			}
		}
		return stdAnswer2OptionModels;
	}

	public void setStdAnswer2OptionModel(List<StdAnswer2OptionModel> stdAnswer2OptionModel) {
		this.stdAnswer2OptionModel = stdAnswer2OptionModel;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public int getRqr() {
		if (this.stdRqrId != null)
			return this.stdRqrId.getStdRqrId();
		else return 0;
	}


	public int getSct() {
		if (this.stdSctId != null)
			return this.stdSctId.getStdSctId();
		else return 0;
	}

	public int getQtrQstOnPremId() {
		return qtrQstOnPremId;
	}

	public void setQtrQstOnPremId(int qtrQstOnPremId) {
		this.qtrQstOnPremId = qtrQstOnPremId;
	}

	public QtrQstTypeEnum getAnswerDate1Type() {
		return answerDate1Type;
	}

	public void setAnswerDate1Type(QtrQstTypeEnum answerDate1Type) {
		this.answerDate1Type = answerDate1Type;
	}

	public QtrQstTypeEnum getAnswerDate2Type() {
		return answerDate2Type;
	}

	public void setAnswerDate2Type(QtrQstTypeEnum answerDate2Type) {
		this.answerDate2Type = answerDate2Type;
	}
}
