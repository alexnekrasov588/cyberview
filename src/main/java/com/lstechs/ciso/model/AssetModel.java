package com.lstechs.ciso.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "Asset")
@EntityListeners(AuditingEntityListener.class)
public class AssetModel extends CRUDModel {
	@Id
	@Column(name = "astId", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int astId;

	@NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "astName", nullable = false)
	private String astName;

	@Column(name = "astDescrption")
	private String astDescrption;

	@Column(name = "astLocation")
	private String astLocation;

	@Column(name = "astOperator")
	private String astOperator;

	@Column(name = "astOwner")
	private String astOwner;

	@Column(name = "astAmount")
	private int astAmount;

	@Column(name = "astManufactor")
	private String astManufactor;

	@Column(name = "astProduct")
	private String astProduct;

	@Column(name = "astModel")
	private String astModel; //for devices

	@Column(name = "astVersion")
	private String astVersion; //applications

	@Column(name = "astStartDate")
	private Date astStartDate;

	@Column(name = "astEndDate")
	private Date astEndDate;

	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "astType", nullable = false)
	private AssetTypeModel astType;

	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cmpId", nullable = false)
	private CompanyModel cmpId;

	@JsonIgnore
	@OneToMany(mappedBy = "astId")
	private List<VulnerabilityAssessmentsModel> vulnerabilityAssessmentsModel;

	public AssetModel() {
		super();
	}

	public AssetModel(String astName, String astDescrption, String astLocation, String astOperator,
					  String astOwner, int astAmount, String astManufactor, String astProduct, String astModel,
					  String astVersion, AssetTypeModel astType, CompanyModel cmpId, Date astStartDate, Date astEndDate) {
		super();
		this.astName = astName;
		this.astDescrption = astDescrption;
		this.astLocation = astLocation;
		this.astOperator = astOperator;
		this.astOwner = astOwner;
		this.astAmount = astAmount;
		this.astManufactor = astManufactor;
		this.astProduct = astProduct;
		this.astModel = astModel;
		this.astVersion = astVersion;
		this.astType = astType;
		this.cmpId = cmpId;
		this.astStartDate = astStartDate;
		this.astEndDate = astEndDate;
	}

	public int getAstId() {
		return astId;
	}

	public void setAstId(int astId) {
		this.astId = astId;
	}

	public String getAstName() {
		return astName;
	}

	public void setAstName(String astName) {
		this.astName = astName;
	}

	public String getAstDescrption() {
		return astDescrption;
	}

	public void setAstDescrption(String astDescrption) {
		this.astDescrption = astDescrption;
	}

	public String getAstLocation() {
		return astLocation;
	}

	public void setAstLocation(String astLocation) {
		this.astLocation = astLocation;
	}

	public String getAstOperator() {
		return astOperator;
	}

	public void setAstOperator(String astOperator) {
		this.astOperator = astOperator;
	}

	public String getAstOwner() {
		return astOwner;
	}

	public void setAstOwner(String astOwner) {
		this.astOwner = astOwner;
	}

	public int getAstAmount() {
		return astAmount;
	}

	public void setAstAmount(int astAmount) {
		this.astAmount = astAmount;
	}

	public String getAstManufactor() {
		return astManufactor;
	}

	public void setAstManufactor(String astManufactor) {
		if (astManufactor == null || astManufactor.isEmpty()) {
			this.astManufactor = null;
		} else {
			this.astManufactor = astManufactor;
		}
	}

	public String getAstProduct() {
		return astProduct;
	}

	public void setAstProduct(String astProduct) {
		if (astProduct == null || astProduct.isEmpty()) {
			this.astProduct = null;
		} else {
			this.astProduct = astProduct;
		}
	}

	public String getAstModel() {
		return astModel;
	}

	public void setAstModel(String astModel) {
		this.astModel = astModel;
	}

	public String getAstVersion() {
		return astVersion;
	}

	public void setAstVersion(String astVersion) {
		this.astVersion = astVersion;
	}

	public AssetTypeModel getAstType() {
		return astType;
	}

	public void setAstType(AssetTypeModel astType) {
		this.astType = astType;
	}

	public CompanyModel getCmpId() {
		return cmpId;
	}

	public void setCmpId(CompanyModel cmpId) {
		this.cmpId = cmpId;
	}

	public Date getAstStartDate() {
		return astStartDate;
	}

	public void setAstStartDate(Date astStartDate) {
		this.astStartDate = astStartDate;
	}

	public Date getAstEndDate() {
		return astEndDate;
	}

	public void setAstEndDate(Date astEndDate) {
		this.astEndDate = astEndDate;
	}

	public List<VulnerabilityAssessmentsModel> getVulnerabilityAssessmentsModel() {
		return vulnerabilityAssessmentsModel;
	}

	public void setVulnerabilityAssessmentsModel(List<VulnerabilityAssessmentsModel> vulnerabilityAssessmentsModel) {
		this.vulnerabilityAssessmentsModel = vulnerabilityAssessmentsModel;
	}


}
