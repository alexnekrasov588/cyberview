package com.lstechs.ciso.model;

public class SingleNewsArticlesTagsModel {
	private int newsArtcTagsId;
	private int newsArtcId;
	private int newsTagId;

	public SingleNewsArticlesTagsModel(int newsArtcTagsId, int newsArtcId, int newsTagId) {
		this.newsArtcTagsId = newsArtcTagsId;
		this.newsArtcId = newsArtcId;
		this.newsTagId = newsTagId;
	}

	public SingleNewsArticlesTagsModel(NewsArticlesTagsModel newsArticlesTagsModel) {
		this.newsArtcTagsId = newsArticlesTagsModel.getNewsArtcTagsId();
		this.newsArtcId = newsArticlesTagsModel.getNewsArtcId().getNewsArtcId();
		this.newsTagId = newsArticlesTagsModel.getNewsTagId().getNewsTagId();
	}

	public int getNewsArtcTagsId() {
		return newsArtcTagsId;
	}

	public void setNewsArtcTagsId(int newsArtcTagsId) {
		this.newsArtcTagsId = newsArtcTagsId;
	}

	public int getNewsArtcId() {
		return newsArtcId;
	}

	public void setNewsArtcId(int newsArtcId) {
		this.newsArtcId = newsArtcId;
	}

	public int getNewsTagId() {
		return newsTagId;
	}

	public void setNewsTagId(int newsTagId) {
		this.newsTagId = newsTagId;
	}
}
