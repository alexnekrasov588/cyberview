package com.lstechs.ciso.model;

public class ComplianceAnswer {
	private String answer;

	public ComplianceAnswer(){
	}

	public ComplianceAnswer(String answer){
	this.answer = answer;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}
}
