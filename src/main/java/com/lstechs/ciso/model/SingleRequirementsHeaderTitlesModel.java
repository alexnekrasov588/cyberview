package com.lstechs.ciso.model;

public class SingleRequirementsHeaderTitlesModel {

	private int requirementsHeaderTitlesId;
	private String requirementTitle;
	private String descriptionTitle;
	private String explanationTitle;
	private String articleNumberTitle;
	private String genericAnswer1Title;
	private String genericAnswer2Title;
	private String documentationTitle;
	private String complianceTitle;
	private String remarkTitle;
	private String taskTitle;
	private StdQuestionaireModel questionnaireId;

	public SingleRequirementsHeaderTitlesModel(int requirementsHeaderTitlesId, String requirementTitle, String descriptionTitle, String explanationTitle, String articleNumberTitle, String genericAnswer1Title, String genericAnswer2Title, String documentationTitle, String complianceTitle, String remarkTitle, String taskTitle) {
		this.requirementsHeaderTitlesId = requirementsHeaderTitlesId;
		this.requirementTitle = requirementTitle;
		this.descriptionTitle = descriptionTitle;
		this.explanationTitle = explanationTitle;
		this.articleNumberTitle = articleNumberTitle;
		this.genericAnswer1Title = genericAnswer1Title;
		this.genericAnswer2Title = genericAnswer2Title;
		this.documentationTitle = documentationTitle;
		this.complianceTitle = complianceTitle;
		this.remarkTitle = remarkTitle;
		this.taskTitle = taskTitle;
	}

	public SingleRequirementsHeaderTitlesModel(RequirementsHeaderTitlesModel requirementsHeaderTitlesModel) {
		this.requirementsHeaderTitlesId = requirementsHeaderTitlesModel.getRequirementsHeaderTitlesId();
		this.requirementTitle = requirementsHeaderTitlesModel.getRequirementTitle();
		this.descriptionTitle = requirementsHeaderTitlesModel.getDescriptionTitle();
		this.explanationTitle = requirementsHeaderTitlesModel.getExplanationTitle();
		this.articleNumberTitle = requirementsHeaderTitlesModel.getArticleNumberTitle();
		this.genericAnswer1Title = requirementsHeaderTitlesModel.getGenericAnswer1Title();
		this.genericAnswer2Title = requirementsHeaderTitlesModel.getGenericAnswer2Title();
		this.documentationTitle = requirementsHeaderTitlesModel.getDocumentationTitle();
		this.complianceTitle = requirementsHeaderTitlesModel.getComplianceTitle();
		this.remarkTitle = requirementsHeaderTitlesModel.getRemarkTitle();
		this.taskTitle = requirementsHeaderTitlesModel.getTaskTitle();
	}

	public int getRequirementsHeaderTitlesId() {
		return requirementsHeaderTitlesId;
	}

	public void setRequirementsHeaderTitlesId(int requirementsHeaderTitlesId) {
		this.requirementsHeaderTitlesId = requirementsHeaderTitlesId;
	}

	public String getRequirementTitle() {
		return requirementTitle;
	}

	public void setRequirementTitle(String requirementTitle) {
		this.requirementTitle = requirementTitle;
	}

	public String getDescriptionTitle() {
		return descriptionTitle;
	}

	public void setDescriptionTitle(String descriptionTitle) {
		this.descriptionTitle = descriptionTitle;
	}

	public String getExplanationTitle() {
		return explanationTitle;
	}

	public void setExplanationTitle(String explanationTitle) {
		this.explanationTitle = explanationTitle;
	}

	public String getArticleNumberTitle() {
		return articleNumberTitle;
	}

	public void setArticleNumberTitle(String articleNumberTitle) {
		this.articleNumberTitle = articleNumberTitle;
	}

	public String getGenericAnswer1Title() {
		return genericAnswer1Title;
	}

	public void setGenericAnswer1Title(String genericAnswer1Title) {
		this.genericAnswer1Title = genericAnswer1Title;
	}

	public String getGenericAnswer2Title() {
		return genericAnswer2Title;
	}

	public void setGenericAnswer2Title(String genericAnswer2Title) {
		this.genericAnswer2Title = genericAnswer2Title;
	}

	public String getDocumentationTitle() {
		return documentationTitle;
	}

	public void setDocumentationTitle(String documentationTitle) {
		this.documentationTitle = documentationTitle;
	}

	public String getComplianceTitle() {
		return complianceTitle;
	}

	public void setComplianceTitle(String complianceTitle) {
		this.complianceTitle = complianceTitle;
	}

	public String getRemarkTitle() {
		return remarkTitle;
	}

	public void setRemarkTitle(String remarkTitle) {
		this.remarkTitle = remarkTitle;
	}

	public String getTaskTitle() {
		return taskTitle;
	}

	public void setTaskTitle(String taskTitle) {
		this.taskTitle = taskTitle;
	}
}
