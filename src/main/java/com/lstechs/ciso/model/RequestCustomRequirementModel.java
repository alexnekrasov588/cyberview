package com.lstechs.ciso.model;

import java.util.List;

public class RequestCustomRequirementModel {
	private int cstRqrId;
	private String cstRqrDescription;
	private List<CrossReferenceModel> crossReferenceModel;

	public int getCstRqrId() {
		return cstRqrId;
	}

	public void setCstRqrId(int cstRqrId) {
		this.cstRqrId = cstRqrId;
	}

	public String getCstRqrDescription() {
		return cstRqrDescription;
	}

	public void setCstRqrDescription(String cstRqrDescription) {
		this.cstRqrDescription = cstRqrDescription;
	}

	public List<CrossReferenceModel> getCrossReferenceModel() {
		return crossReferenceModel;
	}

	public void setCrossReferenceModel(List<CrossReferenceModel> crossReferenceModel) {
		this.crossReferenceModel = crossReferenceModel;
	}
}


