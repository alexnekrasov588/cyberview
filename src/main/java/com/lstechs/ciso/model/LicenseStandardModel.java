package com.lstechs.ciso.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "LicenseStandard")
@EntityListeners(AuditingEntityListener.class)
public class LicenseStandardModel extends CRUDModel {

	@Id
	@Column(name = "lcnsStdId", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int lcnsStdId;

	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "lcnsId", nullable = false)
	private LicenseModel lcnsId;

	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "stdId", nullable = false)
	private StandardModel stdId;

	public LicenseStandardModel(LicenseModel lcnsId, StandardModel stdId) {
		this.lcnsId = lcnsId;
		this.stdId = stdId;
	}

	public LicenseStandardModel() {
	}

	public int getLcnsStdId() {
		return lcnsStdId;
	}

	public void setLcnsStdId(int lcnsStdId) {
		this.lcnsStdId = lcnsStdId;
	}

	@JsonIgnore
	public LicenseModel getLcnsId() {
		return lcnsId;
	}

	@JsonProperty
	public void setLcnsId(LicenseModel lcnsId) {
		this.lcnsId = lcnsId;
	}

	public StandardModel getStdId() {
		return stdId;
	}

	public void setStdId(StandardModel stdId) {
		this.stdId = stdId;
	}
}
