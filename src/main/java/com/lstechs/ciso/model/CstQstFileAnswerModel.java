package com.lstechs.ciso.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "CstQstFileAnswer")
public class CstQstFileAnswerModel extends CRUDModel {

	@Id
	@Column(name = "cstQstFileAnswerId", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int cstQstFileAnswerId;

	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@NotNull(groups = {CRUDCreation.class})
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "customerStandardId", nullable = false)
	private CustomerStandardModel customerStandardId;

	@Column(name = "fileName")
	private String fileName;

	@Column(name = "fileUrl")
	private String fileUrl;

	public CstQstFileAnswerModel(String fileName, String fileUrl, CustomerStandardModel customerStandardId) {
		this.fileName = fileName;
		this.fileUrl = fileUrl;
		this.customerStandardId = customerStandardId;
	}

	public CstQstFileAnswerModel() {
	}

	public int getCstQstFileAnswerId() {
		return cstQstFileAnswerId;
	}

	public void setCstQstFileAnswerId(int cstQstFileAnswerId) {
		this.cstQstFileAnswerId = cstQstFileAnswerId;
	}

	public CustomerStandardModel getCustomerStandardId() {
		return customerStandardId;
	}

	public void setCustomerStandardId(CustomerStandardModel customerStandardId) {
		this.customerStandardId = customerStandardId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileUrl() {
		return fileUrl;
	}

	public void setFileUrl(String fileUrl) {
		this.fileUrl = fileUrl;
	}
}
