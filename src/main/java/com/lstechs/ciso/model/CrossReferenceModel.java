package com.lstechs.ciso.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "CrossReference")
@EntityListeners(AuditingEntityListener.class)
public class CrossReferenceModel extends CRUDModel {

	@Id
	@Column(name = "crsRefId", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int crsRefId;

	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cstRqrId", nullable = true)
	private CustomRequirementModel cstRqrId;

	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "stdRqrId", nullable = false)
	private StdRequirementsModel stdRqrId;

	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@NotNull(groups = {CRUDCreation.class, CRUDUpdate.class})
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "stdId", nullable = false)
	private StandardModel stdId;

	public CrossReferenceModel(StdRequirementsModel stdRqrId, CustomRequirementModel cstRqrId, StandardModel stdId) {
		super();
		this.stdRqrId = stdRqrId;
		this.cstRqrId = cstRqrId;
		this.stdId = stdId;
	}

	public CrossReferenceModel() {
		super();
	}

	public int getCrsRefId() {
		return crsRefId;
	}

	public void setCrsRefId(int crsRefId) {
		this.crsRefId = crsRefId;
	}

	public StdRequirementsModel getStdRqrId() {
		return stdRqrId;
	}

	public void setStdRqrId(StdRequirementsModel stdRqrId) {
		this.stdRqrId = stdRqrId;
	}

	@JsonIgnore
	public CustomRequirementModel getCstRqrID() {
		return cstRqrId;
	}

	@JsonProperty
	public void setCstRqrId(CustomRequirementModel cstRqrId) {
		this.cstRqrId = cstRqrId;
	}

	public StandardModel getStdId() {
		return stdId;
	}

	public void setStdId(StandardModel stdId) {
		this.stdId = stdId;
	}
}
