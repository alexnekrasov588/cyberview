package com.lstechs.ciso.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "VendorStandardHeaderTitles")
@EntityListeners(AuditingEntityListener.class)
public class VendorStandardHeaderTitlesModel extends CRUDModel {

	@Id
	@Column(name = "standardHeaderTitlesId", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int standardHeaderTitlesId;

	@NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "requirementTitle", nullable = false)
	private String requirementTitle = "Requirement";

	@NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "descriptionTitle", nullable = false)
	private String descriptionTitle = "Description";

	@NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "explanationTitle", nullable = false)
	private String explanationTitle = "Explanation";

	@NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "articleNumberTitle", nullable = false)
	private String articleNumberTitle = "Article Number";

	@NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "genericAnswer1Title", nullable = false)
	private String genericAnswer1Title = "Generic Answer 1";

	@NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "genericAnswer2Title", nullable = false)
	private String genericAnswer2Title = "Generic Answer 2";

	@Column(name = "documentationTitle")
	private String documentationTitle = "Documentation";

	@Column(name = "complianceTitle")
	private String complianceTitle = "Compliance";

	@Column(name = "remarkTitle")
	private String remarkTitle = "Remark";

	@Column(name = "taskTitle")
	private String taskTitle = "Task";

	@JsonIgnore
	@OneToOne
	@JoinColumn(name = "standardId")
	private StandardModel standardId;

	public VendorStandardHeaderTitlesModel() {
	}

	public VendorStandardHeaderTitlesModel(String requirementTitle, String descriptionTitle, String explanationTitle, String articleNumberTitle, String genericAnswer1Title, String genericAnswer2Title, String documentationTitle, String complianceTitle, String remarkTitle, String taskTitle) {
		this.requirementTitle = requirementTitle;
		this.descriptionTitle = descriptionTitle;
		this.explanationTitle = explanationTitle;
		this.articleNumberTitle = articleNumberTitle;
		this.genericAnswer1Title = genericAnswer1Title;
		this.genericAnswer2Title = genericAnswer2Title;
		this.documentationTitle = documentationTitle;
		this.complianceTitle = complianceTitle;
		this.remarkTitle = remarkTitle;
		this.taskTitle = taskTitle;
	}

	public String getRequirementTitle() {
		return requirementTitle;
	}

	public void setRequirementTitle(String requirementTitle) {
		this.requirementTitle = requirementTitle;
	}

	public String getDescriptionTitle() {
		return descriptionTitle;
	}

	public void setDescriptionTitle(String descriptionTitle) {
		this.descriptionTitle = descriptionTitle;
	}

	public String getExplanationTitle() {
		return explanationTitle;
	}

	public void setExplanationTitle(String explanationTitle) {
		this.explanationTitle = explanationTitle;
	}

	public String getArticleNumberTitle() {
		return articleNumberTitle;
	}

	public void setArticleNumberTitle(String articleNumberTitle) {
		this.articleNumberTitle = articleNumberTitle;
	}

	public String getGenericAnswer1Title() {
		return genericAnswer1Title;
	}

	public void setGenericAnswer1Title(String genericAnswer1Title) {
		this.genericAnswer1Title = genericAnswer1Title;
	}

	public String getGenericAnswer2Title() {
		return genericAnswer2Title;
	}

	public void setGenericAnswer2Title(String genericAnswer2Title) {
		this.genericAnswer2Title = genericAnswer2Title;
	}

	public String getDocumentationTitle() {
		return documentationTitle;
	}

	public void setDocumentationTitle(String documentationTitle) {
		this.documentationTitle = documentationTitle;
	}

	public String getComplianceTitle() {
		return complianceTitle;
	}

	public void setComplianceTitle(String complianceTitle) {
		this.complianceTitle = complianceTitle;
	}

	public String getRemarkTitle() {
		return remarkTitle;
	}

	public void setRemarkTitle(String remarkTitle) {
		this.remarkTitle = remarkTitle;
	}

	public String getTaskTitle() {
		return taskTitle;
	}

	public void setTaskTitle(String taskTitle) {
		this.taskTitle = taskTitle;
	}

	public int getStandardHeaderTitlesId() { return standardHeaderTitlesId; }

	public void setStandardHeaderTitlesId(int standardHeaderTitlesId) { this.standardHeaderTitlesId = standardHeaderTitlesId; }

	public StandardModel getStandardId() { return standardId; }

	public void setStandardId(StandardModel standardId) { this.standardId = standardId; }
}
