package com.lstechs.ciso.model;

public class SingleCompanyModel {
	private int cmpId;
	private String cmpName;
	private String cmpLogoUrl;
	private String cmpSiteName;
	private String cmpSupportEmail;
	private int cmpLcnsId;
	private int cmpDataRetentionPeriod;

	public SingleCompanyModel(int cmpId, String cmpName, String cmpLogoUrl, String cmpSiteName, String cmpSupportEmail, int cmpLcnsId, int cmpDataRetentionPeriod) {
		this.cmpId = cmpId;
		this.cmpName = cmpName;
		this.cmpLogoUrl = cmpLogoUrl;
		this.cmpSiteName = cmpSiteName;
		this.cmpSupportEmail = cmpSupportEmail;
		this.cmpLcnsId = cmpLcnsId;
		this.cmpDataRetentionPeriod = cmpDataRetentionPeriod;
	}

	public SingleCompanyModel(CompanyModel companyModel) {
		this.cmpId = companyModel.getCmpId();
		this.cmpName = companyModel.getCmpName();
		this.cmpLogoUrl = companyModel.getCmpLogoUrl();
		this.cmpSiteName = companyModel.getCmpSiteName();
		this.cmpSupportEmail = companyModel.getCmpSupportEmail();
		if (companyModel.getCmpLcnsId() != null)
			this.cmpLcnsId = companyModel.getCmpLcnsId().getLcnsId();
		this.cmpDataRetentionPeriod = companyModel.getCmpDataRetentionPeriod();
	}

	public int getCmpId() {
		return cmpId;
	}

	public void setCmpId(int cmpId) {
		this.cmpId = cmpId;
	}

	public String getCmpName() {
		return cmpName;
	}

	public void setCmpName(String cmpName) {
		this.cmpName = cmpName;
	}

	public String getCmpLogoUrl() {
		return cmpLogoUrl;
	}

	public void setCmpLogoUrl(String cmpLogoUrl) {
		this.cmpLogoUrl = cmpLogoUrl;
	}

	public String getCmpSiteName() {
		return cmpSiteName;
	}

	public void setCmpSiteName(String cmpSiteName) {
		this.cmpSiteName = cmpSiteName;
	}

	public String getCmpSupportEmail() {
		return cmpSupportEmail;
	}

	public void setCmpSupportEmail(String cmpSupportEmail) {
		this.cmpSupportEmail = cmpSupportEmail;
	}

	public int getCmpLcnsId() {
		return cmpLcnsId;
	}

	public void setCmpLcnsId(int cmpLcnsId) {
		this.cmpLcnsId = cmpLcnsId;
	}

	public int getCmpDataRetentionPeriod() {
		return cmpDataRetentionPeriod;
	}

	public void setCmpDataRetentionPeriod(int cmpDataRetentionPeriod) {
		this.cmpDataRetentionPeriod = cmpDataRetentionPeriod;
	}
}
