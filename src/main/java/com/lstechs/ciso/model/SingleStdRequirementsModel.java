package com.lstechs.ciso.model;

public class SingleStdRequirementsModel {

	private int stdRqrId;
	private String articleNum;
	private String questionnaireName;


	public SingleStdRequirementsModel(int stdRqrId, String articleNum,String questionnaireName) {
		this.stdRqrId = stdRqrId;
		this.articleNum = articleNum;
		this.questionnaireName = questionnaireName;
	}

	public SingleStdRequirementsModel(StdRequirementsModel stdRequirementsModel,String questionnaireName) {
		this.stdRqrId = stdRequirementsModel.getStdRqrId();
		this.articleNum = stdRequirementsModel.getArticleNum();
		this.questionnaireName = questionnaireName;
	}

	public int getStdRqrId() {
		return stdRqrId;
	}

	public void setStdRqrId(int stdRqrId) {
		this.stdRqrId = stdRqrId;
	}

	public String getArticleNum() {
		return articleNum;
	}

	public void setArticleNum(String articleNum) {
		this.articleNum = articleNum;
	}

	public String getQuestionnaireName() {
		return questionnaireName;
	}

	public void setQuestionnaireName(String questionnaireName) {
		this.questionnaireName = questionnaireName;
	}
}
