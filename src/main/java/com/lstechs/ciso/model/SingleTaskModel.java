package com.lstechs.ciso.model;
import com.lstechs.ciso.enums.StatusEnum;
import java.util.Date;

public class SingleTaskModel {
	private int tskId;
	private String tskName;
	private String tskDescription;
	private SingleCompanyUserModel tskAssignee;
	private int tskPrjID;
	private Date tskStartDate;
	private Date tskEndDate;
	private int tskPriority;
	private StatusEnum status;

	public SingleTaskModel(int tskId, String tskName, String tskDescription, SingleCompanyUserModel tskAssignee, int tskPrjID, Date tskStartDate, Date tskEndDate, int tskPriority, StatusEnum status) {
		this.tskId = tskId;
		this.tskName = tskName;
		this.tskDescription = tskDescription;
		this.tskAssignee = tskAssignee;
		this.tskPrjID = tskPrjID;
		this.tskStartDate = tskStartDate;
		this.tskEndDate = tskEndDate;
		this.tskPriority = tskPriority;
		this.status = status;
	}
	public SingleTaskModel(TaskModel taskModel,SingleCompanyUserModel singleCompanyUserModel) {
		this.tskId = taskModel.getTskId();
		this.tskName = taskModel.getTskName();
		this.tskDescription = taskModel.getTskDescription();
		this.tskAssignee = singleCompanyUserModel;
		this.tskPrjID = taskModel.getTskPrjID().getPrjId();
		this.tskStartDate = taskModel.getTskStartDate();
		this.tskEndDate = taskModel.getTskEndDate();
		this.tskPriority = taskModel.getTskPriority();
		this.status = taskModel.getStatus();
	}

	public int getTskId() {
		return tskId;
	}

	public void setTskId(int tskId) {
		this.tskId = tskId;
	}

	public String getTskName() {
		return tskName;
	}

	public void setTskName(String tskName) {
		this.tskName = tskName;
	}

	public String getTskDescription() {
		return tskDescription;
	}

	public void setTskDescription(String tskDescription) {
		this.tskDescription = tskDescription;
	}

	public SingleCompanyUserModel getTskAssignee() {
		return tskAssignee;
	}

	public void setTskAssignee(SingleCompanyUserModel tskAssignee) {
		this.tskAssignee = tskAssignee;
	}

	public int getTskPrjID() {
		return tskPrjID;
	}

	public void setTskPrjID(int tskPrjID) {
		this.tskPrjID = tskPrjID;
	}

	public Date getTskStartDate() {
		return tskStartDate;
	}

	public void setTskStartDate(Date tskStartDate) {
		this.tskStartDate = tskStartDate;
	}

	public Date getTskEndDate() {
		return tskEndDate;
	}

	public void setTskEndDate(Date tskEndDate) {
		this.tskEndDate = tskEndDate;
	}

	public int getTskPriority() {
		return tskPriority;
	}

	public void setTskPriority(int tskPriority) {
		this.tskPriority = tskPriority;
	}

	public StatusEnum getStatus() {
		return status;
	}

	public void setStatus(StatusEnum status) {
		this.status = status;
	}
}
