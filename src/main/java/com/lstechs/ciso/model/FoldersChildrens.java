package com.lstechs.ciso.model;

import java.util.List;

public class FoldersChildrens {
	private String type;
	private String name;
	private int newsId;
	private int folderId;
	private List<FoldersChildrens> childrens;
	
	public FoldersChildrens() {
		super();
	}

	
	
	public FoldersChildrens(String type, String name,int newsId,int folderId, List<FoldersChildrens> childrens) {
		super();
		this.type = type;
		this.name = name;
		this.newsId = newsId;
		this.folderId = folderId;
		this.childrens = childrens;
	}
	
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<FoldersChildrens> getChildrens() {
		return childrens;
	}
	public void setChildrens(List<FoldersChildrens> childrens) {
		this.childrens = childrens;
	}



	public int getNewsId() {
		return newsId;
	}



	public void setNewsId(int newsId) {
		this.newsId = newsId;
	}



	public int getFolderId() {
		return folderId;
	}



	public void setFolderId(int folderId) {
		this.folderId = folderId;
	}
	
	
	
	
}


// const structure = [
//     {
//       type: "folder",
//       name: "src",
//       childrens: [
//         {
//           type: "folder",
//           name: "Components",
//           childrens: [
//             { type: "file", name: "Modal.js" },
//             { type: "file", name: "Modal.css" }
//           ]
//         },
//         { type: "file", name: "index.js" },
//         { type: "file", name: "index.html" }
//       ]
//     },
//     { type: "file", name: "package.json" }
//   ];