package com.lstechs.ciso.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.lstechs.ciso.enums.FolderTypeEnum;

@Entity
@Table(name = "FavouriteFolders")
@EntityListeners(AuditingEntityListener.class)
public class FavouriteFoldersModel extends CRUDModel {

	@Id
    @Column(name = "favId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int favId;

	@NotEmpty(groups = {CRUDCreation.class, CRUDUpdate.class})
	@Column(name = "favName", nullable = false)
	private String favName;

	@Column(name = "favType", nullable = false)
	private FolderTypeEnum favType;

    @NotNull(groups = { CRUDCreation.class, CRUDUpdate.class })
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cmpUserId",nullable = false)
    private CompanyUserModel cmpUserId;

	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@ManyToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "favParent", nullable = true)
	private FavouriteFoldersModel parent;

	@JsonIgnore
	@OneToMany(mappedBy = "folder")
	private List<FavouriteCVEModel> favouriteCVEModels;

	@JsonIgnore
	@OneToMany(mappedBy = "folder")
	private List<FavouriteNewsModel> favouriteNewsModels;

	public FavouriteFoldersModel(String favName,FolderTypeEnum favType,FavouriteFoldersModel parent,CompanyUserModel cmpUserId) {
		super();
		this.favName = favName;
		this.favType = favType;
		this.parent = parent;
		this.cmpUserId = cmpUserId;
	}

	public FavouriteFoldersModel() {
		super();
	}

	public int getFavId() {
		return favId;
	}

	public void setFavId(int favId) {
		this.favId = favId;
	}

	public String getFavName() {
		return favName;
	}

	public void setFavName(String favName) {
		this.favName = favName;
	}

	public FolderTypeEnum getFavType() {
		return favType;
	}

	public void setFavType(FolderTypeEnum favType) {
		this.favType = favType;
	}


	public FavouriteFoldersModel getParent() {
		return parent;
	}

	public void setParent(FavouriteFoldersModel parent) {
		this.parent = parent;
	}

	public List<FavouriteCVEModel> getFavouriteCVEModels() {
		return favouriteCVEModels;
	}

	public void setFavouriteCVEModels(List<FavouriteCVEModel> favouriteCVEModels) {
		this.favouriteCVEModels = favouriteCVEModels;
	}

	public List<FavouriteNewsModel> getFavouriteNewsModels() {
		return favouriteNewsModels;
	}

	public void setFavouriteNewsModels(List<FavouriteNewsModel> favouriteNewsModels) {
		this.favouriteNewsModels = favouriteNewsModels;
	}

	public CompanyUserModel getCmpUserId() {
		return cmpUserId;
	}

	public void setCmpUserId(CompanyUserModel cmpUserId) {
		this.cmpUserId = cmpUserId;
	}




}
