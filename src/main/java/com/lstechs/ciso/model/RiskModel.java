package com.lstechs.ciso.model;

public class RiskModel {
	private int initToOrg;
	private int initToData;
	private int afterToOrg;
	private int afterToData;
	
	
	
	public RiskModel() {
		super();
	}
	public RiskModel(int initToOrg, int initToData, int afterToOrg, int afterToData) {
		super();
		this.initToOrg = initToOrg;
		this.initToData = initToData;
		this.afterToOrg = afterToOrg;
		this.afterToData = afterToData;
	}
	public int getInitToOrg() {
		return initToOrg;
	}
	public void setInitToOrg(int initToOrg) {
		this.initToOrg = initToOrg;
	}
	public int getInitToData() {
		return initToData;
	}
	public void setInitToData(int initToData) {
		this.initToData = initToData;
	}
	public int getAfterToOrg() {
		return afterToOrg;
	}
	public void setAfterToOrg(int afterToOrg) {
		this.afterToOrg = afterToOrg;
	}
	public int getAfterToData() {
		return afterToData;
	}
	public void setAfterToData(int afterToData) {
		this.afterToData = afterToData;
	}
	
	
}
