package com.lstechs.ciso.model;

import com.lstechs.ciso.enums.ModuleEnum;

public class SingleModuleTaskModel {

    private int mdlTskId;
    private int tskId;
    private ModuleEnum module;
    private int mdlTskIdInternal;

    public SingleModuleTaskModel(int mdlTskId, int tskId, ModuleEnum module, int mdlTskIdInternal) {
        this.mdlTskId = mdlTskId;
        this.module = module;
        this.mdlTskIdInternal = mdlTskIdInternal;
    }

    public SingleModuleTaskModel(ModuleTaskModel moduleTaskModel, TaskModel tskId) {
        this.mdlTskId = moduleTaskModel.getMdlTskId();
        this.tskId = tskId.getTskId();
        this.module = moduleTaskModel.getModule();
        this.mdlTskIdInternal = moduleTaskModel.getMdlTskIdInternal();
    }

    public int getMdlTskId() {
        return mdlTskId;
    }

    public void setMdlTskId(int mdlTskId) {
        this.mdlTskId = mdlTskId;
    }

    public int getTskId() {
        return tskId;
    }

    public void setTskId(int tskId) {
        this.tskId = tskId;
    }

    public ModuleEnum getModule() {
        return module;
    }

    public void setModule(ModuleEnum module) {
        this.module = module;
    }

    public int getMdlTskIdInternal() {
        return mdlTskIdInternal;
    }

    public void setMdlTskIdInternal(int mdlTskIdInternal) {
        this.mdlTskIdInternal = mdlTskIdInternal;
    }

}
