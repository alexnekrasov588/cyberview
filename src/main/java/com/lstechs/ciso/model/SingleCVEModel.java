package com.lstechs.ciso.model;

import java.util.Date;

public class SingleCVEModel {
	private int cveId;
	private int cveSrcId;
	private String cveTitle;
	private String cveDescription;
	private String cveUrl;
	private String cveHash;
	private boolean cveIsAvilable;
	private Date publishedDate;

	public SingleCVEModel(int cveId, int cveSrcId, String cveTitle, String cveDescription, String cveUrl, String cveHash, boolean cveIsAvilable, Date publishedDate) {
		this.cveId = cveId;
		this.cveSrcId = cveSrcId;
		this.cveTitle = cveTitle;
		this.cveDescription = cveDescription;
		this.cveUrl = cveUrl;
		this.cveHash = cveHash;
		this.cveIsAvilable = cveIsAvilable;
		this.publishedDate = publishedDate;
	}

	public SingleCVEModel(CVEModel cveModel) {
		this.cveId = cveModel.getCveId();
		this.cveSrcId = cveModel.getCveSrcId().getCveSrcId();
		this.cveTitle = cveModel.getCveTitle();
		this.cveDescription = cveModel.getCveDescription();
		this.cveUrl = cveModel.getCveUrl();
		this.cveHash = cveModel.getCveHash();
		this.cveIsAvilable = cveModel.isCveIsAvilable();
		this.publishedDate = cveModel.getPublishedDate();
	}

	public int getCveId() {
		return cveId;
	}

	public void setCveId(int cveId) {
		this.cveId = cveId;
	}

	public int getCveSrcId() {
		return cveSrcId;
	}

	public void setCveSrcId(int cveSrcId) {
		this.cveSrcId = cveSrcId;
	}

	public String getCveTitle() {
		return cveTitle;
	}

	public void setCveTitle(String cveTitle) {
		this.cveTitle = cveTitle;
	}

	public String getCveDescription() {
		return cveDescription;
	}

	public void setCveDescription(String cveDescription) {
		this.cveDescription = cveDescription;
	}

	public String getCveUrl() {
		return cveUrl;
	}

	public void setCveUrl(String cveUrl) {
		this.cveUrl = cveUrl;
	}

	public String getCveHash() {
		return cveHash;
	}

	public void setCveHash(String cveHash) {
		this.cveHash = cveHash;
	}

	public boolean isCveIsAvilable() {
		return cveIsAvilable;
	}

	public void setCveIsAvilable(boolean cveIsAvilable) {
		this.cveIsAvilable = cveIsAvilable;
	}

	public Date getPublishedDate() {
		return publishedDate;
	}

	public void setPublishedDate(Date publishedDate) {
		this.publishedDate = publishedDate;
	}
}
