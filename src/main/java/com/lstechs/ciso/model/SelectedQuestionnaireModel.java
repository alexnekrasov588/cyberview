package com.lstechs.ciso.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "SelectedQuestionnaire")
@EntityListeners(AuditingEntityListener.class)
public class SelectedQuestionnaireModel extends CRUDModel {

	@Id
	@Column(name = "selectedQuestionnaireId", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int selectedQuestionnaireId;

	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@NotNull(groups = {CRUDModel.CRUDCreation.class})
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "roleId", nullable = false)
	private RoleModel roleId;

	@NotEmpty(groups = {CRUDModel.CRUDCreation.class, CRUDModel.CRUDUpdate.class})
	@Column(name = "questionnaireId", nullable = false)
	private int questionnaireId;

	@NotEmpty(groups = {CRUDModel.CRUDCreation.class, CRUDModel.CRUDUpdate.class})
	@Column(name = "standardId", nullable = false)
	private int standardId;

	public SelectedQuestionnaireModel() {
	}

	public SelectedQuestionnaireModel(int questionnaireId, RoleModel roleId, int standardId) {
		this.questionnaireId = questionnaireId;
		this.roleId = roleId;
		this.standardId = standardId;
	}

	public int getQuestionnaireId() {
		return questionnaireId;
	}

	public void setQuestionnaireId(int questionnaireId) {
		this.questionnaireId = questionnaireId;
	}

	public RoleModel getRoleId() {
		return roleId;
	}

	public void setRoleId(RoleModel roleId) {
		this.roleId = roleId;
	}

	public int getStandardId() {
		return standardId;
	}

	public void setStandardId(int standardId) {
		this.standardId = standardId;
	}
}
