package com.lstechs.ciso.config;

import com.lstechs.ciso.model.LdapConnectionModel;
import com.lstechs.ciso.model.VendorUserModel;
import com.lstechs.ciso.service.LdapConnectionService;
import com.lstechs.ciso.service.VendorUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import java.util.List;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

	@Qualifier("authenticationService")
	@Autowired
	private UserDetailsService jwtUserDetailsService;

	@Autowired
	private JwtRequestFilter jwtRequestFilter;

	@Autowired
	private LdapConnectionService ldapConnectionService;

	@Autowired
	private VendorUserService vendorUserService;

	@Value("${onPrem}")
	private boolean onPrem;

	@Value("${vendor.username}")
	private String username;

	@Value("${vendor.password}")
	private String password;

	@Value("${cors.allowed-origins}")
	private String corsAllowedOrigins;

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		// configure AuthenticationManager so that it knows from where to load
		// user for matching credentials
		// Use BCryptPasswordEncoder
		auth.userDetailsService(jwtUserDetailsService).passwordEncoder(passwordEncoder());
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity.csrf().disable()
			// Don't authenticate this particular request
			.authorizeRequests().antMatchers("/auth/login","/auth/test", "/auth/login/vendor", "/auth/isResetTokenValid",
					"/license/onPrem","/sync/test",
					"/auth/forgotPasswordRequest", "/auth/forgotPasswordRequest/vendor","/news/sync/{\\d+}","/cve/sync/{\\d+}","/standard/sync/{\\d+}",
					"/threat/sync/{\\d+}","/sync/syncAll","sync/news","sync/cve","/auth/resetPassword", "/vendorUser",
					"/newsSources/sync/{\\d+}","/cveSources/sync/{\\d+}",
					"/templates/syncStd","/templates/syncQuestionaire","/templates/syncSections","/templates/syncRequirements","/templates/syncQuestions","/templates/syncOptions","/templates/syncAnswer1Options","/templates/syncAnswer2Options").permitAll()
			.antMatchers(HttpMethod.OPTIONS).permitAll().
			// all other requests need to be authenticated
				anyRequest().authenticated().and().
			// make sure we use stateless session; session won't be used to
			// store user's state.
				exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint).and().sessionManagement()
			.sessionCreationPolicy(SessionCreationPolicy.STATELESS);

		// Add a filter to validate the tokens with every request
		httpSecurity.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
	}

	@Override
	protected void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
		if (onPrem) {
			List<LdapConnectionModel> ldapConnectionModels = ldapConnectionService.getAll();
			if (ldapConnectionModels != null && ldapConnectionModels.size() > 0) {
				LdapConnectionModel ldapConnectionModel = ldapConnectionModels.get(0);
				authenticationManagerBuilder
					.ldapAuthentication()
					.contextSource()
					.url(ldapConnectionModel.getFullUrl())
					.managerDn(ldapConnectionModel.getLdapManagerDn())
					.managerPassword(ldapConnectionModel.getLdapManagerPassword())
					.and()
					.userSearchBase(ldapConnectionModel.getLdapUserSearchBase())
					.userSearchFilter(ldapConnectionModel.getLdapUserSearchFilter());
				ldapConnectionService.isReadyToConnect = true;
			} else {
				List<VendorUserModel> vendorUserModels = vendorUserService.getAll();
				if (vendorUserModels != null && vendorUserModels.size() == 0) {
					VendorUserModel vendorUserModel = new VendorUserModel("ldap vendor", password, username);
					vendorUserService.create(vendorUserModel);
				}
				authenticationManagerBuilder.userDetailsService(jwtUserDetailsService);
				ldapConnectionService.isReadyToConnect = false;
			}
//			authenticationManagerBuilder.ldapAuthentication()
//			.contextSource().url("ldap://3.19.30.29:389/dc=ciso,dc=com")
//			.managerDn("cn=Manager,dc=ciso,dc=com").managerPassword("123456")
//			.and()
//			.userSearchBase("ou=users")
//			.userSearchFilter("(cn={0})");
		} else {
			authenticationManagerBuilder.userDetailsService(jwtUserDetailsService);
		}
	}

}
