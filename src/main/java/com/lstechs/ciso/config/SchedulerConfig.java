package com.lstechs.ciso.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

/**
 * Configures the scheduler to allow multiple concurrent pools.
 * Prevents blocking.
 * for more details see: https://crmepham.github.io/spring-boot-multi-thread-scheduling/
 */
@Configuration
@EnableScheduling
public class SchedulerConfig implements SchedulingConfigurer
{
	/**
	 * The pool size.
	 */
	private final int POOL_SIZE = 15;

	/**
	 * Configures the scheduler to allow multiple pools.
	 *
	 * @param taskRegistrar The task registrar.
	 */
	@Override
	public void configureTasks(ScheduledTaskRegistrar taskRegistrar)
	{
		ThreadPoolTaskScheduler threadPoolTaskScheduler = new ThreadPoolTaskScheduler();

		threadPoolTaskScheduler.setPoolSize(POOL_SIZE);
		threadPoolTaskScheduler.setThreadNamePrefix("scheduled-task-pool-");
		threadPoolTaskScheduler.initialize();

		taskRegistrar.setTaskScheduler(threadPoolTaskScheduler);
	}
}
