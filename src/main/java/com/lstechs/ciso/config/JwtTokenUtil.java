package com.lstechs.ciso.config;

import com.lstechs.ciso.enums.UsersEnum;
import com.lstechs.ciso.model.CompanyModel;
import com.lstechs.ciso.model.LicenseModel;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Component
public class JwtTokenUtil implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 3497621302196899482L;

	@Value("${jwtExpires}")
	private int expires;

	@Value("${jwt.secret}")
	private String secret;

	@Value("${onPrem}")
	private boolean onPrem;

	public int getExpiresAt() {
		if (expires != 0)
			return expires * 60 * 60 * 1000;
		else return 5 * 60 * 60 * 1000;
	}


	//retrieve username from jwt token
	public String getUsernameFromToken(String token) {
		return getClaimFromToken(token, Claims::getSubject);
	}

	//retrieve expiration date from jwt token
	public Date getExpirationDateFromToken(String token) {
		return getClaimFromToken(token, Claims::getExpiration);
	}


	public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
		final Claims claims = getAllClaimsFromToken(token);
		return claimsResolver.apply(claims);
	}

	//for retrieveing any information from token we will need the secret key
	public Claims getAllClaimsFromToken(String token) {
		return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
	}

	//check if the token has expired
	public Boolean isTokenExpired(String token) {
		final Date expiration = getExpirationDateFromToken(token);
		return expiration.before(new Date());
	}

	//generate token for user
	public String generateToken(String email, Object object, UsersEnum type) {
		Map<String, Object> claims = new HashMap<>();
		claims.put("user", object);
		if (onPrem) claims.put("isLDAP", true);
		else claims.put("isLDAP", false);
		claims.put("type", type);
		return doGenerateToken(claims, email);
	}

	public String generateTempToken(String email) {
		Map<String, Object> claims = new HashMap<>();
		return doGenerateToken(claims, email);
	}

	public String generateLicenseToken(LicenseModel licenseModel, CompanyModel companyModel) {
		Map<String, Object> claims = new HashMap<>();
		claims.put("License", licenseModel);
		claims.put("Company", companyModel);
		return doGenerateToken(claims, licenseModel.getLcnsToken());
	}

	//while creating the token -
	//1. Define  claims of the token, like Issuer, Expiration, Subject, and the ID
	//2. Sign the JWT using the HS512 algorithm and secret key.
	//3. According to JWS Compact Serialization(https://tools.ietf.org/html/draft-ietf-jose-json-web-signature-41#section-3.1)
	//   compaction of the JWT to a URL-safe string
	private String doGenerateToken(Map<String, Object> claims, String subject) {

		return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
			.setExpiration(new Date(System.currentTimeMillis() + getExpiresAt()))
			.signWith(SignatureAlgorithm.HS512, secret).compact();
	}

	//validate token
	public Boolean validateToken(String token, UserDetails userDetails) {
		final String username = getUsernameFromToken(token);
		return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
	}
}
