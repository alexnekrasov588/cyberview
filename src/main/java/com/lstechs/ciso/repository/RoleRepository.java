package com.lstechs.ciso.repository;

import com.lstechs.ciso.model.RoleModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleRepository extends CRUDRepository<RoleModel> {

	@Query("SELECT roleModel FROM RoleModel  roleModel WHERE roleModel.cmpId.cmpId =(:cmpId) AND roleModel.deletedAt = NULL AND lower(roleModel.roleName) LIKE %:query% ")
	Page<RoleModel> findAllRolesByCmpId(Pageable pageable, @Param("query") String query, @Param("cmpId") int cmpId);

	@Query("SELECT roleModel FROM RoleModel  roleModel WHERE roleModel.cmpId.cmpId =(:cmpId) AND roleModel.deletedAt = NULL")
	List<RoleModel> getAllRolesByCmpId(@Param("cmpId") int cmpId);

	@Query("SELECT roleModel FROM RoleModel  roleModel WHERE lower(roleModel.roleName) LIKE %:roleName% AND roleModel.deletedAt = NULL")
	List<RoleModel> getRolesBySpecificName(@Param("roleName") String roleName);

	@Query(value = "SELECT * \n" +
		"FROM   role \n" +
		"WHERE  role_id = (SELECT cmp_usr_role_id \n" +
		"                  FROM   company_user \n" +
		"                  WHERE  deleted_at IS NULL \n" +
		"                         AND cmp_usr_id = (:cmpUsrId)) ", nativeQuery = true)
	RoleModel getRoleModelByCmpUsrId(@Param("cmpUsrId") int cmpUsrId);

}
