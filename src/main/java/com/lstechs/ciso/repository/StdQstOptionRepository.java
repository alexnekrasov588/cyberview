package com.lstechs.ciso.repository;

import com.lstechs.ciso.model.StdQstOptionModel;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface StdQstOptionRepository extends CRUDRepository<StdQstOptionModel> {

	@Transactional
	@Modifying(clearAutomatically = true)
	@Query("UPDATE StdQstOptionModel stdQstOptionModel set stdQstOptionModel.deletedAt = CURRENT_TIMESTAMP  WHERE stdQstOptionModel.sqoQstId.qtrQstId=(:pId)")
	void deleteStdQstOptionModel(@Param("pId") int pId);

	@Query("SELECT stdQstOptionModel FROM StdQstOptionModel stdQstOptionModel WHERE stdQstOptionModel.deletedAt = NULL AND stdQstOptionModel.sqoQstId.qtrQstId IN(:ids) ")
	List<StdQstOptionModel> getOptionsListByQuestionsIds(@Param("ids") List<Integer> ids);
}
