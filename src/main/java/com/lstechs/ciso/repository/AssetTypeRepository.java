package com.lstechs.ciso.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.lstechs.ciso.enums.AssetTypeEnum;
import com.lstechs.ciso.model.AssetTypeModel;


@Repository
public interface AssetTypeRepository  extends CRUDRepository<AssetTypeModel> {
	

	@Query("SELECT assetTypeModel FROM AssetTypeModel assetTypeModel WHERE assetTypeModel.cmpId.cmpId = (:cmpId) AND assetTypeModel.deletedAt IS NULL"
			+ " AND lower(assetTypeModel.astTypeName) like lower(CONCAT('%',(:query),'%')) ")
	Page<AssetTypeModel> getAllAssetTypeByCmpId(Pageable pageable,@Param("cmpId") int cmpId,@Param("query") String query);
	
	
	@Query("SELECT assetTypeModel FROM AssetTypeModel assetTypeModel WHERE assetTypeModel.cmpId.cmpId = (:cmpId) "
			+ "AND assetTypeModel.deletedAt IS NULL AND "
			+ " ( (:type) IS NULL OR  assetTypeModel.astType =  (:type) )")
	List<AssetTypeModel> getAssetTypeByCmpId(@Param("cmpId") int cmpId,@Param("type") AssetTypeEnum type);
	
	@Query("SELECT assetTypeModel FROM AssetTypeModel assetTypeModel WHERE assetTypeModel.cmpId.cmpId = (:cmpId) "
			+ "AND assetTypeModel.deletedAt IS NULL AND lower(assetTypeModel.astTypeName) =  (:name) AND assetTypeModel.astType =  (:type)")
	List<AssetTypeModel> getAssetTypeByName(@Param("cmpId") int cmpId,@Param("name") String name,@Param("type") AssetTypeEnum type);


}