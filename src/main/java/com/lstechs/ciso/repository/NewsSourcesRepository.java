package com.lstechs.ciso.repository;

import com.lstechs.ciso.model.NewsSourcesModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NewsSourcesRepository extends CRUDRepository<NewsSourcesModel> {
	@Query("SELECT newsSourceModel FROM NewsSourcesModel newsSourceModel WHERE newsSourceModel.newsSrcIsActive = true AND newsSourceModel.deletedAt = NULL")
	List<NewsSourcesModel> findAll();

	@Query("SELECT newsSourceModel FROM NewsSourcesModel newsSourceModel WHERE  newsSourceModel.newsSrcOnPremId = (:pId) ORDER BY newsSourceModel.createdAt DESC")
	List<NewsSourcesModel> getOnPrem(@Param("pId") int pId);

}
