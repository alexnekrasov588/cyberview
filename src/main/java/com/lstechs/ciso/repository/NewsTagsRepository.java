package com.lstechs.ciso.repository;

import java.util.List;

import com.lstechs.ciso.model.NewsTagsModel;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface NewsTagsRepository extends CRUDRepository<NewsTagsModel> {

    @Query("SELECT newsTagsModel FROM NewsTagsModel newsTagsModel WHERE newsTagsModel.deletedAt = NULL AND (lower(newsTagsModel.newsTagName) LIKE %:query% OR lower(newsTagsModel.newsTagName) LIKE %:query% )")
	Page<NewsTagsModel> AutoComplete(Pageable pageable, @Param("query") String query);

    @Query("SELECT newsTagsModel FROM NewsTagsModel newsTagsModel WHERE newsTagsModel.deletedAt = NULL AND (lower(newsTagsModel.newsTagName) LIKE :tagName OR upper(newsTagsModel.newsTagName) LIKE :tagName )")
	List<NewsTagsModel> getByName(@Param("tagName") String tagName);

	@Query(value = "SELECT *\n" +
		"FROM news_tags\n" +
		"WHERE deleted_at IS NULL AND news_tag_id IN (\n" +
		"SELECT news_tag_id FROM news_articles_tags WHERE news_artc_id = :newsArtcId)\n"
		, nativeQuery = true)
	List<NewsTagsModel> getNewsTagsByNewsArticleId(@Param("newsArtcId") int newsArtcId);
}
