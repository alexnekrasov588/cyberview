package com.lstechs.ciso.repository;

import com.lstechs.ciso.model.UserProjectModel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserProjectRepository extends CRUDRepository<UserProjectModel> {
	@Query("SELECT userProjectModel FROM UserProjectModel userProjectModel WHERE userProjectModel.deletedAt = NULL AND userProjectModel.prjId.prjId =(:projId) ")
	List<UserProjectModel> getAllUsersByProjId( @Param("projId") int projId);
}
