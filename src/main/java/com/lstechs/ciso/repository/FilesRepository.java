package com.lstechs.ciso.repository;

import com.lstechs.ciso.model.FilesModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FilesRepository extends JpaRepository<FilesModel, String> {

	@Query("SELECT filesModel FROM FilesModel filesModel WHERE filesModel.id =(:fileId) ")
	FilesModel getById(@Param("fileId") String fileId);

	@Query("SELECT filesModel FROM FilesModel filesModel WHERE filesModel.deletedAt IS NULL AND filesModel.stdQtrId.stdQtrId =(:stdQtrId) ")
	List<FilesModel> getAllFilesModelByQuestionaireId(@Param("stdQtrId") int stdQtrId);

	@Query("SELECT filesModel FROM FilesModel filesModel WHERE filesModel.deletedAt IS NULL AND filesModel.cstStdId.cstStdId =(:cstStdId) ")
	List<FilesModel> getAllFilesModelByCustomerStandardId(@Param("cstStdId") int cstStdId);

	@Query("SELECT filesModel FROM FilesModel filesModel WHERE filesModel.deletedAt IS NULL AND filesModel.cstStdId.cmpId.cmpId =(:cmpId) ")
	List<FilesModel> getAllFilesModelByCompanyId(@Param("cmpId") int cmpId);

	@Query("SELECT filesModel FROM FilesModel filesModel WHERE filesModel.deletedAt IS NULL AND filesModel.cstStdId.cstStdId IN (:cstStdIds) ")
	List<FilesModel> getAllFilesModelByCustomerStandardIds(@Param("cstStdIds") List<Integer> cstStdIds);

	@Query("SELECT filesModel FROM FilesModel filesModel WHERE filesModel.deletedAt IS NULL AND filesModel.stdQtrId.stdQtrId IN (:stdQtrIds) ")
	List<FilesModel> getAllFilesModelByStdQtrIds(@Param("stdQtrIds") List<Integer> stdQtrIds);
}
