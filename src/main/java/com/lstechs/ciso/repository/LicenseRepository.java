package com.lstechs.ciso.repository;

import com.lstechs.ciso.model.LicenseModel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LicenseRepository extends CRUDRepository<LicenseModel> {

	@Query("SELECT licenseModel FROM LicenseModel licenseModel WHERE licenseModel.deletedAt = NULL")
	List<LicenseModel> getAllLicense();

	@Query("SELECT licenseModel FROM LicenseModel licenseModel WHERE licenseModel.deletedAt = NULL AND licenseModel.lcnsToken=(:token)")
	LicenseModel findLicenseByToken(@Param("token") String token);

	@Query("SELECT licenseModel.lcnsToken FROM LicenseModel licenseModel WHERE licenseModel.deletedAt = NULL AND licenseModel.lcnsId=(:lcnsId)")
	String findLicenseTokenByLcnsId(@Param("lcnsId") int lcnsId);

	@Query("SELECT licenseModel FROM LicenseModel licenseModel WHERE licenseModel.deletedAt = NULL AND licenseModel.lcnsId=(:lcnsId)")
	LicenseModel findLicenseByLcnsId(@Param("lcnsId") int lcnsId);

}
