package com.lstechs.ciso.repository;

import com.lstechs.ciso.model.SelectedStandardModel;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface SelectedStandardsRepository extends CRUDRepository<SelectedStandardModel> {

	@Query("SELECT selectedStandardModel FROM SelectedStandardModel selectedStandardModel WHERE selectedStandardModel.deletedAt IS NULL AND selectedStandardModel.roleId.roleId = (:roleId)")
	List<SelectedStandardModel> getSelectedStandardModelByRoleId(@Param("roleId") int roleId);

	@Transactional
	@Modifying(clearAutomatically = true)
	@Query("UPDATE SelectedStandardModel selectedStandardModel set selectedStandardModel.deletedAt = CURRENT_TIMESTAMP  WHERE selectedStandardModel.roleId.roleId = (:roleId)")
	void deleteSelectedStandardModel(@Param("roleId") int roleId);
}
