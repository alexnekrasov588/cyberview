package com.lstechs.ciso.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.lstechs.ciso.model.CRUDModel;

@NoRepositoryBean
public interface CRUDRepository<T extends CRUDModel> extends PagingAndSortingRepository<T, Integer> {

    @Query("SELECT gr FROM #{#entityName} gr WHERE gr.deletedAt = NULL")
    Page<T> findAll(Pageable pageable, @Param("query") String query);

	@Query("SELECT gr FROM #{#entityName} gr WHERE gr.deletedAt = NULL")
	List<T> findAll();

    @Override
    @Query("SELECT gr FROM #{#entityName} gr WHERE gr.id=(:pId) AND gr.deletedAt = NULL")
    Optional<T> findById(@Param("pId") Integer pId);

    @Query("SELECT gr FROM  #{#entityName} gr WHERE gr.updatedAt > (:updatedAt) AND gr.deletedAt = NULL ORDER BY gr.updatedAt ASC")
    Page<T> syncList(Pageable pageable,@Param("updatedAt") Date updatedAt);

	@Query("SELECT MAX(gr.updatedAt) FROM #{#entityName} gr WHERE gr.deletedAt = NULL")
	Date lastUpdateAt();

}
