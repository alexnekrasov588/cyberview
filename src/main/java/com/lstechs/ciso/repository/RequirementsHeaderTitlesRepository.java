package com.lstechs.ciso.repository;

import com.lstechs.ciso.model.RequirementsHeaderTitlesModel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface RequirementsHeaderTitlesRepository extends CRUDRepository<RequirementsHeaderTitlesModel> {

	@Query("SELECT requirementsHeaderTitlesModel FROM RequirementsHeaderTitlesModel requirementsHeaderTitlesModel WHERE requirementsHeaderTitlesModel.questionnaireId.stdQtrId =(:questionnaireId) AND requirementsHeaderTitlesModel.deletedAt IS NULL")
	RequirementsHeaderTitlesModel getRequirementsHeaderTitlesByQuestionnaireId(@Param("questionnaireId") int questionnaireId);
}
