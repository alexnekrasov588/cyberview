package com.lstechs.ciso.repository;

import com.lstechs.ciso.model.TaskCommentModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TaskCommentRepository extends CRUDRepository<TaskCommentModel>{

    @Query("SELECT taskCommentModel FROM TaskCommentModel taskCommentModel WHERE taskCommentModel.cmntTskId.tskId =(:cmmtId) AND taskCommentModel.deletedAt = NULL AND (lower(taskCommentModel.cmntText) LIKE %:query% )")
    Page<TaskCommentModel> findTasksComment(Pageable pageable, @Param("cmmtId") int cmmtId, @Param("query") String query);
}
