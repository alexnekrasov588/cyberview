package com.lstechs.ciso.repository;

import com.lstechs.ciso.model.PermissionModel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PermissionRepository extends CRUDRepository<PermissionModel> {

	@Query("SELECT permissionModel FROM PermissionModel  permissionModel WHERE permissionModel.roleId.roleId =(:id) AND permissionModel.deletedAt = NULL")
	List<PermissionModel> getPermissionByRoleId(@Param("id") int id);
}
