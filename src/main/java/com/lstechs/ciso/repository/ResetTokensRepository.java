package com.lstechs.ciso.repository;

import com.lstechs.ciso.model.ResetTokensModel;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Repository
public interface ResetTokensRepository extends PagingAndSortingRepository<ResetTokensModel, Integer> {

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("DELETE FROM ResetTokensModel r  WHERE   r.userId=(:pUserId) AND r.userType=(:pUserType)")
    void deleteDuplicate(@Param("pUserId") int pUserId, @Param("pUserType") String pUserType);

    @Query("SELECT resetTokensModel FROM ResetTokensModel resetTokensModel WHERE resetTokensModel.resetToken=(:pResetToken)")
    ResetTokensModel findByResetToken(@Param("pResetToken") String pResetToken);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("DELETE FROM ResetTokensModel r  WHERE r.expiresAt < (:pExpires)")
    void deleteExpires(@Param("pExpires") Date pExpires);

}
