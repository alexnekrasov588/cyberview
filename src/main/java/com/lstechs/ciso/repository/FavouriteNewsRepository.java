package com.lstechs.ciso.repository;

import com.lstechs.ciso.model.FavouriteNewsModel;
import com.lstechs.ciso.model.NewsArticlesModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FavouriteNewsRepository extends CRUDRepository<FavouriteNewsModel> {

    @Query("SELECT f FROM FavouriteNewsModel f WHERE f.deletedAt = NULL AND f.cmpUsrId.cmpUsrId=(:pCmpUsrId) ORDER BY f.createdAt DESC")
    List<FavouriteNewsModel> getNewsArticlesModelByCmpUsrId(@Param("pCmpUsrId") int pCmpUsrId);

	@Query("SELECT f FROM FavouriteNewsModel f WHERE f.deletedAt = NULL AND f.folder.favId=(:folderId) ORDER BY f.createdAt DESC")
	List<FavouriteNewsModel> getNewsArticlesModelByFolderId(@Param("folderId") int folderId);

    @Query("SELECT favouriteNewsModel FROM FavouriteNewsModel  favouriteNewsModel WHERE favouriteNewsModel.deletedAt = NULL AND favouriteNewsModel.newsArtcId.newsArtcId = (:artId) AND favouriteNewsModel.cmpUsrId.cmpUsrId = (:userId)")
    FavouriteNewsModel getFavouriteNews(@Param("artId") int artId,@Param("userId") int userId);

	@Query("SELECT f.newsArtcId FROM FavouriteNewsModel f WHERE f.deletedAt = NULL AND f.cmpUsrId.cmpUsrId=(:pCmpUsrId)")
	Page<NewsArticlesModel> getNewsArticlesModelByCmpUsrIdWithPaging(Pageable pageable,@Param("pCmpUsrId") int pCmpUsrId);

}
