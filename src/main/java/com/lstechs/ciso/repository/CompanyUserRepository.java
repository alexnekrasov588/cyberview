package com.lstechs.ciso.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.lstechs.ciso.model.CompanyUserModel;


import java.util.List;

@Repository
public interface CompanyUserRepository extends CRUDRepository<CompanyUserModel> {

	@Query("SELECT companyUserModel FROM CompanyUserModel  companyUserModel WHERE lower(companyUserModel.cmpUsrEmail)=(:pEmail) AND companyUserModel.deletedAt IS NULL")
    CompanyUserModel findCompanyUserModelByCmpUsrAndCmpUsrEmail(@Param("pEmail") String pEmail);

    @Transactional
    @Modifying(clearAutomatically = true)
	@Query("UPDATE CompanyUserModel c  set  c.cmpUsrPassword=(:pPassword) WHERE c.cmpUsrId=(:pId)")
    void changePassword(@Param("pId") int pId, @Param("pPassword") String pPassword);

    @Override
    @Query("SELECT companyUserModel FROM CompanyUserModel  companyUserModel WHERE companyUserModel.deletedAt IS NULL AND (lower(companyUserModel.cmpUsrFullName) LIKE lower(CONCAT('%', :query,'%')) OR lower(companyUserModel.cmpUsrEmail)  LIKE lower(CONCAT('%', :query,'%')) )")
    Page<CompanyUserModel> findAll(Pageable pageable,@Param("query") String query);

    @Query("SELECT companyUserModel FROM CompanyUserModel  companyUserModel WHERE companyUserModel.cmpUsrCmpId.cmpId=(:pId) AND companyUserModel.deletedAt IS NULL AND (lower(companyUserModel.cmpUsrFullName) LIKE %:query% OR lower(companyUserModel.cmpUsrEmail) LIKE %:query% )")
    Page<CompanyUserModel> findUsersCompany(Pageable pageable,@Param("query") String query,@Param("pId") int pId);

	@Query("SELECT companyUserModel FROM CompanyUserModel  companyUserModel WHERE companyUserModel.deletedAt IS NULL AND companyUserModel.cmpUsrCmpId.cmpId=(:cmpId)")
	List<CompanyUserModel> getUsersByCmpId(@Param("cmpId") int cmpId);

	@Query("SELECT COUNT (companyUserModel.cmpUsrId) FROM CompanyUserModel  companyUserModel WHERE companyUserModel.deletedAt IS NULL AND companyUserModel.cmpUsrCmpId.cmpId=(:cmpId)")
	int getUsersCountByCmpId(@Param("cmpId") int cmpId);

}


