package com.lstechs.ciso.repository;

import java.util.List;
import java.util.Map;

import com.lstechs.ciso.model.FilterModel;
import com.lstechs.ciso.model.StandardModel;
import com.lstechs.ciso.model.StandardNameModel;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface StandardRepository extends CRUDRepository<StandardModel> {

	@Transactional
	@Modifying(clearAutomatically = true)
	@Query("UPDATE StandardModel s  set   s.stdIsCompleted = true WHERE s.stdId=(:pId)")
	void markComplete(@Param("pId") int pId);

	@Query("SELECT standardModel FROM StandardModel  standardModel WHERE standardModel.deletedAt IS NULL AND standardModel.stdOnPremId=(:pId) ORDER BY standardModel.createdAt DESC")
	StandardModel getStdIdOnPrem(@Param("pId") int pId);

	@Query("SELECT standardModel FROM StandardModel  standardModel WHERE standardModel.deletedAt IS NULL AND standardModel.stdId=(:standardId)")
	StandardModel getStandardById(@Param("standardId") int standardId);

	@Query("SELECT standardModel FROM StandardModel  standardModel WHERE standardModel.deletedAt IS NULL AND standardModel.stdId IN (:stdId) AND standardModel.stdIsCompleted = true")
	Page<StandardModel> getCompanyStandard(Pageable pageable, @Param("stdId") List<Integer> stdId);

	@Query("SELECT New com.lstechs.ciso.model.StandardNameModel(standardModel.stdId , standardModel.stdName) FROM StandardModel  standardModel WHERE standardModel.deletedAt IS NULL AND standardModel.stdId IN (:stdId) AND standardModel.stdIsCompleted = true")
	List<StandardNameModel> getCompanyStandardName(@Param("stdId") List<Integer> stdId);

	@Query("SELECT standardModel FROM StandardModel  standardModel WHERE standardModel.deletedAt IS NULL AND standardModel.stdId IN (:stdId) AND standardModel.stdIsCompleted = true")
	List<StandardModel> getStandardsByIds(@Param("stdId") List<Integer> stdId);

	@Query("SELECT standardModel FROM StandardModel  standardModel WHERE standardModel.deletedAt IS NULL AND standardModel.stdType = com.lstechs.ciso.enums.StandardTypeEnum.STANDARD")
	Page<StandardModel> getTempalteStandard(Pageable pageable);

	@Query("SELECT NEW com.lstechs.ciso.model.FilterModel(standardModel.stdId,standardModel.stdName) FROM StandardModel  standardModel"
		+ " WHERE standardModel.deletedAt IS NULL AND standardModel.stdId IN (:stdId) AND standardModel.stdIsCompleted = true")
	List<FilterModel> getStandardListOption(@Param("stdId") List<Integer> stdId);

	@Query("SELECT CASE WHEN COUNT(standardModel) > 0 THEN TRUE ELSE FALSE END" +
		" FROM StandardModel  standardModel " +
		"WHERE lower(standardModel.stdName) LIKE lower(:stdName) " +
		"AND standardModel.deletedAt IS NULL " +
		"AND lower(standardModel.stdLvlName) LIKE lower(:stdLvlName)  " +
		"AND lower(standardModel.stdVersion) LIKE lower(:stdVersion) " +
		"AND standardModel.stdId <> (:stdId) ")
	boolean isStandardExistsByNameVersionAndLevel(@Param("stdName") String stdName, @Param("stdLvlName") String stdLvlName, @Param("stdVersion") String stdVersion, @Param("stdId") int stdId);

	@Query("SELECT standardModel FROM StandardModel standardModel " +
		"WHERE lower(standardModel.stdName) LIKE lower(:stdName) " +
		"AND standardModel.deletedAt IS NULL " +
		"AND lower(standardModel.stdLvlName) LIKE lower(:stdLvlName)  " +
		"AND lower(standardModel.stdVersion) LIKE lower(:stdVersion)")
	StandardModel getStandardExistsByNameVersionAndLevel(@Param("stdName") String stdName, @Param("stdLvlName") String stdLvlName, @Param("stdVersion") String stdVersion);

	@Query(value = "SELECT req.std_id, \n" +
		"       s.std_name, \n" +
		"       Cast(100.0 * Sum(CASE cs.compliant \n" +
		"                          WHEN 0 THEN 1 \n" +
		"                          WHEN 3 THEN 0 \n" +
		"                          ELSE 0 \n" +
		"                        END) / Count(req.std_id) AS DECIMAL(10, 2)) \n" +
		"FROM   standard AS s \n" +
		"       INNER JOIN std_requirements AS req \n" +
		"               ON s.std_id = req.std_id \n" +
		"       LEFT JOIN customer_standard AS cs \n" +
		"              ON cs.std_rqr_id = req.std_rqr_id \n" +
		"              AND cs.cmp_id = (:cmpId)\n" +
		"WHERE  req.deleted_at IS NULL \n" +
		"       AND cs.deleted_at IS NULL \n" +
		"       AND s.deleted_at IS NULL \n" +
		"       AND s.std_is_completed = true" +
		"       AND s.std_id IN (SELECT DISTINCT ls.std_id \n" +
		"                        FROM   license_standard ls \n" +
		"                        WHERE  ls.deleted_at IS NULL \n" +
		"                               AND ls.lcns_id IN (SELECT lcns_id \n" +
		"                                                  FROM   license l \n" +
		"                                                  WHERE  l.lcns_token = ( :lcnsToken ) \n" +
		"                                                         AND l.deleted_at IS \n" +
		"                                                             NULL)) \n" +
		"GROUP  BY req.std_id, \n" +
		"          s.std_id, \n" +
		"          s.std_name ", nativeQuery = true)
	List<Object[]> getCompliantStdReqCountByLcnsTokenAndCmpId(@Param("lcnsToken") String lcnsToken, @Param("cmpId") int cmpId);

	@Query(value = "SELECT * \n" +
		"FROM   standard \n" +
		"WHERE  std_id IN (SELECT all_std.std_id \n" +
		"                  FROM   (SELECT std_id \n" +
		"                          FROM   PUBLIC.license_standard \n" +
		"                          WHERE  lcns_id IN (SELECT lcns_id \n" +
		"                                             FROM   PUBLIC.license \n" +
		"                                             WHERE \n" +
		"                 lcns_token = ( :lcnsToken ) \n" +
		"                 AND deleted_at IS NULL) \n" +
		"                 AND deleted_at IS NULL) AS all_std \n" +
		"                         LEFT JOIN (SELECT std_id \n" +
		"                                    FROM   PUBLIC.std_requirements \n" +
		"                                    WHERE  deleted_at IS NULL \n" +
		"                                    ORDER  BY std_id) AS all_std_req \n" +
		"                                ON all_std.std_id = all_std_req.std_id \n" +
		"                  WHERE  all_std_req.std_id IS NULL)", nativeQuery = true)
	List<StandardModel> getAllStandardWithoutReqsByToken(@Param("lcnsToken") String lcnsToken);
}
