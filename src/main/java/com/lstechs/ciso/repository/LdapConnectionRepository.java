package com.lstechs.ciso.repository;

import com.lstechs.ciso.model.LdapConnectionModel;
import org.springframework.stereotype.Repository;

@Repository
public interface LdapConnectionRepository extends CRUDRepository<LdapConnectionModel> {
}
