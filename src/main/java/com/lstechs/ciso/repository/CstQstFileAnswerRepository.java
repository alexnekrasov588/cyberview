package com.lstechs.ciso.repository;

import com.lstechs.ciso.model.CstQstFileAnswerModel;
import com.lstechs.ciso.model.CustomerStandardModel;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface CstQstFileAnswerRepository extends CRUDRepository<CstQstFileAnswerModel> {

	@Query("SELECT cstQstFileAnswerModel FROM CstQstFileAnswerModel cstQstFileAnswerModel WHERE cstQstFileAnswerModel.deletedAt IS NULL AND cstQstFileAnswerModel.customerStandardId = (:customerStandardId) ")
	List<CstQstFileAnswerModel> getCstQstFileAnswerByCustomerStandardId(@Param("customerStandardId") CustomerStandardModel customerStandardId);

	@Transactional
	@Modifying(clearAutomatically = true)
	@Query("UPDATE CstQstFileAnswerModel cstQstFileAnswerModel set cstQstFileAnswerModel.deletedAt = NOW() WHERE cstQstFileAnswerModel.customerStandardId = (:customerStandardId) AND cstQstFileAnswerModel.fileUrl = (:fileUrl)")
	void deleteCustomerStandardFiles(@Param("customerStandardId") CustomerStandardModel customerStandardId, @Param("fileUrl") String fileUrl);
}
