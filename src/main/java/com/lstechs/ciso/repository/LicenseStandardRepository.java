package com.lstechs.ciso.repository;

import com.lstechs.ciso.model.LicenseStandardModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface LicenseStandardRepository extends CRUDRepository<LicenseStandardModel> {

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("UPDATE LicenseStandardModel licenseStandardModel set licenseStandardModel.deletedAt = CURRENT_TIMESTAMP  WHERE licenseStandardModel.lcnsId.lcnsId=(:pId)")
    void deleteLicenseStandardByLicence(@Param("pId") int pId);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("UPDATE LicenseStandardModel licenseStandardModel set licenseStandardModel.deletedAt = CURRENT_TIMESTAMP  WHERE licenseStandardModel.stdId.stdId=(:pId)")
    void deleteLicenseStandardByStandard(@Param("pId") int pId);

    @Query("SELECT DISTINCT licenseStandardModel FROM LicenseStandardModel  licenseStandardModel WHERE licenseStandardModel.deletedAt = NULL AND licenseStandardModel.lcnsId.lcnsId=(:id)")
    List<LicenseStandardModel> findAllLicenseStandardByLcnsId(@Param("id") int id);

}
