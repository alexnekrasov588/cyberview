package com.lstechs.ciso.repository;

import com.lstechs.ciso.model.StdQtrQuestionsModel;
import com.lstechs.ciso.model.StdRequirementsModel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface StdQtrQuestionsRepository extends CRUDRepository<StdQtrQuestionsModel> {
	@Transactional
	@Query("SELECT stdQtrQuestionsModel FROM StdQtrQuestionsModel  stdQtrQuestionsModel WHERE stdQtrQuestionsModel.deletedAt = NULL AND stdQtrQuestionsModel.stdSctId.stdSctId =(:stdSctId) ORDER BY stdQtrQuestionsModel.order ASC ")
	List<StdQtrQuestionsModel> getAllQuestionBySectionId(@Param("stdSctId") int stdSctId);

	@Transactional
	@Query("SELECT stdQtrQuestionsModel FROM StdQtrQuestionsModel  stdQtrQuestionsModel WHERE stdQtrQuestionsModel.deletedAt = NULL AND stdQtrQuestionsModel.qtrQstId =(:qtrQstId) ")
	StdQtrQuestionsModel getStdQtrQuestionsModelById(@Param("qtrQstId") int qtrQstId);

	@Query("SELECT stdQtrQuestionsModel FROM StdQtrQuestionsModel  stdQtrQuestionsModel WHERE stdQtrQuestionsModel.deletedAt = NULL AND stdQtrQuestionsModel.stdSctId.stdSctId IN(:stdSctIds) ORDER BY stdQtrQuestionsModel.order ASC ")
	List<StdQtrQuestionsModel> getAllQuestionBySectionIds(@Param("stdSctIds") List<Integer> stdSctIds);

	@Query("SELECT stdQtrQuestionsModel.stdRqrId FROM StdQtrQuestionsModel  stdQtrQuestionsModel WHERE stdQtrQuestionsModel.stdRqrId.deletedAt IS NULL AND stdQtrQuestionsModel.stdSctId.stdSctId =(:stdSctId) ORDER BY stdQtrQuestionsModel.stdRqrId.order ASC")
	List<StdRequirementsModel> getAllReqBySectionId(@Param("stdSctId") int stdSctId);

	@Transactional
	@Query("SELECT stdQtrQuestionsModel FROM StdQtrQuestionsModel  stdQtrQuestionsModel WHERE stdQtrQuestionsModel.deletedAt = NULL AND stdQtrQuestionsModel.stdRqrId.stdRqrId =(:stdRqrId) ORDER BY stdQtrQuestionsModel.stdSctId ASC")
	StdQtrQuestionsModel getQuestionByReqId(@Param("stdRqrId") int stdRqrId);

	@Query("SELECT coalesce(max(stdQtrQuestionsModel.order), 0) FROM StdQtrQuestionsModel  stdQtrQuestionsModel WHERE stdQtrQuestionsModel.stdRqrId.deletedAt IS NULL AND stdQtrQuestionsModel.stdSctId.stdSctId = (:ids)")
	int getMaxOrder(@Param("ids") int ids);

	@Query("SELECT coalesce(max(stdQtrQuestionsModel.order), 0) FROM StdQtrQuestionsModel  stdQtrQuestionsModel WHERE stdQtrQuestionsModel.stdRqrId.deletedAt IS NULL AND stdQtrQuestionsModel.stdSctId.stdSctId IN (:ids)")
	int getMaxOrderByIds(@Param("ids") List<Integer> ids);

	@Query("SELECT stdQtrQuestionsModel FROM StdQtrQuestionsModel  stdQtrQuestionsModel WHERE stdQtrQuestionsModel.deletedAt = NULL AND stdQtrQuestionsModel.qtrQstOnPremId = (:id)")
	StdQtrQuestionsModel getOnPrem(@Param("id") int id);
}
