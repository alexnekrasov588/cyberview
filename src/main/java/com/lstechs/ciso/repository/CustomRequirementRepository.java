package com.lstechs.ciso.repository;

import com.lstechs.ciso.model.CustomRequirementModel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomRequirementRepository extends CRUDRepository<CustomRequirementModel> {

	@Query("SELECT customRequirementModel FROM CustomRequirementModel customRequirementModel  WHERE customRequirementModel.deletedAt IS NULL ORDER BY customRequirementModel.createdAt ASC")
	List<CustomRequirementModel> findAll();

}
