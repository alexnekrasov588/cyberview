package com.lstechs.ciso.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.lstechs.ciso.enums.FolderTypeEnum;
import com.lstechs.ciso.model.FavouriteFoldersModel;
import com.lstechs.ciso.model.FilterModel;
import com.lstechs.ciso.model.NewsArticlesModel;

@Repository
public interface FavouriteFoldersRepository  extends CRUDRepository<FavouriteFoldersModel>{
	
    @Query("SELECT f FROM FavouriteFoldersModel f WHERE f.deletedAt = NULL AND f.cmpUserId.cmpUsrId=(:pCmpUsrId) AND"
    		+ " f.favType = (:type) ORDER BY f.createdAt DESC")
    List<FavouriteFoldersModel> getFavoriteFolders(@Param("pCmpUsrId") int pCmpUsrId,@Param("type") FolderTypeEnum favType);
    
    @Query("SELECT f FROM FavouriteFoldersModel f WHERE f.deletedAt = NULL AND f.cmpUserId.cmpUsrId=(:pCmpUsrId) AND"
    		+ " f.favType = (:type) AND f.parent = NULL ORDER BY f.createdAt DESC")
    List<FavouriteFoldersModel> getFavoriteFoldersWithoutParents(@Param("pCmpUsrId") int pCmpUsrId,@Param("type") FolderTypeEnum favType);
    
    @Query("SELECT f.parent FROM FavouriteFoldersModel f WHERE f.deletedAt = NULL AND f.cmpUserId.cmpUsrId=(:pCmpUsrId) AND"
    		+ " f.favType = (:type) AND f.parent IS NOT NULL ORDER BY f.createdAt DESC")
    List<FavouriteFoldersModel> getParents(@Param("pCmpUsrId") int pCmpUsrId,@Param("type") FolderTypeEnum favType);
    
    @Query("SELECT f FROM FavouriteFoldersModel f WHERE f.deletedAt = NULL AND f.parent.favId = (:parentId) ORDER BY f.createdAt DESC")
    List<FavouriteFoldersModel> getFavoriteFoldersByParent(@Param("parentId") int parentId);
    
	@Query("SELECT NEW com.lstechs.ciso.model.FilterModel(f.favId,f.favName) FROM FavouriteFoldersModel f "
			+ " WHERE f.deletedAt = NULL AND f.cmpUserId.cmpUsrId=(:pCmpUsrId) AND "
			+  " f.favType = (:type) ORDER BY f.createdAt DESC")
	List<FilterModel> getFavoriteFoldersOption(@Param("pCmpUsrId") int pCmpUsrId,@Param("type") FolderTypeEnum favType);

}
