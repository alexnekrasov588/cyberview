package com.lstechs.ciso.repository;

import com.lstechs.ciso.model.SelectedQuestionnaireModel;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface SelectedQuestionnaireRepository extends CRUDRepository<SelectedQuestionnaireModel>{
	@Query("SELECT selectedQuestionnaireModel FROM SelectedQuestionnaireModel selectedQuestionnaireModel WHERE selectedQuestionnaireModel.deletedAt IS NULL AND selectedQuestionnaireModel.roleId.roleId = (:roleId) AND selectedQuestionnaireModel.standardId = (:standardId)")
	List<SelectedQuestionnaireModel> getSelectedQuestionnairesModelByRoleIdAndStandardId(@Param("roleId") int roleId, @Param("standardId") int standardId);

	@Query("SELECT selectedQuestionnaireModel FROM SelectedQuestionnaireModel selectedQuestionnaireModel WHERE selectedQuestionnaireModel.deletedAt IS NULL AND selectedQuestionnaireModel.roleId.roleId = (:roleId)")
	List<SelectedQuestionnaireModel> getSelectedQuestionnairesModelByRoleId(@Param("roleId") int roleId);

	@Transactional
	@Modifying(clearAutomatically = true)
	@Query("UPDATE SelectedQuestionnaireModel selectedQuestionnaireModel set selectedQuestionnaireModel.deletedAt = CURRENT_TIMESTAMP  WHERE selectedQuestionnaireModel.roleId.roleId = (:roleId)")
	void deleteSelectedQuestionnaireModel(@Param("roleId") int roleId);
}
