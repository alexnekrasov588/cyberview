package com.lstechs.ciso.repository;

import com.lstechs.ciso.model.ProjectModel;
import com.lstechs.ciso.model.TaskModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface TaskRepository extends CRUDRepository<TaskModel> {

	@Query("SELECT taskModel FROM TaskModel  taskModel WHERE taskModel.tskPrjID.prjId =(:prjId) AND taskModel.deletedAt IS NULL AND (lower(taskModel.tskName) LIKE %:name% AND (((:assignee) > 0 AND taskModel.tskAssignee.cmpUsrId =(:assignee)) OR (:assignee)=0))")
	Page<TaskModel> findTasks(Pageable pageable, @Param("prjId") int prjId, @Param("name") String name, @Param("assignee") int assignee);

	@Query("SELECT taskModel FROM TaskModel  taskModel WHERE taskModel.tskPrjID.prjId =(:prjId) AND taskModel.deletedAt IS NULL ")
	List<TaskModel> getAllTaskByProjId(@Param("prjId") int prjId);

	@Query("SELECT COUNT(taskModel) FROM TaskModel  taskModel WHERE taskModel.tskPrjID.prjId =(:prjId) AND taskModel.deletedAt IS NULL")
	int getAllTasksCountByProjId(@Param("prjId") int prjId);

	@Query("SELECT taskModel FROM TaskModel  taskModel WHERE taskModel.tskPrjID.prjId =(:prjId) AND taskModel.deletedAt IS NULL AND taskModel.status <> com.lstechs.ciso.enums.StatusEnum.DONE")
	List<TaskModel> getAllUnDoneTasksByProjId(@Param("prjId") int prjId);

	@Query("SELECT COUNT(taskModel) FROM TaskModel  taskModel WHERE taskModel.tskPrjID.prjId =(:prjId) AND taskModel.deletedAt IS NULL AND taskModel.status = com.lstechs.ciso.enums.StatusEnum.DONE")
	int getAllDoneTasksCountByProjId(@Param("prjId") int prjId);

	@Query("SELECT COUNT(taskModel) FROM TaskModel  taskModel WHERE taskModel.tskPrjID.prjId =(:prjId) AND taskModel.deletedAt IS NULL AND taskModel.status <> com.lstechs.ciso.enums.StatusEnum.DONE")
	int getAllUnDoneTasksCountByProjId(@Param("prjId") int prjId);

	@Query("SELECT COUNT(taskModel.tskId) FROM TaskModel  taskModel WHERE taskModel.deletedAt IS NULL AND taskModel.tskPrjID.prjCmpId.cmpId =(:cmpId)")
	int getTasksCountByCompId(@Param("cmpId") int cmpId);

	@Query("SELECT DISTINCT taskModel.tskPrjID FROM TaskModel  taskModel WHERE taskModel.deletedAt IS NULL AND taskModel.status <>  com.lstechs.ciso.enums.StatusEnum.DONE AND taskModel.tskPrjID.prjEndDate < CURRENT_DATE AND taskModel.tskPrjID.deletedAt IS NULL")
	List<ProjectModel> getAllProgressOverdueProjects();

	@Query("SELECT CASE WHEN COUNT(taskModel) > 0 THEN TRUE ELSE FALSE END" +
		" FROM TaskModel  taskModel " +
		"WHERE taskModel.tskId = (:tskId) AND taskModel.deletedAt IS NULL AND taskModel.tskAssignee.cmpUsrId = (:tskAssignee) ")
	boolean isTaskAssignToCmpUsrId(@Param("tskAssignee") int tskAssignee, @Param("tskId") int tskId);
	
	
	@Query("SELECT taskModel FROM TaskModel taskModel WHERE  taskModel.deletedAt = NULL "
			+ " AND taskModel.tskPrjID.prjCmpId.cmpId = (:cmpId) AND "
			+ " ((:isEndDateExist) = FALSE OR taskModel.tskEndDate <= (:tskEndDate) ) AND "
			+ " ((:isStartDateExist) = FALSE OR taskModel.tskStartDate >= (:tskStartDate) ) AND "
			+ "( (:prjIds) IS NULL OR taskModel.tskPrjID.prjId IN (:prjIds)) ORDER BY taskModel.tskPrjID.prjId ")
	List<TaskModel> getTasksReport(@Param("cmpId") int cmpId,
			@Param("isEndDateExist") Boolean isEndDateExist,
			@Param("isStartDateExist") Boolean isStartDateExist,
			@Param("tskStartDate") Date tskStartDate,	
			@Param("tskEndDate") Date tskEndDate,
			@Param("prjIds") List<Integer> prjIds);
	
	


}
