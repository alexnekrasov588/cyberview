package com.lstechs.ciso.repository;

import com.lstechs.ciso.model.StdAnswer2OptionModel;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface StdAnswer2OptionRepository extends CRUDRepository<StdAnswer2OptionModel> {

	@Transactional
	@Modifying(clearAutomatically = true)
	@Query("UPDATE StdAnswer2OptionModel stdAnswer2OptionModel set stdAnswer2OptionModel.deletedAt = CURRENT_TIMESTAMP  WHERE stdAnswer2OptionModel.sqoQstId.qtrQstId=(:pId)")
	void deleteStdAnswer2OptionModel(@Param("pId") int pId);

	@Query("SELECT stdAnswer2OptionModel FROM StdAnswer2OptionModel stdAnswer2OptionModel WHERE stdAnswer2OptionModel.deletedAt = NULL AND stdAnswer2OptionModel.sqoQstId.qtrQstId IN(:ids) ")
	List<StdAnswer2OptionModel> getOptionsListByQuestionsIds(@Param("ids") List<Integer> ids);
}
