package com.lstechs.ciso.repository;

import com.lstechs.ciso.model.NewsArticlesTagsModel;
import com.lstechs.ciso.model.NewsArticlesModel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NewsArticlesTagsRepository extends CRUDRepository<NewsArticlesTagsModel> {

    @Query("SELECT DISTINCT articlesTagsModel.newsArtcId FROM NewsArticlesTagsModel articlesTagsModel WHERE  articlesTagsModel.deletedAt = NULL AND articlesTagsModel.newsTagId.newsTagId IN (:tagsId)")
    List<NewsArticlesModel> getArticlesTags(@Param("tagsId") int[] tagsId);
}
