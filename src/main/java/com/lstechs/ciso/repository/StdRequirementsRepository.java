package com.lstechs.ciso.repository;

import com.lstechs.ciso.model.StdRequirementsModel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StdRequirementsRepository extends CRUDRepository<StdRequirementsModel> {

	@Query("SELECT stdRequirementsModel FROM StdRequirementsModel  stdRequirementsModel WHERE stdRequirementsModel.deletedAt = NULL AND stdRequirementsModel.stdId.stdId = (:id) ORDER BY stdRequirementsModel.order ASC")
	List<StdRequirementsModel> getReqListByTemplateId(@Param("id") int id);


	@Query("SELECT stdRequirementsModel FROM StdRequirementsModel  stdRequirementsModel WHERE stdRequirementsModel.deletedAt = NULL AND stdRequirementsModel.stdRqrOnPremId = (:id) ORDER BY stdRequirementsModel.createdAt DESC")
	List<StdRequirementsModel> getOnPrem(@Param("id") int id);


	@Query("SELECT stdRequirementsModel FROM StdRequirementsModel  stdRequirementsModel WHERE stdRequirementsModel.deletedAt = NULL AND stdRequirementsModel.stdId.stdId IN (:ids) ORDER BY stdRequirementsModel.order ASC")
	List<StdRequirementsModel> getReqListByTemplateIds(@Param("ids") List<Integer> ids);

	@Query("SELECT count(stdRequirementsModel) FROM StdRequirementsModel  stdRequirementsModel WHERE stdRequirementsModel.deletedAt = NULL AND stdRequirementsModel.stdId.stdId = (:id)")
	int getReqListCountByTemplateId(@Param("id") int id);

	@Query("SELECT coalesce(max(stdRequirementsModel.order), 0) FROM StdRequirementsModel  stdRequirementsModel WHERE stdRequirementsModel.deletedAt = NULL AND " +
		" stdRequirementsModel.stdRqrId IN (SELECT stdQtrQuestionsModel.stdRqrId FROM StdQtrQuestionsModel stdQtrQuestionsModel WHERE stdQtrQuestionsModel.deletedAt = NULL AND" +
		" stdQtrQuestionsModel.stdSctId.stdSctId = (:id) )")
	int getMaxOrder(@Param("id") int id);

	@Query("SELECT stdRequirementsModel FROM StdRequirementsModel  stdRequirementsModel WHERE stdRequirementsModel.deletedAt = NULL AND stdRequirementsModel.stdId.stdId = (:id) ORDER BY stdRequirementsModel.order ASC")
	List<StdRequirementsModel> getReqByStdId(@Param("id") int id);

	@Query("SELECT stdRequirementsModel FROM StdRequirementsModel  stdRequirementsModel WHERE stdRequirementsModel.deletedAt = NULL AND stdRequirementsModel.stdRqrId = (:id)")
	StdRequirementsModel getReqByReqId(@Param("id") int id);


	@Query("SELECT coalesce(max(stdRequirementsModel.order), 0) FROM StdRequirementsModel  stdRequirementsModel WHERE stdRequirementsModel.deletedAt IS NULL AND stdRequirementsModel.stdId.stdId = (:stdId)")
	int getMaxOrderByStdId(@Param("stdId") int stdId);
}
