package com.lstechs.ciso.repository;

import com.lstechs.ciso.model.CrossReferenceModel;
import com.lstechs.ciso.model.CrossReferenceReportResultModel;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

@Repository
public interface CrossReferenceRepository extends CRUDRepository<CrossReferenceModel> {

	@Transactional
	@Modifying(clearAutomatically = true)
	@Query("DELETE FROM CrossReferenceModel crossReferenceModel WHERE crossReferenceModel.cstRqrId.cstRqrId = (:cstRqrId)")
	void deleteByCstRqrId(@Param("cstRqrId") int cstRqrId);

	@Query("SELECT crossReferenceModel  FROM CrossReferenceModel crossReferenceModel WHERE crossReferenceModel.cstRqrId.cstRqrId = (:cstRqrId) AND crossReferenceModel.deletedAt IS NULL")
	List<CrossReferenceModel> findAllByCstRqrId(@Param("cstRqrId") int cstRqrId);

	@Query(value = "WITH customer_standard AS (SELECT COUNT(std_rqr_id) as count_req, std_id\n" +
		"FROM cross_reference\n" +
		"WHERE cst_rqr_id IN (SELECT cst_rqr_id FROM cross_reference WHERE std_rqr_id\n" +
		"IN (SELECT std_rqr_id FROM customer_standard WHERE compliant = 0))\n" +
		"AND std_id NOT IN (SELECT std_id FROM customer_standard WHERE compliant = 0) GROUP BY std_id),\n" +
		"total_standards_requirements AS (\n" +
		"SELECT COUNT(std_rqr_id) as total_requirements_count, std_id\n" +
		"FROM std_requirements\n" +
		"WHERE std_id IN (SELECT std_id FROM customer_standard)\n" +
		"GROUP BY std_id)\n" +
		"SELECT (count_req / total_requirements_count) as percent, s.std_name as standardName\n" +
		"FROM\n" +
		"(SELECT cs.count_req , tsr.total_requirements_count, cs.std_id FROM customer_standard as cs\n" +
		"JOIN total_standards_requirements as tsr ON tsr.std_id = cs.std_id) as j\n" +
		"INNER JOIN standard as s ON s.std_id = j.std_id", nativeQuery = true)
	Collection<CrossReferenceReportResultModel> crossReference();


	@Transactional
	@Modifying(clearAutomatically = true)
	@Query("DELETE FROM CrossReferenceModel crossReferenceModel WHERE crossReferenceModel.stdId.stdId = (:stdId)")
	void deleteByStdId(@Param("stdId") int stdId);

	@Transactional
	@Modifying(clearAutomatically = true)
	@Query("DELETE FROM CrossReferenceModel crossReferenceModel WHERE crossReferenceModel.stdRqrId.stdRqrId = (:rqrId)")
	void deleteByRqrId(@Param("rqrId") int rqrId);

}
