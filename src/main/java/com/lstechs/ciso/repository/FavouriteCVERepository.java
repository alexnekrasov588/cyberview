package com.lstechs.ciso.repository;

import com.lstechs.ciso.model.FavouriteCVEModel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FavouriteCVERepository extends CRUDRepository<FavouriteCVEModel> {

    @Query("SELECT f FROM FavouriteCVEModel f WHERE f.deletedAt = NULL AND f.cmpUserId.cmpUsrId=(:pCmpUsrId) ORDER BY f.createdAt DESC")
    List<FavouriteCVEModel> getFavouriteCVEByCmpUserId(@Param("pCmpUsrId") int pCmpUsrId);

    @Query("SELECT favouriteCVEModel FROM FavouriteCVEModel favouriteCVEModel WHERE favouriteCVEModel.deletedAt = NULL AND favouriteCVEModel.cveId.cveId= (:cveId) AND favouriteCVEModel.cmpUserId.cmpUsrId = (:userId)")
    FavouriteCVEModel getFavouriteCVES(@Param("cveId") int cveId,@Param("userId") int userId);

	@Query("SELECT f FROM FavouriteCVEModel f WHERE f.deletedAt = NULL AND f.folder.favId=(:folderId) ORDER BY f.createdAt DESC")
	List<FavouriteCVEModel> getNewsCVEsModelByFolderId(@Param("folderId") int folderId);
}
