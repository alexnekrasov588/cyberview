package com.lstechs.ciso.repository;

import com.lstechs.ciso.enums.ModuleEnum;
import com.lstechs.ciso.model.ModuleTaskModel;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface ModuleTaskRepository extends CRUDRepository<ModuleTaskModel> {

	@Transactional
	@Modifying(clearAutomatically = true)
	@Query("DELETE FROM ModuleTaskModel m  WHERE  m.mdlTskId = (:mdlTskId) ")
	void deleteModuleTaskModelByMdlTskId(@Param("mdlTskId") int mdlTskId);


	@Query("SELECT moduleTaskModle FROM ModuleTaskModel moduleTaskModle WHERE moduleTaskModle.deletedAt IS NULL AND moduleTaskModle.module=(:pModule) AND moduleTaskModle.mdlTskIdInternal =(:pId) AND moduleTaskModle.tskId.tskPrjID.prjCmpId.cmpId = (:cmpId)")
	ModuleTaskModel findModuleTaskByModuleAndIdAndCmpId(@Param("pModule") ModuleEnum pModule, @Param("pId") int id, @Param("cmpId") int cmpId);


	@Query("SELECT moduleTaskModle FROM ModuleTaskModel moduleTaskModle WHERE moduleTaskModle.deletedAt IS NULL  AND moduleTaskModle.tskId.tskId =(:pId)")
	ModuleTaskModel findModuleTaskByTaskId(@Param("pId") int id);

}
