package com.lstechs.ciso.repository;

import com.lstechs.ciso.model.NewsArticlesModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface NewsArticlesRepository extends CRUDRepository<NewsArticlesModel> {

	@Override
	@Query("SELECT newsArticlesModel FROM NewsArticlesModel newsArticlesModel WHERE newsArticlesModel.deletedAt = NULL AND (lower(newsArticlesModel.newsArtcTitle) LIKE %:query% OR lower(newsArticlesModel.newsArtcContent) LIKE %:query% ) ORDER BY newsArticlesModel.newsPubDate DESC")
	Page<NewsArticlesModel> findAll(Pageable pageable, @Param("query") String query);

	@Override
	@Query("SELECT MAX(newsArticlesModel.newsUpdateOnPrem) FROM  NewsArticlesModel newsArticlesModel")
	Date lastUpdateAt();

	@Query("SELECT newsArticlesModel FROM NewsArticlesModel newsArticlesModel WHERE newsArticlesModel.newsIsActive = TRUE AND newsArticlesModel.deletedAt = NULL AND (lower(newsArticlesModel.newsArtcTitle) LIKE %:query% OR lower(newsArticlesModel.newsArtcContent) LIKE %:query% ) ORDER BY newsArticlesModel.newsPubDate DESC")
	Page<NewsArticlesModel> findAllActiveNews(Pageable pageable, @Param("query") String query);

	@Query("SELECT newsArticlesModel.newsArtcHash FROM NewsArticlesModel newsArticlesModel")
	List<String> findAllArticleHashes();

	@Query(value = "SELECT DISTINCT * FROM news_articles " +
		"WHERE deleted_at IS NULL AND" +
		"(lower(news_artc_title) LIKE %:query% " +
		"OR lower(news_artc_content) LIKE %:query% ) AND " +
		"news_artc_id IN ( SELECT news_artc_id " +
		"                  FROM news_articles_tags " +
		"                  WHERE news_tag_id IN (:tagsId)" +
		") ORDER BY news_pub_date DESC \n-- #pageable\n",
		countQuery = "SELECT count(*) as count FROM (SELECT DISTINCT * FROM news_articles WHERE deleted_at IS NULL AND (lower(news_artc_title) LIKE %:query% OR\n lower(news_artc_content) LIKE %:query% )  AND news_artc_id IN(SELECT  news_artc_id FROM news_articles_tags WHERE news_tag_id IN (:tagsId)) ORDER BY created_at DESC) as all_news_by_tags",
		nativeQuery = true)
	Page<NewsArticlesModel> findAllNewsByTags(Pageable pageable, @Param("query") String query, @Param("tagsId") int[] tagsId);

	@Query(value = "SELECT DISTINCT * FROM news_articles " +
		"WHERE deleted_at IS NULL AND " +
		"news_is_active = true AND" +
		"(lower(news_artc_title) LIKE %:query% " +
		"OR lower(news_artc_content) LIKE %:query% ) AND " +
		"news_artc_id IN ( SELECT news_artc_id " +
		"                  FROM news_articles_tags " +
		"                  WHERE news_tag_id IN (:tagsId)" +
		") ORDER BY news_pub_date DESC \n-- #pageable\n",
		countQuery = "SELECT count(*) AS count FROM (SELECT DISTINCT * FROM news_articles WHERE deleted_at IS NULL AND news_is_active = TRUE AND (lower(news_artc_title) LIKE %:query% OR\n lower(news_artc_content) LIKE %:query% )  AND news_artc_id IN(SELECT  news_artc_id FROM news_articles_tags WHERE news_tag_id IN (:tagsId)) ORDER BY created_at DESC) as all_active_news_by_tags",
		nativeQuery = true)
	Page<NewsArticlesModel> findAllActiveNewsByTags(Pageable pageable, @Param("query") String query, @Param("tagsId") int[] tagsId);



	@Query("SELECT  newsArticlesModel FROM NewsArticlesModel newsArticlesModel "
			+ "WHERE newsArticlesModel.newsIsActive = TRUE "
			+ "AND newsArticlesModel.deletedAt = NULL  ORDER BY newsArticlesModel.newsPubDate DESC"
			)
	Page<NewsArticlesModel> findActiveNewsForDashboard(Pageable pageable);

	@Query("SELECT  newsArticlesModel FROM NewsArticlesModel newsArticlesModel "
			+ "WHERE newsArticlesModel.newsIsActive = TRUE "
			+ " AND newsArticlesModel.deletedAt = NULL"
			+ " AND exists "
			+ " (SELECT 1 FROM AssetModel assetModel where assetModel.deletedAt is NULL AND "
			+ " assetModel.cmpId.cmpId = (:cmpId) AND "
			+ "(newsArticlesModel.newsArtcTitle like ( CONCAT('%',assetModel.astName,'%')) OR "
			+ "	newsArticlesModel.newsArtcTitle like ( CONCAT('%',assetModel.astManufactor,'%')) OR"
			+ "	newsArticlesModel.newsArtcTitle like ( CONCAT('%',assetModel.astProduct,'%')) OR"
			+ "	newsArticlesModel.newsArtcContent like ( CONCAT('%',assetModel.astName,'%')) OR "
			+ "	newsArticlesModel.newsArtcContent like ( CONCAT('%',assetModel.astManufactor,'%')) OR"
			+ "	newsArticlesModel.newsArtcContent like ( CONCAT('%',assetModel.astProduct,'%')) )"
			+ " ) "
			+ "  ORDER BY newsArticlesModel.newsPubDate DESC")
	Page<NewsArticlesModel> findActiveAssetNewsForDashboard(Pageable pageable,@Param("cmpId") int cmpId);


	@Query("SELECT  newsArticlesModel FROM NewsArticlesModel newsArticlesModel "
			+ "WHERE newsArticlesModel.newsIsActive = TRUE "
			+ " AND newsArticlesModel.deletedAt IS NULL "
			+ " AND (lower(newsArticlesModel.newsArtcTitle) LIKE %:query% OR lower(newsArticlesModel.newsArtcContent) LIKE %:query% ) "
			+ " AND exists "
			+ " (SELECT 1 FROM AssetModel assetModel where assetModel.deletedAt is NULL AND "
			+ " assetModel.cmpId.cmpId = (:cmpId) AND "
			+ " assetModel.astId IN (:astsId) AND "
			+ " (newsArticlesModel.newsArtcTitle like ( CONCAT('% ',assetModel.astName,' %')) OR "
			+ "	newsArticlesModel.newsArtcTitle like ( CONCAT('% ',assetModel.astManufactor,' %')) OR"
			+ "	newsArticlesModel.newsArtcTitle like ( CONCAT('% ',assetModel.astProduct,' %')) OR"
			+ "	newsArticlesModel.newsArtcContent like ( CONCAT('% ',assetModel.astName,' %')) OR "
			+ "	newsArticlesModel.newsArtcContent like ( CONCAT('% ',assetModel.astManufactor,' %')) OR"
			+ "	newsArticlesModel.newsArtcContent like ( CONCAT('% ',assetModel.astProduct,' %')) ) "
			+ " ) "
			+ "  ORDER BY newsArticlesModel.newsPubDate DESC")
	Page<NewsArticlesModel> findAllActiveNewsByAssetFilter(Pageable pageable,@Param("cmpId") int cmpId,	@Param("query") String query, @Param("astsId") int[] astsId);


	@Query("SELECT  newsArticlesModel FROM NewsArticlesModel newsArticlesModel "
			+ "WHERE newsArticlesModel.newsIsActive = TRUE "
			+ " AND newsArticlesModel.deletedAt IS NULL "
			+ " AND (lower(newsArticlesModel.newsArtcTitle) LIKE %:query% OR lower(newsArticlesModel.newsArtcContent) LIKE %:query% ) "
			+ " AND exists "
			+ " (SELECT 1 FROM AssetModel assetModel where assetModel.deletedAt is NULL AND "
			+ " assetModel.cmpId.cmpId = (:cmpId) AND "
			+ " (newsArticlesModel.newsArtcTitle like ( CONCAT('% ',assetModel.astName,' %')) OR "
			+ "	newsArticlesModel.newsArtcTitle like ( CONCAT('% ',assetModel.astManufactor,' %')) OR"
			+ "	newsArticlesModel.newsArtcTitle like ( CONCAT('% ',assetModel.astProduct,' %')) OR"
			+ "	newsArticlesModel.newsArtcContent like ( CONCAT('% ',assetModel.astName,' %')) OR "
			+ "	newsArticlesModel.newsArtcContent like ( CONCAT('% ',assetModel.astManufactor,' %')) OR"
			+ "	newsArticlesModel.newsArtcContent like ( CONCAT('% ',assetModel.astProduct,' %')) ) "
			+ " ) "
			+ "  ORDER BY newsArticlesModel.newsPubDate DESC")
	Page<NewsArticlesModel> findAllActiveNewsByAsset(Pageable pageable,@Param("cmpId") int cmpId,@Param("query") String query);

	@Query("SELECT newsArticlesModel FROM NewsArticlesModel newsArticlesModel WHERE newsArticlesModel.deletedAt = NULL AND newsArticlesModel.id = (:id)")
	NewsArticlesModel getNewsArticleById(@Param("id") int id);
}
