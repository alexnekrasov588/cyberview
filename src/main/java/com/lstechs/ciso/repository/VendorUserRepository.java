package com.lstechs.ciso.repository;

import com.lstechs.ciso.model.VendorUserModel;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface VendorUserRepository extends CRUDRepository<VendorUserModel> {

	@Query("SELECT vendorUser FROM VendorUserModel vendorUser WHERE lower(vendorUser.vndrEmail)=(:pEmail) AND vendorUser.deletedAt = NULL")
	VendorUserModel findVendorUserByEmailAndPassword(@Param("pEmail") String pEmail);

	@Transactional
	@Modifying(clearAutomatically = true)
	@Query("UPDATE VendorUserModel v  set   v.vndrPassword=(:pPassword) WHERE v.vndrId=(:pId)")
	void changePassword(@Param("pId") int pId, @Param("pPassword") String pPassword);
}
