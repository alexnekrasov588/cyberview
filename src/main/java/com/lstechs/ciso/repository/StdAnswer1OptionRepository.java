package com.lstechs.ciso.repository;

import com.lstechs.ciso.model.StdAnswer1OptionModel;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface StdAnswer1OptionRepository extends CRUDRepository<StdAnswer1OptionModel> {

	@Transactional
	@Modifying(clearAutomatically = true)
	@Query("UPDATE StdAnswer1OptionModel stdAnswer1OptionModel set stdAnswer1OptionModel.deletedAt = CURRENT_TIMESTAMP  WHERE stdAnswer1OptionModel.sqoQstId.qtrQstId=(:pId)")
	void deleteStdAnswer1OptionModel(@Param("pId") int pId);

	@Query("SELECT stdAnswer1OptionModel FROM StdAnswer1OptionModel stdAnswer1OptionModel WHERE stdAnswer1OptionModel.deletedAt = NULL AND stdAnswer1OptionModel.sqoQstId.qtrQstId IN(:ids) ")
	List<StdAnswer1OptionModel> getOptionsListByQuestionsIds(@Param("ids") List<Integer> ids);
}
