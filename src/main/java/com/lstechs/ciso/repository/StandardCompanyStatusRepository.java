package com.lstechs.ciso.repository;

import com.lstechs.ciso.model.StandardCompanyStatusModel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StandardCompanyStatusRepository extends CRUDRepository<StandardCompanyStatusModel> {

	@Query("SELECT standardCompanyStatusModel FROM StandardCompanyStatusModel  standardCompanyStatusModel WHERE standardCompanyStatusModel.deletedAt IS NULL AND standardCompanyStatusModel.cmpId.cmpId = (:cmpId) AND standardCompanyStatusModel.stdId.stdId = (:stdId)")
	StandardCompanyStatusModel isStandardCompanyStatusExist(@Param("stdId") int stdId, @Param("cmpId") int cmpId);

	@Query("SELECT standardCompanyStatusModel FROM StandardCompanyStatusModel  standardCompanyStatusModel WHERE standardCompanyStatusModel.deletedAt IS NULL AND standardCompanyStatusModel.cmpId.cmpId = (:cmpId)")
	List<StandardCompanyStatusModel> getStandardCompanyStatusByCmpId(@Param("cmpId") int cmpId);

	@Query("SELECT standardCompanyStatusModel FROM StandardCompanyStatusModel  standardCompanyStatusModel WHERE standardCompanyStatusModel.deletedAt IS NULL AND standardCompanyStatusModel.stdId.stdId = (:stdId)")
	List<StandardCompanyStatusModel> getStandardCompanyStatusByStdId(@Param("stdId") int stdId);


}
