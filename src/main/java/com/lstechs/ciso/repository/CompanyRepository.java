package com.lstechs.ciso.repository;

import com.lstechs.ciso.model.CompanyModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface CompanyRepository extends CRUDRepository<CompanyModel> {

	@Query("SELECT company FROM CompanyModel company WHERE company.deletedAt IS NULL")
	List<CompanyModel> getLDAPCompany();

	@Override
	@Query("SELECT company FROM CompanyModel company WHERE company.deletedAt IS NULL AND lower(company.cmpName) LIKE lower(CONCAT('%', :query,'%')) ")
	Page<CompanyModel> findAll(Pageable pageable, @Param("query") String query);

	@Query(value = "SELECT cmp_id, \n" +
		"       created_at, \n" +
		"       deleted_at, \n" +
		"       updated_at, \n" +
		"       cmp_data_retention_period, \n" +
		"       cmp_logo_url, \n" +
		"       cmp_name, \n" +
		"       cmp_site_name, \n" +
		"       cmp_support_email, \n" +
		"       cmp_lcns_id, \n" +
		"       cmp_logo_file \n" +
		"FROM   PUBLIC.company \n" +
		"       LEFT JOIN (SELECT DISTINCT company_user.cmp_usr_cmp_id, \n" +
		"                                  Count(company_user.cmp_usr_cmp_id)AS \n" +
		"                                  cmp_user_count \n" +
		"                  FROM   company_user \n" +
		"                  WHERE  company_user.deleted_at IS NULL \n" +
		"                  GROUP  BY company_user.cmp_usr_cmp_id) AS cmp_user \n" +
		"              ON cmp_user.cmp_usr_cmp_id = company.cmp_id \n" +
		"WHERE  company.deleted_at IS NULL \n" +
		"       AND  Lower(company.cmp_name)  LIKE %:query% \n" +
		"ORDER  BY CASE \n" +
		"            WHEN cmp_user.cmp_user_count IS NULL THEN 0 \n" +
		"            ELSE 1 \n" +
		"          END, \n" +
		"          cmp_user.cmp_user_count, \n" +
		"          PUBLIC.company.cmp_name ASC "
		, nativeQuery = true)
	Page<CompanyModel> findAllOrderByCmpUsersCountASC(Pageable pageable, @Param("query") String query);

	@Query(value = "SELECT cmp_id, \n" +
		"       created_at, \n" +
		"       deleted_at, \n" +
		"       updated_at, \n" +
		"       cmp_data_retention_period, \n" +
		"       cmp_logo_url, \n" +
		"       cmp_name, \n" +
		"       cmp_site_name, \n" +
		"       cmp_support_email, \n" +
		"       cmp_lcns_id, \n" +
		"       cmp_logo_file \n" +
		"FROM   PUBLIC.company \n" +
		"       LEFT JOIN (SELECT DISTINCT company_user.cmp_usr_cmp_id, \n" +
		"                                  Count(company_user.cmp_usr_cmp_id)AS \n" +
		"                                  cmp_user_count \n" +
		"                  FROM   company_user \n" +
		"                  WHERE  company_user.deleted_at IS NULL \n" +
		"                  GROUP  BY company_user.cmp_usr_cmp_id) AS cmp_user \n" +
		"              ON cmp_user.cmp_usr_cmp_id = company.cmp_id \n" +
		"WHERE  company.deleted_at IS NULL \n" +
		"       AND  Lower(company.cmp_name)  LIKE %:query% \n" +
		"ORDER  BY CASE \n" +
		"            WHEN cmp_user.cmp_user_count IS NULL THEN 1 \n" +
		"            ELSE 0 \n" +
		"          END, \n" +
		"          cmp_user.cmp_user_count DESC "
		, nativeQuery = true)
	Page<CompanyModel> findAllOrderByCmpUsersCountDESC(Pageable pageable, @Param("query") String query);

	@Query(value = "SELECT cmp_id, \n" +
		"       created_at, \n" +
		"       deleted_at, \n" +
		"       updated_at, \n" +
		"       cmp_data_retention_period, \n" +
		"       cmp_logo_url, \n" +
		"       cmp_name, \n" +
		"       cmp_site_name, \n" +
		"       cmp_support_email, \n" +
		"       cmp_lcns_id, \n" +
		"       cmp_logo_file \n" +
		"FROM   PUBLIC.company \n" +
		"       LEFT JOIN (SELECT DISTINCT lcns_id, \n" +
		"                                  Count(std_id) AS standard_count \n" +
		"                  FROM   PUBLIC.license_standard \n" +
		"                  WHERE  deleted_at IS NULL \n" +
		"                  GROUP  BY lcns_id) AS cmp_lcns \n" +
		"              ON PUBLIC.company.cmp_lcns_id = cmp_lcns.lcns_id \n" +
		"WHERE  company.deleted_at IS NULL \n" +
		"       AND Lower(company.cmp_name)  LIKE %:query% \n" +
		"ORDER  BY CASE \n" +
		"            WHEN cmp_lcns.standard_count IS NULL THEN 0 \n" +
		"            ELSE 1 \n" +
		"          END, \n" +
		"          cmp_lcns.standard_count, \n" +
		"          company.cmp_name ASC"
		, nativeQuery = true)
	Page<CompanyModel> findAllOrderByStandardsCountASC(Pageable pageable, @Param("query") String query);

	@Query(value = "SELECT cmp_id, \n" +
		"       created_at, \n" +
		"       deleted_at, \n" +
		"       updated_at, \n" +
		"       cmp_data_retention_period, \n" +
		"       cmp_logo_url, \n" +
		"       cmp_name, \n" +
		"       cmp_site_name, \n" +
		"       cmp_support_email, \n" +
		"       cmp_lcns_id, \n" +
		"       cmp_logo_file \n" +
		"FROM   PUBLIC.company \n" +
		"       LEFT JOIN (SELECT DISTINCT lcns_id, \n" +
		"                                  Count(std_id) AS standard_count \n" +
		"                  FROM   PUBLIC.license_standard \n" +
		"                  WHERE  deleted_at IS NULL \n" +
		"                  GROUP  BY lcns_id) AS cmp_lcns \n" +
		"              ON PUBLIC.company.cmp_lcns_id = cmp_lcns.lcns_id \n" +
		"WHERE  company.deleted_at IS NULL \n" +
		"       AND Lower(company.cmp_name)  LIKE %:query% \n" +
		"ORDER  BY CASE \n" +
		"            WHEN cmp_lcns.standard_count IS NULL THEN 1 \n" +
		"            ELSE 0 \n" +
		"          END, \n" +
		"          cmp_lcns.standard_count DESC "
		, nativeQuery = true)
	Page<CompanyModel> findAllOrderByStandardsCountDESC(Pageable pageable, @Param("query") String query);
}
