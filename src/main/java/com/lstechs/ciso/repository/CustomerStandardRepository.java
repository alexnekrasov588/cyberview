package com.lstechs.ciso.repository;

import com.lstechs.ciso.model.CustomerStandardModel;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface CustomerStandardRepository extends CRUDRepository<CustomerStandardModel> {

	@Query("SELECT customerStandardModel FROM CustomerStandardModel  customerStandardModel WHERE customerStandardModel.deletedAt IS NULL AND customerStandardModel.cstStdId =(:customerStandardId) ")
	CustomerStandardModel getCustomerStandardsByCustomerStandardId(@Param("customerStandardId") int customerStandardId);

	@Query("SELECT customerStandardModel FROM CustomerStandardModel  customerStandardModel WHERE customerStandardModel.deletedAt IS NULL AND customerStandardModel.stdRqrId.stdRqrId =(:stdRqrId) ")
	List<CustomerStandardModel> getCustomerStandardsByReqId(@Param("stdRqrId") int stdRqrId);

	@Query("SELECT customerStandardModel FROM CustomerStandardModel  customerStandardModel WHERE customerStandardModel.deletedAt IS NULL AND customerStandardModel.stdRqrId.stdRqrId =(:stdRqrId) AND customerStandardModel.cmpId.cmpId = (:cmpId) ")
	CustomerStandardModel getCustomerStandardByReqIdAndCmpId(@Param("stdRqrId") int stdRqrId, @Param("cmpId") int cmpId);

	@Query("SELECT count (customerStandardModel.stdRqrId) FROM CustomerStandardModel  customerStandardModel WHERE customerStandardModel.deletedAt IS NULL AND customerStandardModel.stdRqrId.deletedAt = NULL AND customerStandardModel.compliant = com.lstechs.ciso.enums.CompliantEnum.YES AND customerStandardModel.stdId.stdId =(:stdId) ")
	int getCompliantCount(@Param("stdId") int stdId);

	@Transactional
	@Modifying(clearAutomatically = true)
	@Query(value = "update customer_standard  set deleted_at = NOW() where deleted_at is  null and \n" +
		"std_id in(select std_id from standard_company_status scs where scs.deleted_at is null and scs.created_at < now() - interval '1 year')\n", nativeQuery = true)
	void deleteCustomerStandard();

	@Query("SELECT customerStandardModel FROM CustomerStandardModel  customerStandardModel WHERE customerStandardModel.deletedAt IS NULL AND customerStandardModel.cmpId.cmpId = (:cmpId) AND customerStandardModel.stdRqrId.stdRqrId = (:stdRqrId)")
	CustomerStandardModel isCustomerStandardExist(@Param("cmpId") int cmpId, @Param("stdRqrId") int stdRqrId);

	@Query("SELECT customerStandardModel FROM CustomerStandardModel  customerStandardModel WHERE customerStandardModel.deletedAt IS NULL "
		+ "AND customerStandardModel.cmpId.cmpId = (:cmpId) AND customerStandardModel.qtrQstId.qtrQstId =(:qtrQstId) ")
	List<CustomerStandardModel> getCustomerStandardsByQuestionsId(@Param("qtrQstId") int qtrQstId, @Param("cmpId") int cmpId);

	@Query("SELECT customerStandardModel FROM CustomerStandardModel  customerStandardModel WHERE customerStandardModel.deletedAt IS NULL "
		+ "AND customerStandardModel.qtrQstId.qtrQstId =(:qtrQstId) ")
	List<CustomerStandardModel> getCustomerStandardsByQuestionsIds(@Param("qtrQstId") int qtrQstId);

	@Query("SELECT customerStandardModel FROM CustomerStandardModel  customerStandardModel WHERE customerStandardModel.deletedAt IS NULL "
		+ "AND customerStandardModel.stdId.stdId =(:stdId) ")
	List<CustomerStandardModel> getCustomerStandardsByStandardId(@Param("stdId") int stdId);

}
