package com.lstechs.ciso.repository;

import java.util.List;

import com.lstechs.ciso.model.StdSectionModel;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
public interface StdSectionRepository extends CRUDRepository<StdSectionModel> {
    @Query("SELECT stdSectionModel FROM StdSectionModel  stdSectionModel WHERE stdSectionModel.deletedAt = NULL AND stdSectionModel.stdQtrId.stdQtrId =(:questionaireId) AND stdSectionModel.parent = NULL ORDER BY stdSectionModel.order ASC")
    List<StdSectionModel> getAllSectionByQuestionaireIdWithOutParent(@Param("questionaireId") int questionaireId);

	@Query("SELECT stdSectionModel FROM StdSectionModel  stdSectionModel WHERE stdSectionModel.deletedAt = NULL AND stdSectionModel.stdQtrId.stdQtrId =(:questionaireId) ORDER BY stdSectionModel.order ASC")
	List<StdSectionModel> getAllSectionByQuestionaireId(@Param("questionaireId") int questionaireId);

	@Query("SELECT stdSectionModel FROM StdSectionModel  stdSectionModel WHERE stdSectionModel.deletedAt = NULL AND stdSectionModel.stdQtrId.stdQtrId IN(:questionaireId) ORDER BY stdSectionModel.order ASC")
	List<StdSectionModel> getAllSectionByQuestionaireIds(@Param("questionaireId") List<Integer> questionaireId);

	@Query("SELECT stdSectionModel FROM StdSectionModel  stdSectionModel WHERE stdSectionModel.deletedAt = NULL AND stdSectionModel.parent.stdSctId =(:id) ORDER BY stdSectionModel.order ASC")
    List<StdSectionModel> getAllSectionByParentId(@Param("id") int id);

    @Query("SELECT stdSectionModel FROM StdSectionModel  stdSectionModel WHERE stdSectionModel.stdSctId=(:id)")
	StdSectionModel getSectionForDelete(@Param("id") int id);

    @Query("SELECT stdSectionModel FROM StdSectionModel  stdSectionModel WHERE stdSectionModel.deletedAt IS NULL and stdSectionModel.stdSctOnPremId = (:id) ORDER BY stdSectionModel.createdAt DESC")
	List<StdSectionModel> getOnPrem(@Param("id") int id);

	@Query("SELECT  coalesce(max(stdSectionModel.order), 0) FROM StdSectionModel  stdSectionModel WHERE stdSectionModel.stdQtrId.stdQtrId = (:stdQtrId)")
	int getMaxOrder(@Param("stdQtrId") int stdQtrId);

	@Query("SELECT  coalesce(max(stdSectionModel.order), 0) FROM StdSectionModel  stdSectionModel WHERE stdSectionModel.parent.stdSctId = (:id)")
	int getMaxOrderByParentId(@Param("id") int id);

	@Query(value = "SELECT CASE WHEN EXISTS (\n" +
		"SELECT *\n" +
		"FROM std_section\n" +
		"WHERE  std_sct_id = :stdSctId and std_qtr_id = :std_qtr_id\n" +
		")\n" +
		"THEN CAST(1 AS BIT)\n" +
		"ELSE CAST(0 AS BIT) END", nativeQuery = true)
    boolean isSectionInQuestionnaire(@Param("stdSctId") int stdSctId, @Param("std_qtr_id") int stdQtrId);

	@Query("SELECT stdQtrQst.stdSctId FROM StdQtrQuestionsModel stdQtrQst WHERE stdQtrQst.deletedAt = NULL AND stdQtrQst.qtrQstId = (:qstId)")
	StdSectionModel getStdSectionModelByStdQtrQuestionsId(@Param("qstId")int qstId);

	@Transactional
	@Modifying(clearAutomatically = true)
	@Query("UPDATE StdSectionModel stdSectionModel set stdSectionModel.deletedAt = CURRENT_TIMESTAMP  WHERE stdSectionModel.stdSctOnPremId = 0")
	void deleteDuplicatedSectionOnPremise();

	@Query("SELECT stdSectionModel FROM StdSectionModel stdSectionModel WHERE stdSectionModel.deletedAt = NULL ORDER BY stdSectionModel.order ASC")
	List<StdSectionModel> getAllSectionFromOnPrem();

	@Query("SELECT stdSectionModel FROM StdSectionModel stdSectionModel WHERE stdSectionModel.deletedAt = NULL AND stdSectionModel.previousParentId = (:parentId) ORDER BY stdSectionModel.order ASC")
	StdSectionModel getAllNotParentSectionsSectionFromOnPrem(@Param("parentId") int parentId);
}
