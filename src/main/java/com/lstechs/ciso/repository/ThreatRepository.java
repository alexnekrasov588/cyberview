package com.lstechs.ciso.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.lstechs.ciso.model.ThreatModel;

@Repository
public interface ThreatRepository  extends CRUDRepository<ThreatModel> {
	@Query("SELECT threatModel FROM ThreatModel threatModel WHERE threatModel.deletedAt IS NULL")
	List<ThreatModel> getAll();

}
