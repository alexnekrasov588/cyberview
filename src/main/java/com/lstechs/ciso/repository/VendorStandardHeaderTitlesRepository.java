package com.lstechs.ciso.repository;

import com.lstechs.ciso.model.VendorStandardHeaderTitlesModel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface VendorStandardHeaderTitlesRepository extends CRUDRepository<VendorStandardHeaderTitlesModel>{

	@Query("SELECT vendorStandardHeaderTitlesModel FROM VendorStandardHeaderTitlesModel vendorStandardHeaderTitlesModel WHERE vendorStandardHeaderTitlesModel.standardId.stdId =(:standardId) AND vendorStandardHeaderTitlesModel.deletedAt IS NULL")
	VendorStandardHeaderTitlesModel getVendorHeaderTitlesByStandardId(@Param("standardId") int standardId);
}
