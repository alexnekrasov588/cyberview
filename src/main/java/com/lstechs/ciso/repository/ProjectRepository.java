package com.lstechs.ciso.repository;

import com.lstechs.ciso.enums.PrjStatusEnum;
import com.lstechs.ciso.model.ProjectModel;
import com.lstechs.ciso.model.ProjectsNameModel;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface ProjectRepository extends CRUDRepository<ProjectModel> {

	@Override
	@Query("SELECT projectModel FROM ProjectModel  projectModel WHERE projectModel.deletedAt IS NULL AND lower(projectModel.prjName) LIKE %:query% ")
	Page<ProjectModel> findAll(Pageable pageable, @Param("query") String query);

	@Query("SELECT projectModel FROM ProjectModel projectModel WHERE projectModel.prjCmpId.cmpId = :cmpId AND projectModel.deletedAt IS NULL  AND  projectModel.prjStatus <> com.lstechs.ciso.enums.PrjStatusEnum.COMPLETED")
	List<ProjectModel> getAllActiveProjectsByCmpId(@Param("cmpId") int cmpId);

	@Query("SELECT New com.lstechs.ciso.model.ProjectsNameModel(projectModel.prjId,projectModel.prjName) FROM ProjectModel projectModel"
			+ " WHERE projectModel.prjCmpId.cmpId = :cmpId AND projectModel.deletedAt IS NULL AND"
			+ "((:isTeamMember) = FALSE OR projectModel.prjId IN "
			+ "(SELECT taskModel.tskPrjID FROM TaskModel taskModel WHERE taskModel.deletedAt IS NULL AND taskModel.tskAssignee.cmpUsrId = (:cmpUsrId)) )")
	List<ProjectsNameModel> getAllProjectsName(@Param("cmpId") int cmpId,@Param("isTeamMember") Boolean isTeamMember,@Param("cmpUsrId") int cmpUsrId);


	@Query("SELECT DISTINCT projectModel.prjManagerId.cmpUsrFullName FROM ProjectModel projectModel"
			+ " WHERE projectModel.prjCmpId.cmpId = :cmpId AND projectModel.deletedAt IS NULL AND "
			+ "((:isTeamMember) = FALSE OR projectModel.prjId IN "
			+ "(SELECT taskModel.tskPrjID FROM TaskModel taskModel WHERE taskModel.deletedAt IS NULL AND taskModel.tskAssignee.cmpUsrId = (:cmpUsrId)) )")

	List<String> getAllProjectsManagers(@Param("cmpId") int cmpId,@Param("isTeamMember") Boolean isTeamMember,@Param("cmpUsrId") int cmpUsrId);

	@Query("SELECT projectModel FROM ProjectModel projectModel WHERE  projectModel.deletedAt = NULL "
			+ " AND projectModel.prjCmpId.cmpId = (:cmpId) AND "
			+ " ( (:prjIds) IS NULL OR projectModel.prjId IN (:prjIds)) AND lower(projectModel.prjName) LIKE %:query% AND "
			+ " ( (:prjStatus) IS NULL OR projectModel.prjStatus IN (:prjStatus)) AND "
			+ " ( (:prjManager) IS NULL OR  projectModel.prjManagerId.cmpUsrFullName IN (:prjManager)) AND "
			+ " ( (:isStartDateExist) = FALSE OR  (projectModel.prjStartDate BETWEEN (:prjStartDateFrom) AND (:prjStartDateTo)  ) ) AND "
			+ " ( (:isEndDateExist) = FALSE OR  (projectModel.prjEndDate BETWEEN (:prjEndDateFrom) AND (:prjEndDateTo)  ) ) AND "
			+ " ("
			+ "(:schedule) IS NULL OR"
			+ "("
			+ "("
			+ "(:schedule) = '0' AND  projectModel.prjStatus <> com.lstechs.ciso.enums.PrjStatusEnum.COMPLETED AND  projectModel.prjEndDate < CURRENT_TIMESTAMP"
			+ ") OR "
			+ "("
			+ "(:schedule) = '1' AND  (projectModel.prjStatus = com.lstechs.ciso.enums.PrjStatusEnum.COMPLETED OR  projectModel.prjEndDate >= CURRENT_TIMESTAMP)"
			+ ")"
			+ ")"
			+ ") AND "
			+ "((:isTeamMember) = FALSE OR projectModel.prjId IN "
			+ "(SELECT taskModel.tskPrjID FROM TaskModel taskModel WHERE taskModel.deletedAt IS NULL AND taskModel.tskAssignee.cmpUsrId = (:cmpUsrId))"
		    + " OR projectModel.prjManagerId = (:cmpUsrId) )")

	Page<ProjectModel> getFilterProjects(Pageable pageable, @Param("cmpId") int cmpId,
			@Param("prjIds") List<Integer> prjIds,@Param("prjStatus") List<PrjStatusEnum> prjStatus,
			@Param("prjStartDateFrom") Date prjStartDateFrom, @Param("prjStartDateTo") Date prjStartDateTo,
			@Param("prjEndDateFrom") Date prjEndDateFrom, @Param("prjEndDateTo") Date prjEndDateTo,
			@Param("schedule") String schedule, @Param("prjManager") List<String> prjManager,
			@Param("isEndDateExist") Boolean isEndDateExist, @Param("isStartDateExist") Boolean isStartDateExist,
			@Param("isTeamMember") Boolean isTeamMember,@Param("cmpUsrId") int cmpUsrId,@Param("query") String query);





	@Query("SELECT projectModel FROM ProjectModel projectModel WHERE  projectModel.deletedAt = NULL "
			+ " AND projectModel.prjCmpId.cmpId = (:cmpId) AND "
			+ " ( (:prjIds) IS NULL OR projectModel.prjId IN (:prjIds)) AND lower(projectModel.prjName) LIKE %:query% AND "
			+ " ( (:prjStatus) IS NULL OR projectModel.prjStatus IN (:prjStatus)) AND "
			+ " ( (:prjManager) IS NULL OR  projectModel.prjManagerId.cmpUsrFullName IN (:prjManager)) AND "
			+ " ( (:isStartDateExist) = FALSE OR  (projectModel.prjStartDate BETWEEN (:prjStartDateFrom) AND (:prjStartDateTo)  ) ) AND "
			+ " ( (:isEndDateExist) = FALSE OR  (projectModel.prjEndDate BETWEEN (:prjEndDateFrom) AND (:prjEndDateTo)  ) ) AND "
			+ " ("
			+ "(:schedule) IS NULL OR"
			+ "("
			+ "("
			+ "(:schedule) = '0' AND  projectModel.prjStatus <> com.lstechs.ciso.enums.PrjStatusEnum.COMPLETED AND  projectModel.prjEndDate < CURRENT_TIMESTAMP"
			+ ") OR "
			+ "("
			+ "(:schedule) = '1' AND  (projectModel.prjStatus = com.lstechs.ciso.enums.PrjStatusEnum.COMPLETED OR  projectModel.prjEndDate >= CURRENT_TIMESTAMP)"
			+ ")"
			+ ")"
			+ ") AND "
			+ "((:isTeamMember) = FALSE OR projectModel.prjId IN "
			+ "(SELECT taskModel.tskPrjID FROM TaskModel taskModel WHERE taskModel.deletedAt IS NULL AND taskModel.tskAssignee.cmpUsrId = (:cmpUsrId)) )")

	List<ProjectModel> getFilterProjectsList(@Param("cmpId") int cmpId,
			@Param("prjIds") List<Integer> prjIds,@Param("prjStatus") List<PrjStatusEnum> prjStatus,
			@Param("prjStartDateFrom") Date prjStartDateFrom, @Param("prjStartDateTo") Date prjStartDateTo,
			@Param("prjEndDateFrom") Date prjEndDateFrom, @Param("prjEndDateTo") Date prjEndDateTo,
			@Param("schedule") String schedule, @Param("prjManager") List<String> prjManager,
			@Param("isEndDateExist") Boolean isEndDateExist, @Param("isStartDateExist") Boolean isStartDateExist,
			@Param("isTeamMember") Boolean isTeamMember,@Param("cmpUsrId") int cmpUsrId,@Param("query") String query);




	@Query("SELECT projectModel FROM ProjectModel projectModel WHERE  projectModel.deletedAt IS NULL "
			+ " AND projectModel.prjCmpId.cmpId = (:cmpId) AND "
			+ " ((:isEndDateExist) = FALSE OR projectModel.prjEndDate <= (:prjEndDate) ) AND "
			+ " ((:isStartDateExist) = FALSE OR projectModel.prjStartDate >= (:prjStartDate) ) ")
	List<ProjectModel> getProjectReport(@Param("cmpId") int cmpId,
			@Param("isEndDateExist") Boolean isEndDateExist,
			@Param("isStartDateExist") Boolean isStartDateExist,
			@Param("prjStartDate") Date prjStartDate,
			@Param("prjEndDate") Date prjEndDate);


	@Query(value = "SELECT * \n" + "FROM   project \n" + "WHERE  prj_id IN (SELECT tsk_prjid \n"
			+ "                  FROM   public.task \n" + "                  WHERE  tsk_assignee = (:assignee) \n"
			+ "                         AND deleted_at IS NULL) AND lower(project.prj_name) LIKE %:query%"

			, nativeQuery = true)
	Page<ProjectModel> getAllProjectsByTskAssignee(Pageable pageable, @Param("assignee") int assignee,
			@Param("query") String query);

	@Query(value = "SELECT pro.prj_id, \n" + "       pro.created_at, \n" + "       pro.deleted_at, \n"
			+ "       pro.updated_at, \n" + "       pro.prj_budget, \n" + "       pro.prj_description, \n"
			+ "       pro.prj_end_date, \n" + "       pro.prj_name, \n" + "       pro.prj_start_date, \n"
			+ "       pro.prj_status, \n" + "       pro.prj_cmp_id, \n" + "       pro.prj_manager_id \n"
			+ "FROM   (SELECT project_detailes.prj_id, \n" + "               project_detailes.created_at, \n"
			+ "               project_detailes.deleted_at, \n" + "               project_detailes.updated_at, \n"
			+ "               project_detailes.prj_budget, \n" + "               project_detailes.prj_description, \n"
			+ "               project_detailes.prj_end_date, \n" + "               project_detailes.prj_name, \n"
			+ "               project_detailes.prj_start_date, \n" + "               project_detailes.prj_status, \n"
			+ "               project_detailes.prj_cmp_id, \n" + "               project_detailes.prj_manager_id, \n"
			+ "               CASE \n" + "                 WHEN Count(project_detailes.tsk_id) < 1 THEN 0.00 \n"
			+ "                 ELSE Cast(100.0 * Sum(CASE project_detailes.status \n"
			+ "                                         WHEN 2 THEN 1 \n"
			+ "                                         ELSE 0 \n"
			+ "                                       END) / Count(project_detailes.tsk_id) AS \n"
			+ "                           DECIMAL \n" + "                           (10, 2) \n"
			+ "                      ) \n" + "               END AS progress \n" + "        FROM   (SELECT * \n"
			+ "                FROM   project AS p \n" + "                       LEFT JOIN (SELECT task.tsk_id, \n"
			+ "                                         task.status, \n"
			+ "                                         task.tsk_prjid \n"
			+ "                                  FROM   task \n"
			+ "                                  WHERE  deleted_at IS NULL) AS task \n"
			+ "                              ON p.prj_id = task.tsk_prjid) AS project_detailes \n"
			+ "        WHERE  deleted_at IS NULL \n" + "               AND prj_id IN(SELECT prj_id \n"
			+ "                             FROM   project \n"
			+ "                             WHERE  prj_id IN (SELECT tsk_prjid \n"
			+ "                                               FROM   PUBLIC.task \n"
			+ "                                               WHERE \n"
			+ "                                    tsk_assignee = ( :assignee ) \n"
			+ "                                    AND deleted_at IS NULL)) \n"
			+ "        GROUP  BY project_detailes.prj_id, \n" + "                  project_detailes.created_at, \n"
			+ "                  project_detailes.deleted_at, \n" + "                  project_detailes.updated_at, \n"
			+ "                  project_detailes.prj_budget, \n"
			+ "                  project_detailes.prj_description, \n"
			+ "                  project_detailes.prj_end_date, \n" + "                  project_detailes.prj_name, \n"
			+ "                  project_detailes.prj_start_date, \n"
			+ "                  project_detailes.prj_status, \n" + "                  project_detailes.prj_cmp_id, \n"
			+ "                  project_detailes.prj_manager_id \n"
			+ "        ORDER  BY progress ASC) AS pro AND lower(project.prj_name) LIKE %:query%", nativeQuery = true)
	Page<ProjectModel> getAllProjectsListByTskAssigneeOrderByProgressAsc(Pageable pageable,
			@Param("assignee") int assignee, @Param("query") String query);

	@Query(value = "SELECT pro.prj_id, \n" + "       pro.created_at, \n" + "       pro.deleted_at, \n"
			+ "       pro.updated_at, \n" + "       pro.prj_budget, \n" + "       pro.prj_description, \n"
			+ "       pro.prj_end_date, \n" + "       pro.prj_name, \n" + "       pro.prj_start_date, \n"
			+ "       pro.prj_status, \n" + "       pro.prj_cmp_id, \n" + "       pro.prj_manager_id \n"
			+ "FROM   (SELECT project_detailes.prj_id, \n" + "               project_detailes.created_at, \n"
			+ "               project_detailes.deleted_at, \n" + "               project_detailes.updated_at, \n"
			+ "               project_detailes.prj_budget, \n" + "               project_detailes.prj_description, \n"
			+ "               project_detailes.prj_end_date, \n" + "               project_detailes.prj_name, \n"
			+ "               project_detailes.prj_start_date, \n" + "               project_detailes.prj_status, \n"
			+ "               project_detailes.prj_cmp_id, \n" + "               project_detailes.prj_manager_id, \n"
			+ "               CASE \n" + "                 WHEN Count(project_detailes.tsk_id) < 1 THEN 0.00 \n"
			+ "                 ELSE Cast(100.0 * Sum(CASE project_detailes.status \n"
			+ "                                         WHEN 2 THEN 1 \n"
			+ "                                         ELSE 0 \n"
			+ "                                       END) / Count(project_detailes.tsk_id) AS \n"
			+ "                           DECIMAL \n" + "                           (10, 2) \n"
			+ "                      ) \n" + "               END AS progress \n" + "        FROM   (SELECT * \n"
			+ "                FROM   project AS p \n" + "                       LEFT JOIN (SELECT task.tsk_id, \n"
			+ "                                         task.status, \n"
			+ "                                         task.tsk_prjid \n"
			+ "                                  FROM   task \n"
			+ "                                  WHERE  deleted_at IS NULL) AS task \n"
			+ "                              ON p.prj_id = task.tsk_prjid) AS project_detailes \n"
			+ "        WHERE  deleted_at IS NULL \n" + "               AND prj_id IN(SELECT prj_id \n"
			+ "                             FROM   project \n"
			+ "                             WHERE  prj_id IN (SELECT tsk_prjid \n"
			+ "                                               FROM   PUBLIC.task \n"
			+ "                                               WHERE \n"
			+ "                                    tsk_assignee = ( :assignee ) \n"
			+ "                                    AND deleted_at IS NULL)) \n"
			+ "        GROUP  BY project_detailes.prj_id, \n" + "                  project_detailes.created_at, \n"
			+ "                  project_detailes.deleted_at, \n" + "                  project_detailes.updated_at, \n"
			+ "                  project_detailes.prj_budget, \n"
			+ "                  project_detailes.prj_description, \n"
			+ "                  project_detailes.prj_end_date, \n" + "                  project_detailes.prj_name, \n"
			+ "                  project_detailes.prj_start_date, \n"
			+ "                  project_detailes.prj_status, \n" + "                  project_detailes.prj_cmp_id, \n"
			+ "                  project_detailes.prj_manager_id \n"
			+ "        ORDER  BY progress DESC) AS pro AND lower(project.prj_name) LIKE %:query%", nativeQuery = true)
	Page<ProjectModel> getAllProjectsListByTskAssigneeOrderByProgressDesc(Pageable pageable,
			@Param("assignee") int assignee, @Param("query") String query);

	@Query(value = "SELECT * FROM project WHERE deleted_at is null and  prj_status <> 0 and  prj_id IN (SELECT tsk_prjid \n "
			+ " FROM task  WHERE  tsk_assignee = (:assignee)  AND deleted_at IS NULL) ", nativeQuery = true)
	List<ProjectModel> getAllProjectsListByTskAssignee(@Param("assignee") int assignee);

	@Query("SELECT projectModel FROM ProjectModel projectModel WHERE projectModel.prjCmpId.cmpId = :cmpId"
			+  " AND projectModel.deletedAt IS NULL AND lower(projectModel.prjName) LIKE %:query%")
	Page<ProjectModel> getAllProjectsByCompanyId(Pageable pageable, @Param("cmpId") int cmpId,
			@Param("query") String query);

	@Query(value = "SELECT pro.prj_id, \n" + "       pro.created_at, \n" + "       pro.deleted_at, \n"
			+ "       pro.updated_at, \n" + "       pro.prj_budget, \n" + "       pro.prj_description, \n"
			+ "       pro.prj_end_date, \n" + "       pro.prj_name, \n" + "       pro.prj_start_date, \n"
			+ "       pro.prj_status, \n" + "       pro.prj_cmp_id, \n" + "       pro.prj_manager_id \n"
			+ "FROM  (SELECT project_detailes.prj_id, \n" + "              project_detailes.created_at, \n"
			+ "              project_detailes.deleted_at, \n" + "              project_detailes.updated_at, \n"
			+ "              project_detailes.prj_budget, \n" + "              project_detailes.prj_description, \n"
			+ "              project_detailes.prj_end_date, \n" + "              project_detailes.prj_name, \n"
			+ "              project_detailes.prj_start_date, \n" + "              project_detailes.prj_status, \n"
			+ "              project_detailes.prj_cmp_id, \n" + "              project_detailes.prj_manager_id, \n"
			+ "              CASE \n" + "                WHEN Count(project_detailes.tsk_id) < 1 THEN 0.00 \n"
			+ "                ELSE Cast(100.0 * Sum(CASE project_detailes.status \n"
			+ "                                        WHEN 2 THEN 1 \n"
			+ "                                        ELSE 0 \n"
			+ "                                      END) / Count(project_detailes.tsk_id) AS \n"
			+ "                          DECIMAL \n" + "                          (10, 2)) \n"
			+ "              END AS progress \n" + "       FROM   (SELECT * \n"
			+ "               FROM   project AS p \n" + "                      LEFT JOIN (SELECT task.tsk_id, \n"
			+ "                                        task.status, \n"
			+ "                                        task.tsk_prjid \n"
			+ "                                 FROM   task \n"
			+ "                                 WHERE  deleted_at IS NULL) AS task \n"
			+ "                             ON p.prj_id = task.tsk_prjid) AS project_detailes \n"
			+ "       WHERE  deleted_at IS NULL \n" + "              AND prj_cmp_id = ( :cmpId ) \n"
			+ "       GROUP  BY project_detailes.prj_id, \n" + "                 project_detailes.created_at, \n"
			+ "                 project_detailes.deleted_at, \n" + "                 project_detailes.updated_at, \n"
			+ "                 project_detailes.prj_budget, \n"
			+ "                 project_detailes.prj_description, \n"
			+ "                 project_detailes.prj_end_date, \n" + "                 project_detailes.prj_name, \n"
			+ "                 project_detailes.prj_start_date, \n"
			+ "                 project_detailes.prj_status, \n" + "                 project_detailes.prj_cmp_id, \n"
			+ "                 project_detailes.prj_manager_id \n"
			+ "       ORDER  BY progress DESC) AS pro", nativeQuery = true)
	Page<ProjectModel> getAllProjectsByCompanyIdOrderByProgressDesc(Pageable pageable, @Param("cmpId") int cmpId);

	@Query(value = "SELECT pro.prj_id, \n" + "       pro.created_at, \n" + "       pro.deleted_at, \n"
			+ "       pro.updated_at, \n" + "       pro.prj_budget, \n" + "       pro.prj_description, \n"
			+ "       pro.prj_end_date, \n" + "       pro.prj_name, \n" + "       pro.prj_start_date, \n"
			+ "       pro.prj_status, \n" + "       pro.prj_cmp_id, \n" + "       pro.prj_manager_id \n"
			+ "FROM  (SELECT project_detailes.prj_id, \n" + "              project_detailes.created_at, \n"
			+ "              project_detailes.deleted_at, \n" + "              project_detailes.updated_at, \n"
			+ "              project_detailes.prj_budget, \n" + "              project_detailes.prj_description, \n"
			+ "              project_detailes.prj_end_date, \n" + "              project_detailes.prj_name, \n"
			+ "              project_detailes.prj_start_date, \n" + "              project_detailes.prj_status, \n"
			+ "              project_detailes.prj_cmp_id, \n" + "              project_detailes.prj_manager_id, \n"
			+ "              CASE \n" + "                WHEN Count(project_detailes.tsk_id) < 1 THEN 0.00 \n"
			+ "                ELSE Cast(100.0 * Sum(CASE project_detailes.status \n"
			+ "                                        WHEN 2 THEN 1 \n"
			+ "                                        ELSE 0 \n"
			+ "                                      END) / Count(project_detailes.tsk_id) AS \n"
			+ "                          DECIMAL \n" + "                          (10, 2)) \n"
			+ "              END AS progress \n" + "       FROM   (SELECT * \n"
			+ "               FROM   project AS p \n" + "                      LEFT JOIN (SELECT task.tsk_id, \n"
			+ "                                        task.status, \n"
			+ "                                        task.tsk_prjid \n"
			+ "                                 FROM   task \n"
			+ "                                 WHERE  deleted_at IS NULL) AS task \n"
			+ "                             ON p.prj_id = task.tsk_prjid) AS project_detailes \n"
			+ "       WHERE  deleted_at IS NULL \n" + "              AND prj_cmp_id = ( :cmpId ) \n"
			+ "       GROUP  BY project_detailes.prj_id, \n" + "                 project_detailes.created_at, \n"
			+ "                 project_detailes.deleted_at, \n" + "                 project_detailes.updated_at, \n"
			+ "                 project_detailes.prj_budget, \n"
			+ "                 project_detailes.prj_description, \n"
			+ "                 project_detailes.prj_end_date, \n" + "                 project_detailes.prj_name, \n"
			+ "                 project_detailes.prj_start_date, \n"
			+ "                 project_detailes.prj_status, \n" + "                 project_detailes.prj_cmp_id, \n"
			+ "                 project_detailes.prj_manager_id \n"
			+ "       ORDER  BY progress ASC) AS pro", nativeQuery = true)
	Page<ProjectModel> getAllProjectsByCompanyIdOrderByProgressAsc(Pageable pageable, @Param("cmpId") int cmpId);

	@Query(value = "SELECT project_detailes.prj_id, \n" + "       project_detailes.created_at, \n"
			+ "       project_detailes.deleted_at, \n" + "       project_detailes.updated_at, \n"
			+ "       project_detailes.prj_budget, \n" + "       project_detailes.prj_description, \n"
			+ "       project_detailes.prj_end_date, \n" + "       project_detailes.prj_name, \n"
			+ "       project_detailes.prj_start_date, \n" + "       project_detailes.prj_status, \n"
			+ "       project_detailes.prj_cmp_id, \n" + "       project_detailes.prj_manager_id, \n"
			+ "       Count(project_detailes.tsk_id) AS totalTask, \n" + "       Sum(CASE project_detailes.status \n"
			+ "             WHEN 2 THEN 1 \n" + "             ELSE 0 \n"
			+ "           END)                       AS completedTask, \n" + "       CASE \n"
			+ "         WHEN Count(project_detailes.tsk_id) < 1 THEN 0.00 \n"
			+ "         ELSE Cast(100.0 * Sum(CASE project_detailes.status \n"
			+ "                                 WHEN 2 THEN 1 \n" + "                                 ELSE 0 \n"
			+ "                               END) / Count(project_detailes.tsk_id) AS DECIMAL( \n"
			+ "                   10, 2)) \n" + "       END                            AS progress \n"
			+ "FROM   (SELECT * \n" + "        FROM   project AS p \n"
			+ "               LEFT JOIN (SELECT task.tsk_id, \n" + "                                 task.status, \n"
			+ "                                 task.tsk_prjid \n" + "                          FROM   task \n"
			+ "                          WHERE  deleted_at IS NULL) AS task \n"
			+ "                      ON p.prj_id = task.tsk_prjid) AS project_detailes \n"
			+ "WHERE  deleted_at IS NULL \n" + "       AND prj_cmp_id = ( :prjCmpId ) \n"
			+ "GROUP  BY project_detailes.prj_id, \n" + "          project_detailes.created_at, \n"
			+ "          project_detailes.deleted_at, \n" + "          project_detailes.updated_at, \n"
			+ "          project_detailes.prj_budget, \n" + "          project_detailes.prj_description, \n"
			+ "          project_detailes.prj_end_date, \n" + "          project_detailes.prj_name, \n"
			+ "          project_detailes.prj_start_date, \n" + "          project_detailes.prj_status, \n"
			+ "          project_detailes.prj_cmp_id, \n"
			+ "          project_detailes.prj_manager_id ", nativeQuery = true)
	List<Object> getAllProjectOverview(@Param("prjCmpId") int cmpId);

	@Query("SELECT projectModel from ProjectModel projectModel where projectModel.deletedAt IS NULL And projectModel.prjId = (:prjId) AND projectModel.prjCmpId.cmpId = (:cmpId)")
	ProjectModel getProjectById(@Param("prjId") int prjId, @Param("cmpId") int cmpId);


}
