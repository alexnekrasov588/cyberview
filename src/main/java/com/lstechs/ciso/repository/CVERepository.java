package com.lstechs.ciso.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.lstechs.ciso.model.CVEModel;

@Repository
public interface CVERepository extends CRUDRepository<CVEModel> {

    @Override
    @Query("SELECT cveModel FROM CVEModel cveModel WHERE cveModel.deletedAt = NULL AND (lower(cveModel.cveTitle) LIKE %:query% OR lower(cveModel.cveDescription) LIKE %:query% ) AND cveModel.cveDescription NOT LIKE '** REJECT **%'")
    Page<CVEModel> findAll(Pageable pageable,@Param("query") String query);

//	@Query("SELECT cveModel FROM CVEModel cveModel WHERE cveModel.deletedAt = NULL ORDER BY cveModel.createdAt DESC LIMIT 2")
//	List<CVEModel> getLatestCVEForDashboard();

	@Query(value = "SELECT * FROM cve WHERE deleted_at ISNULL ORDER BY published_date DESC LIMIT 2", nativeQuery = true)
	List<CVEModel> getLatestCVEForDashboard();

	@Override
	@Query("SELECT MAX(cveModel.cveUpdateOnPrem) FROM  CVEModel cveModel")
	Date lastUpdateAt();

	@Query("SELECT  cveModel FROM CVEModel cveModel "
		+ "WHERE cveModel.cveIsAvilable = TRUE "
		+ " AND cveModel.deletedAt IS NULL "
		+ " AND exists "
		+ " (SELECT 1 FROM AssetModel assetModel where assetModel.deletedAt is NULL AND "
		+ " assetModel.cmpId.cmpId = (:cmpId) AND "
//		+ " (cveModel.cveTitle like ( CONCAT('% ',assetModel.astName,' %')) OR "
//		+ "	cveModel.cveTitle like ( CONCAT('% ',assetModel.astManufactor,' %')) OR"
//		+ "	cveModel.cveTitle like ( CONCAT('% ',assetModel.astProduct,' %')) OR"
		+ "	(cveModel.cveDescription like ( CONCAT('% ',assetModel.astName,' %')) OR "
		+ "	cveModel.cveDescription like ( CONCAT('% ',assetModel.astManufactor,' %')) OR"
		+ "	cveModel.cveDescription like ( CONCAT('% ',assetModel.astProduct,' %')) ) "
		+ " ) "
		+ "  ORDER BY cveModel.publishedDate DESC")
	Page<CVEModel> findAllCVEsByAssetFilter(Pageable pageable, @Param("cmpId") int cmpId);
}
