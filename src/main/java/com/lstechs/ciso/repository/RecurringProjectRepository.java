
package com.lstechs.ciso.repository;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.lstechs.ciso.model.RecurringProjectModel;

@Repository
public interface RecurringProjectRepository extends CRUDRepository<RecurringProjectModel> {
	
    
    @Query("SELECT recurringProject FROM RecurringProjectModel recurringProject WHERE recurringProject.deletedAt = NULL AND recurringProject.prjId.prjId =(:prjId)") 
    RecurringProjectModel findRecurringProjectByPrjId(@Param("prjId") int prjId);

}

