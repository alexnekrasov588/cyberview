package com.lstechs.ciso.repository;

import com.lstechs.ciso.model.CVESourcesModel;
import com.lstechs.ciso.model.NewsSourcesModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CVESourcesRepository extends CRUDRepository<CVESourcesModel> {
  @Override
  @Query("SELECT cveSourcesModel FROM CVESourcesModel cveSourcesModel WHERE cveSourcesModel.cveSrcIsActive = TRUE AND cveSourcesModel.deletedAt = NULL")
  Page<CVESourcesModel> findAll(Pageable pageable, @Param("query") String query);
  
	@Query("SELECT cveSourcesModel FROM CVESourcesModel cveSourcesModel WHERE  cveSourcesModel.cveSrcOnPremId = (:pId)")
	CVESourcesModel getOnPrem(@Param("pId") int pId);
}
