package com.lstechs.ciso.repository;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.lstechs.ciso.model.RecurringTaskModel;

@Repository
public interface RecurringTaskRepository extends CRUDRepository<RecurringTaskModel> {
	
    
    @Query("SELECT recurringTask FROM RecurringTaskModel recurringTask WHERE recurringTask.deletedAt = NULL AND recurringTask.tskId.tskId =(:tskId)") 
    RecurringTaskModel findRecurringTaskByTaskId(@Param("tskId") int tskId);

}
