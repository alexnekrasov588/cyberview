package com.lstechs.ciso.repository;

import java.util.Date;
import java.util.List;

import com.lstechs.ciso.model.CompanyModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.lstechs.ciso.enums.AssetTypeEnum;
import com.lstechs.ciso.model.AssetModel;
import com.lstechs.ciso.model.FilterModel;

@Repository
public interface AssetRepository extends CRUDRepository<AssetModel> {

	@Query("SELECT assetModel FROM AssetModel assetModel WHERE assetModel.cmpId.cmpId = (:cmpId) AND assetModel.deletedAt IS NULL"
		+ " AND lower(assetModel.astName) like lower(CONCAT('%',(:query),'%'))"
		+ " AND ((:type) IS NULL OR assetModel.astType.astType = (:type)) ")
	Page<AssetModel> getAllAssetByCmpId(Pageable pageable, @Param("cmpId") int cmpId, @Param("query") String query, @Param("type") AssetTypeEnum type);

	@Query("SELECT NEW com.lstechs.ciso.model.FilterModel(assetModel.astId,assetModel.astName) FROM AssetModel assetModel "
		+ " WHERE assetModel.cmpId.cmpId = (:cmpId) AND assetModel.deletedAt IS NULL")
	List<FilterModel> getAllAssetForNews(@Param("cmpId") int cmpId);

	@Query("SELECT DISTINCT assetModel.astName FROM AssetModel assetModel "
		+ " WHERE assetModel.cmpId.cmpId = (:cmpId) AND assetModel.deletedAt IS NULL "
		+ "AND ( (:type) IS NULL OR assetModel.astType.astType =  (:type))")
	List<String> getAllAssetName(@Param("cmpId") int cmpId, @Param("type") AssetTypeEnum type);

	@Query("SELECT DISTINCT assetModel.astVersion  FROM AssetModel assetModel "
		+ " WHERE assetModel.cmpId.cmpId = (:cmpId) AND assetModel.deletedAt IS NULL "
		+ "AND ( (:type) IS NULL OR assetModel.astType.astType =  (:type))")
	List<String> getAllAssetVersion(@Param("cmpId") int cmpId, @Param("type") AssetTypeEnum type);

	@Query("SELECT DISTINCT assetModel.astModel  FROM AssetModel assetModel "
		+ " WHERE assetModel.cmpId.cmpId = (:cmpId) AND assetModel.deletedAt IS NULL "
		+ "AND ( (:type) IS NULL OR assetModel.astType.astType =  (:type))")
	List<String> getAllAssetModel(@Param("cmpId") int cmpId, @Param("type") AssetTypeEnum type);

	@Query("SELECT DISTINCT assetModel.astProduct  FROM AssetModel assetModel "
		+ " WHERE assetModel.cmpId.cmpId = (:cmpId) AND assetModel.deletedAt IS NULL "
		+ "AND ( (:type) IS NULL OR assetModel.astType.astType =  (:type))")
	List<String> getAllAssetProduct(@Param("cmpId") int cmpId, @Param("type") AssetTypeEnum type);

	@Query("SELECT DISTINCT assetModel.astManufactor  FROM AssetModel assetModel "
		+ " WHERE assetModel.cmpId.cmpId = (:cmpId) AND assetModel.deletedAt IS NULL "
		+ "AND ( (:type) IS NULL OR assetModel.astType.astType =  (:type))")
	List<String> getAllAssetManufactor(@Param("cmpId") int cmpId, @Param("type") AssetTypeEnum type);

	@Query("SELECT DISTINCT assetModel.astOperator FROM AssetModel assetModel "
		+ " WHERE assetModel.cmpId.cmpId = (:cmpId) AND assetModel.deletedAt IS NULL "
		+ "AND ( (:type) IS NULL OR assetModel.astType.astType =  (:type))")
	List<String> getAllAssetOperator(@Param("cmpId") int cmpId, @Param("type") AssetTypeEnum type);

	@Query("SELECT DISTINCT assetModel.astOwner FROM AssetModel assetModel "
		+ " WHERE assetModel.cmpId.cmpId = (:cmpId) AND assetModel.deletedAt IS NULL "
		+ "AND ( (:type) IS NULL OR assetModel.astType.astType =  (:type))")
	List<String> getAllAssetOwner(@Param("cmpId") int cmpId, @Param("type") AssetTypeEnum type);

	@Query("SELECT DISTINCT assetModel.astType.astTypeName FROM AssetModel assetModel "
		+ " WHERE assetModel.cmpId.cmpId = (:cmpId) AND assetModel.deletedAt IS NULL "
		+ "AND ( (:type) IS NULL OR assetModel.astType.astType =  (:type))")
	List<String> getAllAssetType(@Param("cmpId") int cmpId, @Param("type") AssetTypeEnum type);


	@Query("SELECT assetModel FROM AssetModel assetModel WHERE assetModel.cmpId.cmpId = (:cmpId) AND assetModel.deletedAt IS NULL AND "
		+ " ( (:type) IS NULL OR assetModel.astType.astType = (:type)) AND "
		+ " ((:astName) IS NULL OR assetModel.astName IN (:astName)) AND "
		+ " ((:astType) IS NULL OR assetModel.astType.astTypeName IN (:astType)) AND "
		+ " ((:astOperator) IS NULL OR assetModel.astOperator IN (:astOperator)) AND "
		+ " ((:astOwner) IS NULL OR assetModel.astOwner IN (:astOwner)) AND "
		+ " ((:astVersion) IS NULL OR assetModel.astVersion IN (:astVersion)) AND "
		+ " ((:astManufactor) IS NULL OR assetModel.astManufactor IN (:astManufactor)) AND "
		+ " ((:astProduct) IS NULL OR assetModel.astProduct IN (:astProduct)) AND "
		+ " ((:astModel) IS NULL OR assetModel.astModel IN (:astModel)) AND "
		+ " ( (:isStartDateExist) = FALSE OR  (assetModel.astStartDate BETWEEN (:astStartDateFrom) AND (:astStartDateTo)  ) ) AND "
		+ " ( (:isEndDateExist) = FALSE OR  (assetModel.astEndDate BETWEEN (:astEndDateFrom) AND (:astEndDateTo)  ) )  ")
	Page<AssetModel> getAssetsFilter(Pageable pageable, @Param("cmpId") int cmpId,
									 @Param("astName") List<String> astName, @Param("astType") List<String> astType,
									 @Param("astOperator") List<String> astOperator, @Param("astOwner") List<String> astOwner,
									 @Param("astVersion") List<String> astVersion, @Param("astManufactor") List<String> astManufactor,
									 @Param("astProduct") List<String> astProduct, @Param("astModel") List<String> astModel, @Param("type") AssetTypeEnum type,
									 @Param("isStartDateExist") Boolean isStartDateExist, @Param("astStartDateFrom") Date astStartDateFrom, @Param("astStartDateTo") Date astStartDateTo,
									 @Param("isEndDateExist") Boolean isEndDateExist, @Param("astEndDateFrom") Date astEndDateFrom, @Param("astEndDateTo") Date astEndDateTo);

	@Query("SELECT assetModel FROM AssetModel assetModel WHERE assetModel.deletedAt IS NULL AND assetModel.cmpId.cmpId = (:cmpId)")
	List<AssetModel> checkForCompanyAssets(@Param("cmpId") int cmpId);
}
