package com.lstechs.ciso.repository;

import com.lstechs.ciso.model.QuestionnaireNameModel;
import com.lstechs.ciso.model.StdQuestionaireModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StdQuestionaireRepository extends CRUDRepository<StdQuestionaireModel> {
	@Query("SELECT stdQuestionaireModel FROM StdQuestionaireModel  stdQuestionaireModel WHERE stdQuestionaireModel.deletedAt = NULL AND stdQuestionaireModel.stdId.stdId =(:templateId) AND stdQuestionaireModel.isQuestionnaireCreatedByCompany = false")
	Page<StdQuestionaireModel> getAllQuestionaireByTemplateId(Pageable pageable, @Param("templateId") int templateId);

	@Query("SELECT stdQuestionaireModel FROM StdQuestionaireModel  stdQuestionaireModel WHERE stdQuestionaireModel.deletedAt = NULL AND stdQuestionaireModel.stdId.stdId =(:templateId) ")
	List<StdQuestionaireModel> getQuestionaireListByTemplateId(@Param("templateId") int templateId);

	@Query("SELECT stdQuestionaireModel FROM StdQuestionaireModel  stdQuestionaireModel WHERE stdQuestionaireModel.deletedAt = NULL AND stdQuestionaireModel.stdId.stdId =(:templateId) AND (stdQuestionaireModel.companyId = 0 OR stdQuestionaireModel.companyId = (:companyId)) ")
	List<StdQuestionaireModel> getQuestionaireListByTemplateIdWithDuplicated(@Param("templateId") int templateId, @Param("companyId") int companyId);

	@Query("SELECT stdQuestionaireModel FROM StdQuestionaireModel  stdQuestionaireModel WHERE stdQuestionaireModel.deletedAt = NULL AND stdQuestionaireModel.stdId.stdId IN(:templateId) ")
	List<StdQuestionaireModel> getQuestionaireListByStdIds(@Param("templateId") List<Integer> templateId);

	@Query("SELECT stdQuestionaireModel FROM StdQuestionaireModel  stdQuestionaireModel WHERE stdQuestionaireModel.deletedAt = NULL AND stdQuestionaireModel.stdId.stdId IN(:templateId) AND stdQuestionaireModel.duplicatedFrom = 0")
	List<StdQuestionaireModel> getQuestionaireListByStdIdsForOnPremise(@Param("templateId") List<Integer> templateId);

	@Query("SELECT std.stdQtrId FROM StdSectionModel  std WHERE std.deletedAt = NULL AND std.stdSctId = (:stdId)")
	StdQuestionaireModel getStdQuestionaireBySectionId(@Param("stdId") int stdId);

	@Query("SELECT stdQuestionaireModel FROM StdQuestionaireModel  stdQuestionaireModel WHERE stdQuestionaireModel.deletedAt = NULL AND stdQuestionaireModel.stdQtrId = (:questionnaireId)")
	StdQuestionaireModel getStdQuestionnaireByQuestionnaireId(@Param("questionnaireId") int questionnaireId);

	@Query("SELECT New com.lstechs.ciso.model.QuestionnaireNameModel(stdQuestionaireModel.stdQtrId,stdQuestionaireModel.stdQtrName)"
		+ " FROM StdQuestionaireModel  stdQuestionaireModel WHERE stdQuestionaireModel.deletedAt = NULL AND stdQuestionaireModel.stdId.stdId IN(:templateId) ")
	List<QuestionnaireNameModel> getQuestionairesNameByStdIds(@Param("templateId") List<Integer> templateId);

	@Query("SELECT stdQuestionaireModel FROM StdQuestionaireModel  stdQuestionaireModel WHERE"
		+ " stdQuestionaireModel.deletedAt = NULL AND stdQuestionaireModel.stdQtrId IN(:ids) ")
	List<StdQuestionaireModel> getQuestionairesByIds(@Param("ids") List<Integer> ids);

	@Query("SELECT stdQuestionaireModel FROM StdQuestionaireModel stdQuestionaireModel WHERE stdQuestionaireModel.deletedAt = NULL AND stdQuestionaireModel.stdQtrOnPremId=(:id) ORDER BY stdQuestionaireModel.createdAt DESC")
	List<StdQuestionaireModel> getOnPrem(@Param("id") int id);

	@Query("SELECT stdQuestionaireModel FROM StdQuestionaireModel stdQuestionaireModel WHERE stdQuestionaireModel.deletedAt = NULL AND stdQuestionaireModel.duplicatedFrom=(:id)")
	List<StdQuestionaireModel> getQuestionnairesByDuplicatedFrom(@Param("id") int id);
}
