package com.lstechs.ciso;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class CisoApplication {
	public static void main(String[] args) {
		SpringApplication.run(CisoApplication.class, args);
	}
}
