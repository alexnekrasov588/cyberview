package com.lstechs.ciso.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {
	public static String formatDate(Date d) {
		SimpleDateFormat sdt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");
		return sdt.format(d);
	}
}
